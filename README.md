# API Dinbog [![pipeline status](https://gitlab.com/dinbog-estable/backend/api/badges/master/pipeline.svg)](https://gitlab.com/dinbog-estable/backend/api/-/commits/master) [![coverage report](https://gitlab.com/dinbog-estable/backend/api/badges/master/coverage.svg)](https://gitlab.com/dinbog-estable/backend/api/-/commits/master)

API Rest para servir de capa Backend de las funcionalidades principales de [DINBOG](https://dinbog.com)
* Version: ESTABLE
* Framework: [Spring Boot 2](https://spring.io/projects/spring-boot) (version 2.2.4)
* Version Java: 13 [OpenJDK](https://jdk.java.net/13/)


# [Fuentes del Proyecto](src/main/java/com/dinbog/api) (src/main/java/com/dinbog/api)
En esta ruta se encuentran los fuentes java que ejecutan las funciones principales del sistema, hay 2 tipos de clases: las comunes y las que estan ordenadas por lógica de negocio

## Clases comunes/uso multiple
Carpetas con clases que se utilizan en multiples lugares:

### [config](src/main/java/com/dinbog/api/config)
Capa de configuración, clases donde se manejan las configuraciones generales de la aplicación
### [exception](src/main/java/com/dinbog/api/exception)
Manejo global de excepciones, objetos para crear, capturar y mandar respuestas sobre cualquier error
### [model](src/main/java/com/dinbog/api/model)
Objetos que representan las tablas de base de datos
### [security](src/main/java/com/dinbog/api/security)
Manejo de seguridad de la api
### [util](src/main/java/com/dinbog/api/util)
Objetos y clases de uso comun
* [mapper](src/main/java/com/dinbog/api/util/mapper): Subcarpeta de [util](src/main/java/com/dinbog/api/util) donde están todas las clases que se encargan de mapear clases model a dto's (viceversa de ser necesario)

## Clases asociadas a la lógica del negocio
Carpetas que agrupan las clases relacionadas a cada entidad principal de negocio de la api. Se manejan clases de tipo: [@RestController](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/RestController.html), [@Service](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/stereotype/Service.html), [@Repository](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/stereotype/Repository.html) y [DTO](https://en.wikipedia.org/wiki/Data_transfer_object) 

### [login](src/main/java/com/dinbog/api/login)
Operaciones sobre la authenticacion de los usuarios
### [profile](src/main/java/com/dinbog/api/profile)
Operaciones sobre los profiles
### [user](src/main/java/com/dinbog/api/user)
Operaciones sobre los users y sus dependencias principales
* [user.email](src/main/java/com/dinbog/api/user/email): Operaciones sobre los email de los users
* [user.group](src/main/java/com/dinbog/api/user/groups): Operaciones sobre los grupos de users
* [user.status](src/main/java/com/dinbog/api/user/status): Operaciones sobre los status de los users


# [Archivos de soporte](src/main/resources) (src/main/resources)
En esta ruta se encuentran los archivos complementarios y/o de configuración de la API

* [application.properties](src/main/resources/application.properties): Archivo principal de propiedades


# [Fuentes de las Pruebas/Tests](src/test/java/com/dinbog/api) (src/test/java/com/dinbog/api)
En esta ruta se encuentran los fuentes java que ejecutan los test sobre las clases principales del sistema. Generan documentacion implementando [Spring REST Docs](https://spring.io/projects/spring-restdocs)

* [controller](src/test/java/com/dinbog/api/controller): Tests sobre los controllers (ayudan a generar la documentacion de cada endpoint)



***************************************************************************************
# DOCUMENTACION OFICIAL 

## Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/maven-plugin/)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Rest Repositories](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/htmlsingle/#howto-use-exposing-spring-data-repositories-rest-endpoint)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Data JDBC](https://docs.spring.io/spring-data/jdbc/docs/current/reference/html/)
* [Spring cache abstraction](https://docs.spring.io/spring-boot/docs/2.2.4.RELEASE/reference/htmlsingle/#boot-features-caching)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing JPA Data with REST](https://spring.io/guides/gs/accessing-data-rest/)
* [Accessing Neo4j Data with REST](https://spring.io/guides/gs/accessing-neo4j-data-rest/)
* [Accessing MongoDB Data with REST](https://spring.io/guides/gs/accessing-mongodb-data-rest/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Using Spring Data JDBC](https://github.com/spring-projects/spring-data-examples/tree/master/jdbc/basics)
* [Caching Data with Spring](https://spring.io/guides/gs/caching/)
