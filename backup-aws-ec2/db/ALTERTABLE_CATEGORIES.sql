ALTER TABLE "dinbog"."profile_category_type"
  ADD COLUMN "attachment_id" int4,
  ADD FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;