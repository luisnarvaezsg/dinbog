
/************************** PROFILE LIKE NOTIFICATION GENERAL PROCEDURE ***************************/

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_like"() RETURNS TRIGGER AS $create_notification_profile_like$
    DECLARE		
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			to_count_like INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.owner_profile_id;
				profile_id = OLD.profile_id;
			
				FOR users_records IN (SELECT id, user_id, count_like FROM "dinbog"."profile" WHERE id IN (OLD.owner_profile_id, OLD.profile_id)) LOOP
					IF (users_records.id = OLD.profile_id)
						THEN 
							profile_id = users_records.id;
							to_count_like = users_records.count_like;
						END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_like = (CASE WHEN to_count_like > 0 THEN to_count_like - 1 ELSE 0 END) WHERE id = profile_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, user_id, count_like FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE 
							p_user_id = users_records.user_id;
							to_count_like = users_records.count_like;
						END IF;
						countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_like = COALESCE(to_count_like,0) + 1 WHERE id = profile_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
			
				--RAISE NOTICE 'Value: %', user_owner_id;
				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				
				RETURN NEW;
			
			END IF;
		END;
$create_notification_profile_like$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "profile_like_action" ON "dinbog"."profile_like";
CREATE TRIGGER profile_like_action AFTER INSERT OR DELETE ON "dinbog"."profile_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_profile_like();


/************************** PROFILE FOLLOW NOTIFICATION GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_follow"() RETURNS TRIGGER AS $create_notification_profile_follow$
    DECLARE		
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_follow INTEGER;
			r_count_followers INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.owner_profile_id;
				profile_id = OLD.profile_id;
				FOR users_records IN (SELECT id, user_id, count_follow, count_followers FROM "dinbog"."profile" WHERE id IN (OLD.owner_profile_id, OLD.profile_id)) LOOP
					
					IF (users_records.id = OLD.owner_profile_id)
						THEN 
							owner_profile_id = users_records.id;
							r_count_follow = users_records.count_follow;
						ELSE 
							profile_id = users_records.id;
							r_count_followers = users_records.count_followers;
					END IF;
				
				END LOOP;
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_follow = (CASE WHEN r_count_follow > 0 THEN r_count_follow - 1 ELSE 0 END) WHERE id = owner_profile_id;
				UPDATE "dinbog"."profile" SET count_followers = (CASE WHEN r_count_followers > 0 THEN r_count_followers - 1 ELSE 0 END) WHERE id = profile_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, user_id, count_follow, count_followers FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
							r_count_follow = users_records.count_follow;
						ELSE 
							p_user_id = users_records.user_id;
							r_count_followers = users_records.count_followers;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_follow = COALESCE(r_count_follow,0) + 1 WHERE id = owner_profile_id;
				UPDATE "dinbog"."profile" SET count_followers = COALESCE(r_count_followers,0) + 1  WHERE id = profile_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
			
				--RAISE NOTICE 'Value: %', user_owner_id;
				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger profile_follow NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			
			END IF;
		END;
$create_notification_profile_follow$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "profile_follow_action" ON "dinbog"."profile_follow";
CREATE TRIGGER profile_follow_action AFTER INSERT OR DELETE ON "dinbog"."profile_follow"
FOR EACH ROW EXECUTE PROCEDURE create_notification_profile_follow();

/************************** PROFILE CONNECTION NOTIFICATION GENERAL PROCEDURE ***************************/ 


CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_connection"() RETURNS TRIGGER AS $create_notification_profile_connection$
    DECLARE		
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_owner_profile_connect INTEGER;
			r_count_profile_connect INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.owner_profile_id;
				profile_id = OLD.profile_id;
				
				FOR users_records IN (SELECT id, user_id, count_conections FROM "dinbog"."profile" WHERE id IN (OLD.owner_profile_id, OLD.profile_id)) LOOP
					
					IF (users_records.id = OLD.owner_profile_id)
						THEN 
							owner_profile_id = users_records.id;
							r_count_owner_profile_connect = users_records.count_conections;
						ELSE 
							profile_id = users_records.id;
							r_count_profile_connect = users_records.count_conections;
					END IF;
				
				END LOOP;	
				IF (OLD.profile_request_status_id = 2) THEN
					/*CONTADOR*/
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_owner_profile_connect > 0 THEN r_count_owner_profile_connect - 1 ELSE 0 END) WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_profile_connect > 0 THEN r_count_profile_connect - 1 ELSE 0 END) WHERE id = profile_id;
				END IF;
					
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				
				FOR users_records IN (SELECT id, user_id, count_conections FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
							r_count_owner_profile_connect = users_records.count_conections;
						ELSE 
							p_user_id = users_records.user_id;
							r_count_profile_connect = users_records.count_conections;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
			
			
				IF (NEW.profile_request_status_id = 2) THEN
					/*CONTADOR*/
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_owner_profile_connect,0) + 1 WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_profile_connect,0) + 1  WHERE id = profile_id;
				END IF;
			
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				--RAISE NOTICE 'Value: %', user_owner_id;
				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(
					NEW.id, owner_profile_id, profile_id, CONCAT(
					'trigger CONNECTION NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:'
					,'USER_OD-> USER_TO VALUE:(',p_user_id,')',table_name
					)
				) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				
				RETURN NEW;
			
			END IF;
			
			IF(TG_OP = 'UPDATE') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				
				FOR users_records IN (SELECT id, user_id, count_conections FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
							r_count_owner_profile_connect = users_records.count_conections;
						ELSE 
							p_user_id = users_records.user_id;
							r_count_profile_connect = users_records.count_conections;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				IF (OLD.profile_request_status_id != NEW.profile_request_status_id AND NEW.profile_request_status_id = 2) THEN
					table_name = 'profile_request_connection_accepted';
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_owner_profile_connect,0) + 1 WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_profile_connect,0) + 1  WHERE id = profile_id;
				END IF;
				
				/*CONTADOR*/
				IF (OLD.profile_request_status_id != NEW.profile_request_status_id AND NEW.profile_request_status_id <> 2) THEN
					
					IF(NEW.profile_request_status_id = 3) THEN
						table_name = 'profile_request_connection_denied';
					ELSIF (NEW.profile_request_status_id = 5) THEN
						table_name = 'profile_request_connection_cancelled';
					END IF;
					
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_owner_profile_connect > 0 THEN r_count_owner_profile_connect - 1 ELSE 0 END) WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_profile_connect > 0 THEN r_count_profile_connect - 1 ELSE 0 END) WHERE id = profile_id;
				END IF;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				--Se Invierten los parametros de owner a profile y profile a owner
				INSERT INTO "dinbog"."profile_notification" (object_id, to_profile_id, owner_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger PROFILE_CONNECTION MODIFIED status NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
				
				--Se Invierten los parametros de owner a profile y profile a owner
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_id, p_user_owner_id, notification_id, NEW.created);
				RETURN NEW;
			
			END IF;
			
		END;
$create_notification_profile_connection$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS "profile_connection_action" ON "dinbog"."profile_request_connection";
CREATE TRIGGER profile_connection_action AFTER INSERT OR DELETE OR UPDATE ON "dinbog"."profile_request_connection"
FOR EACH ROW EXECUTE PROCEDURE create_notification_profile_connection();


/************************** POST COUNT NOTIFICATION GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post"() RETURNS TRIGGER AS $create_notification_post$
    DECLARE		
			users_records RECORD;
			profile_id INTEGER;
			to_count_post INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
                profile_id = OLD.profile_id;
				FOR users_records IN (SELECT id, user_id, count_post FROM "dinbog"."profile" WHERE id IN (OLD.profile_id)) LOOP
							profile_id = users_records.id;
							to_count_post = users_records.count_post;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_post = (CASE WHEN to_count_post > 0 THEN to_count_post - 1 ELSE 0 END) WHERE id = profile_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, user_id, count_post FROM "dinbog"."profile" WHERE id IN (profile_id)) LOOP
							profile_id = users_records.id;
							to_count_post = users_records.count_post;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_post = COALESCE(to_count_post,0) + 1 WHERE id = profile_id;
				RETURN NEW;
			
			END IF;
		END;
$create_notification_post$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "post_action" ON "dinbog"."post";
CREATE TRIGGER post_action AFTER INSERT OR DELETE ON "dinbog"."post"
FOR EACH ROW EXECUTE PROCEDURE create_notification_post();

/************************** NOTIFICATION ALBUM LIKE GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_like"() RETURNS TRIGGER AS $album_like_action$
       DECLARE
			table_target VARCHAR := 'album_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			album_id INTEGER;
			r_count_like INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				FOR users_records IN (SELECT id, count_like FROM "dinbog"."album" WHERE id IN (OLD.album_id)) LOOP
							r_count_like = users_records.count_like;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.album_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, count_like FROM "dinbog"."album" WHERE id IN (NEW.album_id)) LOOP
							r_count_like = users_records.count_like;
				END LOOP;
			
		
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(a.profile_id,',',al.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."album_like" as al
                        INNER JOIN "dinbog"."album" a ON a.id = al.album_id 
                        WHERE al.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.album_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger album_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$album_like_action$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_album_like" ON "dinbog"."album_like";
CREATE TRIGGER create_notification_album_like AFTER INSERT OR DELETE ON "dinbog"."album_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_album_like();

/************************** NOTIFICATION ALBUM COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_comment"() RETURNS TRIGGER AS $album_comment_action$
       DECLARE
			table_target VARCHAR := 'album_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
		
			r_count_comment INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				FOR users_records IN (SELECT id, count_comment FROM "dinbog"."album" WHERE id IN (OLD.album_id)) LOOP
							r_count_comment = users_records.count_comment;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_comment = (CASE WHEN r_count_comment > 0 THEN r_count_comment - 1 ELSE 0 END) WHERE id = OLD.album_id;
				RETURN OLD;
				
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, count_comment FROM "dinbog"."album" WHERE id IN (NEW.album_id)) LOOP
							r_count_comment = users_records.count_comment;
				END LOOP;
			

				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."album_comment" as pl
                        INNER JOIN "dinbog"."album" p ON p.id = pl.album_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
		
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_comment = COALESCE(r_count_comment,0) + 1 WHERE id = NEW.album_id;
					
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger album_comment NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
		
				RETURN NEW;
			END IF;
		END;
$album_comment_action$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_album_comment" ON "dinbog"."album_comment";
CREATE TRIGGER create_notification_album_comment AFTER INSERT OR DELETE ON "dinbog"."album_comment"
FOR EACH ROW EXECUTE PROCEDURE create_notification_album_comment();

/************************** NOTIFICATION POST LIKE GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_like"() RETURNS TRIGGER AS $post_like_action$
    DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_like INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN

				FOR users_records IN ( SELECT id, count_like FROM "dinbog"."post" WHERE id = (OLD.post_id)) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
					ELSE
						r_count_like = 0;
					END IF;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_like = (CASE WHEN r_count_like > 0 THEN COALESCE(r_count_like) - 1 ELSE 0 END) WHERE id = OLD.post_id;
				
				RETURN OLD;
				
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				FOR users_records IN ( SELECT id, count_like FROM "dinbog"."post" WHERE id = (NEW.post_id)) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
					ELSE
						r_count_like = 0;
					END IF;
				END LOOP;
				
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."post_like" as pl
                        INNER JOIN "dinbog"."post" p ON p.id = pl.post_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.post_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger post_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);		
				RETURN NEW;
			END IF;
		END;
$post_like_action$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_post_like" ON "dinbog"."post_like";
CREATE TRIGGER create_notification_post_like AFTER INSERT OR DELETE ON "dinbog"."post_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_post_like();

/************************** NOTIFICATION POST MENTION GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_mention"() RETURNS TRIGGER AS $post_mention_action$
    DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER := NEW.profile_id;
			
		BEGIN
			
			IF(TG_OP = 'INSERT') THEN
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."post_mention" as pl
                        INNER JOIN "dinbog"."post" p ON p.id = pl.post_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id = users_records.id;
							p_user_owner_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;

			END IF;
		END;
$post_mention_action$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_post_mention" ON "dinbog"."post_mention";
CREATE TRIGGER create_notification_post_mention AFTER INSERT ON "dinbog"."post_mention"
FOR EACH ROW EXECUTE PROCEDURE create_notification_post_mention();


/************************** NOTIFICATION POST COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_comment"() RETURNS TRIGGER AS $post_comment_action$
    DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_comment INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN

				FOR users_records IN ( SELECT id, count_comment FROM "dinbog"."post" WHERE id = (OLD.post_id)) LOOP
					IF(users_records.count_comment NOTNULL) THEN
						r_count_comment = users_records.count_comment;
					ELSE
						r_count_comment = 0;
					END IF;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_comment = (CASE WHEN r_count_comment > 0 THEN r_count_comment - 1 ELSE 0 END) WHERE id = OLD.post_id;
				
				RETURN OLD;
				
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.profile_id;
				FOR users_records IN ( SELECT id, count_comment FROM "dinbog"."post" WHERE id = (NEW.post_id)) LOOP
					IF(users_records.count_comment NOTNULL) THEN
						r_count_comment = users_records.count_comment;
					ELSE
						r_count_comment = 0;
					END IF;
				END LOOP;
				
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."post_comment" as pl
                        INNER JOIN "dinbog"."post" p ON p.id = pl.post_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_comment = COALESCE(r_count_comment,0) + 1 WHERE id = NEW.post_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger post_comment NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$post_comment_action$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_post_comment" ON "dinbog"."post_comment";
CREATE TRIGGER create_notification_post_comment AFTER INSERT OR DELETE ON "dinbog"."post_comment"
FOR EACH ROW EXECUTE PROCEDURE create_notification_post_comment();

/************************** NOTIFICATION EVENT INVITATION GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_event_invitation"() RETURNS TRIGGER AS $event_invitation_action$
    DECLARE
			table_target VARCHAR := 'event_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
							
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER := NEW.profile_id;
			
		BEGIN
			
			IF(TG_OP = 'INSERT') THEN
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',e.owner_profile_id),',')::INTEGER
                        FROM
                            "dinbog"."event_invitation" as pl
                        INNER JOIN "dinbog"."event" e ON e.id = pl.event_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id = users_records.id;
							p_user_owner_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger NOTIFICATION NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;

			END IF;
		END;
$event_invitation_action$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_event_invitation" ON "dinbog"."event_invitation";
CREATE TRIGGER create_notification_event_invitation AFTER INSERT ON "dinbog"."event_invitation"
FOR EACH ROW EXECUTE PROCEDURE create_notification_event_invitation();

/************************** EVEN PARTICIPANT NOTIFICATION GENERAL PROCEDURE ***************************/

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_event_participant"() RETURNS TRIGGER AS $create_notification_event_participant$
    DECLARE		
			table_target VARCHAR := 'event_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			connections_profile RECORD;
			connection_profile_id INTEGER;
			
			event_id INTEGER;
			status_participant INTEGER;
			r_count_participant INTEGER;
    BEGIN
        IF(TG_OP = 'DELETE') THEN
            event_id = OLD.event_id;

            SELECT INTO
                event_id, r_count_participant
                e.id, e.count_participant
            FROM "dinbog"."event" as e
            WHERE e.id = event_id;
						
						/*CONTADOR*/
            UPDATE "dinbog"."event" SET count_participant = (CASE WHEN r_count_participant > 0 THEN r_count_participant - 1 ELSE 0 END) WHERE id = event_id;

            RETURN OLD;
        END IF;
        
        IF(TG_OP = 'UPDATE') THEN
            event_id = NEW.event_id;
            owner_profile_id = NEW.profile_id;

            SELECT INTO 
                r_count_participant
                count_participant 
            FROM "dinbog"."event" WHERE id = (event_id);

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (
                    SELECT
                        regexp_split_to_table(CONCAT(event_p.profile_id,',',a.owner_profile_id),',')::INTEGER
                    FROM
                        "dinbog"."event_participant" as event_p
                    INNER JOIN "dinbog"."event" a ON a.id = event_p.event_id 
                    WHERE event_p.id = (NEW.id)
                )
            ) LOOP
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id = users_records.user_id;
                    ELSE 
                        profile_id = users_records.user_id;
                        p_user_id = users_records.user_id;
                END IF;
							countUsers = countUsers + 1;
						END LOOP;
						
						          
						IF (NEW.event_participant_status_id = 9 OR NEW.event_participant_status_id = 13) THEN
								IF (NEW.event_participant_status_id = 9) THEN
									table_name = 'event_participant_acepted';
									/*CONTADOR*/
									UPDATE "dinbog"."event" SET count_participant = COALESCE(r_count_participant,0) + 1 WHERE id = event_id; 
									
									IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
										RETURN NEW;
									END IF;
									
									--Se Invierten los parametros de owner a profile y profile a owner
									INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
									VALUES(NEW.id, profile_id, owner_profile_id, CONCAT('trigger EVENT_PARTICIPANT MODIFIED ACCEPTED status NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
									PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_id, p_user_owner_id, notification_id, NEW.created);
								
								ELSE
									
									table_name = 'event_participant_confirmed_attendance';
									INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger EVENT_PARTICIPANT MODIFIED ATTEMPT NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
									PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
								END IF;
   
            END IF;

            RETURN NEW;
        END IF;

        IF(TG_OP = 'INSERT') THEN
            event_id = NEW.event_id;
			owner_profile_id = NEW.profile_id;
            SELECT INTO 
                r_count_participant
                count_participant 
            FROM "dinbog"."event" WHERE id = (event_id);

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (
                    SELECT
                        regexp_split_to_table(CONCAT(event_p.profile_id,',',a.owner_profile_id),',')::INTEGER
                    FROM "dinbog"."event_participant" as event_p
                    INNER JOIN "dinbog"."event" a ON a.id = event_p.event_id 
                    WHERE event_p.id = (NEW.id)
                )
            ) LOOP
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id = users_records.user_id;
                    ELSE 
                        profile_id = users_records.user_id;
                        p_user_id = users_records.user_id;
                END IF;
							countUsers = countUsers + 1;
						END LOOP;
						
						/*CONTADOR*/
						UPDATE "dinbog"."event" SET count_participant = (CASE WHEN r_count_participant > 0 THEN COALESCE(r_count_participant,0) + 1 ELSE 0 END) 
						WHERE id = event_id;

						IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
							RETURN NEW;
						END IF;
						
            INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger EVENT_PARTICIPANT MODIFIED status NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
            PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
						
						
						connection_profile_id = owner_profile_id;
						FOR connections_profile IN 
						(SELECT
								p.user_id,
								regexp_split_to_table(CONCAT(pc.profile_id,',',pc.owner_profile_id),',')::INTEGER as profile_id
						FROM "dinbog"."profile_request_connection" as pc
						INNER JOIN "dinbog"."profile" p ON p.id = pc.profile_id 
						WHERE pc.owner_profile_id = connection_profile_id 
						OR pc.profile_id = connection_profile_id 
						AND pc.type_connection_id = 2 AND P.profile_type_id = 2 LIMIT 1) LOOP
						
							IF(connections_profile.user_id IS NOT NULL) THEN 
								table_name = 'event_participant_profile_connection';
								p_user_id = connections_profile.user_id;
								PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
							END IF;
						
						END LOOP;
						
            RETURN NEW;
        END IF;
        
	END;
$create_notification_event_participant$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS "create_notification_event_participant" ON "dinbog"."event_participant";
CREATE TRIGGER create_notification_event_participant AFTER INSERT OR UPDATE OR DELETE ON "dinbog"."event_participant"
FOR EACH ROW EXECUTE PROCEDURE create_notification_event_participant();

/************************** EVENT CREATE AND UPDATE STATUS NOTIFICATION GENERAL PROCEDURE ***************************/

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_event_status"() RETURNS TRIGGER AS $create_notification_event_status$
    DECLARE		
			table_target VARCHAR := 'event_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			
			p_user_owner_id INTEGER;
			owner_profile_id INTEGER;
			
			connections_profile RECORD;
			connection_profile_id INTEGER;
			
			event_id INTEGER;
			event_status_id INTEGER;
    BEGIN

        IF(TG_OP = 'UPDATE') THEN
            event_id = NEW.id;
						event_status_id = NEW.event_status_id;
						owner_profile_id = NEW.owner_profile_id;

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (NEW.owner_profile_id)
            ) LOOP
							p_user_owner_id = users_records.user_id;
						END LOOP;
						
								          
						IF (event_status_id = 3 OR event_status_id = 4) THEN
								IF (event_status_id = 3) THEN
									table_name = 'event_approved';
								ELSIF (event_status_id = 4) THEN
									table_name = 'event_denied';
								END IF;
								
								INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
								VALUES(NEW.id, owner_profile_id, owner_profile_id, CONCAT('trigger EVENT CHANGE STATUS-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
								PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_owner_id, notification_id, NEW.created);					
            END IF;

            RETURN NEW;
        END IF;

        IF(TG_OP = 'INSERT') THEN
            event_id = NEW.id;
						event_status_id = NEW.event_status_id;
						owner_profile_id = NEW.owner_profile_id;
						table_name = 'event_wait';
						
            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (NEW.owner_profile_id)
            ) LOOP
							p_user_owner_id = users_records.user_id;
						END LOOP;
						
            INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, owner_profile_id, CONCAT('trigger EVENT CHANGE STATUS-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
						PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_owner_id, notification_id, NEW.created);
						
            RETURN NEW;
        END IF;
        
	END;
$create_notification_event_status$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS "create_notification_event_status" ON "dinbog"."event";
CREATE TRIGGER create_notification_event_status AFTER INSERT OR UPDATE ON "dinbog"."event"
FOR EACH ROW EXECUTE PROCEDURE create_notification_event_status();

/************************** NOTIFICATION ATTACHMENT LIKE GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_like"() RETURNS TRIGGER AS $attachment_like_action$
    DECLARE
			table_target VARCHAR := 'attachment_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			r_attachment_id INTEGER;
			owner_profile_id INTEGER;
			profile_id INTEGER;
			r_count_like INTEGER;

		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.profile_id;
				FOR users_records IN ( SELECT id, count_like FROM "dinbog"."attachment" WHERE id = (OLD.attachment_id)) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
					ELSE
						r_count_like = 0;
					END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.attachment_id;
				RETURN OLD;
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.profile_id;
				r_attachment_id = NEW.attachment_id;
				FOR users_records IN ( 
                    
                    SELECT 
                        al.*, a.count_like,
                        COALESCE(album.profile_id,post.profile_id,event.profile_id,profile_t.profile_id) as owner_profile_id,
                        COALESCE(album.type,post.type,event.type,profile_t.type) as type_element
                    FROM "dinbog"."attachment_like" al
                    LEFT JOIN (
                    SELECT al_at.attachment_id, al.profile_id, 'album' as type FROM "dinbog"."album" al
                    INNER JOIN "dinbog"."album_attachment" al_at ON al_at.album_id = al.id
                    WHERE al_at.attachment_id = r_attachment_id) album ON album.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT post_at.attachment_id, pt.profile_id, 'post' as type FROM "dinbog"."post" pt
                    INNER JOIN "dinbog"."post_attachment" post_at ON post_at.post_id = pt.id
                    WHERE post_at.attachment_id = r_attachment_id) post ON post.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT eve_at.attachment_id, eve.owner_profile_id as profile_id, 'event' as type FROM "dinbog"."event" eve
                    INNER JOIN "dinbog"."event_attachment" eve_at ON eve_at.event_id = eve.id
                    WHERE eve_at.attachment_id = r_attachment_id) event ON event.attachment_id = al.attachment_id
										LEFT JOIN (
                    SELECT profile_at.attachment_id, profile_at.profile_id, 'profile' as type FROM "dinbog"."profile_attachment" profile_at
                    WHERE profile_at.attachment_id = r_attachment_id) profile_t ON profile_t.attachment_id = al.attachment_id
                    INNER JOIN "dinbog"."attachment" a ON a.id = al.attachment_id
                    WHERE al.id = NEW.id

                ) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
						profile_id = users_records.owner_profile_id;
					END IF;
				END LOOP;

				FOR users_records IN ( SELECT id, user_id FROM "dinbog"."profile" WHERE id IN (owner_profile_id, profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = r_attachment_id;
					
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
			
				INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);				

				RETURN NEW;
			END IF;
		END;
$attachment_like_action$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS "create_notification_attachment_like" ON "dinbog"."attachment_like";
CREATE TRIGGER create_notification_attachment_like AFTER INSERT OR DELETE ON "dinbog"."attachment_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_attachment_like();

/************************** NOTIFICATION ATTACHMENT COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_comment"() RETURNS TRIGGER AS $attachment_comment_action$
    DECLARE
			table_target VARCHAR := 'attachment_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
      r_attachment_id INTEGER;
			owner_profile_id INTEGER;
			profile_id INTEGER;
			r_count_comments INTEGER;

		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.profile_id;
				FOR users_records IN ( SELECT id, count_comments FROM "dinbog"."attachment" WHERE id = (OLD.attachment_id)) LOOP
					IF(users_records.count_comments NOTNULL) THEN
						r_count_comments = users_records.count_comments;
					ELSE
						r_count_comments = 0;
					END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_comments = (CASE WHEN r_count_comments > 0 THEN r_count_comments - 1 ELSE 0 END) WHERE id = OLD.attachment_id;
				RETURN OLD;
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.profile_id;
				r_attachment_id = NEW.attachment_id;
				FOR users_records IN ( 
                    
                    SELECT 
                        al.*, a.count_comments,
                        COALESCE(album.profile_id,post.profile_id,event.profile_id,profile_t.profile_id) as owner_profile_id,
                        COALESCE(album.type,post.type,event.type,profile_t.type) as type_element
                    FROM "dinbog"."attachment_comment" al
                    LEFT JOIN (
                    SELECT al_at.attachment_id, al.profile_id, 'album' as type FROM "dinbog"."album" al
                    INNER JOIN "dinbog"."album_attachment" al_at ON al_at.album_id = al.id
                    WHERE al_at.attachment_id = r_attachment_id) album ON album.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT post_at.attachment_id, pt.profile_id, 'post' as type FROM "dinbog"."post" pt
                    INNER JOIN "dinbog"."post_attachment" post_at ON post_at.post_id = pt.id
                    WHERE post_at.attachment_id = r_attachment_id) post ON post.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT eve_at.attachment_id, eve.owner_profile_id as profile_id, 'event' as type FROM "dinbog"."event" eve
                    INNER JOIN "dinbog"."event_attachment" eve_at ON eve_at.event_id = eve.id
                    WHERE eve_at.attachment_id = r_attachment_id) event ON event.attachment_id = al.attachment_id

					LEFT JOIN (
                    SELECT profile_at.attachment_id, profile_at.profile_id, 'profile' as type FROM "dinbog"."profile_attachment" profile_at
                    WHERE profile_at.attachment_id = r_attachment_id) profile_t ON profile_t.attachment_id = al.attachment_id

                    INNER JOIN "dinbog"."attachment" a ON a.id = al.attachment_id
                    WHERE al.id = NEW.id

                ) LOOP
					IF(users_records.count_comments NOTNULL) THEN
						r_count_comments = users_records.count_comments;
						profile_id = users_records.owner_profile_id;
					END IF;
				END LOOP;

				FOR users_records IN ( SELECT id, user_id FROM "dinbog"."profile" WHERE id IN (owner_profile_id, profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_comments = COALESCE(r_count_comments,0) + 1 WHERE id = r_attachment_id;

				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);				
				
				RETURN NEW;
			END IF;
		END;
$attachment_comment_action$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS "create_notification_attachment_comment" ON "dinbog"."attachment_comment";
CREATE TRIGGER create_notification_attachment_comment AFTER INSERT OR DELETE ON "dinbog"."attachment_comment"
FOR EACH ROW EXECUTE PROCEDURE create_notification_attachment_comment();

/************************** NOTIFICATION POLL VOTE GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_poll_vote"() RETURNS TRIGGER AS $notification_poll_vote_action$
    DECLARE
			table_target VARCHAR := 'poll_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER := NEW.owner_profile_id;
			profile_id INTEGER;
			r_count_vote INTEGER;

		BEGIN
			IF(TG_OP = 'DELETE') THEN

				FOR users_records IN ( SELECT id, vote_count FROM "dinbog"."poll_participant" WHERE id = (OLD.poll_participant_id)) LOOP
					IF(users_records.vote_count NOTNULL) THEN
						r_count_vote = users_records.vote_count;
					ELSE
						r_count_vote = 0;
					END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."poll_participant" SET vote_count = (CASE WHEN r_count_vote > 0 THEN r_count_vote - 1 ELSE 0 END) WHERE id = OLD.poll_participant_id;
				RETURN OLD;
			END IF;
		
			IF(TG_OP = 'INSERT') THEN

				FOR users_records IN ( SELECT id, vote_count FROM "dinbog"."poll_participant" WHERE id = (NEW.poll_participant_id)) LOOP
					IF(users_records.vote_count NOTNULL) THEN
						r_count_vote = users_records.vote_count;
					ELSE
						r_count_vote = 0;
					END IF;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."poll_participant" SET vote_count = COALESCE(r_count_vote,0) + 1 WHERE id = NEW.poll_participant_id;
				RETURN NEW;
			END IF;
		END;
$notification_poll_vote_action$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS "create_notification_poll_vote" ON "dinbog"."poll_vote";
CREATE TRIGGER create_notification_poll_vote AFTER INSERT OR DELETE ON "dinbog"."poll_vote"
FOR EACH ROW EXECUTE PROCEDURE create_notification_poll_vote();


/************************** NOTIFICATION PROFILE RECOMMENDATION PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_recommendation"() RETURNS TRIGGER AS $create_notification_profile_recommendation$
    DECLARE
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				
				FOR users_records IN (SELECT id, user_id FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							p_user_owner_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger Reccommendation-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
			
		END;
$create_notification_profile_recommendation$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_profile_recommendation" ON "dinbog"."profile_recommendation";
CREATE TRIGGER create_notification_profile_recommendation AFTER INSERT ON "dinbog"."profile_recommendation"
FOR EACH ROW EXECUTE PROCEDURE create_notification_profile_recommendation();

/************************** NOTIFICATION ALBUM LIKE COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_comment_like"() RETURNS TRIGGER AS $create_notification_album_comment_like$
    DECLARE			
            table_target VARCHAR := 'album_notification';
            table_name VARCHAR := TG_TABLE_NAME;
            notification_id INTEGER;
            users_records RECORD;
            countUsers INTEGER := 0;

            p_user_owner_id INTEGER;
            p_user_id INTEGER;
            
            owner_profile_id INTEGER;
            profile_id INTEGER;
            r_count_like INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				SELECT INTO 
					r_count_like 
					count_like 
				FROM "dinbog"."album_comment" WHERE id IN (OLD.album_comment_id);
				
				/*CONTADOR*/
				UPDATE "dinbog"."album_comment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.album_comment_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				SELECT INTO 
					r_count_like 
					count_like 
				FROM "dinbog"."album_comment" WHERE id IN (NEW.album_comment_id);

				/*CONTADOR*/
				UPDATE "dinbog"."album_comment" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.album_comment_id;			
				
                FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile"
                    WHERE id IN (
                        SELECT regexp_split_to_table(CONCAT(cl.profile_id,',',c.profile_id),',')::INTEGER
                        FROM "dinbog"."album_comment_like" as cl
                        INNER JOIN "dinbog"."album_comment" c ON c.id = cl.album_comment_id
                        WHERE cl.id = (NEW.id)
                    )
                ) LOOP
                    --//Se Invierten los patrones de Envio
                    IF (users_records.id = owner_profile_id)
                        THEN 
                            p_user_owner_id 	= users_records.user_id;
                        ELSE
                            profile_id  = users_records.id;
                            p_user_id   = users_records.user_id;
                    END IF;
                    countUsers = countUsers + 1;
                END LOOP;
                
                IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
                    RETURN NEW;
                END IF;

                INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
                VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger LIKE IN ALBUM COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
                RETURNING id INTO notification_id;
                
                PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
                RETURN NEW;
			END IF;
		END;
$create_notification_album_comment_like$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_album_comment_like" ON "dinbog"."album_comment_like";
CREATE TRIGGER create_notification_album_comment_like AFTER INSERT OR DELETE ON "dinbog"."album_comment_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_album_comment_like();

/************************** NOTIFICATION ATTACHMENT LIKE COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_comment_like"() RETURNS TRIGGER AS $create_notification_attachment_comment_like$
    DECLARE
			table_target VARCHAR := 'attachment_notification';
            table_name VARCHAR := TG_TABLE_NAME;
            notification_id INTEGER;
            users_records RECORD;
            countUsers INTEGER := 0;

            p_user_owner_id INTEGER;
            p_user_id INTEGER;
            
            owner_profile_id INTEGER;
            profile_id INTEGER;
            r_count_like INTEGER;
    BEGIN
        IF(TG_OP = 'DELETE') THEN
            SELECT INTO 
                r_count_like
                count_like 
            FROM "dinbog"."attachment_comment" WHERE id = OLD.attachment_comment_id;
            
            /*CONTADOR*/
            UPDATE "dinbog"."attachment_comment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) 
            WHERE id = OLD.attachment_comment_id;
            
            RETURN OLD;
        END IF;
        
        IF(TG_OP = 'INSERT') THEN
            owner_profile_id = NEW.profile_id;
            SELECT INTO 
                r_count_like
                count_like 
            FROM "dinbog"."attachment_comment" WHERE id = NEW.attachment_comment_id;

            /*CONTADOR*/
            UPDATE "dinbog"."attachment_comment" SET count_like = COALESCE(r_count_like,0) + 1 
            WHERE id = NEW.attachment_comment_id;

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile"
                WHERE id IN (
                    SELECT regexp_split_to_table(CONCAT(cl.profile_id,',',c.profile_id),',')::INTEGER
                    FROM "dinbog"."attachment_comment_like" as cl
                    INNER JOIN "dinbog"."attachment_comment" c ON c.id = cl.attachment_comment_id
                    WHERE cl.id = (NEW.id)
                )
            ) LOOP
                --//Se Invierten los patrones de Envio
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id 	= users_records.user_id;
                    ELSE
                        profile_id  = users_records.id;
                        p_user_id   = users_records.user_id;
                END IF;
                countUsers = countUsers + 1;
            END LOOP;
            
            IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
                RETURN NEW;
            END IF;

            INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger LIKE IN ATTACHMENT COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
            RETURNING id INTO notification_id;
            
            PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
            RETURN NEW;
        END IF;
    END;
$create_notification_attachment_comment_like$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_attachment_comment_like" ON "dinbog"."attachment_comment_like";
CREATE TRIGGER create_notification_attachment_comment_like AFTER INSERT OR DELETE ON "dinbog"."attachment_comment_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_attachment_comment_like();

/************************** NOTIFICATION POST LIKE COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_comment_like"() RETURNS TRIGGER AS $create_notification_post_comment_like$
    DECLARE
        table_target VARCHAR := 'post_notification';
        table_name VARCHAR := TG_TABLE_NAME;
        notification_id INTEGER;
        users_records RECORD;
        countUsers INTEGER := 0;

        p_user_owner_id INTEGER;
        p_user_id INTEGER;
        
        owner_profile_id INTEGER;
        profile_id INTEGER;
        r_count_like INTEGER;
    BEGIN
        IF(TG_OP = 'DELETE') THEN
            SELECT INTO 
                r_count_like 
                count_like 
            FROM "dinbog"."post_comment" WHERE id IN (OLD.post_comment_id);
            
            /*CONTADOR*/
            UPDATE "dinbog"."post_comment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.post_comment_id;
            RETURN OLD;
        END IF;
        
        IF(TG_OP = 'INSERT') THEN
            owner_profile_id = NEW.profile_id;

            SELECT INTO 
                r_count_like 
                count_like 
            FROM "dinbog"."post_comment" WHERE id IN (NEW.post_comment_id);

            /*CONTADOR*/
            UPDATE "dinbog"."post_comment" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.post_comment_id;
            				
            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile"
                WHERE id IN (
                    SELECT regexp_split_to_table(CONCAT(cl.profile_id,',',c.profile_id),',')::INTEGER
                    FROM "dinbog"."post_comment_like" as cl
                    INNER JOIN "dinbog"."post_comment" c ON c.id = cl.post_comment_id
                    WHERE cl.id = (NEW.id)
                )
            ) LOOP
                --//Se Invierten los patrones de Envio
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id = users_records.user_id;
                    ELSE
                        profile_id  = users_records.id;
                        p_user_id   = users_records.user_id;
                END IF;
                countUsers = countUsers + 1;
            END LOOP;

            IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
                RETURN NEW;
            END IF;

            INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger LIKE IN POST COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
            RETURNING id INTO notification_id;
            
            PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
            RETURN NEW;
        END IF;
    END;
$create_notification_post_comment_like$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_post_comment_like" ON "dinbog"."post_comment_like";
CREATE TRIGGER create_notification_post_comment_like AFTER INSERT OR DELETE ON "dinbog"."post_comment_like"
FOR EACH ROW EXECUTE PROCEDURE create_notification_post_comment_like();

/************************** NOTIFICATION ALBUM MENTION COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_comment_mention"() RETURNS TRIGGER AS $create_notification_album_comment_mention$
     DECLARE
			table_target VARCHAR := 'album_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			IF(TG_OP = 'INSERT') THEN
				profile_id = NEW.profile_id;
				
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile"
                    WHERE id IN (
                        SELECT regexp_split_to_table(CONCAT(m.profile_id,',',c.profile_id),',')::INTEGER
                        FROM "dinbog"."album_comment_mention" as m
                        INNER JOIN "dinbog"."album_comment" c ON c.id = m.album_comment_id
                        WHERE m.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id 	= users_records.id;
							p_user_owner_id 	= users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION IN ALBUM COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$create_notification_album_comment_mention$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_album_comment_mention" ON "dinbog"."album_comment_mention";
CREATE TRIGGER create_notification_album_comment_mention AFTER INSERT ON "dinbog"."album_comment_mention"
FOR EACH ROW EXECUTE PROCEDURE create_notification_album_comment_mention();

/************************** NOTIFICATION POST MENTION COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_comment_mention"() RETURNS TRIGGER AS $create_notification_post_comment_mention$
     DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			IF(TG_OP = 'INSERT') THEN
				profile_id = NEW.profile_id;
				
				FOR users_records IN (
						SELECT id, user_id 
						FROM "dinbog"."profile"
						WHERE id IN (
								SELECT regexp_split_to_table(CONCAT(m.profile_id,',',c.profile_id),',')::INTEGER
								FROM "dinbog"."post_comment_mention" as m
								INNER JOIN "dinbog"."post_comment" c ON c.id = m.post_comment_id
								WHERE m.id = (NEW.id)
						)
        ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id 	= users_records.id;
							p_user_owner_id 	= users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
			
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				
				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION IN POST COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$create_notification_post_comment_mention$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_post_comment_mention" ON "dinbog"."post_comment_mention";
CREATE TRIGGER create_notification_post_comment_mention AFTER INSERT ON "dinbog"."post_comment_mention"
FOR EACH ROW EXECUTE PROCEDURE create_notification_post_comment_mention();

/************************** NOTIFICATION POST MENTION COMMENT GENERAL PROCEDURE ***************************/ 

CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_comment_mention"() RETURNS TRIGGER AS $create_notification_attachment_comment_mention$
     DECLARE
			table_target VARCHAR := 'attachment_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			IF(TG_OP = 'INSERT') THEN
				profile_id = NEW.profile_id;
				
				FOR users_records IN (
						SELECT id, user_id 
						FROM "dinbog"."profile"
						WHERE id IN (
								SELECT regexp_split_to_table(CONCAT(m.profile_id,',',c.profile_id),',')::INTEGER
								FROM "dinbog"."attachment_comment_mention" as m
								INNER JOIN "dinbog"."attachment_comment" c ON c.id = m.attachment_comment_id
								WHERE m.id = (NEW.id)
						)
        ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id 	= users_records.id;
							p_user_owner_id 	= users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
			
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				
				INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION IN ATTACHMENT COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$create_notification_attachment_comment_mention$ LANGUAGE plpgsql;

--//TRIGGER CREATE LIKE
DROP TRIGGER IF EXISTS "create_notification_attachment_comment_mention" ON "dinbog"."attachment_comment_mention";
CREATE TRIGGER create_notification_attachment_comment_mention AFTER INSERT ON "dinbog"."attachment_comment_mention"
FOR EACH ROW EXECUTE PROCEDURE create_notification_attachment_comment_mention();

/************************** NOTIFICATION GENERAL PROCEDURE ***************************/ 
			
DROP FUNCTION IF EXISTS notification_general(character varying, character varying, integer,integer,integer,timestamp with time zone);
CREATE OR REPLACE FUNCTION notification_general(
	field_notification_name VARCHAR
	,table_name_action VARCHAR
	,p_user_owner_id INTEGER
	,p_user_id INTEGER
	,notification_id INTEGER
	,created_notification_date TIMESTAMPTZ
) RETURNS void AS $$
DECLARE
	query_str VARCHAR;
	
	notification_action_record RECORD;
	action_id INTEGER;
	
	notification_field RECORD;
	notification_field_name VARCHAR:='';
	notification_message_code VARCHAR;
BEGIN
		FOR notification_field IN (SELECT column_name FROM  information_schema.columns 
		WHERE table_schema = 'dinbog' AND table_name = 'notification' AND column_name LIKE CONCAT('%',field_notification_name,'%id'))
		LOOP 
			notification_field_name = notification_field.column_name;
		END LOOP; 
		
		FOR notification_action_record IN (SELECT na.id, nm.code_message
			FROM "dinbog"."notification_action" as na 
			INNER JOIN "dinbog"."notification_type" nt ON nt.id = na.notification_type_id
			INNER JOIN "dinbog"."notification_message" nm ON na.id = nm.notification_action_id
		WHERE CONCAT(nt.value,'_',na.value) = table_name_action OR na.key = table_name_action LIMIT 1)
		LOOP
			action_id = notification_action_record.id;
			notification_message_code = notification_action_record.code_message;
		END LOOP;
		
		IF (notification_field_name <> '' AND action_id > 0 AND notification_message_code IS NOT NULL)
		THEN 
			query_str = CONCAT(
			'INSERT INTO "dinbog"."notification" 
			("owner_user_id", "to_user_id","notification_action_id","',notification_field_name,'","code_message", "date_notification") 
			VALUES (',p_user_owner_id,' 
			,',p_user_id,'
			,',action_id,'
			,',notification_id,'
			,',notification_message_code,'
			,''',created_notification_date,''');'
			);
			EXECUTE(query_str);
		ELSE
			ROLLBACK;
		END IF;
	
		EXCEPTION WHEN OTHERS THEN
		BEGIN
			RAISE NOTICE 'Error Rollback Exception On notification_general';
		END;
END;
$$ LANGUAGE plpgsql;
