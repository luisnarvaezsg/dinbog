ALTER TABLE "dinbog"."profile"
  ADD COLUMN "country_id" int4,
  ADD COLUMN "city_id" int4,
  ADD CONSTRAINT "fk_profile_country_1" FOREIGN KEY ("country_id") REFERENCES "dinbog"."country" ("id") ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT "fk_profile_city_1" FOREIGN KEY ("city_id") REFERENCES "dinbog"."city" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE "dinbog"."post"
  ALTER COLUMN "count_like" SET DEFAULT 0,
  ALTER COLUMN "count_comment" SET DEFAULT 0;

ALTER TABLE "dinbog"."post"
  ALTER COLUMN "count_like" TYPE int4 USING "count_like"::int4,
  ALTER COLUMN "count_comment" TYPE int4 USING "count_comment"::int4;

-- UNIQUE URL_NAME 
DROP INDEX "dinbog"."index_url_name";

CREATE UNIQUE INDEX "index_url_name" ON "dinbog"."profile" USING btree (
  "url_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);