DROP FUNCTION IF EXISTS "dinbog"."get_interactions_with_profile"(token_profile_id INTEGER, profile_visit_id INTEGER);
CREATE FUNCTION "dinbog"."get_interactions_with_profile"(IN token_profile_id INTEGER, IN profile_visit_id INTEGER) 
	RETURNS TABLE(
		profile_token INTEGER,
		profile_visit INTEGER,
		follow BOOLEAN,
		follower BOOLEAN,
		connection BOOLEAN
	) AS $$
	DECLARE
		/*
		* token_profile_id = id del perfil que hace la solicitud por su token
		*	profile_visit_id = id del perfil al cual se hace la solicitud o comparación de busqueda
		*/
		x1 RECORD;
	BEGIN
		FOR x1 IN (SELECT 
			token_profile_id AS profile_token,
			profile_visit_id AS profile_search,
			CASE 
				WHEN (pfollow.owner_profile_id = token_profile_id) THEN true ELSE FALSE END as follow,
			CASE 
				WHEN (pfollower.profile_id = token_profile_id) THEN true ELSE FALSE END as follower,
			CASE 
				WHEN (pc.id IS NOT NULL) THEN TRUE ELSE FALSE END AS connection
		FROM profile p
		LEFT JOIN profile_follow pfollow ON pfollow.owner_profile_id = p.id AND pfollow.owner_profile_id = token_profile_id AND pfollow.profile_id = profile_visit_id
		LEFT JOIN profile_follow pfollower ON pfollower.profile_id = p.id AND pfollower.owner_profile_id = profile_visit_id AND pfollower.profile_id = token_profile_id
		LEFT JOIN profile_request_connection pc ON 
			(pc.owner_profile_id = p.id OR pc.profile_id = p.id) 
			AND (
				(pc.owner_profile_id = token_profile_id AND pc.profile_id = 9) OR
				(pc.owner_profile_id = profile_visit_id AND pc.profile_id = token_profile_id)
			)
			AND (pc.profile_request_status_id = 2)
		WHERE p.id = token_profile_id) 
		LOOP
		profile_token := x1.profile_token;
		profile_visit := x1.profile_search;
		follow := x1.follow;
		follower := x1.follower;
		connection := x1.connection;
			RETURN NEXT;
		END LOOP;
END; 
$$ LANGUAGE plpgsql;

-- Implementación Funcion procedural para obtener el follow, following, connection de un usuario
-- SELECT * FROM get_interactions_with_profile(4,5)