/*
 Navicat Premium Data Transfer

 Source Server         : postgres_local
 Source Server Type    : PostgreSQL
 Source Server Version : 100012
 Source Host           : localhost:5432
 Source Catalog        : dinbog
 Source Schema         : dinbog

 Target Server Type    : PostgreSQL
 Target Server Version : 100012
 File Encoding         : 65001

 Date: 19/06/2020 17:33:34
*/


-- ----------------------------
-- Sequence structure for album_attachment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_attachment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_attachment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_comment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_comment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_comment_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_comment_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_comment_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_comment_mention_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_comment_mention_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_comment_mention_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_report_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_report_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_report_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_share_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_share_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for album_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."album_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."album_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_comment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_comment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_comment_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_comment_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_comment_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_comment_mention_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_comment_mention_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_comment_mention_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_report_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_report_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_report_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_share_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_share_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_tag_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_tag_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_tag_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for attachment_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."attachment_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."attachment_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auditory_action_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."auditory_action_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."auditory_action_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for auditory_action_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."auditory_action_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."auditory_action_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for city_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."city_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."city_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for connection_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."connection_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."connection_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for conversation_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."conversation_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."conversation_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for conversation_message_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."conversation_message_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."conversation_message_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for conversation_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."conversation_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."conversation_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for conversation_participant_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."conversation_participant_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."conversation_participant_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for conversation_participant_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."conversation_participant_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."conversation_participant_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for conversation_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."conversation_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."conversation_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for country_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."country_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."country_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_attachment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_attachment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_attachment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_filter_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_filter_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_filter_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_invitation_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_invitation_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_invitation_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_participant_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_participant_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_participant_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_participant_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_participant_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_participant_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_profile_category_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_profile_category_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_profile_category_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_report_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_report_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_report_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_role_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_share_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_share_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for event_view_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."event_view_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."event_view_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for field_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."field_category_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."field_category_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for field_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."field_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."field_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for gender_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."gender_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."gender_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."group_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for group_menu_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."group_menu_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."group_menu_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for language_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."language_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."language_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."log_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."log_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for membership_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."membership_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."membership_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 32767
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for menu_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."menu_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."menu_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 32767
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for notification_action_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."notification_action_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."notification_action_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for notification_message_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."notification_message_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."notification_message_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for notification_messages_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."notification_messages_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."notification_messages_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for notification_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."notification_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."notification_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for option_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."option_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."option_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for parameter_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."parameter_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."parameter_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for poll_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."poll_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."poll_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for poll_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."poll_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."poll_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for poll_participant_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."poll_participant_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."poll_participant_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for poll_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."poll_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."poll_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for poll_vote_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."poll_vote_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."poll_vote_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for poll_vote_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."poll_vote_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."poll_vote_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_attachment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_attachment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_attachment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_comment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_comment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_comment_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_comment_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_comment_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_comment_mention_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_comment_mention_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_comment_mention_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_hashtag_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_hashtag_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_hashtag_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_mention_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_mention_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_mention_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 32767
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_report_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_report_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_report_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."post_share_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."post_share_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_attachment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_attachment_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_attachment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_block_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_block_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_block_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_category_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_category_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_category_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_category_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_category_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_connection_rol_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_connection_rol_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_connection_rol_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_connection_type_rol_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_connection_type_rol_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_connection_type_rol_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_country_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_country_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_country_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_country_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_country_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_country_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 32767
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_detail_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_detail_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_detail_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_follow_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_follow_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_follow_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_job_history_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_job_history_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_job_history_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_language_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_language_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_language_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_like_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_like_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_like_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_recommendation_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_recommendation_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_recommendation_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_report_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_report_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_report_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_request_connection_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_request_connection_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_request_connection_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_request_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_request_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_request_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_share_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_share_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_share_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_skill_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_skill_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for profile_view_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."profile_view_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."profile_view_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for report_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."report_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."report_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for site_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."site_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."site_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for site_social_app_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."site_social_app_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."site_social_app_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."skill_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."skill_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for social_account_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."social_account_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."social_account_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for social_app_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."social_app_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."social_app_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for ticket_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."ticket_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."ticket_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for ticket_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."ticket_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."ticket_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_email_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."user_email_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."user_email_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."user_group_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."user_group_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."user_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."user_notification_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."user_notification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_status_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."user_status_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."user_status_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for vote_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "dinbog"."vote_type_id_seq" CASCADE;
CREATE SEQUENCE "dinbog"."vote_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album" CASCADE;
CREATE TABLE "dinbog"."album" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_id_seq'::regclass),
  "album_type_id" int4 NOT NULL,
  "name" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "profile_id" int4 NOT NULL,
  "is_private" bool NOT NULL DEFAULT true,
  "created" timestamptz(6) DEFAULT now(),
  "count_like" int4 DEFAULT 0,
  "count_comment" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of album
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (1, 4, 'test', 'test', 1, 't', '2020-04-22 17:53:44.408796-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (91, 4, 'Album Otro', 'Album Prueba', 1, 'f', '2020-05-03 19:04:44.612966-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (78, 4, 'Album 2 Fotos', 'Album 2 Prueba', 4, 'f', '2020-05-02 20:59:43.439013-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (81, 4, 'Album 3 Personas', 'Album 3 Prueba', 4, 'f', '2020-05-02 21:00:08.039969-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (19, 4, 'Album 1 Animales', 'Album 1 Prueba', 4, 't', '2020-04-22 17:53:44.408796-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (348, 4, 'Album @Test', 'Album @testing 1', 1, 'f', '2020-06-19 15:12:08.144-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (359, 4, 'Album @Test', 'Album @testing 1', 1, 'f', '2020-06-19 16:04:54.239-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (361, 4, 'Album @Test', 'Album @testing 1', 1, 'f', '2020-06-19 16:14:20.737-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (362, 4, 'Album @Test', 'Album @testing 1', 1, 'f', '2020-06-19 16:21:02.834-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (363, 4, 'Album @Test', 'Album @testing 1', 1186, 't', '2020-06-19 16:24:42.172-04', 0, 0);
INSERT INTO "dinbog"."album" ("id", "album_type_id", "name", "description", "profile_id", "is_private", "created", "count_like", "count_comment") VALUES (365, 4, 'Album @Test', 'Album @testing 1', 1, 'f', '2020-06-19 17:18:50.395-04', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for album_attachment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_attachment" CASCADE;
CREATE TABLE "dinbog"."album_attachment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_attachment_id_seq'::regclass),
  "album_id" int4 NOT NULL,
  "attachment_id" int4 NOT NULL,
  "is_cover" int2 DEFAULT 0
)
;

-- ----------------------------
-- Records of album_attachment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (216, 348, 436, 1);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (217, 348, 437, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (218, 348, 438, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (249, 359, 469, 1);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (250, 359, 470, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (251, 359, 471, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (252, 359, 472, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (256, 361, 477, 1);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (257, 361, 478, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (258, 361, 479, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (259, 361, 480, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (260, 362, 481, 1);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (261, 362, 482, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (262, 362, 483, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (263, 362, 484, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (264, 363, 485, 1);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (265, 363, 486, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (266, 363, 487, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (267, 363, 488, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (273, 365, 502, 1);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (274, 365, 503, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (275, 365, 504, 0);
INSERT INTO "dinbog"."album_attachment" ("id", "album_id", "attachment_id", "is_cover") VALUES (276, 365, 505, 0);
COMMIT;

-- ----------------------------
-- Table structure for album_comment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_comment" CASCADE;
CREATE TABLE "dinbog"."album_comment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_comment_id_seq'::regclass),
  "parent_id" int4,
  "album_id" int4,
  "profile_id" int4,
  "value" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now(),
  "count_like" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of album_comment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_comment" ("id", "parent_id", "album_id", "profile_id", "value", "created", "count_like") VALUES (25, NULL, 19, 1, 'hola miundo', '2020-04-22 17:54:04.024749-04', 0);
INSERT INTO "dinbog"."album_comment" ("id", "parent_id", "album_id", "profile_id", "value", "created", "count_like") VALUES (501, NULL, 19, 11, NULL, '2020-05-23 03:44:50.122993-04', 0);
COMMIT;

-- ----------------------------
-- Table structure for album_comment_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_comment_like" CASCADE;
CREATE TABLE "dinbog"."album_comment_like" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_comment_like_id_seq'::regclass),
  "album_comment_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Table structure for album_comment_mention
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_comment_mention" CASCADE;
CREATE TABLE "dinbog"."album_comment_mention" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_comment_mention_id_seq'::regclass),
  "album_comment_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Table structure for album_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_like" CASCADE;
CREATE TABLE "dinbog"."album_like" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_like_id_seq'::regclass),
  "album_id" int4,
  "profile_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of album_like
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_like" ("id", "album_id", "profile_id", "created") VALUES (85, 19, 15, '2020-05-07 00:51:36.33483-04');
INSERT INTO "dinbog"."album_like" ("id", "album_id", "profile_id", "created") VALUES (86, 78, 8, '2020-05-07 00:55:21.13394-04');
COMMIT;

-- ----------------------------
-- Table structure for album_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_notification" CASCADE;
CREATE TABLE "dinbog"."album_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4,
  "message" text COLLATE "pg_catalog"."default",
  "created" timestamptz(0) DEFAULT now()
)
;

-- ----------------------------
-- Records of album_notification
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (7, 85, 15, 4, NULL, 'trigger album_like NEW OWNER USER_OD-> user_owner VALUE:(15) TABLA INFO:album_like', '2020-05-07 00:51:36-04');
INSERT INTO "dinbog"."album_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (8, 86, 8, 4, NULL, 'trigger album_like NEW OWNER USER_OD-> user_owner VALUE:(8) TABLA INFO:album_like', '2020-05-07 00:55:21-04');
INSERT INTO "dinbog"."album_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (23, 501, 11, 4, NULL, 'trigger album_comment NEW OWNER USER_OD-> user_owner VALUE:(11) TABLA INFO:album_comment', '2020-05-23 03:44:50-04');
COMMIT;

-- ----------------------------
-- Table structure for album_report
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_report" CASCADE;
CREATE TABLE "dinbog"."album_report" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_report_id_seq'::regclass),
  "report_type_id" int4,
  "detail" varchar(1000) COLLATE "pg_catalog"."default",
  "owner_id" int4,
  "album_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of album_report
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_report" ("id", "report_type_id", "detail", "owner_id", "album_id", "created") VALUES (9, 1, 'Hola mundo Prueba Reporte 2', 5, 19, '2020-04-27 02:30:31.181-04');
INSERT INTO "dinbog"."album_report" ("id", "report_type_id", "detail", "owner_id", "album_id", "created") VALUES (8, 1, NULL, 4, 19, '2020-04-27 02:24:48.991-04');
COMMIT;

-- ----------------------------
-- Table structure for album_share
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_share" CASCADE;
CREATE TABLE "dinbog"."album_share" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_share_id_seq'::regclass),
  "album_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of album_share
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_share" ("id", "album_id", "owner_profile_id", "created") VALUES (7, 19, 4, '2020-04-26 23:51:48.136536-04');
INSERT INTO "dinbog"."album_share" ("id", "album_id", "owner_profile_id", "created") VALUES (8, 19, 5, '2020-04-27 01:19:32.433-04');
COMMIT;

-- ----------------------------
-- Table structure for album_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."album_type" CASCADE;
CREATE TABLE "dinbog"."album_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".album_type_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "is_active" bool NOT NULL DEFAULT false,
  "created" timestamptz(6) DEFAULT now(),
  "updated" timestamptz(6)
)
;

-- ----------------------------
-- Records of album_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."album_type" ("id", "name", "description", "is_active", "created", "updated") VALUES (1, 'book', 'Album de fotos', 't', '2020-04-17 10:07:53.981781-04', NULL);
INSERT INTO "dinbog"."album_type" ("id", "name", "description", "is_active", "created", "updated") VALUES (2, 'porfolio', 'porfolio', 't', '2020-04-17 10:08:32.39211-04', NULL);
INSERT INTO "dinbog"."album_type" ("id", "name", "description", "is_active", "created", "updated") VALUES (3, 'gallery', 'Gallery', 't', '2020-04-17 10:09:04.256888-04', NULL);
INSERT INTO "dinbog"."album_type" ("id", "name", "description", "is_active", "created", "updated") VALUES (4, 'album prueba', 'a;mub para prueba', 't', '2020-04-11 00:37:38-04', NULL);
COMMIT;

-- ----------------------------
-- Table structure for attachment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment" CASCADE;
CREATE TABLE "dinbog"."attachment" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".attachment_id_seq'::regclass),
  "attachment_type_id" int4 NOT NULL,
  "path" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "metadata" varchar(255) COLLATE "pg_catalog"."default",
  "extra" varchar(255) COLLATE "pg_catalog"."default",
  "purged" bool,
  "date_purged" timestamp(0),
  "created" timestamptz(6) DEFAULT now(),
  "count_attachment" int4,
  "count_like" int4 DEFAULT 0,
  "count_comments" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of attachment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (8, 2, '/test3', 'ffdsfsdfsd', 'FFF', NULL, NULL, '2020-04-22 09:46:10.862401-04', NULL, 0, 0);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (7, 1, '/test2', 'kjsdakjkjda', 'A', NULL, NULL, '2020-04-22 09:45:58.86613-04', NULL, 0, 0);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (188, 1, '/Attahcmment_post1', '/Attahcmment_post1', '/Attahcmment_post1', NULL, NULL, '2020-05-23 01:32:44.275841-04', NULL, 0, 0);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (189, 2, '/Attahcmment_post2', '/Attahcmment_post2', NULL, NULL, NULL, '2020-05-23 01:32:56.852291-04', NULL, 0, 0);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (9, 3, '/TES444', 'GSDSDFSD', 'ASD', NULL, NULL, '2020-04-22 10:20:51.720741-04', NULL, 0, 0);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (229, 1, 'http://52.207.106.153:8082/image/descarga-45ec1e0e-97f9-42f1-8737-5a0daedb2541.jpg', NULL, 'descarga-45ec1e0e-97f9-42f1-8737-5a0daedb2541.jpg', 'f', '2020-06-02 02:55:02', '2020-06-02 02:55:02.07-04', 6, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (311, 1, 'http://52.207.106.153:8082/image/15319187_10154681813036788_7291304822623569133_n.jpg', NULL, '15319187_10154681813036788_7291304822623569133_n.jpg', 'f', '2020-06-16 05:28:11', '2020-06-16 05:28:11.043-04', 7, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (312, 1, 'http://52.207.106.153:8082/image/15319187_10154681813036788_7291304822623569133_n.jpg', NULL, '15319187_10154681813036788_7291304822623569133_n.jpg', 'f', '2020-06-16 05:44:54', '2020-06-16 05:44:54.041-04', 8, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (313, 1, 'http://52.207.106.153:8082/image/15319187_10154681813036788_7291304822623569133_n.jpg', NULL, '15319187_10154681813036788_7291304822623569133_n.jpg', 'f', '2020-06-16 05:57:29', '2020-06-16 05:57:34.172-04', 9, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (314, 1, 'http://52.207.106.153:8082/image/15319187_10154681813036788_7291304822623569133_n.jpg', NULL, '15319187_10154681813036788_7291304822623569133_n.jpg', 'f', '2020-06-16 06:42:21', '2020-06-16 06:42:20.682-04', 10, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (316, 1, 'http://52.207.106.153:8082/image/avatar-e21227bf-bb6e-48a6-aac6-d2dfb10f9534.jpg', NULL, 'avatar-e21227bf-bb6e-48a6-aac6-d2dfb10f9534.jpg', 'f', '2020-06-16 07:50:26', '2020-06-16 07:50:25.664-04', 11, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (317, 1, 'http://52.207.106.153:8082/image/avatar-b5d809b8-a2a3-49fa-86f8-e069951667ac.jpg', NULL, 'avatar-b5d809b8-a2a3-49fa-86f8-e069951667ac.jpg', 'f', '2020-06-16 08:09:30', '2020-06-16 08:09:30.22-04', 12, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (318, 1, 'http://52.207.106.153:8082/image/avatar-7578cf3a-0509-45d4-983a-13fd38ce3285.jpg', NULL, 'avatar-7578cf3a-0509-45d4-983a-13fd38ce3285.jpg', 'f', '2020-06-16 08:26:37', '2020-06-16 08:26:36.612-04', 13, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (319, 1, 'http://52.207.106.153:8082/image/avatar-a13640fa-073b-4381-9e91-6d7810ad8080.jpg', NULL, 'avatar-a13640fa-073b-4381-9e91-6d7810ad8080.jpg', 'f', '2020-06-16 08:29:13', '2020-06-16 08:29:12.586-04', 14, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (320, 1, 'http://52.207.106.153:8082/image/avatar-7f9ab54d-9ddb-4b11-a028-db6e601d685c.jpg', NULL, 'avatar-7f9ab54d-9ddb-4b11-a028-db6e601d685c.jpg', 'f', '2020-06-16 08:33:32', '2020-06-16 08:33:31.761-04', 15, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (321, 1, 'http://52.207.106.153:8082/image/avatar-df9cd65e-9d61-4c00-95ed-fa7571eb2695.jpg', NULL, 'avatar-df9cd65e-9d61-4c00-95ed-fa7571eb2695.jpg', 'f', '2020-06-16 08:55:30', '2020-06-16 08:55:30.223-04', 16, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (322, 1, 'http://52.207.106.153:8082/image/avatar-9f6ace98-50ae-49be-a890-fad30ae42660.jpg', NULL, 'avatar-9f6ace98-50ae-49be-a890-fad30ae42660.jpg', 'f', '2020-06-16 10:27:01', '2020-06-16 10:27:01.06-04', 17, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (323, 1, 'http://52.207.106.153:8082/image/avatar-d086a481-b836-4cb5-9631-48e8eb2b1eac.jpg', NULL, 'avatar-d086a481-b836-4cb5-9631-48e8eb2b1eac.jpg', 'f', '2020-06-16 10:34:54', '2020-06-16 10:34:53.547-04', 18, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (324, 1, 'http://52.207.106.153:8082/image/avatar-e88b1b83-000f-466a-9799-f35eb6a53735.jpg', NULL, 'avatar-e88b1b83-000f-466a-9799-f35eb6a53735.jpg', 'f', '2020-06-16 10:36:47', '2020-06-16 10:36:46.778-04', 19, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (325, 1, 'http://52.207.106.153:8082/image/avatar-5479bc0e-92fc-4c3c-9e40-ff5747dda689.jpg', NULL, 'avatar-5479bc0e-92fc-4c3c-9e40-ff5747dda689.jpg', 'f', '2020-06-16 10:38:55', '2020-06-16 10:38:55.2-04', 20, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (327, 1, 'http://52.207.106.153:8082/image/avatar-8992e565-88d1-4c96-94ab-19b57c1ef2b3.jpg', NULL, 'avatar-8992e565-88d1-4c96-94ab-19b57c1ef2b3.jpg', 'f', '2020-06-16 10:49:49', '2020-06-16 10:49:49.064-04', 21, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (329, 1, 'http://52.207.106.153:8082/image/avatar-4824a861-c53c-4a26-ac41-5bf0d0eba659.jpg', NULL, 'avatar-4824a861-c53c-4a26-ac41-5bf0d0eba659.jpg', 'f', '2020-06-16 11:03:58', '2020-06-16 11:03:58.236-04', 22, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (340, 1, 'http://52.207.106.153:8082/image/category-645b15df-e061-40e1-bf6a-193ad18a55c9.jpg', NULL, 'category-645b15df-e061-40e1-bf6a-193ad18a55c9.jpg', 'f', '2020-06-18 07:30:17', '2020-06-18 07:30:17.16-04', 26, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (341, 1, 'http://52.207.106.153:8082/image/avatar-138734e8-c11e-4962-96f7-1255552db099.jpg', NULL, 'avatar-138734e8-c11e-4962-96f7-1255552db099.jpg', 'f', '2020-06-18 07:30:21', '2020-06-18 07:30:21.135-04', 27, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (342, 1, 'http://52.207.106.153:8082/image/avatar-b4fa1842-8b6f-406f-8c14-7c8522f2165d.jpg', NULL, 'avatar-b4fa1842-8b6f-406f-8c14-7c8522f2165d.jpg', 'f', '2020-06-18 07:35:12', '2020-06-18 07:35:12.109-04', 28, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (343, 1, 'http://52.207.106.153:8082/image/avatar-58cfa075-d464-4203-81f3-4b43be026417.jpg', NULL, 'avatar-58cfa075-d464-4203-81f3-4b43be026417.jpg', 'f', '2020-06-18 07:35:12', '2020-06-18 07:35:12.144-04', 29, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (334, 1, 'http://52.207.106.153:8082/image/category-546fc64c-97bb-4f73-bf3b-351fc8719b91.jpg', NULL, 'category-546fc64c-97bb-4f73-bf3b-351fc8719b91.jpg', 'f', '2020-06-18 07:18:53', '2020-06-18 07:18:53.057-04', 23, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (344, 1, 'http://52.207.106.153:8082/image/avatar-b9840b5a-a13d-4cc1-a256-663771567841.jpg', NULL, 'avatar-b9840b5a-a13d-4cc1-a256-663771567841.jpg', 'f', '2020-06-18 07:35:12', '2020-06-18 07:35:12.16-04', 30, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (350, 1, 'http://52.207.106.153:8082/image/category-52dc565f-241e-4903-a1b1-16a8a0f8a384.jpg', NULL, 'category-52dc565f-241e-4903-a1b1-16a8a0f8a384.jpg', 'f', '2020-06-18 07:42:52', '2020-06-18 07:42:51.607-04', 35, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (351, 1, 'http://52.207.106.153:8082/image/avatar-91068901-6e33-4b8b-88d4-d1ff521be2f6.jpg', NULL, 'avatar-91068901-6e33-4b8b-88d4-d1ff521be2f6.jpg', 'f', '2020-06-18 07:42:56', '2020-06-18 07:42:55.704-04', 36, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (352, 1, 'http://52.207.106.153:8082/image/category-c1835d35-e895-4da7-ab57-328f087e6421.jpg', NULL, 'category-c1835d35-e895-4da7-ab57-328f087e6421.jpg', 'f', '2020-06-18 10:39:49', '2020-06-18 10:39:49.559-04', 37, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (336, 1, 'http://52.207.106.153:8082/image/category-f37d59ea-20b9-45e5-b822-38c2fc95f288.jpg', NULL, 'category-f37d59ea-20b9-45e5-b822-38c2fc95f288.jpg', 'f', '2020-06-18 07:22:30', '2020-06-18 07:22:30.101-04', 24, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (346, 1, 'http://52.207.106.153:8082/image/category-bcc20983-ac31-400d-b18f-8176a7c78478.jpg', NULL, 'category-bcc20983-ac31-400d-b18f-8176a7c78478.jpg', 'f', '2020-06-18 07:35:34', '2020-06-18 07:35:33.549-04', 31, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (347, 1, 'http://52.207.106.153:8082/image/avatar-6218398c-13fb-4cbb-8f0b-b4e7f12dcf6c.jpg', NULL, 'avatar-6218398c-13fb-4cbb-8f0b-b4e7f12dcf6c.jpg', 'f', '2020-06-18 07:35:37', '2020-06-18 07:35:36.66-04', 32, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (338, 1, 'http://52.207.106.153:8082/image/category-884f76f0-a717-4dd3-aa15-cfc5fb6cb0f0.jpg', NULL, 'category-884f76f0-a717-4dd3-aa15-cfc5fb6cb0f0.jpg', 'f', '2020-06-18 07:23:55', '2020-06-18 07:23:55.334-04', 25, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (348, 1, 'http://52.207.106.153:8082/image/category-6693445c-a927-4cb4-a71d-eb2a73dd8e8b.jpg', NULL, 'category-6693445c-a927-4cb4-a71d-eb2a73dd8e8b.jpg', 'f', '2020-06-18 07:38:42', '2020-06-18 07:38:42.001-04', 33, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (349, 1, 'http://52.207.106.153:8082/image/avatar-f00d394f-9661-481e-8f7e-e0c846a6f100.jpg', NULL, 'avatar-f00d394f-9661-481e-8f7e-e0c846a6f100.jpg', 'f', '2020-06-18 07:38:46', '2020-06-18 07:38:46.189-04', 34, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (353, 1, 'http://52.207.106.153:8082/image/category-cbe47fd2-d3bd-48da-9b11-04fc9729d8ff.jpg', NULL, 'category-cbe47fd2-d3bd-48da-9b11-04fc9729d8ff.jpg', 'f', '2020-06-18 10:41:09', '2020-06-18 10:41:09.113-04', 38, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (354, 1, 'http://52.207.106.153:8082/image/avatar-4ff69922-2f5e-46c1-96dc-4f322972f8e3.jpg', NULL, 'avatar-4ff69922-2f5e-46c1-96dc-4f322972f8e3.jpg', 'f', '2020-06-18 10:41:13', '2020-06-18 10:41:12.852-04', 39, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (355, 1, 'http://52.207.106.153:8082/image/category-ca0de61d-162d-4fd0-ab43-299ac0506195.jpg', NULL, 'category-ca0de61d-162d-4fd0-ab43-299ac0506195.jpg', 'f', '2020-06-18 10:47:24', '2020-06-18 10:47:23.61-04', 40, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (356, 1, 'http://52.207.106.153:8082/image/avatar-317bd741-1daf-4f3a-beff-0ed540198007.jpg', NULL, 'avatar-317bd741-1daf-4f3a-beff-0ed540198007.jpg', 'f', '2020-06-18 10:47:29', '2020-06-18 10:47:28.948-04', 41, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (502, 1, 'http://52.207.106.153:8082/image/logocolores.png', NULL, 'logocolores.png', 'f', '2020-06-19 17:18:50', '2020-06-19 17:18:50.492-04', 110, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (357, 1, 'http://52.207.106.153:8082/image/category-cc1c5088-763e-415b-8b38-b20d57787650.jpg', NULL, 'category-cc1c5088-763e-415b-8b38-b20d57787650.jpg', 'f', '2020-06-18 10:50:22', '2020-06-18 10:50:22.348-04', 42, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (358, 1, 'http://52.207.106.153:8082/image/avatar-77ffa765-e463-4669-8d6b-eec15ae82a02.jpg', NULL, 'avatar-77ffa765-e463-4669-8d6b-eec15ae82a02.jpg', 'f', '2020-06-18 10:50:26', '2020-06-18 10:50:26.177-04', 43, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (359, 1, 'http://52.207.106.153:8082/image/category-de6ec50f-17b9-4143-bb63-df016853a8c1.jpg', NULL, 'category-de6ec50f-17b9-4143-bb63-df016853a8c1.jpg', 'f', '2020-06-18 10:51:35', '2020-06-18 10:51:35.447-04', 44, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (360, 1, 'http://52.207.106.153:8082/image/avatar-d98c17b2-bc9e-44e3-9251-046ea9deb68b.jpg', NULL, 'avatar-d98c17b2-bc9e-44e3-9251-046ea9deb68b.jpg', 'f', '2020-06-18 10:51:40', '2020-06-18 10:51:39.664-04', 45, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (361, 1, 'http://52.207.106.153:8082/image/category-f03d8ff0-85c3-41d7-bfbc-949a41ccc84b.jpg', NULL, 'category-f03d8ff0-85c3-41d7-bfbc-949a41ccc84b.jpg', 'f', '2020-06-18 10:53:22', '2020-06-18 10:53:22.403-04', 46, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (362, 1, 'http://52.207.106.153:8082/image/avatar-db86756f-0bb7-4a95-b6d1-a807e051ab98.jpg', NULL, 'avatar-db86756f-0bb7-4a95-b6d1-a807e051ab98.jpg', 'f', '2020-06-18 10:53:27', '2020-06-18 10:53:26.613-04', 47, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (363, 1, 'http://52.207.106.153:8082/image/category-114a0277-2940-4f7f-9ec8-192507fa980d.jpg', NULL, 'category-114a0277-2940-4f7f-9ec8-192507fa980d.jpg', 'f', '2020-06-18 11:00:33', '2020-06-18 11:00:33.321-04', 48, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (364, 1, 'http://52.207.106.153:8082/image/avatar-212fd2c2-fc13-40e4-95a3-d680eec91602.jpg', NULL, 'avatar-212fd2c2-fc13-40e4-95a3-d680eec91602.jpg', 'f', '2020-06-18 11:00:37', '2020-06-18 11:00:37.462-04', 49, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (365, 1, 'http://52.207.106.153:8082/image/category-a1205906-01e8-4985-b524-f2c6cc31aa5e.jpg', NULL, 'category-a1205906-01e8-4985-b524-f2c6cc31aa5e.jpg', 'f', '2020-06-18 11:05:08', '2020-06-18 11:05:07.751-04', 50, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (366, 1, 'http://52.207.106.153:8082/image/avatar-978ef998-7c08-4029-8218-0a167bc6e920.jpg', NULL, 'avatar-978ef998-7c08-4029-8218-0a167bc6e920.jpg', 'f', '2020-06-18 11:05:12', '2020-06-18 11:05:12.027-04', 51, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (367, 1, 'http://52.207.106.153:8082/image/category-26fee8a3-c759-4f6b-92da-7d7826c99038.jpg', NULL, 'category-26fee8a3-c759-4f6b-92da-7d7826c99038.jpg', 'f', '2020-06-18 11:09:42', '2020-06-18 11:09:41.588-04', 52, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (368, 1, 'http://52.207.106.153:8082/image/avatar-d026659f-a568-4ea3-b647-27b094e9f980.jpg', NULL, 'avatar-d026659f-a568-4ea3-b647-27b094e9f980.jpg', 'f', '2020-06-18 11:09:45', '2020-06-18 11:09:45.473-04', 53, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (369, 1, 'http://52.207.106.153:8082/image/category-b5126851-f019-4025-af84-75d4f872db98.jpg', NULL, 'category-b5126851-f019-4025-af84-75d4f872db98.jpg', 'f', '2020-06-18 11:13:24', '2020-06-18 11:13:23.694-04', 54, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (370, 1, 'http://52.207.106.153:8082/image/avatar-fe2b09a2-b5b3-49b5-ad94-ac374a6fd3ec.jpg', NULL, 'avatar-fe2b09a2-b5b3-49b5-ad94-ac374a6fd3ec.jpg', 'f', '2020-06-18 11:13:28', '2020-06-18 11:13:27.63-04', 55, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (371, 1, 'http://52.207.106.153:8082/image/category-963d849e-c86f-4cab-ad50-c8f18e8e534d.jpg', NULL, 'category-963d849e-c86f-4cab-ad50-c8f18e8e534d.jpg', 'f', '2020-06-18 11:28:07', '2020-06-18 11:28:06.912-04', 56, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (372, 1, 'http://52.207.106.153:8082/image/avatar-60407353-6551-495c-aaa3-f0a16471fc2e.jpg', NULL, 'avatar-60407353-6551-495c-aaa3-f0a16471fc2e.jpg', 'f', '2020-06-18 11:28:11', '2020-06-18 11:28:11.242-04', 57, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (373, 1, 'http://52.207.106.153:8082/image/category-c0c7fad2-0b2b-4c45-9bc1-7e3872ddd347.jpg', NULL, 'category-c0c7fad2-0b2b-4c45-9bc1-7e3872ddd347.jpg', 'f', '2020-06-18 11:34:12', '2020-06-18 11:34:12.449-04', 58, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (374, 1, 'http://52.207.106.153:8082/image/avatar-919e91ef-9486-41a7-acde-1777f21306e1.jpg', NULL, 'avatar-919e91ef-9486-41a7-acde-1777f21306e1.jpg', 'f', '2020-06-18 11:34:18', '2020-06-18 11:34:17.644-04', 59, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (375, 1, 'http://52.207.106.153:8082/image/category-1d2931da-143f-442d-a4fa-96faa385246d.jpg', NULL, 'category-1d2931da-143f-442d-a4fa-96faa385246d.jpg', 'f', '2020-06-18 11:42:20', '2020-06-18 11:42:20.49-04', 60, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (376, 1, 'http://52.207.106.153:8082/image/avatar-3a51b0a9-22bb-4dc5-b3a8-694037558262.jpg', NULL, 'avatar-3a51b0a9-22bb-4dc5-b3a8-694037558262.jpg', 'f', '2020-06-18 11:42:24', '2020-06-18 11:42:24.39-04', 61, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (377, 1, 'http://52.207.106.153:8082/image/category-9169f846-1268-4412-b465-4c4ce1bbf916.jpg', NULL, 'category-9169f846-1268-4412-b465-4c4ce1bbf916.jpg', 'f', '2020-06-18 11:44:08', '2020-06-18 11:44:08.145-04', 62, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (378, 1, 'http://52.207.106.153:8082/image/avatar-bbf69a36-0d92-4c0d-8a48-4478ba6805ca.jpg', NULL, 'avatar-bbf69a36-0d92-4c0d-8a48-4478ba6805ca.jpg', 'f', '2020-06-18 11:44:12', '2020-06-18 11:44:11.996-04', 63, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (379, 1, 'http://52.207.106.153:8082/image/category-c87feb7d-85ff-4af6-b1d9-6a18f46823bb.jpg', NULL, 'category-c87feb7d-85ff-4af6-b1d9-6a18f46823bb.jpg', 'f', '2020-06-18 11:45:44', '2020-06-18 11:45:44.389-04', 64, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (380, 1, 'http://52.207.106.153:8082/image/avatar-5021696a-6573-4672-bf8b-73f236b54e5d.jpg', NULL, 'avatar-5021696a-6573-4672-bf8b-73f236b54e5d.jpg', 'f', '2020-06-18 11:45:49', '2020-06-18 11:45:48.63-04', 65, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (381, 1, 'http://52.207.106.153:8082/image/category-10af878b-59e3-4f9a-808e-78d2e1226432.jpg', NULL, 'category-10af878b-59e3-4f9a-808e-78d2e1226432.jpg', 'f', '2020-06-18 11:49:19', '2020-06-18 11:49:18.668-04', 66, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (382, 1, 'http://52.207.106.153:8082/image/avatar-9eb45421-900e-4999-9f1a-5a0fff85faef.jpg', NULL, 'avatar-9eb45421-900e-4999-9f1a-5a0fff85faef.jpg', 'f', '2020-06-18 11:49:22', '2020-06-18 11:49:22.417-04', 67, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (383, 1, 'http://52.207.106.153:8082/image/avatar-641ac38b-6de1-4908-90e2-87ab9b60cfb1.jpg', NULL, 'avatar-641ac38b-6de1-4908-90e2-87ab9b60cfb1.jpg', 'f', '2020-06-18 11:51:32', '2020-06-18 11:51:31.71-04', 68, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (384, 1, 'http://52.207.106.153:8082/image/avatar-a2e32664-af8b-40c9-9ce5-10d5d28e16f6.jpg', NULL, 'avatar-a2e32664-af8b-40c9-9ce5-10d5d28e16f6.jpg', 'f', '2020-06-18 11:51:32', '2020-06-18 11:51:31.788-04', 69, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (385, 1, 'http://52.207.106.153:8082/image/avatar-e5ddabd8-86a4-4b34-a4d2-58a6e3db94e7.jpg', NULL, 'avatar-e5ddabd8-86a4-4b34-a4d2-58a6e3db94e7.jpg', 'f', '2020-06-18 11:51:32', '2020-06-18 11:51:31.805-04', 70, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (391, 1, 'http://52.207.106.153:8082/image/avatar-0910948b-1a51-4c15-a65c-525980e2135d.jpg', NULL, 'avatar-0910948b-1a51-4c15-a65c-525980e2135d.jpg', 'f', '2020-06-18 11:58:37', '2020-06-18 11:58:37.002-04', 75, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (496, 1, 'http://52.207.106.153:8082/image/avatar-909541e3-7315-4afe-a66e-ce427fbc8e7e.jpg', NULL, 'avatar-909541e3-7315-4afe-a66e-ce427fbc8e7e.jpg', 'f', '2020-06-19 16:46:55', '2020-06-19 16:46:54.622-04', 105, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (387, 1, 'http://52.207.106.153:8082/image/category-a18e9d56-d19f-454c-b133-bc912f73d0e2.jpg', NULL, 'category-a18e9d56-d19f-454c-b133-bc912f73d0e2.jpg', 'f', '2020-06-18 11:52:08', '2020-06-18 11:52:07.874-04', 71, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (388, 1, 'http://52.207.106.153:8082/image/avatar-f0c43e89-e573-4b28-8dcd-6b0e462081e8.jpg', NULL, 'avatar-f0c43e89-e573-4b28-8dcd-6b0e462081e8.jpg', 'f', '2020-06-18 11:52:11', '2020-06-18 11:52:11.433-04', 72, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (389, 1, 'http://52.207.106.153:8082/image/avatar-5673b371-fba8-4695-8b10-7ad7cd4c5e0b.jpg', NULL, 'avatar-5673b371-fba8-4695-8b10-7ad7cd4c5e0b.jpg', 'f', '2020-06-18 11:58:37', '2020-06-18 11:58:36.935-04', 73, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (390, 1, 'http://52.207.106.153:8082/image/avatar-c0ae9420-8fed-4cb5-97ce-07e95ff45853.jpg', NULL, 'avatar-c0ae9420-8fed-4cb5-97ce-07e95ff45853.jpg', 'f', '2020-06-18 11:58:37', '2020-06-18 11:58:36.978-04', 74, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (393, 1, 'http://52.207.106.153:8082/image/category-220669b8-ec93-4b99-92eb-b30fc2e39002.jpg', NULL, 'category-220669b8-ec93-4b99-92eb-b30fc2e39002.jpg', 'f', '2020-06-18 11:59:05', '2020-06-18 11:59:04.888-04', 76, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (394, 1, 'http://52.207.106.153:8082/image/avatar-e143bb13-e4f3-491d-a89a-942342e77a17.jpg', NULL, 'avatar-e143bb13-e4f3-491d-a89a-942342e77a17.jpg', 'f', '2020-06-18 11:59:08', '2020-06-18 11:59:08.031-04', 77, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (400, 1, 'http://52.207.106.153:8082/image/category-59af41c6-33f2-4f22-b31a-a262336ea42c.jpg', NULL, 'category-59af41c6-33f2-4f22-b31a-a262336ea42c.jpg', 'f', '2020-06-19 14:05:35', '2020-06-19 14:05:34.959-04', 78, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (401, 1, 'http://52.207.106.153:8082/image/avatar-5aabf98f-31f0-4574-809a-eb6b4ffb99b7.jpg', NULL, 'avatar-5aabf98f-31f0-4574-809a-eb6b4ffb99b7.jpg', 'f', '2020-06-19 14:05:40', '2020-06-19 14:05:40.489-04', 79, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (497, 1, 'http://52.207.106.153:8082/image/avatar-6d558a32-ef7f-40dc-bacc-fd2cff45af0d.jpg', NULL, 'avatar-6d558a32-ef7f-40dc-bacc-fd2cff45af0d.jpg', 'f', '2020-06-19 16:46:55', '2020-06-19 16:46:54.726-04', 106, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (498, 1, 'http://52.207.106.153:8082/image/avatar-520ec510-ef32-40d0-a4cb-191217146383.jpg', NULL, 'avatar-520ec510-ef32-40d0-a4cb-191217146383.jpg', 'f', '2020-06-19 16:46:55', '2020-06-19 16:46:54.766-04', 107, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (499, 2, 'https://www.youtube.com/watch?v=B-YnLoozIAs&list=RDB-YnLoozIAs&start_radio=1&t=1', NULL, 'https://www.youtube.com/watch?v=B-YnLoozIAs&list=RDB-YnLoozIAs&start_radio=1&t=1', 'f', '2020-06-19 16:46:55', '2020-06-19 16:46:54.821-04', 108, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (503, 1, 'http://52.207.106.153:8082/image/OCBi7.jpg', NULL, 'OCBi7.jpg', 'f', '2020-06-19 17:18:51', '2020-06-19 17:18:50.568-04', 111, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (504, 1, 'http://52.207.106.153:8082/image/splashv2.png', NULL, 'splashv2.png', 'f', '2020-06-19 17:18:51', '2020-06-19 17:18:50.599-04', 112, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (505, 2, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', NULL, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', 'f', '2020-06-19 17:18:51', '2020-06-19 17:18:50.63-04', 113, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (432, 1, 'http://52.207.106.153:8082/image/splashv2.png', NULL, 'splashv2.png', 'f', '2020-06-19 15:07:30', '2020-06-19 15:07:30.126-04', 80, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (436, 1, 'http://52.207.106.153:8082/image/logocolores.png', NULL, 'logocolores.png', 'f', '2020-06-19 15:12:08', '2020-06-19 15:12:08.165-04', 81, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (437, 1, 'http://52.207.106.153:8082/image/OCBi7.jpg', NULL, 'OCBi7.jpg', 'f', '2020-06-19 15:12:08', '2020-06-19 15:12:08.192-04', 82, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (438, 1, 'http://52.207.106.153:8082/image/splashv2.png', NULL, 'splashv2.png', 'f', '2020-06-19 15:12:08', '2020-06-19 15:12:08.215-04', 83, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (469, 1, 'http://52.207.106.153:8082/image/logocolores.png', NULL, 'logocolores.png', 'f', '2020-06-19 16:04:54', '2020-06-19 16:04:54.33-04', 84, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (470, 1, 'http://52.207.106.153:8082/image/OCBi7.jpg', NULL, 'OCBi7.jpg', 'f', '2020-06-19 16:04:54', '2020-06-19 16:04:54.405-04', 85, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (471, 1, 'http://52.207.106.153:8082/image/splashv2.png', NULL, 'splashv2.png', 'f', '2020-06-19 16:04:54', '2020-06-19 16:04:54.433-04', 86, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (472, 2, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', NULL, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', 'f', '2020-06-19 16:04:54', '2020-06-19 16:04:54.463-04', 87, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (476, 1, 'http://52.207.106.153:8082/image/descarga.jpg', NULL, 'descarga.jpg', 'f', '2020-06-19 16:14:08', '2020-06-19 16:14:07.912-04', 88, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (477, 1, 'http://52.207.106.153:8082/image/logocolores.png', NULL, 'logocolores.png', 'f', '2020-06-19 16:14:21', '2020-06-19 16:14:20.755-04', 89, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (478, 1, 'http://52.207.106.153:8082/image/OCBi7.jpg', NULL, 'OCBi7.jpg', 'f', '2020-06-19 16:14:21', '2020-06-19 16:14:20.782-04', 90, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (479, 1, 'http://52.207.106.153:8082/image/splashv2.png', NULL, 'splashv2.png', 'f', '2020-06-19 16:14:21', '2020-06-19 16:14:20.802-04', 91, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (480, 2, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', NULL, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', 'f', '2020-06-19 16:14:21', '2020-06-19 16:14:20.828-04', 92, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (481, 1, 'http://52.207.106.153:8082/image/logocolores.png', NULL, 'logocolores.png', 'f', '2020-06-19 16:21:03', '2020-06-19 16:21:02.923-04', 93, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (482, 1, 'http://52.207.106.153:8082/image/OCBi7.jpg', NULL, 'OCBi7.jpg', 'f', '2020-06-19 16:21:03', '2020-06-19 16:21:02.992-04', 94, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (483, 1, 'http://52.207.106.153:8082/image/splashv2.png', NULL, 'splashv2.png', 'f', '2020-06-19 16:21:03', '2020-06-19 16:21:03.02-04', 95, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (484, 2, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', NULL, 'https://www.youtube.com/watch?v=J5o8Daw1ZsY&list=RDB-YnLoozIAs&index=17', 'f', '2020-06-19 16:21:03', '2020-06-19 16:21:03.055-04', 96, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (485, 1, 'http://52.207.106.153:8082/image/avatar-18223783-df37-43f8-81a8-c75367c6d476.jpg', NULL, 'avatar-18223783-df37-43f8-81a8-c75367c6d476.jpg', 'f', '2020-06-19 16:24:42', '2020-06-19 16:24:42.235-04', 97, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (486, 1, 'http://52.207.106.153:8082/image/avatar-9845a23c-c939-4d4d-a0fa-c790945cf677.jpg', NULL, 'avatar-9845a23c-c939-4d4d-a0fa-c790945cf677.jpg', 'f', '2020-06-19 16:24:42', '2020-06-19 16:24:42.296-04', 98, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (487, 1, 'http://52.207.106.153:8082/image/avatar-191abf47-d4b4-4043-8d0e-cfef73007a74.jpg', NULL, 'avatar-191abf47-d4b4-4043-8d0e-cfef73007a74.jpg', 'f', '2020-06-19 16:24:42', '2020-06-19 16:24:42.323-04', 99, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (488, 2, 'https://www.youtube.com/watch?v=B-YnLoozIAs&list=RDB-YnLoozIAs&start_radio=1&t=1', NULL, 'https://www.youtube.com/watch?v=B-YnLoozIAs&list=RDB-YnLoozIAs&start_radio=1&t=1', 'f', '2020-06-19 16:24:42', '2020-06-19 16:24:42.362-04', 100, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (1, 1, '/test', 'metadata prueba', 'ASD', NULL, NULL, '2020-04-14 07:30:27.907623-04', NULL, 0, 11);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (490, 1, 'http://52.207.106.153:8082/image/category-b5b964d3-b5ec-4bfc-a11d-1ef212e10618.jpg', NULL, 'category-b5b964d3-b5ec-4bfc-a11d-1ef212e10618.jpg', 'f', '2020-06-19 16:25:53', '2020-06-19 16:25:53.234-04', 101, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (491, 1, 'http://52.207.106.153:8082/image/avatar-6cdb9ff0-74a7-4e32-9eaf-5d1d6339d702.jpg', NULL, 'avatar-6cdb9ff0-74a7-4e32-9eaf-5d1d6339d702.jpg', 'f', '2020-06-19 16:26:01', '2020-06-19 16:26:00.59-04', 102, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (501, 1, 'http://52.207.106.153:8082/image/category-62362435-f0d2-4c68-b243-8b483d5c955a.jpg', NULL, 'category-62362435-f0d2-4c68-b243-8b483d5c955a.jpg', 'f', '2020-06-19 16:48:09', '2020-06-19 16:48:08.663-04', 109, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (506, 1, 'http://52.207.106.153:8082/image/descarga.jpg', NULL, 'descarga.jpg', 'f', '2020-06-19 17:22:36', '2020-06-19 17:22:36.117-04', 114, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (507, 1, 'http://52.207.106.153:8082/image/entrenador.pdf', NULL, 'entrenador.pdf', 'f', '2020-06-19 17:26:38', '2020-06-19 17:26:37.587-04', 115, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (508, 1, 'http://52.207.106.153:8082/image/entrenador.pdf', NULL, 'entrenador.pdf', 'f', '2020-06-19 17:27:46', '2020-06-19 17:27:46.434-04', 116, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (509, 1, 'http://52.207.106.153:8082/image/IMG_20200522_200409 (1).jpg', NULL, 'IMG_20200522_200409 (1).jpg', 'f', '2020-06-19 17:31:31', '2020-06-19 17:31:31.027-04', 117, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (493, 1, 'http://52.207.106.153:8082/image/category-60711633-8b8c-488a-9084-0cfcdb0971b0.jpg', NULL, 'category-60711633-8b8c-488a-9084-0cfcdb0971b0.jpg', 'f', '2020-06-19 16:32:52', '2020-06-19 16:32:51.883-04', 103, NULL, NULL);
INSERT INTO "dinbog"."attachment" ("id", "attachment_type_id", "path", "metadata", "extra", "purged", "date_purged", "created", "count_attachment", "count_like", "count_comments") VALUES (494, 1, 'http://52.207.106.153:8082/image/avatar-7968c6e0-b8f8-4a4f-8069-683c14cbbd0f.jpg', NULL, 'avatar-7968c6e0-b8f8-4a4f-8069-683c14cbbd0f.jpg', 'f', '2020-06-19 16:32:58', '2020-06-19 16:32:58.322-04', 104, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for attachment_comment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_comment" CASCADE;
CREATE TABLE "dinbog"."attachment_comment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_comment_id_seq'::regclass),
  "parent_id" int4,
  "profile_id" int4,
  "attachment_id" int4,
  "value" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now(),
  "count_like" int4 DEFAULT 0
)
;

-- ----------------------------
-- Records of attachment_comment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (14, NULL, 5, 1, 'nueva prueba sssssss', '2020-04-14 13:32:09.542-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (15, NULL, 9, 1, 'hola', '2020-04-14 13:34:17.516-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (16, 1, 6, 1, 'subcomentario sobre parent 1', '2020-04-14 13:35:16.834-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (17, 1, 4, 1, 'otro coment sobre parent 1', '2020-04-14 13:37:49.195-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (18, 17, 1, 1, 'le respondo a id 17', '2020-04-14 13:38:30.324-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (1, NULL, 1, 1, 'Mensaje PADRE prueba para comentar un attachment - COMENTARIO EDITADO', '2020-04-14 10:13:57.821-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (167, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-13 19:13:18.632-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (168, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-13 19:18:55.345-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (214, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-22 22:31:16.599-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (215, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-23 00:59:21.615-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (216, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-23 01:19:16.914-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (217, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-23 01:24:44.863-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (219, NULL, 1, 189, 'Comentario al attahcment 189 ', '2020-05-23 02:05:41.331716-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (220, NULL, 5, 189, 'Comentario Profile 5 a attachment 189', '2020-05-23 02:07:52.286014-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (221, NULL, 9, 189, 'Comentario Profile 9 a attachment 189', '2020-05-23 02:21:05.893616-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (222, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-05-23 02:32:08.267-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (224, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-05-23 02:46:11.595-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (225, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-05-23 02:53:32.489-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (226, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-05-23 03:19:29.722-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (227, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-05-23 03:48:57.608-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (228, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-05-23 03:59:59.048-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (229, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-05-23 04:18:19.048-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (223, NULL, 7, 1, 'Comentario Creado', '2020-05-23 02:44:03.575954-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (288, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-06-05 02:42:43.475-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (289, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-06-05 10:21:25.933-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (290, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-06-05 11:10:27.679-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (291, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-06-05 13:02:04.392-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (292, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-06-05 14:13:27.615-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (293, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-06-05 14:21:19.097-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (263, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-06-02 02:55:01.088-04', 0);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (302, NULL, 1, 1, 'Mensaje desde @Test: se asume ya existe un profile con id 1', '2020-06-06 00:04:04.645-04', NULL);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (335, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-06-15 00:24:19.618-04', NULL);
INSERT INTO "dinbog"."attachment_comment" ("id", "parent_id", "profile_id", "attachment_id", "value", "created", "count_like") VALUES (336, NULL, 1, 1, 'Modificacion *** comentario desde @test', '2020-06-15 00:25:59.342-04', NULL);
COMMIT;

-- ----------------------------
-- Table structure for attachment_comment_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_comment_like" CASCADE;
CREATE TABLE "dinbog"."attachment_comment_like" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_comment_like_id_seq'::regclass),
  "attachment_comment_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Table structure for attachment_comment_mention
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_comment_mention" CASCADE;
CREATE TABLE "dinbog"."attachment_comment_mention" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_comment_mention_id_seq'::regclass),
  "attachment_comment_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Table structure for attachment_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_like" CASCADE;
CREATE TABLE "dinbog"."attachment_like" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_like_id_seq'::regclass),
  "profile_id" int4,
  "attachment_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for attachment_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_notification" CASCADE;
CREATE TABLE "dinbog"."attachment_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4,
  "message" text COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of attachment_notification
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (17, 205, 1, 7, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(1) TABLA INFO:attachment_like', '2020-05-23 02:04:57.561457-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (18, 206, 8, 7, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(8) TABLA INFO:attachment_like', '2020-05-23 02:05:12.90827-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (19, 219, 1, 7, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(1) TABLA INFO:attachment_comment', '2020-05-23 02:05:41.331716-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (20, 220, 5, 7, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:attachment_comment', '2020-05-23 02:07:52.286014-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (21, 221, 9, 7, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(9) TABLA INFO:attachment_comment', '2020-05-23 02:21:05.893616-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (22, 207, 9, 7, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(9) TABLA INFO:attachment_like', '2020-05-23 02:23:56.424856-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (23, 210, 9, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(9) TABLA INFO:attachment_like', '2020-05-23 02:27:43.333206-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (24, 211, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 02:28:02.933103-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (25, 212, 9, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(9) TABLA INFO:attachment_like', '2020-05-23 02:28:08.488865-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (26, 213, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 02:43:54.004735-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (27, 223, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_comment', '2020-05-23 02:44:03.575954-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (28, 216, 6, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(6) TABLA INFO:attachment_like', '2020-05-23 04:10:09.089548-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (29, 217, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 04:10:18.044992-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (30, 218, 8, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(8) TABLA INFO:attachment_like', '2020-05-23 04:17:03.745733-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (31, 219, 10, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(10) TABLA INFO:attachment_like', '2020-05-23 04:17:18.088271-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (32, 220, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 04:18:19.850688-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (33, 221, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 04:38:55.203929-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (34, 222, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 04:42:21.245422-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (35, 223, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 04:46:31.450599-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (36, 224, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 15:23:19.08345-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (37, 225, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 16:09:31.159455-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (38, 226, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-23 18:51:50.279063-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (39, 227, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-24 18:53:58.386302-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (41, 229, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-24 19:49:20.802335-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (42, 230, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-24 19:56:17.719962-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (43, 231, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-24 20:21:07.168756-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (44, 232, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-24 23:20:18.120705-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (45, 233, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 00:12:31.145624-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (46, 234, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 00:22:21.902154-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (47, 235, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 01:51:48.150743-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (48, 236, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 17:10:38.25858-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (49, 237, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 17:22:33.91163-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (50, 238, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 17:32:23.328493-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (51, 239, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 17:38:35.953303-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (52, 240, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 17:53:21.141998-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (53, 241, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 17:56:58.316192-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (54, 242, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 18:06:34.835513-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (55, 243, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 18:09:56.83862-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (56, 244, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 19:14:17.44349-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (57, 245, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 19:26:00.070148-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (58, 246, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 19:43:01.109898-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (59, 247, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 20:00:37.755332-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (60, 248, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 20:18:00.944014-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (61, 249, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-25 20:21:13.671386-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (62, 250, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-31 11:22:35.033282-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (63, 251, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-31 11:26:23.769247-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (64, 252, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-31 11:30:35.457238-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (65, 253, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-05-31 11:34:14.957383-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (66, 254, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 02:55:01.724731-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (67, 255, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 03:16:16.984744-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (68, 256, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 03:40:18.102129-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (69, 257, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 04:00:17.216484-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (70, 258, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 04:20:17.902528-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (71, 259, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 04:49:21.165259-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (72, 260, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-02 18:03:49.07228-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (73, 261, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 08:57:03.734708-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (74, 262, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 09:33:58.900952-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (75, 263, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 09:49:43.429678-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (76, 264, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 10:12:55.961958-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (77, 265, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 10:32:23.751608-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (78, 266, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 10:33:56.819929-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (79, 267, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 10:51:36.678701-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (80, 268, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 11:41:38.771947-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (81, 269, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 11:43:01.986583-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (82, 270, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 11:47:49.212169-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (83, 271, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 11:51:41.787195-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (84, 272, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 12:03:28.082872-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (85, 273, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-03 12:10:53.623442-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (86, 274, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-04 01:11:58.251205-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (87, 275, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-04 01:28:55.33661-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (88, 276, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-04 01:34:18.739631-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (89, 277, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-04 01:45:55.234621-04');
INSERT INTO "dinbog"."attachment_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (90, 278, 7, 1, NULL, 'trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(7) TABLA INFO:attachment_like', '2020-06-04 02:03:40.033515-04');
COMMIT;

-- ----------------------------
-- Table structure for attachment_report
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_report" CASCADE;
CREATE TABLE "dinbog"."attachment_report" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_report_id_seq'::regclass),
  "report_type_id" int4,
  "detail" varchar(1000) COLLATE "pg_catalog"."default",
  "owner_id" int4,
  "attachment_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of attachment_report
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (6, 1, 'mensaje prueba de report', 1, 1, '2020-05-04 06:24:13.486-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (7, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-04 13:17:21.479-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (8, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-04 13:18:05.008-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (93, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-12 15:15:38.062-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (94, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-12 15:26:15.143-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (96, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-13 19:13:18.916-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (97, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-13 19:18:55.615-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (98, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-13 19:45:18.283-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (99, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-13 19:50:03.685-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (100, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-13 20:09:54.216-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (102, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-13 20:25:37.095-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (104, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-14 13:59:40.043-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (105, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-14 14:05:20.836-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (106, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-14 15:06:46.191-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (107, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-14 16:00:57.644-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (108, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-15 14:03:42.867-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (109, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-17 21:35:54.408-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (110, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-17 22:41:07.496-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (111, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-17 22:45:38.143-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (112, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-17 23:28:38.396-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (113, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-17 23:30:44.179-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (114, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 04:29:15.121-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (115, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 05:00:29.017-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (116, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 16:04:49.096-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (117, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 16:17:11.95-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (118, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 17:06:59.537-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (119, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 17:11:53.553-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (120, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 17:42:14.644-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (121, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 17:45:52.446-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (122, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 17:48:54.531-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (123, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 18:06:53.901-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (124, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 18:43:58.438-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (125, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 19:06:31.137-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (126, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 20:22:48.677-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (127, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 20:29:42.412-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (128, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 20:43:16.727-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (129, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 20:45:43.154-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (130, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-18 20:57:48.255-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (131, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-20 18:06:38.158-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (132, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-20 18:45:15.836-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (133, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 08:52:04.727-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (134, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 09:03:34.174-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (135, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 09:08:51.999-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (136, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 09:15:47.402-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (137, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 09:24:27.93-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (138, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 09:34:38.949-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (139, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 10:23:17.562-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (140, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 20:20:14.307-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (141, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-21 20:28:24.26-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (143, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-22 22:31:16.915-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (144, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 00:59:21.898-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (145, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 01:19:17.337-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (146, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 01:24:45.227-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (147, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 02:32:08.648-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (148, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 04:38:54.82-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (149, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 04:42:20.84-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (150, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 04:46:31.251-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (151, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 15:23:18.934-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (152, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 16:09:30.937-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (153, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-23 18:51:50.106-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (154, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-24 18:53:58.189-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (156, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-24 19:49:20.61-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (157, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-24 19:56:17.538-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (158, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-24 20:21:06.907-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (159, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-24 23:20:17.898-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (160, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 00:12:30.977-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (161, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 00:22:21.738-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (162, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 01:51:47.935-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (163, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 17:10:38.097-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (164, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 17:22:33.688-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (165, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 17:32:22.975-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (166, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 17:38:35.602-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (167, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 17:53:20.864-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (168, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 17:56:58.143-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (169, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 18:06:34.661-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (170, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 18:09:56.661-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (171, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 19:14:17.236-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (172, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 19:25:59.837-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (173, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 19:43:00.946-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (174, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 20:00:37.611-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (175, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 20:18:00.698-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (176, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-25 20:21:13.464-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (177, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-31 11:22:34.808-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (178, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-31 11:26:23.569-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (179, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-31 11:30:35.265-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (180, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-05-31 11:34:14.761-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (181, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 02:55:01.608-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (182, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 03:16:16.816-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (183, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 03:40:17.858-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (184, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 04:00:16.865-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (185, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 04:20:17.542-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (186, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 04:49:21.011-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (187, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-02 18:03:48.783-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (188, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 08:57:03.559-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (189, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 09:33:58.678-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (190, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 09:49:43.227-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (191, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 10:12:55.647-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (192, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 10:32:23.539-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (193, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 10:33:56.65-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (194, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 10:51:36.492-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (195, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 11:41:38.528-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (196, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 11:43:01.835-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (197, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 11:47:48.987-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (198, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 11:51:41.618-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (199, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 12:03:27.355-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (200, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-03 12:10:53.189-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (201, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-04 01:11:58.041-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (202, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-04 01:28:55.173-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (203, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-04 01:34:18.575-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (204, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-04 01:45:55.078-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (205, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-04 02:03:39.858-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (206, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 15:21:59.529-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (207, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 16:44:01.171-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (208, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 17:03:39.412-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (209, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 17:13:54.463-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (210, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 17:31:59.757-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (211, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 18:47:36.13-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (212, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-05 20:17:58.778-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (214, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 00:11:44.269-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (215, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 00:27:34.076-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (216, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 00:34:03.316-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (217, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 01:15:37.088-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (218, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 01:28:36.997-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (219, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 01:35:25.859-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (220, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 02:00:54.183-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (221, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 02:19:56.997-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (222, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 02:41:14.757-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (223, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 02:46:38.652-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (224, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-06 17:14:05.532-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (225, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-07 21:56:21.068-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (226, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-07 22:05:38.305-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (227, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-07 22:11:57.696-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (228, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-07 22:33:00.688-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (229, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-07 22:48:22.257-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (230, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-08 06:01:28.841-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (231, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-08 19:25:59.213-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (232, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-10 23:54:12.456-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (233, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 00:13:22.882-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (234, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 00:20:58.632-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (235, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 01:54:35.228-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (236, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:02:21.298-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (237, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:13:49.891-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (238, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:22:39.374-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (239, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:23:58.584-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (240, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:29:48.984-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (241, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:38:06.635-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (242, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-11 02:42:44.968-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (243, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 00:26:00.342-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (244, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 00:30:18.991-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (245, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 01:29:27.535-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (246, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 01:42:28.06-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (247, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 01:49:55.945-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (248, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 11:52:22.45-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (249, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-15 11:57:47.53-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (251, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-16 04:15:12.144-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (252, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-16 07:40:38.998-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (253, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-16 10:49:20.644-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (254, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-16 11:03:29.189-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (256, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 07:18:46.42-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (257, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 07:22:24.057-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (258, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 07:23:49.222-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (259, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 07:30:10.816-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (260, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 07:35:14.546-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (261, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 11:51:34.402-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (262, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-18 11:58:40.037-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (264, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-19 14:04:50.318-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (265, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-19 16:24:44.979-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (266, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-19 16:31:43.098-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (267, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-19 16:43:07.236-04');
INSERT INTO "dinbog"."attachment_report" ("id", "report_type_id", "detail", "owner_id", "attachment_id", "created") VALUES (268, 1, 'Mensaje de prueba para reporte desde un @Test', 1, 1, '2020-06-19 16:47:00.944-04');
COMMIT;

-- ----------------------------
-- Table structure for attachment_share
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_share" CASCADE;
CREATE TABLE "dinbog"."attachment_share" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_share_id_seq'::regclass),
  "attachment_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for attachment_tag
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_tag" CASCADE;
CREATE TABLE "dinbog"."attachment_tag" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_tag_id_seq'::regclass),
  "profile_id" int4,
  "attachment_id" int4,
  "positionx" float4,
  "positiony" float4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for attachment_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."attachment_type" CASCADE;
CREATE TABLE "dinbog"."attachment_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".attachment_type_id_seq'::regclass),
  "value" varchar COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of attachment_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."attachment_type" ("id", "value") VALUES (1, 'image');
INSERT INTO "dinbog"."attachment_type" ("id", "value") VALUES (2, 'video');
INSERT INTO "dinbog"."attachment_type" ("id", "value") VALUES (3, 'audio');
COMMIT;

-- ----------------------------
-- Table structure for auditory_action
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."auditory_action" CASCADE;
CREATE TABLE "dinbog"."auditory_action" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".auditory_action_id_seq'::regclass),
  "user_id" int4,
  "profile_id" int4,
  "auditory_action_type_id" int4,
  "target" varchar COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for auditory_action_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."auditory_action_type" CASCADE;
CREATE TABLE "dinbog"."auditory_action_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".auditory_action_type_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for auditory_errors
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."auditory_errors" CASCADE;
CREATE TABLE "dinbog"."auditory_errors" (
  "id" int4 NOT NULL,
  "user_id" int4,
  "error_value" text COLLATE "pg_catalog"."default" NOT NULL,
  "platform" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "path" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "ip" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."city" CASCADE;
CREATE TABLE "dinbog"."city" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".city_id_seq'::regclass),
  "country_id" int4 NOT NULL,
  "code" varchar(100) COLLATE "pg_catalog"."default",
  "latitude" varchar(255) COLLATE "pg_catalog"."default",
  "longitude" varchar(255) COLLATE "pg_catalog"."default",
  "state_name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of city
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."city" ("id", "country_id", "code", "latitude", "longitude", "state_name") VALUES (1, 144, 'veval', NULL, NULL, 'Caracas');
INSERT INTO "dinbog"."city" ("id", "country_id", "code", "latitude", "longitude", "state_name") VALUES (2, 144, 'veccs', NULL, NULL, 'Valencia');
INSERT INTO "dinbog"."city" ("id", "country_id", "code", "latitude", "longitude", "state_name") VALUES (3, 144, 'vebar', NULL, NULL, 'Barquisimeto');
COMMIT;

-- ----------------------------
-- Table structure for connection_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."connection_type" CASCADE;
CREATE TABLE "dinbog"."connection_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".connection_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of connection_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."connection_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (1, 'friends', 'amistad', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."connection_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (2, 'professional', 'profesional', NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for conversation
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."conversation" CASCADE;
CREATE TABLE "dinbog"."conversation" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".conversation_id_seq'::regclass),
  "conversation_status_id" int4,
  "description_name" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now(),
  "last_message_date" timestamptz(0)
)
;

-- ----------------------------
-- Table structure for conversation_message
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."conversation_message" CASCADE;
CREATE TABLE "dinbog"."conversation_message" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".conversation_message_id_seq'::regclass),
  "conversation_participant_id" int4 NOT NULL,
  "message" text COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for conversation_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."conversation_notification" CASCADE;
CREATE TABLE "dinbog"."conversation_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".conversation_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4 NOT NULL,
  "message" text COLLATE "pg_catalog"."default",
  "created" timestamptz(6)
)
;

-- ----------------------------
-- Table structure for conversation_participant
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."conversation_participant" CASCADE;
CREATE TABLE "dinbog"."conversation_participant" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".conversation_participant_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "is_owner" int2 NOT NULL,
  "conversation_id" int4 NOT NULL,
  "conversation_participant_status_id" int4 NOT NULL,
  "is_archived" int2
)
;

-- ----------------------------
-- Table structure for conversation_participant_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."conversation_participant_status" CASCADE;
CREATE TABLE "dinbog"."conversation_participant_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".conversation_participant_status_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for conversation_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."conversation_status" CASCADE;
CREATE TABLE "dinbog"."conversation_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".conversation_status_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."country" CASCADE;
CREATE TABLE "dinbog"."country" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".country_id_seq'::regclass),
  "language_id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "iso_code" varchar(3) COLLATE "pg_catalog"."default",
  "unit_measurement" varchar(255) COLLATE "pg_catalog"."default",
  "wear_measurement" varchar(255) COLLATE "pg_catalog"."default",
  "is_onu" bool DEFAULT false,
  "created" timestamptz(0) DEFAULT now(),
  "updated" timestamptz(0)
)
;

-- ----------------------------
-- Records of country
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (2, 1, 'Saudi Arabia', 'SA', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (3, 1, 'United Arab Emirates', 'AE', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (4, 1, 'Heard Island and McDonald Islands', 'HM', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (5, 1, 'Israel', 'IL', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (6, 1, 'Italy', 'IT', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (7, 1, 'Kenya', 'KE', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (8, 1, 'Senegal', 'SN', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (9, 1, 'Serbia', 'RS', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (10, 1, 'Togo', 'TG', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (11, 1, 'Spain', 'ES', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (12, 1, 'France', 'FR', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (13, 1, 'Gambia', 'GM', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (14, 1, 'Greenland', 'GL', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (15, 1, 'Guinea', 'GN', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (16, 1, 'Equatorial Guinea', 'GQ', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (17, 1, 'Guyana', 'GY', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (18, 1, 'Haiti', 'HT', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (19, 1, 'India', 'IN', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (20, 1, 'Iraq', 'IQ', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (21, 1, 'Bouvet Island', 'BV', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (22, 1, 'Isle of Man', 'IM', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (23, 1, 'Iceland', 'IS', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (24, 1, 'Bermuda Islands', 'BM', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (25, 1, 'Faroe Islands', 'FO', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (26, 1, 'South Georgia and the South Sandwich Islands', 'GS', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (27, 1, 'Maldives', 'MV', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (28, 1, 'Falkland Islands (Malvinas)', 'FK', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (29, 1, 'Northern Mariana Islands', 'MP', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (30, 1, 'Marshall Islands', 'MH', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (31, 1, 'Pitcairn Islands', 'PN', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (32, 1, 'Solomon Islands', 'SB', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (34, 1, 'Virgin Islands', 'VG', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (35, 1, 'United States Virgin Islands', 'VI', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (36, 1, 'Jersey', 'JE', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (37, 1, 'Jordan', 'JO', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (38, 1, 'Kazakhstan', 'KZ', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (39, 1, 'Kyrgyzstan', 'KG', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (40, 1, 'Kiribati', 'KI', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (41, 1, 'Kuwait', 'KW', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (42, 1, 'Lebanon', 'LB', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (43, 1, 'Laos', 'LA', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (45, 1, 'Latvia', 'LV', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (46, 1, 'Liberia', 'LR', 'E', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (47, 1, 'Libya', 'LY', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (48, 1, 'Liechtenstein', 'LI', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (49, 1, 'Lithuania', 'LT', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (50, 1, 'Luxembourg', 'LU', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (51, 1, 'Mexico', 'MX', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (52, 1, 'Monaco', 'MC', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (53, 1, 'Macao', 'MO', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (54, 1, 'Macedonia', 'MK', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (55, 1, 'Madagascar', 'MG', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (56, 1, 'Malaysia', 'MY', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (58, 1, 'Mali', 'ML', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (59, 1, 'Malta', 'MT', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (60, 1, 'Morocco', 'MA', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (61, 1, 'Martinique', 'MQ', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (62, 1, 'Mauritius', 'MU', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (63, 1, 'Mauritania', 'MR', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (64, 1, 'Moldova', 'MD', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (65, 1, 'Mongolia', 'MN', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (66, 1, 'Montenegro', 'ME', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (68, 1, 'Mozambique', 'MZ', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (69, 1, 'Namibia', 'NA', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (70, 1, 'Nauru', 'NR', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (71, 1, 'Nepal', 'NP', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (72, 1, 'Nicaragua', 'NI', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (73, 1, 'Niger', 'NE', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (74, 1, 'Nigeria', 'NG', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (75, 1, 'Niue', 'NU', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (76, 1, 'Norway', 'NO', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (77, 1, 'New Caledonia', 'NC', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (78, 1, 'New Zealand', 'NZ', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (79, 1, 'Oman', 'OM', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (81, 1, 'Pakistan', 'PK', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (82, 1, 'Palau', 'PW', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (83, 1, 'Palestine', 'PS', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (84, 1, 'Panama', 'PA', 'E', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (85, 1, 'Papua New Guinea', 'PG', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (86, 1, 'Paraguay', 'PY', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (87, 1, 'Peru', 'PE', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (89, 1, 'Poland', 'PL', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (90, 1, 'Portugal', 'PT', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (91, 1, 'Puerto Rico', 'PR', 'E', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (92, 1, 'Qatar', 'QA', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (94, 1, 'Czech Republic', 'CZ', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (95, 1, 'Dominican Republic', 'DO', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (96, 1, 'Réunion', 'RE', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (97, 1, 'Rwanda', 'RW', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (98, 1, 'Romania', 'RO', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (99, 1, 'Russia', 'RU', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (100, 1, 'Western Sahara', 'EH', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (101, 1, 'Samoa', 'WS', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (102, 1, 'American Samoa', 'AS', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (103, 1, 'Saint Barthélemy', 'BL', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (104, 1, 'Saint Kitts and Nevis', 'KN', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (105, 1, 'San Marino', 'SM', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (106, 1, 'Saint Martin (French part)', 'MF', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (108, 1, 'Saint Vincent and the Grenadines', 'VC', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (109, 1, 'Ascensión y Tristán de Acuña', 'SH', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (110, 1, 'Saint Lucia', 'LC', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (1, 1, 'Eritrea', 'ER', 'M', 'E', 'f', '2020-02-28 17:03:56-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (113, 1, 'Sierra Leone', 'SL', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (114, 1, 'Singapore', 'SG', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (115, 1, 'Syria', 'SY', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (116, 1, 'Somalia', 'SO', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (117, 1, 'Sri lanka', 'LK', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (118, 1, 'South Africa', 'ZA', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (119, 1, 'Sudan', 'SD', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (120, 1, 'Sweden', 'SE', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (121, 1, 'Switzerland', 'CH', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (122, 1, 'Suriname', 'SR', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (123, 1, 'Svalbard and Jan Mayen', 'SJ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (124, 1, 'Swaziland', 'SZ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (125, 1, 'Tajikistan', 'TJ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (126, 1, 'Thailand', 'TH', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (127, 1, 'Taiwan', 'TW', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (130, 1, 'French Southern and Antarctic Lands', 'TF', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (131, 1, 'East Timor', 'TL', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (132, 1, 'Tokelau', 'TK', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (133, 1, 'Tonga', 'TO', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (134, 1, 'Trinidad and Tobago', 'TT', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (135, 1, 'Tunisia', 'TN', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (136, 1, 'Turkmenistan', 'TM', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (137, 1, 'Turkey', 'TR', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (138, 1, 'Tuvalu', 'TV', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (140, 1, 'Uganda', 'UG', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (141, 1, 'Uruguay', 'UY', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (142, 1, 'Uzbekistan', 'UZ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (143, 1, 'Vanuatu', 'VU', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (144, 1, 'Venezuela', 'VE', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (145, 1, 'Vietnam', 'VN', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (146, 1, 'Yemen', 'YE', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (147, 1, 'Djibouti', 'DJ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (148, 1, 'Zambia', 'ZM', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (149, 1, 'Zimbabwe', 'ZW', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (151, 1, 'Myanmar', 'MM', 'E', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (152, 1, 'Bahamas', 'BS', 'E', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (153, 1, 'Barbados', 'BB', 'E', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (154, 1, 'Jamaica', 'JM', 'E', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (155, 1, 'United Kingdom', 'GB', 'E', 'K', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (156, 1, 'Ireland', 'IE', 'E', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (157, 1, 'Argentina', 'AR', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (158, 1, 'Armenia', 'AM', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (159, 1, 'Botswana', 'BW', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (160, 1, 'Congo', 'CG', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (161, 1, 'United States Minor Outlying Islands', 'UM', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (162, 1, 'Afghanistan', 'AF', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (163, 1, 'Albania', 'AL', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (164, 1, 'Germany', 'DE', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (165, 1, 'Algeria', 'DZ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (166, 1, 'Andorra', 'AD', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (167, 1, 'Angola', 'AO', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (168, 1, 'Anguilla', 'AI', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (170, 1, 'Aruba', 'AW', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (171, 1, 'Australia', 'AU', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (172, 1, 'Austria', 'AT', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (173, 1, 'Azerbaijan', 'AZ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (174, 1, 'Belgium', 'BE', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (175, 1, 'Bahrain', 'BH', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (176, 1, 'Bangladesh', 'BD', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (177, 1, 'Belize', 'BZ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (178, 1, 'Benin', 'BJ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (179, 1, 'Bhutan', 'BT', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (180, 1, 'Belarus', 'BY', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (181, 1, 'Bolivia', 'BO', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (182, 1, 'Bosnia and Herzegovina', 'BA', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (183, 1, 'Brazil', 'BR', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (184, 1, 'Brunei', 'BN', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (185, 1, 'Bulgaria', 'BG', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (187, 1, 'Burundi', 'BI', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (188, 1, 'Cape Verde', 'CV', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (189, 1, 'Cambodia', 'KH', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (190, 1, 'Cameroon', 'CM', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (191, 1, 'Canada', 'CA', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (192, 1, 'Chad', 'TD', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (193, 1, 'Chile', 'CL', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (194, 1, 'China', 'CN', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (195, 1, 'Cyprus', 'CY', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (197, 1, 'Colombia', 'CO', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (198, 1, 'Comoros', 'KM', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (199, 1, 'North Korea', 'KP', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (201, 1, 'Ivory Coast', 'CI', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (202, 1, 'Costa Rica', 'CR', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (203, 1, 'Croatia', 'HR', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (204, 1, 'Cuba', 'CU', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (205, 1, 'Denmark', 'DK', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (206, 1, 'Dominica', 'DM', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (207, 1, 'Ecuador', 'EC', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (208, 1, 'Egypt', 'EG', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (209, 1, 'El Salvador', 'SV', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (210, 1, 'Japan', 'JP', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (211, 1, 'Slovakia', 'SK', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (213, 1, 'Estonia', 'EE', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (214, 1, 'Ethiopia', 'ET', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (215, 1, 'Philippines', 'PH', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (216, 1, 'Finland', 'FI', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (217, 1, 'Fiji', 'FJ', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (218, 1, 'Gabon', 'GA', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (219, 1, 'Georgia', 'GE', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (220, 1, 'Ghana', 'GH', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (221, 1, 'Gibraltar', 'GI', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (222, 1, 'Grenada', 'GD', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (223, 1, 'Greece', 'GR', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (224, 1, 'Guadeloupe', 'GP', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (226, 1, 'Guatemala', 'GT', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (112, 1, 'Seychelles', 'SC', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (33, 1, 'Turks and Caicos Islands', 'TC', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (44, 1, 'Lesotho', 'LS', 'M', 'E', 'f', '2020-02-28 17:03:57-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (57, 1, 'Malawi', 'MW', 'M', 'E', 'f', '2020-02-28 17:03:58-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (67, 1, 'Montserrat', 'MS', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (80, 1, 'Netherlands', 'NL', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (88, 1, 'French Polynesia', 'PF', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (93, 1, 'Central African Republic', 'CF', 'M', 'E', 'f', '2020-02-28 17:03:59-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (107, 1, 'Saint Pierre and Miquelon', 'PM', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (111, 1, 'Sao Tome and Principe', 'ST', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (128, 1, 'Tanzania', 'TZ', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (129, 1, 'British Indian Ocean Territory', 'IO', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (139, 1, 'Ukraine', 'UA', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (150, 1, 'United States of America', 'US', 'E', 'U', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (169, 1, 'Antigua and Barbuda', 'AG', 'M', 'E', 'f', '2020-02-28 17:04:00-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (186, 1, 'Burkina Faso', 'BF', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (196, 1, 'Vatican City State', 'VA', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (200, 1, 'South Korea', 'KR', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (212, 1, 'Slovenia', 'SI', 'M', 'E', 'f', '2020-02-28 17:04:01-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (225, 1, 'Guam', 'GU', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (227, 1, 'French Guiana', 'GF', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (228, 1, 'Guernsey', 'GG', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (229, 1, 'Guinea-Bissau', 'GW', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (230, 1, 'Honduras', 'HN', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (231, 1, 'Hungary', 'HU', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (232, 1, 'Indonesia', 'ID', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (233, 1, 'Iran', 'IR', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (234, 1, 'Cayman Islands', 'KY', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (235, 1, 'Bonaire', 'BQ', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (236, 1, 'Palestinian Territories', 'PS', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (237, 1, 'Curacao', 'CW', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
INSERT INTO "dinbog"."country" ("id", "language_id", "name", "iso_code", "unit_measurement", "wear_measurement", "is_onu", "created", "updated") VALUES (238, 1, 'Kosovo', 'XK', 'M', 'E', 'f', '2020-02-28 17:04:02-04', NULL);
COMMIT;

-- ----------------------------
-- Table structure for event
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event" CASCADE;
CREATE TABLE "dinbog"."event" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_id_seq'::regclass),
  "event_type_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "event_status_id" int4 NOT NULL,
  "country_id" int4,
  "city_id" int4,
  "cover_attachment" varchar(255) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "name_event" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "publish_date" timestamp(0),
  "start_date_event" timestamp(0),
  "end_date_event" timestamp(0),
  "announcement_date_event" timestamp(0),
  "count_participant" int4 DEFAULT 0,
  "positionx" float4 NOT NULL,
  "positiony" float4 NOT NULL,
  "profile_type_id" int4 NOT NULL,
  "age_from" int2,
  "age_to" int2,
  "participant_private" int2 DEFAULT 0,
  "created" timestamptz(6) DEFAULT now(),
  "updated" timestamp(0)
)
;

-- ----------------------------
-- Records of event
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."event" ("id", "event_type_id", "owner_profile_id", "event_status_id", "country_id", "city_id", "cover_attachment", "address", "name_event", "description", "publish_date", "start_date_event", "end_date_event", "announcement_date_event", "count_participant", "positionx", "positiony", "profile_type_id", "age_from", "age_to", "participant_private", "created", "updated") VALUES (1, 2, 1, 1, 144, 1, 'cover attachment que guarda?', 'Av. principal de las mercedes', 'Calendario Abr 2020', 'Calendario desfile de modasa abr 2020 ', '2020-04-26 19:22:20', '2020-04-26 19:22:20', '2020-04-26 19:22:20', '2020-04-26 19:22:20', 0, 10.2, 11.2, 1, 20, 30, 0, '2020-04-28 12:11:46.68-04', '2020-04-28 16:11:47');
INSERT INTO "dinbog"."event" ("id", "event_type_id", "owner_profile_id", "event_status_id", "country_id", "city_id", "cover_attachment", "address", "name_event", "description", "publish_date", "start_date_event", "end_date_event", "announcement_date_event", "count_participant", "positionx", "positiony", "profile_type_id", "age_from", "age_to", "participant_private", "created", "updated") VALUES (212, 1, 1, 1, 144, 1, 'cover attachment', 'x hay', 'EVENT 01', 'event 01', '2020-06-07 21:56:24', '2020-06-07 21:56:24', '2020-06-07 21:56:24', '2020-06-07 21:56:24', 2, 10.2, 11.2, 1, 20, 30, 50, '2020-06-07 21:56:23.714-04', '2020-06-07 21:56:24');
INSERT INTO "dinbog"."event" ("id", "event_type_id", "owner_profile_id", "event_status_id", "country_id", "city_id", "cover_attachment", "address", "name_event", "description", "publish_date", "start_date_event", "end_date_event", "announcement_date_event", "count_participant", "positionx", "positiony", "profile_type_id", "age_from", "age_to", "participant_private", "created", "updated") VALUES (219, 1, 1, 1, 144, 1, 'cover attachment', 'x hay', 'EVENT 01', 'event 01', '2020-06-10 10:43:40', '2020-06-10 10:43:40', '2020-06-10 10:43:40', '2020-06-10 10:43:40', 2, 10.2, 11.2, 1, 20, 30, 50, '2020-06-10 10:43:40.146-04', '2020-06-10 10:43:40');
INSERT INTO "dinbog"."event" ("id", "event_type_id", "owner_profile_id", "event_status_id", "country_id", "city_id", "cover_attachment", "address", "name_event", "description", "publish_date", "start_date_event", "end_date_event", "announcement_date_event", "count_participant", "positionx", "positiony", "profile_type_id", "age_from", "age_to", "participant_private", "created", "updated") VALUES (220, 1, 1, 1, 144, 1, 'cover attachment', 'x hay', 'EVENT 01', 'event 01', '2020-06-10 20:24:25', '2020-06-10 20:24:25', '2020-06-10 20:24:25', '2020-06-10 20:24:25', 2, 10.2, 11.2, 1, 20, 30, 50, '2020-06-10 20:24:24.667-04', '2020-06-10 20:24:25');
COMMIT;

-- ----------------------------
-- Table structure for event_attachment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_attachment" CASCADE;
CREATE TABLE "dinbog"."event_attachment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_attachment_id_seq'::regclass),
  "event_id" int4,
  "attachment_id" int4
)
;

-- ----------------------------
-- Table structure for event_filter
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_filter" CASCADE;
CREATE TABLE "dinbog"."event_filter" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_filter_id_seq'::regclass),
  "event_id" int4 NOT NULL,
  "option_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for event_invitation
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_invitation" CASCADE;
CREATE TABLE "dinbog"."event_invitation" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_invitation_id_seq'::regclass),
  "event_id" int2,
  "profile_id" int4,
  "event_role_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for event_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_notification" CASCADE;
CREATE TABLE "dinbog"."event_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4,
  "message" text COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of event_notification
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (14, 200, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 00:04:06.864149-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (15, 201, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 00:11:46.708922-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (16, 202, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 00:27:36.39596-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (17, 203, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 00:34:05.572709-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (18, 204, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 01:15:39.449789-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (19, 205, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 01:28:38.862609-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (20, 206, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 01:35:28.084773-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (21, 207, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 02:00:56.13404-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (22, 208, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 02:19:59.415562-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (23, 209, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 02:41:17.748035-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (24, 210, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 02:46:40.954732-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (25, 211, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-06 17:14:07.507418-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (26, 212, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-07 21:56:23.585132-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (27, 213, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-07 22:05:40.157769-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (28, 214, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-07 22:11:59.717623-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (29, 215, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-07 22:33:03.194765-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (30, 216, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-07 22:48:24.669622-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (31, 217, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-08 06:01:30.908385-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (32, 218, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-08 19:26:02.463747-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (33, 219, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-10 10:43:40.147839-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (34, 220, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-10 20:24:24.668954-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (35, 221, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-10 23:54:14.677063-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (36, 222, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 00:13:25.184024-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (37, 223, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 00:21:00.922468-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (38, 224, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 01:54:37.553478-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (39, 225, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 02:02:23.311968-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (40, 226, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 02:13:52.212645-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (41, 227, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 02:38:09.053516-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (42, 228, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-11 02:42:47.001963-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (43, 229, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-14 18:38:28.612637-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (44, 230, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-15 01:29:30.584086-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (45, 231, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-15 01:42:30.934375-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (46, 232, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-15 01:49:58.587068-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (47, 233, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-15 11:52:24.456389-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (48, 234, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-15 11:57:49.653147-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (50, 236, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-16 04:15:14.321331-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (51, 237, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-16 07:40:40.759415-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (52, 238, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-16 10:49:23.069711-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (53, 239, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-16 11:03:31.281735-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (55, 241, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 07:18:49.072108-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (56, 242, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 07:22:26.280682-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (57, 243, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 07:23:51.531986-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (58, 244, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 07:30:13.298795-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (59, 245, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 07:35:16.695988-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (60, 246, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 11:51:36.944813-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (61, 247, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-18 11:58:42.202475-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (63, 249, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-19 14:04:58.462337-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (64, 250, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-19 16:24:50.596991-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (65, 251, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-19 16:31:48.506954-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (66, 252, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-19 16:43:11.262338-04');
INSERT INTO "dinbog"."event_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (67, 253, 1, 1, NULL, 'trigger EVENT CHANGE STATUS-> user_owner VALUE:(1) TABLA INFO:event_wait', '2020-06-19 16:47:06.117344-04');
COMMIT;

-- ----------------------------
-- Table structure for event_participant
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_participant" CASCADE;
CREATE TABLE "dinbog"."event_participant" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_participant_id_seq'::regclass),
  "event_id" int4,
  "profile_id" int4,
  "event_role_id" int4 NOT NULL,
  "event_participant_status_id" int4,
  "confirmed_date" timestamp(0),
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for event_participant_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_participant_status" CASCADE;
CREATE TABLE "dinbog"."event_participant_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_participant_status_id_seq'::regclass),
  "event_type_id" int4 NOT NULL,
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of event_participant_status
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."event_participant_status" ("id", "event_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (8, 1, 'Waiting', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 20:07:54.226877-04');
INSERT INTO "dinbog"."event_participant_status" ("id", "event_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (9, 1, 'Accepted', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 20:07:54.226877-04');
INSERT INTO "dinbog"."event_participant_status" ("id", "event_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (10, 1, 'Denied', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 20:07:54.226877-04');
INSERT INTO "dinbog"."event_participant_status" ("id", "event_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (11, 1, 'Restricted', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 20:07:54.226877-04');
INSERT INTO "dinbog"."event_participant_status" ("id", "event_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (12, 1, 'Canceled', NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-22 20:07:54.226877-04');
COMMIT;

-- ----------------------------
-- Table structure for event_profile_category_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_profile_category_type" CASCADE;
CREATE TABLE "dinbog"."event_profile_category_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_profile_category_type_id_seq'::regclass),
  "event_id" int4,
  "profile_category_type_id" int4
)
;

-- ----------------------------
-- Table structure for event_report
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_report" CASCADE;
CREATE TABLE "dinbog"."event_report" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_report_id_seq'::regclass),
  "report_type_id" int4,
  "detail" varchar(1000) COLLATE "pg_catalog"."default",
  "owner_profile_id" int4,
  "event_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for event_role
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_role" CASCADE;
CREATE TABLE "dinbog"."event_role" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_role_id_seq'::regclass),
  "event_type_id" int4,
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of event_role
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."event_role" ("id", "event_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (11, 1, 'Camarografo', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for event_share
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_share" CASCADE;
CREATE TABLE "dinbog"."event_share" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_share_id_seq'::regclass),
  "event_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "created" varchar(255) COLLATE "pg_catalog"."default" DEFAULT now()
)
;

-- ----------------------------
-- Table structure for event_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_status" CASCADE;
CREATE TABLE "dinbog"."event_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_status_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of event_status
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."event_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (1, 'Draft', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 15:59:07.454387-04');
INSERT INTO "dinbog"."event_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (2, 'Published', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 15:59:17.044871-04');
INSERT INTO "dinbog"."event_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (3, 'Approved', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 15:59:24.060464-04');
INSERT INTO "dinbog"."event_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (4, 'Denied', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 15:59:26.636969-04');
COMMIT;

-- ----------------------------
-- Table structure for event_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_type" CASCADE;
CREATE TABLE "dinbog"."event_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of event_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."event_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (1, 'Event', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."event_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (2, 'Casting', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."event_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (3, 'Casting Online', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."event_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (4, 'Contest', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for event_view
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."event_view" CASCADE;
CREATE TABLE "dinbog"."event_view" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".event_view_id_seq'::regclass),
  "event_id" int4 NOT NULL,
  "profile_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for field
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."field" CASCADE;
CREATE TABLE "dinbog"."field" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".field_id_seq'::regclass),
  "code" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of field
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (1, 'ht', 'hair texture');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (2, 'sf', 'special feature');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (3, 'hl', 'hair length');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (4, 'hc', 'hair color');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (5, 'sc', 'skin color');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (6, 'ec', 'eye color');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (7, 'bw', 'breast width');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (8, 'hw', 'hip width');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (9, 'ww', 'waist width');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (10, 'h', 'height');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (11, 'w', 'weight');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (12, 'ss', 'shoe size');
INSERT INTO "dinbog"."field" ("id", "code", "name") VALUES (13, 'ds', 'dress size');
COMMIT;

-- ----------------------------
-- Table structure for field_category
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."field_category" CASCADE;
CREATE TABLE "dinbog"."field_category" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".field_category_id_seq'::regclass),
  "field_id" int4 NOT NULL,
  "category_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of field_category
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."field_category" ("id", "field_id", "category_id") VALUES (1, 1, 1);
INSERT INTO "dinbog"."field_category" ("id", "field_id", "category_id") VALUES (2, 2, 2);
INSERT INTO "dinbog"."field_category" ("id", "field_id", "category_id") VALUES (3, 3, 3);
INSERT INTO "dinbog"."field_category" ("id", "field_id", "category_id") VALUES (4, 4, 4);
INSERT INTO "dinbog"."field_category" ("id", "field_id", "category_id") VALUES (5, 5, 5);
COMMIT;

-- ----------------------------
-- Table structure for gender
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."gender" CASCADE;
CREATE TABLE "dinbog"."gender" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".gender_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default",
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of gender
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."gender" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (1, 'male', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."gender" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (2, 'female', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."group" CASCADE;
CREATE TABLE "dinbog"."group" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".group_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "is_active" bool NOT NULL DEFAULT false,
  "created" timestamptz(0) DEFAULT now()
)
;

-- ----------------------------
-- Records of group
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."group" ("id", "name", "description", "is_active", "created") VALUES (1, 'ADMIN', 'Grupo de administradores', 't', '2020-04-11 16:30:36-04');
INSERT INTO "dinbog"."group" ("id", "name", "description", "is_active", "created") VALUES (2, 'PUBLIC', 'Grupo para publico en General', 't', '2020-04-11 16:31:26-04');
COMMIT;

-- ----------------------------
-- Table structure for group_menu
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."group_menu" CASCADE;
CREATE TABLE "dinbog"."group_menu" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".group_menu_id_seq'::regclass),
  "menu_id" int4 NOT NULL,
  "group_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."language" CASCADE;
CREATE TABLE "dinbog"."language" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".language_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "iso_code_language" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of language
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (1, 'African', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (2, 'Albanian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (3, 'German', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (4, 'Arabic', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (5, 'Armenian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (6, 'Belarusian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (7, 'Bosnian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (8, 'Bulgarian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (9, 'Czech', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (10, 'Chinese', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (11, 'Korean', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (12, 'Croatian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (13, 'Danish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (14, 'Scottish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (15, 'Español', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (16, 'Estonian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (17, 'Philippine', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (18, 'Finnish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (19, 'Fijian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (20, 'French', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (21, 'Wales', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (22, 'Greek', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (23, 'Haitian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (24, 'Hebrew', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (25, 'Hindu', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (26, 'Dutch', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (27, 'Hungarian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (28, 'Indonesian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (29, 'English', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (30, 'Irish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (31, 'Icelandic', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (32, 'Italian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (33, 'Japanese', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (34, 'Kinyarwanda', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (35, 'Kirundi', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (36, 'Kurdish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (37, 'Lithuanian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (38, 'Luxembourg', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (39, 'Malay', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (40, 'Malagasy', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (41, 'Maltese', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (42, 'Mandarin', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (43, 'Maori', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (44, 'Montenegrin', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (45, 'Nauruan', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (46, 'Norwegian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (47, 'Papiamentu', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (48, 'Polish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (49, 'Portuguese', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (50, 'Romanian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (51, 'Russian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (52, 'Serbian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (53, 'Setswana', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (54, 'Swahili', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (55, 'Swedish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (56, 'Thai', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (57, 'Turkish', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (58, 'Ukrainian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (59, 'Urdu', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (60, 'Uzbek', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (61, 'Vietnamese', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (62, 'Egyptian Arabic', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (63, 'Persian', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (64, 'Oromo', NULL);
INSERT INTO "dinbog"."language" ("id", "name", "iso_code_language") VALUES (65, 'Tigrinya', NULL);
COMMIT;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."log" CASCADE;
CREATE TABLE "dinbog"."log" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".log_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "is_error" bool NOT NULL DEFAULT false,
  "is_warning" bool NOT NULL DEFAULT false,
  "message" text COLLATE "pg_catalog"."default" NOT NULL,
  "created" timestamptz(0) NOT NULL DEFAULT now()
)
;

-- ----------------------------
-- Records of log
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (1, 1, 'f', 'f', 'prueba log', '2020-04-13 18:48:03-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (2, 1, 'f', 'f', 'prueba log 2', '2020-04-13 18:48:15-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (3, 1, 'f', 'f', 'prueba log 3', '2020-04-13 18:48:27-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (4, 1, 'f', 't', 'prueba mensaje warning', '2020-04-13 18:52:44-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (5, 1, 't', 'f', 'prueba mensaje error', '2020-04-13 18:54:23-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (175, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-12 15:15:40-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (56, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-04 13:25:02-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (176, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-12 15:26:18-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (173, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-12 10:00:53-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (174, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-12 10:08:40-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (178, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-13 19:13:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (179, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-13 19:18:58-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (180, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-13 19:45:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (181, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-13 19:50:06-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (182, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-13 20:09:56-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (184, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-13 20:25:39-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (186, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-14 13:59:43-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (187, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-14 14:05:23-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (188, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-14 15:06:49-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (189, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-14 16:01:00-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (190, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-15 14:03:47-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (191, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-17 21:35:57-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (192, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-17 22:41:10-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (193, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-17 22:45:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (194, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-17 23:28:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (195, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-17 23:30:46-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (196, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 04:29:18-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (197, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 05:00:31-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (198, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 16:04:51-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (199, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 16:17:14-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (200, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 17:07:02-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (201, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 17:11:56-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (202, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 17:42:17-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (203, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 17:45:55-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (204, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 17:48:57-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (205, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 18:06:56-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (206, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 18:44:01-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (207, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 19:06:34-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (208, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 20:22:51-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (209, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 20:37:59-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (210, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 20:43:19-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (211, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 20:45:46-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (212, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-18 20:57:51-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (213, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-20 18:06:40-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (214, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-20 18:45:18-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (215, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 08:52:08-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (216, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 09:03:37-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (217, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 09:08:55-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (218, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 09:15:51-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (219, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 09:24:32-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (220, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 09:34:42-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (221, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 10:23:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (222, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 20:20:18-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (223, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-21 20:28:27-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (225, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-22 22:31:19-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (226, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 00:59:24-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (227, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 01:19:20-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (228, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 01:24:47-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (229, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 02:32:11-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (230, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 02:46:15-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (231, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 03:19:33-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (232, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 04:42:53-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (233, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 04:46:34-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (234, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 15:23:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (235, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 16:09:34-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (236, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-23 18:51:53-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (237, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-24 18:54:02-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (239, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-24 19:49:25-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (240, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-24 19:56:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (241, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-24 20:21:10-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (242, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-24 23:20:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (243, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 00:12:34-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (244, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 00:22:25-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (245, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 01:51:52-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (246, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 17:10:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (247, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 17:22:37-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (248, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 17:35:22-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (249, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 17:38:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (250, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 17:53:25-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (251, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 17:57:01-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (252, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 18:06:38-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (253, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 18:10:00-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (254, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 19:14:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (255, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 19:26:04-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (256, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 19:43:04-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (257, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 20:00:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (258, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 20:18:04-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (259, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-25 20:21:16-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (260, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-31 11:22:39-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (261, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-31 11:26:27-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (262, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-31 11:30:39-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (263, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-05-31 11:34:18-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (264, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 02:55:05-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (265, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 03:16:20-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (266, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 03:40:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (267, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 04:00:21-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (268, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 04:20:22-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (269, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 04:49:24-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (270, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-02 18:03:52-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (271, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 08:57:06-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (272, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 09:34:02-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (273, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 09:49:47-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (274, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 10:13:01-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (275, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 10:32:28-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (276, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 10:34:00-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (277, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 10:51:40-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (278, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 11:41:42-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (279, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 11:43:05-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (280, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 11:47:53-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (281, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 11:51:45-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (282, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 12:03:31-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (283, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-03 12:10:57-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (284, 1, 'f', 'f', 'prueba log normal', '2020-06-03 21:54:25-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (285, 1, 't', 'f', 'prueba log error', '2020-06-03 21:54:38-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (286, 1, 'f', 't', 'prueba log warning', '2020-06-03 21:55:00-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (287, 1, 'f', 't', 'prueba log warning', '2020-06-03 21:55:50-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (288, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-04 01:28:58-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (289, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-04 01:34:22-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (290, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-04 01:45:58-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (291, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-04 02:03:43-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (292, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 02:42:48-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (293, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 10:21:32-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (294, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 11:10:31-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (295, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 13:02:07-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (296, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 14:13:31-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (297, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 14:21:23-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (298, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 15:22:04-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (299, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 16:44:05-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (300, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 17:03:42-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (301, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 17:13:57-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (302, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 17:32:03-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (303, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 18:47:40-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (304, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-05 20:18:02-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (306, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 00:04:08-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (307, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 00:11:48-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (308, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 00:27:37-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (309, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 00:34:07-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (310, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 01:15:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (311, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 01:28:40-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (312, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 01:35:29-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (313, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 02:00:57-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (314, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 02:20:01-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (315, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 02:41:19-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (316, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 02:46:42-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (317, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-06 17:14:09-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (318, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-07 21:56:24-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (319, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-07 22:05:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (320, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-07 22:12:01-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (321, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-07 22:33:05-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (322, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-07 22:48:26-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (323, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-08 06:01:32-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (324, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-08 19:26:04-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (325, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-10 10:43:41-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (326, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-10 20:24:19-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (327, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-10 23:54:16-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (328, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 00:13:26-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (329, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 00:21:02-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (330, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 01:54:39-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (331, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 02:02:24-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (332, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 02:13:53-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (333, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 02:38:10-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (334, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-11 02:42:48-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (335, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-14 18:38:30-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (336, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-15 01:29:33-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (337, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-15 01:42:32-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (338, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-15 01:50:00-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (339, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-15 11:52:26-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (340, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-15 11:57:51-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (342, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-16 04:15:16-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (343, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-16 07:40:42-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (344, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-16 10:49:24-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (345, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-16 11:03:32-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (347, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 07:18:50-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (348, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 07:22:27-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (349, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 07:23:53-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (350, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 07:30:14-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (351, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 07:35:18-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (352, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 11:51:38-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (353, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-18 11:58:44-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (355, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-19 14:05:03-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (356, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-19 16:24:55-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (357, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-19 16:31:52-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (358, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-19 16:43:15-04');
INSERT INTO "dinbog"."log" ("id", "user_id", "is_error", "is_warning", "message", "created") VALUES (359, 1, 'f', 'f', 'Mensaje comun para el log desde un @Test', '2020-06-19 16:47:11-04');
COMMIT;

-- ----------------------------
-- Table structure for membership
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."membership" CASCADE;
CREATE TABLE "dinbog"."membership" (
  "id" int2 NOT NULL DEFAULT nextval('"dinbog".membership_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of membership
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."membership" ("id", "name", "created") VALUES (1, 'master', '2020-04-10 09:58:23-04');
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."menu" CASCADE;
CREATE TABLE "dinbog"."menu" (
  "id" int2 NOT NULL DEFAULT nextval('"dinbog".menu_id_seq'::regclass),
  "route" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "is_available" bool,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."menu" ("id", "route", "name", "is_available", "created") VALUES (4, '/test1', 'ruta test', 't', '2020-04-11 16:33:42-04');
INSERT INTO "dinbog"."menu" ("id", "route", "name", "is_available", "created") VALUES (5, '/test2', 'ruta test 2', 't', '2020-04-11 16:33:56-04');
INSERT INTO "dinbog"."menu" ("id", "route", "name", "is_available", "created") VALUES (6, '/admin1', 'ruta admin 1', 't', '2020-04-11 16:35:05-04');
INSERT INTO "dinbog"."menu" ("id", "route", "name", "is_available", "created") VALUES (7, '/admin2', 'ruta admin 2', 't', '2020-04-11 16:35:20-04');
COMMIT;

-- ----------------------------
-- Table structure for notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."notification" CASCADE;
CREATE TABLE "dinbog"."notification" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".notification_id_seq'::regclass),
  "owner_user_id" int4 NOT NULL,
  "to_user_id" int4,
  "notification_action_id" int4,
  "event_notification_id" int4,
  "profile_notification_id" int4,
  "post_notification_id" int4,
  "attachment_notification_id" int4,
  "poll_notification_id" int4,
  "album_notification_id" int4,
  "user_notification_id" int4,
  "conversation_notification_id" int4,
  "date_notification" timestamptz(0) NOT NULL,
  "is_read" int2 DEFAULT 0,
  "date_read" timestamp(0),
  "code_message" varchar COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of notification
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (551, 1, 1, 37, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-10 10:43:40-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (552, 1, 1, 37, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-10 20:24:25-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (553, 1, 1, 37, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-10 23:54:15-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (614, 1, 1, 37, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-15 11:52:24-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (557, 1, 1, 37, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 00:13:25-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (562, 1, 1, 37, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 00:21:01-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (567, 1, 1, 37, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 01:54:38-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (571, 1, 1, 37, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 02:02:23-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (575, 1, 1, 37, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 02:13:52-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (580, 1, 1, 37, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 02:38:09-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (590, 1, 1, 37, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-14 18:38:29-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (602, 1, 1, 37, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-15 01:29:31-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (604, 1, 1, 37, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-15 01:42:31-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (625, 1, 1, 37, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 04:15:14-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (626, 1, 1, 37, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 07:40:41-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (631, 1, 1, 37, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-16 10:49:23-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (642, 1, 1, 37, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-18 07:18:49-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (643, 1, 1, 37, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-18 07:22:26-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (644, 1, 1, 37, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-18 07:23:52-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (645, 1, 1, 37, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-18 07:30:13-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (646, 1, 1, 37, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-18 07:35:17-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (652, 1, 1, 37, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-18 11:51:37-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (663, 1, 1, 37, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 14:04:58-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (668, 1, 1, 37, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 16:24:51-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (673, 1, 1, 37, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 16:31:49-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (678, 1, 1, 37, 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 16:43:11-04', 0, NULL, '26');
INSERT INTO "dinbog"."notification" ("id", "owner_user_id", "to_user_id", "notification_action_id", "event_notification_id", "profile_notification_id", "post_notification_id", "attachment_notification_id", "poll_notification_id", "album_notification_id", "user_notification_id", "conversation_notification_id", "date_notification", "is_read", "date_read", "code_message") VALUES (679, 1, 1, 37, 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 16:47:06-04', 0, NULL, '26');
COMMIT;

-- ----------------------------
-- Table structure for notification_action
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."notification_action" CASCADE;
CREATE TABLE "dinbog"."notification_action" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".notification_action_id_seq'::regclass),
  "notification_type_id" int4 NOT NULL,
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "key" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of notification_action
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (22, 7, 'comment like', NULL, NULL, NULL, NULL, NULL, NULL, 'attachment_comment_like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (23, 7, 'comment mention', NULL, NULL, NULL, NULL, NULL, NULL, 'attachment_comment_mention');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (24, 2, 'comment like', NULL, NULL, NULL, NULL, NULL, NULL, 'post_comment_like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (25, 2, 'comment mention', NULL, NULL, NULL, NULL, NULL, NULL, 'post_comment_mention');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (26, 4, 'comment like', NULL, NULL, NULL, NULL, NULL, NULL, 'album_comment_like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (27, 4, 'comment mention', NULL, NULL, NULL, NULL, NULL, NULL, 'album_comment_mention');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (28, 3, 'participant acepted', NULL, NULL, NULL, NULL, NULL, NULL, 'event_participant_acepted');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (29, 3, 'participant profile connection', NULL, NULL, NULL, NULL, NULL, NULL, 'event_participant_profile_connection');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (31, 3, 'participant attendance confirmed', NULL, NULL, NULL, NULL, NULL, NULL, 'event_participant_confirmed_attendance');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (32, 1, 'request connection accepted', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_request_connection_accepted');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (33, 1, 'profile request connection cancelled', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_request_connection_cancelled');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (36, 1, 'profile request connection reject', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_request_connection_denied');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (37, 3, 'event wait', NULL, NULL, NULL, NULL, NULL, NULL, 'event_wait');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (38, 3, 'event approved', NULL, NULL, NULL, NULL, NULL, NULL, 'event_approved');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (39, 3, 'event denied', NULL, NULL, NULL, NULL, NULL, NULL, 'event_denied');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (1, 1, 'like', NULL, NULL, NULL, NULL, NULL, NULL, 'like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (8, 1, 'follow', NULL, NULL, NULL, NULL, NULL, NULL, 'follow');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (11, 1, 'request connection', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_request_connection');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (12, 2, 'like', NULL, NULL, NULL, NULL, NULL, NULL, 'like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (13, 2, 'comment', NULL, NULL, NULL, NULL, NULL, NULL, 'comment');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (14, 2, 'mention', NULL, NULL, NULL, NULL, NULL, NULL, 'mention');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (15, 4, 'like', NULL, NULL, NULL, NULL, NULL, NULL, 'like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (16, 4, 'comment', NULL, NULL, NULL, NULL, NULL, NULL, 'comment');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (17, 3, 'invitation', NULL, NULL, NULL, NULL, NULL, NULL, 'event_invitation');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (18, 3, 'participant', NULL, NULL, NULL, NULL, NULL, NULL, 'event_participant');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (19, 7, 'like', NULL, NULL, NULL, NULL, NULL, NULL, 'attachment_like');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (20, 7, 'comment', NULL, NULL, NULL, NULL, NULL, NULL, 'attachment_comment');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (21, 5, 'participant', NULL, NULL, NULL, NULL, NULL, NULL, 'poll_participant');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (40, 5, 'vote', NULL, NULL, NULL, NULL, NULL, NULL, 'poll_participant_vote');
INSERT INTO "dinbog"."notification_action" ("id", "notification_type_id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "key") VALUES (10, 1, 'recommendation', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_recommendation');
COMMIT;

-- ----------------------------
-- Table structure for notification_message
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."notification_message" CASCADE;
CREATE TABLE "dinbog"."notification_message" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".notification_message_id_seq'::regclass),
  "notification_messages_type" int4 NOT NULL,
  "code_message" varchar(3) COLLATE "pg_catalog"."default" NOT NULL,
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "notification_action_id" int4
)
;

-- ----------------------------
-- Records of notification_message
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (41, 7, '27', 'We have successfully verified the  {event_type_name} {event_name} {link_event}', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (42, 7, '28', 'Your  {event_type_name} {event_name} {link_event}  doesn‘t meet some of the requirements to start the profile search', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (15, 7, '2', '{profile_name} {link_owner_profile} likes your profile', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (25, 7, '12', '{profile_name} {link_owner_profile} is now following you', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (30, 7, '17', '{profile_name} {link_owner_profile} sent you a connection request', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (14, 7, '1', '{profile_name} {link_owner_profile} likes your post {link_post}', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (21, 7, '8', '{profile_name} {link_owner_profile} comment your post {link_post}', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (29, 7, '16', '{profile_name} {link_owner_profile} mentioned you in a post {link_post}', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (17, 7, '4', '{profile_name} {link_owner_profile} likes your {album_name} {link_album}', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (22, 7, '9', '{profile_name} {link_owner_profile} comment your {album_type_name} {link_album}', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (31, 7, '18', '{profile_name} {link_owner_profile} has postulated to your casting {event_type_name} {event_name} {link_event} ', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (28, 7, '15', '{profile_name} {link_owner_profile} mentioned you in a comment {link_comment_attachment} post', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (32, 7, '19', 'Congratulations! You were selected to attend the {event_type_name} {event_name} {link_event}. Confirm your attendance', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (34, 7, '21', 'Talent name {profile_name} {link_owner_profile} confirmed her/his attendance to the {event_type_name} {event_name} {link_event}', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (33, 7, '20', '{profile_name} {link_owner_profile} member of your team, has postulated to the {event_type_name} {event_name} {link_event} ', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (36, 7, '22', '{profile_name} {link_owner_profile} has confirmed the request', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (37, 7, '23', '{profile_name} {link_owner_profile} has rejected the request, We suggest contacting them and try again', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (38, 7, '24', '{profile_name} {link_owner_profile} has ended the partnership, We suggest contacting them and try again
', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (39, 7, '25', 'Congratulations! You are invited {event_type_name} {event_name} {link_event}. Confirm your attendance', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (40, 7, '26', 'Soon you''ll be receiving news about the status of the {event_type_name} {event_name} {link_event}', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (23, 7, '10', '{profile_name} {link_owner_profile} mentioned you in a comment {link_album_comment_mention} in your {album_type_name}', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (18, 7, '5', '{profile_name} {link_owner_profile} likes your {attachment_type_value} {link_attachment} in your {entity_type} {link_entity}', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (20, 7, '7', '{profile_name} {link_owner_profile} likes your comment {link_comment_attachment} in your {attachment_type_value}', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (19, 7, '6', '{profile_name} {link_owner_profile} comment your {attachmeny_type_value} {link_attachment} on {entity_type} {link_entity}
', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (16, 7, '3', '{profile_name} {link_owner_profile} likes your comment {link_album_comment} in your {album_type_name}', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (27, 7, '14', '{profile_name} {link_owner_profile} likes your comment {link_post_comment} in your post', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (24, 7, '11', '{profile_name} {link_owner_profile} mentioned you in a comment {link_comment_attachment} in your {attachment_type_value}', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."notification_message" ("id", "notification_messages_type", "code_message", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "notification_action_id") VALUES (45, 7, '29', '{profile_name} {link_owner_profile} recommend you {link_profile_recommendation}', NULL, NULL, NULL, NULL, NULL, NULL, 10);
COMMIT;

-- ----------------------------
-- Table structure for notification_messages_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."notification_messages_type" CASCADE;
CREATE TABLE "dinbog"."notification_messages_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".notification_messages_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of notification_messages_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."notification_messages_type" ("id", "value", "created") VALUES (7, 'notification', '2020-05-06 01:20:11.229461-04');
INSERT INTO "dinbog"."notification_messages_type" ("id", "value", "created") VALUES (8, 'email', '2020-05-06 01:20:14.071927-04');
COMMIT;

-- ----------------------------
-- Table structure for notification_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."notification_type" CASCADE;
CREATE TABLE "dinbog"."notification_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".notification_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of notification_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (1, 'profile', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (2, 'post', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (3, 'event', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (4, 'album', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (5, 'poll', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (6, 'conversation', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."notification_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (7, 'attachment', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for option
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."option" CASCADE;
CREATE TABLE "dinbog"."option" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".option_id_seq'::regclass),
  "field_id" int4 NOT NULL,
  "order_by" varchar(255) COLLATE "pg_catalog"."default",
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "id_value_old" int4
)
;

-- ----------------------------
-- Records of option
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (2, 1, NULL, 'Smooth', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (3, 1, NULL, 'Wavey', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (4, 1, NULL, 'Curly', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (5, 2, NULL, 'Vitiligo', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (6, 2, NULL, 'Ink Models', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (7, 2, NULL, 'Physical Disability', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (8, 2, NULL, 'Androgynous', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (9, 2, NULL, 'Albino', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (10, 2, NULL, 'Body Modification', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (11, 3, NULL, 'Short', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (12, 3, NULL, 'Medium', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (13, 3, NULL, 'Long', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (14, 3, NULL, 'Bald', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (15, 4, NULL, 'Blond', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (16, 4, NULL, 'Light Blond', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (17, 4, NULL, 'Dark Blond', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (18, 4, NULL, 'Chesnut', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (19, 4, NULL, 'Light Chesnut', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (20, 4, NULL, 'Dark Chesnut', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (21, 4, NULL, 'Red', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (22, 4, NULL, 'Mahogany', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (23, 4, NULL, 'Black', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (24, 4, NULL, 'Grey / White', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (25, 5, NULL, 'Caucasian', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (26, 5, NULL, 'Hispanic', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (27, 5, NULL, 'Afro-American', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (28, 5, NULL, 'Afro-Asian', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (29, 5, NULL, 'Asian', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (30, 5, NULL, 'Asian American', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (31, 5, NULL, 'Aboriginal', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (32, 5, NULL, 'Eastern European', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (33, 5, NULL, 'African', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (34, 5, NULL, 'Indian', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (35, 5, NULL, 'Middle Eastern', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (36, 5, NULL, 'Native American', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (37, 5, NULL, 'Polynesian', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (38, 5, NULL, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (39, 6, NULL, 'Honey', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (40, 6, NULL, 'Blue', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (41, 6, NULL, 'Grey', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (42, 6, NULL, 'Brown', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (43, 6, NULL, 'Black', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (44, 6, NULL, 'Green', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (45, 7, NULL, 'E:2559|M:650', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (46, 7, NULL, 'E:2598|M:660', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (47, 7, NULL, 'E:2638|M:670', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (48, 7, NULL, 'E:2677|M:680', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (49, 7, NULL, 'E:2717|M:690', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (50, 7, NULL, 'E:2756|M:700', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (51, 7, NULL, 'E:2795|M:710', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (52, 7, NULL, 'E:2835|M:720', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (53, 7, NULL, 'E:2874|M:730', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (54, 7, NULL, 'E:2913|M:740', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (55, 7, NULL, 'E:2953|M:750', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (56, 7, NULL, 'E:2992|M:760', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (57, 7, NULL, 'E:3031|M:770', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (58, 7, NULL, 'E:3071|M:780', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (59, 7, NULL, 'E:311|M:790', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (60, 7, NULL, 'E:315|M:800', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (61, 7, NULL, 'E:3189|M:810', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (62, 7, NULL, 'E:3228|M:820', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (63, 7, NULL, 'E:3268|M:830', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (64, 7, NULL, 'E:3307|M:840', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (65, 7, NULL, 'E:3346|M:850', NULL, NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (66, 7, NULL, 'E:3386|M:860', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (67, 7, NULL, 'E:3425|M:870', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (68, 7, NULL, 'E:3465|M:880', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (69, 7, NULL, 'E:3504|M:890', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (70, 7, NULL, 'E:3543|M:900', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (71, 7, NULL, 'E:3583|M:910', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (72, 7, NULL, 'E:3622|M:920', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (73, 7, NULL, 'E:3661|M:930', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (74, 7, NULL, 'E:3701|M:940', NULL, NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (75, 7, NULL, 'E:374|M:950', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (76, 7, NULL, 'E:378|M:960', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (77, 7, NULL, 'E:3819|M:970', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (78, 7, NULL, 'E:3858|M:980', NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (79, 7, NULL, 'E:3898|M:990', NULL, NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (80, 7, NULL, 'E:3937|M:1000', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (81, 7, NULL, 'E:3976|M:1010', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (82, 7, NULL, 'E:4016|M:1020', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (83, 7, NULL, 'E:4055|M:1030', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (84, 7, NULL, 'E:4094|M:1040', NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (85, 7, NULL, 'E:4134|M:1050', NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (86, 7, NULL, 'E:4173|M:1060', NULL, NULL, NULL, NULL, NULL, NULL, 42);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (87, 7, NULL, 'E:4213|M:1070', NULL, NULL, NULL, NULL, NULL, NULL, 43);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (88, 7, NULL, 'E:4252|M:1080', NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (89, 7, NULL, 'E:4291|M:1090', NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (90, 7, NULL, 'E:4331|M:1100', NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (91, 7, NULL, 'E:437|M:1110', NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (92, 7, NULL, 'E:4409|M:1120', NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (93, 7, NULL, 'E:4449|M:1130', NULL, NULL, NULL, NULL, NULL, NULL, 49);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (94, 7, NULL, 'E:4488|M:1140', NULL, NULL, NULL, NULL, NULL, NULL, 50);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (95, 7, NULL, 'E:4528|M:1150', NULL, NULL, NULL, NULL, NULL, NULL, 51);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (96, 7, NULL, 'E:4567|M:1160', NULL, NULL, NULL, NULL, NULL, NULL, 52);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (97, 7, NULL, 'E:4606|M:1170', NULL, NULL, NULL, NULL, NULL, NULL, 53);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (98, 7, NULL, 'E:4646|M:1180', NULL, NULL, NULL, NULL, NULL, NULL, 54);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (99, 7, NULL, 'E:4685|M:1190', NULL, NULL, NULL, NULL, NULL, NULL, 55);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (100, 7, NULL, 'E:4724|M:1200', NULL, NULL, NULL, NULL, NULL, NULL, 56);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (101, 7, NULL, 'E:4764|M:1210', NULL, NULL, NULL, NULL, NULL, NULL, 57);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (102, 7, NULL, 'E:4803|M:1220', NULL, NULL, NULL, NULL, NULL, NULL, 58);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (103, 7, NULL, 'E:4843|M:1230', NULL, NULL, NULL, NULL, NULL, NULL, 59);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (104, 7, NULL, 'E:4882|M:1240', NULL, NULL, NULL, NULL, NULL, NULL, 60);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (105, 7, NULL, 'E:4921|M:1250', NULL, NULL, NULL, NULL, NULL, NULL, 61);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (106, 7, NULL, 'E:4961|M:1260', NULL, NULL, NULL, NULL, NULL, NULL, 62);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (107, 7, NULL, 'E:500|M:1270', NULL, NULL, NULL, NULL, NULL, NULL, 63);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (108, 7, NULL, 'E:5039|M:1280', NULL, NULL, NULL, NULL, NULL, NULL, 64);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (109, 7, NULL, 'E:5079|M:1290', NULL, NULL, NULL, NULL, NULL, NULL, 65);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (110, 7, NULL, 'E:5118|M:1300', NULL, NULL, NULL, NULL, NULL, NULL, 66);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (111, 7, NULL, 'E:5157|M:1310', NULL, NULL, NULL, NULL, NULL, NULL, 67);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (112, 7, NULL, 'E:5197|M:1320', NULL, NULL, NULL, NULL, NULL, NULL, 68);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (113, 7, NULL, 'E:5236|M:1330', NULL, NULL, NULL, NULL, NULL, NULL, 69);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (114, 7, NULL, 'E:5276|M:1340', NULL, NULL, NULL, NULL, NULL, NULL, 70);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (115, 7, NULL, 'E:5315|M:1350', NULL, NULL, NULL, NULL, NULL, NULL, 71);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (116, 7, NULL, 'E:5354|M:1360', NULL, NULL, NULL, NULL, NULL, NULL, 72);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (117, 7, NULL, 'E:5394|M:1370', NULL, NULL, NULL, NULL, NULL, NULL, 73);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (118, 7, NULL, 'E:5433|M:1380', NULL, NULL, NULL, NULL, NULL, NULL, 74);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (119, 7, NULL, 'E:5472|M:1390', NULL, NULL, NULL, NULL, NULL, NULL, 75);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (120, 7, NULL, 'E:5512|M:1400', NULL, NULL, NULL, NULL, NULL, NULL, 76);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (121, 7, NULL, 'E:5551|M:1410', NULL, NULL, NULL, NULL, NULL, NULL, 77);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (122, 7, NULL, 'E:5591|M:1420', NULL, NULL, NULL, NULL, NULL, NULL, 78);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (123, 7, NULL, 'E:563|M:1430', NULL, NULL, NULL, NULL, NULL, NULL, 79);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (124, 7, NULL, 'E:5669|M:1440', NULL, NULL, NULL, NULL, NULL, NULL, 80);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (125, 7, NULL, 'E:5709|M:1450', NULL, NULL, NULL, NULL, NULL, NULL, 81);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (126, 7, NULL, 'E:5748|M:1460', NULL, NULL, NULL, NULL, NULL, NULL, 82);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (127, 7, NULL, 'E:5787|M:1470', NULL, NULL, NULL, NULL, NULL, NULL, 83);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (128, 7, NULL, 'E:5827|M:1480', NULL, NULL, NULL, NULL, NULL, NULL, 84);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (129, 7, NULL, 'E:5866|M:1490', NULL, NULL, NULL, NULL, NULL, NULL, 85);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (130, 7, NULL, 'E:5906|M:1500', NULL, NULL, NULL, NULL, NULL, NULL, 86);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (131, 8, NULL, 'E:2559|M:650', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (132, 8, NULL, 'E:2598|M:660', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (133, 8, NULL, 'E:2638|M:670', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (134, 8, NULL, 'E:2677|M:680', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (135, 8, NULL, 'E:2717|M:690', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (136, 8, NULL, 'E:2756|M:700', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (137, 8, NULL, 'E:2795|M:710', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (138, 8, NULL, 'E:2835|M:720', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (139, 8, NULL, 'E:2874|M:730', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (140, 8, NULL, 'E:2913|M:740', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (141, 8, NULL, 'E:2953|M:750', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (142, 8, NULL, 'E:2992|M:760', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (143, 8, NULL, 'E:3031|M:770', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (144, 8, NULL, 'E:3071|M:780', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (145, 8, NULL, 'E:311|M:790', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (146, 8, NULL, 'E:315|M:800', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (147, 8, NULL, 'E:3189|M:810', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (148, 8, NULL, 'E:3228|M:820', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (149, 8, NULL, 'E:3268|M:830', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (150, 8, NULL, 'E:3307|M:840', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (151, 8, NULL, 'E:3346|M:850', NULL, NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (152, 8, NULL, 'E:3386|M:860', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (153, 8, NULL, 'E:3425|M:870', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (154, 8, NULL, 'E:3465|M:880', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (155, 8, NULL, 'E:3504|M:890', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (156, 8, NULL, 'E:3543|M:900', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (157, 8, NULL, 'E:3583|M:910', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (158, 8, NULL, 'E:3622|M:920', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (159, 8, NULL, 'E:3661|M:930', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (160, 8, NULL, 'E:3701|M:940', NULL, NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (161, 8, NULL, 'E:374|M:950', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (162, 8, NULL, 'E:378|M:960', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (163, 8, NULL, 'E:3819|M:970', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (164, 8, NULL, 'E:3858|M:980', NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (165, 8, NULL, 'E:3898|M:990', NULL, NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (166, 8, NULL, 'E:3937|M:1000', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (167, 8, NULL, 'E:3976|M:1010', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (168, 8, NULL, 'E:4016|M:1020', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (169, 8, NULL, 'E:4055|M:1030', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (170, 8, NULL, 'E:4094|M:1040', NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (171, 8, NULL, 'E:4134|M:1050', NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (172, 8, NULL, 'E:4173|M:1060', NULL, NULL, NULL, NULL, NULL, NULL, 42);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (173, 8, NULL, 'E:4213|M:1070', NULL, NULL, NULL, NULL, NULL, NULL, 43);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (174, 8, NULL, 'E:4252|M:1080', NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (175, 8, NULL, 'E:4291|M:1090', NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (176, 8, NULL, 'E:4331|M:1100', NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (177, 8, NULL, 'E:437|M:1110', NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (178, 8, NULL, 'E:4409|M:1120', NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (179, 8, NULL, 'E:4449|M:1130', NULL, NULL, NULL, NULL, NULL, NULL, 49);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (180, 8, NULL, 'E:4488|M:1140', NULL, NULL, NULL, NULL, NULL, NULL, 50);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (181, 8, NULL, 'E:4528|M:1150', NULL, NULL, NULL, NULL, NULL, NULL, 51);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (182, 8, NULL, 'E:4567|M:1160', NULL, NULL, NULL, NULL, NULL, NULL, 52);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (183, 8, NULL, 'E:4606|M:1170', NULL, NULL, NULL, NULL, NULL, NULL, 53);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (184, 8, NULL, 'E:4646|M:1180', NULL, NULL, NULL, NULL, NULL, NULL, 54);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (185, 8, NULL, 'E:4685|M:1190', NULL, NULL, NULL, NULL, NULL, NULL, 55);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (186, 8, NULL, 'E:4724|M:1200', NULL, NULL, NULL, NULL, NULL, NULL, 56);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (187, 8, NULL, 'E:4764|M:1210', NULL, NULL, NULL, NULL, NULL, NULL, 57);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (188, 8, NULL, 'E:4803|M:1220', NULL, NULL, NULL, NULL, NULL, NULL, 58);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (189, 8, NULL, 'E:4843|M:1230', NULL, NULL, NULL, NULL, NULL, NULL, 59);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (190, 8, NULL, 'E:4882|M:1240', NULL, NULL, NULL, NULL, NULL, NULL, 60);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (191, 8, NULL, 'E:4921|M:1250', NULL, NULL, NULL, NULL, NULL, NULL, 61);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (192, 8, NULL, 'E:4961|M:1260', NULL, NULL, NULL, NULL, NULL, NULL, 62);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (193, 8, NULL, 'E:500|M:1270', NULL, NULL, NULL, NULL, NULL, NULL, 63);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (194, 8, NULL, 'E:5039|M:1280', NULL, NULL, NULL, NULL, NULL, NULL, 64);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (195, 8, NULL, 'E:5079|M:1290', NULL, NULL, NULL, NULL, NULL, NULL, 65);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (196, 8, NULL, 'E:5118|M:1300', NULL, NULL, NULL, NULL, NULL, NULL, 66);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (197, 8, NULL, 'E:5157|M:1310', NULL, NULL, NULL, NULL, NULL, NULL, 67);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (198, 8, NULL, 'E:5197|M:1320', NULL, NULL, NULL, NULL, NULL, NULL, 68);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (199, 8, NULL, 'E:5236|M:1330', NULL, NULL, NULL, NULL, NULL, NULL, 69);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (200, 8, NULL, 'E:5276|M:1340', NULL, NULL, NULL, NULL, NULL, NULL, 70);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (201, 8, NULL, 'E:5315|M:1350', NULL, NULL, NULL, NULL, NULL, NULL, 71);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (202, 8, NULL, 'E:5354|M:1360', NULL, NULL, NULL, NULL, NULL, NULL, 72);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (203, 8, NULL, 'E:5394|M:1370', NULL, NULL, NULL, NULL, NULL, NULL, 73);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (204, 8, NULL, 'E:5433|M:1380', NULL, NULL, NULL, NULL, NULL, NULL, 74);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (205, 8, NULL, 'E:5472|M:1390', NULL, NULL, NULL, NULL, NULL, NULL, 75);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (206, 8, NULL, 'E:5512|M:1400', NULL, NULL, NULL, NULL, NULL, NULL, 76);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (207, 8, NULL, 'E:5551|M:1410', NULL, NULL, NULL, NULL, NULL, NULL, 77);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (208, 8, NULL, 'E:5591|M:1420', NULL, NULL, NULL, NULL, NULL, NULL, 78);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (209, 8, NULL, 'E:563|M:1430', NULL, NULL, NULL, NULL, NULL, NULL, 79);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (210, 8, NULL, 'E:5669|M:1440', NULL, NULL, NULL, NULL, NULL, NULL, 80);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (211, 8, NULL, 'E:5709|M:1450', NULL, NULL, NULL, NULL, NULL, NULL, 81);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (212, 8, NULL, 'E:5748|M:1460', NULL, NULL, NULL, NULL, NULL, NULL, 82);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (213, 8, NULL, 'E:5787|M:1470', NULL, NULL, NULL, NULL, NULL, NULL, 83);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (214, 8, NULL, 'E:5827|M:1480', NULL, NULL, NULL, NULL, NULL, NULL, 84);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (215, 8, NULL, 'E:5866|M:1490', NULL, NULL, NULL, NULL, NULL, NULL, 85);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (216, 8, NULL, 'E:5906|M:1500', NULL, NULL, NULL, NULL, NULL, NULL, 86);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (217, 9, NULL, 'E:1969|M:500', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (218, 9, NULL, 'E:2008|M:510', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (219, 9, NULL, 'E:2047|M:520', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (220, 9, NULL, 'E:2087|M:530', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (221, 9, NULL, 'E:2126|M:540', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (222, 9, NULL, 'E:2165|M:550', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (223, 9, NULL, 'E:2205|M:560', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (224, 9, NULL, 'E:2244|M:570', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (225, 9, NULL, 'E:2283|M:580', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (226, 9, NULL, 'E:2323|M:590', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (227, 9, NULL, 'E:2362|M:600', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (228, 9, NULL, 'E:2402|M:610', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (229, 9, NULL, 'E:2441|M:620', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (230, 9, NULL, 'E:248|M:630', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (231, 9, NULL, 'E:252|M:640', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (232, 9, NULL, 'E:2559|M:650', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (233, 9, NULL, 'E:2598|M:660', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (234, 9, NULL, 'E:2638|M:670', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (235, 9, NULL, 'E:2677|M:680', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (236, 9, NULL, 'E:2717|M:690', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (237, 9, NULL, 'E:2756|M:700', NULL, NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (238, 9, NULL, 'E:2795|M:710', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (239, 9, NULL, 'E:2835|M:720', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (240, 9, NULL, 'E:2874|M:730', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (241, 9, NULL, 'E:2913|M:740', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (242, 9, NULL, 'E:2953|M:750', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (243, 9, NULL, 'E:2992|M:760', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (244, 9, NULL, 'E:3031|M:770', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (245, 9, NULL, 'E:3071|M:780', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (246, 9, NULL, 'E:311|M:790', NULL, NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (247, 9, NULL, 'E:315|M:800', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (248, 9, NULL, 'E:3189|M:810', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (249, 9, NULL, 'E:3228|M:820', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (250, 9, NULL, 'E:3268|M:830', NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (251, 9, NULL, 'E:3307|M:840', NULL, NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (252, 9, NULL, 'E:3346|M:850', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (253, 9, NULL, 'E:3386|M:860', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (254, 9, NULL, 'E:3425|M:870', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (255, 9, NULL, 'E:3465|M:880', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (256, 9, NULL, 'E:3504|M:890', NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (257, 9, NULL, 'E:3543|M:900', NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (258, 9, NULL, 'E:3583|M:910', NULL, NULL, NULL, NULL, NULL, NULL, 42);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (259, 9, NULL, 'E:3622|M:920', NULL, NULL, NULL, NULL, NULL, NULL, 43);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (260, 9, NULL, 'E:3661|M:930', NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (261, 9, NULL, 'E:3701|M:940', NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (262, 9, NULL, 'E:374|M:950', NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (263, 9, NULL, 'E:378|M:960', NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (264, 9, NULL, 'E:3819|M:970', NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (265, 9, NULL, 'E:3858|M:980', NULL, NULL, NULL, NULL, NULL, NULL, 49);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (266, 9, NULL, 'E:3898|M:990', NULL, NULL, NULL, NULL, NULL, NULL, 50);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (267, 9, NULL, 'E:3937|M:1000', NULL, NULL, NULL, NULL, NULL, NULL, 51);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (268, 9, NULL, 'E:3976|M:1010', NULL, NULL, NULL, NULL, NULL, NULL, 52);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (269, 9, NULL, 'E:4016|M:1020', NULL, NULL, NULL, NULL, NULL, NULL, 53);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (270, 9, NULL, 'E:4055|M:1030', NULL, NULL, NULL, NULL, NULL, NULL, 54);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (271, 9, NULL, 'E:4094|M:1040', NULL, NULL, NULL, NULL, NULL, NULL, 55);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (272, 9, NULL, 'E:4134|M:1050', NULL, NULL, NULL, NULL, NULL, NULL, 56);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (273, 9, NULL, 'E:4173|M:1060', NULL, NULL, NULL, NULL, NULL, NULL, 57);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (274, 9, NULL, 'E:4213|M:1070', NULL, NULL, NULL, NULL, NULL, NULL, 58);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (275, 9, NULL, 'E:4252|M:1080', NULL, NULL, NULL, NULL, NULL, NULL, 59);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (276, 9, NULL, 'E:4291|M:1090', NULL, NULL, NULL, NULL, NULL, NULL, 60);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (277, 9, NULL, 'E:4331|M:1100', NULL, NULL, NULL, NULL, NULL, NULL, 61);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (278, 10, NULL, 'E:4724|M:12', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (279, 10, NULL, 'E:4764|M:121', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (280, 10, NULL, 'E:4803|M:122', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (281, 10, NULL, 'E:4843|M:123', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (282, 10, NULL, 'E:4882|M:124', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (283, 10, NULL, 'E:4921|M:125', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (284, 10, NULL, 'E:4961|M:126', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (285, 10, NULL, 'E:500|M:127', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (286, 10, NULL, 'E:5039|M:128', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (287, 10, NULL, 'E:5079|M:129', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (288, 10, NULL, 'E:5118|M:13', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (289, 10, NULL, 'E:5157|M:131', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (290, 10, NULL, 'E:5197|M:132', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (291, 10, NULL, 'E:5236|M:133', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (292, 10, NULL, 'E:5276|M:134', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (293, 10, NULL, 'E:5315|M:135', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (294, 10, NULL, 'E:5354|M:136', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (295, 10, NULL, 'E:5394|M:137', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (296, 10, NULL, 'E:5433|M:138', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (297, 10, NULL, 'E:5472|M:139', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (298, 10, NULL, 'E:5512|M:14', NULL, NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (299, 10, NULL, 'E:5551|M:141', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (300, 10, NULL, 'E:5591|M:142', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (301, 10, NULL, 'E:563|M:143', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (302, 10, NULL, 'E:5669|M:144', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (303, 10, NULL, 'E:5709|M:145', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (304, 10, NULL, 'E:5748|M:146', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (305, 10, NULL, 'E:5787|M:147', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (306, 10, NULL, 'E:5827|M:148', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (307, 10, NULL, 'E:5866|M:149', NULL, NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (308, 10, NULL, 'E:5906|M:15', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (309, 10, NULL, 'E:5945|M:151', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (310, 10, NULL, 'E:5984|M:152', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (311, 10, NULL, 'E:6024|M:153', NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (312, 10, NULL, 'E:6063|M:154', NULL, NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (313, 10, NULL, 'E:6102|M:155', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (314, 10, NULL, 'E:6142|M:156', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (315, 10, NULL, 'E:6181|M:157', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (316, 10, NULL, 'E:622|M:158', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (317, 10, NULL, 'E:626|M:159', NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (318, 10, NULL, 'E:6299|M:16', NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (319, 10, NULL, 'E:6339|M:161', NULL, NULL, NULL, NULL, NULL, NULL, 42);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (320, 10, NULL, 'E:6378|M:162', NULL, NULL, NULL, NULL, NULL, NULL, 43);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (321, 10, NULL, 'E:6417|M:163', NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (322, 10, NULL, 'E:6457|M:164', NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (323, 10, NULL, 'E:6496|M:165', NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (324, 10, NULL, 'E:6535|M:166', NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (325, 10, NULL, 'E:6575|M:167', NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (326, 10, NULL, 'E:6614|M:168', NULL, NULL, NULL, NULL, NULL, NULL, 49);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (327, 10, NULL, 'E:6654|M:169', NULL, NULL, NULL, NULL, NULL, NULL, 50);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (328, 10, NULL, 'E:6693|M:17', NULL, NULL, NULL, NULL, NULL, NULL, 51);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (329, 10, NULL, 'E:6732|M:171', NULL, NULL, NULL, NULL, NULL, NULL, 52);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (330, 10, NULL, 'E:6772|M:172', NULL, NULL, NULL, NULL, NULL, NULL, 53);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (331, 10, NULL, 'E:6811|M:173', NULL, NULL, NULL, NULL, NULL, NULL, 54);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (332, 10, NULL, 'E:685|M:174', NULL, NULL, NULL, NULL, NULL, NULL, 55);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (333, 10, NULL, 'E:689|M:175', NULL, NULL, NULL, NULL, NULL, NULL, 56);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (334, 10, NULL, 'E:6929|M:176', NULL, NULL, NULL, NULL, NULL, NULL, 57);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (335, 10, NULL, 'E:6969|M:177', NULL, NULL, NULL, NULL, NULL, NULL, 58);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (336, 10, NULL, 'E:7008|M:178', NULL, NULL, NULL, NULL, NULL, NULL, 59);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (337, 10, NULL, 'E:7047|M:179', NULL, NULL, NULL, NULL, NULL, NULL, 60);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (338, 10, NULL, 'E:7087|M:18', NULL, NULL, NULL, NULL, NULL, NULL, 61);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (339, 10, NULL, 'E:7126|M:181', NULL, NULL, NULL, NULL, NULL, NULL, 62);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (340, 10, NULL, 'E:7165|M:182', NULL, NULL, NULL, NULL, NULL, NULL, 63);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (341, 10, NULL, 'E:7205|M:183', NULL, NULL, NULL, NULL, NULL, NULL, 64);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (342, 10, NULL, 'E:7244|M:184', NULL, NULL, NULL, NULL, NULL, NULL, 65);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (343, 10, NULL, 'E:7283|M:185', NULL, NULL, NULL, NULL, NULL, NULL, 66);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (344, 10, NULL, 'E:7323|M:186', NULL, NULL, NULL, NULL, NULL, NULL, 67);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (345, 10, NULL, 'E:7362|M:187', NULL, NULL, NULL, NULL, NULL, NULL, 68);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (346, 10, NULL, 'E:7402|M:188', NULL, NULL, NULL, NULL, NULL, NULL, 69);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (347, 10, NULL, 'E:7441|M:189', NULL, NULL, NULL, NULL, NULL, NULL, 70);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (348, 10, NULL, 'E:748|M:19', NULL, NULL, NULL, NULL, NULL, NULL, 71);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (349, 10, NULL, 'E:752|M:191', NULL, NULL, NULL, NULL, NULL, NULL, 72);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (350, 10, NULL, 'E:7559|M:192', NULL, NULL, NULL, NULL, NULL, NULL, 73);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (351, 10, NULL, 'E:7598|M:193', NULL, NULL, NULL, NULL, NULL, NULL, 74);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (352, 10, NULL, 'E:7638|M:194', NULL, NULL, NULL, NULL, NULL, NULL, 75);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (353, 10, NULL, 'E:7677|M:195', NULL, NULL, NULL, NULL, NULL, NULL, 76);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (354, 10, NULL, 'E:7717|M:196', NULL, NULL, NULL, NULL, NULL, NULL, 77);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (355, 10, NULL, 'E:7756|M:197', NULL, NULL, NULL, NULL, NULL, NULL, 78);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (356, 10, NULL, 'E:7795|M:198', NULL, NULL, NULL, NULL, NULL, NULL, 79);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (357, 10, NULL, 'E:7835|M:199', NULL, NULL, NULL, NULL, NULL, NULL, 80);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (358, 10, NULL, 'E:7874|M:20', NULL, NULL, NULL, NULL, NULL, NULL, 81);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (359, 10, NULL, 'E:7913|M:201', NULL, NULL, NULL, NULL, NULL, NULL, 82);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (360, 10, NULL, 'E:7953|M:202', NULL, NULL, NULL, NULL, NULL, NULL, 83);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (361, 10, NULL, 'E:7992|M:203', NULL, NULL, NULL, NULL, NULL, NULL, 84);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (362, 10, NULL, 'E:8031|M:204', NULL, NULL, NULL, NULL, NULL, NULL, 85);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (363, 10, NULL, 'E:8071|M:205', NULL, NULL, NULL, NULL, NULL, NULL, 86);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (364, 10, NULL, 'E:811|M:206', NULL, NULL, NULL, NULL, NULL, NULL, 87);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (365, 10, NULL, 'E:815|M:207', NULL, NULL, NULL, NULL, NULL, NULL, 88);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (366, 10, NULL, 'E:8189|M:208', NULL, NULL, NULL, NULL, NULL, NULL, 89);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (367, 10, NULL, 'E:8228|M:209', NULL, NULL, NULL, NULL, NULL, NULL, 90);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (368, 10, NULL, 'E:8268|M:21', NULL, NULL, NULL, NULL, NULL, NULL, 91);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (369, 11, NULL, 'E:6614|M:300', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (370, 11, NULL, 'E:6724|M:305', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (371, 11, NULL, 'E:6834|M:310', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (372, 11, NULL, 'E:6945|M:315', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (373, 11, NULL, 'E:7055|M:320', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (374, 11, NULL, 'E:7165|M:325', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (375, 11, NULL, 'E:7275|M:330', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (376, 11, NULL, 'E:7385|M:335', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (377, 11, NULL, 'E:7496|M:340', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (378, 11, NULL, 'E:7606|M:345', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (379, 11, NULL, 'E:7716|M:350', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (380, 11, NULL, 'E:7826|M:355', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (381, 11, NULL, 'E:7937|M:360', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (382, 11, NULL, 'E:8047|M:365', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (383, 11, NULL, 'E:8157|M:370', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (384, 11, NULL, 'E:8267|M:375', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (385, 11, NULL, 'E:8378|M:380', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (386, 11, NULL, 'E:8488|M:385', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (387, 11, NULL, 'E:8598|M:390', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (388, 11, NULL, 'E:8708|M:395', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (389, 11, NULL, 'E:8818|M:400', NULL, NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (390, 11, NULL, 'E:8929|M:405', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (391, 11, NULL, 'E:9039|M:410', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (392, 11, NULL, 'E:9149|M:415', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (393, 11, NULL, 'E:9259|M:420', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (394, 11, NULL, 'E:937|M:425', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (395, 11, NULL, 'E:948|M:430', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (396, 11, NULL, 'E:959|M:435', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (397, 11, NULL, 'E:970|M:440', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (398, 11, NULL, 'E:9811|M:445', NULL, NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (399, 11, NULL, 'E:9921|M:450', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (400, 11, NULL, 'E:10031|M:455', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (401, 11, NULL, 'E:10141|M:460', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (402, 11, NULL, 'E:10251|M:465', NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (403, 11, NULL, 'E:10362|M:470', NULL, NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (404, 11, NULL, 'E:10472|M:475', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (405, 11, NULL, 'E:10582|M:480', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (406, 11, NULL, 'E:10692|M:485', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (407, 11, NULL, 'E:10803|M:490', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (408, 11, NULL, 'E:10913|M:495', NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (409, 11, NULL, 'E:11023|M:500', NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (410, 11, NULL, 'E:11133|M:505', NULL, NULL, NULL, NULL, NULL, NULL, 42);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (411, 11, NULL, 'E:11244|M:510', NULL, NULL, NULL, NULL, NULL, NULL, 43);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (412, 11, NULL, 'E:11354|M:515', NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (413, 11, NULL, 'E:11464|M:520', NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (414, 11, NULL, 'E:11574|M:525', NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (415, 11, NULL, 'E:11684|M:530', NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (416, 11, NULL, 'E:11795|M:535', NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (417, 11, NULL, 'E:11905|M:540', NULL, NULL, NULL, NULL, NULL, NULL, 49);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (418, 11, NULL, 'E:12015|M:545', NULL, NULL, NULL, NULL, NULL, NULL, 50);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (419, 11, NULL, 'E:12125|M:550', NULL, NULL, NULL, NULL, NULL, NULL, 51);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (420, 11, NULL, 'E:12236|M:555', NULL, NULL, NULL, NULL, NULL, NULL, 52);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (421, 11, NULL, 'E:12346|M:560', NULL, NULL, NULL, NULL, NULL, NULL, 53);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (422, 11, NULL, 'E:12456|M:565', NULL, NULL, NULL, NULL, NULL, NULL, 54);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (423, 11, NULL, 'E:12566|M:570', NULL, NULL, NULL, NULL, NULL, NULL, 55);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (424, 11, NULL, 'E:12677|M:575', NULL, NULL, NULL, NULL, NULL, NULL, 56);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (425, 11, NULL, 'E:12787|M:580', NULL, NULL, NULL, NULL, NULL, NULL, 57);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (426, 11, NULL, 'E:12897|M:585', NULL, NULL, NULL, NULL, NULL, NULL, 58);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (427, 11, NULL, 'E:13007|M:590', NULL, NULL, NULL, NULL, NULL, NULL, 59);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (428, 11, NULL, 'E:13118|M:595', NULL, NULL, NULL, NULL, NULL, NULL, 60);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (429, 11, NULL, 'E:13228|M:600', NULL, NULL, NULL, NULL, NULL, NULL, 61);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (430, 11, NULL, 'E:13338|M:605', NULL, NULL, NULL, NULL, NULL, NULL, 62);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (431, 11, NULL, 'E:13448|M:610', NULL, NULL, NULL, NULL, NULL, NULL, 63);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (432, 11, NULL, 'E:13558|M:615', NULL, NULL, NULL, NULL, NULL, NULL, 64);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (433, 11, NULL, 'E:13669|M:620', NULL, NULL, NULL, NULL, NULL, NULL, 65);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (434, 11, NULL, 'E:13779|M:625', NULL, NULL, NULL, NULL, NULL, NULL, 66);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (435, 11, NULL, 'E:13889|M:630', NULL, NULL, NULL, NULL, NULL, NULL, 67);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (436, 11, NULL, 'E:13999|M:635', NULL, NULL, NULL, NULL, NULL, NULL, 68);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (437, 11, NULL, 'E:1411|M:640', NULL, NULL, NULL, NULL, NULL, NULL, 69);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (438, 11, NULL, 'E:1422|M:645', NULL, NULL, NULL, NULL, NULL, NULL, 70);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (439, 11, NULL, 'E:1433|M:650', NULL, NULL, NULL, NULL, NULL, NULL, 71);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (440, 11, NULL, 'E:1444|M:655', NULL, NULL, NULL, NULL, NULL, NULL, 72);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (441, 11, NULL, 'E:14551|M:660', NULL, NULL, NULL, NULL, NULL, NULL, 73);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (442, 11, NULL, 'E:14661|M:665', NULL, NULL, NULL, NULL, NULL, NULL, 74);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (443, 11, NULL, 'E:14771|M:670', NULL, NULL, NULL, NULL, NULL, NULL, 75);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (444, 11, NULL, 'E:14881|M:675', NULL, NULL, NULL, NULL, NULL, NULL, 76);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (445, 11, NULL, 'E:14991|M:680', NULL, NULL, NULL, NULL, NULL, NULL, 77);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (446, 11, NULL, 'E:15102|M:685', NULL, NULL, NULL, NULL, NULL, NULL, 78);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (447, 11, NULL, 'E:15212|M:690', NULL, NULL, NULL, NULL, NULL, NULL, 79);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (448, 11, NULL, 'E:15322|M:695', NULL, NULL, NULL, NULL, NULL, NULL, 80);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (449, 11, NULL, 'E:15432|M:700', NULL, NULL, NULL, NULL, NULL, NULL, 81);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (450, 11, NULL, 'E:15543|M:705', NULL, NULL, NULL, NULL, NULL, NULL, 82);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (451, 11, NULL, 'E:15653|M:710', NULL, NULL, NULL, NULL, NULL, NULL, 83);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (452, 11, NULL, 'E:15763|M:715', NULL, NULL, NULL, NULL, NULL, NULL, 84);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (453, 11, NULL, 'E:15873|M:720', NULL, NULL, NULL, NULL, NULL, NULL, 85);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (454, 11, NULL, 'E:15984|M:725', NULL, NULL, NULL, NULL, NULL, NULL, 86);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (455, 11, NULL, 'E:16094|M:730', NULL, NULL, NULL, NULL, NULL, NULL, 87);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (456, 11, NULL, 'E:16204|M:735', NULL, NULL, NULL, NULL, NULL, NULL, 88);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (457, 11, NULL, 'E:16314|M:740', NULL, NULL, NULL, NULL, NULL, NULL, 89);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (458, 11, NULL, 'E:16424|M:745', NULL, NULL, NULL, NULL, NULL, NULL, 90);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (459, 11, NULL, 'E:16535|M:750', NULL, NULL, NULL, NULL, NULL, NULL, 91);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (460, 11, NULL, 'E:16645|M:755', NULL, NULL, NULL, NULL, NULL, NULL, 92);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (461, 11, NULL, 'E:16755|M:760', NULL, NULL, NULL, NULL, NULL, NULL, 93);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (462, 11, NULL, 'E:16865|M:765', NULL, NULL, NULL, NULL, NULL, NULL, 94);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (463, 11, NULL, 'E:16976|M:770', NULL, NULL, NULL, NULL, NULL, NULL, 95);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (464, 11, NULL, 'E:17086|M:775', NULL, NULL, NULL, NULL, NULL, NULL, 96);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (465, 11, NULL, 'E:17196|M:780', NULL, NULL, NULL, NULL, NULL, NULL, 97);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (466, 11, NULL, 'E:17306|M:785', NULL, NULL, NULL, NULL, NULL, NULL, 98);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (467, 11, NULL, 'E:17417|M:790', NULL, NULL, NULL, NULL, NULL, NULL, 99);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (468, 11, NULL, 'E:17527|M:795', NULL, NULL, NULL, NULL, NULL, NULL, 100);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (469, 11, NULL, 'E:17637|M:800', NULL, NULL, NULL, NULL, NULL, NULL, 101);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (470, 11, NULL, 'E:17747|M:805', NULL, NULL, NULL, NULL, NULL, NULL, 102);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (471, 11, NULL, 'E:17857|M:810', NULL, NULL, NULL, NULL, NULL, NULL, 103);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (472, 11, NULL, 'E:17968|M:815', NULL, NULL, NULL, NULL, NULL, NULL, 104);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (473, 11, NULL, 'E:18078|M:820', NULL, NULL, NULL, NULL, NULL, NULL, 105);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (474, 11, NULL, 'E:18188|M:825', NULL, NULL, NULL, NULL, NULL, NULL, 106);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (475, 11, NULL, 'E:18298|M:830', NULL, NULL, NULL, NULL, NULL, NULL, 107);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (476, 11, NULL, 'E:18409|M:835', NULL, NULL, NULL, NULL, NULL, NULL, 108);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (477, 11, NULL, 'E:18519|M:840', NULL, NULL, NULL, NULL, NULL, NULL, 109);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (478, 11, NULL, 'E:18629|M:845', NULL, NULL, NULL, NULL, NULL, NULL, 110);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (479, 11, NULL, 'E:18739|M:850', NULL, NULL, NULL, NULL, NULL, NULL, 111);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (480, 11, NULL, 'E:1885|M:855', NULL, NULL, NULL, NULL, NULL, NULL, 112);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (481, 11, NULL, 'E:1896|M:860', NULL, NULL, NULL, NULL, NULL, NULL, 113);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (482, 11, NULL, 'E:1907|M:865', NULL, NULL, NULL, NULL, NULL, NULL, 114);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (483, 11, NULL, 'E:1918|M:870', NULL, NULL, NULL, NULL, NULL, NULL, 115);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (484, 11, NULL, 'E:1929|M:875', NULL, NULL, NULL, NULL, NULL, NULL, 116);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (485, 11, NULL, 'E:19401|M:880', NULL, NULL, NULL, NULL, NULL, NULL, 117);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (486, 11, NULL, 'E:19511|M:885', NULL, NULL, NULL, NULL, NULL, NULL, 118);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (487, 11, NULL, 'E:19621|M:890', NULL, NULL, NULL, NULL, NULL, NULL, 119);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (488, 11, NULL, 'E:19731|M:895', NULL, NULL, NULL, NULL, NULL, NULL, 120);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (489, 11, NULL, 'E:19842|M:900', NULL, NULL, NULL, NULL, NULL, NULL, 121);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (490, 11, NULL, 'E:19952|M:905', NULL, NULL, NULL, NULL, NULL, NULL, 122);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (491, 11, NULL, 'E:20062|M:910', NULL, NULL, NULL, NULL, NULL, NULL, 123);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (492, 11, NULL, 'E:20172|M:915', NULL, NULL, NULL, NULL, NULL, NULL, 124);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (493, 11, NULL, 'E:20283|M:920', NULL, NULL, NULL, NULL, NULL, NULL, 125);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (494, 11, NULL, 'E:20393|M:925', NULL, NULL, NULL, NULL, NULL, NULL, 126);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (495, 11, NULL, 'E:20503|M:930', NULL, NULL, NULL, NULL, NULL, NULL, 127);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (496, 11, NULL, 'E:20613|M:935', NULL, NULL, NULL, NULL, NULL, NULL, 128);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (497, 11, NULL, 'E:20723|M:940', NULL, NULL, NULL, NULL, NULL, NULL, 129);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (498, 11, NULL, 'E:20834|M:945', NULL, NULL, NULL, NULL, NULL, NULL, 130);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (499, 11, NULL, 'E:20944|M:950', NULL, NULL, NULL, NULL, NULL, NULL, 131);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (500, 11, NULL, 'E:21054|M:955', NULL, NULL, NULL, NULL, NULL, NULL, 132);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (501, 11, NULL, 'E:21164|M:960', NULL, NULL, NULL, NULL, NULL, NULL, 133);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (502, 11, NULL, 'E:21275|M:965', NULL, NULL, NULL, NULL, NULL, NULL, 134);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (503, 11, NULL, 'E:21385|M:970', NULL, NULL, NULL, NULL, NULL, NULL, 135);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (504, 11, NULL, 'E:21495|M:975', NULL, NULL, NULL, NULL, NULL, NULL, 136);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (505, 11, NULL, 'E:21605|M:980', NULL, NULL, NULL, NULL, NULL, NULL, 137);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (506, 11, NULL, 'E:21716|M:985', NULL, NULL, NULL, NULL, NULL, NULL, 138);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (507, 11, NULL, 'E:21826|M:990', NULL, NULL, NULL, NULL, NULL, NULL, 139);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (508, 11, NULL, 'E:21936|M:995', NULL, NULL, NULL, NULL, NULL, NULL, 140);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (509, 11, NULL, 'E:22046|M:1000', NULL, NULL, NULL, NULL, NULL, NULL, 141);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (510, 11, NULL, 'E:22156|M:1005', NULL, NULL, NULL, NULL, NULL, NULL, 142);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (511, 11, NULL, 'E:22267|M:1010', NULL, NULL, NULL, NULL, NULL, NULL, 143);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (512, 11, NULL, 'E:22377|M:1015', NULL, NULL, NULL, NULL, NULL, NULL, 144);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (513, 11, NULL, 'E:22487|M:1020', NULL, NULL, NULL, NULL, NULL, NULL, 145);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (514, 11, NULL, 'E:22597|M:1025', NULL, NULL, NULL, NULL, NULL, NULL, 146);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (515, 11, NULL, 'E:22708|M:1030', NULL, NULL, NULL, NULL, NULL, NULL, 147);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (516, 11, NULL, 'E:22818|M:1035', NULL, NULL, NULL, NULL, NULL, NULL, 148);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (517, 11, NULL, 'E:22928|M:1040', NULL, NULL, NULL, NULL, NULL, NULL, 149);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (518, 11, NULL, 'E:23038|M:1045', NULL, NULL, NULL, NULL, NULL, NULL, 150);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (519, 11, NULL, 'E:23149|M:1050', NULL, NULL, NULL, NULL, NULL, NULL, 151);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (520, 11, NULL, 'E:23259|M:1055', NULL, NULL, NULL, NULL, NULL, NULL, 152);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (521, 11, NULL, 'E:23369|M:1060', NULL, NULL, NULL, NULL, NULL, NULL, 153);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (522, 11, NULL, 'E:23479|M:1065', NULL, NULL, NULL, NULL, NULL, NULL, 154);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (523, 11, NULL, 'E:23589|M:1070', NULL, NULL, NULL, NULL, NULL, NULL, 155);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (524, 11, NULL, 'E:2370|M:1075', NULL, NULL, NULL, NULL, NULL, NULL, 156);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (525, 11, NULL, 'E:2381|M:1080', NULL, NULL, NULL, NULL, NULL, NULL, 157);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (526, 11, NULL, 'E:2392|M:1085', NULL, NULL, NULL, NULL, NULL, NULL, 158);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (527, 11, NULL, 'E:2403|M:1090', NULL, NULL, NULL, NULL, NULL, NULL, 159);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (528, 11, NULL, 'E:24141|M:1095', NULL, NULL, NULL, NULL, NULL, NULL, 160);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (529, 11, NULL, 'E:24251|M:1100', NULL, NULL, NULL, NULL, NULL, NULL, 161);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (530, 11, NULL, 'E:24361|M:1105', NULL, NULL, NULL, NULL, NULL, NULL, 162);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (531, 11, NULL, 'E:24471|M:1110', NULL, NULL, NULL, NULL, NULL, NULL, 163);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (532, 11, NULL, 'E:24582|M:1115', NULL, NULL, NULL, NULL, NULL, NULL, 164);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (533, 11, NULL, 'E:24692|M:1120', NULL, NULL, NULL, NULL, NULL, NULL, 165);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (534, 11, NULL, 'E:24802|M:1125', NULL, NULL, NULL, NULL, NULL, NULL, 166);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (535, 11, NULL, 'E:24912|M:1130', NULL, NULL, NULL, NULL, NULL, NULL, 167);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (536, 11, NULL, 'E:25022|M:1135', NULL, NULL, NULL, NULL, NULL, NULL, 168);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (537, 11, NULL, 'E:25133|M:1140', NULL, NULL, NULL, NULL, NULL, NULL, 169);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (538, 11, NULL, 'E:25243|M:1145', NULL, NULL, NULL, NULL, NULL, NULL, 170);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (539, 11, NULL, 'E:25353|M:1150', NULL, NULL, NULL, NULL, NULL, NULL, 171);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (540, 11, NULL, 'E:25463|M:1155', NULL, NULL, NULL, NULL, NULL, NULL, 172);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (541, 11, NULL, 'E:25574|M:1160', NULL, NULL, NULL, NULL, NULL, NULL, 173);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (542, 11, NULL, 'E:25684|M:1165', NULL, NULL, NULL, NULL, NULL, NULL, 174);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (543, 11, NULL, 'E:25794|M:1170', NULL, NULL, NULL, NULL, NULL, NULL, 175);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (544, 11, NULL, 'E:25904|M:1175', NULL, NULL, NULL, NULL, NULL, NULL, 176);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (545, 11, NULL, 'E:26015|M:1180', NULL, NULL, NULL, NULL, NULL, NULL, 177);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (546, 11, NULL, 'E:26125|M:1185', NULL, NULL, NULL, NULL, NULL, NULL, 178);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (547, 11, NULL, 'E:26235|M:1190', NULL, NULL, NULL, NULL, NULL, NULL, 179);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (548, 11, NULL, 'E:26345|M:1195', NULL, NULL, NULL, NULL, NULL, NULL, 180);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (549, 11, NULL, 'E:26455|M:1200', NULL, NULL, NULL, NULL, NULL, NULL, 181);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (550, 11, NULL, 'E:26566|M:1205', NULL, NULL, NULL, NULL, NULL, NULL, 182);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (551, 11, NULL, 'E:26676|M:1210', NULL, NULL, NULL, NULL, NULL, NULL, 183);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (552, 11, NULL, 'E:26786|M:1215', NULL, NULL, NULL, NULL, NULL, NULL, 184);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (553, 11, NULL, 'E:26896|M:1220', NULL, NULL, NULL, NULL, NULL, NULL, 185);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (554, 11, NULL, 'E:27007|M:1225', NULL, NULL, NULL, NULL, NULL, NULL, 186);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (555, 11, NULL, 'E:27117|M:1230', NULL, NULL, NULL, NULL, NULL, NULL, 187);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (556, 11, NULL, 'E:27227|M:1235', NULL, NULL, NULL, NULL, NULL, NULL, 188);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (557, 11, NULL, 'E:27337|M:1240', NULL, NULL, NULL, NULL, NULL, NULL, 189);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (558, 11, NULL, 'E:27448|M:1245', NULL, NULL, NULL, NULL, NULL, NULL, 190);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (559, 11, NULL, 'E:27558|M:1250', NULL, NULL, NULL, NULL, NULL, NULL, 191);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (560, 11, NULL, 'E:27668|M:1255', NULL, NULL, NULL, NULL, NULL, NULL, 192);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (561, 11, NULL, 'E:27778|M:1260', NULL, NULL, NULL, NULL, NULL, NULL, 193);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (562, 11, NULL, 'E:27888|M:1265', NULL, NULL, NULL, NULL, NULL, NULL, 194);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (563, 11, NULL, 'E:27999|M:1270', NULL, NULL, NULL, NULL, NULL, NULL, 195);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (564, 11, NULL, 'E:28109|M:1275', NULL, NULL, NULL, NULL, NULL, NULL, 196);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (565, 11, NULL, 'E:28219|M:1280', NULL, NULL, NULL, NULL, NULL, NULL, 197);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (566, 11, NULL, 'E:28329|M:1285', NULL, NULL, NULL, NULL, NULL, NULL, 198);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (567, 11, NULL, 'E:2844|M:1290', NULL, NULL, NULL, NULL, NULL, NULL, 199);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (568, 11, NULL, 'E:2855|M:1295', NULL, NULL, NULL, NULL, NULL, NULL, 200);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (569, 11, NULL, 'E:2866|M:1300', NULL, NULL, NULL, NULL, NULL, NULL, 201);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (570, 12, NULL, 'U:60|K:55|E:385|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (571, 12, NULL, 'U:65|K:60|E:390|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (572, 12, NULL, 'U:70|K:60|E:400|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (573, 12, NULL, 'U:75|K:65|E:405|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (574, 12, NULL, 'U:80|K:70|E:410|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (575, 12, NULL, 'U:85|K:75|E:420|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (576, 12, NULL, 'U:90|K:80|E:425|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (577, 12, NULL, 'U:95|K:85|E:430|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (578, 12, NULL, 'U:100|K:90|E:440|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (579, 12, NULL, 'U:105|K:95|E:445|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (580, 12, NULL, 'U:110|K:100|E:450|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (581, 12, NULL, 'U:115|K:105|E:455|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (582, 12, NULL, 'U:120|K:110|E:460|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (583, 12, NULL, 'U:125|K:115|E:470|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (584, 12, NULL, 'U:130|K:120|E:475|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (585, 12, NULL, 'U:135|K:125|E:480|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (586, 12, NULL, 'U:140|K:130|E:485|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (587, 12, NULL, 'U:145|K:135|E:490|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (588, 12, NULL, 'U:150|K:140|E:495|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (589, 12, NULL, 'U:155|K:145|E:500|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (590, 12, NULL, 'U:160|K:150|E:505|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (591, 12, NULL, 'U:165|K:155|E:510|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 22);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (592, 12, NULL, 'U:170|K:160|E:515|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 23);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (593, 12, NULL, 'U:175|K:165|E:520|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (594, 12, NULL, 'U:180|K:170|E:525|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (595, 12, NULL, 'U:190|K:180|E:535|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (596, 12, NULL, 'U:200|K:190|E:545|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (597, 12, NULL, 'U:210|K:200|E:555|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (598, 12, NULL, 'U:220|K:210|E:565|gender:M', NULL, NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (599, 12, NULL, 'U:40|K:15|E:345|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (600, 12, NULL, 'U:45|K:20|E:350|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (601, 12, NULL, 'U:50|K:25|E:355|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (602, 12, NULL, 'U:55|K:30|E:360|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (603, 12, NULL, 'U:60|K:35|E:365|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (604, 12, NULL, 'U:65|K:40|E:375|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (605, 12, NULL, 'U:70|K:45|E:380|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (606, 12, NULL, 'U:75|K:50|E:385|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (607, 12, NULL, 'U:80|K:55|E:390|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (608, 12, NULL, 'U:85|K:60|E:400|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (609, 12, NULL, 'U:90|K:65|E:405|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (610, 12, NULL, 'U:100|K:75|E:420|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (611, 12, NULL, 'U:110|K:85|E:430|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 42);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (612, 12, NULL, 'U:115|K:90|E:440|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 43);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (613, 12, NULL, 'U:120|K:95|E:445|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (614, 12, NULL, 'U:125|K:100|E:450|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (615, 12, NULL, 'U:130|K:105|E:455|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (616, 12, NULL, 'U:135|K:110|E:460|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (617, 12, NULL, 'U:140|K:115|E:470|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (618, 12, NULL, 'U:145|K:120|E:475|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 49);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (619, 12, NULL, 'U:150|K:125|E:480|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 50);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (620, 12, NULL, 'U:155|K:130|E:485|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 51);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (621, 12, NULL, 'U:160|K:135|E:490|gender:F', NULL, NULL, NULL, NULL, NULL, NULL, 52);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (622, 13, NULL, 'U:20|K:40|E:320', NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (623, 13, NULL, 'U:40|K:60|E:340', NULL, NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (624, 13, NULL, 'U:60|K:80|E:360', NULL, NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (625, 13, NULL, 'U:80|K:100|E:380', NULL, NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (626, 13, NULL, 'U:100|K:120|E:400', NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (627, 13, NULL, 'U:120|K:140|E:420', NULL, NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (628, 13, NULL, 'U:140|K:160|E:440', NULL, NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (629, 13, NULL, 'U:160|K:180|E:460', NULL, NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (630, 13, NULL, 'U:180|K:200|E:480', NULL, NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "dinbog"."option" ("id", "field_id", "order_by", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "id_value_old") VALUES (631, 13, NULL, 'U:200|K:220|E:500', NULL, NULL, NULL, NULL, NULL, NULL, 10);
COMMIT;

-- ----------------------------
-- Table structure for parameter
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."parameter" CASCADE;
CREATE TABLE "dinbog"."parameter" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".parameter_id_seq'::regclass),
  "key" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(150) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of parameter
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (1, 'URL_NEW_USER', 'http://52.207.106.153:8081/api-1.0/v1/users/emails/tokens/', 'URL para confirmar el email de registro');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (2, 'IMAGE_DIRECTORY', 'c:/imagenes/', 'c');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (3, 'IMAGE_URL', 'http://52.207.106.153:8082/image/', 'xcvxv');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (4, 'URL_NEW_EMAIL', 'http://52.207.106.153:8081/api-1.0/v1/users/emails/tokens/', 'URL para confirmar un nuevo email agregado');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (5, 'USER_GROUP_DEFAULT_ID', '1', 'Grupo de usuario por defecto');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (6, 'USER_STATUS_DEFAULT_ID', '1', 'Status por defecto del usuario');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (7, 'MEMBERSHIP_DEFAULT_ID', '1', 'Membership por defecto del usuario');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (9, 'REGEXP_VIDEO', '(avi|mp4)$', 'videos');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (10, 'REGEXP_AUDIO', '(mp3)$', 'audios');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (13, 'ARRAY_TYPE_FILE', 'audio,video,image', 'tipo de archivos');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (11, 'REGEXP_AUDIO_URL', '(soundcloud|spotify)$', 'url audios');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (12, 'REGEXP_VIDEO_URL', '(http:\/\/|https:\/\/)(vimeo\.com|youtu\.be|www\.youtube\.com)\/([\w\/]+)([\?].*)?$', 'url videos');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (15, 'FILE_MAX_LENGTH_PROP', '20MB', 'file size');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (14, 'FILE_MAX_LENGTH', '20', 'peso del archivo');
INSERT INTO "dinbog"."parameter" ("id", "key", "value", "description") VALUES (8, 'REGEXP_IMAGE', '(jpe?g|png|gif)$', 'imagenes');
COMMIT;

-- ----------------------------
-- Table structure for poll
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."poll" CASCADE;
CREATE TABLE "dinbog"."poll" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".poll_id_seq'::regclass),
  "owner_profile_id" int4 NOT NULL,
  "date_init" date NOT NULL,
  "date_end" date NOT NULL,
  "poll_status_id" int4 NOT NULL,
  "event_id" int4 NOT NULL,
  "is_private" int2 DEFAULT 0,
  "participant_minimun" int4 NOT NULL,
  "participant_maximun" int4 NOT NULL,
  "count_votation" int4 DEFAULT 0,
  "winner_total" int4 DEFAULT 0,
  "created" timestamp(6)
)
;

-- ----------------------------
-- Table structure for poll_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."poll_notification" CASCADE;
CREATE TABLE "dinbog"."poll_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".poll_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4,
  "message" text COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for poll_participant
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."poll_participant" CASCADE;
CREATE TABLE "dinbog"."poll_participant" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".poll_participant_id_seq'::regclass),
  "poll_id" int4,
  "event_participant_id" int4,
  "vote_count" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for poll_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."poll_status" CASCADE;
CREATE TABLE "dinbog"."poll_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".poll_status_id_seq'::regclass),
  "event_type_id" int4,
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for poll_vote
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."poll_vote" CASCADE;
CREATE TABLE "dinbog"."poll_vote" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".poll_vote_id_seq'::regclass),
  "poll_id" int4,
  "poll_participant_id" int4,
  "owner_profile_id" int4,
  "vote_type_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for poll_vote_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."poll_vote_type" CASCADE;
CREATE TABLE "dinbog"."poll_vote_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".poll_vote_type_id_seq'::regclass),
  "poll_id" int4 NOT NULL,
  "vote_type_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post" CASCADE;
CREATE TABLE "dinbog"."post" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_id_seq'::regclass),
  "profile_id" int4,
  "content" varchar(500) COLLATE "pg_catalog"."default",
  "programed" int2 DEFAULT 0,
  "programed_date_gmt" timestamptz(6),
  "created" timestamptz(6) DEFAULT now(),
  "count_like" int4 NOT NULL DEFAULT 0,
  "count_comment" int4 NOT NULL DEFAULT 0
)
;

-- ----------------------------
-- Records of post
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post" ("id", "profile_id", "content", "programed", "programed_date_gmt", "created", "count_like", "count_comment") VALUES (1, 1, 'hola mundo', NULL, '2020-04-10 07:49:46-04', '2020-04-10 07:49:42-04', 0, 0);
INSERT INTO "dinbog"."post" ("id", "profile_id", "content", "programed", "programed_date_gmt", "created", "count_like", "count_comment") VALUES (118, 1, 'esto es un @Test Editado del perfil ->1 - Mon May 11 12:59:41 VET 2020', 1, '2020-05-11 12:59:41.145-04', '2020-05-11 12:59:41.093-04', 0, 0);
INSERT INTO "dinbog"."post" ("id", "profile_id", "content", "programed", "programed_date_gmt", "created", "count_like", "count_comment") VALUES (119, 1, 'esto es un @Test Editado del perfil ->1 - Mon May 11 13:25:10 VET 2020', 1, '2020-05-11 13:25:10.228-04', '2020-05-11 13:25:10.16-04', 0, 0);
INSERT INTO "dinbog"."post" ("id", "profile_id", "content", "programed", "programed_date_gmt", "created", "count_like", "count_comment") VALUES (197, 7, 'Post Attachment test', 0, NULL, '2020-05-23 01:43:52.851961-04', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for post_attachment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_attachment" CASCADE;
CREATE TABLE "dinbog"."post_attachment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_attachment_id_seq'::regclass),
  "post_id" int4 NOT NULL,
  "attachment_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of post_attachment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_attachment" ("id", "post_id", "attachment_id") VALUES (199, 1, 1);
INSERT INTO "dinbog"."post_attachment" ("id", "post_id", "attachment_id") VALUES (200, 1, 7);
INSERT INTO "dinbog"."post_attachment" ("id", "post_id", "attachment_id") VALUES (201, 1, 9);
INSERT INTO "dinbog"."post_attachment" ("id", "post_id", "attachment_id") VALUES (206, 119, 188);
INSERT INTO "dinbog"."post_attachment" ("id", "post_id", "attachment_id") VALUES (207, 197, 189);
COMMIT;

-- ----------------------------
-- Table structure for post_comment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_comment" CASCADE;
CREATE TABLE "dinbog"."post_comment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_comment_id_seq'::regclass),
  "parent_id" int4,
  "post_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now(),
  "count_like" int4 DEFAULT 0,
  "value" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of post_comment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_comment" ("id", "parent_id", "post_id", "profile_id", "created", "count_like", "value") VALUES (20, NULL, 119, 7, '2020-05-17 14:14:23.906538-04', 8, NULL);
INSERT INTO "dinbog"."post_comment" ("id", "parent_id", "post_id", "profile_id", "created", "count_like", "value") VALUES (21, NULL, 119, 9, '2020-06-02 16:26:49.758647-04', 0, NULL);
INSERT INTO "dinbog"."post_comment" ("id", "parent_id", "post_id", "profile_id", "created", "count_like", "value") VALUES (22, NULL, 119, 5, '2020-06-02 16:27:00.607963-04', 0, NULL);
INSERT INTO "dinbog"."post_comment" ("id", "parent_id", "post_id", "profile_id", "created", "count_like", "value") VALUES (23, 22, 119, 1, '2020-06-02 16:27:21.789673-04', 0, 'Hijo XXXX');
INSERT INTO "dinbog"."post_comment" ("id", "parent_id", "post_id", "profile_id", "created", "count_like", "value") VALUES (24, 22, 119, 1, '2020-06-02 16:27:39.003676-04', 0, 'Hijo 2xxxxx');
COMMIT;

-- ----------------------------
-- Table structure for post_comment_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_comment_like" CASCADE;
CREATE TABLE "dinbog"."post_comment_like" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_comment_like_id_seq'::regclass),
  "post_comment_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Records of post_comment_like
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (6, 20, 1, '2020-05-25 12:43:57.270351');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (7, 20, 1, '2020-05-25 12:44:11.132396');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (8, 20, 10, '2020-05-25 15:11:26.627062');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (9, 20, 12, '2020-05-25 15:11:31.324667');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (10, 20, 10, '2020-05-25 15:32:19.611428');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (11, 20, 1, '2020-05-25 15:32:24.463139');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (12, 20, 7, '2020-05-25 15:32:28.484751');
INSERT INTO "dinbog"."post_comment_like" ("id", "post_comment_id", "profile_id", "created") VALUES (13, 20, 7, '2020-05-25 15:33:27.619106');
COMMIT;

-- ----------------------------
-- Table structure for post_comment_mention
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_comment_mention" CASCADE;
CREATE TABLE "dinbog"."post_comment_mention" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_comment_mention_id_seq'::regclass),
  "post_comment_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Records of post_comment_mention
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (1, 20, 11, '2020-05-25 13:42:20.904236');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (3, 20, 12, '2020-05-25 13:42:35.637914');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (4, 20, 6, '2020-05-25 14:11:26.584844');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (5, 20, 6, '2020-05-25 14:12:21.498464');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (6, 20, 6, '2020-05-25 14:13:14.521036');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (7, 20, 6, '2020-05-25 14:13:22.66486');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (15, 20, 6, '2020-05-25 14:16:08.897376');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (16, 20, 6, '2020-05-25 14:16:14.293398');
INSERT INTO "dinbog"."post_comment_mention" ("id", "post_comment_id", "profile_id", "created") VALUES (28, 20, 10, '2020-05-25 14:29:53.044659');
COMMIT;

-- ----------------------------
-- Table structure for post_hashtag
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_hashtag" CASCADE;
CREATE TABLE "dinbog"."post_hashtag" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_hashtag_id_seq'::regclass),
  "post_id" int4 NOT NULL,
  "name_hastag" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of post_hashtag
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_hashtag" ("id", "post_id", "name_hastag", "created") VALUES (1, 1, '#probandoElCodigo', '2020-05-07 13:14:09.543-04');
COMMIT;

-- ----------------------------
-- Table structure for post_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_like" CASCADE;
CREATE TABLE "dinbog"."post_like" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_like_id_seq'::regclass),
  "post_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of post_like
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_like" ("id", "post_id", "profile_id", "created") VALUES (208, 119, 13, '2020-05-17 14:14:38.331133-04');
INSERT INTO "dinbog"."post_like" ("id", "post_id", "profile_id", "created") VALUES (214, 118, 10, '2020-05-22 22:37:17.202267-04');
INSERT INTO "dinbog"."post_like" ("id", "post_id", "profile_id", "created") VALUES (215, 119, 9, '2020-06-02 01:35:59.485084-04');
COMMIT;

-- ----------------------------
-- Table structure for post_mention
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_mention" CASCADE;
CREATE TABLE "dinbog"."post_mention" (
  "id" int2 NOT NULL DEFAULT nextval('"dinbog".post_mention_id_seq'::regclass),
  "post_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for post_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_notification" CASCADE;
CREATE TABLE "dinbog"."post_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4,
  "message" text COLLATE "pg_catalog"."default",
  "created" timestamptz(6)
)
;

-- ----------------------------
-- Records of post_notification
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "notification_action_id", "message", "created") VALUES (1, 5, 1, 5, NULL, 'trigger post_like NEW OWNER USER_OD-> user_owner VALUE:(1) TABLA INFO:post_like', NULL);
COMMIT;

-- ----------------------------
-- Table structure for post_report
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_report" CASCADE;
CREATE TABLE "dinbog"."post_report" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_report_id_seq'::regclass),
  "report_type_id" int4,
  "detail" varchar(1000) COLLATE "pg_catalog"."default",
  "owner_id" int4,
  "post_id" int4,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of post_report
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."post_report" ("id", "report_type_id", "detail", "owner_id", "post_id", "created") VALUES (51, 1, 'this is a report', 1, 1, '2020-05-09 04:35:11.634-04');
COMMIT;

-- ----------------------------
-- Table structure for post_share
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."post_share" CASCADE;
CREATE TABLE "dinbog"."post_share" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".post_share_id_seq'::regclass),
  "post_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile" CASCADE;
CREATE TABLE "dinbog"."profile" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "url_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "first_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "birth_date" timestamp(6),
  "gender_id" int4,
  "profile_type_id" int4 NOT NULL,
  "is_searchable" bool NOT NULL DEFAULT false,
  "searchable_since" timestamp(6),
  "membership_id" int4,
  "count_like" int4 NOT NULL DEFAULT 0,
  "count_follow" int4 NOT NULL DEFAULT 0,
  "count_followers" int4 NOT NULL DEFAULT 0,
  "count_conections" int4 NOT NULL DEFAULT 0,
  "count_post" int4 NOT NULL DEFAULT 0,
  "count_views" int4 NOT NULL DEFAULT 0,
  "registration_step" int4,
  "created" timestamptz(6) DEFAULT now(),
  "avatar_id" int4,
  "cover_id" int4,
  "is_whitelist" bool DEFAULT false,
  "token_firebase" varchar(255) COLLATE "pg_catalog"."default",
  "current_country_id" int4,
  "current_city_id" int4
)
;

-- ----------------------------
-- Records of profile
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (1, 1, 'testing01', 'name 1', 'last name 1', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 2, 1, 1, NULL, '2020-04-10 07:50:50-04', 1, 7, 'f', 'ck4apnkUMy4:APA91bHlDYpz2XjJjSikjho6hMlOJUp72KSMpCn3KbdVqIoIBBY7w7YQCdE-5b_zUw4hkQPaVknm9S9qFr6Kr8o_g-_FbigHSU4FbNP-vBQn-5FMioUcq2l91r2P-ekNaOu6OEnbFFOu', 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (4, 4, 'testing04', 'name 4', 'last name 4', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (5, 5, 'testing05', 'name 5', 'last name 5', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 3, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 2);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (6, 6, 'testing06', 'name 6', 'last name 6', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (7, 7, 'testing07', 'name 7', 'last name 7', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 2, 2, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 2);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (8, 8, 'testing08', 'name 8', 'last name 8', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 0, 0, 0, 0, 0, 0, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (9, 9, 'testing09', 'name 9', 'last name 9', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 2, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (10, 10, 'testing10', 'name 10', 'last name 10', '1989-02-10 04:52:52', 1, 2, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (11, 11, 'testing11', 'name 11', 'last name 11', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (12, 12, 'testing12', 'name 12', 'last name 12', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 2);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (13, 13, 'testing13', 'name 13', 'last name 13', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 3);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (14, 14, 'testing14', 'name 14', 'last name 14', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (15, 15, 'testing15', 'prueba', 'last name 15', '1989-02-10 04:52:52', 1, 1, 'f', '2020-04-10 04:52:04', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-04-10 07:50:50-04', NULL, NULL, 'f', NULL, 144, 1);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (1179, 1288, 'nombre.test.apellido.test.tzwmfyo6', 'Nombre Test', 'Apellido Test', '1982-12-03 20:00:00', 1, 1, 't', '1992-12-03 20:00:00', 1, 0, 0, 0, 0, 0, 0, 1, '2020-06-19 14:04:47.442-04', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (1201, 1315, 'nombre.test.apellido.test.bj5kolgk', 'Nombre Test', 'Apellido Test', '1982-12-03 20:00:00', 1, 1, 't', '1992-12-03 20:00:00', 1, 0, 0, 0, 0, 0, 0, 1, '2020-06-19 16:47:24.84-04', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (1186, 1295, 'nombre.test.apellido.test.9ufmhlsd', 'Nombre Test', 'Apellido Test', '1982-12-03 20:00:00', 1, 1, 't', '1992-12-03 20:00:00', 1, 0, 0, 0, 0, 0, 0, 1, '2020-06-19 16:24:41.813-04', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile" ("id", "user_id", "url_name", "first_name", "last_name", "birth_date", "gender_id", "profile_type_id", "is_searchable", "searchable_since", "membership_id", "count_like", "count_follow", "count_followers", "count_conections", "count_post", "count_views", "registration_step", "created", "avatar_id", "cover_id", "is_whitelist", "token_firebase", "current_country_id", "current_city_id") VALUES (1198, 1310, 'nombre.test.apellido.test.voehex1s', 'Nombre Test', 'Apellido Test', '1982-12-03 20:00:00', 1, 1, 't', '1992-12-03 20:00:00', 1, 0, 0, 0, 0, 0, 0, 1, '2020-06-19 16:43:24.167-04', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for profile_attachment
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_attachment" CASCADE;
CREATE TABLE "dinbog"."profile_attachment" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_attachment_id_seq'::regclass),
  "profile_id" int4,
  "attachment_id" int4
)
;

-- ----------------------------
-- Records of profile_attachment
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_attachment" ("id", "profile_id", "attachment_id") VALUES (6, 1, 8);
COMMIT;

-- ----------------------------
-- Table structure for profile_block
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_block" CASCADE;
CREATE TABLE "dinbog"."profile_block" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_block_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_block
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_block" ("id", "profile_id", "owner_profile_id", "created") VALUES (10, 6, 8, '2020-05-03 03:47:45.754');
COMMIT;

-- ----------------------------
-- Table structure for profile_category
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_category" CASCADE;
CREATE TABLE "dinbog"."profile_category" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_category_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "profile_category_type_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of profile_category
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (7, 4, 2);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (13, 1, 1);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (14, 1, 94);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (15, 1, 10);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (16, 1, 154);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (306, 1179, 1);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (307, 1179, 2);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (308, 1179, 3);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (372, 1201, 1);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (373, 1201, 2);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (374, 1201, 3);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (327, 1186, 1);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (328, 1186, 2);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (329, 1186, 3);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (363, 1198, 1);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (364, 1198, 2);
INSERT INTO "dinbog"."profile_category" ("id", "profile_id", "profile_category_type_id") VALUES (365, 1198, 3);
COMMIT;

-- ----------------------------
-- Table structure for profile_category_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_category_type" CASCADE;
CREATE TABLE "dinbog"."profile_category_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_category_type_id_seq'::regclass),
  "parent_id" int4,
  "description" varchar(200) COLLATE "pg_catalog"."default",
  "created" timestamp(6) NOT NULL DEFAULT now(),
  "profile_type_id" int4,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "attachment_id" int4
)
;

-- ----------------------------
-- Records of profile_category_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (1, NULL, NULL, '2020-02-28 12:33:30.334491', 1, 'Actor', NULL, NULL, NULL, NULL, NULL, 'Actors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (2, NULL, NULL, '2020-02-28 12:33:30.345399', 1, 'Actor de Doblaje', NULL, NULL, NULL, NULL, NULL, 'Voice Actors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (3, NULL, NULL, '2020-02-28 12:33:30.353103', 1, 'Artista de Maquillaje', NULL, NULL, NULL, NULL, NULL, 'Makeup Artists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (4, NULL, NULL, '2020-02-28 12:33:30.360406', 1, 'Artista Escenico', NULL, NULL, NULL, NULL, NULL, 'Scenic Artists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (5, NULL, NULL, '2020-02-28 12:33:30.368769', 1, 'Artista Plástico', NULL, NULL, NULL, NULL, NULL, 'Plastic Artists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (6, NULL, NULL, '2020-02-28 12:33:30.376416', 1, 'Bailarin', NULL, NULL, NULL, NULL, NULL, 'Dancers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (7, NULL, NULL, '2020-02-28 12:33:30.393431', 1, 'Blogger', NULL, NULL, NULL, NULL, NULL, 'Bloggers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (8, NULL, NULL, '2020-02-28 12:33:30.400739', 1, 'Booker', NULL, NULL, NULL, NULL, NULL, 'Bookers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (9, NULL, NULL, '2020-02-28 12:33:30.409217', 1, 'Cantante', NULL, NULL, NULL, NULL, NULL, 'Singers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (10, NULL, NULL, '2020-02-28 12:33:30.415831', 1, 'Cantautor', NULL, NULL, NULL, NULL, NULL, 'Singers & Songwriters', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (11, NULL, NULL, '2020-02-28 12:33:30.425265', 1, 'Comediante', NULL, NULL, NULL, NULL, NULL, 'Comedians', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (12, NULL, NULL, '2020-02-28 12:33:30.432287', 1, 'Comentarista', NULL, NULL, NULL, NULL, NULL, 'Commenters', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (13, NULL, NULL, '2020-02-28 12:33:30.439279', 1, 'Compositor', NULL, NULL, NULL, NULL, NULL, 'Composers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (14, NULL, NULL, '2020-02-28 12:33:30.446565', 1, 'Conductor', NULL, NULL, NULL, NULL, NULL, 'Conductors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (15, NULL, NULL, '2020-02-28 12:33:30.453325', 1, 'Coreógrafo', NULL, NULL, NULL, NULL, NULL, 'Choreographers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (16, NULL, NULL, '2020-02-28 12:33:30.458826', 1, 'Deportista', NULL, NULL, NULL, NULL, NULL, 'Athletes', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (17, NULL, NULL, '2020-02-28 12:33:30.464886', 1, 'Director', NULL, NULL, NULL, NULL, NULL, 'Directors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (18, NULL, NULL, '2020-02-28 12:33:30.47214', 1, 'Director Creativo', NULL, NULL, NULL, NULL, NULL, 'Creative Directors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (19, NULL, NULL, '2020-02-28 12:33:30.47925', 1, 'Director de Arte', NULL, NULL, NULL, NULL, NULL, 'Art Directors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (20, NULL, NULL, '2020-02-28 12:33:30.485574', 1, 'Director de Fotografía', NULL, NULL, NULL, NULL, NULL, 'Photography Directors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (21, NULL, NULL, '2020-02-28 12:33:30.492181', 1, 'Director de Orquesta', NULL, NULL, NULL, NULL, NULL, 'Orchestra Directors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (22, NULL, NULL, '2020-02-28 12:33:30.497938', 1, 'Diseñador de Moda', NULL, NULL, NULL, NULL, NULL, 'Fashion Designers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (23, NULL, NULL, '2020-02-28 12:33:30.505396', 1, 'Diseñador de Sonido', NULL, NULL, NULL, NULL, NULL, 'Sound Designers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (24, NULL, NULL, '2020-02-28 12:33:30.511497', 1, 'DJ', NULL, NULL, NULL, NULL, NULL, 'DJs', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (25, NULL, NULL, '2020-02-28 12:33:30.517451', 1, 'Editor', NULL, NULL, NULL, NULL, NULL, 'Editors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (26, NULL, NULL, '2020-02-28 12:33:30.524184', 1, 'Escenógrafo', NULL, NULL, NULL, NULL, NULL, 'Scenographers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (27, NULL, NULL, '2020-02-28 12:33:30.530063', 1, 'Escritor', NULL, NULL, NULL, NULL, NULL, 'Writers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (28, NULL, NULL, '2020-02-28 12:33:30.538219', 1, 'Estilista de Cabello', NULL, NULL, NULL, NULL, NULL, 'Hair Stylists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (29, NULL, NULL, '2020-02-28 12:33:30.545359', 1, 'Estilista de Moda', NULL, NULL, NULL, NULL, NULL, 'Fashion Stylists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (30, NULL, NULL, '2020-02-28 12:33:30.552446', 1, 'Fotógrafo', NULL, NULL, NULL, NULL, NULL, 'Photographers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (31, NULL, NULL, '2020-02-28 12:33:30.56217', 1, 'Grafitero', NULL, NULL, NULL, NULL, NULL, 'Grafitti Artists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (32, NULL, NULL, '2020-02-28 12:33:30.570733', 1, 'Guionista', NULL, NULL, NULL, NULL, NULL, 'Scriptwriters', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (33, NULL, NULL, '2020-02-28 12:33:30.579104', 1, 'Influencer', NULL, NULL, NULL, NULL, NULL, 'Influencers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (34, NULL, NULL, '2020-02-28 12:33:30.584936', 1, 'Manager', NULL, NULL, NULL, NULL, NULL, 'Managers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (35, NULL, NULL, '2020-02-28 12:33:30.593916', 1, 'Microfonista', NULL, NULL, NULL, NULL, NULL, 'Boom Operators', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (36, NULL, NULL, '2020-02-28 12:33:30.600495', 1, 'Modelo', NULL, NULL, NULL, NULL, NULL, 'Models', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (37, NULL, NULL, '2020-02-28 12:33:30.609202', 1, 'Músico', NULL, NULL, NULL, NULL, NULL, 'Musicians', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (38, NULL, NULL, '2020-02-28 12:33:30.616025', 1, 'Periodista ', NULL, NULL, NULL, NULL, NULL, 'Journalists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (39, NULL, NULL, '2020-02-28 12:33:30.623838', 1, 'Pro Gamer', NULL, NULL, NULL, NULL, NULL, 'Pro Gamers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (40, NULL, NULL, '2020-02-28 12:33:30.629926', 1, 'Productor', NULL, NULL, NULL, NULL, NULL, 'Producers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (41, NULL, NULL, '2020-02-28 12:33:30.636324', 1, 'Tatuador', NULL, NULL, NULL, NULL, NULL, 'Tattoo Artists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (42, NULL, NULL, '2020-02-28 12:33:30.644858', 1, 'Ventriloquo', NULL, NULL, NULL, NULL, NULL, 'Ventriloquists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (43, NULL, NULL, '2020-02-28 12:33:30.652983', 1, 'Vestuarista', NULL, NULL, NULL, NULL, NULL, 'Costume Designers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (44, NULL, NULL, '2020-02-28 12:33:30.661345', 1, 'Youtuber', NULL, NULL, NULL, NULL, NULL, 'Youtubers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (45, NULL, NULL, '2020-02-28 12:33:30.668068', 1, 'Otros', NULL, NULL, NULL, NULL, NULL, 'Others', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (46, NULL, NULL, '2020-02-28 12:33:30.675493', 1, 'Seguidores ', NULL, NULL, NULL, NULL, NULL, 'Followers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (47, NULL, NULL, '2020-02-28 12:33:30.682352', 1, 'Kids ', NULL, NULL, NULL, NULL, NULL, 'Kids', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (48, NULL, NULL, '2020-02-28 12:33:30.69116', 2, 'Agencias de Modelaje', NULL, NULL, NULL, NULL, NULL, 'Agencies ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (49, NULL, NULL, '2020-02-28 12:33:30.698059', 2, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Movies', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (50, NULL, NULL, '2020-02-28 12:33:30.703506', 2, 'Conciertos', NULL, NULL, NULL, NULL, NULL, 'Concerts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (51, NULL, NULL, '2020-02-28 12:33:30.7111', 2, 'Concursos', NULL, NULL, NULL, NULL, NULL, 'Contests', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (52, NULL, NULL, '2020-02-28 12:33:30.717045', 2, 'Disquera', NULL, NULL, NULL, NULL, NULL, 'Record Companies', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (53, NULL, NULL, '2020-02-28 12:33:30.723929', 2, 'Productoras', NULL, NULL, NULL, NULL, NULL, 'Production Companies', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (54, NULL, NULL, '2020-02-28 12:33:30.730076', 2, 'Estética y Belleza', NULL, NULL, NULL, NULL, NULL, 'Esthetics & Beauty', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (55, NULL, NULL, '2020-02-28 12:33:30.735287', 2, 'Estudios Fotográficos', NULL, NULL, NULL, NULL, NULL, 'Photo Studios', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (56, NULL, NULL, '2020-02-28 12:33:30.745371', 2, 'Eventos', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (57, NULL, NULL, '2020-02-28 12:33:30.751134', 2, 'Festivales', NULL, NULL, NULL, NULL, NULL, 'Festivals', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (58, NULL, NULL, '2020-02-28 12:33:30.757232', 2, 'Bandas', NULL, NULL, NULL, NULL, NULL, 'Bands', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (59, NULL, NULL, '2020-02-28 12:33:30.763454', 2, 'Impresos', NULL, NULL, NULL, NULL, NULL, 'Prints', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (60, NULL, NULL, '2020-02-28 12:33:30.769472', 2, 'Institutos / Universidades', NULL, NULL, NULL, NULL, NULL, 'Institutes / Universities', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (61, NULL, NULL, '2020-02-28 12:33:30.777093', 2, 'Localizacion', NULL, NULL, NULL, NULL, NULL, 'Location', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (62, NULL, NULL, '2020-02-28 12:33:30.783529', 2, 'Marcas', NULL, NULL, NULL, NULL, NULL, 'Brands', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (63, NULL, NULL, '2020-02-28 12:33:30.792082', 2, 'Medios de Comunicación', NULL, NULL, NULL, NULL, NULL, 'Mass Medias', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (64, NULL, NULL, '2020-02-28 12:33:30.798978', 2, 'Medios Digitales', NULL, NULL, NULL, NULL, NULL, 'Digital Medias', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (65, NULL, NULL, '2020-02-28 12:33:30.804995', 2, 'Pasarela', NULL, NULL, NULL, NULL, NULL, 'Runways', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (66, NULL, NULL, '2020-02-28 12:33:30.812207', 2, 'Programas de TV', NULL, NULL, NULL, NULL, NULL, 'TV Programs', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (67, NULL, NULL, '2020-02-28 12:33:30.818722', 2, 'Relaciones Públicas', NULL, NULL, NULL, NULL, NULL, 'PR', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (68, NULL, NULL, '2020-02-28 12:33:30.828376', 2, 'Teatro', NULL, NULL, NULL, NULL, NULL, 'Theatre', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (69, NULL, NULL, '2020-02-28 12:33:30.835257', 2, 'Castineras', NULL, NULL, NULL, NULL, NULL, 'Casting Companies', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (70, NULL, NULL, '2020-02-28 12:33:30.843374', 2, 'Nutricionistas ', NULL, NULL, NULL, NULL, NULL, 'Nutritionists', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (71, NULL, NULL, '2020-02-28 12:33:30.849139', 2, 'Fitness', NULL, NULL, NULL, NULL, NULL, 'Fitness', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (72, NULL, NULL, '2020-02-28 12:33:30.855844', 2, 'Salud', NULL, NULL, NULL, NULL, NULL, 'Health', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (73, NULL, NULL, '2020-02-28 12:33:30.862152', 2, 'Museos', NULL, NULL, NULL, NULL, NULL, 'Museum', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (74, NULL, NULL, '2020-02-28 12:33:30.867696', 2, 'Galerias', NULL, NULL, NULL, NULL, NULL, 'Gallery ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (75, NULL, NULL, '2020-02-28 12:33:30.875429', 2, 'Otros', NULL, NULL, NULL, NULL, NULL, 'Others', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (76, NULL, NULL, '2020-02-28 12:33:30.881273', 1, 'Diseñador', NULL, NULL, NULL, NULL, NULL, 'Designer', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (77, NULL, NULL, '2020-02-28 12:33:30.887674', 1, 'Creador Digital', NULL, NULL, NULL, NULL, NULL, 'Digital Creator', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (78, NULL, NULL, '2020-02-28 12:33:30.894663', 1, 'Coach', NULL, NULL, NULL, NULL, NULL, 'Coach', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (79, NULL, NULL, '2020-02-28 12:33:30.899889', 1, 'Autor', NULL, NULL, NULL, NULL, NULL, 'Author', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (80, NULL, NULL, '2020-02-28 12:33:30.906033', 1, 'Técnico de Sonido', NULL, NULL, NULL, NULL, NULL, 'Sound Technician', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (81, NULL, NULL, '2020-02-28 12:33:30.913111', 1, 'Salud', NULL, NULL, NULL, NULL, NULL, 'Bless you', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (82, NULL, NULL, '2020-02-28 12:33:30.918661', 1, 'Relaciones Públicas', NULL, NULL, NULL, NULL, NULL, 'Public Relations', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (83, NULL, NULL, '2020-02-28 12:33:30.927849', 1, 'Caricaturista', NULL, NULL, NULL, NULL, NULL, 'Caricaturist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (84, NULL, NULL, '2020-02-28 12:33:30.933823', 1, 'Fitness Trainer', NULL, NULL, NULL, NULL, NULL, 'Fitness Trainer', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (85, NULL, NULL, '2020-02-28 12:33:30.941012', 1, 'Ingeniero Musical', NULL, NULL, NULL, NULL, NULL, 'Music Engineer', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (86, NULL, NULL, '2020-02-28 12:33:30.949311', 2, 'Agencia de Publicidad', NULL, NULL, NULL, NULL, NULL, 'Advertising Agency', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (87, NULL, NULL, '2020-02-28 12:33:30.957041', 1, 'Arreglista', NULL, NULL, NULL, NULL, NULL, 'Arranger', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (88, NULL, NULL, '2020-02-28 12:33:30.965532', 1, 'Creador de Video', NULL, NULL, NULL, NULL, NULL, 'Video Creator', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (89, NULL, NULL, '2020-02-28 12:33:30.973227', 1, 'Doble', NULL, NULL, NULL, NULL, NULL, 'Double', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (90, NULL, NULL, '2020-02-28 12:33:30.982052', 1, 'Nutricionista', NULL, NULL, NULL, NULL, NULL, 'Nutritionist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (91, NULL, NULL, '2020-02-28 12:33:30.988953', 2, 'Agencia de Locución y Doblaje', NULL, NULL, NULL, NULL, NULL, 'Locution and Dubbing Agency', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (92, NULL, NULL, '2020-02-28 12:33:30.996941', 1, 'Estética y Belleza', NULL, NULL, NULL, NULL, NULL, 'Esthetics & Beauty', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (93, 1, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatral', NULL, NULL, NULL, NULL, NULL, 'Theatrical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (94, 1, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine / TV', NULL, NULL, NULL, NULL, NULL, 'Movies / TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (95, 1, NULL, '2020-02-28 14:04:46.063642', 1, 'Danza', NULL, NULL, NULL, NULL, NULL, 'Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (96, 1, NULL, '2020-02-28 14:04:46.063642', 1, 'General', NULL, NULL, NULL, NULL, NULL, 'General', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (97, 2, NULL, '2020-02-28 14:04:46.063642', 1, 'Radio ', NULL, NULL, NULL, NULL, NULL, 'Radio ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (98, 2, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (99, 2, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (100, 2, NULL, '2020-02-28 14:04:46.063642', 1, 'Deportes', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (101, 3, NULL, '2020-02-28 14:04:46.063642', 1, 'Exterior', NULL, NULL, NULL, NULL, NULL, 'Outdoors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (102, 3, NULL, '2020-02-28 14:04:46.063642', 1, 'Interior', NULL, NULL, NULL, NULL, NULL, 'Interior', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (103, 3, NULL, '2020-02-28 14:04:46.063642', 1, 'General', NULL, NULL, NULL, NULL, NULL, 'General', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (104, 4, NULL, '2020-02-28 14:04:46.063642', 1, 'Profesional', NULL, NULL, NULL, NULL, NULL, 'Professional', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (105, 4, NULL, '2020-02-28 14:04:46.063642', 1, 'Duro', NULL, NULL, NULL, NULL, NULL, 'Hardcore', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (106, 4, NULL, '2020-02-28 14:04:46.063642', 1, 'Regular', NULL, NULL, NULL, NULL, NULL, 'Regular', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (107, 4, NULL, '2020-02-28 14:04:46.063642', 1, 'Casual', NULL, NULL, NULL, NULL, NULL, 'Casual', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (108, 5, NULL, '2020-02-28 14:04:46.063642', 1, 'Interpretativa', NULL, NULL, NULL, NULL, NULL, 'Interpretive', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (109, 5, NULL, '2020-02-28 14:04:46.063642', 1, 'Documentales', NULL, NULL, NULL, NULL, NULL, 'Documentals', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (110, 5, NULL, '2020-02-28 14:04:46.063642', 1, 'Animaciones', NULL, NULL, NULL, NULL, NULL, 'Animations', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (111, 5, NULL, '2020-02-28 14:04:46.063642', 1, 'Videojuegos', NULL, NULL, NULL, NULL, NULL, 'Video Games', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (112, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Tags', NULL, NULL, NULL, NULL, NULL, 'Tags', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (113, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Tag con Outline', NULL, NULL, NULL, NULL, NULL, 'Tag with Outline', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (114, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Bubble Letter', NULL, NULL, NULL, NULL, NULL, 'Bubble Letter', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (115, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Throw ups', NULL, NULL, NULL, NULL, NULL, 'Throw ups', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (116, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Block Letter', NULL, NULL, NULL, NULL, NULL, 'Block Letter', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (117, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Wild Style', NULL, NULL, NULL, NULL, NULL, 'Wild Style', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (118, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Model Pastel', NULL, NULL, NULL, NULL, NULL, 'Model Pastel', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (119, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Dirty', NULL, NULL, NULL, NULL, NULL, 'Dirty', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (120, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Characters', NULL, NULL, NULL, NULL, NULL, 'Characters', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (121, 6, NULL, '2020-02-28 14:04:46.063642', 1, 'Iconos', NULL, NULL, NULL, NULL, NULL, 'Icons', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (122, 7, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (123, 7, NULL, '2020-02-28 14:04:46.063642', 1, 'TV / Programa / documental', NULL, NULL, NULL, NULL, NULL, 'TV / TV shows / documentaries', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (124, 7, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (125, 7, NULL, '2020-02-28 14:04:46.063642', 1, 'Productora', NULL, NULL, NULL, NULL, NULL, 'Production', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (126, 7, NULL, '2020-02-28 14:04:46.063642', 1, 'Evento', NULL, NULL, NULL, NULL, NULL, 'Event', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (127, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Blues', NULL, NULL, NULL, NULL, NULL, 'Blues', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (128, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Jazz', NULL, NULL, NULL, NULL, NULL, 'Jazz', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (129, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Rock and Roll', NULL, NULL, NULL, NULL, NULL, 'Rock and Roll', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (130, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Soul', NULL, NULL, NULL, NULL, NULL, 'Soul', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (131, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Country', NULL, NULL, NULL, NULL, NULL, 'Country', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (132, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Disco ', NULL, NULL, NULL, NULL, NULL, 'Disco ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (133, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Techno', NULL, NULL, NULL, NULL, NULL, 'Techno', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (134, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Pop', NULL, NULL, NULL, NULL, NULL, 'Pop', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (135, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Reggae', NULL, NULL, NULL, NULL, NULL, 'Reggae', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (136, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Hip Hop', NULL, NULL, NULL, NULL, NULL, 'Hip Hop', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (137, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Rap', NULL, NULL, NULL, NULL, NULL, 'Rap', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (138, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Flamenco ', NULL, NULL, NULL, NULL, NULL, 'Flamenco ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (139, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Salsa ', NULL, NULL, NULL, NULL, NULL, 'Salsa ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (140, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Reggaeton', NULL, NULL, NULL, NULL, NULL, 'Reggaeton', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (141, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Cristiana', NULL, NULL, NULL, NULL, NULL, 'Christian', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (142, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Neoclásica', NULL, NULL, NULL, NULL, NULL, 'Neoclassic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (143, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Old time', NULL, NULL, NULL, NULL, NULL, 'Old time', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (144, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Boleros', NULL, NULL, NULL, NULL, NULL, 'Boleros', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (145, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Merengue', NULL, NULL, NULL, NULL, NULL, 'Merengue', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (146, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Dance', NULL, NULL, NULL, NULL, NULL, 'Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (147, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Metal', NULL, NULL, NULL, NULL, NULL, 'Metal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (148, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Rumba', NULL, NULL, NULL, NULL, NULL, 'Rumba', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (149, 8, NULL, '2020-02-28 14:04:46.063642', 1, 'Tropical Latino', NULL, NULL, NULL, NULL, NULL, 'Tropical -  Latin', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (150, 9, NULL, '2020-02-28 14:04:46.063642', 1, 'Editor de contenido', NULL, NULL, NULL, NULL, NULL, 'Content editor', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (151, 9, NULL, '2020-02-28 14:04:46.063642', 1, 'Editor Digital', NULL, NULL, NULL, NULL, NULL, 'Digital editor', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (152, 9, NULL, '2020-02-28 14:04:46.063642', 1, 'Editor Técnico', NULL, NULL, NULL, NULL, NULL, 'Technical editor', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (153, 9, NULL, '2020-02-28 14:04:46.063642', 1, 'Editor de Videos', NULL, NULL, NULL, NULL, NULL, 'Video editor', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (154, 10, NULL, '2020-02-28 14:04:46.063642', 1, 'TV / Programa / documental', NULL, NULL, NULL, NULL, NULL, 'TV / TV shows / documentaries', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (155, 10, NULL, '2020-02-28 14:04:46.063642', 1, 'Radio', NULL, NULL, NULL, NULL, NULL, 'Radio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (156, 10, NULL, '2020-02-28 14:04:46.063642', 1, 'Productoras', NULL, NULL, NULL, NULL, NULL, 'Producers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (157, 10, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (158, 83, NULL, '2020-02-28 14:04:46.063642', 1, 'Standup', NULL, NULL, NULL, NULL, NULL, 'Standup', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (159, 83, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (160, 83, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (161, 83, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (162, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Futbol Americano', NULL, NULL, NULL, NULL, NULL, 'American Football', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (163, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Futbol  ', NULL, NULL, NULL, NULL, NULL, 'Football / Soccer', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (164, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Beisbol', NULL, NULL, NULL, NULL, NULL, 'Baseball', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (165, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Baloncesto', NULL, NULL, NULL, NULL, NULL, 'Basketball', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (166, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Voleibol', NULL, NULL, NULL, NULL, NULL, 'Volleyball', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (167, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Tenis', NULL, NULL, NULL, NULL, NULL, 'Tennis', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (168, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Natacion', NULL, NULL, NULL, NULL, NULL, 'Swimming', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (169, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Gimnasia', NULL, NULL, NULL, NULL, NULL, 'Gymnastics', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (170, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Atletismo', NULL, NULL, NULL, NULL, NULL, 'Athletics', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (171, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Automobilismo', NULL, NULL, NULL, NULL, NULL, 'Motor Racing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (172, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Boxeo', NULL, NULL, NULL, NULL, NULL, 'Boxing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (173, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Criquet', NULL, NULL, NULL, NULL, NULL, 'Cricket', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (174, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Golf', NULL, NULL, NULL, NULL, NULL, 'Golf', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (175, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Hockey', NULL, NULL, NULL, NULL, NULL, 'Hockey', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (176, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Lacrosse', NULL, NULL, NULL, NULL, NULL, 'Lacrosse', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (177, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Judo', NULL, NULL, NULL, NULL, NULL, 'Judo', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (178, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Karate', NULL, NULL, NULL, NULL, NULL, 'Karate', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (179, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Kick boxing', NULL, NULL, NULL, NULL, NULL, 'Kick boxing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (180, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Lucha', NULL, NULL, NULL, NULL, NULL, 'Wrestling', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (181, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Motociclismo', NULL, NULL, NULL, NULL, NULL, 'Motorcycling', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (182, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Polo', NULL, NULL, NULL, NULL, NULL, 'Polo', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (183, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Tiro al Arco', NULL, NULL, NULL, NULL, NULL, 'Archery', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (184, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Vela', NULL, NULL, NULL, NULL, NULL, 'Sailing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (185, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Windsurf', NULL, NULL, NULL, NULL, NULL, 'Windsurf', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (186, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Surf', NULL, NULL, NULL, NULL, NULL, 'Surfing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (187, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Equitación', NULL, NULL, NULL, NULL, NULL, 'Horse Riding', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (188, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Esgrima', NULL, NULL, NULL, NULL, NULL, 'Fencing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (189, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Esqui Acuatico', NULL, NULL, NULL, NULL, NULL, 'Water Skiing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (190, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Kitesurf', NULL, NULL, NULL, NULL, NULL, 'Kitesurf', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (191, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Pole Fitness', NULL, NULL, NULL, NULL, NULL, 'Pole Fitness', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (192, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Deporte Extremo', NULL, NULL, NULL, NULL, NULL, 'Extreme Sport', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (193, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Esqui de Montaña', NULL, NULL, NULL, NULL, NULL, 'Alpine Skiing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (194, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Patinaje sobre hielo', NULL, NULL, NULL, NULL, NULL, 'Figure Skating', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (195, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Luge', NULL, NULL, NULL, NULL, NULL, 'Luge', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (196, 11, NULL, '2020-02-28 14:04:46.063642', 1, 'Snowboard', NULL, NULL, NULL, NULL, NULL, 'Snowboard', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (197, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (198, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro ', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (199, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (200, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Moda', NULL, NULL, NULL, NULL, NULL, 'Fashion', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (201, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Fotografía', NULL, NULL, NULL, NULL, NULL, 'Photography', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (202, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Alimentos', NULL, NULL, NULL, NULL, NULL, 'Food Makeup Artist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (203, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (204, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Artísticos', NULL, NULL, NULL, NULL, NULL, 'Artistic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (205, 12, NULL, '2020-02-28 14:04:46.063642', 1, 'Corporal', NULL, NULL, NULL, NULL, NULL, 'Body Makeup - Artist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (206, 13, NULL, '2020-02-28 14:04:46.063642', 1, 'Colorimetría', NULL, NULL, NULL, NULL, NULL, 'Colorimetry', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (207, 13, NULL, '2020-02-28 14:04:46.063642', 1, 'Cortes', NULL, NULL, NULL, NULL, NULL, 'Hair cuts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (208, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Bohemio', NULL, NULL, NULL, NULL, NULL, 'Bohemian', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (209, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Casual', NULL, NULL, NULL, NULL, NULL, 'Casual', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (210, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Clásico', NULL, NULL, NULL, NULL, NULL, 'Classic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (211, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Grunge', NULL, NULL, NULL, NULL, NULL, 'Grunge', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (212, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Minimalista', NULL, NULL, NULL, NULL, NULL, 'Minimalist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (213, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Oversize', NULL, NULL, NULL, NULL, NULL, 'Oversize', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (214, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Retro ', NULL, NULL, NULL, NULL, NULL, 'Retro ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (215, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Romántico', NULL, NULL, NULL, NULL, NULL, 'Romantic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (216, 14, NULL, '2020-02-28 14:04:46.063642', 1, 'Vanguardista', NULL, NULL, NULL, NULL, NULL, 'Avant-garde', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (217, 15, NULL, '2020-02-28 14:04:46.063642', 1, 'Arte Teatral', NULL, NULL, NULL, NULL, NULL, 'Theatrical Arts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (218, 15, NULL, '2020-02-28 14:04:46.063642', 1, 'Danza ', NULL, NULL, NULL, NULL, NULL, 'Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (219, 15, NULL, '2020-02-28 14:04:46.063642', 1, 'Circo', NULL, NULL, NULL, NULL, NULL, 'Circus', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (220, 15, NULL, '2020-02-28 14:04:46.063642', 1, 'Canto ', NULL, NULL, NULL, NULL, NULL, 'Vocal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (221, 15, NULL, '2020-02-28 14:04:46.063642', 1, 'Música', NULL, NULL, NULL, NULL, NULL, 'Music', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (222, 16, NULL, '2020-02-28 14:04:46.063642', 1, 'Radio', NULL, NULL, NULL, NULL, NULL, 'Radio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (223, 16, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (224, 16, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (225, 16, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos ', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (226, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Moda', NULL, NULL, NULL, NULL, NULL, 'Fashion', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (227, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Foodie Bloggers', NULL, NULL, NULL, NULL, NULL, 'Food / Drinks', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (228, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Viajeros', NULL, NULL, NULL, NULL, NULL, 'Travel', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (229, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Entretenimiento', NULL, NULL, NULL, NULL, NULL, 'Entertainment', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (230, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Pedagógico', NULL, NULL, NULL, NULL, NULL, 'Educational', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (231, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine ', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (232, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Video juegos ', NULL, NULL, NULL, NULL, NULL, 'Video Games', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (233, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Deportes', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (234, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Belleza', NULL, NULL, NULL, NULL, NULL, 'Beauty', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (235, 17, NULL, '2020-02-28 14:04:46.063642', 1, 'Maquillaje', NULL, NULL, NULL, NULL, NULL, 'Makeup', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (236, 18, NULL, '2020-02-28 14:04:46.063642', 1, 'Personal', NULL, NULL, NULL, NULL, NULL, 'Personal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (237, 18, NULL, '2020-02-28 14:04:46.063642', 1, 'Profesional', NULL, NULL, NULL, NULL, NULL, 'Professional', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (238, 18, NULL, '2020-02-28 14:04:46.063642', 1, 'Híbridos', NULL, NULL, NULL, NULL, NULL, 'Hybrids', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (239, 18, NULL, '2020-02-28 14:04:46.063642', 1, 'Nicho', NULL, NULL, NULL, NULL, NULL, 'Niche', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (240, 18, NULL, '2020-02-28 14:04:46.063642', 1, 'Corporativo', NULL, NULL, NULL, NULL, NULL, 'Corporate', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (241, 19, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (242, 19, NULL, '2020-02-28 14:04:46.063642', 1, 'TV / Programa / documental', NULL, NULL, NULL, NULL, NULL, 'TV / TV shows / documentaries', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (243, 19, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (244, 19, NULL, '2020-02-28 14:04:46.063642', 1, 'Productora', NULL, NULL, NULL, NULL, NULL, 'Production Company', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (245, 19, NULL, '2020-02-28 14:04:46.063642', 1, 'Evento', NULL, NULL, NULL, NULL, NULL, 'Event', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (246, 20, NULL, '2020-02-28 14:04:46.063642', 1, 'Top', NULL, NULL, NULL, NULL, NULL, 'Top', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (247, 20, NULL, '2020-02-28 14:04:46.063642', 1, 'Modelo', NULL, NULL, NULL, NULL, NULL, 'Model', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (248, 20, NULL, '2020-02-28 14:04:46.063642', 1, 'New Face', NULL, NULL, NULL, NULL, NULL, 'New Face', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (249, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Realista', NULL, NULL, NULL, NULL, NULL, 'Realist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (250, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Acuarela', NULL, NULL, NULL, NULL, NULL, 'Watercolor', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (251, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Tribal', NULL, NULL, NULL, NULL, NULL, 'Tribal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (252, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Graffiti', NULL, NULL, NULL, NULL, NULL, 'Graffiti', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (253, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Old School ', NULL, NULL, NULL, NULL, NULL, 'Old School ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (254, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Neotradicional', NULL, NULL, NULL, NULL, NULL, 'Neotraditional', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (255, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Góticos', NULL, NULL, NULL, NULL, NULL, 'Gothic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (256, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Escritos', NULL, NULL, NULL, NULL, NULL, 'Writings', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (257, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Geométricos', NULL, NULL, NULL, NULL, NULL, 'Geometric', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (258, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Dotwork', NULL, NULL, NULL, NULL, NULL, 'Dotwork', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (259, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Japonés', NULL, NULL, NULL, NULL, NULL, 'Japonese', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (260, 21, NULL, '2020-02-28 14:04:46.063642', 1, '3D', NULL, NULL, NULL, NULL, NULL, '3D', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (261, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Maoríes', NULL, NULL, NULL, NULL, NULL, 'Maori', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (262, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Pin up', NULL, NULL, NULL, NULL, NULL, 'Pin Up', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (263, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'New School', NULL, NULL, NULL, NULL, NULL, 'New School', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (264, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Biomecánicos', NULL, NULL, NULL, NULL, NULL, 'Biomechanical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (265, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Egipcios', NULL, NULL, NULL, NULL, NULL, 'Egyptian', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (266, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Celtas ', NULL, NULL, NULL, NULL, NULL, 'Celtic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (267, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Árabes', NULL, NULL, NULL, NULL, NULL, 'Arab', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (268, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Blackout', NULL, NULL, NULL, NULL, NULL, 'Blackout', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (269, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Budistas', NULL, NULL, NULL, NULL, NULL, 'Budist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (270, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Vikingos', NULL, NULL, NULL, NULL, NULL, 'Viking', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (271, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Mexicanos', NULL, NULL, NULL, NULL, NULL, 'Mexican', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (272, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Americano Tradicional', NULL, NULL, NULL, NULL, NULL, 'Traditional American', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (273, 21, NULL, '2020-02-28 14:04:46.063642', 1, 'Lineas continuas', NULL, NULL, NULL, NULL, NULL, 'Continuous Lines', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (274, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'VIP', NULL, NULL, NULL, NULL, NULL, 'VIP', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (275, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Comunicadores', NULL, NULL, NULL, NULL, NULL, 'Communication', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (276, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Líder de opinión', NULL, NULL, NULL, NULL, NULL, 'Opinion Leader', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (277, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Aventureros', NULL, NULL, NULL, NULL, NULL, 'Adventurous', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (278, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Reviewers', NULL, NULL, NULL, NULL, NULL, 'Reviewer', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (279, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Reporteros', NULL, NULL, NULL, NULL, NULL, 'Reporter', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (280, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Activistas ', NULL, NULL, NULL, NULL, NULL, 'Activist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (281, 22, NULL, '2020-02-28 14:04:46.063642', 1, 'Políticos', NULL, NULL, NULL, NULL, NULL, 'Politics', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (282, 23, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (283, 23, NULL, '2020-02-28 14:04:46.063642', 1, 'Radio', NULL, NULL, NULL, NULL, NULL, 'Radio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (284, 23, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (285, 23, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (286, 23, NULL, '2020-02-28 14:04:46.063642', 1, 'Musical', NULL, NULL, NULL, NULL, NULL, 'Musicals', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (287, 24, NULL, '2020-02-28 14:04:46.063642', 1, 'Principal ', NULL, NULL, NULL, NULL, NULL, 'Main', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (288, 24, NULL, '2020-02-28 14:04:46.063642', 1, 'Secundario', NULL, NULL, NULL, NULL, NULL, 'Supporting', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (289, 24, NULL, '2020-02-28 14:04:46.063642', 1, 'Extras', NULL, NULL, NULL, NULL, NULL, 'Extra', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (290, 89, NULL, '2020-02-28 14:04:46.063642', 1, 'Grupal', NULL, NULL, NULL, NULL, NULL, 'Group', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (291, 89, NULL, '2020-02-28 14:04:46.063642', 1, 'Individual', NULL, NULL, NULL, NULL, NULL, 'Individual', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (292, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Publicidad', NULL, NULL, NULL, NULL, NULL, 'Advertisement', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (293, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Moda', NULL, NULL, NULL, NULL, NULL, 'Fashion', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (294, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Aereo', NULL, NULL, NULL, NULL, NULL, 'Aerial ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (295, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Documental', NULL, NULL, NULL, NULL, NULL, 'Documentary', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (296, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Retrato', NULL, NULL, NULL, NULL, NULL, 'Protrait', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (297, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Submarinismo', NULL, NULL, NULL, NULL, NULL, 'Underwater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (298, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Paisajista', NULL, NULL, NULL, NULL, NULL, 'Landscape', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (299, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Artistico', NULL, NULL, NULL, NULL, NULL, 'Artistic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (300, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (301, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Estudios fotograficos', NULL, NULL, NULL, NULL, NULL, 'Studio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (302, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Naturaleza', NULL, NULL, NULL, NULL, NULL, 'Nature', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (303, 25, NULL, '2020-02-28 14:04:46.063642', 1, 'Deportiva', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (304, 26, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (305, 26, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (306, 26, NULL, '2020-02-28 14:04:46.063642', 1, 'Conciertos', NULL, NULL, NULL, NULL, NULL, 'Concerts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (307, 27, NULL, '2020-02-28 14:04:46.063642', 1, 'Títeres', NULL, NULL, NULL, NULL, NULL, 'Puppets', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (308, 28, NULL, '2020-02-28 14:04:46.063642', 1, 'Alta costura', NULL, NULL, NULL, NULL, NULL, 'High Fashion / Haute Couture', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (309, 28, NULL, '2020-02-28 14:04:46.063642', 1, 'Lista para usar', NULL, NULL, NULL, NULL, NULL, 'Ready to use / Prêt-à-porter', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (310, 28, NULL, '2020-02-28 14:04:46.063642', 1, 'Mercado de masa', NULL, NULL, NULL, NULL, NULL, 'Mass Market', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (311, 28, NULL, '2020-02-28 14:04:46.063642', 1, 'Accesorios', NULL, NULL, NULL, NULL, NULL, 'Accesories', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (312, 28, NULL, '2020-02-28 14:04:46.063642', 1, 'Calzado', NULL, NULL, NULL, NULL, NULL, 'Footwear', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (313, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Arquitecto', NULL, NULL, NULL, NULL, NULL, 'Architect', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (314, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Pintor', NULL, NULL, NULL, NULL, NULL, 'Painter', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (315, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Dibujante', NULL, NULL, NULL, NULL, NULL, 'Sketcher', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (316, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Escultor', NULL, NULL, NULL, NULL, NULL, 'Sculptor', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (317, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Ceramista', NULL, NULL, NULL, NULL, NULL, 'Ceramist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (318, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Orfebre', NULL, NULL, NULL, NULL, NULL, 'Goldsmith', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (319, 29, NULL, '2020-02-28 14:04:46.063642', 1, 'Artesano', NULL, NULL, NULL, NULL, NULL, 'Craftsman', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (320, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Bolero', NULL, NULL, NULL, NULL, NULL, 'Bolero', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (321, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Cancan', NULL, NULL, NULL, NULL, NULL, 'Cancan', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (322, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Chachachá', NULL, NULL, NULL, NULL, NULL, 'Chachacha', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (323, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Conga ', NULL, NULL, NULL, NULL, NULL, 'Conga ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (324, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Country', NULL, NULL, NULL, NULL, NULL, 'Country', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (325, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Cumbia', NULL, NULL, NULL, NULL, NULL, 'Cumbia', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (326, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Dancehall', NULL, NULL, NULL, NULL, NULL, 'Dancehall', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (327, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Danza acrobática', NULL, NULL, NULL, NULL, NULL, 'Acrobatic Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (328, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Danza del vientre', NULL, NULL, NULL, NULL, NULL, 'Belly Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (329, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Flamenco', NULL, NULL, NULL, NULL, NULL, 'Flamenco', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (330, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Danza jazz', NULL, NULL, NULL, NULL, NULL, 'Jazz', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (331, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Free Step', NULL, NULL, NULL, NULL, NULL, 'Free Step', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (332, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Tap', NULL, NULL, NULL, NULL, NULL, 'Tap', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (333, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Jumpstyle', NULL, NULL, NULL, NULL, NULL, 'Jumpstyle', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (334, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Mambo', NULL, NULL, NULL, NULL, NULL, 'Mambo', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (335, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Merengue', NULL, NULL, NULL, NULL, NULL, 'Merengue', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (336, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Polka', NULL, NULL, NULL, NULL, NULL, 'Polka', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (337, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Rock and Roll', NULL, NULL, NULL, NULL, NULL, 'Rock and Roll', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (338, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Salsa ', NULL, NULL, NULL, NULL, NULL, 'Salsa ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (339, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Samba', NULL, NULL, NULL, NULL, NULL, 'Samba', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (340, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'New Style', NULL, NULL, NULL, NULL, NULL, 'New Style', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (341, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Tango', NULL, NULL, NULL, NULL, NULL, 'Tango', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (342, 30, NULL, '2020-02-28 14:04:46.063642', 1, 'Vals', NULL, NULL, NULL, NULL, NULL, 'Waltz', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (343, 31, NULL, '2020-02-28 14:04:46.063642', 1, 'Radio', NULL, NULL, NULL, NULL, NULL, 'Radio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (344, 31, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro ', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (345, 31, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (346, 31, NULL, '2020-02-28 14:04:46.063642', 1, 'Deporte', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (347, 31, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos ', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (348, 32, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro ', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (349, 32, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (350, 32, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (351, 32, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos ', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (352, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Novelas', NULL, NULL, NULL, NULL, NULL, 'Novels', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (353, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Guiones', NULL, NULL, NULL, NULL, NULL, 'Scripts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (354, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Páginas Web', NULL, NULL, NULL, NULL, NULL, 'Websites', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (355, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Blogs', NULL, NULL, NULL, NULL, NULL, 'Blogs', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (356, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Revistas', NULL, NULL, NULL, NULL, NULL, 'Magazine', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (357, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Periódicos', NULL, NULL, NULL, NULL, NULL, 'Newspaper Columnist', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (358, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Cuentos Infantiles', NULL, NULL, NULL, NULL, NULL, 'Children''s Stories', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (359, 33, NULL, '2020-02-28 14:04:46.063642', 1, 'Musica', NULL, NULL, NULL, NULL, NULL, 'Music', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (360, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Música Clásica', NULL, NULL, NULL, NULL, NULL, 'Classical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (361, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Blues', NULL, NULL, NULL, NULL, NULL, 'Blues', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (362, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Jazz', NULL, NULL, NULL, NULL, NULL, 'Jazz', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (363, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Rock and Roll', NULL, NULL, NULL, NULL, NULL, 'Rock and Roll', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (364, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Soul', NULL, NULL, NULL, NULL, NULL, 'Soul', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (365, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Country', NULL, NULL, NULL, NULL, NULL, 'Country', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (366, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Funk ', NULL, NULL, NULL, NULL, NULL, 'Funk ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (367, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Disco ', NULL, NULL, NULL, NULL, NULL, 'Disco ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (368, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'house', NULL, NULL, NULL, NULL, NULL, 'House', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (369, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Techno', NULL, NULL, NULL, NULL, NULL, 'Techno', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (370, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Pop', NULL, NULL, NULL, NULL, NULL, 'Pop', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (371, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Reggae', NULL, NULL, NULL, NULL, NULL, 'Reggae', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (372, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Hip Hop', NULL, NULL, NULL, NULL, NULL, 'Hip Hop', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (373, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Rap', NULL, NULL, NULL, NULL, NULL, 'Rap', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (374, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Flamenco ', NULL, NULL, NULL, NULL, NULL, 'Flamenco ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (375, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Salsa ', NULL, NULL, NULL, NULL, NULL, 'Salsa ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (376, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Regueton', NULL, NULL, NULL, NULL, NULL, 'Regueton', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (377, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Ambient', NULL, NULL, NULL, NULL, NULL, 'Ambient', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (378, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Fusión', NULL, NULL, NULL, NULL, NULL, 'Fusion', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (379, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Cristiana', NULL, NULL, NULL, NULL, NULL, 'Christian', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (380, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Neoclásica', NULL, NULL, NULL, NULL, NULL, 'Neoclassic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (381, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Old time', NULL, NULL, NULL, NULL, NULL, 'Old time', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (382, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Boleros', NULL, NULL, NULL, NULL, NULL, 'Boleros', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (383, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Electro', NULL, NULL, NULL, NULL, NULL, 'Electro', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (384, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Merengue', NULL, NULL, NULL, NULL, NULL, 'Merengue', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (385, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Dance', NULL, NULL, NULL, NULL, NULL, 'Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (386, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Metal', NULL, NULL, NULL, NULL, NULL, 'Metal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (387, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Rumba', NULL, NULL, NULL, NULL, NULL, 'Rumba', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (388, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Punk', NULL, NULL, NULL, NULL, NULL, 'Punk', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (389, 85, NULL, '2020-02-28 14:04:46.063642', 1, 'Tropical Latino', NULL, NULL, NULL, NULL, NULL, 'Tropical - Latin', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (390, 34, NULL, '2020-02-28 14:04:46.063642', 1, 'Clásica', NULL, NULL, NULL, NULL, NULL, 'Classical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (391, 34, NULL, '2020-02-28 14:04:46.063642', 1, 'Popular', NULL, NULL, NULL, NULL, NULL, 'Popular', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (392, 34, NULL, '2020-02-28 14:04:46.063642', 1, 'Electrónicos', NULL, NULL, NULL, NULL, NULL, 'Electronic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (393, 35, NULL, '2020-02-28 14:04:46.063642', 1, 'Mixtos', NULL, NULL, NULL, NULL, NULL, 'Mix', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (394, 35, NULL, '2020-02-28 14:04:46.063642', 1, 'Ambient Género', NULL, NULL, NULL, NULL, NULL, 'Ambient', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (395, 35, NULL, '2020-02-28 14:04:46.063642', 1, 'Chiptune', NULL, NULL, NULL, NULL, NULL, 'Chiptune', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (396, 35, NULL, '2020-02-28 14:04:46.063642', 1, 'Disco', NULL, NULL, NULL, NULL, NULL, 'Disco', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (397, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Opera', NULL, NULL, NULL, NULL, NULL, 'Opera', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (398, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Country ', NULL, NULL, NULL, NULL, NULL, 'Country ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (399, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Pop ', NULL, NULL, NULL, NULL, NULL, 'Pop ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (400, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Pop Rock ', NULL, NULL, NULL, NULL, NULL, 'Pop Rock ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (401, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Cumbia', NULL, NULL, NULL, NULL, NULL, 'Cumbia', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (402, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Merengue', NULL, NULL, NULL, NULL, NULL, 'Merengue', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (403, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Salsa', NULL, NULL, NULL, NULL, NULL, 'Salsa', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (404, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Urbano ', NULL, NULL, NULL, NULL, NULL, 'Urban ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (405, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Rock and Roll', NULL, NULL, NULL, NULL, NULL, 'Rock and Roll', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (406, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Reggae ', NULL, NULL, NULL, NULL, NULL, 'Reggae ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (407, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Dance', NULL, NULL, NULL, NULL, NULL, 'Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (408, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'House', NULL, NULL, NULL, NULL, NULL, 'House', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (409, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Electro', NULL, NULL, NULL, NULL, NULL, 'Electro', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (410, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Hip Hop', NULL, NULL, NULL, NULL, NULL, 'Hip Hop', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (411, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Rap', NULL, NULL, NULL, NULL, NULL, 'Rap', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (412, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Trap', NULL, NULL, NULL, NULL, NULL, 'Trap', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (413, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Balada', NULL, NULL, NULL, NULL, NULL, 'Balada', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (414, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Clásico', NULL, NULL, NULL, NULL, NULL, 'Classical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (415, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Blues', NULL, NULL, NULL, NULL, NULL, 'Blues', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (416, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Boleros', NULL, NULL, NULL, NULL, NULL, 'Boleros', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (417, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Techno', NULL, NULL, NULL, NULL, NULL, 'Techno', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (418, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Disco', NULL, NULL, NULL, NULL, NULL, 'Disco', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (419, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Funk', NULL, NULL, NULL, NULL, NULL, 'Funk', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (420, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Metal', NULL, NULL, NULL, NULL, NULL, 'Metal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (421, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Rumba', NULL, NULL, NULL, NULL, NULL, 'Rumba', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (422, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Soul', NULL, NULL, NULL, NULL, NULL, 'Soul', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (423, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Punk', NULL, NULL, NULL, NULL, NULL, 'Punk', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (424, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Ambient', NULL, NULL, NULL, NULL, NULL, 'Ambient', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (425, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Fusión', NULL, NULL, NULL, NULL, NULL, 'Fusion', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (426, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Cristiana', NULL, NULL, NULL, NULL, NULL, 'Christian', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (427, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Neoclásica', NULL, NULL, NULL, NULL, NULL, 'Neoclassic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (428, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Old time', NULL, NULL, NULL, NULL, NULL, 'Old time', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (429, 36, NULL, '2020-02-28 14:04:46.063642', 1, 'Regueton', NULL, NULL, NULL, NULL, NULL, 'Regueton', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (430, 37, NULL, '2020-02-28 14:04:46.063642', 1, 'Talento', NULL, NULL, NULL, NULL, NULL, 'Talent', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (431, 37, NULL, '2020-02-28 14:04:46.063642', 1, 'General', NULL, NULL, NULL, NULL, NULL, 'General', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (432, 38, NULL, '2020-02-28 14:04:46.063642', 1, 'Radio', NULL, NULL, NULL, NULL, NULL, 'Radio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (433, 38, NULL, '2020-02-28 14:04:46.063642', 1, 'Teatro ', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (434, 38, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (435, 38, NULL, '2020-02-28 14:04:46.063642', 1, 'Deporte', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (436, 38, NULL, '2020-02-28 14:04:46.063642', 1, 'Eventos ', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (437, 39, NULL, '2020-02-28 14:04:46.063642', 1, 'Deportivo', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (438, 39, NULL, '2020-02-28 14:04:46.063642', 1, 'Cultural', NULL, NULL, NULL, NULL, NULL, 'Cultural', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (439, 39, NULL, '2020-02-28 14:04:46.063642', 1, 'Prensa Rosa', NULL, NULL, NULL, NULL, NULL, 'Celebrity Journalisim', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (440, 39, NULL, '2020-02-28 14:04:46.063642', 1, 'Informativo', NULL, NULL, NULL, NULL, NULL, 'Informative', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (441, 40, NULL, '2020-02-28 14:04:46.063642', 1, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (442, 40, NULL, '2020-02-28 14:04:46.063642', 1, 'TV', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (443, 40, NULL, '2020-02-28 14:04:46.063642', 1, 'Video', NULL, NULL, NULL, NULL, NULL, 'Video', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (444, 41, NULL, '2020-02-28 14:04:46.063642', 1, 'Talento', NULL, NULL, NULL, NULL, NULL, 'Talent', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (445, 41, NULL, '2020-02-28 14:04:46.063642', 1, 'Medio Social', NULL, NULL, NULL, NULL, NULL, 'Social Media', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (446, 41, NULL, '2020-02-28 14:04:46.063642', 1, 'Community', NULL, NULL, NULL, NULL, NULL, 'Community', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (447, 41, NULL, '2020-02-28 14:04:46.063642', 1, 'Deportistas', NULL, NULL, NULL, NULL, NULL, 'Athletes', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (448, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Gimnasio', NULL, NULL, NULL, NULL, NULL, 'Gym', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (449, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Aerobic', NULL, NULL, NULL, NULL, NULL, 'Aerobic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (450, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Yoga', NULL, NULL, NULL, NULL, NULL, 'Yoga', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (451, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Pilates', NULL, NULL, NULL, NULL, NULL, 'Pilates', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (452, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Stretching', NULL, NULL, NULL, NULL, NULL, 'Stretching', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (453, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Gap', NULL, NULL, NULL, NULL, NULL, 'Gap', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (454, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Body pump', NULL, NULL, NULL, NULL, NULL, 'Body pump', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (455, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Boxing', NULL, NULL, NULL, NULL, NULL, 'Boxing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (456, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Crossfit', NULL, NULL, NULL, NULL, NULL, 'Crossfit', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (457, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Power jump', NULL, NULL, NULL, NULL, NULL, 'Power jump', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (458, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Balance Training', NULL, NULL, NULL, NULL, NULL, 'Balance Training', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (459, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Fitness experiencial', NULL, NULL, NULL, NULL, NULL, 'Fitness experiencial', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (460, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'TRX', NULL, NULL, NULL, NULL, NULL, 'TRX', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (461, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Aerobox', NULL, NULL, NULL, NULL, NULL, 'Aerobox', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (462, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Taebo', NULL, NULL, NULL, NULL, NULL, 'Taebo', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (463, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Zumba', NULL, NULL, NULL, NULL, NULL, 'Zumba', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (464, 48, NULL, '2020-02-28 14:04:46.063642', 2, 'Step', NULL, NULL, NULL, NULL, NULL, 'Step', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (465, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Redes Sociales', NULL, NULL, NULL, NULL, NULL, 'Social Networks', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (466, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Podcast', NULL, NULL, NULL, NULL, NULL, 'Podcast', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (467, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Radio digital', NULL, NULL, NULL, NULL, NULL, 'Digital Radio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (468, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Tv Digital', NULL, NULL, NULL, NULL, NULL, 'Digital TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (469, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Blogs', NULL, NULL, NULL, NULL, NULL, 'Blogs', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (470, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Revistas digitales', NULL, NULL, NULL, NULL, NULL, 'Digital Magazines', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (471, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Periódicos digitales', NULL, NULL, NULL, NULL, NULL, 'Digital Newspaper', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (472, 49, NULL, '2020-02-28 14:04:46.063642', 2, 'Páginas Web', NULL, NULL, NULL, NULL, NULL, 'Websites', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (473, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Servicio', NULL, NULL, NULL, NULL, NULL, 'Service', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (474, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Comercial', NULL, NULL, NULL, NULL, NULL, 'Commercial', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (475, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Mixta', NULL, NULL, NULL, NULL, NULL, 'Mix', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (476, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Colectiva', NULL, NULL, NULL, NULL, NULL, 'Collective', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (477, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Productos', NULL, NULL, NULL, NULL, NULL, 'Products', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (478, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Personal', NULL, NULL, NULL, NULL, NULL, 'Personal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (479, 50, NULL, '2020-02-28 14:04:46.063642', 2, 'Industrial', NULL, NULL, NULL, NULL, NULL, 'Industrial', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (480, 51, NULL, '2020-02-28 14:04:46.063642', 2, 'Exteriores', NULL, NULL, NULL, NULL, NULL, 'Outdoors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (481, 51, NULL, '2020-02-28 14:04:46.063642', 2, 'interiores', NULL, NULL, NULL, NULL, NULL, 'Indoors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (482, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Canto', NULL, NULL, NULL, NULL, NULL, 'Vocal', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (483, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Baile', NULL, NULL, NULL, NULL, NULL, 'Dance', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (484, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Artes Plasticas', NULL, NULL, NULL, NULL, NULL, 'Plastic Arts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (485, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Fotografia', NULL, NULL, NULL, NULL, NULL, 'Photography', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (486, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Musica', NULL, NULL, NULL, NULL, NULL, 'Music', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (487, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Belleza', NULL, NULL, NULL, NULL, NULL, 'Beauty', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (488, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Cine', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (489, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Documentales', NULL, NULL, NULL, NULL, NULL, 'Documentaries', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (490, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Deportivo', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (491, 52, NULL, '2020-02-28 14:04:46.063642', 2, 'Videos', NULL, NULL, NULL, NULL, NULL, 'Videos', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (492, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Publicidad', NULL, NULL, NULL, NULL, NULL, 'Advertisement', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (493, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Comunicacion', NULL, NULL, NULL, NULL, NULL, 'Communication', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (494, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Contenido', NULL, NULL, NULL, NULL, NULL, 'Content', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (495, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Inbound Marketing', NULL, NULL, NULL, NULL, NULL, 'Inbound Marketing', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (496, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Talento', NULL, NULL, NULL, NULL, NULL, 'Talent', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (497, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Modelaje', NULL, NULL, NULL, NULL, NULL, 'Modeling', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (498, 53, NULL, '2020-02-28 14:04:46.063642', 2, 'Representación', NULL, NULL, NULL, NULL, NULL, 'Representation', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (499, 54, NULL, '2020-02-28 14:04:46.063642', 2, 'Digitales', NULL, NULL, NULL, NULL, NULL, 'Digital', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (500, 54, NULL, '2020-02-28 14:04:46.063642', 2, 'Impresos', NULL, NULL, NULL, NULL, NULL, 'Print', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (501, 54, NULL, '2020-02-28 14:04:46.063642', 2, 'Audiovisuales', NULL, NULL, NULL, NULL, NULL, 'Audiovisual', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (502, 55, NULL, '2020-02-28 14:04:46.063642', 2, 'Cinematográfica', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (503, 55, NULL, '2020-02-28 14:04:46.063642', 2, 'Televisión', NULL, NULL, NULL, NULL, NULL, 'TV', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (504, 55, NULL, '2020-02-28 14:04:46.063642', 2, 'Eventos ', NULL, NULL, NULL, NULL, NULL, 'Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (505, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Música', NULL, NULL, NULL, NULL, NULL, 'Music', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (506, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Comida', NULL, NULL, NULL, NULL, NULL, 'Food', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (507, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Cine ', NULL, NULL, NULL, NULL, NULL, 'Film', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (508, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Teatro', NULL, NULL, NULL, NULL, NULL, 'Theater', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (509, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Poesía', NULL, NULL, NULL, NULL, NULL, 'Poetry', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (510, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Aéreos', NULL, NULL, NULL, NULL, NULL, 'Aerial', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (511, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Arte', NULL, NULL, NULL, NULL, NULL, 'Artistic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (512, 56, NULL, '2020-02-28 14:04:46.063642', 2, 'Aire libre', NULL, NULL, NULL, NULL, NULL, 'Outdoors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (513, 57, NULL, '2020-02-28 14:04:46.063642', 2, 'Internas', NULL, NULL, NULL, NULL, NULL, 'Inhouse', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (514, 57, NULL, '2020-02-28 14:04:46.063642', 2, 'Externas', NULL, NULL, NULL, NULL, NULL, 'External', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (515, 57, NULL, '2020-02-28 14:04:46.063642', 2, 'Empresariales', NULL, NULL, NULL, NULL, NULL, 'Corporative', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (516, 57, NULL, '2020-02-28 14:04:46.063642', 2, 'Políticas', NULL, NULL, NULL, NULL, NULL, 'Political', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (517, 57, NULL, '2020-02-28 14:04:46.063642', 2, 'Institucionales', NULL, NULL, NULL, NULL, NULL, 'Institutional', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (518, 57, NULL, '2020-02-28 14:04:46.063642', 2, 'General', NULL, NULL, NULL, NULL, NULL, 'General', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (519, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Informativos', NULL, NULL, NULL, NULL, NULL, 'Informative', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (520, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Entretenimiento', NULL, NULL, NULL, NULL, NULL, 'Entertainment', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (521, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Educativos', NULL, NULL, NULL, NULL, NULL, 'Educational', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (522, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Documentales', NULL, NULL, NULL, NULL, NULL, 'Documental', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (523, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Series', NULL, NULL, NULL, NULL, NULL, 'Series', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (524, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Concursos', NULL, NULL, NULL, NULL, NULL, 'Contests', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (525, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Reality', NULL, NULL, NULL, NULL, NULL, 'Reality', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (526, 58, NULL, '2020-02-28 14:04:46.063642', 2, 'Variados', NULL, NULL, NULL, NULL, NULL, 'Varied', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (527, 59, NULL, '2020-02-28 14:04:46.063642', 2, 'Salones de belleza', NULL, NULL, NULL, NULL, NULL, 'Beauty Salon', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (528, 59, NULL, '2020-02-28 14:04:46.063642', 2, 'Salones infantiles', NULL, NULL, NULL, NULL, NULL, 'Kids Salon', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (529, 59, NULL, '2020-02-28 14:04:46.063642', 2, 'Barberías', NULL, NULL, NULL, NULL, NULL, 'Barbershop', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (530, 59, NULL, '2020-02-28 14:04:46.063642', 2, 'Spa', NULL, NULL, NULL, NULL, NULL, 'Spa', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (531, 60, NULL, '2020-02-28 14:04:46.063642', 2, 'Europea', NULL, NULL, NULL, NULL, NULL, 'European', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (532, 60, NULL, '2020-02-28 14:04:46.063642', 2, 'Latina', NULL, NULL, NULL, NULL, NULL, 'Latin', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (533, 60, NULL, '2020-02-28 14:04:46.063642', 2, 'Americana', NULL, NULL, NULL, NULL, NULL, 'American', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (534, 60, NULL, '2020-02-28 14:04:46.063642', 2, 'Inglesa', NULL, NULL, NULL, NULL, NULL, 'British', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (535, 61, NULL, '2020-02-28 14:04:46.063642', 2, 'Bandas', NULL, NULL, NULL, NULL, NULL, 'Band', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (536, 61, NULL, '2020-02-28 14:04:46.063642', 2, 'Agrupaciones', NULL, NULL, NULL, NULL, NULL, 'Group', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (537, 61, NULL, '2020-02-28 14:04:46.063642', 2, 'Orquestas', NULL, NULL, NULL, NULL, NULL, 'Orchestra', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (538, 61, NULL, '2020-02-28 14:04:46.063642', 2, 'Duos', NULL, NULL, NULL, NULL, NULL, 'Duo', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (539, 61, NULL, '2020-02-28 14:04:46.063642', 2, 'Trios', NULL, NULL, NULL, NULL, NULL, 'Trio', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (540, 62, NULL, '2020-02-28 14:04:46.063642', 2, 'Empresariales', NULL, NULL, NULL, NULL, NULL, 'Corporative', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (541, 62, NULL, '2020-02-28 14:04:46.063642', 2, 'Ferias y exhibiciones', NULL, NULL, NULL, NULL, NULL, 'Festivals & Exhibitions', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (542, 62, NULL, '2020-02-28 14:04:46.063642', 2, 'Congresos y convenciones', NULL, NULL, NULL, NULL, NULL, 'Congress & Convention', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (543, 62, NULL, '2020-02-28 14:04:46.063642', 2, 'Eventos sociales', NULL, NULL, NULL, NULL, NULL, 'Social Events', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (544, 62, NULL, '2020-02-28 14:04:46.063642', 2, 'Catering', NULL, NULL, NULL, NULL, NULL, 'Catering', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (545, 62, NULL, '2020-02-28 14:04:46.063642', 2, 'Conciertos y espectáculos', NULL, NULL, NULL, NULL, NULL, 'Concert & Show', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (546, 63, NULL, '2020-02-28 14:04:46.063642', 2, 'Independiente ', NULL, NULL, NULL, NULL, NULL, 'Independent', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (547, 63, NULL, '2020-02-28 14:04:46.063642', 2, 'Animación', NULL, NULL, NULL, NULL, NULL, 'Animation', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (548, 63, NULL, '2020-02-28 14:04:46.063642', 2, 'Documental ', NULL, NULL, NULL, NULL, NULL, 'Documentary', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (549, 63, NULL, '2020-02-28 14:04:46.063642', 2, 'Experimental', NULL, NULL, NULL, NULL, NULL, 'Experimental', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (550, 63, NULL, '2020-02-28 14:04:46.063642', 2, 'Ambiental ', NULL, NULL, NULL, NULL, NULL, 'Environmental', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (551, 63, NULL, '2020-02-28 14:04:46.063642', 2, 'Comercial', NULL, NULL, NULL, NULL, NULL, 'Commercial', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (552, 64, NULL, '2020-02-28 14:04:46.063642', 2, 'General', NULL, NULL, NULL, NULL, NULL, 'General', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (553, 64, NULL, '2020-02-28 14:04:46.063642', 2, 'Arte', NULL, NULL, NULL, NULL, NULL, 'Artistic', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (554, 64, NULL, '2020-02-28 14:04:46.063642', 2, 'Moda', NULL, NULL, NULL, NULL, NULL, 'Fashion', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (555, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Cómico', NULL, NULL, NULL, NULL, NULL, 'Comedy', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (556, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Dramatico', NULL, NULL, NULL, NULL, NULL, 'Drama', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (557, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Infantil', NULL, NULL, NULL, NULL, NULL, 'Kids', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (558, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Al aire libre ', NULL, NULL, NULL, NULL, NULL, 'Outdoors', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (559, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Títeres', NULL, NULL, NULL, NULL, NULL, 'Puppets', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (560, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Pedagógico', NULL, NULL, NULL, NULL, NULL, 'Educational', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (561, 65, NULL, '2020-02-28 14:04:46.063642', 2, 'Mímicos', NULL, NULL, NULL, NULL, NULL, 'Mime', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (562, 66, NULL, '2020-02-28 14:04:46.063642', 2, 'Revistas', NULL, NULL, NULL, NULL, NULL, 'Magazines', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (563, 66, NULL, '2020-02-28 14:04:46.063642', 2, 'Periódicos', NULL, NULL, NULL, NULL, NULL, 'Newspapers', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (564, 66, NULL, '2020-02-28 14:04:46.063642', 2, 'Libros / Textos', NULL, NULL, NULL, NULL, NULL, 'Books / Texts', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (565, 67, NULL, '2020-02-28 14:04:46.063642', 2, 'Públicos', NULL, NULL, NULL, NULL, NULL, 'Private', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (566, 67, NULL, '2020-02-28 14:04:46.063642', 2, 'Privados', NULL, NULL, NULL, NULL, NULL, 'Public', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (567, 68, NULL, '2020-02-28 14:04:46.063642', 2, 'Discográficas', NULL, NULL, NULL, NULL, NULL, 'Records', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (568, 69, NULL, '2020-02-28 14:04:46.063642', 2, 'Festivales', NULL, NULL, NULL, NULL, NULL, 'Festivals', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (569, 69, NULL, '2020-02-28 14:04:46.063642', 2, 'Clásicos', NULL, NULL, NULL, NULL, NULL, 'Classical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (570, 69, NULL, '2020-02-28 14:04:46.063642', 2, 'Románticos', NULL, NULL, NULL, NULL, NULL, 'Románticos', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (571, 69, NULL, '2020-02-28 14:04:46.063642', 2, 'Musicales', NULL, NULL, NULL, NULL, NULL, 'Musicals', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (572, 69, NULL, '2020-02-28 14:04:46.063642', 2, 'Improvisado', NULL, NULL, NULL, NULL, NULL, 'Imprumptu', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (573, 69, NULL, '2020-02-28 14:04:46.063642', 2, 'Orquestas', NULL, NULL, NULL, NULL, NULL, 'Orchestras', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (574, 71, NULL, '2020-02-28 14:04:46.063642', 2, 'Clínico', NULL, NULL, NULL, NULL, NULL, 'Medical', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (575, 71, NULL, '2020-02-28 14:04:46.063642', 2, 'Administrativos', NULL, NULL, NULL, NULL, NULL, 'Administrative', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (576, 71, NULL, '2020-02-28 14:04:46.063642', 2, 'Deportivos', NULL, NULL, NULL, NULL, NULL, 'Sports', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (577, 71, NULL, '2020-02-28 14:04:46.063642', 2, 'Generales', NULL, NULL, NULL, NULL, NULL, 'General', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (578, 72, NULL, '2020-02-28 14:04:46.063642', 2, 'Talentos', NULL, NULL, NULL, NULL, NULL, 'Talents', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (579, 72, NULL, '2020-02-28 14:04:46.063642', 2, 'General ', NULL, NULL, NULL, NULL, NULL, 'General ', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (580, 73, NULL, '2020-02-28 14:04:46.063642', 2, 'Cirugia Plástica', NULL, NULL, NULL, NULL, NULL, 'Plastic Surgery', NULL);
INSERT INTO "dinbog"."profile_category_type" ("id", "parent_id", "description", "created", "profile_type_id", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "value", "attachment_id") VALUES (581, 73, NULL, '2020-02-28 14:04:46.063642', 2, 'Ortodoncia', NULL, NULL, NULL, NULL, NULL, 'Orthodontics', NULL);
COMMIT;

-- ----------------------------
-- Table structure for profile_connection_rol
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_connection_rol" CASCADE;
CREATE TABLE "dinbog"."profile_connection_rol" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_connection_rol_id_seq'::regclass),
  "profile_request_connection_id" int4 NOT NULL,
  "profile_connection_type_rol_id" int4 NOT NULL,
  "is_owner" int2 DEFAULT 0,
  "created" varchar COLLATE "pg_catalog"."default" DEFAULT now()
)
;

-- ----------------------------
-- Table structure for profile_connection_type_rol
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_connection_type_rol" CASCADE;
CREATE TABLE "dinbog"."profile_connection_type_rol" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_connection_type_rol_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for profile_country
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_country" CASCADE;
CREATE TABLE "dinbog"."profile_country" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_country_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "country_id" int4 NOT NULL,
  "city_id" int4 NOT NULL,
  "profile_country_type_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of profile_country
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (1, 1, 144, 1, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (2, 4, 144, 2, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (3, 5, 144, 3, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (4, 6, 144, 3, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (5, 7, 144, 2, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (6, 8, 144, 1, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (7, 9, 144, 1, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (8, 10, 144, 1, 1);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (9, 10, 144, 2, 2);
INSERT INTO "dinbog"."profile_country" ("id", "profile_id", "country_id", "city_id", "profile_country_type_id") VALUES (10, 10, 144, 3, 2);
COMMIT;

-- ----------------------------
-- Table structure for profile_country_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_country_type" CASCADE;
CREATE TABLE "dinbog"."profile_country_type" (
  "id" int2 NOT NULL DEFAULT nextval('"dinbog".profile_country_type_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) NOT NULL DEFAULT now()
)
;
COMMENT ON COLUMN "dinbog"."profile_country_type"."name" IS 'nombre del tipo';

-- ----------------------------
-- Records of profile_country_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_country_type" ("id", "name", "description", "created") VALUES (1, 'Origin', 'Lugar de nacimiento/origen', '2020-06-15 13:57:15.911646-04');
INSERT INTO "dinbog"."profile_country_type" ("id", "name", "description", "created") VALUES (2, 'Branch', 'Lugar de una sucursal de la empresa', '2020-06-15 13:58:08.980362-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_detail
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_detail" CASCADE;
CREATE TABLE "dinbog"."profile_detail" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_detail_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "field_category_id" int4 NOT NULL,
  "value" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) NOT NULL DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_detail
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_detail" ("id", "profile_id", "field_category_id", "value", "created") VALUES (1, 1, 1, 'Smooth', '2020-05-17 23:55:17.031916-04');
INSERT INTO "dinbog"."profile_detail" ("id", "profile_id", "field_category_id", "value", "created") VALUES (2, 1, 2, 'Short', '2020-05-17 23:56:06.644665-04');
INSERT INTO "dinbog"."profile_detail" ("id", "profile_id", "field_category_id", "value", "created") VALUES (3, 1, 3, 'Black', '2020-05-17 23:56:32.050945-04');
INSERT INTO "dinbog"."profile_detail" ("id", "profile_id", "field_category_id", "value", "created") VALUES (4, 1, 4, 'Hispanic', '2020-05-17 23:57:53.21646-04');
INSERT INTO "dinbog"."profile_detail" ("id", "profile_id", "field_category_id", "value", "created") VALUES (5, 1, 5, 'Brown', '2020-05-17 23:58:20.170306-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_follow
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_follow" CASCADE;
CREATE TABLE "dinbog"."profile_follow" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_follow_id_seq'::regclass),
  "owner_profile_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamp(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_follow
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_follow" ("id", "owner_profile_id", "profile_id", "created") VALUES (7, 1, 4, '2020-04-14 03:55:59.369102');
COMMIT;

-- ----------------------------
-- Table structure for profile_job_history
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_job_history" CASCADE;
CREATE TABLE "dinbog"."profile_job_history" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_job_history_id_seq'::regclass),
  "owner_profile_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now(),
  "description" varchar(1000) COLLATE "pg_catalog"."default" NOT NULL,
  "date_job" date NOT NULL,
  "category_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for profile_language
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_language" CASCADE;
CREATE TABLE "dinbog"."profile_language" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_language_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "language_id" int4 NOT NULL,
  "is_principal" int2 NOT NULL DEFAULT 0
)
;

-- ----------------------------
-- Table structure for profile_like
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_like" CASCADE;
CREATE TABLE "dinbog"."profile_like" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".profile_like_id_seq'::regclass),
  "owner_profile_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_like
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_like" ("id", "owner_profile_id", "profile_id", "created") VALUES (238, 6, 9, '2020-05-23 03:04:38.134593-04');
INSERT INTO "dinbog"."profile_like" ("id", "owner_profile_id", "profile_id", "created") VALUES (239, 6, 5, '2020-05-23 03:04:50.129479-04');
INSERT INTO "dinbog"."profile_like" ("id", "owner_profile_id", "profile_id", "created") VALUES (240, 9, 5, '2020-05-23 03:05:05.788227-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_notification" CASCADE;
CREATE TABLE "dinbog"."profile_notification" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "message" text COLLATE "pg_catalog"."default" NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_notification
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (134, 175, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 02:27:28.657147-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (135, 176, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 02:58:58.711718-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (136, 177, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 03:04:38.297863-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (137, 178, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 03:09:00.503251-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (138, 179, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 03:10:55.320648-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (139, 180, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 03:20:07.237419-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (140, 181, 5, 1, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(5) TABLA INFO:profile_like', '2020-05-09 04:35:12.185465-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (149, 238, 6, 9, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(6) TABLA INFO:profile_like', '2020-05-23 03:04:38.134593-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (150, 239, 6, 5, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(6) TABLA INFO:profile_like', '2020-05-23 03:04:50.129479-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (151, 240, 9, 5, 'trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(9) TABLA INFO:profile_like', '2020-05-23 03:05:05.788227-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (299, 90, 1, 7, 'trigger CONNECTION NEW OWNER USER_OD-> user_owner VALUE:(1) TABLA INFO:USER_OD-> USER_TO VALUE:(7)profile_request_connection', '2020-06-05 10:07:07.032877-04');
INSERT INTO "dinbog"."profile_notification" ("id", "object_id", "owner_profile_id", "to_profile_id", "message", "created") VALUES (300, 90, 7, 1, 'trigger PROFILE_CONNECTION MODIFIED status NEW OWNER USER_OD-> user_owner VALUE:(1) TABLA INFO:profile_request_connection', '2020-06-05 10:12:24.567402-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_recommendation
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_recommendation" CASCADE;
CREATE TABLE "dinbog"."profile_recommendation" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_recommendation_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "value" varchar(400) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now(),
  "is_public" bool DEFAULT true
)
;

-- ----------------------------
-- Records of profile_recommendation
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_recommendation" ("id", "profile_id", "owner_profile_id", "value", "created", "is_public") VALUES (2, 6, 4, 'PRUEBA 2', '2020-05-03 15:05:34.192854-04', 't');
INSERT INTO "dinbog"."profile_recommendation" ("id", "profile_id", "owner_profile_id", "value", "created", "is_public") VALUES (3, 6, 8, 'PRUEBA 1', '2020-05-03 15:05:23.770346-04', 't');
INSERT INTO "dinbog"."profile_recommendation" ("id", "profile_id", "owner_profile_id", "value", "created", "is_public") VALUES (27, 1, 5, 'Recomendacion Modificada para Profile Ejemplo: Recomendacion Modificada de profile', '2020-05-09 02:27:29.024-04', 'f');
INSERT INTO "dinbog"."profile_recommendation" ("id", "profile_id", "owner_profile_id", "value", "created", "is_public") VALUES (28, 1, 5, 'Recomendacion Modificada para Profile Ejemplo: Recomendacion Modificada de profile', '2020-05-09 02:58:59.078-04', 'f');
COMMIT;

-- ----------------------------
-- Table structure for profile_report
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_report" CASCADE;
CREATE TABLE "dinbog"."profile_report" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_report_id_seq'::regclass),
  "report_type_id" int4 NOT NULL,
  "detail" varchar(1000) COLLATE "pg_catalog"."default" NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_report
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (1, 4, 'Reporte A Usuario Perjudicial Prueba', 8, 6, '2020-05-03 03:46:12.678-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (2, 4, 'Reporte A Usuario Perjudicial Prueba 2', 8, 6, '2020-05-03 03:46:31.802-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (3, 1, 'reportType PROFILE Reportado Motivo N', 5, 4, '2020-05-04 00:54:05.688-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (22, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 02:27:28.963-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (23, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 02:58:58.999-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (24, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 03:04:38.59-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (25, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 03:09:00.755-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (26, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 03:10:55.577-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (27, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 03:20:07.538-04');
INSERT INTO "dinbog"."profile_report" ("id", "report_type_id", "detail", "owner_profile_id", "profile_id", "created") VALUES (28, 1, 'reportType PROFILE Reportado Motivo N', 5, 1, '2020-05-09 04:35:12.496-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_request_connection
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_request_connection" CASCADE;
CREATE TABLE "dinbog"."profile_request_connection" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_request_connection_id_seq'::regclass),
  "owner_profile_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "profile_request_status_id" int2 NOT NULL DEFAULT 0,
  "type_connection_id" int2 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_request_connection
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_request_connection" ("id", "owner_profile_id", "profile_id", "profile_request_status_id", "type_connection_id", "created") VALUES (90, 1, 7, 2, 1, '2020-06-05 10:07:07-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_request_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_request_status" CASCADE;
CREATE TABLE "dinbog"."profile_request_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_request_status_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of profile_request_status
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_request_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (1, 'Waiting', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile_request_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (2, 'Accepted', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile_request_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (3, 'Denied', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile_request_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (4, 'Restricted', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile_request_status" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (5, 'Canceled', NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for profile_share
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_share" CASCADE;
CREATE TABLE "dinbog"."profile_share" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_share_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "owner_profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_share
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (7, 6, 8, '2020-05-03 03:46:18.496-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (20, 4, 5, '2020-05-04 00:54:05.599-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (22, 1, 5, '2020-05-09 02:27:28.898-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (23, 1, 5, '2020-05-09 02:58:58.949-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (24, 1, 5, '2020-05-09 03:04:38.538-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (25, 1, 5, '2020-05-09 03:09:00.706-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (26, 1, 5, '2020-05-09 03:10:55.512-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (27, 1, 5, '2020-05-09 03:20:07.489-04');
INSERT INTO "dinbog"."profile_share" ("id", "profile_id", "owner_profile_id", "created") VALUES (28, 1, 5, '2020-05-09 04:35:12.443-04');
COMMIT;

-- ----------------------------
-- Table structure for profile_skill
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_skill" CASCADE;
CREATE TABLE "dinbog"."profile_skill" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_skill_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "skill_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for profile_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_type" CASCADE;
CREATE TABLE "dinbog"."profile_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of profile_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (1, 'Talent', 'Talento', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "dinbog"."profile_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi") VALUES (2, 'Company', 'Compañia', NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for profile_view
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."profile_view" CASCADE;
CREATE TABLE "dinbog"."profile_view" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".profile_view_id_seq'::regclass),
  "owner_profile_id" int4 NOT NULL,
  "profile_id" int4 NOT NULL,
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of profile_view
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."profile_view" ("id", "owner_profile_id", "profile_id", "created") VALUES (14, 1, 4, '2020-05-12 15:21:42.015843-04');
INSERT INTO "dinbog"."profile_view" ("id", "owner_profile_id", "profile_id", "created") VALUES (15, 1, 5, '2020-05-12 15:21:46.629396-04');
COMMIT;

-- ----------------------------
-- Table structure for report_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."report_type" CASCADE;
CREATE TABLE "dinbog"."report_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".report_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of report_type
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."report_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (1, 'I want to change my email address', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 16:35:15.608222-04');
INSERT INTO "dinbog"."report_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (2, 'I wish to eliminate my dinbog profile', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 16:35:22.243034-04');
INSERT INTO "dinbog"."report_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (3, 'Help my account', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 16:35:40.686167-04');
INSERT INTO "dinbog"."report_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (4, 'Help my casting', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 16:35:47.750038-04');
INSERT INTO "dinbog"."report_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (5, 'Why does my profile picture change?', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 16:35:52.12609-04');
INSERT INTO "dinbog"."report_type" ("id", "value", "value_spa", "value_fra", "value_ita", "value_por", "value_rus", "value_chi", "created") VALUES (6, 'Other', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 16:35:54.654554-04');
COMMIT;

-- ----------------------------
-- Table structure for site
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."site" CASCADE;
CREATE TABLE "dinbog"."site" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".site_id_seq'::regclass),
  "domain" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "active" bool NOT NULL DEFAULT false,
  "created" timestamptz(0) NOT NULL DEFAULT now(),
  "updated" timestamptz(0) DEFAULT NULL::timestamp with time zone
)
;

-- ----------------------------
-- Table structure for site_social_app
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."site_social_app" CASCADE;
CREATE TABLE "dinbog"."site_social_app" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".site_social_app_id_seq'::regclass),
  "site_id" int4 NOT NULL,
  "social_app_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for skill
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."skill" CASCADE;
CREATE TABLE "dinbog"."skill" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".skill_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(200) COLLATE "pg_catalog"."default",
  "is_active" bool NOT NULL DEFAULT true,
  "created" timestamp(6) NOT NULL DEFAULT now()
)
;

-- ----------------------------
-- Records of skill
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."skill" ("id", "name", "description", "is_active", "created") VALUES (2, 'Actor', 'Actor', 't', '2020-03-09 17:59:29.446434');
COMMIT;

-- ----------------------------
-- Table structure for social_account
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."social_account" CASCADE;
CREATE TABLE "dinbog"."social_account" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".social_account_id_seq'::regclass),
  "social_app_id" int4 NOT NULL,
  "data" text COLLATE "pg_catalog"."default",
  "last_login" timestamptz(0) NOT NULL,
  "created" timestamptz(0) NOT NULL,
  "profile_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for social_app
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."social_app" CASCADE;
CREATE TABLE "dinbog"."social_app" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".social_app_id_seq'::regclass),
  "domain" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "active" bool NOT NULL DEFAULT false,
  "client_id" varchar(200) COLLATE "pg_catalog"."default",
  "secret" varchar(200) COLLATE "pg_catalog"."default",
  "key" varchar(200) COLLATE "pg_catalog"."default",
  "created" timestamptz(0) NOT NULL DEFAULT now(),
  "updated" timestamptz(0) DEFAULT NULL::timestamp with time zone
)
;

-- ----------------------------
-- Records of social_app
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."social_app" ("id", "domain", "name", "active", "client_id", "secret", "key", "created", "updated") VALUES (38, 'www.socialapp.test.com', 'SocialApp Test', 't', '1', '1Jncy28ujYEIHUDASiwje', 'keyTest', '2020-05-18 15:23:15-04', NULL);
INSERT INTO "dinbog"."social_app" ("id", "domain", "name", "active", "client_id", "secret", "key", "created", "updated") VALUES (1, 'dinbog.com', 'Facebook', 'f', '1', 'xxxxx', 'xxxkey', '2020-03-04 18:37:46-04', NULL);
INSERT INTO "dinbog"."social_app" ("id", "domain", "name", "active", "client_id", "secret", "key", "created", "updated") VALUES (2, 'dinbog.com', 'LinkedIn', 'f', '1', 'xxxxx', 'xxxkey', '2020-03-04 18:38:01-04', NULL);
INSERT INTO "dinbog"."social_app" ("id", "domain", "name", "active", "client_id", "secret", "key", "created", "updated") VALUES (3, 'dinbog.com', 'Twitter', 'f', '1', 'xxxxx', 'xxxkey', '2020-03-04 18:38:21-04', NULL);
INSERT INTO "dinbog"."social_app" ("id", "domain", "name", "active", "client_id", "secret", "key", "created", "updated") VALUES (4, 'dinbog.com', 'Google+', 'f', '1', 'xxxxx', 'xxxkey', '2020-03-04 18:38:29-04', NULL);
COMMIT;

-- ----------------------------
-- Table structure for ticket
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."ticket" CASCADE;
CREATE TABLE "dinbog"."ticket" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".ticket_id_seq'::regclass),
  "owner_profile_id" int4,
  "ticket_status_id" int4,
  "number" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "subject" varchar(1) COLLATE "pg_catalog"."default" NOT NULL,
  "status" varchar(32) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "date_created" timestamp(0) NOT NULL,
  "date_last_change" timestamp(0) NOT NULL,
  "date_closed" timestamp(0) NOT NULL
)
;

-- ----------------------------
-- Table structure for ticket_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."ticket_status" CASCADE;
CREATE TABLE "dinbog"."ticket_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".ticket_status_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."user" CASCADE;
CREATE TABLE "dinbog"."user" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".user_id_seq'::regclass),
  "password" varchar(200) COLLATE "pg_catalog"."default",
  "first_name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "last_login" timestamptz(0) NOT NULL,
  "created" timestamptz(0) NOT NULL DEFAULT now(),
  "user_status_id" int4 NOT NULL,
  "default_lang_code" varchar(2) COLLATE "pg_catalog"."default" NOT NULL DEFAULT 'EN'::character varying
)
;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (1, 'QUJDRDEyMw==', 'nombre1', 'apellido1', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (4, 'QUJDRDEyMw==', 'nombre4', 'apellido4', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (5, 'QUJDRDEyMw==', 'nombre5', 'apellido5', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (6, 'QUJDRDEyMw==', 'nombre6', 'apellido6', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (7, 'QUJDRDEyMw==', 'nombre7', 'apellido7', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (8, 'QUJDRDEyMw==', 'nombre8', 'apellido8', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (9, 'QUJDRDEyMw==', 'nombre9', 'apellido9', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (10, 'QUJDRDEyMw==', 'nombre10', 'apellido10', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (11, 'QUJDRDEyMw==', 'nombre11', 'apellido11', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (12, 'QUJDRDEyMw==', 'nombre12', 'apellido12', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (13, 'QUJDRDEyMw==', 'nombre13', 'apellido13', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (14, 'QUJDRDEyMw==', 'nombre14', 'apellido14', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (15, 'QUJDRDEyMw==', 'nombre15', 'apellido15', '2020-04-05 21:20:43-04', '2020-04-05 21:20:52-04', 3, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (1288, 'MTIzNDU2', 'Nombre Test', 'Apellido Test', '2020-06-19 14:04:42-04', '2020-06-19 14:04:42-04', 1, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (1295, 'MTIzNDU2', 'Nombre Test', 'Apellido Test', '2020-06-19 16:24:31-04', '2020-06-19 16:24:31-04', 1, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (1310, 'MTIzNDU2', 'Nombre Test', 'Apellido Test', '2020-06-19 16:43:16-04', '2020-06-19 16:43:16-04', 1, 'EN');
INSERT INTO "dinbog"."user" ("id", "password", "first_name", "last_name", "last_login", "created", "user_status_id", "default_lang_code") VALUES (1315, 'MTIzNDU2', 'Nombre Test', 'Apellido Test', '2020-06-19 16:47:13-04', '2020-06-19 16:47:13-04', 1, 'EN');
COMMIT;

-- ----------------------------
-- Table structure for user_email
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."user_email" CASCADE;
CREATE TABLE "dinbog"."user_email" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".user_email_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "level" int2 NOT NULL DEFAULT 1,
  "email" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(500) COLLATE "pg_catalog"."default",
  "confirmed" bool NOT NULL DEFAULT false,
  "created" timestamptz(6) NOT NULL DEFAULT now()
)
;
COMMENT ON COLUMN "dinbog"."user_email"."level" IS 'Si = 1 [Email Principal]  |  Si > 1 [Email Secundario]';

-- ----------------------------
-- Records of user_email
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (4, 4, 1, 'test04@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (5, 5, 1, 'test05@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (6, 6, 1, 'test06@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (7, 7, 1, 'test07@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (8, 8, 1, 'test08@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (9, 9, 1, 'test09@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (10, 10, 1, 'test10@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (11, 11, 1, 'test11@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (12, 12, 1, 'test12@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (13, 13, 1, 'test13@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (14, 14, 1, 'test14@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (15, 15, 1, 'test15@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (1, 1, 1, 'test01@gmail.com', NULL, 't', '2020-04-05 22:10:36-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (912, 1, 2, 'luisvzla1983@gmail.com', NULL, 't', '2020-06-03 22:20:51.534-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (1471, 1288, 1, '8b6df6d5-0cf3-4af1-858d-4cfd31969c40@gmail.com', 'eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1OTI1ODk4ODEsImV4cCI6MTU5MjY3NjI4MSwiZW1haWwiOiI4YjZkZjZkNS0wY2YzLTRhZjEtODU4ZC00Y2ZkMzE5NjljNDBAZ21haWwuY29tIiwiaXNzIjoiRGluYm9nLUFQSS1qd3R0b2tlbiIsInByb2ZpbGVJZCI6IiIsInN1YiI6ImF1dGgiLCJ1c2VySWQiOiIxMjg4In0.eAmHSA0GzZfo5jz2Lo8JOB4gXTMOhdWiskO9XzTcarOlnlrmfOWJINVlVcejlEtcBFA5tf2QMIjQWjCICcL2yA', 'f', '2020-06-19 14:04:41.713-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (1496, 1310, 1, '7d3f43ea-2458-4870-8669-405c0f3e72f2@gmail.com', 'eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1OTI1OTkzOTYsImV4cCI6MTU5MjY4NTc5NiwiZW1haWwiOiI3ZDNmNDNlYS0yNDU4LTQ4NzAtODY2OS00MDVjMGYzZTcyZjJAZ21haWwuY29tIiwiaXNzIjoiRGluYm9nLUFQSS1qd3R0b2tlbiIsInByb2ZpbGVJZCI6IiIsInN1YiI6ImF1dGgiLCJ1c2VySWQiOiIxMzEwIn0.CSMLYAhdGe6VPQX8j04LTaBFhq2Stuceup_6OBMssiG8EcAGGzJ6H2tc7bRttgsPfrEHDb7Zh9CYero5AHrmmQ', 'f', '2020-06-19 16:43:16.032-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (1479, 1295, 1, 'df4f55d7-d77d-4332-b657-ecde0cdd22a9@gmail.com', 'eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1OTI1OTgyNzEsImV4cCI6MTU5MjY4NDY3MSwiZW1haWwiOiJkZjRmNTVkNy1kNzdkLTQzMzItYjY1Ny1lY2RlMGNkZDIyYTlAZ21haWwuY29tIiwiaXNzIjoiRGluYm9nLUFQSS1qd3R0b2tlbiIsInByb2ZpbGVJZCI6IiIsInN1YiI6ImF1dGgiLCJ1c2VySWQiOiIxMjk1In0.5CuLbBsn9m6Mj_l4vEtpi0Gol3SmLaz7MxD_J1T9tWWyKnnY7eVyJnvfnwNOf3ZvbFkYwJkj46-AukOmXiiDYA', 'f', '2020-06-19 16:24:31.374-04');
INSERT INTO "dinbog"."user_email" ("id", "user_id", "level", "email", "token", "confirmed", "created") VALUES (1501, 1315, 1, '3ab72576-b655-4e2b-9f2e-d56146a3555a@gmail.com', 'eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1OTI1OTk2MzIsImV4cCI6MTU5MjY4NjAzMiwiZW1haWwiOiIzYWI3MjU3Ni1iNjU1LTRlMmItOWYyZS1kNTYxNDZhMzU1NWFAZ21haWwuY29tIiwiaXNzIjoiRGluYm9nLUFQSS1qd3R0b2tlbiIsInByb2ZpbGVJZCI6IiIsInN1YiI6ImF1dGgiLCJ1c2VySWQiOiIxMzE1In0.UaBB4rSUYBRHDzJkudyvS5yFrnS_2lHdqcIGKRCbJjpm7GNoD1cmWsASftkoBgBIZh4Z_mCfRMHNWgHfYj_jVg', 'f', '2020-06-19 16:47:12.669-04');
COMMIT;

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."user_group" CASCADE;
CREATE TABLE "dinbog"."user_group" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".user_group_id_seq'::regclass),
  "group_id" int4 NOT NULL,
  "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of user_group
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (1, 1, 1);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (2, 2, 1);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (6, 2, 4);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (7, 2, 5);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (8, 2, 6);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (9, 2, 7);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (10, 2, 8);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (11, 2, 9);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (12, 2, 10);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (13, 2, 11);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (14, 2, 12);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (15, 2, 13);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (16, 2, 14);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (17, 2, 15);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (111, 1, 1);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (112, 2, 1);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (1582, 1, 1288);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (1591, 1, 1295);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (1610, 1, 1310);
INSERT INTO "dinbog"."user_group" ("id", "group_id", "user_id") VALUES (1615, 1, 1315);
COMMIT;

-- ----------------------------
-- Table structure for user_notification
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."user_notification" CASCADE;
CREATE TABLE "dinbog"."user_notification" (
  "id" int8 NOT NULL DEFAULT nextval('"dinbog".user_notification_id_seq'::regclass),
  "object_id" int4,
  "owner_profile_id" int4 NOT NULL,
  "to_profile_id" int4,
  "notification_action_id" int4,
  "message" text COLLATE "pg_catalog"."default" NOT NULL,
  "created" timestamptz(6)
)
;

-- ----------------------------
-- Table structure for user_status
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."user_status" CASCADE;
CREATE TABLE "dinbog"."user_status" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".user_status_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "is_active" bool NOT NULL DEFAULT false,
  "created" timestamptz(0) NOT NULL DEFAULT now(),
  "updated" timestamptz(0) DEFAULT NULL::timestamp with time zone
)
;

-- ----------------------------
-- Records of user_status
-- ----------------------------
BEGIN;
INSERT INTO "dinbog"."user_status" ("id", "name", "description", "is_active", "created", "updated") VALUES (1, 'status 1', 'prueba status', 't', '2020-04-05 22:09:06-04', NULL);
INSERT INTO "dinbog"."user_status" ("id", "name", "description", "is_active", "created", "updated") VALUES (2, 'status 2', 'prueba status', 't', '2020-04-05 22:09:06-04', NULL);
INSERT INTO "dinbog"."user_status" ("id", "name", "description", "is_active", "created", "updated") VALUES (3, 'status 3', 'prueba status', 't', '2020-04-05 22:09:06-04', NULL);
COMMIT;

-- ----------------------------
-- Table structure for vote_type
-- ----------------------------
DROP TABLE IF EXISTS "dinbog"."vote_type" CASCADE;
CREATE TABLE "dinbog"."vote_type" (
  "id" int4 NOT NULL DEFAULT nextval('"dinbog".vote_type_id_seq'::regclass),
  "value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "value_spa" varchar(255) COLLATE "pg_catalog"."default",
  "value_fra" varchar(255) COLLATE "pg_catalog"."default",
  "value_ita" varchar(255) COLLATE "pg_catalog"."default",
  "value_por" varchar(255) COLLATE "pg_catalog"."default",
  "value_rus" varchar(255) COLLATE "pg_catalog"."default",
  "value_chi" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamptz(6) DEFAULT now()
)
;

-- ----------------------------
-- Function structure for create_notification_album_comment
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_album_comment"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_comment"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
       DECLARE
			table_target VARCHAR := 'album_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
		
			r_count_comment INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				FOR users_records IN (SELECT id, count_comment FROM "dinbog"."album" WHERE id IN (OLD.album_id)) LOOP
							r_count_comment = users_records.count_comment;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_comment = (CASE WHEN r_count_comment > 0 THEN r_count_comment - 1 ELSE 0 END) WHERE id = OLD.album_id;
				RETURN OLD;
				
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, count_comment FROM "dinbog"."album" WHERE id IN (NEW.album_id)) LOOP
							r_count_comment = users_records.count_comment;
				END LOOP;
			

				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."album_comment" as pl
                        INNER JOIN "dinbog"."album" p ON p.id = pl.album_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
		
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_comment = COALESCE(r_count_comment,0) + 1 WHERE id = NEW.album_id;
					
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger album_comment NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
		
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_album_comment_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_album_comment_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_comment_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE			
            table_target VARCHAR := 'album_notification';
            table_name VARCHAR := TG_TABLE_NAME;
            notification_id INTEGER;
            users_records RECORD;
            countUsers INTEGER := 0;

            p_user_owner_id INTEGER;
            p_user_id INTEGER;
            
            owner_profile_id INTEGER;
            profile_id INTEGER;
            r_count_like INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				SELECT INTO 
					r_count_like 
					count_like 
				FROM "dinbog"."album_comment" WHERE id IN (OLD.album_comment_id);
				
				/*CONTADOR*/
				UPDATE "dinbog"."album_comment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.album_comment_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				SELECT INTO 
					r_count_like 
					count_like 
				FROM "dinbog"."album_comment" WHERE id IN (NEW.album_comment_id);

				/*CONTADOR*/
				UPDATE "dinbog"."album_comment" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.album_comment_id;			
				
                FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile"
                    WHERE id IN (
                        SELECT regexp_split_to_table(CONCAT(cl.profile_id,',',c.profile_id),',')::INTEGER
                        FROM "dinbog"."album_comment_like" as cl
                        INNER JOIN "dinbog"."album_comment" c ON c.id = cl.album_comment_id
                        WHERE cl.id = (NEW.id)
                    )
                ) LOOP
                    --//Se Invierten los patrones de Envio
                    IF (users_records.id = owner_profile_id)
                        THEN 
                            p_user_owner_id 	= users_records.user_id;
                        ELSE
                            profile_id  = users_records.id;
                            p_user_id   = users_records.user_id;
                    END IF;
                    countUsers = countUsers + 1;
                END LOOP;
                
                IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
                    RETURN NEW;
                END IF;

                INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
                VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger LIKE IN ALBUM COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
                RETURNING id INTO notification_id;
                
                PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
                RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_album_comment_mention
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_album_comment_mention"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_comment_mention"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
     DECLARE
			table_target VARCHAR := 'album_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			IF(TG_OP = 'INSERT') THEN
				profile_id = NEW.profile_id;
				
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile"
                    WHERE id IN (
                        SELECT regexp_split_to_table(CONCAT(m.profile_id,',',c.profile_id),',')::INTEGER
                        FROM "dinbog"."album_comment_mention" as m
                        INNER JOIN "dinbog"."album_comment" c ON c.id = m.album_comment_id
                        WHERE m.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id 	= users_records.id;
							p_user_owner_id 	= users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION IN ALBUM COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_album_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_album_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_album_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
       DECLARE
			table_target VARCHAR := 'album_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			album_id INTEGER;
			r_count_like INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				FOR users_records IN (SELECT id, count_like FROM "dinbog"."album" WHERE id IN (OLD.album_id)) LOOP
							r_count_like = users_records.count_like;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.album_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, count_like FROM "dinbog"."album" WHERE id IN (NEW.album_id)) LOOP
							r_count_like = users_records.count_like;
				END LOOP;
			
		
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(a.profile_id,',',al.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."album_like" as al
                        INNER JOIN "dinbog"."album" a ON a.id = al.album_id 
                        WHERE al.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."album" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.album_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."album_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger album_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_attachment_comment
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_attachment_comment"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_comment"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'attachment_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
      r_attachment_id INTEGER;
			owner_profile_id INTEGER;
			profile_id INTEGER;
			r_count_comments INTEGER;

		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.profile_id;
				FOR users_records IN ( SELECT id, count_comments FROM "dinbog"."attachment" WHERE id = (OLD.attachment_id)) LOOP
					IF(users_records.count_comments NOTNULL) THEN
						r_count_comments = users_records.count_comments;
					ELSE
						r_count_comments = 0;
					END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_comments = (CASE WHEN r_count_comments > 0 THEN r_count_comments - 1 ELSE 0 END) WHERE id = OLD.attachment_id;
				RETURN OLD;
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.profile_id;
				r_attachment_id = NEW.attachment_id;
				FOR users_records IN ( 
                    
                    SELECT 
                        al.*, a.count_comments,
                        COALESCE(album.profile_id,post.profile_id,event.profile_id,profile_t.profile_id) as owner_profile_id,
                        COALESCE(album.type,post.type,event.type,profile_t.type) as type_element
                    FROM "dinbog"."attachment_comment" al
                    LEFT JOIN (
                    SELECT al_at.attachment_id, al.profile_id, 'album' as type FROM "dinbog"."album" al
                    INNER JOIN "dinbog"."album_attachment" al_at ON al_at.album_id = al.id
                    WHERE al_at.attachment_id = r_attachment_id) album ON album.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT post_at.attachment_id, pt.profile_id, 'post' as type FROM "dinbog"."post" pt
                    INNER JOIN "dinbog"."post_attachment" post_at ON post_at.post_id = pt.id
                    WHERE post_at.attachment_id = r_attachment_id) post ON post.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT eve_at.attachment_id, eve.owner_profile_id as profile_id, 'event' as type FROM "dinbog"."event" eve
                    INNER JOIN "dinbog"."event_attachment" eve_at ON eve_at.event_id = eve.id
                    WHERE eve_at.attachment_id = r_attachment_id) event ON event.attachment_id = al.attachment_id

					LEFT JOIN (
                    SELECT profile_at.attachment_id, profile_at.profile_id, 'profile' as type FROM "dinbog"."profile_attachment" profile_at
                    WHERE profile_at.attachment_id = r_attachment_id) profile_t ON profile_t.attachment_id = al.attachment_id

                    INNER JOIN "dinbog"."attachment" a ON a.id = al.attachment_id
                    WHERE al.id = NEW.id

                ) LOOP
					IF(users_records.count_comments NOTNULL) THEN
						r_count_comments = users_records.count_comments;
						profile_id = users_records.owner_profile_id;
					END IF;
				END LOOP;

				FOR users_records IN ( SELECT id, user_id FROM "dinbog"."profile" WHERE id IN (owner_profile_id, profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_comments = COALESCE(r_count_comments,0) + 1 WHERE id = r_attachment_id;

				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);				
				
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_attachment_comment_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_attachment_comment_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_comment_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'attachment_notification';
            table_name VARCHAR := TG_TABLE_NAME;
            notification_id INTEGER;
            users_records RECORD;
            countUsers INTEGER := 0;

            p_user_owner_id INTEGER;
            p_user_id INTEGER;
            
            owner_profile_id INTEGER;
            profile_id INTEGER;
            r_count_like INTEGER;
    BEGIN
        IF(TG_OP = 'DELETE') THEN
            SELECT INTO 
                r_count_like
                count_like 
            FROM "dinbog"."attachment_comment" WHERE id = OLD.attachment_comment_id;
            
            /*CONTADOR*/
            UPDATE "dinbog"."attachment_comment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) 
            WHERE id = OLD.attachment_comment_id;
            
            RETURN OLD;
        END IF;
        
        IF(TG_OP = 'INSERT') THEN
            owner_profile_id = NEW.profile_id;
            SELECT INTO 
                r_count_like
                count_like 
            FROM "dinbog"."attachment_comment" WHERE id = NEW.attachment_comment_id;

            /*CONTADOR*/
            UPDATE "dinbog"."attachment_comment" SET count_like = COALESCE(r_count_like,0) + 1 
            WHERE id = NEW.attachment_comment_id;

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile"
                WHERE id IN (
                    SELECT regexp_split_to_table(CONCAT(cl.profile_id,',',c.profile_id),',')::INTEGER
                    FROM "dinbog"."attachment_comment_like" as cl
                    INNER JOIN "dinbog"."attachment_comment" c ON c.id = cl.attachment_comment_id
                    WHERE cl.id = (NEW.id)
                )
            ) LOOP
                --//Se Invierten los patrones de Envio
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id 	= users_records.user_id;
                    ELSE
                        profile_id  = users_records.id;
                        p_user_id   = users_records.user_id;
                END IF;
                countUsers = countUsers + 1;
            END LOOP;
            
            IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
                RETURN NEW;
            END IF;

            INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger LIKE IN ATTACHMENT COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
            RETURNING id INTO notification_id;
            
            PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
            RETURN NEW;
        END IF;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_attachment_comment_mention
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_attachment_comment_mention"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_comment_mention"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
     DECLARE
			table_target VARCHAR := 'attachment_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			IF(TG_OP = 'INSERT') THEN
				profile_id = NEW.profile_id;
				
				FOR users_records IN (
						SELECT id, user_id 
						FROM "dinbog"."profile"
						WHERE id IN (
								SELECT regexp_split_to_table(CONCAT(m.profile_id,',',c.profile_id),',')::INTEGER
								FROM "dinbog"."attachment_comment_mention" as m
								INNER JOIN "dinbog"."attachment_comment" c ON c.id = m.attachment_comment_id
								WHERE m.id = (NEW.id)
						)
        ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id 	= users_records.id;
							p_user_owner_id 	= users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
			
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				
				INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION IN ATTACHMENT COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_attachment_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_attachment_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_attachment_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'attachment_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			r_attachment_id INTEGER;
			owner_profile_id INTEGER;
			profile_id INTEGER;
			r_count_like INTEGER;

		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.profile_id;
				FOR users_records IN ( SELECT id, count_like FROM "dinbog"."attachment" WHERE id = (OLD.attachment_id)) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
					ELSE
						r_count_like = 0;
					END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.attachment_id;
				RETURN OLD;
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.profile_id;
				r_attachment_id = NEW.attachment_id;
				FOR users_records IN ( 
                    
                    SELECT 
                        al.*, a.count_like,
                        COALESCE(album.profile_id,post.profile_id,event.profile_id,profile_t.profile_id) as owner_profile_id,
                        COALESCE(album.type,post.type,event.type,profile_t.type) as type_element
                    FROM "dinbog"."attachment_like" al
                    LEFT JOIN (
                    SELECT al_at.attachment_id, al.profile_id, 'album' as type FROM "dinbog"."album" al
                    INNER JOIN "dinbog"."album_attachment" al_at ON al_at.album_id = al.id
                    WHERE al_at.attachment_id = r_attachment_id) album ON album.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT post_at.attachment_id, pt.profile_id, 'post' as type FROM "dinbog"."post" pt
                    INNER JOIN "dinbog"."post_attachment" post_at ON post_at.post_id = pt.id
                    WHERE post_at.attachment_id = r_attachment_id) post ON post.attachment_id = al.attachment_id
                    LEFT JOIN (
                    SELECT eve_at.attachment_id, eve.owner_profile_id as profile_id, 'event' as type FROM "dinbog"."event" eve
                    INNER JOIN "dinbog"."event_attachment" eve_at ON eve_at.event_id = eve.id
                    WHERE eve_at.attachment_id = r_attachment_id) event ON event.attachment_id = al.attachment_id
										LEFT JOIN (
                    SELECT profile_at.attachment_id, profile_at.profile_id, 'profile' as type FROM "dinbog"."profile_attachment" profile_at
                    WHERE profile_at.attachment_id = r_attachment_id) profile_t ON profile_t.attachment_id = al.attachment_id
                    INNER JOIN "dinbog"."attachment" a ON a.id = al.attachment_id
                    WHERE al.id = NEW.id

                ) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
						profile_id = users_records.owner_profile_id;
					END IF;
				END LOOP;

				FOR users_records IN ( SELECT id, user_id FROM "dinbog"."profile" WHERE id IN (owner_profile_id, profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."attachment" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = r_attachment_id;
					
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
			
				INSERT INTO "dinbog"."attachment_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger attachment_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);				

				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_event_invitation
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_event_invitation"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_event_invitation"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'event_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
							
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER := NEW.profile_id;
			
		BEGIN
			
			IF(TG_OP = 'INSERT') THEN
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',e.owner_profile_id),',')::INTEGER
                        FROM
                            "dinbog"."event_invitation" as pl
                        INNER JOIN "dinbog"."event" e ON e.id = pl.event_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id = users_records.id;
							p_user_owner_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger NOTIFICATION NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;

			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_event_participant
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_event_participant"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_event_participant"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE		
			table_target VARCHAR := 'event_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			connections_profile RECORD;
			connection_profile_id INTEGER;
			
			event_id INTEGER;
			status_participant INTEGER;
			r_count_participant INTEGER;
    BEGIN
        IF(TG_OP = 'DELETE') THEN
            event_id = OLD.event_id;

            SELECT INTO
                event_id, r_count_participant
                e.id, e.count_participant
            FROM "dinbog"."event" as e
            WHERE e.id = event_id;
						
						/*CONTADOR*/
            UPDATE "dinbog"."event" SET count_participant = (CASE WHEN r_count_participant > 0 THEN r_count_participant - 1 ELSE 0 END) WHERE id = event_id;

            RETURN OLD;
        END IF;
        
        IF(TG_OP = 'UPDATE') THEN
            event_id = NEW.event_id;
            owner_profile_id = NEW.profile_id;

            SELECT INTO 
                r_count_participant
                count_participant 
            FROM "dinbog"."event" WHERE id = (event_id);

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (
                    SELECT
                        regexp_split_to_table(CONCAT(event_p.profile_id,',',a.owner_profile_id),',')::INTEGER
                    FROM
                        "dinbog"."event_participant" as event_p
                    INNER JOIN "dinbog"."event" a ON a.id = event_p.event_id 
                    WHERE event_p.id = (NEW.id)
                )
            ) LOOP
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id = users_records.user_id;
                    ELSE 
                        profile_id = users_records.user_id;
                        p_user_id = users_records.user_id;
                END IF;
							countUsers = countUsers + 1;
						END LOOP;
						
						          
						IF (NEW.event_participant_status_id = 9 OR NEW.event_participant_status_id = 13) THEN
								IF (NEW.event_participant_status_id = 9) THEN
									table_name = 'event_participant_acepted';
									/*CONTADOR*/
									UPDATE "dinbog"."event" SET count_participant = COALESCE(r_count_participant,0) + 1 WHERE id = event_id; 
									
									IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
										RETURN NEW;
									END IF;
									
									--Se Invierten los parametros de owner a profile y profile a owner
									INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
									VALUES(NEW.id, profile_id, owner_profile_id, CONCAT('trigger EVENT_PARTICIPANT MODIFIED ACCEPTED status NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
									PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_id, p_user_owner_id, notification_id, NEW.created);
								
								ELSE
									
									table_name = 'event_participant_confirmed_attendance';
									INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger EVENT_PARTICIPANT MODIFIED ATTEMPT NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
									PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
								END IF;
   
            END IF;

            RETURN NEW;
        END IF;

        IF(TG_OP = 'INSERT') THEN
            event_id = NEW.event_id;
			owner_profile_id = NEW.profile_id;
            SELECT INTO 
                r_count_participant
                count_participant 
            FROM "dinbog"."event" WHERE id = (event_id);

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (
                    SELECT
                        regexp_split_to_table(CONCAT(event_p.profile_id,',',a.owner_profile_id),',')::INTEGER
                    FROM "dinbog"."event_participant" as event_p
                    INNER JOIN "dinbog"."event" a ON a.id = event_p.event_id 
                    WHERE event_p.id = (NEW.id)
                )
            ) LOOP
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id = users_records.user_id;
                    ELSE 
                        profile_id = users_records.user_id;
                        p_user_id = users_records.user_id;
                END IF;
							countUsers = countUsers + 1;
						END LOOP;
						
						/*CONTADOR*/
						UPDATE "dinbog"."event" SET count_participant = (CASE WHEN r_count_participant > 0 THEN COALESCE(r_count_participant,0) + 1 ELSE 0 END) 
						WHERE id = event_id;

						IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
							RETURN NEW;
						END IF;
						
            INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger EVENT_PARTICIPANT MODIFIED status NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
            PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
						
						
						connection_profile_id = owner_profile_id;
						FOR connections_profile IN 
						(SELECT
								p.user_id,
								regexp_split_to_table(CONCAT(pc.profile_id,',',pc.owner_profile_id),',')::INTEGER as profile_id
						FROM "dinbog"."profile_request_connection" as pc
						INNER JOIN "dinbog"."profile" p ON p.id = pc.profile_id 
						WHERE pc.owner_profile_id = connection_profile_id 
						OR pc.profile_id = connection_profile_id 
						AND pc.type_connection_id = 2 AND P.profile_type_id = 2 LIMIT 1) LOOP
						
							IF(connections_profile.user_id IS NOT NULL) THEN 
								table_name = 'event_participant_profile_connection';
								p_user_id = connections_profile.user_id;
								PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
							END IF;
						
						END LOOP;
						
            RETURN NEW;
        END IF;
        
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_event_status
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_event_status"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_event_status"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE		
			table_target VARCHAR := 'event_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			
			p_user_owner_id INTEGER;
			owner_profile_id INTEGER;
			
			connections_profile RECORD;
			connection_profile_id INTEGER;
			
			event_id INTEGER;
			event_status_id INTEGER;
    BEGIN

        IF(TG_OP = 'UPDATE') THEN
            event_id = NEW.id;
						event_status_id = NEW.event_status_id;
						owner_profile_id = NEW.owner_profile_id;

            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (NEW.owner_profile_id)
            ) LOOP
							p_user_owner_id = users_records.user_id;
						END LOOP;
						
								          
						IF (event_status_id = 3 OR event_status_id = 4) THEN
								IF (event_status_id = 3) THEN
									table_name = 'event_approved';
								ELSIF (event_status_id = 4) THEN
									table_name = 'event_denied';
								END IF;
								
								INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
								VALUES(NEW.id, owner_profile_id, owner_profile_id, CONCAT('trigger EVENT CHANGE STATUS-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
								PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_owner_id, notification_id, NEW.created);					
            END IF;

            RETURN NEW;
        END IF;

        IF(TG_OP = 'INSERT') THEN
            event_id = NEW.id;
						event_status_id = NEW.event_status_id;
						owner_profile_id = NEW.owner_profile_id;
						table_name = 'event_wait';
						
            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile" 
                WHERE id IN (NEW.owner_profile_id)
            ) LOOP
							p_user_owner_id = users_records.user_id;
						END LOOP;
						
            INSERT INTO "dinbog"."event_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, owner_profile_id, CONCAT('trigger EVENT CHANGE STATUS-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
						PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_owner_id, notification_id, NEW.created);
						
            RETURN NEW;
        END IF;
        
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_poll_vote
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_poll_vote"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_poll_vote"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'poll_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER := NEW.owner_profile_id;
			profile_id INTEGER;
			r_count_vote INTEGER;

		BEGIN
			IF(TG_OP = 'DELETE') THEN

				FOR users_records IN ( SELECT id, vote_count FROM "dinbog"."poll_participant" WHERE id = (OLD.poll_participant_id)) LOOP
					IF(users_records.vote_count NOTNULL) THEN
						r_count_vote = users_records.vote_count;
					ELSE
						r_count_vote = 0;
					END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."poll_participant" SET vote_count = (CASE WHEN r_count_vote > 0 THEN r_count_vote - 1 ELSE 0 END) WHERE id = OLD.poll_participant_id;
				RETURN OLD;
			END IF;
		
			IF(TG_OP = 'INSERT') THEN

				FOR users_records IN ( SELECT id, vote_count FROM "dinbog"."poll_participant" WHERE id = (NEW.poll_participant_id)) LOOP
					IF(users_records.vote_count NOTNULL) THEN
						r_count_vote = users_records.vote_count;
					ELSE
						r_count_vote = 0;
					END IF;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."poll_participant" SET vote_count = COALESCE(r_count_vote,0) + 1 WHERE id = NEW.poll_participant_id;
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_post
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_post"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE		
			users_records RECORD;
			profile_id INTEGER;
			to_count_post INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
                profile_id = OLD.profile_id;
				FOR users_records IN (SELECT id, user_id, count_post FROM "dinbog"."profile" WHERE id IN (OLD.profile_id)) LOOP
							profile_id = users_records.id;
							to_count_post = users_records.count_post;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_post = (CASE WHEN to_count_post > 0 THEN to_count_post - 1 ELSE 0 END) WHERE id = profile_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
                profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, user_id, count_post FROM "dinbog"."profile" WHERE id IN (profile_id)) LOOP
							profile_id = users_records.id;
							to_count_post = users_records.count_post;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_post = COALESCE(to_count_post,0) + 1 WHERE id = profile_id;
				RETURN NEW;
			
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_post_comment
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_post_comment"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_comment"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_comment INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN

				FOR users_records IN ( SELECT id, count_comment FROM "dinbog"."post" WHERE id = (OLD.post_id)) LOOP
					IF(users_records.count_comment NOTNULL) THEN
						r_count_comment = users_records.count_comment;
					ELSE
						r_count_comment = 0;
					END IF;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_comment = (CASE WHEN r_count_comment > 0 THEN r_count_comment - 1 ELSE 0 END) WHERE id = OLD.post_id;
				
				RETURN OLD;
				
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.profile_id;
				FOR users_records IN ( SELECT id, count_comment FROM "dinbog"."post" WHERE id = (NEW.post_id)) LOOP
					IF(users_records.count_comment NOTNULL) THEN
						r_count_comment = users_records.count_comment;
					ELSE
						r_count_comment = 0;
					END IF;
				END LOOP;
				
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."post_comment" as pl
                        INNER JOIN "dinbog"."post" p ON p.id = pl.post_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_comment = COALESCE(r_count_comment,0) + 1 WHERE id = NEW.post_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger post_comment NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_post_comment_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_post_comment_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_comment_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
        table_target VARCHAR := 'post_notification';
        table_name VARCHAR := TG_TABLE_NAME;
        notification_id INTEGER;
        users_records RECORD;
        countUsers INTEGER := 0;

        p_user_owner_id INTEGER;
        p_user_id INTEGER;
        
        owner_profile_id INTEGER;
        profile_id INTEGER;
        r_count_like INTEGER;
    BEGIN
        IF(TG_OP = 'DELETE') THEN
            SELECT INTO 
                r_count_like 
                count_like 
            FROM "dinbog"."post_comment" WHERE id IN (OLD.post_comment_id);
            
            /*CONTADOR*/
            UPDATE "dinbog"."post_comment" SET count_like = (CASE WHEN r_count_like > 0 THEN r_count_like - 1 ELSE 0 END) WHERE id = OLD.post_comment_id;
            RETURN OLD;
        END IF;
        
        IF(TG_OP = 'INSERT') THEN
            owner_profile_id = NEW.profile_id;

            SELECT INTO 
                r_count_like 
                count_like 
            FROM "dinbog"."post_comment" WHERE id IN (NEW.post_comment_id);

            /*CONTADOR*/
            UPDATE "dinbog"."post_comment" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.post_comment_id;
            				
            FOR users_records IN (
                SELECT id, user_id 
                FROM "dinbog"."profile"
                WHERE id IN (
                    SELECT regexp_split_to_table(CONCAT(cl.profile_id,',',c.profile_id),',')::INTEGER
                    FROM "dinbog"."post_comment_like" as cl
                    INNER JOIN "dinbog"."post_comment" c ON c.id = cl.post_comment_id
                    WHERE cl.id = (NEW.id)
                )
            ) LOOP
                --//Se Invierten los patrones de Envio
                IF (users_records.id = owner_profile_id)
                    THEN 
                        p_user_owner_id = users_records.user_id;
                    ELSE
                        profile_id  = users_records.id;
                        p_user_id   = users_records.user_id;
                END IF;
                countUsers = countUsers + 1;
            END LOOP;

            IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
                RETURN NEW;
            END IF;

            INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
            VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger LIKE IN POST COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
            RETURNING id INTO notification_id;
            
            PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
            RETURN NEW;
        END IF;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_post_comment_mention
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_post_comment_mention"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_comment_mention"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
     DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			IF(TG_OP = 'INSERT') THEN
				profile_id = NEW.profile_id;
				
				FOR users_records IN (
						SELECT id, user_id 
						FROM "dinbog"."profile"
						WHERE id IN (
								SELECT regexp_split_to_table(CONCAT(m.profile_id,',',c.profile_id),',')::INTEGER
								FROM "dinbog"."post_comment_mention" as m
								INNER JOIN "dinbog"."post_comment" c ON c.id = m.post_comment_id
								WHERE m.id = (NEW.id)
						)
        ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id 	= users_records.id;
							p_user_owner_id 	= users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
			
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				
				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION IN POST COMMENT-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_post_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_post_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_like INTEGER;
		BEGIN
			IF(TG_OP = 'DELETE') THEN

				FOR users_records IN ( SELECT id, count_like FROM "dinbog"."post" WHERE id = (OLD.post_id)) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
					ELSE
						r_count_like = 0;
					END IF;
				END LOOP;

				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_like = (CASE WHEN r_count_like > 0 THEN COALESCE(r_count_like) - 1 ELSE 0 END) WHERE id = OLD.post_id;
				
				RETURN OLD;
				
			END IF;
		
			IF(TG_OP = 'INSERT') THEN
                owner_profile_id = NEW.profile_id;
				FOR users_records IN ( SELECT id, count_like FROM "dinbog"."post" WHERE id = (NEW.post_id)) LOOP
					IF(users_records.count_like NOTNULL) THEN
						r_count_like = users_records.count_like;
					ELSE
						r_count_like = 0;
					END IF;
				END LOOP;
				
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."post_like" as pl
                        INNER JOIN "dinbog"."post" p ON p.id = pl.post_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE
							profile_id = users_records.id;
							p_user_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."post" SET count_like = COALESCE(r_count_like,0) + 1 WHERE id = NEW.post_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger post_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);		
				RETURN NEW;
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_post_mention
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_post_mention"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_post_mention"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'post_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER := NEW.profile_id;
			
		BEGIN
			
			IF(TG_OP = 'INSERT') THEN
				FOR users_records IN (
                    SELECT id, user_id 
                    FROM "dinbog"."profile" 
                    WHERE id IN (
                        SELECT
                            regexp_split_to_table(CONCAT(pl.profile_id,',',p.profile_id),',')::INTEGER
                        FROM
                            "dinbog"."post_mention" as pl
                        INNER JOIN "dinbog"."post" p ON p.id = pl.post_id 
                        WHERE pl.id = (NEW.id)
                    )
                ) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							owner_profile_id = users_records.id;
							p_user_owner_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."post_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger MENTION NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;

			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_profile_connection
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_profile_connection"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_connection"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE		
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_owner_profile_connect INTEGER;
			r_count_profile_connect INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.owner_profile_id;
				profile_id = OLD.profile_id;
				
				FOR users_records IN (SELECT id, user_id, count_conections FROM "dinbog"."profile" WHERE id IN (OLD.owner_profile_id, OLD.profile_id)) LOOP
					
					IF (users_records.id = OLD.owner_profile_id)
						THEN 
							owner_profile_id = users_records.id;
							r_count_owner_profile_connect = users_records.count_conections;
						ELSE 
							profile_id = users_records.id;
							r_count_profile_connect = users_records.count_conections;
					END IF;
				
				END LOOP;	
				IF (OLD.profile_request_status_id = 2) THEN
					/*CONTADOR*/
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_owner_profile_connect > 0 THEN r_count_owner_profile_connect - 1 ELSE 0 END) WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_profile_connect > 0 THEN r_count_profile_connect - 1 ELSE 0 END) WHERE id = profile_id;
				END IF;
					
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				
				FOR users_records IN (SELECT id, user_id, count_conections FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
							r_count_owner_profile_connect = users_records.count_conections;
						ELSE 
							p_user_id = users_records.user_id;
							r_count_profile_connect = users_records.count_conections;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
			
			
				IF (NEW.profile_request_status_id = 2) THEN
					/*CONTADOR*/
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_owner_profile_connect,0) + 1 WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_profile_connect,0) + 1  WHERE id = profile_id;
				END IF;
			
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				--RAISE NOTICE 'Value: %', user_owner_id;
				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(
					NEW.id, owner_profile_id, profile_id, CONCAT(
					'trigger CONNECTION NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:'
					,'USER_OD-> USER_TO VALUE:(',p_user_id,')',table_name
					)
				) 
				RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				
				RETURN NEW;
			
			END IF;
			
			IF(TG_OP = 'UPDATE') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				
				FOR users_records IN (SELECT id, user_id, count_conections FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
							r_count_owner_profile_connect = users_records.count_conections;
						ELSE 
							p_user_id = users_records.user_id;
							r_count_profile_connect = users_records.count_conections;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				IF (OLD.profile_request_status_id != NEW.profile_request_status_id AND NEW.profile_request_status_id = 2) THEN
					table_name = 'profile_request_connection_accepted';
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_owner_profile_connect,0) + 1 WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = COALESCE(r_count_profile_connect,0) + 1  WHERE id = profile_id;
				END IF;
				
				/*CONTADOR*/
				IF (OLD.profile_request_status_id != NEW.profile_request_status_id AND NEW.profile_request_status_id <> 2) THEN
					
					IF(NEW.profile_request_status_id = 3) THEN
						table_name = 'profile_request_connection_denied';
					ELSIF (NEW.profile_request_status_id = 5) THEN
						table_name = 'profile_request_connection_cancelled';
					END IF;
					
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_owner_profile_connect > 0 THEN r_count_owner_profile_connect - 1 ELSE 0 END) WHERE id = owner_profile_id;
					UPDATE "dinbog"."profile" SET count_conections = (CASE WHEN r_count_profile_connect > 0 THEN r_count_profile_connect - 1 ELSE 0 END) WHERE id = profile_id;
				END IF;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
				
				--Se Invierten los parametros de owner a profile y profile a owner
				INSERT INTO "dinbog"."profile_notification" (object_id, to_profile_id, owner_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger PROFILE_CONNECTION MODIFIED status NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
				
				--Se Invierten los parametros de owner a profile y profile a owner
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_id, p_user_owner_id, notification_id, NEW.created);
				RETURN NEW;
			
			END IF;
			
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_profile_follow
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_profile_follow"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_follow"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE		
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			r_count_follow INTEGER;
			r_count_followers INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.owner_profile_id;
				profile_id = OLD.profile_id;
				FOR users_records IN (SELECT id, user_id, count_follow, count_followers FROM "dinbog"."profile" WHERE id IN (OLD.owner_profile_id, OLD.profile_id)) LOOP
					
					IF (users_records.id = OLD.owner_profile_id)
						THEN 
							owner_profile_id = users_records.id;
							r_count_follow = users_records.count_follow;
						ELSE 
							profile_id = users_records.id;
							r_count_followers = users_records.count_followers;
					END IF;
				
				END LOOP;
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_follow = (CASE WHEN r_count_follow > 0 THEN r_count_follow - 1 ELSE 0 END) WHERE id = owner_profile_id;
				UPDATE "dinbog"."profile" SET count_followers = (CASE WHEN r_count_followers > 0 THEN r_count_followers - 1 ELSE 0 END) WHERE id = profile_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, user_id, count_follow, count_followers FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
							r_count_follow = users_records.count_follow;
						ELSE 
							p_user_id = users_records.user_id;
							r_count_followers = users_records.count_followers;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_follow = COALESCE(r_count_follow,0) + 1 WHERE id = owner_profile_id;
				UPDATE "dinbog"."profile" SET count_followers = COALESCE(r_count_followers,0) + 1  WHERE id = profile_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
			
				--RAISE NOTICE 'Value: %', user_owner_id;
				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger profile_follow NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_profile_like
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_profile_like"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_like"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE		
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;
			
			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
			to_count_like INTEGER;
			
		BEGIN
			IF(TG_OP = 'DELETE') THEN
				owner_profile_id = OLD.owner_profile_id;
				profile_id = OLD.profile_id;
			
				FOR users_records IN (SELECT id, user_id, count_like FROM "dinbog"."profile" WHERE id IN (OLD.owner_profile_id, OLD.profile_id)) LOOP
					IF (users_records.id = OLD.profile_id)
						THEN 
							profile_id = users_records.id;
							to_count_like = users_records.count_like;
						END IF;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_like = (CASE WHEN to_count_like > 0 THEN to_count_like - 1 ELSE 0 END) WHERE id = profile_id;
				RETURN OLD;
			END IF;
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				FOR users_records IN (SELECT id, user_id, count_like FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = owner_profile_id)
						THEN 
							p_user_owner_id = users_records.user_id;
						ELSE 
							p_user_id = users_records.user_id;
							to_count_like = users_records.count_like;
						END IF;
						countUsers = countUsers + 1;
				END LOOP;
				
				/*CONTADOR*/
				UPDATE "dinbog"."profile" SET count_like = COALESCE(to_count_like,0) + 1 WHERE id = profile_id;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;
			
				--RAISE NOTICE 'Value: %', user_owner_id;
				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger profile_like NEW OWNER USER_OD-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) RETURNING id INTO notification_id;
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				
				RETURN NEW;
			
			END IF;
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for create_notification_profile_recommendation
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."create_notification_profile_recommendation"() CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."create_notification_profile_recommendation"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
    DECLARE
			table_target VARCHAR := 'profile_notification';
			table_name VARCHAR := TG_TABLE_NAME;
			notification_id INTEGER;
			users_records RECORD;
			countUsers INTEGER := 0;

			p_user_owner_id INTEGER;
			p_user_id INTEGER;
			
			owner_profile_id INTEGER;
			profile_id INTEGER;
			
		BEGIN
			
			IF(TG_OP = 'INSERT') THEN
				owner_profile_id = NEW.owner_profile_id;
				profile_id = NEW.profile_id;
				
				FOR users_records IN (SELECT id, user_id FROM "dinbog"."profile" WHERE id IN (NEW.owner_profile_id, NEW.profile_id)) LOOP
					IF (users_records.id = profile_id)
						THEN 
							p_user_id = users_records.user_id;
						ELSE
							p_user_owner_id = users_records.user_id;
					END IF;
					countUsers = countUsers + 1;
				END LOOP;
				
				IF(countUsers <> 2 OR owner_profile_id = profile_id OR p_user_owner_id = p_user_id) THEN 
					RETURN NEW;
				END IF;

				INSERT INTO "dinbog"."profile_notification" (object_id, owner_profile_id, to_profile_id, message) 
				VALUES(NEW.id, owner_profile_id, profile_id, CONCAT('trigger Reccommendation-> user_owner VALUE:(',p_user_owner_id,') TABLA INFO:',table_name)) 
				RETURNING id INTO notification_id;
				
				PERFORM "dinbog"."notification_general"(table_target, table_name, p_user_owner_id, p_user_id, notification_id, NEW.created);
				RETURN NEW;
			END IF;
			
		END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for get_interactions_with_profile
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."get_interactions_with_profile"("token_profile_id" int4, "profile_visit_id" int4) CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."get_interactions_with_profile"("token_profile_id" int4, "profile_visit_id" int4)
  RETURNS TABLE("profile_token" int4, "profile_visit" int4, "follow" bool, "follower" bool, "connection" bool) AS $BODY$
	DECLARE
		/*
		* token_profile_id = id del perfil que hace la solicitud por su token
		*	profile_visit_id = id del perfil al cual se hace la solicitud o comparación de busqueda
		*/
		profile_token INTEGER:= 0;
		profile_visit INTEGER:= 0;
		follow BOOLEAN:= false;
		follower BOOLEAN:= false;
		connection BOOLEAN:= false;
		x1 RECORD;
	BEGIN
		IF (token_profile_id IS NULL OR profile_visit_id IS NULL) THEN
				RETURN QUERY SELECT profile_token, profile_visit, follow, follower, connection;
		END IF;
	
		FOR x1 IN (SELECT 
			token_profile_id AS profile_token,
			profile_visit_id AS profile_search,
			CASE 
				WHEN (pfollow.owner_profile_id = token_profile_id) THEN true ELSE FALSE END as follow,
			CASE 
				WHEN (pfollower.profile_id = token_profile_id) THEN true ELSE FALSE END as follower,
			CASE 
				WHEN (pc.id IS NOT NULL) THEN TRUE ELSE FALSE END AS connection
		FROM profile p
		LEFT JOIN profile_follow pfollow ON pfollow.owner_profile_id = p.id AND pfollow.owner_profile_id = token_profile_id AND pfollow.profile_id = profile_visit_id
		LEFT JOIN profile_follow pfollower ON pfollower.profile_id = p.id AND pfollower.owner_profile_id = profile_visit_id AND pfollower.profile_id = token_profile_id
		LEFT JOIN profile_request_connection pc ON 
			(pc.owner_profile_id = p.id OR pc.profile_id = p.id) 
			AND (
				(pc.owner_profile_id = token_profile_id AND pc.profile_id = 9) OR
				(pc.owner_profile_id = profile_visit_id AND pc.profile_id = token_profile_id)
			)
			AND (pc.profile_request_status_id = 2)
		WHERE p.id = token_profile_id) 
		LOOP
			profile_token = x1.profile_token;
			profile_visit = x1.profile_search;
			follow = x1.follow;
			follower = x1.follower;
			connection = x1.connection;
			RETURN QUERY SELECT profile_token, profile_visit, follow, follower, connection;
		END LOOP;
END; 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notification_general
-- ----------------------------
DROP FUNCTION IF EXISTS "dinbog"."notification_general"("field_notification_name" varchar, "table_name_action" varchar, "p_user_owner_id" int4, "p_user_id" int4, "notification_id" int4, "created_notification_date" timestamptz) CASCADE;
CREATE OR REPLACE FUNCTION "dinbog"."notification_general"("field_notification_name" varchar, "table_name_action" varchar, "p_user_owner_id" int4, "p_user_id" int4, "notification_id" int4, "created_notification_date" timestamptz)
  RETURNS "pg_catalog"."void" AS $BODY$
DECLARE
	query_str VARCHAR;
	
	notification_action_record RECORD;
	action_id INTEGER;
	
	notification_field RECORD;
	notification_field_name VARCHAR:='';
	notification_message_code VARCHAR;
BEGIN
		FOR notification_field IN (SELECT column_name FROM  information_schema.columns 
		WHERE table_schema = 'dinbog' AND table_name = 'notification' AND column_name LIKE CONCAT('%',field_notification_name,'%id'))
		LOOP 
			notification_field_name = notification_field.column_name;
		END LOOP; 
		
		FOR notification_action_record IN (SELECT na.id, nm.code_message
			FROM "dinbog"."notification_action" as na 
			INNER JOIN "dinbog"."notification_type" nt ON nt.id = na.notification_type_id
			INNER JOIN "dinbog"."notification_message" nm ON na.id = nm.notification_action_id
		WHERE CONCAT(nt.value,'_',na.value) = table_name_action OR na.key = table_name_action LIMIT 1)
		LOOP
			action_id = notification_action_record.id;
			notification_message_code = notification_action_record.code_message;
		END LOOP;
		
		IF (notification_field_name <> '' AND action_id > 0 AND notification_message_code IS NOT NULL)
		THEN 
			query_str = CONCAT(
			'INSERT INTO "dinbog"."notification" 
			("owner_user_id", "to_user_id","notification_action_id","',notification_field_name,'","code_message", "date_notification") 
			VALUES (',p_user_owner_id,' 
			,',p_user_id,'
			,',action_id,'
			,',notification_id,'
			,',notification_message_code,'
			,''',created_notification_date,''');'
			);
			EXECUTE(query_str);
		ELSE
			ROLLBACK;
		END IF;
	
		EXCEPTION WHEN OTHERS THEN
		BEGIN
			RAISE NOTICE 'Error Rollback Exception On notification_general';
		END;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "dinbog"."album_attachment_id_seq"
OWNED BY "dinbog"."album_attachment"."id";
SELECT setval('"dinbog"."album_attachment_id_seq"', 277, true);
ALTER SEQUENCE "dinbog"."album_comment_id_seq"
OWNED BY "dinbog"."album_comment"."id";
SELECT setval('"dinbog"."album_comment_id_seq"', 819, true);
ALTER SEQUENCE "dinbog"."album_comment_like_id_seq"
OWNED BY "dinbog"."album_comment_like"."id";
SELECT setval('"dinbog"."album_comment_like_id_seq"', 6, false);
ALTER SEQUENCE "dinbog"."album_comment_mention_id_seq"
OWNED BY "dinbog"."album_comment_mention"."id";
SELECT setval('"dinbog"."album_comment_mention_id_seq"', 6, false);
ALTER SEQUENCE "dinbog"."album_id_seq"
OWNED BY "dinbog"."album"."id";
SELECT setval('"dinbog"."album_id_seq"', 366, true);
ALTER SEQUENCE "dinbog"."album_like_id_seq"
OWNED BY "dinbog"."album_like"."id";
SELECT setval('"dinbog"."album_like_id_seq"', 275, true);
ALTER SEQUENCE "dinbog"."album_notification_id_seq"
OWNED BY "dinbog"."album_notification"."id";
SELECT setval('"dinbog"."album_notification_id_seq"', 29, true);
ALTER SEQUENCE "dinbog"."album_report_id_seq"
OWNED BY "dinbog"."album_report"."id";
SELECT setval('"dinbog"."album_report_id_seq"', 228, true);
ALTER SEQUENCE "dinbog"."album_share_id_seq"
OWNED BY "dinbog"."album_share"."id";
SELECT setval('"dinbog"."album_share_id_seq"', 232, true);
ALTER SEQUENCE "dinbog"."album_type_id_seq"
OWNED BY "dinbog"."album_type"."id";
SELECT setval('"dinbog"."album_type_id_seq"', 196, true);
ALTER SEQUENCE "dinbog"."attachment_comment_id_seq"
OWNED BY "dinbog"."attachment_comment"."id";
SELECT setval('"dinbog"."attachment_comment_id_seq"', 362, true);
ALTER SEQUENCE "dinbog"."attachment_comment_like_id_seq"
OWNED BY "dinbog"."attachment_comment_like"."id";
SELECT setval('"dinbog"."attachment_comment_like_id_seq"', 6, false);
ALTER SEQUENCE "dinbog"."attachment_comment_mention_id_seq"
OWNED BY "dinbog"."attachment_comment_mention"."id";
SELECT setval('"dinbog"."attachment_comment_mention_id_seq"', 6, false);
ALTER SEQUENCE "dinbog"."attachment_id_seq"
OWNED BY "dinbog"."attachment"."id";
SELECT setval('"dinbog"."attachment_id_seq"', 510, true);
ALTER SEQUENCE "dinbog"."attachment_like_id_seq"
OWNED BY "dinbog"."attachment_like"."id";
SELECT setval('"dinbog"."attachment_like_id_seq"', 348, true);
ALTER SEQUENCE "dinbog"."attachment_notification_id_seq"
OWNED BY "dinbog"."attachment_notification"."id";
SELECT setval('"dinbog"."attachment_notification_id_seq"', 95, true);
ALTER SEQUENCE "dinbog"."attachment_report_id_seq"
OWNED BY "dinbog"."attachment_report"."id";
SELECT setval('"dinbog"."attachment_report_id_seq"', 269, true);
ALTER SEQUENCE "dinbog"."attachment_share_id_seq"
OWNED BY "dinbog"."attachment_share"."id";
SELECT setval('"dinbog"."attachment_share_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."attachment_tag_id_seq"
OWNED BY "dinbog"."attachment_tag"."id";
SELECT setval('"dinbog"."attachment_tag_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."attachment_type_id_seq"
OWNED BY "dinbog"."attachment_type"."id";
SELECT setval('"dinbog"."attachment_type_id_seq"', 150, true);
ALTER SEQUENCE "dinbog"."auditory_action_id_seq"
OWNED BY "dinbog"."auditory_action"."id";
SELECT setval('"dinbog"."auditory_action_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."auditory_action_type_id_seq"
OWNED BY "dinbog"."auditory_action_type"."id";
SELECT setval('"dinbog"."auditory_action_type_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."city_id_seq"
OWNED BY "dinbog"."city"."id";
SELECT setval('"dinbog"."city_id_seq"', 137, true);
ALTER SEQUENCE "dinbog"."connection_type_id_seq"
OWNED BY "dinbog"."connection_type"."id";
SELECT setval('"dinbog"."connection_type_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."conversation_id_seq"
OWNED BY "dinbog"."conversation"."id";
SELECT setval('"dinbog"."conversation_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."conversation_message_id_seq"
OWNED BY "dinbog"."conversation_message"."id";
SELECT setval('"dinbog"."conversation_message_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."conversation_notification_id_seq"
OWNED BY "dinbog"."conversation_notification"."id";
SELECT setval('"dinbog"."conversation_notification_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."conversation_participant_id_seq"
OWNED BY "dinbog"."conversation_participant"."id";
SELECT setval('"dinbog"."conversation_participant_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."conversation_participant_status_id_seq"
OWNED BY "dinbog"."conversation_participant_status"."id";
SELECT setval('"dinbog"."conversation_participant_status_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."conversation_status_id_seq"
OWNED BY "dinbog"."conversation_status"."id";
SELECT setval('"dinbog"."conversation_status_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."country_id_seq"
OWNED BY "dinbog"."country"."id";
SELECT setval('"dinbog"."country_id_seq"', 369, true);
ALTER SEQUENCE "dinbog"."event_attachment_id_seq"
OWNED BY "dinbog"."event_attachment"."id";
SELECT setval('"dinbog"."event_attachment_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_filter_id_seq"
OWNED BY "dinbog"."event_filter"."id";
SELECT setval('"dinbog"."event_filter_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_id_seq"
OWNED BY "dinbog"."event"."id";
SELECT setval('"dinbog"."event_id_seq"', 254, true);
ALTER SEQUENCE "dinbog"."event_invitation_id_seq"
OWNED BY "dinbog"."event_invitation"."id";
SELECT setval('"dinbog"."event_invitation_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_notification_id_seq"
OWNED BY "dinbog"."event_notification"."id";
SELECT setval('"dinbog"."event_notification_id_seq"', 68, true);
ALTER SEQUENCE "dinbog"."event_participant_id_seq"
OWNED BY "dinbog"."event_participant"."id";
SELECT setval('"dinbog"."event_participant_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_participant_status_id_seq"
OWNED BY "dinbog"."event_participant_status"."id";
SELECT setval('"dinbog"."event_participant_status_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_profile_category_type_id_seq"
OWNED BY "dinbog"."event_profile_category_type"."id";
SELECT setval('"dinbog"."event_profile_category_type_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_report_id_seq"
OWNED BY "dinbog"."event_report"."id";
SELECT setval('"dinbog"."event_report_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_role_id_seq"
OWNED BY "dinbog"."event_role"."id";
SELECT setval('"dinbog"."event_role_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_share_id_seq"
OWNED BY "dinbog"."event_share"."id";
SELECT setval('"dinbog"."event_share_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."event_status_id_seq"
OWNED BY "dinbog"."event_status"."id";
SELECT setval('"dinbog"."event_status_id_seq"', 21, true);
ALTER SEQUENCE "dinbog"."event_type_id_seq"
OWNED BY "dinbog"."event_type"."id";
SELECT setval('"dinbog"."event_type_id_seq"', 21, true);
ALTER SEQUENCE "dinbog"."event_view_id_seq"
OWNED BY "dinbog"."event_view"."id";
SELECT setval('"dinbog"."event_view_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."field_category_id_seq"
OWNED BY "dinbog"."field_category"."id";
SELECT setval('"dinbog"."field_category_id_seq"', 22, true);
ALTER SEQUENCE "dinbog"."field_id_seq"
OWNED BY "dinbog"."field"."id";
SELECT setval('"dinbog"."field_id_seq"', 31, true);
ALTER SEQUENCE "dinbog"."gender_id_seq"
OWNED BY "dinbog"."gender"."id";
SELECT setval('"dinbog"."gender_id_seq"', 147, true);
ALTER SEQUENCE "dinbog"."group_id_seq"
OWNED BY "dinbog"."group"."id";
SELECT setval('"dinbog"."group_id_seq"', 88, true);
ALTER SEQUENCE "dinbog"."group_menu_id_seq"
OWNED BY "dinbog"."group_menu"."id";
SELECT setval('"dinbog"."group_menu_id_seq"', 157, true);
ALTER SEQUENCE "dinbog"."language_id_seq"
OWNED BY "dinbog"."language"."id";
SELECT setval('"dinbog"."language_id_seq"', 244, true);
ALTER SEQUENCE "dinbog"."log_id_seq"
OWNED BY "dinbog"."log"."id";
SELECT setval('"dinbog"."log_id_seq"', 360, true);
ALTER SEQUENCE "dinbog"."membership_id_seq"
OWNED BY "dinbog"."membership"."id";
SELECT setval('"dinbog"."membership_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."menu_id_seq"
OWNED BY "dinbog"."menu"."id";
SELECT setval('"dinbog"."menu_id_seq"', 137, true);
ALTER SEQUENCE "dinbog"."notification_action_id_seq"
OWNED BY "dinbog"."notification_action"."id";
SELECT setval('"dinbog"."notification_action_id_seq"', 27, true);
ALTER SEQUENCE "dinbog"."notification_id_seq"
OWNED BY "dinbog"."notification"."id";
SELECT setval('"dinbog"."notification_id_seq"', 680, true);
ALTER SEQUENCE "dinbog"."notification_message_id_seq"
OWNED BY "dinbog"."notification_message"."id";
SELECT setval('"dinbog"."notification_message_id_seq"', 19, true);
ALTER SEQUENCE "dinbog"."notification_messages_type_id_seq"
OWNED BY "dinbog"."notification_messages_type"."id";
SELECT setval('"dinbog"."notification_messages_type_id_seq"', 19, true);
ALTER SEQUENCE "dinbog"."notification_type_id_seq"
OWNED BY "dinbog"."notification_type"."id";
SELECT setval('"dinbog"."notification_type_id_seq"', 35, true);
ALTER SEQUENCE "dinbog"."option_id_seq"
OWNED BY "dinbog"."option"."id";
SELECT setval('"dinbog"."option_id_seq"', 647, true);
ALTER SEQUENCE "dinbog"."parameter_id_seq"
OWNED BY "dinbog"."parameter"."id";
SELECT setval('"dinbog"."parameter_id_seq"', 16, true);
ALTER SEQUENCE "dinbog"."poll_id_seq"
OWNED BY "dinbog"."poll"."id";
SELECT setval('"dinbog"."poll_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."poll_notification_id_seq"
OWNED BY "dinbog"."poll_notification"."id";
SELECT setval('"dinbog"."poll_notification_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."poll_participant_id_seq"
OWNED BY "dinbog"."poll_participant"."id";
SELECT setval('"dinbog"."poll_participant_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."poll_status_id_seq"
OWNED BY "dinbog"."poll_status"."id";
SELECT setval('"dinbog"."poll_status_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."poll_vote_id_seq"
OWNED BY "dinbog"."poll_vote"."id";
SELECT setval('"dinbog"."poll_vote_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."poll_vote_type_id_seq"
OWNED BY "dinbog"."poll_vote_type"."id";
SELECT setval('"dinbog"."poll_vote_type_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."post_attachment_id_seq"
OWNED BY "dinbog"."post_attachment"."id";
SELECT setval('"dinbog"."post_attachment_id_seq"', 309, true);
ALTER SEQUENCE "dinbog"."post_comment_id_seq"
OWNED BY "dinbog"."post_comment"."id";
SELECT setval('"dinbog"."post_comment_id_seq"', 26, true);
ALTER SEQUENCE "dinbog"."post_comment_like_id_seq"
OWNED BY "dinbog"."post_comment_like"."id";
SELECT setval('"dinbog"."post_comment_like_id_seq"', 6, false);
ALTER SEQUENCE "dinbog"."post_comment_mention_id_seq"
OWNED BY "dinbog"."post_comment_mention"."id";
SELECT setval('"dinbog"."post_comment_mention_id_seq"', 6, false);
ALTER SEQUENCE "dinbog"."post_hashtag_id_seq"
OWNED BY "dinbog"."post_hashtag"."id";
SELECT setval('"dinbog"."post_hashtag_id_seq"', 20, true);
ALTER SEQUENCE "dinbog"."post_id_seq"
OWNED BY "dinbog"."post"."id";
SELECT setval('"dinbog"."post_id_seq"', 323, true);
ALTER SEQUENCE "dinbog"."post_like_id_seq"
OWNED BY "dinbog"."post_like"."id";
SELECT setval('"dinbog"."post_like_id_seq"', 353, true);
ALTER SEQUENCE "dinbog"."post_mention_id_seq"
OWNED BY "dinbog"."post_mention"."id";
SELECT setval('"dinbog"."post_mention_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."post_notification_id_seq"
OWNED BY "dinbog"."post_notification"."id";
SELECT setval('"dinbog"."post_notification_id_seq"', 159, true);
ALTER SEQUENCE "dinbog"."post_report_id_seq"
OWNED BY "dinbog"."post_report"."id";
SELECT setval('"dinbog"."post_report_id_seq"', 226, true);
ALTER SEQUENCE "dinbog"."post_share_id_seq"
OWNED BY "dinbog"."post_share"."id";
SELECT setval('"dinbog"."post_share_id_seq"', 233, true);
ALTER SEQUENCE "dinbog"."profile_attachment_id_seq"
OWNED BY "dinbog"."profile_attachment"."id";
SELECT setval('"dinbog"."profile_attachment_id_seq"', 274, true);
ALTER SEQUENCE "dinbog"."profile_block_id_seq"
OWNED BY "dinbog"."profile_block"."id";
SELECT setval('"dinbog"."profile_block_id_seq"', 182, true);
ALTER SEQUENCE "dinbog"."profile_category_id_seq"
OWNED BY "dinbog"."profile_category"."id";
SELECT setval('"dinbog"."profile_category_id_seq"', 375, true);
ALTER SEQUENCE "dinbog"."profile_category_type_id_seq"
OWNED BY "dinbog"."profile_category_type"."id";
SELECT setval('"dinbog"."profile_category_type_id_seq"', 758, true);
ALTER SEQUENCE "dinbog"."profile_connection_rol_id_seq"
OWNED BY "dinbog"."profile_connection_rol"."id";
SELECT setval('"dinbog"."profile_connection_rol_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."profile_connection_type_rol_id_seq"
OWNED BY "dinbog"."profile_connection_type_rol"."id";
SELECT setval('"dinbog"."profile_connection_type_rol_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."profile_country_id_seq"
OWNED BY "dinbog"."profile_country"."id";
SELECT setval('"dinbog"."profile_country_id_seq"', 23, true);
ALTER SEQUENCE "dinbog"."profile_country_type_id_seq"
OWNED BY "dinbog"."profile_country_type"."id";
SELECT setval('"dinbog"."profile_country_type_id_seq"', 7, true);
ALTER SEQUENCE "dinbog"."profile_detail_id_seq"
OWNED BY "dinbog"."profile_detail"."id";
SELECT setval('"dinbog"."profile_detail_id_seq"', 22, true);
ALTER SEQUENCE "dinbog"."profile_follow_id_seq"
OWNED BY "dinbog"."profile_follow"."id";
SELECT setval('"dinbog"."profile_follow_id_seq"', 291, true);
ALTER SEQUENCE "dinbog"."profile_id_seq"
OWNED BY "dinbog"."profile"."id";
SELECT setval('"dinbog"."profile_id_seq"', 1202, true);
ALTER SEQUENCE "dinbog"."profile_job_history_id_seq"
OWNED BY "dinbog"."profile_job_history"."id";
SELECT setval('"dinbog"."profile_job_history_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."profile_language_id_seq"
OWNED BY "dinbog"."profile_language"."id";
SELECT setval('"dinbog"."profile_language_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."profile_like_id_seq"
OWNED BY "dinbog"."profile_like"."id";
SELECT setval('"dinbog"."profile_like_id_seq"', 339, true);
ALTER SEQUENCE "dinbog"."profile_notification_id_seq"
OWNED BY "dinbog"."profile_notification"."id";
SELECT setval('"dinbog"."profile_notification_id_seq"', 443, true);
ALTER SEQUENCE "dinbog"."profile_recommendation_id_seq"
OWNED BY "dinbog"."profile_recommendation"."id";
SELECT setval('"dinbog"."profile_recommendation_id_seq"', 157, true);
ALTER SEQUENCE "dinbog"."profile_report_id_seq"
OWNED BY "dinbog"."profile_report"."id";
SELECT setval('"dinbog"."profile_report_id_seq"', 152, true);
ALTER SEQUENCE "dinbog"."profile_request_connection_id_seq"
OWNED BY "dinbog"."profile_request_connection"."id";
SELECT setval('"dinbog"."profile_request_connection_id_seq"', 136, true);
ALTER SEQUENCE "dinbog"."profile_request_status_id_seq"
OWNED BY "dinbog"."profile_request_status"."id";
SELECT setval('"dinbog"."profile_request_status_id_seq"', 22, true);
ALTER SEQUENCE "dinbog"."profile_share_id_seq"
OWNED BY "dinbog"."profile_share"."id";
SELECT setval('"dinbog"."profile_share_id_seq"', 163, true);
ALTER SEQUENCE "dinbog"."profile_skill_id_seq"
OWNED BY "dinbog"."profile_skill"."id";
SELECT setval('"dinbog"."profile_skill_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."profile_type_id_seq"
OWNED BY "dinbog"."profile_type"."id";
SELECT setval('"dinbog"."profile_type_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."profile_view_id_seq"
OWNED BY "dinbog"."profile_view"."id";
SELECT setval('"dinbog"."profile_view_id_seq"', 25, true);
ALTER SEQUENCE "dinbog"."report_type_id_seq"
OWNED BY "dinbog"."report_type"."id";
SELECT setval('"dinbog"."report_type_id_seq"', 189, true);
ALTER SEQUENCE "dinbog"."site_id_seq"
OWNED BY "dinbog"."site"."id";
SELECT setval('"dinbog"."site_id_seq"', 147, true);
ALTER SEQUENCE "dinbog"."site_social_app_id_seq"
OWNED BY "dinbog"."site_social_app"."id";
SELECT setval('"dinbog"."site_social_app_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."skill_id_seq"
OWNED BY "dinbog"."skill"."id";
SELECT setval('"dinbog"."skill_id_seq"', 18, true);
ALTER SEQUENCE "dinbog"."social_account_id_seq"
OWNED BY "dinbog"."social_account"."id";
SELECT setval('"dinbog"."social_account_id_seq"', 370, true);
ALTER SEQUENCE "dinbog"."social_app_id_seq"
OWNED BY "dinbog"."social_app"."id";
SELECT setval('"dinbog"."social_app_id_seq"', 153, true);
ALTER SEQUENCE "dinbog"."ticket_id_seq"
OWNED BY "dinbog"."ticket"."id";
SELECT setval('"dinbog"."ticket_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."ticket_status_id_seq"
OWNED BY "dinbog"."ticket_status"."id";
SELECT setval('"dinbog"."ticket_status_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."user_email_id_seq"
OWNED BY "dinbog"."user_email"."id";
SELECT setval('"dinbog"."user_email_id_seq"', 1506, true);
ALTER SEQUENCE "dinbog"."user_group_id_seq"
OWNED BY "dinbog"."user_group"."id";
SELECT setval('"dinbog"."user_group_id_seq"', 1620, true);
ALTER SEQUENCE "dinbog"."user_id_seq"
OWNED BY "dinbog"."user"."id";
SELECT setval('"dinbog"."user_id_seq"', 1320, true);
ALTER SEQUENCE "dinbog"."user_notification_id_seq"
OWNED BY "dinbog"."user_notification"."id";
SELECT setval('"dinbog"."user_notification_id_seq"', 18, false);
ALTER SEQUENCE "dinbog"."user_status_id_seq"
OWNED BY "dinbog"."user_status"."id";
SELECT setval('"dinbog"."user_status_id_seq"', 189, true);
ALTER SEQUENCE "dinbog"."vote_type_id_seq"
OWNED BY "dinbog"."vote_type"."id";
SELECT setval('"dinbog"."vote_type_id_seq"', 18, false);

-- ----------------------------
-- Indexes structure for table album
-- ----------------------------
CREATE INDEX "album_name_idx" ON "dinbog"."album" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table album
-- ----------------------------
ALTER TABLE "dinbog"."album" ADD CONSTRAINT "album_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table album_attachment
-- ----------------------------
ALTER TABLE "dinbog"."album_attachment" ADD CONSTRAINT "album_attachment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table album_comment
-- ----------------------------
CREATE TRIGGER "create_notification_album_comment" AFTER INSERT OR DELETE ON "dinbog"."album_comment"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_album_comment"();

-- ----------------------------
-- Primary Key structure for table album_comment
-- ----------------------------
ALTER TABLE "dinbog"."album_comment" ADD CONSTRAINT "album_comment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table album_comment_like
-- ----------------------------
CREATE TRIGGER "create_notification_album_comment_like" AFTER INSERT OR DELETE ON "dinbog"."album_comment_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_album_comment_like"();

-- ----------------------------
-- Primary Key structure for table album_comment_like
-- ----------------------------
ALTER TABLE "dinbog"."album_comment_like" ADD CONSTRAINT "album_comment_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table album_comment_mention
-- ----------------------------
CREATE TRIGGER "create_notification_album_comment_mention" AFTER INSERT ON "dinbog"."album_comment_mention"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_album_comment_mention"();

-- ----------------------------
-- Primary Key structure for table album_comment_mention
-- ----------------------------
ALTER TABLE "dinbog"."album_comment_mention" ADD CONSTRAINT "album_comment_mention_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table album_like
-- ----------------------------
CREATE TRIGGER "create_notification_album_like" AFTER INSERT OR DELETE ON "dinbog"."album_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_album_like"();

-- ----------------------------
-- Primary Key structure for table album_like
-- ----------------------------
ALTER TABLE "dinbog"."album_like" ADD CONSTRAINT "album_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table album_notification
-- ----------------------------
ALTER TABLE "dinbog"."album_notification" ADD CONSTRAINT "album_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table album_report
-- ----------------------------
ALTER TABLE "dinbog"."album_report" ADD CONSTRAINT "album_report_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table album_share
-- ----------------------------
ALTER TABLE "dinbog"."album_share" ADD CONSTRAINT "album_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table album_type
-- ----------------------------
CREATE UNIQUE INDEX "album_type_name_idx" ON "dinbog"."album_type" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table album_type
-- ----------------------------
ALTER TABLE "dinbog"."album_type" ADD CONSTRAINT "album_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table attachment
-- ----------------------------
ALTER TABLE "dinbog"."attachment" ADD CONSTRAINT "attachments_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table attachment_comment
-- ----------------------------
CREATE TRIGGER "create_notification_attachment_comment" AFTER INSERT OR DELETE ON "dinbog"."attachment_comment"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_attachment_comment"();

-- ----------------------------
-- Primary Key structure for table attachment_comment
-- ----------------------------
ALTER TABLE "dinbog"."attachment_comment" ADD CONSTRAINT "attachment_comment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table attachment_comment_like
-- ----------------------------
CREATE TRIGGER "create_notification_attachment_comment_like" AFTER INSERT OR DELETE ON "dinbog"."attachment_comment_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_attachment_comment_like"();

-- ----------------------------
-- Primary Key structure for table attachment_comment_like
-- ----------------------------
ALTER TABLE "dinbog"."attachment_comment_like" ADD CONSTRAINT "attachment_comment_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table attachment_comment_mention
-- ----------------------------
CREATE TRIGGER "create_notification_attachment_comment_mention" AFTER INSERT ON "dinbog"."attachment_comment_mention"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_attachment_comment_mention"();

-- ----------------------------
-- Primary Key structure for table attachment_comment_mention
-- ----------------------------
ALTER TABLE "dinbog"."attachment_comment_mention" ADD CONSTRAINT "attachment_comment_mention_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table attachment_like
-- ----------------------------
CREATE TRIGGER "create_notification_attachment_like" AFTER INSERT OR DELETE ON "dinbog"."attachment_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_attachment_like"();

-- ----------------------------
-- Primary Key structure for table attachment_like
-- ----------------------------
ALTER TABLE "dinbog"."attachment_like" ADD CONSTRAINT "attachment_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table attachment_notification
-- ----------------------------
ALTER TABLE "dinbog"."attachment_notification" ADD CONSTRAINT "attachment_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table attachment_report
-- ----------------------------
ALTER TABLE "dinbog"."attachment_report" ADD CONSTRAINT "attachment_report_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table attachment_share
-- ----------------------------
ALTER TABLE "dinbog"."attachment_share" ADD CONSTRAINT "attachment_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table attachment_tag
-- ----------------------------
ALTER TABLE "dinbog"."attachment_tag" ADD CONSTRAINT "attachment_tag_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table attachment_type
-- ----------------------------
ALTER TABLE "dinbog"."attachment_type" ADD CONSTRAINT "attachment_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table auditory_action
-- ----------------------------
ALTER TABLE "dinbog"."auditory_action" ADD CONSTRAINT "auditory_action_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table auditory_action_type
-- ----------------------------
ALTER TABLE "dinbog"."auditory_action_type" ADD CONSTRAINT "auditory_action_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table auditory_errors
-- ----------------------------
ALTER TABLE "dinbog"."auditory_errors" ADD CONSTRAINT "auditory_errors_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table city
-- ----------------------------
ALTER TABLE "dinbog"."city" ADD CONSTRAINT "city_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table connection_type
-- ----------------------------
ALTER TABLE "dinbog"."connection_type" ADD CONSTRAINT "connection_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table conversation
-- ----------------------------
ALTER TABLE "dinbog"."conversation" ADD CONSTRAINT "conversation_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table conversation_message
-- ----------------------------
ALTER TABLE "dinbog"."conversation_message" ADD CONSTRAINT "conversation_message_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table conversation_notification
-- ----------------------------
ALTER TABLE "dinbog"."conversation_notification" ADD CONSTRAINT "conversation_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table conversation_participant
-- ----------------------------
ALTER TABLE "dinbog"."conversation_participant" ADD CONSTRAINT "conversation_participant_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table conversation_participant_status
-- ----------------------------
ALTER TABLE "dinbog"."conversation_participant_status" ADD CONSTRAINT "conversation_participant_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table conversation_status
-- ----------------------------
ALTER TABLE "dinbog"."conversation_status" ADD CONSTRAINT "conversation_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table country
-- ----------------------------
ALTER TABLE "dinbog"."country" ADD CONSTRAINT "country_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table event
-- ----------------------------
CREATE INDEX "index_address" ON "dinbog"."event" USING btree (
  "address" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_name_event" ON "dinbog"."event" USING btree (
  "name_event" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Triggers structure for table event
-- ----------------------------
CREATE TRIGGER "create_notification_event_status" AFTER INSERT OR UPDATE ON "dinbog"."event"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_event_status"();

-- ----------------------------
-- Primary Key structure for table event
-- ----------------------------
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "event_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_attachment
-- ----------------------------
ALTER TABLE "dinbog"."event_attachment" ADD CONSTRAINT "event_attachment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_filter
-- ----------------------------
ALTER TABLE "dinbog"."event_filter" ADD CONSTRAINT "event_filter_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table event_invitation
-- ----------------------------
CREATE TRIGGER "create_notification_event_invitation" AFTER INSERT ON "dinbog"."event_invitation"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_event_invitation"();

-- ----------------------------
-- Primary Key structure for table event_invitation
-- ----------------------------
ALTER TABLE "dinbog"."event_invitation" ADD CONSTRAINT "event_invitation_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_notification
-- ----------------------------
ALTER TABLE "dinbog"."event_notification" ADD CONSTRAINT "event_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table event_participant
-- ----------------------------
CREATE TRIGGER "create_notification_event_participant" AFTER INSERT OR UPDATE OR DELETE ON "dinbog"."event_participant"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_event_participant"();

-- ----------------------------
-- Primary Key structure for table event_participant
-- ----------------------------
ALTER TABLE "dinbog"."event_participant" ADD CONSTRAINT "event_participant_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_participant_status
-- ----------------------------
ALTER TABLE "dinbog"."event_participant_status" ADD CONSTRAINT "event_participant_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_profile_category_type
-- ----------------------------
ALTER TABLE "dinbog"."event_profile_category_type" ADD CONSTRAINT "event_profile_category_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_report
-- ----------------------------
ALTER TABLE "dinbog"."event_report" ADD CONSTRAINT "event_report_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_role
-- ----------------------------
ALTER TABLE "dinbog"."event_role" ADD CONSTRAINT "event_role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_share
-- ----------------------------
ALTER TABLE "dinbog"."event_share" ADD CONSTRAINT "event_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_status
-- ----------------------------
ALTER TABLE "dinbog"."event_status" ADD CONSTRAINT "event_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_type
-- ----------------------------
ALTER TABLE "dinbog"."event_type" ADD CONSTRAINT "event_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table event_view
-- ----------------------------
ALTER TABLE "dinbog"."event_view" ADD CONSTRAINT "event_view_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table field
-- ----------------------------
ALTER TABLE "dinbog"."field" ADD CONSTRAINT "field_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table field_category
-- ----------------------------
ALTER TABLE "dinbog"."field_category" ADD CONSTRAINT "field_category_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table gender
-- ----------------------------
ALTER TABLE "dinbog"."gender" ADD CONSTRAINT "gender_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table group
-- ----------------------------
ALTER TABLE "dinbog"."group" ADD CONSTRAINT "user_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table group_menu
-- ----------------------------
ALTER TABLE "dinbog"."group_menu" ADD CONSTRAINT "group_menu_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table language
-- ----------------------------
ALTER TABLE "dinbog"."language" ADD CONSTRAINT "language_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table log
-- ----------------------------
ALTER TABLE "dinbog"."log" ADD CONSTRAINT "log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table membership
-- ----------------------------
ALTER TABLE "dinbog"."membership" ADD CONSTRAINT "membership_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table menu
-- ----------------------------
ALTER TABLE "dinbog"."menu" ADD CONSTRAINT "menu_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table notification
-- ----------------------------
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table notification_action
-- ----------------------------
ALTER TABLE "dinbog"."notification_action" ADD CONSTRAINT "notification_action_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table notification_message
-- ----------------------------
CREATE UNIQUE INDEX "notification_message_code_message_idx" ON "dinbog"."notification_message" USING btree (
  "code_message" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table notification_message
-- ----------------------------
ALTER TABLE "dinbog"."notification_message" ADD CONSTRAINT "notification_message_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table notification_messages_type
-- ----------------------------
ALTER TABLE "dinbog"."notification_messages_type" ADD CONSTRAINT "notification_messages_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table notification_type
-- ----------------------------
ALTER TABLE "dinbog"."notification_type" ADD CONSTRAINT "notification_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table option
-- ----------------------------
ALTER TABLE "dinbog"."option" ADD CONSTRAINT "option_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table parameter
-- ----------------------------
CREATE UNIQUE INDEX "parameter_key_idx" ON "dinbog"."parameter" USING btree (
  "key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table parameter
-- ----------------------------
ALTER TABLE "dinbog"."parameter" ADD CONSTRAINT "parameter_key_key" UNIQUE ("key");

-- ----------------------------
-- Primary Key structure for table parameter
-- ----------------------------
ALTER TABLE "dinbog"."parameter" ADD CONSTRAINT "parameter_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table poll
-- ----------------------------
ALTER TABLE "dinbog"."poll" ADD CONSTRAINT "poll_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table poll_notification
-- ----------------------------
ALTER TABLE "dinbog"."poll_notification" ADD CONSTRAINT "poll_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table poll_participant
-- ----------------------------
ALTER TABLE "dinbog"."poll_participant" ADD CONSTRAINT "poll_participant_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table poll_status
-- ----------------------------
ALTER TABLE "dinbog"."poll_status" ADD CONSTRAINT "poll_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table poll_vote
-- ----------------------------
CREATE TRIGGER "create_notification_poll_vote" AFTER INSERT OR DELETE ON "dinbog"."poll_vote"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_poll_vote"();

-- ----------------------------
-- Primary Key structure for table poll_vote
-- ----------------------------
ALTER TABLE "dinbog"."poll_vote" ADD CONSTRAINT "poll_vote_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table poll_vote_type
-- ----------------------------
ALTER TABLE "dinbog"."poll_vote_type" ADD CONSTRAINT "poll_vote_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table post
-- ----------------------------
CREATE TRIGGER "post_action" AFTER INSERT OR DELETE ON "dinbog"."post"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_post"();

-- ----------------------------
-- Primary Key structure for table post
-- ----------------------------
ALTER TABLE "dinbog"."post" ADD CONSTRAINT "post_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_attachment
-- ----------------------------
ALTER TABLE "dinbog"."post_attachment" ADD CONSTRAINT "post_attachment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table post_comment
-- ----------------------------
CREATE TRIGGER "create_notification_post_comment" AFTER INSERT OR DELETE ON "dinbog"."post_comment"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_post_comment"();

-- ----------------------------
-- Primary Key structure for table post_comment
-- ----------------------------
ALTER TABLE "dinbog"."post_comment" ADD CONSTRAINT "post_comment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table post_comment_like
-- ----------------------------
CREATE TRIGGER "create_notification_post_comment_like" AFTER INSERT OR DELETE ON "dinbog"."post_comment_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_post_comment_like"();

-- ----------------------------
-- Primary Key structure for table post_comment_like
-- ----------------------------
ALTER TABLE "dinbog"."post_comment_like" ADD CONSTRAINT "post_comment_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table post_comment_mention
-- ----------------------------
CREATE TRIGGER "create_notification_post_comment_mention" AFTER INSERT ON "dinbog"."post_comment_mention"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_post_comment_mention"();

-- ----------------------------
-- Primary Key structure for table post_comment_mention
-- ----------------------------
ALTER TABLE "dinbog"."post_comment_mention" ADD CONSTRAINT "post_comment_mention_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table post_hashtag
-- ----------------------------
CREATE INDEX "post_hashtag_name_hastag_idx" ON "dinbog"."post_hashtag" USING btree (
  "name_hastag" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table post_hashtag
-- ----------------------------
ALTER TABLE "dinbog"."post_hashtag" ADD CONSTRAINT "post_hashtag_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table post_like
-- ----------------------------
CREATE TRIGGER "create_notification_post_like" AFTER INSERT OR DELETE ON "dinbog"."post_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_post_like"();

-- ----------------------------
-- Primary Key structure for table post_like
-- ----------------------------
ALTER TABLE "dinbog"."post_like" ADD CONSTRAINT "post_like_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table post_mention
-- ----------------------------
CREATE TRIGGER "create_notification_post_mention" AFTER INSERT ON "dinbog"."post_mention"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_post_mention"();

-- ----------------------------
-- Primary Key structure for table post_mention
-- ----------------------------
ALTER TABLE "dinbog"."post_mention" ADD CONSTRAINT "post_mention_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_notification
-- ----------------------------
ALTER TABLE "dinbog"."post_notification" ADD CONSTRAINT "post_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_report
-- ----------------------------
ALTER TABLE "dinbog"."post_report" ADD CONSTRAINT "post_report_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table post_share
-- ----------------------------
ALTER TABLE "dinbog"."post_share" ADD CONSTRAINT "post_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table profile
-- ----------------------------
CREATE INDEX "index_first_name" ON "dinbog"."profile" USING btree (
  "first_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_last_name" ON "dinbog"."profile" USING btree (
  "last_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "index_url_name" ON "dinbog"."profile" USING btree (
  "url_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table profile
-- ----------------------------
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "profile_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_attachment
-- ----------------------------
ALTER TABLE "dinbog"."profile_attachment" ADD CONSTRAINT "profile_attachment_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_block
-- ----------------------------
ALTER TABLE "dinbog"."profile_block" ADD CONSTRAINT "profile_block_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_category
-- ----------------------------
ALTER TABLE "dinbog"."profile_category" ADD CONSTRAINT "profile_category_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_category_type
-- ----------------------------
ALTER TABLE "dinbog"."profile_category_type" ADD CONSTRAINT "profile_category_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_connection_rol
-- ----------------------------
ALTER TABLE "dinbog"."profile_connection_rol" ADD CONSTRAINT "profile_connection_rol_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_connection_type_rol
-- ----------------------------
ALTER TABLE "dinbog"."profile_connection_type_rol" ADD CONSTRAINT "profile_connection_type_rol_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table profile_country
-- ----------------------------
CREATE INDEX "profile_country_profile_id_idx" ON "dinbog"."profile_country" USING btree (
  "profile_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table profile_country_type
-- ----------------------------
ALTER TABLE "dinbog"."profile_country_type" ADD CONSTRAINT "profile_country_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_detail
-- ----------------------------
ALTER TABLE "dinbog"."profile_detail" ADD CONSTRAINT "profile_detail_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table profile_follow
-- ----------------------------
CREATE TRIGGER "profile_follow_action" AFTER INSERT OR DELETE ON "dinbog"."profile_follow"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_profile_follow"();

-- ----------------------------
-- Primary Key structure for table profile_follow
-- ----------------------------
ALTER TABLE "dinbog"."profile_follow" ADD CONSTRAINT "profile_follow_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_job_history
-- ----------------------------
ALTER TABLE "dinbog"."profile_job_history" ADD CONSTRAINT "profile_job_history_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_language
-- ----------------------------
ALTER TABLE "dinbog"."profile_language" ADD CONSTRAINT "profile_language_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table profile_like
-- ----------------------------
CREATE TRIGGER "profile_like_action" AFTER INSERT OR DELETE ON "dinbog"."profile_like"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_profile_like"();

-- ----------------------------
-- Primary Key structure for table profile_like
-- ----------------------------
ALTER TABLE "dinbog"."profile_like" ADD CONSTRAINT "profile_actions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_notification
-- ----------------------------
ALTER TABLE "dinbog"."profile_notification" ADD CONSTRAINT "profile_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table profile_recommendation
-- ----------------------------
CREATE TRIGGER "create_notification_profile_recommendation" AFTER INSERT ON "dinbog"."profile_recommendation"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_profile_recommendation"();

-- ----------------------------
-- Primary Key structure for table profile_recommendation
-- ----------------------------
ALTER TABLE "dinbog"."profile_recommendation" ADD CONSTRAINT "profile_recommendation_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_report
-- ----------------------------
ALTER TABLE "dinbog"."profile_report" ADD CONSTRAINT "profile_report_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Triggers structure for table profile_request_connection
-- ----------------------------
CREATE TRIGGER "profile_connection_action" AFTER INSERT OR UPDATE OR DELETE ON "dinbog"."profile_request_connection"
FOR EACH ROW
EXECUTE PROCEDURE "dinbog"."create_notification_profile_connection"();

-- ----------------------------
-- Primary Key structure for table profile_request_connection
-- ----------------------------
ALTER TABLE "dinbog"."profile_request_connection" ADD CONSTRAINT "profile_request_connection_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_request_status
-- ----------------------------
ALTER TABLE "dinbog"."profile_request_status" ADD CONSTRAINT "profile_request_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_share
-- ----------------------------
ALTER TABLE "dinbog"."profile_share" ADD CONSTRAINT "profile_share_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_skill
-- ----------------------------
ALTER TABLE "dinbog"."profile_skill" ADD CONSTRAINT "profile_skill_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_type
-- ----------------------------
ALTER TABLE "dinbog"."profile_type" ADD CONSTRAINT "profile_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table profile_view
-- ----------------------------
ALTER TABLE "dinbog"."profile_view" ADD CONSTRAINT "profile_view_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table report_type
-- ----------------------------
ALTER TABLE "dinbog"."report_type" ADD CONSTRAINT "report_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table site
-- ----------------------------
ALTER TABLE "dinbog"."site" ADD CONSTRAINT "site_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table site_social_app
-- ----------------------------
ALTER TABLE "dinbog"."site_social_app" ADD CONSTRAINT "site_social_app_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table skill
-- ----------------------------
ALTER TABLE "dinbog"."skill" ADD CONSTRAINT "skill_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table social_account
-- ----------------------------
ALTER TABLE "dinbog"."social_account" ADD CONSTRAINT "social_account_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table social_app
-- ----------------------------
CREATE INDEX "social_app_domain_idx" ON "dinbog"."social_app" USING btree (
  "domain" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "social_app_name_idx" ON "dinbog"."social_app" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table social_app
-- ----------------------------
ALTER TABLE "dinbog"."social_app" ADD CONSTRAINT "social_app_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ticket
-- ----------------------------
CREATE INDEX "ops_ticket_owner_id_3b9de74a613fa930_fk_social_profile_id" ON "dinbog"."ticket" USING btree (
  "owner_profile_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table ticket
-- ----------------------------
ALTER TABLE "dinbog"."ticket" ADD CONSTRAINT "ticket_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ticket_status
-- ----------------------------
ALTER TABLE "dinbog"."ticket_status" ADD CONSTRAINT "ticket_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "dinbog"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table user_email
-- ----------------------------
CREATE UNIQUE INDEX "ind_email" ON "dinbog"."user_email" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user_email
-- ----------------------------
ALTER TABLE "dinbog"."user_email" ADD CONSTRAINT "user_email_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_group
-- ----------------------------
ALTER TABLE "dinbog"."user_group" ADD CONSTRAINT "user_group_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_notification
-- ----------------------------
ALTER TABLE "dinbog"."user_notification" ADD CONSTRAINT "user_notification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_status
-- ----------------------------
ALTER TABLE "dinbog"."user_status" ADD CONSTRAINT "user_status_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table vote_type
-- ----------------------------
ALTER TABLE "dinbog"."vote_type" ADD CONSTRAINT "vote_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table album
-- ----------------------------
ALTER TABLE "dinbog"."album" ADD CONSTRAINT "album_album_type_id_fkey" FOREIGN KEY ("album_type_id") REFERENCES "dinbog"."album_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album" ADD CONSTRAINT "fk_album_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table album_attachment
-- ----------------------------
ALTER TABLE "dinbog"."album_attachment" ADD CONSTRAINT "fk_album_attachment_album_1" FOREIGN KEY ("album_id") REFERENCES "dinbog"."album" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_attachment" ADD CONSTRAINT "fk_album_attachment_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table album_comment
-- ----------------------------
ALTER TABLE "dinbog"."album_comment" ADD CONSTRAINT "fk_album_comment_album_1" FOREIGN KEY ("album_id") REFERENCES "dinbog"."album" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_comment" ADD CONSTRAINT "fk_album_comment_album_comment_1" FOREIGN KEY ("parent_id") REFERENCES "dinbog"."album_comment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_comment" ADD CONSTRAINT "fk_album_comment_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table album_comment_like
-- ----------------------------
ALTER TABLE "dinbog"."album_comment_like" ADD CONSTRAINT "album_comment_like_album_comment_id_fkey" FOREIGN KEY ("album_comment_id") REFERENCES "dinbog"."album_comment" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dinbog"."album_comment_like" ADD CONSTRAINT "album_comment_like_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table album_comment_mention
-- ----------------------------
ALTER TABLE "dinbog"."album_comment_mention" ADD CONSTRAINT "album_comment_mention_album_comment_id_fkey" FOREIGN KEY ("album_comment_id") REFERENCES "dinbog"."album_comment" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dinbog"."album_comment_mention" ADD CONSTRAINT "album_comment_mention_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table album_like
-- ----------------------------
ALTER TABLE "dinbog"."album_like" ADD CONSTRAINT "fk_album_like_album_1" FOREIGN KEY ("album_id") REFERENCES "dinbog"."album" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_like" ADD CONSTRAINT "fk_album_like_profile" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table album_notification
-- ----------------------------
ALTER TABLE "dinbog"."album_notification" ADD CONSTRAINT "fk_album_notification_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_notification" ADD CONSTRAINT "fk_album_notification_profile_2" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table album_report
-- ----------------------------
ALTER TABLE "dinbog"."album_report" ADD CONSTRAINT "fk_album_report_album_1" FOREIGN KEY ("album_id") REFERENCES "dinbog"."album" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_report" ADD CONSTRAINT "fk_album_report_profile_1" FOREIGN KEY ("owner_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_report" ADD CONSTRAINT "fk_album_report_report_type_1" FOREIGN KEY ("report_type_id") REFERENCES "dinbog"."report_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table album_share
-- ----------------------------
ALTER TABLE "dinbog"."album_share" ADD CONSTRAINT "fk_album_share_album_1" FOREIGN KEY ("album_id") REFERENCES "dinbog"."album" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."album_share" ADD CONSTRAINT "fk_album_share_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment
-- ----------------------------
ALTER TABLE "dinbog"."attachment" ADD CONSTRAINT "fk_attachments_attachment_type_1" FOREIGN KEY ("attachment_type_id") REFERENCES "dinbog"."attachment_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment_comment
-- ----------------------------
ALTER TABLE "dinbog"."attachment_comment" ADD CONSTRAINT "fk_attachmen_comment_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_comment" ADD CONSTRAINT "fk_attachmen_comment_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_comment" ADD CONSTRAINT "fk_attachment_comment_attachment_comment_1" FOREIGN KEY ("parent_id") REFERENCES "dinbog"."attachment_comment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment_comment_like
-- ----------------------------
ALTER TABLE "dinbog"."attachment_comment_like" ADD CONSTRAINT "attachment_comment_like_attachment_comment_id_fkey" FOREIGN KEY ("attachment_comment_id") REFERENCES "dinbog"."attachment_comment" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dinbog"."attachment_comment_like" ADD CONSTRAINT "attachment_comment_like_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table attachment_comment_mention
-- ----------------------------
ALTER TABLE "dinbog"."attachment_comment_mention" ADD CONSTRAINT "attachment_comment_mention_attachment_comment_id_fkey" FOREIGN KEY ("attachment_comment_id") REFERENCES "dinbog"."attachment_comment" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dinbog"."attachment_comment_mention" ADD CONSTRAINT "attachment_comment_mention_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table attachment_like
-- ----------------------------
ALTER TABLE "dinbog"."attachment_like" ADD CONSTRAINT "fk_attachment_like_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_like" ADD CONSTRAINT "fk_attachment_like_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment_notification
-- ----------------------------
ALTER TABLE "dinbog"."attachment_notification" ADD CONSTRAINT "fk_attachment_notification_profile_1" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_notification" ADD CONSTRAINT "fk_attachment_notification_profile_2" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment_report
-- ----------------------------
ALTER TABLE "dinbog"."attachment_report" ADD CONSTRAINT "fk_attachment_report_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_report" ADD CONSTRAINT "fk_attachment_report_profile_1" FOREIGN KEY ("owner_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_report" ADD CONSTRAINT "fk_attachment_report_report_type_1" FOREIGN KEY ("report_type_id") REFERENCES "dinbog"."report_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment_share
-- ----------------------------
ALTER TABLE "dinbog"."attachment_share" ADD CONSTRAINT "fk_attachment_share_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_share" ADD CONSTRAINT "fk_attachment_share_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table attachment_tag
-- ----------------------------
ALTER TABLE "dinbog"."attachment_tag" ADD CONSTRAINT "fk_attachment_tag_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."attachment_tag" ADD CONSTRAINT "fk_attachment_tag_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table auditory_action
-- ----------------------------
ALTER TABLE "dinbog"."auditory_action" ADD CONSTRAINT "fk_auditory_action_auditory_action_type_1" FOREIGN KEY ("auditory_action_type_id") REFERENCES "dinbog"."auditory_action_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."auditory_action" ADD CONSTRAINT "fk_auditory_action_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."auditory_action" ADD CONSTRAINT "fk_auditory_action_user_1" FOREIGN KEY ("user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table auditory_errors
-- ----------------------------
ALTER TABLE "dinbog"."auditory_errors" ADD CONSTRAINT "fk_auditory_errors_user_1" FOREIGN KEY ("user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table city
-- ----------------------------
ALTER TABLE "dinbog"."city" ADD CONSTRAINT "fk_city_country_1" FOREIGN KEY ("country_id") REFERENCES "dinbog"."country" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table conversation
-- ----------------------------
ALTER TABLE "dinbog"."conversation" ADD CONSTRAINT "fk_chat_chat_status_1" FOREIGN KEY ("conversation_status_id") REFERENCES "dinbog"."conversation_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table conversation_message
-- ----------------------------
ALTER TABLE "dinbog"."conversation_message" ADD CONSTRAINT "fk_chat_message_chat_participant_1" FOREIGN KEY ("conversation_participant_id") REFERENCES "dinbog"."conversation_participant" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table conversation_notification
-- ----------------------------
ALTER TABLE "dinbog"."conversation_notification" ADD CONSTRAINT "conversation_notification_owner_profile_id_fkey" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."conversation_notification" ADD CONSTRAINT "conversation_notification_to_profile_id_fkey" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table conversation_participant
-- ----------------------------
ALTER TABLE "dinbog"."conversation_participant" ADD CONSTRAINT "fk_chat_participant_chat_1" FOREIGN KEY ("conversation_id") REFERENCES "dinbog"."conversation" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."conversation_participant" ADD CONSTRAINT "fk_chat_participant_chat_participant_status_1" FOREIGN KEY ("conversation_participant_status_id") REFERENCES "dinbog"."conversation_participant_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."conversation_participant" ADD CONSTRAINT "fk_chat_participant_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table country
-- ----------------------------
ALTER TABLE "dinbog"."country" ADD CONSTRAINT "fk_country_language_1" FOREIGN KEY ("language_id") REFERENCES "dinbog"."language" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event
-- ----------------------------
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "fk_event_city_1" FOREIGN KEY ("city_id") REFERENCES "dinbog"."city" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "fk_event_country_1" FOREIGN KEY ("country_id") REFERENCES "dinbog"."country" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "fk_event_event_status_1" FOREIGN KEY ("event_status_id") REFERENCES "dinbog"."event_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "fk_event_event_type_1" FOREIGN KEY ("event_type_id") REFERENCES "dinbog"."event_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "fk_event_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event" ADD CONSTRAINT "fk_event_profile_type_1" FOREIGN KEY ("profile_type_id") REFERENCES "dinbog"."profile_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_attachment
-- ----------------------------
ALTER TABLE "dinbog"."event_attachment" ADD CONSTRAINT "fk_event_attachment_attachment_1" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_attachment" ADD CONSTRAINT "fk_event_attachment_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_filter
-- ----------------------------
ALTER TABLE "dinbog"."event_filter" ADD CONSTRAINT "fk_event_filter_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_filter" ADD CONSTRAINT "fk_event_filter_option_1" FOREIGN KEY ("option_id") REFERENCES "dinbog"."option" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_invitation
-- ----------------------------
ALTER TABLE "dinbog"."event_invitation" ADD CONSTRAINT "fk_event_invitation_even_invitation_rol_1" FOREIGN KEY ("event_role_id") REFERENCES "dinbog"."event_role" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_invitation" ADD CONSTRAINT "fk_event_invitation_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_invitation" ADD CONSTRAINT "fk_event_invitation_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_notification
-- ----------------------------
ALTER TABLE "dinbog"."event_notification" ADD CONSTRAINT "event_notification_owner_profile_id_fkey" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_notification" ADD CONSTRAINT "event_notification_to_profile_id_fkey" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_participant
-- ----------------------------
ALTER TABLE "dinbog"."event_participant" ADD CONSTRAINT "fk_event_participant_even_role_1" FOREIGN KEY ("event_role_id") REFERENCES "dinbog"."event_role" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_participant" ADD CONSTRAINT "fk_event_request_profile_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_participant" ADD CONSTRAINT "fk_event_request_profile_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_participant" ADD CONSTRAINT "fk_event_request_profile_status_request_profile_1" FOREIGN KEY ("event_participant_status_id") REFERENCES "dinbog"."event_participant_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_participant_status
-- ----------------------------
ALTER TABLE "dinbog"."event_participant_status" ADD CONSTRAINT "fk_event_participant_status_event_type_1" FOREIGN KEY ("event_type_id") REFERENCES "dinbog"."event_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_profile_category_type
-- ----------------------------
ALTER TABLE "dinbog"."event_profile_category_type" ADD CONSTRAINT "fk_event_profile_category_type_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_profile_category_type" ADD CONSTRAINT "fk_event_profile_category_type_profile_category_type_1" FOREIGN KEY ("profile_category_type_id") REFERENCES "dinbog"."profile_category_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_report
-- ----------------------------
ALTER TABLE "dinbog"."event_report" ADD CONSTRAINT "fk_event_report_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_report" ADD CONSTRAINT "fk_event_report_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_report" ADD CONSTRAINT "fk_event_report_report_type_1" FOREIGN KEY ("report_type_id") REFERENCES "dinbog"."report_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_role
-- ----------------------------
ALTER TABLE "dinbog"."event_role" ADD CONSTRAINT "fk_even_invitation_rol_event_type_1" FOREIGN KEY ("event_type_id") REFERENCES "dinbog"."event_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_share
-- ----------------------------
ALTER TABLE "dinbog"."event_share" ADD CONSTRAINT "fk_event_share_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_share" ADD CONSTRAINT "fk_event_share_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table event_view
-- ----------------------------
ALTER TABLE "dinbog"."event_view" ADD CONSTRAINT "fk_event_view_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."event_view" ADD CONSTRAINT "fk_event_view_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table field_category
-- ----------------------------
ALTER TABLE "dinbog"."field_category" ADD CONSTRAINT "fk_category_field_field_1" FOREIGN KEY ("field_id") REFERENCES "dinbog"."field" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."field_category" ADD CONSTRAINT "fk_category_field_profile_category_type_1" FOREIGN KEY ("category_id") REFERENCES "dinbog"."profile_category_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table group_menu
-- ----------------------------
ALTER TABLE "dinbog"."group_menu" ADD CONSTRAINT "fk_menu_group_group_1" FOREIGN KEY ("group_id") REFERENCES "dinbog"."group" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."group_menu" ADD CONSTRAINT "fk_menu_group_menu_1" FOREIGN KEY ("menu_id") REFERENCES "dinbog"."menu" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table log
-- ----------------------------
ALTER TABLE "dinbog"."log" ADD CONSTRAINT "fk_log_user_1" FOREIGN KEY ("user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table notification
-- ----------------------------
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_conversation_notification_1" FOREIGN KEY ("conversation_notification_id") REFERENCES "dinbog"."conversation_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_event_notification_1" FOREIGN KEY ("event_notification_id") REFERENCES "dinbog"."event_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_notification_action_1" FOREIGN KEY ("notification_action_id") REFERENCES "dinbog"."notification_action" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_notification_message_1" FOREIGN KEY ("code_message") REFERENCES "dinbog"."notification_message" ("code_message") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_profile_notification_1" FOREIGN KEY ("profile_notification_id") REFERENCES "dinbog"."profile_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_user_1" FOREIGN KEY ("owner_user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "fk_notification_user_2" FOREIGN KEY ("to_user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "notification_album_notification_id_fkey" FOREIGN KEY ("album_notification_id") REFERENCES "dinbog"."album_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "notification_attachment_notification_id_fkey" FOREIGN KEY ("attachment_notification_id") REFERENCES "dinbog"."attachment_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "notification_poll_notification_id_fkey" FOREIGN KEY ("poll_notification_id") REFERENCES "dinbog"."poll_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "notification_post_notification_id_fkey" FOREIGN KEY ("post_notification_id") REFERENCES "dinbog"."post_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."notification" ADD CONSTRAINT "notification_user_notification_id_fkey" FOREIGN KEY ("user_notification_id") REFERENCES "dinbog"."user_notification" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table notification_action
-- ----------------------------
ALTER TABLE "dinbog"."notification_action" ADD CONSTRAINT "fk_notification_action_notification_type_1" FOREIGN KEY ("notification_type_id") REFERENCES "dinbog"."notification_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table notification_message
-- ----------------------------
ALTER TABLE "dinbog"."notification_message" ADD CONSTRAINT "fk_notification_message_notification_messages_type_1" FOREIGN KEY ("notification_messages_type") REFERENCES "dinbog"."notification_messages_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table option
-- ----------------------------
ALTER TABLE "dinbog"."option" ADD CONSTRAINT "fk_option_field_1" FOREIGN KEY ("field_id") REFERENCES "dinbog"."field" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table poll
-- ----------------------------
ALTER TABLE "dinbog"."poll" ADD CONSTRAINT "fk_poll_event_1" FOREIGN KEY ("event_id") REFERENCES "dinbog"."event" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll" ADD CONSTRAINT "fk_poll_poll_status_1" FOREIGN KEY ("poll_status_id") REFERENCES "dinbog"."poll_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll" ADD CONSTRAINT "fk_poll_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table poll_notification
-- ----------------------------
ALTER TABLE "dinbog"."poll_notification" ADD CONSTRAINT "fk_poll_notification_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll_notification" ADD CONSTRAINT "fk_poll_notification_profile_2" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table poll_participant
-- ----------------------------
ALTER TABLE "dinbog"."poll_participant" ADD CONSTRAINT "fk_poll_participant_event_participant_1" FOREIGN KEY ("event_participant_id") REFERENCES "dinbog"."event_participant" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll_participant" ADD CONSTRAINT "fk_poll_participant_poll_1" FOREIGN KEY ("poll_id") REFERENCES "dinbog"."poll" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table poll_status
-- ----------------------------
ALTER TABLE "dinbog"."poll_status" ADD CONSTRAINT "fk_poll_status_event_type_1" FOREIGN KEY ("event_type_id") REFERENCES "dinbog"."event_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table poll_vote
-- ----------------------------
ALTER TABLE "dinbog"."poll_vote" ADD CONSTRAINT "fk_poll_vote_poll_participant_1" FOREIGN KEY ("poll_participant_id") REFERENCES "dinbog"."poll_participant" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll_vote" ADD CONSTRAINT "fk_poll_vote_profile_2" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll_vote" ADD CONSTRAINT "fk_poll_vote_vote_type_1" FOREIGN KEY ("vote_type_id") REFERENCES "dinbog"."vote_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table poll_vote_type
-- ----------------------------
ALTER TABLE "dinbog"."poll_vote_type" ADD CONSTRAINT "fk_poll_vote_type_poll_1" FOREIGN KEY ("poll_id") REFERENCES "dinbog"."poll" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."poll_vote_type" ADD CONSTRAINT "fk_poll_vote_type_vote_type_1" FOREIGN KEY ("vote_type_id") REFERENCES "dinbog"."vote_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post
-- ----------------------------
ALTER TABLE "dinbog"."post" ADD CONSTRAINT "fk_post_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_attachment
-- ----------------------------
ALTER TABLE "dinbog"."post_attachment" ADD CONSTRAINT "post_attachment_attachment_id_fkey" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_attachment" ADD CONSTRAINT "post_attachment_post_id_fkey" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_comment
-- ----------------------------
ALTER TABLE "dinbog"."post_comment" ADD CONSTRAINT "fk_post_comment_post_comment_1" FOREIGN KEY ("parent_id") REFERENCES "dinbog"."post_comment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_comment" ADD CONSTRAINT "fk_post_comment_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_comment" ADD CONSTRAINT "fk_post_comments_post_1" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_comment_like
-- ----------------------------
ALTER TABLE "dinbog"."post_comment_like" ADD CONSTRAINT "post_comment_like_post_comment_id_fkey" FOREIGN KEY ("post_comment_id") REFERENCES "dinbog"."post_comment" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dinbog"."post_comment_like" ADD CONSTRAINT "post_comment_like_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_comment_mention
-- ----------------------------
ALTER TABLE "dinbog"."post_comment_mention" ADD CONSTRAINT "post_comment_mention_post_comment_id_fkey" FOREIGN KEY ("post_comment_id") REFERENCES "dinbog"."post_comment" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "dinbog"."post_comment_mention" ADD CONSTRAINT "post_comment_mention_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_hashtag
-- ----------------------------
ALTER TABLE "dinbog"."post_hashtag" ADD CONSTRAINT "fk_post_hashtag_post_1" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_like
-- ----------------------------
ALTER TABLE "dinbog"."post_like" ADD CONSTRAINT "fk_post_like_post_1" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_like" ADD CONSTRAINT "fk_post_like_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_mention
-- ----------------------------
ALTER TABLE "dinbog"."post_mention" ADD CONSTRAINT "fk_post_mentions_post_1" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_mention" ADD CONSTRAINT "fk_post_mentions_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_notification
-- ----------------------------
ALTER TABLE "dinbog"."post_notification" ADD CONSTRAINT "post_notification_owner_profile_id_fkey" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_notification" ADD CONSTRAINT "post_notification_to_profile_id_fkey" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_report
-- ----------------------------
ALTER TABLE "dinbog"."post_report" ADD CONSTRAINT "fk_post_report_post_1" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_report" ADD CONSTRAINT "fk_post_report_profile_1" FOREIGN KEY ("owner_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_report" ADD CONSTRAINT "fk_post_report_report_type_1" FOREIGN KEY ("report_type_id") REFERENCES "dinbog"."report_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table post_share
-- ----------------------------
ALTER TABLE "dinbog"."post_share" ADD CONSTRAINT "fk_post_share_post_1" FOREIGN KEY ("post_id") REFERENCES "dinbog"."post" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."post_share" ADD CONSTRAINT "fk_post_share_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile
-- ----------------------------
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "fk_profile_gender_1" FOREIGN KEY ("gender_id") REFERENCES "dinbog"."gender" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "fk_profile_membership_1" FOREIGN KEY ("membership_id") REFERENCES "dinbog"."membership" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "fk_profile_profile_type_1" FOREIGN KEY ("profile_type_id") REFERENCES "dinbog"."profile_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "fk_profile_user_1" FOREIGN KEY ("user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "profile_avatar_id_fkey" FOREIGN KEY ("avatar_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "profile_cover_id_fkey" FOREIGN KEY ("cover_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "profile_current_city_id_fkey" FOREIGN KEY ("current_city_id") REFERENCES "dinbog"."city" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile" ADD CONSTRAINT "profile_current_country_id_fkey" FOREIGN KEY ("current_country_id") REFERENCES "dinbog"."country" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_attachment
-- ----------------------------
ALTER TABLE "dinbog"."profile_attachment" ADD CONSTRAINT "profile_attachment_attachment_id_fkey" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_attachment" ADD CONSTRAINT "profile_attachment_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_block
-- ----------------------------
ALTER TABLE "dinbog"."profile_block" ADD CONSTRAINT "fk_profile_block_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_block" ADD CONSTRAINT "fk_profile_block_profile_2" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_category
-- ----------------------------
ALTER TABLE "dinbog"."profile_category" ADD CONSTRAINT "fk_profile_category_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_category" ADD CONSTRAINT "fk_profile_category_profile_category_type_1" FOREIGN KEY ("profile_category_type_id") REFERENCES "dinbog"."profile_category_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_category_type
-- ----------------------------
ALTER TABLE "dinbog"."profile_category_type" ADD CONSTRAINT "fk_profile_category_type_profile_category_type_1" FOREIGN KEY ("parent_id") REFERENCES "dinbog"."profile_category_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_category_type" ADD CONSTRAINT "fk_profile_category_type_profile_type_1" FOREIGN KEY ("profile_type_id") REFERENCES "dinbog"."profile_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_category_type" ADD CONSTRAINT "profile_category_type_attachment_id_fkey" FOREIGN KEY ("attachment_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_connection_rol
-- ----------------------------
ALTER TABLE "dinbog"."profile_connection_rol" ADD CONSTRAINT "fk_profile_connection_rol_profile_connection_type_rol_1" FOREIGN KEY ("profile_connection_type_rol_id") REFERENCES "dinbog"."profile_connection_type_rol" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_connection_rol" ADD CONSTRAINT "fk_profile_connection_rol_profile_request_connection_1" FOREIGN KEY ("profile_request_connection_id") REFERENCES "dinbog"."profile_request_connection" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_country
-- ----------------------------
ALTER TABLE "dinbog"."profile_country" ADD CONSTRAINT "profile_country_city_id_fkey" FOREIGN KEY ("city_id") REFERENCES "dinbog"."city" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_country" ADD CONSTRAINT "profile_country_country_id_fkey" FOREIGN KEY ("country_id") REFERENCES "dinbog"."country" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_country" ADD CONSTRAINT "profile_country_profile_country_type_id_fkey" FOREIGN KEY ("profile_country_type_id") REFERENCES "dinbog"."profile_country_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_country" ADD CONSTRAINT "profile_country_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_detail
-- ----------------------------
ALTER TABLE "dinbog"."profile_detail" ADD CONSTRAINT "fk_profile_detail_category_field_1" FOREIGN KEY ("field_category_id") REFERENCES "dinbog"."field_category" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_detail" ADD CONSTRAINT "fk_profile_detail_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_follow
-- ----------------------------
ALTER TABLE "dinbog"."profile_follow" ADD CONSTRAINT "fk_profile_follow_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_follow" ADD CONSTRAINT "fk_profile_follow_profile_2" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_job_history
-- ----------------------------
ALTER TABLE "dinbog"."profile_job_history" ADD CONSTRAINT "fk_profile_job_history_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_job_history" ADD CONSTRAINT "fk_profile_job_history_profile_2" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_job_history" ADD CONSTRAINT "profile_job_history_category_id_fkey" FOREIGN KEY ("category_id") REFERENCES "dinbog"."profile_category_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_language
-- ----------------------------
ALTER TABLE "dinbog"."profile_language" ADD CONSTRAINT "fk_profile_language_language_1" FOREIGN KEY ("language_id") REFERENCES "dinbog"."language" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_language" ADD CONSTRAINT "fk_profile_language_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_like
-- ----------------------------
ALTER TABLE "dinbog"."profile_like" ADD CONSTRAINT "fk_profile_actions_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_like" ADD CONSTRAINT "fk_profile_like_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_notification
-- ----------------------------
ALTER TABLE "dinbog"."profile_notification" ADD CONSTRAINT "fk_profile_notification_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_notification" ADD CONSTRAINT "fk_profile_notification_profile_2" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_recommendation
-- ----------------------------
ALTER TABLE "dinbog"."profile_recommendation" ADD CONSTRAINT "fk_profile_recomendation_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_recommendation" ADD CONSTRAINT "fk_profile_recomendation_profile_2" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_report
-- ----------------------------
ALTER TABLE "dinbog"."profile_report" ADD CONSTRAINT "fk_profile_report_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_report" ADD CONSTRAINT "fk_profile_report_profile_2" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_report" ADD CONSTRAINT "fk_profile_report_report_type_1" FOREIGN KEY ("report_type_id") REFERENCES "dinbog"."report_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_request_connection
-- ----------------------------
ALTER TABLE "dinbog"."profile_request_connection" ADD CONSTRAINT "fk_profile_company_interaction_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_request_connection" ADD CONSTRAINT "fk_profile_company_interaction_profile_2" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_request_connection" ADD CONSTRAINT "fk_profile_request_connection_profile_request_status_1" FOREIGN KEY ("profile_request_status_id") REFERENCES "dinbog"."profile_request_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_request_connection" ADD CONSTRAINT "fk_profile_request_connection_type_connection_1" FOREIGN KEY ("type_connection_id") REFERENCES "dinbog"."connection_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_share
-- ----------------------------
ALTER TABLE "dinbog"."profile_share" ADD CONSTRAINT "fk_profile_share_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_share" ADD CONSTRAINT "fk_profile_share_profile_2" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_skill
-- ----------------------------
ALTER TABLE "dinbog"."profile_skill" ADD CONSTRAINT "fk_profile_skill_profile_1" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_skill" ADD CONSTRAINT "fk_profile_skill_skill_1" FOREIGN KEY ("skill_id") REFERENCES "dinbog"."skill" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table profile_view
-- ----------------------------
ALTER TABLE "dinbog"."profile_view" ADD CONSTRAINT "fk_profile_view_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."profile_view" ADD CONSTRAINT "fk_profile_view_profile_2" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table site_social_app
-- ----------------------------
ALTER TABLE "dinbog"."site_social_app" ADD CONSTRAINT "site_social_app_site_id_fkey" FOREIGN KEY ("site_id") REFERENCES "dinbog"."site" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."site_social_app" ADD CONSTRAINT "site_social_app_social_app_id_fkey" FOREIGN KEY ("social_app_id") REFERENCES "dinbog"."social_app" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table social_account
-- ----------------------------
ALTER TABLE "dinbog"."social_account" ADD CONSTRAINT "social_account_profile_id_fkey" FOREIGN KEY ("profile_id") REFERENCES "dinbog"."profile" ("id") MATCH FULL ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."social_account" ADD CONSTRAINT "social_account_social_app_id_fkey" FOREIGN KEY ("social_app_id") REFERENCES "dinbog"."social_app" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table ticket
-- ----------------------------
ALTER TABLE "dinbog"."ticket" ADD CONSTRAINT "fk_ops_ticket_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."ticket" ADD CONSTRAINT "fk_ticket_ticket_status_1" FOREIGN KEY ("ticket_status_id") REFERENCES "dinbog"."ticket_status" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user
-- ----------------------------
ALTER TABLE "dinbog"."user" ADD CONSTRAINT "user_user_status_id_fkey" FOREIGN KEY ("user_status_id") REFERENCES "dinbog"."user_status" ("id") MATCH FULL ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user_email
-- ----------------------------
ALTER TABLE "dinbog"."user_email" ADD CONSTRAINT "fk_user_email_user_1" FOREIGN KEY ("user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user_group
-- ----------------------------
ALTER TABLE "dinbog"."user_group" ADD CONSTRAINT "fk_user_type_groups_user_1" FOREIGN KEY ("user_id") REFERENCES "dinbog"."user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."user_group" ADD CONSTRAINT "fk_user_type_roles_user_type_1" FOREIGN KEY ("group_id") REFERENCES "dinbog"."group" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table user_notification
-- ----------------------------
ALTER TABLE "dinbog"."user_notification" ADD CONSTRAINT "fk_user_notification_profile_1" FOREIGN KEY ("owner_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "dinbog"."user_notification" ADD CONSTRAINT "fk_user_notification_profile_2" FOREIGN KEY ("to_profile_id") REFERENCES "dinbog"."profile" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
