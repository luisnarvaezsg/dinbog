ALTER TABLE "dinbog"."profile_category_type" RENAME COLUMN "attachment_id" TO "avatar_id";

ALTER TABLE "dinbog"."profile_category_type"
  DROP CONSTRAINT "profile_category_type_attachment_id_fkey",
  ADD COLUMN "banner_id" int4,
  ADD CONSTRAINT "profile_category_type_attachment_id_fkey" FOREIGN KEY ("avatar_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD FOREIGN KEY ("banner_id") REFERENCES "dinbog"."attachment" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;