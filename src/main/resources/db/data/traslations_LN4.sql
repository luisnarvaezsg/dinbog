DELETE FROM dinbog.traslations WHERE  key LIKE 'country%' AND iso_code_language = 'ES';
DELETE FROM dinbog.traslations WHERE  key LIKE 'city%' AND iso_code_language = 'ES';
INSERT INTO dinbog.traslations
SELECT key, 'ES', value, module, status FROM dinbog.traslations WHERE key LIKE 'country%' AND iso_code_language = 'EN';
INSERT INTO dinbog.traslations
SELECT key, 'ES', value, module, status FROM dinbog.traslations WHERE key LIKE 'city%' AND iso_code_language = 'EN';