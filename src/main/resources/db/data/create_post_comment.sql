drop table post_comment;
CREATE TABLE dinbog.post_comment
(
    id serial NOT NULL,
    post_id integer NOT NULL,
    comment_id integer NOT NULL,
    CONSTRAINT post_comment_pkey PRIMARY KEY (id),
    
    CONSTRAINT fk_post_comment_comment FOREIGN KEY (comment_id)
        REFERENCES dinbog.comment (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict,
    CONSTRAINT fk_post_comments_po FOREIGN KEY (post_id)
        REFERENCES dinbog.post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict
)

TABLESPACE pg_default;

ALTER TABLE dinbog.post_comment
    OWNER to postgres;   
    

ALTER TABLE dinbog.comment
    ADD CONSTRAINT fk_comment_comment FOREIGN KEY (parent_id)
    REFERENCES dinbog.comment (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE RESTRICT;
    
    
INSERT INTO dinbog.traslations(key, iso_code_language, value, module)
			VALUES ('comment.not.found', 'EN', 'Comment not found, empty or null', 'backend'),
			 ('comment.not.found', 'ES', 'El comentario no fue encontrado, esta vacío o es null', 'backend');    
