CREATE TABLE dinbog.comment
(
    id serial NOT NULL,
    parent_id integer,
    profile_id integer NOT NULL,
    created timestamp with time zone DEFAULT now(),
    count_like integer DEFAULT 0,
    value character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT comment_pkey PRIMARY KEY (id),
    CONSTRAINT fk_post_comment_profile_1 FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE dinbog.comment
    OWNER to postgres;
    
drop table dinbog.post_comment_like;
drop table dinbog.post_comment_mention;

CREATE TABLE dinbog.comment_like
(
    id serial NOT NULL,
    comment_id integer NOT NULL,
    profile_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    CONSTRAINT comment_like_pkey PRIMARY KEY (id),
    CONSTRAINT comment_like_comment_id_fkey FOREIGN KEY (comment_id)
        REFERENCES dinbog.comment (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE restrict,
    CONSTRAINT comment_like_profile_id_fkey FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE restrict
)

TABLESPACE pg_default;

ALTER TABLE dinbog.comment_like
    OWNER to postgres;
    
    
CREATE TABLE dinbog.comment_mention
(
    id serial NOT NULL,
    comment_id integer NOT NULL,
    profile_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    CONSTRAINT comment_mention_pkey PRIMARY KEY (id),
    CONSTRAINT comment_mention_comment_id_fkey FOREIGN KEY (comment_id)
        REFERENCES dinbog.comment (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE restrict,
    CONSTRAINT comment_mention_profile_id_fkey FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE restrict
)

TABLESPACE pg_default;

ALTER TABLE dinbog.comment_mention
    OWNER to postgres;
    
    
    