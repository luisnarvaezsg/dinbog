INSERT INTO DINBOG.TRASLATIONS VALUES ('error.countries.not.found','ES','No se encontraron paises','backend');
INSERT INTO DINBOG.TRASLATIONS VALUES ('error.countries.not.found','EN','Countries not found','backend');
INSERT INTO DINBOG.TRASLATIONS VALUES ('error.country.not.found','ES','Pais no encontrado','backend');
INSERT INTO DINBOG.TRASLATIONS VALUES ('error.country.not.found','EN','Country not found','backend');
INSERT INTO DINBOG.TRASLATIONS VALUES ('error.cities.not.found','ES','No se encontraron ciudades','backend');
INSERT INTO DINBOG.TRASLATIONS VALUES ('error.cities.not.found','EN','Cities not found','backend');