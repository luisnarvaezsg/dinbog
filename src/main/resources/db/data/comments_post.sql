CREATE TABLE dinbog.comments_post
(
    id serial NOT NULL,
    parent_id integer,
    profile_id integer NOT NULL,
    created timestamp with time zone DEFAULT now(),
    count_like integer DEFAULT 0,
    value character varying(255) COLLATE pg_catalog."default",
    status smallint,
    CONSTRAINT comments_post_pkey PRIMARY KEY (id),
    CONSTRAINT fk_comment_comment FOREIGN KEY (parent_id)
        REFERENCES dinbog.comments_post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_comment_profile_1 FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE dinbog.comments_post
    OWNER to postgres;


-- Table: dinbog.comment_like

-- DROP TABLE dinbog.comments_post_like;

CREATE TABLE dinbog.comments_post_like
(
    id serial NOT NULL ,
    comments_post_id integer NOT NULL,
    profile_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    status smallint,
    CONSTRAINT comments_post_like_pkey PRIMARY KEY (id),
    CONSTRAINT comment_post_like_comment_id_fkey FOREIGN KEY (comments_post_id)
        REFERENCES dinbog.comments_post (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT comment_like_profile_id_fkey FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE dinbog.comments_post_like
    OWNER to postgres;
    
drop table dinbog.comment_like;


-- Table: dinbog.comments_post_mention;

-- DROP TABLE dinbog.comments_post_mention;

CREATE TABLE dinbog.comments_post_mention
(
    id serial NOT NULL,
    comments_post_id integer NOT NULL,
    profile_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    status smallint,
    CONSTRAINT comments_post_mention_pkey PRIMARY KEY (id),
    CONSTRAINT comment_post_mention_comment_id_fkey FOREIGN KEY (comments_post_id)
        REFERENCES dinbog.comments_post (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT comment_like_profile_id_fkey FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE dinbog.comments_post_mention
    OWNER to postgres;
    
    
drop table dinbog.comment_mention;

drop table  dinbog.comment  cascade;

drop table  dinbog.post_comment;

-- Table: dinbog.post_comment

-- DROP TABLE dinbog.post_comment;

CREATE TABLE dinbog.post_comment
(
    id serial NOT NULL,
    post_id integer NOT NULL,
    comment_post_id integer NOT NULL,
    status smallint,
    CONSTRAINT post_comment_pkey PRIMARY KEY (id),
    CONSTRAINT fk_post_comments_po FOREIGN KEY (post_id)
        REFERENCES dinbog.post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT,
	 CONSTRAINT fk_post_comments_com_post FOREIGN KEY (comment_post_id)
        REFERENCES dinbog.comments_post (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE dinbog.post_comment
	OWNER to postgres;


