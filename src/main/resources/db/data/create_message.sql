
CREATE TABLE dinbog.message
(
    id serial NOT NULL,
    profile_id_src integer,
	profile_id_dest integer,
    content character varying(1500) COLLATE pg_catalog."default",
    message_date timestamp with time zone DEFAULT now(),
    status smallint DEFAULT 1,
    CONSTRAINT message_pkey PRIMARY KEY (id),
    CONSTRAINT fk_profile_id_src FOREIGN KEY (profile_id_src)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT,
	CONSTRAINT fk_profile_id_dest FOREIGN KEY (profile_id_dest)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE dinbog.message
    OWNER to postgres;