CREATE TABLE DINBOG.COMMENT_ATTACHMENT
(
    id serial NOT NULL,
    parent_id integer,
    profile_id integer NOT NULL,
    created timestamp with time zone DEFAULT now(),
    count_like integer DEFAULT 0,
    value character varying(255) COLLATE pg_catalog."default",
    status smallint,
    CONSTRAINT comment_attachment_pkey PRIMARY KEY (id),
    CONSTRAINT fk_at_comment_comment FOREIGN KEY (parent_id)
        REFERENCES dinbog.comment_attachment (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT,
    CONSTRAINT fk_comment_attach_profile FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.COMMENT_ATTACHMENT
    OWNER to postgres;


-- Table: DINBOG.COMMENT_ATTACHMENT_LIKE

DROP TABLE IF EXISTS DINBOG.ATTACHMENT_COMMENT_LIKE;
CREATE TABLE DINBOG.COMMENT_ATTACHMENT_LIKE
(
    id serial NOT NULL ,
    comment_attachment_id integer NOT NULL,
    profile_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    status smallint,
    CONSTRAINT comment_attach_like_pkey PRIMARY KEY (id),
    CONSTRAINT comment_attach_like_comment_id_fkey FOREIGN KEY (comment_attachment_id)
        REFERENCES dinbog.comment_attachment (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT comment_attach_profile_id_fkey FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.COMMENT_ATTACHMENT_LIKE
    OWNER to postgres;
    


-- Table: DINBOG.COMMENT_ATTACHMENT_MENTION;

DROP TABLE IF EXISTS DINBOG.ATTACHMENT_COMMENT_MENTION;

CREATE TABLE DINBOG.COMMENT_ATTACHMENT_MENTION
(
    id serial NOT NULL,
    comment_attachment_id integer NOT NULL,
    profile_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    status smallint,
    CONSTRAINT comments_attachment_mention_pkey PRIMARY KEY (id),
    CONSTRAINT comment_attach_mention_comment_id_fkey FOREIGN KEY (comment_attachment_id)
        REFERENCES dinbog.comment_attachment (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT comment_attach_like_profile_id_fkey FOREIGN KEY (profile_id)
        REFERENCES dinbog.profile (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.COMMENT_ATTACHMENT_MENTION
    OWNER to postgres;
    
DROP TABLE IF EXISTS DINBOG.ATTACHMENT_COMMENT;
CREATE TABLE DINBOG.ATTACHMENT_COMMENT
(
    id serial NOT NULL,
    attachment_id integer NOT NULL,
    comment_id integer NOT NULL,
	status smallint,
    CONSTRAINT attachment_comment_pkey PRIMARY KEY (id),
    
    CONSTRAINT fk_attach_comment_attach FOREIGN KEY (attachment_id)
        REFERENCES dinbog.attachment (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict,
    CONSTRAINT fk_attach_comment_comment FOREIGN KEY (comment_id)
        REFERENCES dinbog.comment_attachment (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.ATTACHMENT_COMMENT
    OWNER to postgres;