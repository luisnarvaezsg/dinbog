CREATE TABLE DINBOG.FILTERS_TYPE
(
    id serial NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    description character varying(255) COLLATE pg_catalog."default",
    created timestamp with time zone DEFAULT now(),
    status smallint default 1,
    CONSTRAINT filter_type_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.FILTERS_TYPE
    OWNER to postgres;


-- Table: DINBOG.FILTERS
CREATE TABLE DINBOG.FILTERS
(
    id serial NOT NULL,
	name character varying(255) COLLATE pg_catalog."default",
    description character varying(255) COLLATE pg_catalog."default",
	filter_type_id integer NOT NULL,
	filter_parent_id integer,
	validator character varying(255) COLLATE pg_catalog."default",
	key character varying(255) COLLATE pg_catalog."default",
    created timestamp with time zone DEFAULT now(),
    status smallint default 1,
    CONSTRAINT filter_pkey PRIMARY KEY (id),
    CONSTRAINT filter_filtertype_id_fkey FOREIGN KEY (filter_type_id)
        REFERENCES dinbog.filters_type (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT filter_paren_id_fkey FOREIGN KEY (filter_parent_id)
        REFERENCES dinbog.filters (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.FILTERS
    OWNER to postgres;
    


-- Table: DINBOG.FILTERS_VALUES;
CREATE TABLE DINBOG.FILTERS_VALUES
(
    id serial NOT NULL,
	description character varying(255) COLLATE pg_catalog."default",
    value character varying(255) COLLATE pg_catalog."default",
	key character varying(255) COLLATE pg_catalog."default",
    filter_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    status smallint default 1,
    CONSTRAINT filter_value_pkey PRIMARY KEY (id),
    CONSTRAINT filter_value_filter_fkey FOREIGN KEY (filter_id)
        REFERENCES dinbog.filters (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.FILTERS_VALUES
    OWNER to postgres;
    
CREATE TABLE DINBOG.CATEGORY_FILTERS
(
    id serial NOT NULL,
    profile_category_type_id integer NOT NULL,
    filter_id integer NOT NULL,
	order_id integer NOT NULL,
	status smallint default 1,
    CONSTRAINT category_filter_pkey PRIMARY KEY (id),
    
    CONSTRAINT fk_catfilter_cattype FOREIGN KEY (profile_category_type_id)
        REFERENCES dinbog.profile_category_type (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict,
    CONSTRAINT fk_catfilter_filter FOREIGN KEY (filter_id)
        REFERENCES dinbog.filters (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.CATEGORY_FILTERS
    OWNER to postgres;
	
	
-- Table: DINBOG.PROFILE_FILTERS_VALUES;	
CREATE TABLE DINBOG.PROFILE_FILTERS_VALUES
(
    id serial NOT NULL,
    profile_category_id integer NOT NULL,
    category_filter_id integer NOT NULL,
	description character varying(255) COLLATE pg_catalog."default",
    value character varying(255) COLLATE pg_catalog."default",
	status smallint default 1,
    
	CONSTRAINT profile_filter_value_pkey PRIMARY KEY (id),
    
    CONSTRAINT filtervalue_profile_fkey FOREIGN KEY (profile_category_id)
        REFERENCES dinbog.profile_category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict,
    CONSTRAINT filtervalue_category FOREIGN KEY (category_filter_id)
        REFERENCES dinbog.category_filters (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE restrict
)

TABLESPACE pg_default;

ALTER TABLE DINBOG.PROFILE_FILTERS_VALUES
    OWNER to postgres;
	
	