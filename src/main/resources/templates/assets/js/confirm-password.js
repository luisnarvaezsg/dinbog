$(document).ready(function(){
    $("#validate").click(function (){
        validatePassword();
    });

    function validatePassword() {
        let validator = $("#loginForm").validate({
            rules: {
                newPassword: "required",
                confirmNewPassword: {
                    equalTo: "#confirmNewPassword"
                }
            },
            messages: {
                newPassword: " Enter Password",
                confirmNewPassword: " Enter Confirm Password Same as Password"
            }
        });
        if (validator.form()) {
            console.log("Bearer " + $("#token").val());
            $.ajax({
                url:document.location.href + '/v1/users/password/new',
                type: 'post',
                headers:{ 'Authorization': 'Bearer '+ $("#token").val() },
                data:{
                    newPassword : $("#newPassword").val()
                },
                dataType: 'json',
                success: function (data){
                    alert("success");
                },
                error: function (error){
                    alert(error);
                }
            });
        }
    }

});

