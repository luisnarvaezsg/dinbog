package com.dinbog.api.aws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.service.IApiErrorService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class S3ServicesImpl implements IS3Services {
	
	private static final String ERROR_MESSAGE = "Error Message: ";

    @Autowired
    private AmazonS3 s3client;

    @Value("${aws.avatar.bucket}")
    private String bucketName;
    
    @Value("${aws.endpoint}")
    private String endpointUrl;
    
    @Value("${path.tmp.file}")
    private String tmpFile;
    
    @Value("${spring.datasource.password}")
    private String servidor;
    
    @Autowired
    private IApiErrorService errorService;
    
    @Override
	public void downloadFile(String keyName) {
        try {
            S3Object s3object = s3client.getObject(new GetObjectRequest(bucketName, keyName));
            log.info("Content-Type: " + s3object.getObjectMetadata().getContentType());
            log.info("===================== Import File - Done! =====================");

        } catch (AmazonServiceException ase) {
            log.info("Caught an AmazonServiceException from GET requests, rejected reasons:");
            log.info("Error Message:    " + ase.getMessage());
            log.info("HTTP Status Code: " + ase.getStatusCode());
            log.info("AWS Error Code:   " + ase.getErrorCode());
            log.info("Error Type:       " + ase.getErrorType());
            log.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            log.info("Caught an AmazonClientException: ");
            log.info(ERROR_MESSAGE + ace.getMessage());
        } 
    }
    
    @Override
	public S3Object downloadFile(String keyName, String bucketName) {
        try {
        	S3Object s3Object = s3client.getObject(new GetObjectRequest(bucketName, keyName));
            log.info("Content-Type: " + s3Object.getObjectMetadata().getContentType());
            log.info("===================== Import File - Done! =====================");
            return s3Object; 

        } catch (AmazonServiceException ase) {
            log.info("Caught an AmazonServiceException from GET requests, rejected reasons:");
            log.info("Error Message:    " + ase.getMessage());
            log.info("HTTP Status Code: " + ase.getStatusCode());
            log.info("AWS Error Code:   " + ase.getErrorCode());
            log.info("Error Type:       " + ase.getErrorType());
            log.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            log.info("Caught an AmazonClientException: ");
            log.info(ERROR_MESSAGE + ace.getMessage());
        }
		return null; 
    }
    
    @Override
	public String uploadFile(MultipartFile multipartFile) {

        String fileUrl = "";
       
        try {
        	if(convertMultiPartToFile(multipartFile).isFile()) {
        		File file = convertMultiPartToFile(multipartFile);
	            String fileName = generateFileName(multipartFile);
	            fileUrl = "https://"+bucketName+"."+endpointUrl+"/" + fileName;
	            uploadFileTos3bucket(fileName, file);
	            if(file.delete())
	            	log.info("MultipartFile deleted successful");
        	}
        } catch (Exception e) {
        	throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.NO_SUCH_BUCKET, CommonsConstant.LANGUAGE_BY_DEFAULT)));
        }
        return fileUrl;
    }

    @Override
   	public String uploadFile(MultipartFile multipartFile, String bucket) {

           String fileName = "";
           try {
        	   if(convertMultiPartToFile(multipartFile).isFile()) {
	               File file = convertMultiPartToFile(multipartFile);
	               
	               fileName = generateFileName(multipartFile);
	               uploadFileTos3bucket(fileName, file, bucket);
	               if(file.delete())
	               	log.info("MultipartFile deleted successful");
        	   }
           } catch (Exception e) {
        	  throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.NO_SUCH_BUCKET,  CommonsConstant.LANGUAGE_BY_DEFAULT)));
           }
           return fileName;
       }
    
    
    private void uploadFileTos3bucket(String fileName, File file) {
        
    	s3client.putObject(new PutObjectRequest(bucketName, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }    
    
    private void uploadFileTos3bucket(String fileName, File file, String bucketname) {
        
    	s3client.putObject(new PutObjectRequest(bucketname, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }  
    
    private File convertMultiPartToFile(MultipartFile file) throws IOException{
        String fileName=file.getOriginalFilename();
        if (servidor.equals("poBDD202*=="))
        	fileName=tmpFile+fileName;
    	File convFile = new File(fileName);
        
    	try (FileOutputStream fos = new FileOutputStream(convFile)){
	   
    		fos.write(file.getBytes());
	   
    	}catch(IOException ex ) {
    		java.util.logging.Logger.getLogger(S3ServicesImpl.class.getName()).log(Level.SEVERE, null, ex);
    	} 
        
    	return convFile;
    }
    
    
    private String generateFileName(MultipartFile multiPart) {
    	
    	String originalFileName = multiPart.getOriginalFilename();
    	Optional<String> oNombre = Optional.of(originalFileName);
    	
    	if(!oNombre.isEmpty()) 
    		originalFileName = new Date().getTime() + "-" + oNombre.get().replace(" ", "_");
		        
    	return originalFileName;
    }
    
    
    @Override
	public String deleteFileFromS3Bucket(String fileUrl) {
        
    	String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        s3client.deleteObject(new DeleteObjectRequest(bucketName + "/", fileName));
        return "Successfully deleted";
    }
}
	
	

