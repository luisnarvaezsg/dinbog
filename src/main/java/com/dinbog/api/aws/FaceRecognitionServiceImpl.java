package com.dinbog.api.aws;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.*;
import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.service.IApiErrorService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.ByteBuffer;


@Service
@Slf4j
public class FaceRecognitionServiceImpl implements IFaceRecognitionService {

	@Autowired
    private AmazonRekognition rekognitionClient;
	
	@Autowired
    private IApiErrorService errorService;

	@Autowired
	private IS3Services s3Service;
       
	private static final Float FACE_RECOGNITION_TOLERANCY = 99.96f;
    
    @Override
	public Avatar initFaceRecognition(MultipartFile multipartFile, Long profileType) {

    	Avatar avatar = new Avatar();
    	ObjectMapper objectMapper = new ObjectMapper();
    	
        try {
        	if(profileType.equals(ProfileConstant.COMPANY_PROFILE_TYPE)) {
        		
        		avatar.setValidAvatar(true);
                String urlFile = s3Service.uploadFile(multipartFile);
                avatar.setUrlFile(urlFile);
                
        	}else if (profileType.equals(ProfileConstant.TALENT_PROFILE_TYPE)) {
        		
	            log.info("Preparing avatar for image:" + multipartFile.getName());
	            Image image = new Image().withBytes(ByteBuffer.wrap(multipartFile.getBytes()));
	            DetectFacesResult faceResult = awsDetectFaces(image, Attribute.ALL);
	            
	            if (faceResult.getFaceDetails() != null && !faceResult.getFaceDetails().isEmpty()) {
	            	
	            	if(faceResult.getFaceDetails().size() > 1) {
	            		log.info("Error: Many faces");
    	                avatar.setValidAvatar(false);
    	                avatar.setMessage("many.faces");
	            	}else {
		            	FaceDetail face = faceResult.getFaceDetails().get(0);
		                if((Float.compare(face.getConfidence(), FACE_RECOGNITION_TOLERANCY) >= 0)) {
		                	
		                	Boolean b = face.getSunglasses().getValue();
		                	if (Boolean.TRUE.equals(b)){
		                		log.info("Error: face with sunglasses");
		    	                avatar.setValidAvatar(false);
		    	                avatar.setMessage("face.with.sunglasses");
		                	}else {
		                		
		                		avatar.setValidAvatar(true);
				                String urlFile = s3Service.uploadFile(multipartFile);
				                avatar.setUrlFile(urlFile);
				                avatar.setMessage("face.recognition.ok");
				                log.info("Face detected by Rekognition");
		                	}
		                }else {
		                	log.info("Error: face with low level confidence");
	    	                avatar.setValidAvatar(false);
	    	                avatar.setMessage("low.level.confidence");
		                }
	            
	            	
	                if (log.isDebugEnabled()) {
	                    log.debug("Face features:");
	                    log.debug(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(face));
	                }
	            }
	            } else {
	                log.info("No face detected by Rekognition");
	                avatar.setValidAvatar(false);
	                avatar.setMessage("face.not.detected");
	            }
        	}

            if (log.isDebugEnabled()) {
                log.debug("Avatar features:");
                log.debug(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(avatar));
            }
        } catch (IOException e) {
        	log.error("Exception: "+ e);
        }

        return avatar;
    }

    private DetectFacesResult awsDetectFaces(Image image, Attribute attributes) {
    	
    	try {
	        DetectFacesRequest request = new DetectFacesRequest()
	                .withImage(image)
	                .withAttributes(attributes);
	        return rekognitionClient.detectFaces(request);
    	}catch(Exception e) {
    		throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, null)));
    	}
    }
    
}