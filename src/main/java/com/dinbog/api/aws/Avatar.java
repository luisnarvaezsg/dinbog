package com.dinbog.api.aws;

import com.amazonaws.services.rekognition.model.FaceDetail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Avatar {
	
	private boolean validAvatar;
	
	private String urlFile;
	
	private FaceDetail face;
	
	
	private String message;

}
