package com.dinbog.api.aws;

import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3Object;

public interface IS3Services {

	void downloadFile(String keyName);

	String uploadFile(MultipartFile multipartFile);

	String deleteFileFromS3Bucket(String fileUrl);

	String uploadFile(MultipartFile multipartFile, String bucket);

	S3Object downloadFile(String keyName, String bucketName);

}