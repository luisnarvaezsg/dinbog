package com.dinbog.api.aws;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@CrossOrigin(allowCredentials = "true")
@RestController
@RequestMapping("/v1/aws")
public class AWSController {

    @Autowired
    private IFaceRecognitionService faceRecognitionService;
    
    @Autowired
    private IS3Services s3Service;

    @PostMapping("/recognition")
    public Avatar initRecognition(@RequestPart(value = "file", required = true) MultipartFile photo,
    							  @RequestParam(required = true) Long profileType) {
        return this.faceRecognitionService.initFaceRecognition(photo, profileType);
    }

    @PostMapping("upload")
    public String upload(@RequestPart(value = "file") MultipartFile file) {
        return this.s3Service.uploadFile(file);
    }
    
    @GetMapping("download/{keyName}")
    public void download(@PathVariable String keyName) {
    	this.s3Service.downloadFile(keyName);
    }
    
    @GetMapping("delete/{fileUrl}")
    public String delete(@PathVariable String fileUrl) {
    	return this.s3Service.deleteFileFromS3Bucket(fileUrl);
    }
}
