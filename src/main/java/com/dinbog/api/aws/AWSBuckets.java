package com.dinbog.api.aws;

public enum AWSBuckets {
    POSTS("dinbog-post");

    private final String bucket;

    AWSBuckets(String bucket) {
        this.bucket = bucket;
    }

    public String getBucket() {
        return bucket;
    }
}
