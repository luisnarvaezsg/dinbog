package com.dinbog.api.aws;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Component
public class AWSConfig {
	
	 @Value("${aws.accessKey}")
	 private String accessKey;
	 
	 @Value("${aws.secretKey}")
	 private String secretKey;
	 
	 @Value("${aws.region}")
	 private String region;
	 
	 
	 @SuppressWarnings("deprecation")
	 @Bean 
	 public AmazonRekognition rekognitionClient() {
		 
		 AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		 AmazonRekognition rekognitionClient = new AmazonRekognitionClient(credentials);
         rekognitionClient.setRegion(RegionUtils.getRegion(region));
        
         return rekognitionClient;
     }
       
     
     @Bean 
     public AmazonS3 s3client() {
         
    	 BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
         
    	 return  AmazonS3ClientBuilder.standard()
                                     .withRegion(Regions.fromName(region))
                                     .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                                     .build();
     }
	
}
