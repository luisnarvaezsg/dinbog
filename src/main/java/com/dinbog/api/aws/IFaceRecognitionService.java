package com.dinbog.api.aws;

import org.springframework.web.multipart.MultipartFile;

public interface IFaceRecognitionService {

	Avatar initFaceRecognition(MultipartFile multipartFile, Long profileType);

}