/**
 * 
 */
package com.dinbog.api.exception;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.http.HttpStatus;

/**
 * RuntimeException para manejar los errores causados por la data del request
 * 
 * @author Luis
 *
 */
public class InvalidRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final String CODE = "E-002";
	public static final String MSG = "Error causado por la información del request";
	public static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;
	private final MutableList<ApiError> apiErrors;
	private final String detail;


	/**
	 * @param apiErrors
	 * @param detail
	 */
	public InvalidRequestException(MutableList<ApiError> apiErrors, String detail) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = detail;
	}

	/**
	 * @param apiErrors
	 */
	public InvalidRequestException(MutableList<ApiError> apiErrors) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = null;
	}
	
	public InvalidRequestException(String message) {
		super(MSG);
		this.apiErrors = null;
		this.detail = null;
	}

	/**
	 * @return the apiErrors
	 */
	public MutableList<ApiError> getApiErrors() {
		return apiErrors;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}
}
