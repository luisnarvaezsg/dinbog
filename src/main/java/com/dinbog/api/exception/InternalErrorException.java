/**
 * 
 */
package com.dinbog.api.exception;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.http.HttpStatus;

import com.dinbog.api.util.Util;

/**
 * RuntimeException para manejar los errores que son responsabilidad de la API,
 * se encapsula cualquier error no controlado de ser necesario
 * 
 * @author Luis
 *
 */
public class InternalErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final String CODE = "E-001";
	public static final String MSG = "Error interno de la API";
	public static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;
	private final MutableList<ApiError> apiErrors;
	private final String detail;

	/**
	 * @param apiErrors
	 * @param detail
	 */
	public InternalErrorException(MutableList<ApiError> apiErrors, String detail) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = detail;
	}

	/**
	 * @param apiErrors
	 */
	public InternalErrorException(MutableList<ApiError> apiErrors) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = null;
	}

	/**
	 * Constructor para encapsular errores no controlados
	 * 
	 * @param cause
	 */
	public InternalErrorException(Throwable cause) {
		super(cause.getMessage(), cause);
		this.apiErrors = null;
		this.detail = Util.getTrowableDetail(cause);
	}
	
	/**
	 * @return the apiErrors
	 */
	public MutableList<ApiError> getApiErrors() {
		return apiErrors;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}
}
