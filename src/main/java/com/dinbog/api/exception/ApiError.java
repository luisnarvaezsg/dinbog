package com.dinbog.api.exception;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Luis
 *
 */
@Getter
@Setter
public class ApiError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8686861622224366031L;

	private Integer errorCode;
	private String message;

	/**
	 * 
	 */
	public ApiError() {
		super();
	}

	/**
	 * @param status
	 * @param errorCode
	 * @param message
	 */
	public ApiError(final Integer errorCode, final String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}
}
