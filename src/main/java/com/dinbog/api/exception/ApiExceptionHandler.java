package com.dinbog.api.exception;

import java.util.Optional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dinbog.api.util.Util;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	// Excepciones pendientes de validar si necesitan handler:
	// - HttpMessageNotReadableException
	// - HttpMessageNotWritableException
	// - DataIntegrityViolationException
	// - HttpMediaTypeNotAcceptableException
	// - MissingPathVariableException
	// - ServletRequestBindingException
	// - ConversionNotSupportedException
	// - AsyncRequestTimeoutException

	// ========================================================================
	// EXCEPCIONES A MANEJARSE BAJO ** InternalErrorException **
	// E-001: Errores internos de la API

	/**
	 * Errores No Controlados arrojados en tiempo de ejecución
	 * 
	 * @param ex      InternalErrorException
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ InternalErrorException.class })
	protected ResponseEntity<Object> internalErrorException(final InternalErrorException ex, final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex, ex.getApiErrors()),InvalidRequestException.HTTP_STATUS);
	}

	
	/**
	 * Errores No Controlados CAPTURADOS en tiempo de ejecución
	 * 
	 * @param ex      Exception
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
		ex.printStackTrace();
		return new ResponseEntity<>(Util.printException(ex),InvalidRequestException.HTTP_STATUS);
	}

	// ========================================================================
	// EXCEPCIONES A MANEJARSE BAJO ** InvalidRequestException **
	// E-002: Errores causados directamente por el request

	/**
	 * Errores controlados arrojados en tiempo de ejecución
	 * 
	 * @param ex      InvalidRequestException
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ InvalidRequestException.class })
	protected ResponseEntity<Object> invalidRequestException(final InvalidRequestException ex,
			final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex, ex.getApiErrors()),InvalidRequestException.HTTP_STATUS);
	}

	/**
	 * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid
	 * validation.
	 *
	 * @param ex      the MethodArgumentNotValidException that is thrown when @Valid
	 *                validation fails
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

		log.info(ex.getClass().getName());
		final MutableList<ApiError> apiErrors = Lists.mutable.empty();
		ApiError apiError;

		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			apiError = new ApiError();
			apiError.setErrorCode(ApiErrorEnum.VALID_ERROR_FIELD.getCode());
			apiError.setMessage(ApiErrorEnum.VALID_ERROR_FIELD.getMessage() + ": [" + error.getField() + "] - "
					+ error.getDefaultMessage());
			apiErrors.add(apiError);
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			apiError = new ApiError();
			apiError.setErrorCode(ApiErrorEnum.VALID_ERROR_OBJECT.getCode());
			apiError.setMessage(ApiErrorEnum.VALID_ERROR_OBJECT.getMessage() + ": [" + error.getObjectName() + "] - "
					+ error.getDefaultMessage());
			apiErrors.add(apiError);
		}
		return new ResponseEntity<>(Util.printException(ex, apiErrors),InvalidRequestException.HTTP_STATUS);
	}

	/**
	 * Handle BindException. Triggered when cannot bind request parameters to match
	 * Input objects
	 *
	 * @param ex      BindException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers,
			final HttpStatus status, final WebRequest request) {

		log.info(ex.getClass().getName());
		final MutableList<ApiError> apiErrors = Lists.mutable.empty();
		ApiError apiError;

		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			apiError = new ApiError();
			apiError.setErrorCode(ApiErrorEnum.BIND_ERROR_FIELD.getCode());
			apiError.setMessage(ApiErrorEnum.BIND_ERROR_FIELD.getMessage() + ": [" + error.getField() + "] - "
					+ error.getDefaultMessage());
			apiErrors.add(apiError);
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			apiError = new ApiError();
			apiError.setErrorCode(ApiErrorEnum.BIND_ERROR_OBJECT.getCode());
			apiError.setMessage(ApiErrorEnum.BIND_ERROR_OBJECT.getMessage() + ": [" + error.getObjectName() + "] - "
					+ error.getDefaultMessage());
			apiErrors.add(apiError);
		}
		return new ResponseEntity<>(Util.printException(ex,apiErrors),InvalidRequestException.HTTP_STATUS);
	}

	/**
	 * Handle TypeMismatchException. Triggered when trying to bind an argument of an
	 * unsupported data type
	 *
	 * @param ex      TypeMismatchException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers,
			final HttpStatus status, final WebRequest request) {

		log.info(ex.getClass().getName());
		
		

		return new ResponseEntity<>(Util.printException(ex,Lists.mutable.of(new ApiError(ApiErrorEnum.TYPE_MISMATCH_ERROR.getCode(),
		ApiErrorEnum.TYPE_MISMATCH_ERROR.getMessage() + ": The value [" + ex.getValue()
		+ "] of the argument [" + ex.getPropertyName() + "] should be of type "))),InvalidRequestException.HTTP_STATUS);
		
	}

	/**
	 * Handle MethodArgumentTypeMismatchException. Triggered when trying to bind to
	 * a METHOD argument an unsupported data type
	 *
	 * @param ex      MethodArgumentTypeMismatchException
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException ex,
			final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex, 
				Lists.mutable.of(new ApiError(ApiErrorEnum.TYPE_MISMATCH_METHOD_ERROR.getCode(),
				ApiErrorEnum.TYPE_MISMATCH_METHOD_ERROR.getMessage() + ": The method argument [" + ex.getName()
				+ "] should be of type "))),
				InvalidRequestException.HTTP_STATUS);
	}

	/**
	 * Handle MissingServletRequestPartException. Triggered when a part of a
	 * multipart request, identified by name, cannot be found
	 *
	 * @param ex      MissingServletRequestPartException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex, Lists.mutable.of(new ApiError(ApiErrorEnum.MISSING_REQUEST_PART.getCode(),
				"The [" + ex.getRequestPartName() + "] "
						+ ApiErrorEnum.MISSING_REQUEST_PART.getMessage()))),InvalidRequestException.HTTP_STATUS);
		
	}

	/**
	 * Handle MissingServletRequestParameterException. Triggered when a 'required'
	 * request parameter is missing.
	 *
	 * @param ex      MissingServletRequestParameterException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex, Lists.mutable.of(new ApiError(ApiErrorEnum.MISSING_REQUEST_PARAMETER.getCode(),
				"The [" + ex.getParameterName() + "] "
						+ ApiErrorEnum.MISSING_REQUEST_PARAMETER.getMessage()))),InvalidRequestException.HTTP_STATUS);

	}

	/**
	 * Handles ConstraintViolationException. Thrown when @Validated fails
	 * 
	 * @param ex      ConstraintViolationException
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex,
			final WebRequest request) {

		final MutableList<ApiError> apiErrors = Lists.mutable.empty();
		ApiError apiError;

		for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			apiError = new ApiError();
			apiError.setErrorCode(ApiErrorEnum.CONSTRAINT_VIOLATION.getCode());
			apiError.setMessage(ApiErrorEnum.CONSTRAINT_VIOLATION.getMessage() + ": "
					+ violation.getRootBeanClass().getSimpleName() + "| en la ruta: "
					+ violation.getPropertyPath().toString() + "| con el valor: " + violation.getInvalidValue());
			apiErrors.add(apiError);
		}

		return new ResponseEntity<>(Util.printException(ex, apiErrors),InvalidRequestException.HTTP_STATUS);
	}

	/**
	 * Handle HttpRequestMethodNotSupportedException. Triggered when a request is
	 * made to an endpoint with an unsupported http method
	 *
	 * @param ex      HttpRequestMethodNotSupportedException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {

		final StringBuilder sb = new StringBuilder(ex.getMethod())
				.append(ApiErrorEnum.HTTP_METHOD_NOT_SUPPORTED.getMessage() + ". Supported methods are: ");
		
		return new ResponseEntity<>(Util.printException(ex, Lists.mutable.of(new ApiError(ApiErrorEnum.HTTP_METHOD_NOT_SUPPORTED.getCode(),
				sb.toString()))),InvalidRequestException.HTTP_STATUS);
	}

	/**
	 * Handle HttpMediaTypeNotSupportedException. This one triggers when JSON is
	 * invalid as well.
	 *
	 * @param ex      HttpMediaTypeNotSupportedException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

		Optional<MediaType> mediaType = Optional.of(ex.getContentType());
		final StringBuilder sb = new StringBuilder();
		if(mediaType.isPresent()) {
			sb.append(mediaType.get().toString())
			.append(" media type is not supported. Supported media types are ");
		}

		ex.getSupportedMediaTypes().forEach(t -> sb.append(t + " "));

		return new ResponseEntity<>(Util.printException(ex, Lists.mutable.of(new ApiError(ApiErrorEnum.HTTP_METHOD_NOT_SUPPORTED.getCode(),
				sb.toString()))),InvalidRequestException.HTTP_STATUS);
	}
	
	
	// ========================================================================
	// EXCEPCIONES A MANEJARSE BAJO ** BadCredentialsException **
	// E-003: Errores debido a problemas con alguna de las credenciales del request

	/**
	 * Errores controlados arrojados en tiempo de ejecución
	 * 
	 * @param ex      BadCredentialsException
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ BadCredentialsException.class })
	protected ResponseEntity<Object> badCredentialsException(final BadCredentialsException ex,
			final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex),InvalidRequestException.HTTP_STATUS);
	}
	
	
	@ExceptionHandler({ ExpiredJwtException.class })
	protected ResponseEntity<Object> expiredJWTException(final BadCredentialsException ex,
			final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex),InvalidRequestException.HTTP_STATUS);
	}
	/**
	 * Errores controlados arrojados en tiempo de ejecución
	 * 
	 * @param ex      ResourceNotFoundException
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@ExceptionHandler({ ResourceNotFoundException.class })
	protected ResponseEntity<Object> resourceNotFoundException(final ResourceNotFoundException ex,
			final WebRequest request) {

		return new ResponseEntity<>(Util.printException(ex, ex.getApiErrors()),ResourceNotFoundException.HTTP_STATUS);
	}

	/**
	 * Handle NoHandlerFoundException. Triggered when a request is made to a path
	 * inside the API that is not handled by any controller
	 *
	 * @param ex      NoHandlerFoundException
	 * @param headers HttpHeaders
	 * @param status  HttpStatus
	 * @param request WebRequest
	 * @return the ApiExceptionResponse object
	 */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {


		return new ResponseEntity<>(Util.printException(ex, Lists.mutable.of(new ApiError(ApiErrorEnum.METHOD_HADLER_NOT_FOUND.getCode(),
				ApiErrorEnum.METHOD_HADLER_NOT_FOUND.getMessage() + " [" + ex.getHttpMethod()
				+ "] en la ruta: " + ex.getRequestURL()))),ResourceNotFoundException.HTTP_STATUS);
		
	}

}
