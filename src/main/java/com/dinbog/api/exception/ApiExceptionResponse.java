/**
 * 
 */
package com.dinbog.api.exception;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;

/**
 * Clase para responder TODOS los errores que se generen
 * 
 * @author Luis
 *
 */
@Getter
public class ApiExceptionResponse extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6258757180048051041L;
	public static final String MSG = "Error en la aplicacion. ";
	private String code;
	private String message;
	private String timestamp;
	private MutableList<ApiError> apiErrors;
	private String detail;

	/**
	 * @param apiErrors
	 */
	public ApiExceptionResponse(MutableList<ApiError> apiErrors) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = null;
	}
	/**
	 * @param code
	 * @param message
	 * @param timestamp
	 * @param subErrors
	 * @param details
	 */
	public ApiExceptionResponse(String code, String message, MutableList<ApiError> subErrors, String details) {

		this.code = code;
		this.message = message;
		this.timestamp = ZonedDateTime.of(LocalDateTime.now(ZoneOffset.UTC), ZoneId.of("UTC"))
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"));
		this.apiErrors = subErrors;
		this.detail = details;
	}
}
