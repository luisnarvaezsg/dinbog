/**
 * 
 */
package com.dinbog.api.exception;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.http.HttpStatus;

/**
 * RuntimeException para indicar que el recurso solicitado no pudo encontrarse
 * 
 * @author Luis
 *
 */
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final String CODE = "E-004";
	public static final String MSG = "No se pudo encontrar el recurso solicitado";
	public static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;
	private final MutableList<ApiError> apiErrors;
	private final String detail;

	/**
	 * @param apiErrors
	 * @param detail
	 */
	public ResourceNotFoundException(MutableList<ApiError> apiErrors, String detail) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = detail;
	}

	/**
	 * @param apiErrors
	 */
	public ResourceNotFoundException(MutableList<ApiError> apiErrors) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = null;
	}

	/**
	 * @return the apiErrors
	 */
	public MutableList<ApiError> getApiErrors() {
		return apiErrors;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}
}
