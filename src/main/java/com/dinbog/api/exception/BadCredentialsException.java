/**
 * 
 */
package com.dinbog.api.exception;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.http.HttpStatus;

/**
 * RuntimeException para manejar los errores con las credenciales del request
 * 
 * @author Luis
 *
 */
public class BadCredentialsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final String CODE = "E-003";
	public static final String MSG = "Error causado por un problema con alguna de las credenciales del request";
	public static final HttpStatus HTTP_STATUS = HttpStatus.FORBIDDEN;
	private final MutableList<ApiError> apiErrors;
	private final String detail;

	/**
	 * @param apiErrors
	 * @param detail
	 */
	public BadCredentialsException(MutableList<ApiError> apiErrors, String detail) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = detail;
	}

	/**
	 * @param apiErrors
	 */
	public BadCredentialsException(MutableList<ApiError> apiErrors) {
		super(MSG);
		this.apiErrors = apiErrors;
		this.detail = null;
	}

	/**
	 * @return the apiErrors
	 */
	public MutableList<ApiError> getApiErrors() {
		return apiErrors;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}
}
