package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>, JpaSpecificationExecutor<Event> {
	
	@Query(value = "SELECT * FROM event WHERE owner_profile_id=:ownerProfileId", nativeQuery = true)
	List<Event> findByOwnerId(@Param("ownerProfileId") Long id);
}
