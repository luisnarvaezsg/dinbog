package com.dinbog.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.CommentAttachmentLike;


@Repository
public interface CommentAttachmentLikeRepository
		extends JpaRepository<CommentAttachmentLike, Long>, JpaSpecificationExecutor<CommentAttachmentLike> {

}
