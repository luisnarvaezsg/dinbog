package com.dinbog.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.AttachmentType;

@Repository
public interface AttachmentTypeRepository
		extends JpaRepository<AttachmentType, Long>, JpaSpecificationExecutor<AttachmentType> {

	//@Query(value = "SELECT * FROM attachment_type WHERE value=:value", nativeQuery = true)
	Optional<AttachmentType> findByValue(@Param("value") String value);
}
