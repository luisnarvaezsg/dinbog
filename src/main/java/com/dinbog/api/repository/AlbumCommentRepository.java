package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.AlbumComment;

import java.util.List;

@Repository
@Transactional
public interface AlbumCommentRepository
		extends JpaRepository<AlbumComment, Long>, JpaSpecificationExecutor<AlbumComment> {

	@Query(value = "DELETE FROM album_comment WHERE album_id=:albumId AND id=:id", nativeQuery = true)
	@Modifying
	void deleteByAlbumIdAndById(@Param("albumId") Long albumId, @Param("id") Long id);

	List<AlbumComment> findAllByAlbum(Album album);
}
