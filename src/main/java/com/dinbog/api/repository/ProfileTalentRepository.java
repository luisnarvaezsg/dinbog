package com.dinbog.api.repository;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileTalent;


public interface ProfileTalentRepository extends JpaRepository<ProfileTalent, Long>, JpaSpecificationExecutor<ProfileTalent> {
	
	@Query("select p from ProfileTalent p inner join fetch p.profile f where f.id=?1")
	Optional<ProfileTalent> findByProfile(Long id);
	
	Optional<ProfileTalent> findByProfile(Profile profile);

	@Query("select pt.profile from ProfileTalent pt where pt.firstName like ?1% or pt.lastName like ?1%")
	Page<Profile> findByFirstNameOrLastNameContainingIgnoreCase(Pageable pageable, String firstName, String lastName);
	
	@Query("select pt.profile from ProfileTalent pt where pt.profile.fullName like CONCAT('%',:fullName,'%') and pt.profile.id NOT IN (:id)")
	Page<Profile> findByFullNameContainingIgnoreCase(Long id, Pageable pageable, String fullName);
	
	@Query("select pt.profile from ProfileCompany pt where pt.profile.fullName like CONCAT('%',:fullName,'%') and pt.profile.id NOT IN (:id) ")
	Page<Profile> findByFullNameCompanyContainingIgnoreCase(Long id, Pageable pageable, String fullName);
	
}
