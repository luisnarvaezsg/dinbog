package com.dinbog.api.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentComment;
import com.dinbog.api.models.entity.CommentAttachment;

@Repository
public interface AttachmentCommentRepository 
		extends JpaRepository<AttachmentComment, Long>, JpaSpecificationExecutor<AttachmentComment> {
	
	Optional<List<AttachmentComment>> findByAttachmentAndStatus(Attachment attachment, int status);
	
	Optional<AttachmentComment> findByCommentAttachment(CommentAttachment attachment);

	List<AttachmentComment> findAllByAttachment(Attachment attachment);

}
