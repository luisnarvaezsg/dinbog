package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRequestStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.ProfileRequestConnection;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProfileRequestConnectionRepository extends JpaRepository<ProfileRequestConnection, Long>, JpaSpecificationExecutor<ProfileRequestConnection> {

    List<ProfileRequestConnection> findProfileRequestConnectionByProfile_IdAndOwner_Id(Long profileId,Long ownerId);

    @Query(
            "select pr from ProfileRequestConnection pr " +
            "where pr.id = :id and pr.owner.id = :ownerId " +
            "and (pr.profileRequestStatus.id = 1L OR pr.profileRequestStatus.id = 2L)"
    )
    Optional<ProfileRequestConnection> findByIdAndOwnerIdAndStatusActiveOrPending(@Param("id") Long id,@Param("ownerId") Long ownerId);

    Optional<ProfileRequestConnection> findByIdAndProfile_IdAndProfileRequestStatus_Id(Long id, Long profileId,Long requestStatus);

    List<ProfileRequestConnection> findAllByProfile_IdAndProfileRequestStatus_Id(Long profileId,Long requestStatus,Pageable pageable);

    @Query(
            "select pr from ProfileRequestConnection pr " +
            "where pr.profileRequestStatus.id = :status and (pr.profile.id = :profileId or pr.owner.id=:profileId)"
    )
    List<ProfileRequestConnection>findAllConnectionsByStatusRequest(Long profileId,Long status,Pageable pageable);

}
