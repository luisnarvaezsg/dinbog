package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.CommentsPost;

@Repository
@Transactional
public interface CommentsPostRepository extends JpaRepository<CommentsPost, Long>, JpaSpecificationExecutor<CommentsPost> {
	
}
