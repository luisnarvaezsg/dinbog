package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileAttachment;

@Repository
@Transactional
public interface ProfileAttachmentRepository
		extends JpaRepository<ProfileAttachment, Long>, JpaSpecificationExecutor<ProfileAttachment> {

	@Query(value = "SELECT * FROM profile_attachment WHERE profile_id=:profileId and attachment_id=:attachmentId", nativeQuery = true)
	Optional<ProfileAttachment> findByProfileAttachment(@Param("profileId") Long profileId,
			@Param("attachmentId") Long attachmentId);

	@Query(value = "SELECT * FROM profile_attachment WHERE profile_id=:profileId", nativeQuery = true)
	List<ProfileAttachment> findByProfileAttachment(@Param("profileId") Long profileId);
}
