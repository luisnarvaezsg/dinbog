package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileLike;

@Repository
@Transactional
public interface ProfileLikeRepository  extends JpaRepository<ProfileLike, Long>, JpaSpecificationExecutor<ProfileLike> {

    /**
     * @param profileId
     * @return
     */
    List<ProfileLike> findByProfileOwner(Profile profileOwner);

    /**
     * @param ownerProfileId
     * @param profileId
     * @return
     */
    Optional<ProfileLike> findByProfileOwnerAndProfile(Profile profileOwner, Profile profileLike);

}

    
