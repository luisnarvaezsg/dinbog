package com.dinbog.api.repository;

import com.dinbog.api.models.entity.CommentsAlbum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CommentsAlbumRepository extends JpaRepository<CommentsAlbum,Long>, JpaSpecificationExecutor<CommentsAlbum> {

    List<CommentsAlbum> findAllByParent_Id(Long Id);
}
