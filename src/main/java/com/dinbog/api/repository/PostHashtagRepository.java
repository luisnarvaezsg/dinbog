package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostHashtag;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostHashtagRepository extends JpaRepository<PostHashtag, Long>, JpaSpecificationExecutor<PostHashtag> {

	Optional<PostHashtag> findByNameHasTag(@Param("nameHasTag") String nameHasTag);
	
	List<PostHashtag> findByPost(Post post);
	
	void deleteByPost(Post post);
	
	Page<PostHashtag> findByNameHasTagContainingIgnoreCase(Pageable pageable, String hashtag);
}
