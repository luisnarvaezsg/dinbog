/**
 * 
 */
package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dinbog.api.models.entity.SocialAccount;

/**
 * @author Luis
 *
 */
public interface SocialAccountRepository
		extends JpaRepository<SocialAccount, Long>, JpaSpecificationExecutor<SocialAccount> {

}
