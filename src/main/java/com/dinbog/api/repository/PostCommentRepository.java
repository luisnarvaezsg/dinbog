package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostComment;


@Repository
@Transactional
public interface PostCommentRepository extends JpaRepository<PostComment, Long>, JpaSpecificationExecutor<PostComment> {

	List<PostComment> findByPost(Post post);
	
	void deleteByPost(Post post);

}
