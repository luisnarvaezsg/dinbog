package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Traslation;

@Repository
public interface TraslationRepository extends JpaRepository<Traslation, String>, JpaSpecificationExecutor<Traslation> {
	
	public Traslation findByKeyAndIsoCode(String key, String isoCode);
	
	public List<Traslation> findByIsoCode(String isoCode);

}
