package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.dinbog.api.models.entity.ProfileCategoryType;


@Repository
public interface ProfileCategoryTypeRepository extends JpaRepository<ProfileCategoryType, Long>, JpaSpecificationExecutor<ProfileCategoryType> {
	
	 @Query("select c from ProfileCategoryType c inner join fetch c.profileType p where c.level=?1 and p.id=?2 order by c.value asc")
	 List<ProfileCategoryType> findByLevelWithProfileType(Long level, Long id);
	 
	 @Query("select c from ProfileCategoryType c where c.id=?1")
	 Optional<ProfileCategoryType> findById(Long id);
	 
}
