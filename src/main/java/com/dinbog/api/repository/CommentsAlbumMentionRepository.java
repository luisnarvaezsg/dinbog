package com.dinbog.api.repository;

import com.dinbog.api.models.entity.CommentsAlbum;
import com.dinbog.api.models.entity.CommentsAlbumMention;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CommentsAlbumMentionRepository extends JpaRepository<CommentsAlbumMention,Long>, JpaSpecificationExecutor<CommentsAlbumMention> {

    List<CommentsAlbumMention> findAllByCommentsAlbum(CommentsAlbum commentsAlbum);
}
