package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>, JpaSpecificationExecutor<City> {

	List<City> findByCountryIdOrderByStateName(Long countryId);

	@Modifying
	@Transactional
	@Query(value = "DELETE  FROM city WHERE country_id=:countryId AND id=:id", nativeQuery = true)
	void deleteByCountry(@Param("countryId") Long countryId, @Param("id") Long id);
}
