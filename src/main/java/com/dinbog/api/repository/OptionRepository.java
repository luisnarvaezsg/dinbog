package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Option;


@Repository
public interface OptionRepository extends JpaRepository<Option,Long>, JpaSpecificationExecutor<Option> {
	
	@Query(value = "SELECT * FROM option WHERE field_id=:fieldId", nativeQuery = true)
	List<Option> findByFieldId(@Param("fieldId") Long fieldId);
}
