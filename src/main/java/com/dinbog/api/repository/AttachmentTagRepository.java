package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface AttachmentTagRepository extends JpaRepository<AttachmentTag, Long>, JpaSpecificationExecutor<AttachmentTag> {

    Optional<List<AttachmentTag>> findAttachmentTagByProfileId(Long profileId);

    Optional<List<AttachmentTag>> findAttachmentTagByAttachment(Attachment attachment);
}
