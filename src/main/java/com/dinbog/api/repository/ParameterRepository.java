/**
 * 
 */
package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dinbog.api.models.entity.Parameter;
import com.google.common.base.Optional;

/**
 * @author Luis
 *
 */
public interface ParameterRepository extends JpaRepository<Parameter, Long>, JpaSpecificationExecutor<Parameter> {

	Optional<Parameter> findByKey(String key);
}
