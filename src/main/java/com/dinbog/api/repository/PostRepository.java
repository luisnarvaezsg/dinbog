package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.Profile;

@Repository
@Transactional
public interface PostRepository extends JpaRepository<Post, Long>, JpaSpecificationExecutor<Post> {

	Page<Post> findAllByProfile(@Param("profile") Profile profile, Pageable pageable);

	List<Post> findByProfile(Profile profile);

	Page<Post> findAll(Pageable pageable);
	
}
