package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.AlbumType;
import com.dinbog.api.models.entity.Profile;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long>, JpaSpecificationExecutor<Album> {
	
	List<Album> findByProfileAndAlbumType(Profile profile, AlbumType albumType);

}
