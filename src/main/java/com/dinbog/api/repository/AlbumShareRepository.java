package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dinbog.api.models.entity.AlbumShare;


public interface AlbumShareRepository extends JpaRepository<AlbumShare, Long>, JpaSpecificationExecutor<AlbumShare> {

}
