package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.GroupMenu;

@Repository
public interface GroupMenuRepository extends JpaRepository<GroupMenu, Long>, JpaSpecificationExecutor<GroupMenu> {

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM \"group_menu\" WHERE \"group_id\"=:groupId", nativeQuery = true)
	void deleteByGroup(@Param("groupId") Long groupId);
}
