package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostLike;
import com.dinbog.api.models.entity.Profile;

@Repository
@Transactional
public interface PostLikeRepository extends JpaRepository<PostLike, Long>, JpaSpecificationExecutor<PostLike> {

	/**
	@Query(value = "DELETE FROM post_like WHERE profile_id=:profileId and post_id=:id", nativeQuery = true)
	@Modifying
	void deleteByIdAndByProfileId(@Param("profileId") Long profileId, @Param("id") Long id);
**/
	List<PostLike> findByPost(Post post);
	
	List<PostLike> findByPostAndProfile(@Param("post")Post post, @Param("profile")Profile profile);
	
	void deleteByPost(Post post);
}
