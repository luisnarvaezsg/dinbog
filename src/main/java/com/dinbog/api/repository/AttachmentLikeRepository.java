package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.models.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.AttachmentLike;

@Repository
public interface AttachmentLikeRepository
		extends JpaRepository<AttachmentLike, Long>, JpaSpecificationExecutor<AttachmentLike> {

	/**
	 * @param ownerProfileId
	 * @param profileId
	 * @return
	 */
	@Query(value = "SELECT * FROM attachment_like WHERE profile_id=:profileId and attachment_id=:attachmentId", nativeQuery = true)
	Optional<List<AttachmentLike>> findByProfileAttachment(@Param("profileId") Long profileId, @Param("attachmentId") Long attachmentId);

	List<AttachmentLike> findAllByAttachment(Attachment attachment);
}
