/**
 * 
 */
package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Group;

/**
 * @author Luis
 *
 */
@Repository
public interface GroupRepository extends JpaRepository<Group, Long>, JpaSpecificationExecutor<Group> {

	/**
	 * @param email
	 * @return
	 */
	@Query(value = "SELECT * FROM \"group\" WHERE \"id\" IN(SELECT group_id FROM user_group WHERE user_id=:userId)", nativeQuery = true)
	Optional<List<Group>> findByUser(@Param("userId") Long userId);
}
