package com.dinbog.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.CommentAttachment;


@Repository
public interface CommentAttachmentRepository
		extends JpaRepository<CommentAttachment, Long>, JpaSpecificationExecutor<CommentAttachment> {

}
