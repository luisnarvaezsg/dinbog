package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileRecommendation;


@Repository
@Transactional
public interface ProfileRecommendationRepository extends JpaRepository<ProfileRecommendation, Long>, JpaSpecificationExecutor<ProfileRecommendation> {

    /**
     * @param id
     * @param profileId
     * @return
     */

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM profile_recommendation WHERE id=:id and profile_id=:profileId", nativeQuery = true)
    void deleteRecommendationByIdAndProfileId(@Param("id") Long id, @Param("profileId") Long profileId);
}

