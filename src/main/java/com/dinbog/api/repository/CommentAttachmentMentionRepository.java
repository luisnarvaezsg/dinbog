package com.dinbog.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.CommentAttachmentMention;


@Repository
public interface CommentAttachmentMentionRepository
		extends JpaRepository<CommentAttachmentMention, Long>, JpaSpecificationExecutor<CommentAttachmentMention> {

}
