package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.AlbumAttachment;

import java.util.List;

@Repository
public interface AlbumAttachmentRepository extends JpaRepository<AlbumAttachment, Long>, JpaSpecificationExecutor<AlbumAttachment> {
	
	@Query(value = "DELETE FROM album_attachment WHERE album_id=:albumId", nativeQuery = true)
	@Modifying
	void deleteByAlbumId(@Param("albumId") Long albumId);

	List<AlbumAttachment> findAllByAlbum(Album album);
}
