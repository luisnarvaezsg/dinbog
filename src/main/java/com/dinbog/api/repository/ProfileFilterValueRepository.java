package com.dinbog.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.CategoryFilter;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileFilterValue;

@Repository
public interface ProfileFilterValueRepository extends JpaRepository<ProfileFilterValue, Long>, JpaSpecificationExecutor<ProfileFilterValue> {
	
	Optional<ProfileFilterValue>  findByCategoryFilterAndProfileCategory(CategoryFilter categoryFilter, ProfileCategory profileCategory);
	
}
