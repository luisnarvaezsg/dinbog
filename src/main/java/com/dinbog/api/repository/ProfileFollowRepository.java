package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileFollow;

public interface ProfileFollowRepository extends JpaRepository<ProfileFollow, Long>, JpaSpecificationExecutor<ProfileFollow> {

	@Query(value = "SELECT owner_profile_id FROM profile_follow WHERE owner_profile_id=:profileId GROUP BY owner_profile_id", nativeQuery = true)
	Long findByOwnerIdGroupBy(@Param("profileId") Long profileId);
	
	Optional<ProfileFollow> findByOwnerProfileAndProfile(Profile ownerProfile, Profile profile);
	
	@Override
	default long count() {
		
		return 0;
	}
	
	Optional<List<ProfileFollow>> findByOwnerProfile(Profile ownerProfile, Pageable pageable);
	
	Optional<List<ProfileFollow>> findByProfile(Profile profile, Pageable pageable);
}
