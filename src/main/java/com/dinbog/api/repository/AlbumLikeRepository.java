package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.AlbumLike;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface AlbumLikeRepository extends JpaRepository<AlbumLike, Long>, JpaSpecificationExecutor<AlbumLike> {

    /**
     * @param id
     * @return
     */
    @Query(value = "DELETE FROM album_like WHERE album_id=:albumId AND id=:id", nativeQuery = true)
    @Modifying
    void deleteLikeByAlbumIdAndById(@Param("albumId") Long albumId, @Param("id") Long id);

    Optional<AlbumLike> findByAlbumAndProfile(Album album, Profile profile);

    List<AlbumLike> findAlbumLikesByAlbum_Id(Long id);

    List<AlbumLike> findAllByAlbum(Album album);
}
