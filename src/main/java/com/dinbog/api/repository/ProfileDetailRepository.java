package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileDetail;

@Repository
@Transactional
public interface ProfileDetailRepository
		extends JpaRepository<ProfileDetail, Long>, JpaSpecificationExecutor<ProfileDetail> {

	/**
	 * Trae los details de un Profile
	 * 
	 * @param profileId
	 * @return
	 */
	@Query(value = "SELECT * FROM profile_detail WHERE profile_id=:profileId", nativeQuery = true)
    Optional<List<ProfileDetail>> findByProfile(@Param("profileId") Long profileId);
}