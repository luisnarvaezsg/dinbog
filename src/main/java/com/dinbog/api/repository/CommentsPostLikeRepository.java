package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.CommentsPost;
import com.dinbog.api.models.entity.CommentsPostLike;
import com.dinbog.api.models.entity.Profile;

@Repository
public interface CommentsPostLikeRepository extends JpaRepository<CommentsPostLike, Long>, JpaSpecificationExecutor<CommentsPost> {

	List<CommentsPostLike> findByCommentsPost(CommentsPost comment);
	
	List<CommentsPostLike> findByCommentsPostAndProfile(CommentsPost comment, Profile profile);
	
	

}
