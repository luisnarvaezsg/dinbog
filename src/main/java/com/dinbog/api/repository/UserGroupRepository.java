/**
 * 
 */
package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.UserGroup;

/**
 * @author Luis
 *
 */
@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long>, JpaSpecificationExecutor<UserGroup> {

	/**
	 * @param userId
	 * @return
	 */
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM \"user_group\" WHERE \"user_id\"=:userId", nativeQuery = true)
	void deleteByUser(@Param("userId") Long userId);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM \"user_group\" WHERE \"group_id\"=:groupId", nativeQuery = true)
	void deleteByGroup(@Param("groupId") Long groupId);

	/**
	 * @param userId
	 * @return
	 */
	@Query(value = "SELECT * FROM \"user_group\" WHERE \"user_id\"=:userId", nativeQuery = true)
	Optional<List<UserGroup>> readByUser(@Param("userId") Long userId);
}
