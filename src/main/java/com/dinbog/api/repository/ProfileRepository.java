package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;

public interface ProfileRepository extends JpaRepository<Profile, Long>, JpaSpecificationExecutor<Profile> {

	/**
	 * 
	 * @param user
	 * @return  Optional<Profile>
	 */
	Optional<Profile> findByUser(@Param("user") User user);
	
	/**
	 * @param urlName
	 * @return
	 */
	//@Query(value = "SELECT count(*) FROM \"profile\" WHERE url_name=:urlName", nativeQuery = true)
	Long countProfileByUrlName(@Param("urlName") String urlName);

	/**
	 * @param urlName
	 * @return
	 */
	Profile findByUrlName(String urlName);

	List<Profile> findByUrlNameContaining(String urlName);

	/**
	 * @param profileId
	 * @return
	 */
	@Query(value = "SELECT * FROM \"profile\" WHERE \"id\" IN(SELECT profile_id FROM profile_view WHERE owner_profile_id=:profileId)", nativeQuery = true)
	Optional<List<Profile>> findByProfileView(@Param("profileId") Long profileId);
	
	Optional<List<Profile>> findByFullNameContainingIgnoreCase(String name);
	
	Page<Profile> findByFullNameContainingIgnoreCase(Pageable pageable, String name);

	@Query(
			"select p from Profile p " +
			"inner join p.user u left join fetch p.profileCountries pc " +
			"where u.status = 1 and  p.id <> :id " +
			"and lower(p.urlName) like %:fullName% and p.isSearchable = true " +
			"and p.id not in (select pf.profile.id from ProfileFollow pf where pf.ownerProfile.id = :id and pf.status = 1) " +
			"and p.id not in (select pf.ownerProfile.id from ProfileFollow pf where pf.profile.id = :id and pf.status = 1) " +
			"and p.id not in (select pb.profile.id from ProfileBlock pb where pb.ownerProfile.id = :id and pb.status = 1 ) " +
			"order by p.user.firstName asc, p.countFollowers desc, p.countViews desc, p.countLike desc"
	)
	Optional<List<Profile>> findAllSuggestedOthersProfilesActivatedByFullName(@Param("id") Long id, @Param("fullName") String fullName);


	@Query(
			"select p from Profile p " +
			"join p.user u join fetch p.profileFollows pf left join fetch p.profileCountries pc " +
			"where u.status = 1 and  p.id <> :id and pf.ownerProfile.id = :id and pf.status = 1 " +
			"and lower(p.urlName) like %:fullName% and p.isSearchable = true " +
			"and p.id not in (select pb.profile.id from ProfileBlock pb where pb.ownerProfile.id = :id and pb.status = 1 ) " +
			"order by p.user.firstName asc, p.countFollowers desc, p.countViews desc, p.countLike desc"
	)
	Optional<List<Profile>> findAllSuggestedFollowersProfilesActivatedByFullName(@Param("id") Long id, @Param("fullName") String fullName);
	
	@Query(
			"select pt.profile from ProfileTalent pt " +
			"join fetch pt.profile.profileCategories pc  " +		
			"where  pt.profile.id <> :id " +
			"and pt.profile.id not in (select pb.profile.id from ProfileBlock pb where pb.ownerProfile.id = :id ) " +
			"and pt.profile.id not in (select pf.profile.id from Profile pr join pr.profileFollows pf  where  pf.ownerProfile.id = :id and pf.status=1) " +
			"and pt.status=1 order by RANDOM()"
	)
	Optional<List<Profile>> findAllProfilesRandomNotInFollowers(@Param("id") Long id, Pageable pageable);

	Optional<Profile> findProfileByUrlName(String urlName);

}
