package com.dinbog.api.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.dinbog.api.models.entity.ProfileCompany;


public interface ProfileCompanyRepository extends JpaRepository<ProfileCompany, Long>, JpaSpecificationExecutor<ProfileCompany> {
	
	@Query("select p from ProfileCompany p inner join fetch p.profile f where f.id=?1")
	Optional<ProfileCompany> findByProfile(Long id);
	
	

}
