package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileBlock;

@Repository
@Transactional
public interface ProfileBlockRepository
		extends JpaRepository<ProfileBlock, Long>, JpaSpecificationExecutor<ProfileBlock> {

	/**
	 * Busca un block entre un profile y otro
	 * 
	 * @param ownerProfileId
	 * @param profileId
	 * @return
	 */
	@Query(value = "SELECT * FROM profile_block WHERE owner_profile_id=:ownerProfileId and profile_id=:profileId", nativeQuery = true)
    Optional<ProfileBlock> findBlockRelated(@Param("ownerProfileId") Long ownerProfileId,
                                            @Param("profileId") Long profileId);

	/**
	 * @param id
	 * @param profileId
	 * @return
	 */

	@Query(value = "DELETE FROM profile_block WHERE id=:id and profile_id=:profileId", nativeQuery = true)
	@Modifying
	void deleteProfileBlock(@Param("id") Long id, @Param("profileId") Long profileId);

	/**
	 * @param profileId
	 * @return
	 */
	@Query(value = "SELECT * FROM \"profile_block\" WHERE \"profile_id\"=:profileId", nativeQuery = true)
	Optional<List<ProfileBlock>> findByProfile(@Param("profileId") Long profileId);

	Optional<ProfileBlock> findByOwnerProfile_IdAndProfile_Id(Long ownerId, Long profileId);
}
