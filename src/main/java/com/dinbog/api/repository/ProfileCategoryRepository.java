package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCategoryType;

@Repository
public interface ProfileCategoryRepository extends JpaRepository<ProfileCategory, Long>, JpaSpecificationExecutor<ProfileCategory> {
	
	public List<ProfileCategory> findByProfileId(Long profileId);
	
	public Optional<ProfileCategory> findByProfile(Profile profile);
	
	public Optional<ProfileCategory> findByProfileAndProfileCategoryType(Profile profile, ProfileCategoryType profileCategoryType);
	
}
