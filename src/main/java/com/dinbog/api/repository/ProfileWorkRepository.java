package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileWork;

@Repository
public interface ProfileWorkRepository extends JpaRepository<ProfileWork,Long>, JpaSpecificationExecutor<ProfileWork> {
	
	List<ProfileWork> findByProfile(Profile profile);
}
