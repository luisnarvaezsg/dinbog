package com.dinbog.api.repository;

import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostMention;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PostMentionRepository extends JpaRepository<PostMention, Long>, JpaSpecificationExecutor<PostMention> {
	
	List<PostMention> findByPost(Post post);
	
	void deleteByPost(Post post);
	
}
