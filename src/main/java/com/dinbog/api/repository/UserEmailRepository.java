/**
 * 
 */
package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.UserEmail;

/**
 * @author Luis
 *
 */
public interface UserEmailRepository extends JpaRepository<UserEmail, Long>, JpaSpecificationExecutor<UserEmail> {

	/**
	 * @param email
	 * @return
	 */
	@Query("select u from UserEmail u where u.email=?1")
	Optional<UserEmail> findByEmail(String email);

	/**
	 * @param userId
	 * @return
	 */
	@Query(value = "SELECT * FROM \"user_email\" WHERE \"user_id\" =:userId ORDER BY \"level\"", nativeQuery = true)
	Optional<List<UserEmail>> findByUserId(@Param("userId") Long userId);

	/**
	 * @param userId
	 * @return
	 */
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM \"user_email\" WHERE \"user_id\"=:userId", nativeQuery = true)
	void deleteByUser(@Param("userId") Long userId);

	/**
	 * Lista especial para ajustar levels de emails de un user
	 * 
	 * @param id
	 * @param level
	 */
	@Query(value = "SELECT * FROM \"user_email\" WHERE \"user_id\"=:userId AND \"id\"<>:id ORDER BY \"level\"", nativeQuery = true)
	Optional<List<UserEmail>> listLevelEdit(@Param("userId") Long userId, @Param("id") Long id);
	
	
	Optional<UserEmail> findByToken(@Param("token") String token);
}
