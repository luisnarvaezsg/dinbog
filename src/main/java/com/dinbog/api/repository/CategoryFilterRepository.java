package com.dinbog.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.CategoryFilter;
import com.dinbog.api.models.entity.Filter;

@Repository
public interface CategoryFilterRepository extends JpaRepository<CategoryFilter, Long>, JpaSpecificationExecutor<CategoryFilter> {
	
	List<CategoryFilter> findByProfileCategoryTypeId(Long profileCategoryTypeId);
	
	 Optional<CategoryFilter> findByProfileCategoryTypeIdAndFilter(Long profileCategoryTypeId, Filter filter);
	
}
