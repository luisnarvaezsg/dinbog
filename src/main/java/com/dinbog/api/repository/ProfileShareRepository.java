package com.dinbog.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.ProfileShare;

@Repository
public interface ProfileShareRepository extends JpaRepository<ProfileShare, Long>, JpaSpecificationExecutor<ProfileShare> {

    /**
     * @param ownerProfileId
     * @param profileId
     * @return
     */
    @Query(value = "SELECT * FROM profile_share WHERE owner_profile_id=:ownerProfileId and profile_id=:profileId", nativeQuery = true)
    Optional<ProfileShare> findShareByOwnerProfileIdAndProfileId(@Param("ownerProfileId") Long ownerProfileId, @Param("profileId") Long profileId);

}
