package com.dinbog.api.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Message;
import com.dinbog.api.models.entity.Profile;

@Repository
@Transactional
public interface MessageRepository extends JpaRepository<Message, Long>, JpaSpecificationExecutor<Message> {

	public Page<Message> findAllByProfileSrc(Profile profileSrc, Pageable pageable);
	
	public Page<Message> findAllByProfileDest(Profile profileDest, Pageable pageable);
	
}
