package com.dinbog.api.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRecent;

public interface ProfileRecentRepository
		extends JpaRepository<ProfileRecent, Long>, JpaSpecificationExecutor<ProfileRecent> {

	
	/**
	 * @param  profile,  dateTagged
	 */
	@Modifying
	@Transactional
	// find the recents tagged
	Optional<List<ProfileRecent>> findByOwnerProfileAndCreatedAfter(Profile profile,Date dateTagged);

	
}
