package com.dinbog.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.ProfileRequestStatus;

@Repository
public interface ProfileRequestStatusRepository extends JpaRepository<ProfileRequestStatus, Long>, JpaSpecificationExecutor<ProfileRequestStatus> {
}
