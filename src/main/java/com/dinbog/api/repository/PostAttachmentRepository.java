package com.dinbog.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostAttachment;


@Repository
@Transactional
public interface PostAttachmentRepository extends JpaRepository<PostAttachment, Long>, JpaSpecificationExecutor<PostAttachment> {
	
	List<PostAttachment> findByPost(Post post);
	//@Modifying
	//@Query(value = "DELETE FROM post_attachment WHERE id IN (SELECT id FROM post_attachment WHERE post_id=:postId)", nativeQuery = true)
	void deleteByPostId(@Param("postId") Long id);
}
