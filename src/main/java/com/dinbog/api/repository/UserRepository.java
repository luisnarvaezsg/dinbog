/**
 *
 */
package com.dinbog.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.dinbog.api.models.entity.User;

import java.util.Optional;

/**
 * @author Luis
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    Optional<User>  findUserByIdAndPasswordNull(Long id);

}
