package com.dinbog.api.dto;

import java.util.Date;
import java.util.List;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class CommentsPostDTO {
	
	private Long id;

	private Long parentId;

	private Integer countLike;
	
	private Boolean liked;
	
	private Long profileId;
	
	private String avatar;
	
	private String fullName;
	
	private String urlName;
	
	private String prueba;

	private String value;
	
	private Date created;
	
	private List<ProfileBasicDTO> listaMention;

	private List<CommentsPostDTO> listaComment;
}
