package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Accessors(chain = true)
@NoArgsConstructor
public class AttachmentMultiFileDTO implements Serializable {

    private static final long serialVersionUID = 5324739639795352692L;

    private MultipartFile file;
    private List<TagDTO> tags;

    public AttachmentMultiFileDTO(MultipartFile file, List<TagDTO> tags) {
        this.file = file;
        this.tags = tags;
    }

    public AttachmentMultiFileDTO(MultipartFile file) {
        this.file = file;
    }

}
