package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileCategoryTypeRequestDTO {
	
	private Long parentId;
	
	private Long profileTypeId;
	
	private String value;
	
	private String valuePrt;
	
	private String valueSpa;
	
	private String valueFra;
	
	private String valueChn;
	
	private String valueRus;
	
	private String valueIta;
}
