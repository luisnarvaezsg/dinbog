/**
 * 
 */
package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class LogDTO {

	private long id;

	private long userId;

	private String message;

	private Boolean isError;

	private Boolean isWarning;

	private Date created;
}
