package com.dinbog.api.dto;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileCategoryTypeDTO {

	private Long id;

	private ProfileCategoryTypeDTO parent;

	private MutableList<ProfileCategoryTypeDTO> child;

	private ProfileTypeDTO profileType;

	private String value;
	
	private String key;
	
	private Long level;
}
