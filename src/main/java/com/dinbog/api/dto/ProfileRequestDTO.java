package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileRequestDTO {
	
	//Commons properties:
	
	private Long profileId;
	
	private Long profileTypeId;

	private Long userId;

	private String avatarPath;
	
	private String phoneNumber;
	
	private String[] categories;
	
	private String urlName;
	
	private Boolean isSearchable;

	private Date searchableSince;
	
	private Long membershipId;

	private Long registrationStep;
	
	private String coverPath;
	
	//Talent Profile properties:
	
	private String firstName;
	
	private String lastName;
		
	private Date birthDate;
	
	private Long genderId;

	private Long bornCountryId;

	private Long bornCityId;
	
	private Long residenceCountryId;

	private Long residenceCityId;
	
	//Company Profile properties:
	
	private String companyName;
	
	private String description;
	
	private Long headQuarterCountryId;
	
	private Long headQuarterCityId;
	
	private String[] branches;

}
