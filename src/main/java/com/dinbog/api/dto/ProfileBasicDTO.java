package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileBasicDTO {
	
	private Long profileId;

	private String urlName;

	private String fullName;

	private String avatarPath;
	
	private String coverPath;
	
	private String message;
}
