package com.dinbog.api.dto;

import java.util.Date;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AlbumCommentDTO {

	private Long id;
	
	private AlbumCommentBasicDTO parent;
	
	private MutableList<AlbumCommentBasicDTO> child;
	
	private AlbumDTO album;
	
	private ProfileSimpleDTO profile;
	
	private String value;
	
	private Date created;
}
