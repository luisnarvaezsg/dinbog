package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class TagDTO implements Serializable {

    private static final long serialVersionUID = -3648566903296267599L;

    private Long profileId;
    private Double posX;
    private Double posY;

    public TagDTO(Long profileId, Double posX, Double posY) {
        this.profileId = profileId;
        this.posX = posX;
        this.posY = posY;
    }
}
