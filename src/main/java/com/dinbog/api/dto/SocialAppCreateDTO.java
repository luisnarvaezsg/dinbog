package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class SocialAppCreateDTO {

	private String domain;

	private String name;

	private Boolean active;

	private String clientId;

	private String secret;

	private String key;
}
