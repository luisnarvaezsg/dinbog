package com.dinbog.api.dto;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Clase para: - Response para crear un usuario
 * 
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class UserCreateResponseDTO {

	private Long id;

	private String firstName;
	
	private String lastName;
	
	private String token;
	
	private String email;

	private String emailToken;
	
	private String urlAvatar;

	private UserStatusBasicDTO userStatus;

	private MutableList<GroupBasicDTO> groups;

	public UserCreateResponseDTO(Long id) {
		super();
		this.id = id;
	}
}
