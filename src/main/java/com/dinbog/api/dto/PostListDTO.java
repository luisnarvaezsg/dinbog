package com.dinbog.api.dto;

import java.util.Date;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class PostListDTO {

	private Long id;
	
	private String postURL;

	private String comment;

	private String countLike;

	private String countComment;

	private MutableList<AttachmentBasicDTO> attachments;

	private Date programedDateGmt;

	private Date created;

	private ProfileSimpleDTO profile;
}
