package com.dinbog.api.dto;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class SearchDTO {

	MutableList<PostHashtagDTO> post;

	MutableList<ProfileSimpleDTO> profile;

	MutableList<EventDTO> event;

	private Long totalElements;

	private Integer totalPages;

	private Integer pageNumber;

	private Integer pageSize;
}
