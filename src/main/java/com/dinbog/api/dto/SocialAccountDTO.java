/**
 * 
 */
package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class SocialAccountDTO {

	private Long id;
	private String data;
	private SocialAppBasicDTO socialApp;
	private Date lastLogin;
	private Date created;
}
