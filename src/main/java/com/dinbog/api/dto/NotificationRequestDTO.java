package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class NotificationRequestDTO {

	private Long ownerUserId;

	private Long toUserId;

	private Boolean isRead;

	private Long eventNotificationId;

	private Long notificationActionId;

	private Long profileNotificationId;

	private Long postNotificationId;

	private Long attachmentNotificationId;

	private Long pollNotificationId;

	private Long albumNotificationId;

	private Long userNotificationId;

	private Long conversationNotificationId;

	private Date dateRead;

	private Date dateNotification;

	private String codeMessage;
}
