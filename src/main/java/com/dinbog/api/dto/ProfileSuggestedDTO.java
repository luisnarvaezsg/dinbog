package com.dinbog.api.dto;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCountry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class ProfileSuggestedDTO implements Serializable {

    private static final long serialVersionUID = -2455307240658020606L;

    private Long profileId;
    private String fullName;
    private String mention;
    private String avatar;
    private String country;
    private Boolean isFollower;



    public ProfileSuggestedDTO(Profile profile) {
        this.profileId = profile.getId();
        this.fullName = profile.getUser().getFirstName() + " " + profile.getUser().getLastName();
        this.mention = getMentionName();
        this.avatar =  profile.getAvatarPath();
        this.isFollower = getFollowerStatus(profile);
        this.country = getCountry(profile.getProfileCountries());
    }

    private String getMentionName(){
        return "@{{" + this.getProfileId() +"}}";
    }

    private String getCountry(Set<ProfileCountry> countryList){
        Optional<ProfileCountry> cn = countryList.stream().filter(c -> c.getProfileCountryType().getId() == 2).findAny();
        return cn.map(profileCountry -> profileCountry.getCountry().getName()).orElse(null);
    }

    private Boolean getFollowerStatus(Profile profile){
        return !profile.getProfileFollows().isEmpty();
    }
}
