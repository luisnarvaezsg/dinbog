package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileStatisticsDTO {

	private Integer countLike;

	private Integer countFollow;

	private Integer countFollowers;

	private Integer countConections;

	private Integer countPost;
	
	private Integer countBooks;
	
	private Integer countVideos;
	
	private Integer countAlbum;
	
	private Integer countWorks;
	
}
