package com.dinbog.api.dto;

import java.util.List;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Clase para:
 * 
 * 1- Request para actualizar un usuario (update)
 * 
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class UserEditDTO {

	private Long userStatusId;

	private List<GroupBasicDTO> groups;
}
