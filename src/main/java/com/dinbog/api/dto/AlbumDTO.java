package com.dinbog.api.dto;

import java.util.ArrayList;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AlbumDTO {

	private Long id;

	private String name;

	private String description;

	private Boolean isPrivate;

	private ProfileSimpleDTO profile;

	private AlbumTypeDTO albumType;

	private Date created;
	
	private ArrayList<AlbumAttachmentDTO> albumAttachments;
}
