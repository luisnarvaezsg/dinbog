package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileFilterValueDTO {
	
	private Long id;
	
	private Long categoryId;
	
	private Long filterId;
	
	private Long profileCategoryId;
	
	private Long categoryFilterId;
	
	private String description;
	
	private String value;
}
