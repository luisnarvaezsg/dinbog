package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AttachmentDTO {

	private Long id;

	private String path;

	private String metadata;

	private String extra;

	private Boolean purged;

	private Date datePurged;

	private Date created;

	private Long countAttachment;

	private AttachmentTypeDTO attachmentType;
}
