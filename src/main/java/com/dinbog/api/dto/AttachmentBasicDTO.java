package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AttachmentBasicDTO {

	private Long id;

	private String path;

	private String metadata;

	private String extra;

	private Boolean purged;

	private Long countLikes;

	private Long countComments;

	private AttachmentTypeDTO attachmentType;
}
