package com.dinbog.api.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileFilterDTO {

	private String firstName;

	private String lastName;

	private List<Long> countries;

	private List<Long> gender;

	private Long profileTypeId;

	private Long profileCategoryTypeId;

	private Date created;
}
