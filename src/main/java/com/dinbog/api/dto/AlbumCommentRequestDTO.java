package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AlbumCommentRequestDTO {

	private Long parentId;

	private Long albumId;

	private Long profileId;

	private String value;

}
