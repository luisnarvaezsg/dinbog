package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AlbumRequestDTO {

	private String name;

	private String description;

	private Boolean isPrivate;

	private Long profileId;

	private Long albumTypeId;
}
