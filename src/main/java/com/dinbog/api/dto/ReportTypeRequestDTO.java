package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ReportTypeRequestDTO {

	private String value;

	private String valueSpa;

	private String valueFra;

	private String valueIta;

	private String valuePor;

	private String valueRus;

	private String valueChi;
}
