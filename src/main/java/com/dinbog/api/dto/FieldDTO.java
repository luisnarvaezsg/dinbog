package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class FieldDTO {

	private Long id;

	private String code;

	private String name;

	public FieldDTO(Long id) {
		super();
		this.id = id;
	}
}
