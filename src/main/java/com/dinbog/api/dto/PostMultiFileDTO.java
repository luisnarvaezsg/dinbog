package com.dinbog.api.dto;

import com.dinbog.api.exception.InvalidRequestException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Accessors(chain = true)
public class PostMultiFileDTO implements Serializable {

    private static final long serialVersionUID = 6286946350157034683L;

    private String language;
    private String comment;
    private List<AttachmentMultiFileDTO> attachments;
    private List<MultipartFile> files;
    private String tags;
    private List<TagsFileDTO> listTags;

    public PostMultiFileDTO() {
        this.listTags = new ArrayList<>();
        this.attachments =  new ArrayList<>();
    }

    private void getTags() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            this.listTags = mapper.readValue(this.tags, new TypeReference<>() {});
        }catch (JsonProcessingException e){
            throw new InvalidRequestException( "JSON NO VALIDO " + e.getMessage() );
        }
    }

    public void doAttachment(){
        if(!this.tags.isEmpty()) getTags();
        int index = 0;
        for(MultipartFile file : files){
            if(!file.isEmpty()){
                AttachmentMultiFileDTO fileAttached =  getAttachment(index);
                this.attachments.add(fileAttached);
            }
            index++;
        }
    }

    private AttachmentMultiFileDTO getAttachment(int index){
        if( gotTags(index) ) {
            Optional<TagsFileDTO> tagsMap = this.listTags.stream().filter(t -> t.getOwnerId() == index).findFirst();
            return tagsMap.map(tagsFile -> new AttachmentMultiFileDTO(files.get(index), tagsFile.getTags())).orElseGet(() -> new AttachmentMultiFileDTO(files.get(index)));
        }else{
            return new AttachmentMultiFileDTO(files.get(index));
        }
    }

    private boolean gotTags(int index){
        return this.listTags.stream().anyMatch( tagsFile -> tagsFile.getOwnerId() == index );
    }
}
