package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileSimpleDTO {
	
	private Long id;

	private String urlName;

	private String firstName;

	private String lastName;
	
	private String fullName;

	private String gender;

	private ProfileTypeDTO profileType;

	private Integer countLike;

	private Integer countFollow;

	private Integer countFollowers;

	private Integer countConections;

	private String avatarPath;
	
	private String categories;
	
	private String profesion;
	
	private String residenceCountry;
	
	private String residenceCountryISOCode;
	
	private String residenceCity;
	
	private Boolean followed;
	
	private Boolean connected;
	
	private Boolean blocked;
	
	private Boolean liked;
}
