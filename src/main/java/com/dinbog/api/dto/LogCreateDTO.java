/**
 * 
 */
package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class LogCreateDTO {

	private long userId;

	private String message;

	private Boolean isError;

	private Boolean isWarning;
}
