package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginResponseDTO {

	private String token;

	private Long userId;

	private Long profileId;
	
	private String profileUrl;
	
	private String avatarPath;
	
	private Long profileTypeId;
}
