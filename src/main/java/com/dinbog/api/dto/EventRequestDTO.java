package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class EventRequestDTO {

	private Long eventTypeId;

	private Long eventStatusId;

	private Long countryId;

	private Long cityId;

	private String coverAttachment;

	private String address;

	private String nameEvent;

	private String description;

	private Date publishDate;

	private Date startDateEvent;

	private Date endDateEvent;

	private Date announcementDateEvent;

	private Integer countParticipant;

	private Float positionX;

	private Float positionY;

	private Integer ageFrom;

	private Integer ageTo;

	private Integer participantPrivate;
	
	private String[] categories;
}
