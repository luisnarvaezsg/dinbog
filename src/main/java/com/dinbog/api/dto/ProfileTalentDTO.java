package com.dinbog.api.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileTalentDTO extends ProfileDTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	
	private String lastName;

	private Date birthDate;
	
	private GenderDTO gender;

	private CountryDTO bornCountry;

	private CityDTO bornCity;
	
	private CountryDTO residenceCountry;

	private CityDTO residenceCity;
}
