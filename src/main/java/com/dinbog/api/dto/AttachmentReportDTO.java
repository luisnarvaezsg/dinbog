/**
 * 
 */
package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AttachmentReportDTO {

	private Long id;
	private ReportTypeDTO reportType;
	private String detail;
	private ProfileSimpleDTO owner;
	private AttachmentDTO attachment;
}
