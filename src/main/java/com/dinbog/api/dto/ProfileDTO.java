package com.dinbog.api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileDTO {

	private Long id;

	private UserDTO user;

	private String urlName;

	private String fullName;

	private ProfileTypeDTO profileType;

	private Boolean isSearchable;

	private Date searchableSince;

	private MembershipDTO membership;

	private Integer countLike;

	private Integer countFollow;

	private Integer countFollowers;

	private Integer countConections;

	private Integer countPost;

	private Integer countViews;

	private Long registrationStep;

	private String avatarPath;

	private String coverPath;

	private Date created;
	
	private String phoneNumber;
	
	private List<ProfileCategoryTypeDTO> categories = new ArrayList<>();
}
