package com.dinbog.api.dto;

import java.util.Comparator;

public class SortCountryByName implements Comparator<CountryDTO> {
	
    @Override
	public int compare(CountryDTO o1, CountryDTO o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
	} 

}
