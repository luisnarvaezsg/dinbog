package com.dinbog.api.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Clase para Response para consultas de usuario (readAll, readByEmail,
 * readById)
 * 
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class UserDTO {

	private Long id;

	private UserStatusBasicDTO userStatus;

	private Date lastLogin;

	private Date created;

	private List<UserEmailDTO> emails;

	private List<GroupBasicDTO> groups;

	public UserDTO(Long id) {
		super();
		this.id = id;
	}
}
