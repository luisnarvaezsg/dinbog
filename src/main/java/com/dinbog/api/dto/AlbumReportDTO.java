package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AlbumReportDTO {

    private Long id;

    private AlbumDTO album;

    private ProfileDTO profile;

    private ReportTypeDTO reportType;

    private String detail;

}

