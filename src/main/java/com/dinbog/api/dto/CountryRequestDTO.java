package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class CountryRequestDTO {

	private String name;

	private String isoCode;

	private String unitMeasurement;

	private String wearMeasurement;

	private Boolean isOnu;

	private Date created;

	private Long languageId;
}
