package com.dinbog.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Clase para:
 * - Request para crear un usuario
 * 
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class UserCreateDTO {

	private String email;
	
	private String password;

	private Long userStatusId;
	
	private String firstName;
	
	private String lastName;
	
	private Long profileTypeId;

	private List<GroupBasicDTO> groups;
}
