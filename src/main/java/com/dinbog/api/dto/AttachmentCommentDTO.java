package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AttachmentCommentDTO {
	
	private Long attachmentId;

	private Long profileId;

	private Long parentId;

	private String value;
}
