package com.dinbog.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlbumShareCreateDTO {

	private Long albumId;

	private Long profileId;
}
