package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileRequestConnectionDTO {
	private Long id;
	
	private ProfileBasicDTO owner;
	
	private ProfileBasicDTO profile;
	
	private ConnectionTypeDTO typeConnection;
	
	private ProfileRequestStatusDTO profileRequestStatus;
	
	private Date created;
}
