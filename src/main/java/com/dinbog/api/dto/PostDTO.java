package com.dinbog.api.dto;

import java.util.Date;

import org.eclipse.collections.api.list.MutableList;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class PostDTO {

	private Long id;

	private String content;

	private Long programed;

	private Integer countLike;

	private Integer countComment;
	
	private Boolean liked;
	
	private Boolean mine;

	private MutableList<AttachmentBasicDTO> attachments;

	private Date programedDateGmt;

	private Date created;
	
	private MutableList<CommentsPostDTO> comments;

	private ProfileSimpleDTO profile;
}
