package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class PostEditDTO {
	
	private String content;
	
	private Long programed;
	
	private String countLike;
	
	private String countComment;
	
	private Date programedDateGmt;
	
	private Long profileId;
}
