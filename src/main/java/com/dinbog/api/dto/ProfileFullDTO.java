package com.dinbog.api.dto;

import java.util.ArrayList;
import java.util.Date;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileFullDTO {

	private Long id;

	private UserDTO user;

	private String urlName;

	private String fullName;

	private ProfileTypeDTO profileType;

	private Boolean isSearchable;

	private Date searchableSince;

	private MembershipDTO membership;

	private Integer countLike;

	private Integer countFollow;

	private Integer countFollowers;

	private Integer countConections;

	private Integer countPost;

	private Integer countViews;

	private Long registrationStep;

	private String avatarPath;

	private MutableList<ProfileDetailBasicDTO> profileDetails;

	private Date created;
	
	private CountryDTO residenceCountry;
	
	private CityDTO residenceCity;
	
	private CountryDTO bornCountry;
	
	private CityDTO bornCity;
	
	private ArrayList<ProfileCategoryDTO> profileCategories;
	
	private Boolean followed;
	
	private Boolean connected;
	
	private Boolean blocked;
	
	private Boolean liked;
}
