package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileNotificationDTO {

	private Long id;

	private Long objectId;

	private ProfileBasicDTO ownerProfile;

	private ProfileBasicDTO toProfile;

	private String message;

	private Date created;
}
