package com.dinbog.api.dto;

import java.util.Date;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileShareDTO {

    private Long id;

    private ProfileSimpleDTO ownerProfile;

    private ProfileSimpleDTO profile;

    private Date created;
}
