package com.dinbog.api.dto;

import java.util.ArrayList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileCategoryDTO {
	
	private Long id;
	
	private ProfileDTO profile;

	private ProfileCategoryTypeDTO profileCategoryType;
	
	ArrayList<ProfileFilterValueDTO> profileFiltersValues;

}
