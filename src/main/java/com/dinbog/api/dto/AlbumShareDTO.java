package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlbumShareDTO {

	private Long id;

	private AlbumDTO album;

	private ProfileDTO profile;

	private Date created;
}
