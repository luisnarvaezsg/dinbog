package com.dinbog.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileCompanyDTO extends ProfileDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String companyName;
	
	private String description;
	
	private CountryDTO headQuarterCountry;
	
	private CityDTO headQuarterCity;
	
	private List<CountryDTO> branches = new ArrayList<>();

}
