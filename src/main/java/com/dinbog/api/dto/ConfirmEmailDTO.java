/**
 * 
 */
package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class ConfirmEmailDTO {

	private Long userId;
	private UserEmailDTO email;

	/**
	 * @param userId
	 * @param email
	 */
	public ConfirmEmailDTO(Long userId, UserEmailDTO email) {
		this.userId = userId;
		this.email = email;
	}
}
