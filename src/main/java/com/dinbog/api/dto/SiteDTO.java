package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class SiteDTO {

    private Long id;

    private String domain;

    private String name;

    private Boolean active;

    private Date created;

    private Date updated;

}
