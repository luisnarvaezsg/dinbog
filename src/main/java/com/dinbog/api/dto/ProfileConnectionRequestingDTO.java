package com.dinbog.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileConnectionRequestingDTO {

    private Long profileId;

    private Long typeConnectionId;

}
