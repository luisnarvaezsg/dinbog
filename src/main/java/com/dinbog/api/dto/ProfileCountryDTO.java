package com.dinbog.api.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileCountryDTO {

	private Long id;

	private CountryDTO country;

	private CityDTO city;

	private ProfileCountryTypeDTO profileCountryType;

}
