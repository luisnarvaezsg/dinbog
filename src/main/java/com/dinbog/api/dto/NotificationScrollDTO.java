
package com.dinbog.api.dto;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class NotificationScrollDTO {

	private Long id;

	private UserDTO ownerUser;

	private UserDTO toUser;

	private Integer isRead;

	private NotificationActionDTO notificationAction;

	private ProfileNotificationDTO notificationDetail;

	private Date dateRead;

	private Date dateNotification;

	private String codeMessage;
}
