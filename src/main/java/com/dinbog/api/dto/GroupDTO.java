/**
 * 
 */
package com.dinbog.api.dto;

import org.eclipse.collections.api.list.MutableList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class GroupDTO {

	private Long id;
	private String name;
	private MutableList<MenuDTO> menus;
}
