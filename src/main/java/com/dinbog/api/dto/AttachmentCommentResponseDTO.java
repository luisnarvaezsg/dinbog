package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class AttachmentCommentResponseDTO {

	private Long id;

	private Long parentId;

	private Long profileId;

	private Long attachmentId;

	private String value;

	private Date created;
}
