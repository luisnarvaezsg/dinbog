package com.dinbog.api.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class TagsFileDTO implements Serializable {

    private static final long serialVersionUID = -3024688323266438637L;

    private Long ownerId;
    private List<TagDTO> tags;
}
