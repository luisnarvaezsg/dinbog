/**
 * 
 */
package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class UserEmailDTO {

	private Long id;

	private Integer level;

	private String email;

	private Boolean isConfirmed;

	private Date created;
}
