package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class PostReportDTO {

	private Long id;

	private PostBasicDTO post;

	private ReportTypeDTO reportType;

	private String detail;

	private ProfileDTO owner;

	private Date created;
}
