package com.dinbog.api.dto;

import java.util.Date;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileRecommendationDTO {

    private Long id;

    private ProfileSimpleDTO profile;

    private ProfileSimpleDTO ownerProfile;

    private Boolean isPublic;

    private String value;

    private Date created;
}
