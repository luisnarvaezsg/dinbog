package com.dinbog.api.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class FilterDTO {
	
	private Long id;
	
	private FilterTypeDTO filterType;
	
	private String name;

	private String description;
	
	private String validator;

	private Date created;

	private FilterDTO parent;

	private List<FilterValueDTO> filtersValues;
	
	private Long status;
}
