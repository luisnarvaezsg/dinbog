package com.dinbog.api.dto;

import java.util.Comparator;

public class SortCityByName implements Comparator<CityDTO> {
	
    @Override
	public int compare(CityDTO o1, CityDTO o2) {
        return o1.getStateName().compareToIgnoreCase(o2.getStateName());
	} 

}
