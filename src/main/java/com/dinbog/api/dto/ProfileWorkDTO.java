package com.dinbog.api.dto;

import java.util.Date;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileWorkDTO {

    private Long id;
    
    private Long profileId;
    
    private String client;
    
    private String description;
    
    private Long yearJob;
    
    private ProfileSimpleDTO profile;

    private Date created;
}
