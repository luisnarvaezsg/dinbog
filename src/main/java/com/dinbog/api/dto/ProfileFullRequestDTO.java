package com.dinbog.api.dto;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class ProfileFullRequestDTO {

	private String urlName;

	private String firstName;

	private String lastName;

	private Long genderId;

	private Long profileTypeId;

	private Boolean isSearchable;

	private Date searchableSince;

	private Long membershipId;

	private Integer countLike;

	private Integer countFollow;

	private Integer countFollowers;

	private Integer countConections;

	private Integer countPost;

	private Integer countViews;

	private Long registrationStep;

	private String avatarPath;

	private String coverPath;
}
