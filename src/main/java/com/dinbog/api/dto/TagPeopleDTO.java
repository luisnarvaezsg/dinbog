package com.dinbog.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Clase para Response de TagPeople
 * 
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class TagPeopleDTO {

	private List<UserCreateResponseDTO> amigosRecientes;

	private List<UserCreateResponseDTO> amigosSigo;
	
	public TagPeopleDTO(List<UserCreateResponseDTO> amigosRecientes, List<UserCreateResponseDTO> amigosSigo) {
		this.amigosRecientes=amigosRecientes;
		this.amigosSigo=amigosSigo;
	}
	
}
