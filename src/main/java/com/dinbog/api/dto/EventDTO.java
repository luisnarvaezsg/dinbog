package com.dinbog.api.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class EventDTO {

	private Long id;

	private EventTypeDTO eventType;

	private EventStatusDTO eventStatus;

	private ProfileBasicDTO owner;

	private ProfileTypeDTO profileType;

	private CountryDTO country;

	private CityDTO city;

	//private AttachmentBasicDTO coverAttachment;
	private String coverAttachment;

	private String address;

	private String nameEvent;

	private String description;

	private Date publishDate;

	private Date startDateEvent;

	private Date endDateEvent;

	private Date announcementDateEvent;

	private Integer countParticipant;

	private Float positionX;

	private Float positionY;

	private Integer ageFrom;

	private Integer ageTo;

	private Integer participantPrivate;

	private Date created;

	private Date updated;
	
	private List<ProfileCategoryTypeDTO> categories;
}
