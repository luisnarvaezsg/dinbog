package com.dinbog.api.notification;

public enum Notification {
	UNREAD(0), SEND(1), READ(2);

	private final Integer code;

	Notification(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
}
