package com.dinbog.api.notification;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dinbog.api.firebase.FCMService;
import com.dinbog.api.firebase.PushNotificationRequest;
import com.dinbog.api.service.INotificationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NotificationScheduler {

	@Autowired
	FCMService fcmService;

	@Autowired
	INotificationService notificationService;

	@Value("#{${app.notifications.defaults}}")
	private Map<String, String> defaults;

	/**
	 * @Description: Este método es para prueba de concepto
	 */
//	@Scheduled(fixedDelay=40000, initialDelay=30000)
	public void sendSamplePushNotification() {
		try {
			// esta funcion envia notificaciones a la app
			fcmService.sendMessageToToken(getSamplePushNotificationRequest());
		} catch (InterruptedException | ExecutionException e) {
			log.warn("Interrupted!", e);
			Thread.currentThread().interrupt();
		}
	}

	// @Scheduled(fixedDelay=40000, initialDelay=30000)
	public void sendSamplePushNotification2() {
		try {
			log.info("-------eject task scheduler ------ " + new Date());
			fcmService.sendMessageToToken(getSamplePushNotificationRequest());
		} catch (InterruptedException | ExecutionException e) {
			log.warn("Interrupted!", e);
			Thread.currentThread().interrupt();
		}
	}

	//@Scheduled(fixedDelay = 4000, initialDelay = 3000)
	public void sendPushNotificationPrueba() {
		log.info("------probando: +++");
		notificationService.sendPushNotificationPrueba();
	}

	/**
	 * @return
	 */
	private PushNotificationRequest getSamplePushNotificationRequest() {
		PushNotificationRequest request = new PushNotificationRequest();
		request.setToken(defaults.get("token"));
		request.setTitle(defaults.get("title"));
		request.setTopic(defaults.get("topic"));
		request.setMessage(defaults.get("message"));
		return request;
	}
}
