package com.dinbog.api.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "post")
@SQLDelete(sql = "UPDATE dinbog.post SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class Post {

	public Post() {
		this.countLike = 0;
		this.countComment = 0;
		this.listaPostAttachment = new ArrayList<>();
		this.listaPostComment = new ArrayList<>();
	}

	@Id
	@GeneratedValue(generator = "post_id_generator")
	@SequenceGenerator(name = "post_id_generator", sequenceName = "post_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "content")
	private String content;

	@Column(name = "programed")
	private Long programed;

	@Column(name = "count_like")
	private Integer countLike;

	@Column(name = "count_comment")
	private Integer countComment;

	@Column(name = "programed_date_gmt", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date programedDateGmt;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PostAttachment> listaPostAttachment;

	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PostComment> listaPostComment;

	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PostHashtag> listaPostHashtag;
	
	@OneToMany(mappedBy = "profile", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProfileFollow> profileFollows;

	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PostMention> postMentionList;
}
