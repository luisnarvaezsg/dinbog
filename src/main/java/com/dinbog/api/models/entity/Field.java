package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "field")
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class Field {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "field_id_generator")
	@SequenceGenerator(name = "field_id_generator", sequenceName = "field_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "code", length = 100, nullable = false)
	private String code;

	@Column(name = "name", length = 255, nullable = false)
	private String name;
}
