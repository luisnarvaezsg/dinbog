package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.NoArgsConstructor;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "post_hashtag")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@SQLDelete(sql = "UPDATE dinbog.post_hashtag SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class PostHashtag {

	@Id
	@GeneratedValue(generator = "post_hashtag_id_generator")
	@SequenceGenerator(name = "post_hashtag_id_generator", sequenceName = "post_hashtag_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "post_id")
	private Post post;

	@Column(name = "name_hastag")
	private String nameHasTag;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	public PostHashtag(Post post, String nameHasTag, Date created) {
		this.post = post;
		this.nameHasTag = nameHasTag;
		this.created = created;
	}
}
