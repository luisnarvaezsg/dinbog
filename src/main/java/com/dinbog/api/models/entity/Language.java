package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "language")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Language {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "languageIdGenerator")
	@SequenceGenerator(name = "languageIdGenerator", sequenceName = "language_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "iso_code_language")
	private String isoCodeLanguage;
}
