package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "post_like")
@SQLDelete(sql = "UPDATE dinbog.post_like SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class PostLike {
	@Id
	@GeneratedValue(generator = "post_like_id_generator")
	@SequenceGenerator(name = "post_like_id_generator", sequenceName = "post_like_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id")
	private Post post;
	
	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	

}
