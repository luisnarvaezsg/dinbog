/**
 * 
 */
package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "menu")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Menu {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "menuIdGenerator")
	@SequenceGenerator(name = "menuIdGenerator", sequenceName = "menu_id_seq", allocationSize = 1)
	private Long id;

	// DECIDIR POR UN SOLO NOMBRE PARA TODOS LOS CAMPOS EN LA BASE DE DATOS Y
	// MODELOS QUE CONTENGAN UN LINK (URL, ROUTE, PATH, LINK, ETC)
	@Column(name = "route", nullable = false)
	private String route;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "is_available", nullable = false)
	private Boolean isAvaliable;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
