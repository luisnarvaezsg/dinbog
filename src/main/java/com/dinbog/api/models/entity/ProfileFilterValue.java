package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "profile_filters_values")
@SQLDelete(sql = "UPDATE dinbog.profile_filters_values SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class ProfileFilterValue {

	@Id
	@GeneratedValue(generator = "profile_filters_values_id_generator")
	@SequenceGenerator(name = "profile_filters_values_id_generator", sequenceName = "profile_filters_values_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "description")
	private String description;

	@Column(name = "value")
	private String value;
	
	@ManyToOne
	@JoinColumn(name = "profile_category_id")
	private ProfileCategory profileCategory;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_filter_id")
	private CategoryFilter categoryFilter;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "status")
	private Long status;
}
