package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "notification_message")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class NotificationMessage {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "notification_message_id_generator")
	@SequenceGenerator(name = "notification_message_id_generator", sequenceName = "notification_message_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "notification_messages_type")
	private NotificationMessageType notificationMessageType;

	@Column(name = "code_message")
	private String codeMessage;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "value_spa", nullable = true)
	private String valueSpa;

	@Column(name = "value_fra", nullable = true)
	private String valueFra;

	@Column(name = "value_ita", nullable = true)
	private String valueIta;

	@Column(name = "value_por", nullable = true)
	private String valuePor;

	@Column(name = "value_rus", nullable = true)
	private String valueRus;

	@Column(name = "value_chi", nullable = true)
	private String valueChi;
}
