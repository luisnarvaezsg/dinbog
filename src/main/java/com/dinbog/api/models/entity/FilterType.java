package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "filters_type")
@SQLDelete(sql = "UPDATE dinbog.filters_type SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class FilterType {

	@Id
	@GeneratedValue(generator = "filters_type_id_generator")
	@SequenceGenerator(name = "filters_type_id_generator", sequenceName = "filters_type_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "status")
	private Long status;
}
