/**
 * 
 */
package com.dinbog.api.models.entity;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "profile")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Profile implements Serializable {

		/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profileIdGenerator")
	@SequenceGenerator(name = "profileIdGenerator", sequenceName = "profile_id_seq", allocationSize = 1)
	private Long id;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Column(name = "url_name")
	private String urlName;
	
	@Column(name = "full_name")
	private String fullName;

	@Column(name = "phone_number")
	private String phoneNumber;
	
	@OneToOne
	@JoinColumn(name = "profile_type_id")
	private ProfileType profileType;

	@Column(name = "is_searchable")
	private Boolean isSearchable;

	@Column(name = "searchable_since")
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date searchableSince;
	
	@OneToOne
	@JoinColumn(name = "membership_id")
	private Membership membership;

	@Column(name = "count_like")
	private Integer countLike;

	@Column(name = "count_follow")
	private Integer countFollow;

	@Column(name = "count_followers")
	private Integer countFollowers;

	@Column(name = "count_conections")
	private Integer countConections;

	@Column(name = "count_post")
	private Integer countPost;

	@Column(name = "count_views")
	private Integer countViews;

	@Column(name = "registration_step")
	private Long registrationStep;

	@Column(name = "avatar_path")
	private String avatarPath;

	@Column(name = "cover_path")
	private String coverPath;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "is_whitelist")
	private Boolean isWhitelist;

	@Column(name = "token_firebase")
	private String tokenFireBase;
	
	@OneToMany(mappedBy = "profile" , fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProfileCategory> profileCategories;

	@OneToMany(mappedBy = "profile",fetch = FetchType.EAGER)
	private Set<ProfileFollow> profileFollows;

	@OneToMany(mappedBy = "profile",fetch = FetchType.EAGER)
	private Set<ProfileCountry> profileCountries;


	public Profile() {
		profileCategories = new ArrayList<>();
	}

	public void addProfileCategories(ProfileCategory profileCategory) {
		this.profileCategories.add(profileCategory);
	}
	
	
	public void removeProfileCategories(ProfileCategory profileCategory) {
		
		if(this.profileCategories != null){
			
			profileCategories.remove(profileCategory);
			profileCategory.setProfile(null);
	       
		}
	}
	
	public void addLike() {
		this.setCountLike(getCountLike() + 1);
	}
	
	public void unLike() {
		this.setCountLike(getCountLike() - 1);
	}
	
	public void addFollow() {
		this.setCountFollow(getCountFollow() + 1);
	}
	
	public void unFollow() {
		this.setCountFollow(getCountFollow() - 1);
	}
	
	public void addFollower() {
		this.setCountFollowers(getCountFollowers() + 1);
	}
	
	public void unFollower() {
		this.setCountFollowers(getCountFollowers() - 1);
	}
	
}
