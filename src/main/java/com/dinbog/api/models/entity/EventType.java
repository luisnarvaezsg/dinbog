package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "event_type")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class EventType {
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "eventTypeIdGenerator")
	@SequenceGenerator(name = "eventTypeIdGenerator", sequenceName = "event_type_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "key", nullable = false)
	private String key;

	}
