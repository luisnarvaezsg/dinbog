/**
 * 
 */
package com.dinbog.api.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "profile_company")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class ProfileCompany implements Serializable {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profileCompanyIdGenerator")
	@SequenceGenerator(name = "profileCompanyIdGenerator", sequenceName = "profile_company_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "description")
	private String description;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "profile_id")
	private Profile profile;
	
	@OneToMany(mappedBy = "profile" , fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProfileCountry> profileCountries;
	
	public ProfileCompany() {

		profileCountries = new ArrayList<>();
	}
	
	public void addProfileCountries(ProfileCountry profileCountry) {
		this.profileCountries.add(profileCountry);
	}
	
	
	public void removeProfileCountries(ProfileCountry profileCountry) {
		
		if(this.profileCountries != null){
			
			profileCountries.remove(profileCountry);
			profileCountry.setProfile(null);
	       
		}
	}
}
