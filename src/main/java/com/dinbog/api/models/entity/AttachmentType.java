package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "attachment_type")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class AttachmentType {
	@Id
	@GeneratedValue(generator = "attachment_type_id_generator")
	@SequenceGenerator(name = "attachment_type_id_generator", sequenceName = "attachment_type_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "value")
	private String value;
	
}
