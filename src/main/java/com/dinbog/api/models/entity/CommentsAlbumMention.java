package com.dinbog.api.models.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Accessors(chain = true)
@Entity
@NoArgsConstructor
@Table(name = "comments_album_mention")
@SQLDelete(sql = "UPDATE dinbog.comments_album_mention SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class CommentsAlbumMention {

    @Id
    @GeneratedValue(generator = "comments_album_mention_id_generator")
    @SequenceGenerator(name = "comments_album_mention_id_generator", sequenceName = "comments_album_mention_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "comments_album_id")
    private CommentsAlbum commentsAlbum;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;

    @Column(name = "created", nullable = false)
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public CommentsAlbumMention(CommentsAlbum commentsAlbum, Profile profile) {
        this.commentsAlbum = commentsAlbum;
        this.profile = profile;
        this.created =  new Date();
    }
}
