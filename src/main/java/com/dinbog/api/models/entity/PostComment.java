package com.dinbog.api.models.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "post_comment")
@SQLDelete(sql = "UPDATE dinbog.post_comment SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
/**
 * Son los comentarios del post enlaza el comentario y el post
 * @author Luis Narvaez
 *
 */
public class PostComment {
	@Id
	@GeneratedValue(generator = "post_comment_id_generator")
	@SequenceGenerator(name = "post_comment_id_generator", sequenceName = "post_comment_id_seq", allocationSize = 1)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id")
	private Post post;
	
	@OneToOne
	@JoinColumn(name = "comments_post_id")
	private CommentsPost commentsPost;
}
