package com.dinbog.api.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "comments_post")
@SQLDelete(sql = "UPDATE dinbog.comments_post SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class CommentsPost {

	public CommentsPost() {
		this.countLike=0;
		this.listaComment=new ArrayList<>();
	}

	
	@Id
	@GeneratedValue(generator = "comments_post_id_generator")
	@SequenceGenerator(name = "comments_post_id_generator", sequenceName = "comments_post_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private CommentsPost parent;

	@Column(name = "count_like")
	private Integer countLike;

	@Column(name = "value")
	private String value;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@OneToMany(mappedBy = "parent" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<CommentsPost> listaComment;

	@OneToMany(mappedBy = "commentsPost" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<CommentsPostLike> listaLike;
	
}
