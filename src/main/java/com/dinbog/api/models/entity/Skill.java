package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "skill")
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class Skill {
	@GenericGenerator(
			name = "skill_id_seq",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "SKILL_ID_SEQ"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	
	@Id
	@GeneratedValue(generator = "skill_id_generator")
	@SequenceGenerator(name = "skill_id_generator", sequenceName = "skill_id_seq", allocationSize = 1)
	private Long id;
	
	@Column(name = "name",length = 50, nullable = false)
    private String name;
    	
	@Column(name = "description",length = 50, nullable = false)
    private String description;
    
	@Column(name = "is_active", nullable = true)
	private Boolean isActive;
	
	@Column(name = "created", nullable = true)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
