package com.dinbog.api.models.entity;


import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "category_filters")
@SQLDelete(sql = "UPDATE dinbog.category_filters SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class CategoryFilter {

	@Id
	@GeneratedValue(generator = "category_filters_id_generator")
	@SequenceGenerator(name = "catgory_filters_generator", sequenceName = "category_filters_seq", allocationSize = 1)
	private Long id;

	@Column(name = "order_id")
	private Long order;

	@Column(name = "status")
	private Long status;

	@Column(name = "profile_category_type_id")
	private Long profileCategoryTypeId;
	
	@ManyToOne
	@JoinColumn(name = "filter_id")
	private Filter filter;
}
