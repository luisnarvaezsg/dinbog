package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "profile_type")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class ProfileType {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profile_type_id_generator")
	@SequenceGenerator(name = "profile_type_id_generator", sequenceName = "profile_type_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "key", nullable = true)
	private String key;

}
