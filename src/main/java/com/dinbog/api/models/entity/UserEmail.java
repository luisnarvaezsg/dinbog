/**
 * 
 */
package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.dinbog.api.security.AttributeEncryptor;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "user_email")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class UserEmail {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "userEmailIdGenerator")
	@SequenceGenerator(name = "userEmailIdGenerator", sequenceName = "user_email_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Column(name = "level", nullable = false)
	private Integer level;

	@Column(name = "email", nullable = false)
	@Convert(converter = AttributeEncryptor.class)
	private String email;

	@Column(name = "token", nullable = true)
	private String token;

	@Column(name = "confirmed", nullable = false)
	private Boolean confirmed;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
