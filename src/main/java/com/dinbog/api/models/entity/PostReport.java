package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "post_report")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE dinbog.post_report SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class PostReport {

	@Id
	@GeneratedValue(generator = "post_report_id_generator")
	@SequenceGenerator(name = "post_report_id_generator", sequenceName = "post_report_id_seq", allocationSize = 1)
	private Long id;
	
	@Column(name = "detail")
	private String detail;
	
	@ManyToOne
	@JoinColumn(name = "report_type_id")
	private ReportType reportType;
	
	@ManyToOne
	@JoinColumn(name = "owner_id")
	private Profile owner;
	
	@ManyToOne
	@JoinColumn(name = "post_id")
	private Post post;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
