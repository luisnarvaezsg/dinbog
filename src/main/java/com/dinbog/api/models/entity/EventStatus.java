package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "event_status")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class EventStatus {
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "eventStatusIdGenerator")
	@SequenceGenerator(name = "eventStatusIdGenerator", sequenceName = "event_status_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "key", nullable = false)
	private String key;

}
