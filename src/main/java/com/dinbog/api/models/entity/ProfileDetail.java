package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "profile_detail")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class ProfileDetail {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profile_detail_id_generator")
	@SequenceGenerator(name = "profile_detail_id_generator", sequenceName = "profile_detail_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "id")
	private Profile profile;

	@ManyToOne
	@JoinColumn(name = "field_category_id")
	private FieldCategory fieldCategory;

	@Column(name = "value", length = 255, nullable = true)
	private String value;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
