package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "profile_attachment")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class ProfileAttachment {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profile_attachment_id_generator")
	@SequenceGenerator(name = "profile_attachment_id_generator", sequenceName = "profile_attachment_id_seq", allocationSize = 1)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "profile_id", referencedColumnName = "id")
	private Profile profile;
	
	@ManyToOne
	@JoinColumn(name = "attachment_id")
	private Attachment attachment;
}
