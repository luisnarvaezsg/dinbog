package com.dinbog.api.models.entity;


import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Entity
@Table(name = "album_comment")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE dinbog.album_comment SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class AlbumComment {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "album_comment_id_generator")
	@SequenceGenerator(name = "album_comment_id_generator", sequenceName = "album_comment_id_seq", allocationSize = 1)
	private Long id;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "album_id")
	private Album album;

	@OneToOne
	@JoinColumn(name = "comments_album_id")
	private CommentsAlbum commentsAlbum;

}
