package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.springframework.data.annotation.CreatedDate;

import com.dinbog.api.commons.CommonsConstant;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "comment_attachment_mention")
@SQLDelete(sql = "UPDATE DINBOG.COMMENT_ATTACHMENT_MENTION SET status = 0 WHERE id = ?")
public class CommentAttachmentMention {

	public CommentAttachmentMention() {
		
		this.setStatus(CommonsConstant.ACTIVE_STATUS);
	}

	
	@Id
	@GeneratedValue(generator = "comment_attachment_mention_id_generator")
	@SequenceGenerator(name = "comment_attachment_mention_id_generator", sequenceName = "comment_attachment_mention_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "comment_attachment_id")
	private CommentAttachment commentAttachment;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@Column(name = "status")
	private Integer status;

	@PrePersist
	public void prePersist() {
		created = new Date();
	}
	
}
