/**
 * 
 */
package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "social_account")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class SocialAccount {

	@Id
	@GeneratedValue(generator = "social_account_id_generator")
	@SequenceGenerator(name = "social_account_id_generator", sequenceName = "social_account_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "social_app_id")
	private SocialApp socialApp;

	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@Column(name = "data")
	private String data;

	@Column(name = "last_login", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
