/**
 * 
 */
package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "parameter")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Parameter {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "parameterIdGenerator")
	@SequenceGenerator(name = "parameterIdGenerator", sequenceName = "parameter_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "key")
	private String key;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "description")
	private String description;
}
