package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "traslations")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Traslation {

	@Id
	@Column(name = "key", nullable = false)
	private String key;

	@Column(name = "iso_code_language", nullable = false, length = 2)
	private String isoCode;
	
	@Column(name = "value", nullable = false)
	private String value;
	
	@Column(name = "module", nullable = false)
	private String module;
	
	
}
