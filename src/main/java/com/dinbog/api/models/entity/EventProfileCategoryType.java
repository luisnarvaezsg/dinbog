package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "event_profile_category_type")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class EventProfileCategoryType {
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "eventProfileCategoryTypeIdGenerator")
	@SequenceGenerator(name = "eventProfileCategoryTypeIdGenerator", sequenceName = "event_profile_category_type_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "event_id")
	private Event event;

	@OneToOne
	@JoinColumn(name = "profile_category_type_id")
	private ProfileCategoryType profileCategoryType;

	}
