package com.dinbog.api.models.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "attachment_tag")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
public class AttachmentTag {

    @Id
    @GeneratedValue(generator = "attachment_tag_id_generator")
    @SequenceGenerator(name = "attachment_tag_id_generator", sequenceName = "attachment_tag_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name="profile_id")
    private Profile profile;

    @ManyToOne
    @JoinColumn(name = "attachment_id")
    private Attachment attachment;

    @Column(name = "positionx", nullable = false)
    private Double positionX;

    @Column(name = "positiony", nullable = false)
    private Double positionY;

    @Column(name = "created", nullable = false)
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name= "status",nullable = false)
    private Integer status;

    public AttachmentTag(Profile profile, Attachment attachment, Double positionX, Double positionY) {
        this.profile = profile;
        this.attachment = attachment;
        this.positionX = positionX;
        this.positionY = positionY;
        this.created = new Date();
    }
}
