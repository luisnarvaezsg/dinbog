package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "city")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class City {

	@Id
	@GeneratedValue(generator = "city_id_generator")
	@SequenceGenerator(name = "city_id_generator", sequenceName = "city_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "code", length = 100)
	private String code;

	@Column(name = "latitude")
	private String latitude;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "state_name")
	private String stateName;

	@Column(name = "country_id")
	private Long countryId;
	
	@Column(name = "key")
	private String key;
}
