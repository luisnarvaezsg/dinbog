package com.dinbog.api.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "event")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Event {
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "eventIdGenerator")
	@SequenceGenerator(name = "eventIdGenerator", sequenceName = "event_id_seq", allocationSize = 1)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "event_type_id")
	private EventType eventType;
	
	@OneToOne
	@JoinColumn(name = "event_status_id")
	private EventStatus eventStatus;
	
	@OneToOne
	@JoinColumn(name = "owner_profile_id")
	private Profile owner;
	
	@OneToOne
	@JoinColumn(name = "profile_type_id")
	private ProfileType profileType;
	
	@OneToOne
	@JoinColumn(name = "country_id")
	private Country country;
	
	@OneToOne
	@JoinColumn(name = "city_id")
	private City city;
	
	//@OneToOne
	//@JoinColumn(name = "cover_attachment")
	//private Attachment coverAttachment;
	@Column(name = "cover_attachment")
	private String coverAttachment;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "name_event")
	private String nameEvent;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "publish_date")
	private Date publishDate;
	
	@Column(name = "start_date_event")
	private Date startDateEvent;
	
	@Column(name = "end_date_event")
	private Date endDateEvent;
	
	@Column(name = "announcement_date_event")
	private Date announcementDateEvent;
	
	@Column(name = "count_participant")
	private Integer countParticipant;
	
	@Column(name = "positionx")
	private Float positionX;
	
	@Column(name = "positiony")
	private Float positionY;
	
	@Column(name = "age_from")
	private Integer ageFrom;
	
	@Column(name = "age_to")
	private Integer ageTo;
	
	@Column(name = "participant_private")
	private Integer participantPrivate;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Column(name = "updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	
	@OneToMany(mappedBy = "event", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EventProfileCategoryType> eventProfileCategoryTypes;
	
	public Event() {
		eventProfileCategoryTypes = new ArrayList<>();
	}
	
	public void addEventProfileCategory(EventProfileCategoryType profileCategory) {
		this.eventProfileCategoryTypes.add(profileCategory);
	}
	
	
	public void removeEventProfileCategory(EventProfileCategoryType profileCategory) {
		
		if(this.eventProfileCategoryTypes != null){
			
			eventProfileCategoryTypes.remove(profileCategory);
			
		}
	}
}
