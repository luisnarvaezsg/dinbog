package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "profile_request_connection")
@SQLDelete(sql = "UPDATE dinbog.profile_request_connection SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class ProfileRequestConnection {
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profileRequestConnectionIdGenerator")
	@SequenceGenerator(name = "profileRequestConnectionIdGenerator", sequenceName = "profile_request_connection_id_seq", allocationSize = 1)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "owner_profile_id")
	private Profile owner;
	
	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;
	
	@ManyToOne
	@JoinColumn(name = "profile_request_status_id")
	private ProfileRequestStatus profileRequestStatus;
	
	@ManyToOne
	@JoinColumn(name = "type_connection_id")
	private ConnectionType connectionType;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

}
