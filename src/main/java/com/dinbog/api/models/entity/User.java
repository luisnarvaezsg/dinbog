/**
 * 
 */
package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "user")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class User {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "userIdGenerator")
	@SequenceGenerator(name = "userIdGenerator", sequenceName = "user_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "password", nullable = false, length = 200)
	private String password;
	
	@Column(name = "first_name", nullable = false, length = 50)
	private String firstName;
	
	@Column(name = "last_name", nullable = false, length = 50)
	private String lastName;

	@ManyToOne
	@JoinColumn(name = "user_status_id", nullable = false)
	private UserStatus userStatus;

	@Column(name = "last_login", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "status", nullable = false)
	private Integer status;
}
