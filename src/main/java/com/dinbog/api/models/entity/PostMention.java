package com.dinbog.api.models.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "post_mention")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@SQLDelete(sql = "UPDATE dinbog.post_mention SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class PostMention {

    @Id
    @GeneratedValue(generator = "post_mention_id_generator")
    @SequenceGenerator(name = "post_mention_id_generator", sequenceName = "post_mention_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="profile_id")
    private Profile profile;

    @Column(name = "created", nullable = false)
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public PostMention(Post post,Profile profile,Date created) {
        this.post = post;
        this.profile = profile;
        this.created = created;
    }
}
