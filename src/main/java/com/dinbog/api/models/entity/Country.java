package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "country")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Country {

	@Id
	@GeneratedValue(generator = "country_id_generator")
	@SequenceGenerator(name = "country_id_generator", sequenceName = "country_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "iso_code", length = 3)
	private String isoCode;

	@Column(name = "unit_measurement")
	private String unitMeasurement;

	@Column(name = "wear_measurement")
	private String wearMeasurement;

	@Column(name = "is_onu")
	private Boolean isOnu;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "language_id")
	private Long languageId;
	
	@Column(name = "key")
	private String key;
}
