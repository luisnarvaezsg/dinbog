package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "attachment")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE dinbog.attachment SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class Attachment {

	@Id
	@GeneratedValue(generator = "attachment_id_generator")
	@SequenceGenerator(name = "attachment_id_generator", sequenceName = "attachment_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "path")
	private String path;

	@Column(name = "metadata")
	private String metadata;

	@Column(name = "extra")
	private String extra;

	@Column(name = "purged")
	private Boolean purged;

	@Column(name = "date_purged")
	@Temporal(TemporalType.TIMESTAMP)
	private Date datePurged;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "count_attachment")
	private Long countAttachment;

	@Column(name = "count_like")
	private Long countLikes;

	@Column(name = "count_comments")
	private Long countComments;

	@ManyToOne
	@JoinColumn(name = "attachment_type_id")
	private AttachmentType attachmentType;

	@Column(name = "status")
	private Integer status;

	
	public void addLike() {
		this.setCountLikes(getCountLikes() + 1);
	}
	
	public void unLike() {
		this.setCountLikes(getCountLikes() - 1);
	}



}
