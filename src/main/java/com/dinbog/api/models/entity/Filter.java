package com.dinbog.api.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "filters")
@SQLDelete(sql = "UPDATE dinbog.filters SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class Filter {
	
	public Filter() {
		this.filtersValues = new ArrayList<>();
	}

	public Filter(Long filterId) {
		this.id = filterId;
	}

	@Id
	@GeneratedValue(generator = "filters_id_generator")
	@SequenceGenerator(name = "filters_id_generator", sequenceName = "filters_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;
	
	@Column(name = "validator")
	private String validator;
	
	@Column(name = "key")
	private String key;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@ManyToOne
	@JoinColumn(name = "filter_type_id")
	private FilterType filterType;
	
	@ManyToOne
	@JoinColumn(name = "filter_parent_id")
	private Filter parent;

	@OneToMany(mappedBy = "filter", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<FilterValue> filtersValues;
	
	@Column(name = "status")
	private Long status;
}
