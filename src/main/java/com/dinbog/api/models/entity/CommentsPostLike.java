package com.dinbog.api.models.entity;

import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "comments_post_like")
@SQLDelete(sql = "UPDATE dinbog.comments_post_like SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class CommentsPostLike {

	
	@Id
	@GeneratedValue(generator = "comments_post_like_id_generator")
	@SequenceGenerator(name = "comments_post_like_id_generator", sequenceName = "comments_post_like_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "comments_post_id")
	private CommentsPost commentsPost;

	
	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	
	
}
