package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "message")
@SQLDelete(sql = "UPDATE dinbog.message SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")

public class Message {

	@Id
	@GeneratedValue(generator = "message_id_generator")
	@SequenceGenerator(name = "message_id_generator", sequenceName = "message_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "content")
	private String content;


	@Column(name = "message_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date messageDate;

	@ManyToOne
	@JoinColumn(name = "profile_id_src")
	private Profile profileSrc;

	@ManyToOne
	@JoinColumn(name = "profile_id_dest")
	private Profile profileDest;
	
}
