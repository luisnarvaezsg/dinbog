package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "social_app")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class SocialApp {

	@Id
	@GeneratedValue(generator = "social_app_id_generator")
	@SequenceGenerator(name = "social_app_id_generator", sequenceName = "social_app_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "domain")
	private String domain;

	@Column(name = "name")
	private String name;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "client_id")
	private String clientId;

	@Column(name = "secret")
	private String secret;

	@Column(name = "key")
	private String key;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Column(name = "updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
}
