package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "poll_notification")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class PollNotification {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "poll_notification_id_generator")
	@SequenceGenerator(name = "poll_notification_id_generator", sequenceName = "poll_notification_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "object_id")
	private Long objectId;

	@ManyToOne
	@JoinColumn(name = "owner_profile_id")
	private Profile ownerProfile;

	@ManyToOne
	@JoinColumn(name = "to_profile_id")
	private Profile toProfile;

	@ManyToOne
	@JoinColumn(name = "notification_action_id")
	private NotificationAction notificationAction;

	@Column(name = "message")
	private String message;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
