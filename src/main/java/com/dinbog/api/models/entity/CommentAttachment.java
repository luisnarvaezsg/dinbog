package com.dinbog.api.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.springframework.data.annotation.CreatedDate;

import com.dinbog.api.commons.CommonsConstant;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "comment_attachment")
@SQLDelete(sql = "UPDATE DINBOG.COMMENT_ATTACHMENT SET status = 0 WHERE id = ?")
public class CommentAttachment {

	public CommentAttachment() {
		
		this.countLike = 0;
		this.setStatus(CommonsConstant.ACTIVE_STATUS);
		this.attachmentList = new ArrayList<>();
	}


	
	@Id
	@GeneratedValue(generator = "comment_attachment_id_generator")
	@SequenceGenerator(name = "comment_attachment_id_generator", sequenceName = "comment_attachment_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private CommentAttachment parent;

	@Column(name = "count_like")
	private Integer countLike;
	
	@Column(name = "status")
	private Integer status;

	@Column(name = "value")
	private String value;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@ManyToOne
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@OneToMany(mappedBy = "parent" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<CommentAttachment> attachmentList;

	@PrePersist
	public void prePersist() {
		created = new Date();
	}
	
}
