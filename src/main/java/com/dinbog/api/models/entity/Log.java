/**
 * 
 */
package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "log")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Log {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "logIdGenerator")
	@SequenceGenerator(name = "logIdGenerator", sequenceName = "log_id_seq", allocationSize = 1)
	private long id;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Column(name = "is_error", nullable = false)
	private Boolean isError;

	@Column(name = "is_warning", nullable = false)
	private Boolean isWarning;

	@Column(name = "message", nullable = false)
	private String message;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
