package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "option")
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class Option {

	@GenericGenerator(name = "option_id_seq", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "OPTION_ID_SEQ"),
			@Parameter(name = "initial_value", value = "1"), @Parameter(name = "increment_size", value = "1") })

	
	@Id
	@GeneratedValue(generator = "option_id_generator")
	@SequenceGenerator(name = "option_id_generator", sequenceName = "option_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "field_id")
	private Field field;

	@Column(name = "order_by", length = 50, nullable = true)
	private Integer parentId;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "value_spa", nullable = true)
	private String valueSpa;

	@Column(name = "value_fra", nullable = true)
	private String valueFra;

	@Column(name = "value_ita", nullable = true)
	private String valueIta;

	@Column(name = "value_por", nullable = true)
	private String valuePor;

	@Column(name = "value_rus", nullable = true)
	private String valueRus;

	@Column(name = "value_chi", nullable = true)
	private String valueChi;

	@Column(name = "id_value_old", length = 50, nullable = false)
	private Integer idValueOld;

}
