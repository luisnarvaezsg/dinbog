package com.dinbog.api.models.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Table(name = "profile_category_type")
@Setter
@Getter
@Accessors(chain = true)
@ToString(exclude = { "parent" })
@EqualsAndHashCode(of = { "id" })
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "child" })
public class ProfileCategoryType {

	public ProfileCategoryType(Long categoryId) {
		this.id = categoryId;
	}

	public ProfileCategoryType() {
		
	}
	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profile_category_type_id_generator")
	@SequenceGenerator(name = "profile_category_type_id_generator", sequenceName = "profile_category_type_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	@JsonBackReference
	private ProfileCategoryType parent;

	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	private List<ProfileCategoryType> child;

	@ManyToOne
	@JoinColumn(name = "profile_type_id")
	private ProfileType profileType;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "level", nullable = false)
	private Long level;
	
	@Column(name = "key", nullable = false)
	private String key;
	
}
