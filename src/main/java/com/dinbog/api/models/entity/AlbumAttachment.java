package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "album_attachment")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE dinbog.album_attachment SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class AlbumAttachment {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "album_attachment_id_generator")
	@SequenceGenerator(name = "album_attachment_id_generator", sequenceName = "album_attachment_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "album_id")
	private Album album;

	@ManyToOne
	@JoinColumn(name = "attachment_id")
	private Attachment attachment;

	@Column(name = "is_cover")
	private Long isCover;

	@Column(name = "status")
	private Integer status;
	
	@PrePersist
	public void prePersist() {
		status = 1;
	}
}
