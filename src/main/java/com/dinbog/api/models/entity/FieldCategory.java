package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "field_category")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class FieldCategory {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "field_category_id_generator")
	@SequenceGenerator(name = "field_category_id_generator", sequenceName = "field_category_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "field_id")
	private Field field;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private ProfileCategoryType profileCategoryType;
}
