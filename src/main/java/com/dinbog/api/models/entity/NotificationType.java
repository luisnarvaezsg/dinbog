package com.dinbog.api.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "notification_type")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class NotificationType {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "notification_type_id_generator")
	@SequenceGenerator(name = "notification_type_id_generator", sequenceName = "notification_type_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "value", nullable = false)
	private String value;

	@Column(name = "value_spa", nullable = true)
	private String valueSpa;

	@Column(name = "value_fra", nullable = true)
	private String valueFra;

	@Column(name = "value_ita", nullable = true)
	private String valueIta;

	@Column(name = "value_por", nullable = true)
	private String valuePor;

	@Column(name = "value_rus", nullable = true)
	private String valueRus;

	@Column(name = "value_chi", nullable = true)
	private String valueChi;
}
