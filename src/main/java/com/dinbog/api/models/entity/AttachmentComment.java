/**
 * 
 */
package com.dinbog.api.models.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;

import com.dinbog.api.commons.CommonsConstant;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "attachment_comment")
@Setter
@Getter
@SQLDelete(sql = "UPDATE DINBOG.ATTACHMENT_COMMENT SET status = 0 WHERE id = ?")

public class AttachmentComment {
	
	public AttachmentComment() {
		
		this.setStatus(CommonsConstant.ACTIVE_STATUS);
	}

	@Id
	@GeneratedValue(generator = "attachment_comment_id_generator")
	@SequenceGenerator(name = "attachment_comment_id_generator", sequenceName = "attachment_comment_id_seq", allocationSize = 1)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "attachment_id")
	private Attachment attachment;
	
	@OneToOne
	@JoinColumn(name = "comment_id")
	private CommentAttachment commentAttachment;
	
	@Column(name = "status")
	private Integer status;
}
