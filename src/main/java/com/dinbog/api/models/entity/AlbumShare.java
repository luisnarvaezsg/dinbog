package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "album_share")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class AlbumShare {
    @Id
    @GeneratedValue(generator = "album_share_id_generator")
    @SequenceGenerator(name = "album_share_id_generator", sequenceName = "album_share_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "owner_profile_id", nullable = false)
    private Profile ownerProfile;

    @ManyToOne
    @JoinColumn(name = "album_id", nullable = false)
    private Album album;

    @Column(name = "created", nullable = false)
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

}
