/**
 * 
 */
package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "attachment_report")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class AttachmentReport {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "attachmentReportIdGenerator")
	@SequenceGenerator(name = "attachmentReportIdGenerator", sequenceName = "attachment_report_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "report_type_id", nullable = false)
	private ReportType reportType;

	@Column(name = "detail", nullable = false)
	private String detail;

	@ManyToOne
	@JoinColumn(name = "owner_id", nullable = false)
	private Profile owner;

	@ManyToOne
	@JoinColumn(name = "attachment_id", nullable = false)
	private Attachment attachment;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
