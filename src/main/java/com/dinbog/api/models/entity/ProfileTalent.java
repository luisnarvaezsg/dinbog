/**
 * 
 */
package com.dinbog.api.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Entity
@Table(name = "profile_talent")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Where(clause = "status <> 0")
public class ProfileTalent implements Serializable {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "profileTalentIdGenerator")
	@SequenceGenerator(name = "profileTalentIdGenerator", sequenceName = "profile_talent_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "birth_date")
	@Temporal(TemporalType.DATE)
	private Date birthDate;
	
	@OneToOne
	@JoinColumn(name = "gender_id")
	private Gender gender;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Column(name="status")
	Integer status;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "profile_id")
	private Profile profile;
	
	
	@OneToMany(mappedBy = "profile" , fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProfileCountry> profileCountries;
	
	public ProfileTalent() {

		profileCountries = new ArrayList<>();
	}
	
	public void addProfileCountries(ProfileCountry profileCountry) {
		this.profileCountries.add(profileCountry);
	}

	public void removeProfileCountries(ProfileCountry profileCountry) {
		
		if(this.profileCountries != null){
			
			profileCountries.remove(profileCountry);
			profileCountry.setProfile(null);
	       
		}
	}

}
