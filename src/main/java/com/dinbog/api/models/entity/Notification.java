package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "notification")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Notification {

	@Id
	@Column(name = "id", updatable = false, nullable = false)
	@GeneratedValue(generator = "notification_id_generator")
	@SequenceGenerator(name = "notification_id_generator", sequenceName = "notification_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "owner_user_id")
	private User ownerUser;

	@ManyToOne
	@JoinColumn(name = "to_user_id")
	private User toUser;

	@Column(name = "is_read")
	private Integer isRead;

	@ManyToOne
	@JoinColumn(name = "event_notification_id")
	private EventNotification eventNotification;

	@ManyToOne
	@JoinColumn(name = "notification_action_id")
	private NotificationAction notificationAction;

	@ManyToOne
	@JoinColumn(name = "profile_notification_id")
	private ProfileNotification profileNotification;

	@ManyToOne
	@JoinColumn(name = "post_notification_id")
	private PostNotification postNotification;

	@ManyToOne
	@JoinColumn(name = "attachment_notification_id")
	private AttachmentNotification attachmentNotification;

	@ManyToOne
	@JoinColumn(name = "poll_notification_id")
	private PollNotification pollNotification;

	@ManyToOne
	@JoinColumn(name = "album_notification_id")
	private AlbumNotification albumNotification;

	@ManyToOne
	@JoinColumn(name = "user_notification_id")
	private UserNotification userNotification;

	@ManyToOne
	@JoinColumn(name = "conversation_notification_id")
	private ConversationNotification conversationNotification;

	@Column(name = "date_read", nullable = false)
	private Date dateRead;

	@Column(name = "date_notification", nullable = false)
	private Date dateNotification;

	@Column(name = "code_message")
	private String codeMessage;
}
