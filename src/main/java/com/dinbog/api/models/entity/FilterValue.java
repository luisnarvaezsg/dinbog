package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "filters_values")
@SQLDelete(sql = "UPDATE dinbog.filters_values SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class FilterValue {

	@Id
	@GeneratedValue(generator = "filters_value_id_generator")
	@SequenceGenerator(name = "filters_value_id_generator", sequenceName = "filters_value_id_seq", allocationSize = 1)
	private Long id;

	@Column(name = "description")
	private String description;

	@Column(name = "value")
	private String value;
	
	@Column(name = "key")
	private String key;
	
	@ManyToOne
	@JoinColumn(name = "filter_id")
	private Filter filter;
	
	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "status")
	private Long status;
}
