package com.dinbog.api.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Persistent;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "profile_report")
@Setter
@Getter
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class ProfileReport {

	@Id
	@GeneratedValue(generator = "profile_report_id_generator")
	@SequenceGenerator(name = "profile_report_id_generator", sequenceName = "profile_report_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "owner_profile_id")
	private Profile ownerProfile;

	@ManyToOne
	@Persistent
	@JoinColumn(name = "profile_id")
	private Profile profile;

	@ManyToOne
	@JoinColumn(name = "report_type_id", nullable = false)
	private ReportType reportType;

	@Column(name = "detail", nullable = false, length = 400)
	private String detail;

	@Column(name = "created", nullable = false)
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
}
