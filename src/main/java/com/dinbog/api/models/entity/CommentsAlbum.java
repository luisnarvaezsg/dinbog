package com.dinbog.api.models.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "comments_album")
@SQLDelete(sql = "UPDATE dinbog.comments_album SET status = 0 WHERE id = ?")
@Where(clause = "status <> 0")
public class CommentsAlbum {

    @Id
    @GeneratedValue(generator = "comments_album_id_generator")
    @SequenceGenerator(name = "comments_album_id_generator", sequenceName = "comments_album_id_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private CommentsAlbum parent;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;

    @Column(name = "count_like")
    private Integer countLike;

    @Column(name = "value")
    private String value;

    @Column(name = "created", nullable = false)
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

}
