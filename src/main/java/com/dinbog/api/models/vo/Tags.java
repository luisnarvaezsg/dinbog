package com.dinbog.api.models.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class Tags {
    private Long profileId;
    private String posX;
    private String posY;

    public Tags(Long profileId, String posX, String posY) {
        this.profileId = profileId;
        this.posX = posX;
        this.posY = posY;
    }
}
