package com.dinbog.api.models.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Setter
@Getter
@Accessors(chain = true)
@NoArgsConstructor
public class Attachment {
    private MultipartFile file;
    private List<Tags> tags;

    public Attachment(MultipartFile file, List<Tags> tags) {
        this.file = file;
        this.tags = tags;
    }

    public Attachment(MultipartFile file){
        this.file =  file;
    }
}
