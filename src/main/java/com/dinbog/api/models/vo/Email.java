package com.dinbog.api.models.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.Map;

@Getter
@Setter
@ToString
public class Email  {

    private String to;
    private String subject;
    private Map<String,Object> bodyMap;

    public Email() {
        super();
    }

    public Email(String to, String subject, Map<String,Object> map){
        this.to =  to;
        this.subject = subject;
        this.bodyMap = map;
    }

}
