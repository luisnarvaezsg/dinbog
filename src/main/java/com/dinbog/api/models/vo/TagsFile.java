package com.dinbog.api.models.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class TagsFile {
    private Long ownerId;
    private List<Tags> tags;
}
