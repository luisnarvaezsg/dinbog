package com.dinbog.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.dinbog.api.dto.*;

import com.dinbog.api.exception.*;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.util.mapper.UserEmailMapper;
import com.dinbog.api.util.mapper.UserStatusMapper;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.service.IUserService;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 */
@Slf4j
@RestController
@RequestMapping("/v1/users")
@CrossOrigin(allowCredentials = "true")
public class UserController {

	@Autowired
	private IUserService userService;

	
	/**
	 * Crea un User (Proceso de registro)
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<UserCreateResponseDTO> create(@RequestBody final UserCreateDTO request,
														@RequestParam(name="lang", required = false) String lang) {

			return new ResponseEntity<>(userService.create(request, lang), HttpStatus.CREATED);
			
	}

	/**
	 * Busca lista paginada de users
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<PaginateResponse<UserDTO>> readAll(@RequestParam(required = true) Integer pageNumber,
															 @RequestParam(required = true) Integer pageSize, @RequestParam(required = false) Integer statusId) {

		try {
		
			return new ResponseEntity<>(userService.getAll(pageNumber, pageSize, statusId), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> readById(@PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), 
						ApiErrorEnum.USER_NOT_FOUND.getMessage())));
			} else {
				return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.readById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Enviar un email a un user
	 * 
	 * @return
	 */
	@PostMapping("/emails")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void sendEmail(@RequestBody @Valid final UserSendEmailDTO request) {

		try {
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {

				// VALIDA QUE EMAIL EXISTA
				if (userService.userEmailExists(request.getSendTo())) {
					// LLAMADA A CLASE SERVICE
				} else {
					// SI EMAIL NO EXISTE
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMAIL_TO_NOT_FOUND.getCode(), 
							ApiErrorEnum.EMAIL_TO_NOT_FOUND.getMessage())));
				}

			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error en request EN [UserController.sendEmail]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.sendEmail]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Crea un email para un user
	 * 
	 * @param id
	 * @param request
	 * @return
	 */
	@PostMapping("/{id}/emails")
	public ResponseEntity<UserEmailDTO> createEmail(@PathVariable(value = "id") Long id,
			@RequestBody final UserEmailRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(UserEmailMapper.mapUserEmail(userService.createEmail(id, request)), HttpStatus.CREATED);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error validando el request en [UserController.createEmail]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.createEmail]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Trae los emails de un user
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}/emails")
	public ResponseEntity<List<UserEmailDTO>> getEmails(@PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
			return new ResponseEntity<>(userService.getEmails(id), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.getEmails]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Edita el nivel de un email y retorna la lista completa de emails del usuario
	 * actualizada
	 * 
	 * @param userId
	 * @param id
	 * @param level
	 * @return
	 */
	@PatchMapping("/{userId}/emails/{id}/levels/{level}")
	public ResponseEntity<List<UserEmailDTO>> editEmailLevel(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "id") Long id, @PathVariable(value = "level") Integer level) {

		try {
			if (Util.isEmpty(userId)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_USER_ID.getCode(), ApiErrorEnum.EMPTY_USER_ID.getMessage())));
			}
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			if (Util.isEmpty(level)) {
				throw new InvalidRequestException(Lists.mutable
						.of(new ApiError(ApiErrorEnum.EMPTY_LEVEL.getCode(), ApiErrorEnum.EMPTY_LEVEL.getMessage())));
			}

			// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
			return new ResponseEntity<>(userService.editEmail(userId, id, level), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.createEmail]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Elimina un correo de un usuario
	 * 
	 * @param userId
	 * @param id
	 */
	@DeleteMapping("/{userId}/emails/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteEmail(@PathVariable(value = "userId") Long userId, @PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			if (Util.isEmpty(userId)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_USER_ID.getCode(), ApiErrorEnum.EMPTY_USER_ID.getMessage())));
			}

			userService.deleteEmail(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.readById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Busca un user asociado a un email
	 * 
	 * @param email
	 * @return
	 */
	@GetMapping("/emails")
	public ResponseEntity<UserDTO> readByEmail(@RequestParam(name = "search", required = true) String email) {

		try {
			if (Util.isEmpty(email)) {
				throw new ResourceNotFoundException(Lists.mutable
						.of(new ApiError(ApiErrorEnum.EMPTY_EMAIL.getCode(), ApiErrorEnum.EMPTY_EMAIL.getMessage())));
			} else {
				return new ResponseEntity<>(userService.getByEmail(email), HttpStatus.OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.readByEmail]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Valida un token via URL, para confirmar un email
	 * 
	 * @return
	 */
	@GetMapping("/emails/{token}")
	public ResponseEntity<ConfirmEmailDTO> verifyToken(@PathVariable(value = "token") String token) {

		return new ResponseEntity<>(userService.confirmEmailToken(token), HttpStatus.OK);
		
	}

	/**
	 * @param request
	 * @return
	 */
	@PatchMapping("/{id}")
	public ResponseEntity<UserDTO> editUser(@PathVariable(value = "id") Long id,
			@RequestBody final UserEditDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage())));
			}
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(userService.update(id, request), HttpStatus.OK);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error validando el request en [UserController.editUser]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.editUser]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * 
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping("/password/reset")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void resetPassword(@RequestBody final PasswordResetDTO passwordResetDTO, HttpServletRequest request) {
		try {
			User user =  userService.findByEmail( passwordResetDTO.getEmail());

			if (Util.isEmpty(user)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage())));
			}
			userService.resetPassword(user,passwordResetDTO.getEmail(),request);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.resetPassword]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage())));
			}

			userService.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("/status")
	public ResponseEntity<UserStatusDTO> createStatus(@RequestBody final UserStatusRequestDTO request) {
		
		return new ResponseEntity<>(UserStatusMapper.mapUserStatus(userService.createUserStatus(request)), HttpStatus.CREATED);
		
	}

	/**
	 * @return
	 */
	@GetMapping("/status")
	public ResponseEntity<MutableList<UserStatusDTO>> readAllStatus() {
		
		return new ResponseEntity<>(userService.readAllUserStatus(), HttpStatus.OK);
	
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/status/{id}")
	public ResponseEntity<UserStatusDTO> readByIdStatus(@PathVariable("id") Long id) {
		
		return new ResponseEntity<>(UserStatusMapper.mapUserStatus(userService.readUserStatusById(id)), HttpStatus.OK);
	
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/status/{id}")
	public ResponseEntity<UserStatusDTO> updateStatus(@PathVariable("id") Long id,
													  @RequestBody final UserStatusRequestDTO request) {
		
		return new ResponseEntity<>(UserStatusMapper.mapUserStatus(userService.updateUserStatus(id, request)), HttpStatus.ACCEPTED);
		
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/status/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") Long id) {
		
		userService.deleteUserStatusById(id);
	
	}

	
	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaRequest(UserEditDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		// ID STATUS
		if (Util.isEmpty(request.getUserStatusId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_USER_STATUS.getCode(),
					ApiErrorEnum.EMPTY_USER_STATUS.getMessage()));
		}
		// LISTA DE GRUPOS
		/**/
		if (request.getGroups().isEmpty()) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GROUPS.getCode(), ApiErrorEnum.EMPTY_GROUPS.getMessage()));
		}

		return apiErrors;
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaRequest(UserEmailRequestDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		// LEVEL
		if (Util.isEmpty(request.getLevel())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_EMAIL_LEVEL.getCode(),
					ApiErrorEnum.EMPTY_EMAIL_LEVEL.getMessage()));
		}
		// EMAIL
		if (Util.isEmpty(request.getEmail())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_EMAIL.getCode(), ApiErrorEnum.EMPTY_EMAIL.getMessage()));
		} else {
			// VALIDA QUE EMAIL NO EXISTA
			if (userService.userEmailExists(request.getEmail())) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.USER_EMAIL_EXISTS.getCode(), ApiErrorEnum.USER_EMAIL_EXISTS.getMessage())));
			}
		}

		return apiErrors;
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaRequest(UserSendEmailDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		// SEND TO
		if (Util.isEmpty(request.getSendTo())) {
			apiErrors
					.add(new ApiError(ApiErrorEnum.EMPTY_EMAIL_TO.getCode(), ApiErrorEnum.EMPTY_EMAIL_TO.getMessage()));
		} else {
			if (!Util.isValidEmail(request.getSendTo())) {
				apiErrors.add(new ApiError(ApiErrorEnum.FORMAT_EMAIL_TO.getCode(),
						ApiErrorEnum.FORMAT_EMAIL_TO.getMessage()));
			}
		}
		// SUBJECT
		if (Util.isEmpty(request.getSubject())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_EMAIL_SUBJECT.getCode(),
					ApiErrorEnum.EMPTY_EMAIL_SUBJECT.getMessage()));
		}
		// BODY
		if (Util.isEmpty(request.getBody())) {
			apiErrors.add(
					new ApiError(ApiErrorEnum.EMPTY_EMAIL_BODY.getCode(), ApiErrorEnum.EMPTY_EMAIL_BODY.getMessage()));
		}

		return apiErrors;
	}

	@PostMapping("/password/renew")
	@ResponseStatus(HttpStatus.OK)
	public void patchPassword(
			@RequestBody final UserPatchPasswordDTO userPatchPasswordDTO,
			HttpServletRequest request,
			@RequestParam("lang") String lang)
	{
		userService.patchPassword(userPatchPasswordDTO,request,lang);
	}
}
