package com.dinbog.api.controller;

import com.dinbog.api.service.IReportTypeService;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.ReportTypeDTO;
import com.dinbog.api.dto.ReportTypeRequestDTO;
import com.dinbog.api.dto.ReportTypeResponseDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/report-types")
@CrossOrigin(allowCredentials = "true")
public class ReportTypeController {

	@Autowired
	private IReportTypeService reportTypeService;

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<ReportTypeResponseDTO> create(@RequestBody final ReportTypeRequestDTO request) {

		try {
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(reportTypeService.create(request), HttpStatus.CREATED);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error validando el request en [ReportTypeController.create]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Busca una lista de tipos de reporte
	 * 
	 * @param lang
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<ReportTypeDTO>> read(@RequestParam(name = "lang", required = false) String lang) {

		try {
			if (Util.isEmpty(lang)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_LANGUAGE.getCode(), ApiErrorEnum.EMPTY_LANGUAGE.getMessage())));
			} else {
				return new ResponseEntity<>(reportTypeService.read(lang), HttpStatus.OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeController.read]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ReportTypeResponseDTO> update(@PathVariable(value = "id") Long id,
			@RequestBody final ReportTypeRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(reportTypeService.update(id, request), HttpStatus.OK);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error validando el request en [ReportTypeController.update]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeController.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			reportTypeService.delete(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeController.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaRequest(ReportTypeRequestDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		// VALUE
		if (Util.isEmpty(request.getValue())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_VALUE.getCode(), ApiErrorEnum.EMPTY_VALUE.getMessage()));
		}
		
		return apiErrors;
	}
}
