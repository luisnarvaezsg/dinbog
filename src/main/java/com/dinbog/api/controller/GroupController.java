package com.dinbog.api.controller;

import com.dinbog.api.dto.*;
import com.dinbog.api.service.IGroupMenuService;
import com.dinbog.api.service.IGroupService;
import com.dinbog.api.service.IUserGroupService;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/groups")
@CrossOrigin(allowCredentials = "true")
public class GroupController {

	@Autowired
	private IUserGroupService userGroupService;

	@Autowired
	private IGroupService groupService;

	@Autowired
	private IGroupMenuService groupMenuService;

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<GroupBasicDTO>> readAll() {
		try {
			return new ResponseEntity<>(groupService.readAll(), HttpStatus.OK);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<GroupBasicDTO> create(@RequestBody final GroupRequestDTO request) {
		try {
			MutableList<ApiError> errors = validateRequest(request);
			if (errors.isEmpty()) {
				return new ResponseEntity<>(groupService.create(request), HttpStatus.CREATED);
			} else {
				InvalidRequestException e = new InvalidRequestException(errors);
				Util.printLogError(e);
				throw e;
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.createGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/{id}")
	public ResponseEntity<GroupBasicDTO> update(@PathVariable("id") Long id,
			@RequestBody final GroupRequestDTO request) {
		try {

			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			return new ResponseEntity<>(groupService.update(id, request), HttpStatus.ACCEPTED);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.updateGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {
		try {

			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			groupService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.deleteGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/{id}/users")
	public ResponseEntity<MutableList<UserGroupDTO>> createUserGroup(@PathVariable("id") Long id,
                                                                     @RequestBody final UserGroupRequestDTO request) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			return new ResponseEntity<>(userGroupService.create(id, request), HttpStatus.CREATED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.createUserGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@GetMapping("/{id}/users")
	public ResponseEntity<MutableList<UserGroupDTO>> readByGroupId(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			return new ResponseEntity<>(userGroupService.readByGroupId(id), HttpStatus.OK);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.readByGroupId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}/users")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteByGroupId(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			userGroupService.deleteByGroupId(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.deleteByGroupId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/user/{userId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteGroupByUserId(@PathVariable("userId") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			userGroupService.deleteGroupByUser(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.deleteGroupByUserId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/{id}/menus")
	public ResponseEntity<MutableList<GroupMenuDTO>> createGroupMenu(@PathVariable("id") Long id,
                                                                     @RequestBody final GroupMenuRequestDTO request) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			return new ResponseEntity<>(groupMenuService.create(id, request), HttpStatus.CREATED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.createUserGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@GetMapping("/{id}/menus")
	public ResponseEntity<MutableList<GroupMenuDTO>> readMenuByGroupId(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			return new ResponseEntity<>(groupMenuService.readByGroupId(id), HttpStatus.OK);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.readByGroupId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}/menus")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteMenuByGroupId(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			groupMenuService.deleteByGroup(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupController.deleteByGroupId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	private MutableList<ApiError> validateRequest(GroupRequestDTO request) {
		MutableList<ApiError> apiErrors = Lists.mutable.of();
		if (Util.isEmpty(request.getName())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_NAME.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_NAME.getMessage()));
		}

		if (Util.isEmpty(request.getDescription())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_DESCRIPTION.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_DESCRIPTION.getMessage()));
		}

		if (Util.isEmpty(request.getIsActive())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ACTIVE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_ACTIVE.getMessage()));
		}
		return apiErrors;
	}
}
