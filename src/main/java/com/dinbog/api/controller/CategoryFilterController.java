package com.dinbog.api.controller;
import java.util.Optional;

import com.dinbog.api.service.*;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.dto.CategoryFilterDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.ProfileCategoryType;
import com.dinbog.api.util.Util;


@RestController
@RequestMapping("/v1/filters")
@CrossOrigin(allowCredentials = "true")
public class CategoryFilterController {
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private IProfileCategoryTypeService categoryTypeService;
	
	@Autowired
	private ICategoryFilterService categoryService;

	@GetMapping("/{category}/")
	public ResponseEntity<MutableList<CategoryFilterDTO>> getFilters(@RequestParam(name = "lang", required = false) String lang,
															@PathVariable("category") final Long categoryId) {
		
		if(Util.isEmpty(lang)) {
			
			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		if (Util.isEmpty(categoryId)) {
				
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
		
		} 
		
		Optional<ProfileCategoryType> optional = categoryTypeService.findById(categoryId);
		ProfileCategoryType profileCategoryType = new ProfileCategoryType();
			
		if(optional.isPresent()) {
			profileCategoryType = optional.get();
		}
		
		return new ResponseEntity<>(categoryService.findByProfileCategoryType(profileCategoryType, lang), HttpStatus.OK);		
	}
	
}




