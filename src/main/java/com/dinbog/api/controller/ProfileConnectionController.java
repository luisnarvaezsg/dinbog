package com.dinbog.api.controller;

import com.dinbog.api.dto.ProfileConnectionRequestingDTO;
import com.dinbog.api.dto.ProfileRequestConnectionDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRequestConnection;
import com.dinbog.api.models.entity.ProfileRequestStatus;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.IApiErrorService;
import com.dinbog.api.service.IProfileRequestConnectionService;
import com.dinbog.api.service.IProfileRequestStatusService;
import com.dinbog.api.service.IProfileService;
import com.dinbog.api.util.ProfileConnectionStatus;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/v1/profiles-connection")
@CrossOrigin(allowCredentials = "true")
public class ProfileConnectionController {

    private final JwtProvider jwtProvider;
    private final IProfileRequestConnectionService requestConnectionService;
    private final IProfileService profileService;
    private final IApiErrorService iApiErrorService;
    private final IProfileRequestStatusService requestStatusService;

    @Autowired
    public ProfileConnectionController(
            JwtProvider jwtProvider,
            IProfileRequestConnectionService requestConnectionService,
            IProfileService profileService,
            IApiErrorService iApiErrorService,
            IProfileRequestStatusService requestStatusService
    ) {
        this.jwtProvider = jwtProvider;
        this.requestConnectionService =  requestConnectionService;
        this.profileService = profileService;
        this.iApiErrorService = iApiErrorService;
        this.requestStatusService = requestStatusService;
    }

    @PostMapping("/new")
    @ResponseStatus(CREATED)
    public void addNewConnection(
            @RequestBody final ProfileConnectionRequestingDTO profileConnectionRequestingDTO,
            @RequestParam("lang") String lang,
            HttpServletRequest request
    ){

        User user = jwtProvider.getUser( request );

        Optional<Profile> profileOwner = profileService.findByUser(user);

        if(profileOwner.isEmpty()){
            throw new InvalidRequestException(
                    Lists.mutable.of(iApiErrorService.getApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND, lang ) )
            );
        }

        requestConnectionService.create(profileOwner.get(), profileConnectionRequestingDTO,lang );
    }

    @PutMapping("/{id}/{statusId}/request")
    @ResponseStatus(OK)
    public void strategyProfileConnection(
            @PathVariable("id") Long id,
            @PathVariable("statusId") Long statusId,
            @RequestParam("lang") String lang ,
            HttpServletRequest request
    ){
        User user = jwtProvider.getUser( request );

        Optional<Profile> profile = profileService.findByUser(user);
        if(profile.isEmpty()){
            throw new InvalidRequestException(
                    Lists.mutable.of(iApiErrorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang ) )
            );
        }

        Optional<ProfileRequestStatus> requestStatus = requestStatusService.findById(statusId);
        if(requestStatus.isEmpty())
            throw new InvalidRequestException(
                    Lists.mutable.of(
                            iApiErrorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND, lang)
                    )
            );

        Long code = ProfileConnectionStatus.fitProfileStrategy( requestStatus.get().getId() );

        if (code == null)
            throw new InvalidRequestException(
                    Lists.mutable.of(
                            iApiErrorService.getApiError(ApiErrorEnum.PROFILE_CONNECTION_ACTION_NOT_VALID, lang)
                    )
            );

        Optional<ProfileRequestConnection> requestConnection = requestConnectionService.findConnectionByIdProfileIdAndRequestStatus(
            id, profile.get().getId(), code
        );

        if(requestConnection.isEmpty())
            throw new InvalidRequestException(
                    Lists.mutable.of(
                            iApiErrorService.getApiError(
                                    ApiErrorEnum.PROFILE_CONNECTION_ACTION_NOT_VALID, lang
                            )
                    )
            );

        requestConnectionService.editingConnection(requestConnection.get(),requestStatus.get().getId(), lang);
    }

    @PutMapping("/{id}/cancel")
    @ResponseStatus(OK)
    public void cancelConnection(
            @PathVariable("id") Long id,
            @RequestParam("lang") String lang ,
            HttpServletRequest request
    ){
        User user = jwtProvider.getUser( request );

        Optional<Profile> profileOwner = profileService.findByUser(user);
        if(profileOwner.isEmpty()){
            throw new InvalidRequestException(
                    Lists.mutable.of(iApiErrorService.getApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND, lang ) )
            );
        }

        Optional<ProfileRequestConnection> requestConnection = requestConnectionService.findActiveRequestConnectionByIdAndOwnerId(id, profileOwner.get().getId());
        if (requestConnection.isEmpty()){
            throw new InvalidRequestException(
                    Lists.mutable.of(iApiErrorService.getApiError(ApiErrorEnum.PROFILE_CONNECTION_CAN_NOT_BE_CANCELLED, lang ) )
            );
        }

        requestConnectionService.editingConnection(requestConnection.get(), ProfileConnectionStatus.CANCELED.getCode(), lang);
    }

  /*  @GetMapping("/{index}/{limit}/pending-list")
    public ResponseEntity<MutableList<ProfileRequestConnectionDTO>> getPendingList(
            @PathVariable("index") Integer index,
            @PathVariable("limit") Integer limit,
            @RequestParam("lang") String lang,
            HttpServletRequest request
    ){
        User user = jwtProvider.getUser( request );

        Optional<Profile> profile = profileService.findByUser(user);
        if(profile.isEmpty()){
            throw new InvalidRequestException(
                    Lists.mutable.of(iApiErrorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_PENDING_NOT_FOUND, lang ) )
            );
        }

        MutableList<ProfileRequestConnectionDTO> listOfPending = requestConnectionService.findAllPendingByProfileId(
                profile.get().getId(),index,limit,lang
        );

        return new ResponseEntity<>(listOfPending, HttpStatus.OK);
    }

   */

    @GetMapping("/{index}/{limit}/{statusCode}/list")
    public ResponseEntity<MutableList<ProfileRequestConnectionDTO>> listByStatus(
            @PathVariable("statusCode") Long statusCode,
            @PathVariable("index") Integer index,
            @PathVariable("limit") Integer limit,
            @RequestParam("lang") String lang,
            HttpServletRequest request
    ){
        User user = jwtProvider.getUser( request );

        Optional<Profile> profile = profileService.findByUser(user);
        if(profile.isEmpty()){
            throw new InvalidRequestException(
                    Lists.mutable.of(iApiErrorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang ) )
            );
        }

        MutableList<ProfileRequestConnectionDTO> listByRequestStatus = requestConnectionService
                .findAllConnectionsByRequestStatus( profile.get(),statusCode,index,limit,lang);

        return new ResponseEntity<>(listByRequestStatus, HttpStatus.OK);
    }

}
