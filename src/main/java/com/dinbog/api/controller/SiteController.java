package com.dinbog.api.controller;

import com.dinbog.api.dto.SiteCreateDTO;
import com.dinbog.api.dto.SiteDTO;
import com.dinbog.api.service.ISiteService;
import com.dinbog.api.util.mapper.SiteMapper;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/sites")
@CrossOrigin(allowCredentials = "true")
public class SiteController {

	@Autowired
	private ISiteService siteService;

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<SiteDTO>> readAll() {
		try {

			return new ResponseEntity<>(siteService.readAll(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("")
	public ResponseEntity<SiteDTO> create(@RequestBody final SiteCreateDTO request) {

		try {
			MutableList<ApiError> errors = validateRequest(request);
			if (errors.isEmpty()) {
				return new ResponseEntity<>(SiteMapper.mapSite(siteService.create(request)), HttpStatus.CREATED);
			} else {
				throw new InvalidRequestException(errors);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchById(@PathVariable("id") final Long id, @RequestBody final SiteCreateDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_SITE_ID.getCode(), ApiErrorEnum.EMPTY_SITE_ID.getMessage())));
			}

			siteService.patchById(id, request);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteController.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") final Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_SITE_ID.getCode(), ApiErrorEnum.EMPTY_SITE_ID.getMessage())));
			}
			siteService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteController.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(SiteCreateDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getActive())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SITE_IS_ACTIVE.getCode(),
					ApiErrorEnum.EMPTY_SITE_IS_ACTIVE.getMessage()));
		}

		if (Util.isEmpty(request.getDomain())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SITE_DOMAIN.getCode(),
					ApiErrorEnum.EMPTY_SITE_DOMAIN.getMessage()));
		}

		if (Util.isEmpty(request.getName())) {
			apiErrors.add(
					new ApiError(ApiErrorEnum.EMPTY_SITE_NAME.getCode(), ApiErrorEnum.EMPTY_SITE_NAME.getMessage()));
		}

		return apiErrors;
	}
}
