package com.dinbog.api.controller;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.CityDTO;
import com.dinbog.api.dto.CityRequestDTO;
import com.dinbog.api.dto.CountryDTO;
import com.dinbog.api.dto.CountryRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.City;
import com.dinbog.api.models.entity.Country;
import com.dinbog.api.service.IApiErrorService;
import com.dinbog.api.service.ICityService;
import com.dinbog.api.service.ICountryService;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.CityMapper;
import com.dinbog.api.util.mapper.CountryMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/countries")
@CrossOrigin(allowCredentials = "true")
public class CountryController {

	@Autowired
	private ICountryService countryService;

	@Autowired
	private ICityService cityService;
	
	@Autowired
	private IApiErrorService errorService;
	

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<CountryDTO> create(@RequestBody final CountryRequestDTO request) {
		try {
			MutableList<ApiError> errors = validateRequest(request);
			if (errors.isEmpty()) {
				Country country=countryService.create(request);
				return new ResponseEntity<>(CountryMapper.mapCounty(country), HttpStatus.CREATED);
			}
			throw new InvalidRequestException(errors);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<CountryDTO> update(@PathVariable("id") Long id,
			@RequestBody final CountryRequestDTO request) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			Country country=countryService.update(id, request);
			return new ResponseEntity<>(CountryMapper.mapCounty(country), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryController.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			countryService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryController.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param languageId
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<CountryDTO>> readAll(@RequestParam(required = false) String lang) {
		
		return new ResponseEntity<>(countryService.readAll(lang), HttpStatus.OK);
	
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("{id}/cities")
	public ResponseEntity<MutableList<CityDTO>> readByCountry(@PathVariable("id") Long id, 
															  @RequestParam(name = "lang",required = false) String lang) {

		if (Util.isEmpty(id)) {
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NO_FOUND, lang)));
		} 
		
		return new ResponseEntity<>(cityService.getCities(id, lang), HttpStatus.OK);

	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PostMapping("/{id}/cities")
	public ResponseEntity<CityDTO> createCity(@PathVariable("id") Long id, @RequestBody final CityRequestDTO request) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			City city=cityService.create(id, request);
			return new ResponseEntity<>(CityMapper.mapCity(city), HttpStatus.CREATED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryController.createCity]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param countryId
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/{countryId}/cities/{id}")
	public ResponseEntity<CityDTO> updateCity(@PathVariable("countryId") Long countryId, @PathVariable("id") Long id,
			@RequestBody final CityRequestDTO request) {
		try {
			if (Util.isEmpty(countryId)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_CITY_ID.getCode(), ApiErrorEnum.EMPTY_CITY_ID.getMessage())));
			}

			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			City city = cityService.update(id, countryId, request);
			return new ResponseEntity<>(CityMapper.mapCity(city), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryController.updateCity]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param countryId
	 * @param id
	 */
	@DeleteMapping("/{countryId}/cities/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCity(@PathVariable("countryId") Long countryId, @PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(countryId)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_CITY_ID.getCode(), ApiErrorEnum.EMPTY_CITY_ID.getMessage())));
			}

			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			cityService.deleteByCountry(countryId, id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryController.deleteCity]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	private MutableList<ApiError> validateRequest(CountryRequestDTO request) {
		MutableList<ApiError> apiErrors = Lists.mutable.empty();
		if (Util.isEmpty(request.getName())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_NAME.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_NAME.getMessage()));
		}
		return apiErrors;
	}
}
