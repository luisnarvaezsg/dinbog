package com.dinbog.api.controller;

import com.dinbog.api.dto.*;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.PostAttachment;
import com.dinbog.api.models.entity.PostReport;
import com.dinbog.api.models.entity.PostShare;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.*;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.files.FileUtils;
import com.dinbog.api.util.url.UrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;

@Slf4j
@RestController
@RequestMapping("/v1/posts")
@CrossOrigin(allowCredentials = "true")
public class PostController {

	@Autowired
	private JwtProvider jwtProvider; 
	
	@Autowired
	private IPostService postService;

	@Autowired
	private IPostAttachmentService iPostAttachmentService;

	@Autowired
	private IPostLikeService postLikeService;

	@Autowired
	private IPostShareService postShareService;

	@Autowired
	private IPostReportService postReportService;

	@Autowired
	private IPostHashtagService postHashtagService;
	
	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private FileUtils fileUtils;

	@Autowired
	private UrlUtils urlUtils;

	@PostMapping("/multi-file")
	@ResponseStatus(CREATED)
	public void createPostV2(@ModelAttribute("post") PostMultiFileDTO post,HttpServletRequest request) {
		if(  fileUtils.validateFileAttach(post.getFiles()) > 0  ) {
			post.doAttachment();
			postService.createPost(post,jwtProvider.getUser(request),"EN");
		}else{
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, post.getLanguage())));
		}
	}

    /**
     * Post Rest Endpoint for validation and persisting an array of MultipartFile as audio, video, and an image listed
     * and admitted by the app, posted by any logged user.
     * @param file  MultipartFile Array that contains the files for both evaluation and persisting it. This array may contain different kind of file in it
     * @param comment Post description. It may contain hashtags and mentions within.
     * @param lang Request language
     * @param request Request Context
     */
	@PostMapping("")
	@ResponseStatus(CREATED)
	public void createPost(
			@RequestPart("file") MultipartFile[] file,
			@RequestParam("comment") String comment,
			@RequestParam("lang") String lang,
			HttpServletRequest request
	){
			MultipartFile[] toUpload = fileUtils.validateFileAttach(file);

			if( toUpload.length > 0 ) postService.createPost(toUpload, comment, jwtProvider.getUser(request), lang);
			else throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, lang)));
	}

    /**
     * Post Rest Endpoint that enables persist a pair of audio and its cover picture by any logged user.
     * @param audioFile An audio file that it must among the list of admits content type by the app.
     * @param cover An image file that it must among the list of admits content type by the app. It is a nullable parameter
     * @param comment Post description. It may contain hashtags and mentions within.
     * @param lang Request language
     * @param request Request Context
     */
	@PostMapping("/audio-cover")
	@ResponseStatus(CREATED)
	public void createPostMultiFile(
			@RequestPart("audio") MultipartFile audioFile,
			@RequestPart("cover") MultipartFile cover,
			@RequestParam("comment") String comment,
			@RequestParam("lang") String lang,
			HttpServletRequest request
	){
		Map<String, MultipartFile> toUpload = fileUtils.validateFileAttach(audioFile, cover);

		if (!toUpload.isEmpty())
			postService.createPost(toUpload, comment, jwtProvider.getUser(request), lang);
		else
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, lang)));

	}

	/**
	 * Post Rest Endpoint that enables persist a URL posted by any logged user
	 *
	 * @param url     Safe Url for persisting. Only admits https based.
	 * @param comment Post description. It may contain hashtags and mentions within.
	 * @param lang    Request language
	 * @param request Request Context
	 */
	@PostMapping("/link")
	@ResponseStatus(CREATED)
	public void createLinkPost(@RequestParam("url") String url, @RequestParam("comment") String comment,
			@RequestParam("lang") String lang, HttpServletRequest request) {
		if (urlUtils.isValid(url))
			postService.createPost(url, comment, jwtProvider.getUser(request), lang);
		else
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.INVALID_URL, lang)));
	}
	

	@PostMapping("/{id}/comment")
	@ResponseStatus(CREATED)
	public void createPostComment(@PathVariable("id") String id, @RequestParam("comment") String comment,
			@RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(id) || !(Util.isInteger(id))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}
		if (Util.isEmpty(comment)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang)));
		}

		postService.postComment(Long.parseLong(id), comment, jwtProvider.getUser(request), lang);

	}

	@GetMapping("/muro")
	@ResponseBody
	public ResponseEntity<MutableList<PostDTO>> muro(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize, @RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(pageNumber) || Util.isEmpty(pageSize) || !(Util.isInteger(pageNumber))
				|| !(Util.isInteger(pageSize))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PAGE_NUMBER, lang)));
		}
		MutableList<PostDTO> listaPost = postService.muro(jwtProvider.getUser(request), lang, pageNumber, pageSize);

		return new ResponseEntity<>(listaPost, HttpStatus.OK);
	}


	@PostMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void editById(@PathVariable(value = "id") Long id,
			@RequestParam("lang") String lang, HttpServletRequest request ) {

		if (Util.isEmpty(id) ||  !(Util.isInteger(id)) ) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}
		
		postService.deleteById(id, lang);
		
	}
	
	
	@DeleteMapping("/{id}/delete")
	@ResponseStatus(HttpStatus.OK)
	public void deleteById(@PathVariable("id") Long id,	@RequestParam("lang") String lang) {

		if (Util.isEmpty(id) ||  !(Util.isInteger(id)) ) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}
		
		postService.deleteById(id, lang);
		
	}
	
	@GetMapping("/get/{id}")
	@ResponseBody
	public ResponseEntity<PostDTO> getPost(@PathVariable("id")String id, @RequestParam("lang") String lang,
			HttpServletRequest request) {
					
		if (Util.isEmpty(id) ||  !(Util.isInteger(id)) ) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}
		
		return new ResponseEntity<>(postService.getPost(Long.parseLong(id), lang, jwtProvider.getUser(request)), HttpStatus.FOUND);
	}

	
	@GetMapping("/getURL/{id}")
	@ResponseBody
	public ResponseEntity<String> getPostURL(@PathVariable("id")String id, @RequestParam("lang") String lang,
			HttpServletRequest request) {
					
		if (Util.isEmpty(id) ||  !(Util.isInteger(id)) ) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}
		String url=postService.getPostURL(id);
		
		return new ResponseEntity<>(url, HttpStatus.OK);
	}




	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void edit(@PathVariable(value = "id") Long id, @RequestParam("comment") String comment,
			@RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(id) ||  !(Util.isInteger(id)) ) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}
		if (Util.isEmpty(comment)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang)));
		}
		postService.update(id, comment,jwtProvider.getUser(request), lang);
	}

	/**
	 * @param postId
	 * @param attachmentId
	 * @return
	 */
	@PostMapping("/{postId}/attachment/{attachmentId}")
	public ResponseEntity<PostAttachment> addAttachment(@PathVariable(value = "postId") Long postId,
			@PathVariable(value = "attachmentId") Long attachmentId) {

		try {
			if (Util.isEmpty(postId)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage())));
			}
			if (Util.isEmpty(attachmentId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(), ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage())));
			}

			return new ResponseEntity<>(iPostAttachmentService.create(postId, attachmentId), HttpStatus.ACCEPTED);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostController.createByPostId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/like")
	public ResponseEntity<PostLikeResponseDTO> createLike(@RequestBody PostLikeDTO postLike,
			@RequestParam("lang") String lang, HttpServletRequest request) {

			if (Util.isEmpty(postLike.getPostId()) || Util.isEmpty(postLike.getProfileId())) {
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang)));
			} else {
				return new ResponseEntity<>(postLikeService.create(postLike.getPostId(), postLike.getProfileId(), lang),
						HttpStatus.CREATED);
			}

	}
	
	/**
	 * @param id
	 */
	@DeleteMapping("/{id}/dislike")
	@ResponseStatus(HttpStatus.OK)
	public void deletePostLikeById(@PathVariable(value = "id") Long id,  
			@RequestParam("lang") String lang, HttpServletRequest request ) {

		if (Util.isEmpty(id) ||  !(Util.isInteger(id)) ) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_POST_ID, lang)));
		}

		postLikeService.deleteLike(id, jwtProvider.getUser(request), lang);
		
	} 
	

	@PostMapping("/{id}/shares")
	public ResponseEntity<PostShare> createShare(@PathVariable(value = "id") Long id,
			@RequestBody final PostShareRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage())));
			} else {
				if (postService.getById(id, null) == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage())));
				}
				return new ResponseEntity<>(postShareService.create(id, request.getProfileId()), HttpStatus.CREATED);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostController.createShare]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/{id}/reports")
	public ResponseEntity<PostReport> createReport(@PathVariable(value = "id") Long id,
			@RequestBody final PostReportRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage())));
			} else {
				if (postService.getById(id, null) == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage())));
				}
				return new ResponseEntity<>(postReportService.create(id, request), HttpStatus.CREATED);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostController.createReport]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param tag
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/hasTag")
	public ResponseEntity<PaginateResponse<PostHashtagDTO>> readHasTag(@RequestParam(required = false) String tag,
			@RequestParam(required = false) Boolean findExact, @RequestParam(required = false) Boolean isSearch,
			@RequestParam(name = "lang", required = false) String lang, @RequestParam() Integer pageNumber,
			@RequestParam() Integer pageSize) {
		try {
			if (Util.isEmpty(tag)) {
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.POST_HASHTAG_NOT_FOUND.getCode(),
								ApiErrorEnum.POST_HASHTAG_NOT_FOUND.getMessage())));
			} else {

				return new ResponseEntity<>(
						postHashtagService.readAll(pageNumber, pageSize, findExact, tag, isSearch, lang),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostController.readHashTag]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @GetMapping("/tagpeople/") public ResponseEntity<PaginateResponse<PostDTO>>
	 * getTagPeople() { try { String profileValue=""; if
	 * (Util.isEmpty(profileValue)) { throw new
	 * InvalidRequestException(Lists.mutable.of( new
	 * ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
	 * ApiErrorEnum.POST_NOT_FOUND.getMessage()))); } else { typePost = "profile";
	 * return new ResponseEntity<>( postService.getPostsByProfileId(profileValue,
	 * typePost, pageNumber, pageSize, lang), HttpStatus.OK); }
	 *
	 * /** @GetMapping("/tagpeople/") public
	 * ResponseEntity<PaginateResponse<PostDTO>> getTagPeople() { try { String
	 * profileValue=""; if (Util.isEmpty(profileValue)) { throw new
	 * InvalidRequestException(Lists.mutable.of( new
	 * ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
	 * ApiErrorEnum.POST_NOT_FOUND.getMessage()))); } else { typePost = "profile";
	 * return new ResponseEntity<>( postService.getPostsByProfileId(profileValue,
	 * typePost, pageNumber, pageSize, lang), HttpStatus.OK); }
	 *
	 * } catch (Exception e) { // VALIDA SI ES ERROR CONTROLADO if
	 * (Util.isCustomException(e)) throw e; // EN CASO DE ERROR NO CONTROLADO
	 * log.error("*** Exception EN [PostController.getPostProfileHome]");
	 * Util.printLogError(e); throw new InternalErrorException(e); } }
	 **/

	/**
	 * @GetMapping("/tagpeople/") public ResponseEntity<PaginateResponse<PostDTO>>
	 * getPost() { try { String profileValue=""; if (Util.isEmpty(profileValue)) {
	 * throw new InvalidRequestException(Lists.mutable.of( new
	 * ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
	 * ApiErrorEnum.POST_NOT_FOUND.getMessage()))); } else { typePost = "profile";
	 * return new ResponseEntity<>( postService.getPostsByProfileId(profileValue,
	 * typePost, pageNumber, pageSize, lang), HttpStatus.OK); }
	 * 
	 * } catch (Exception e) { // VALIDA SI ES ERROR CONTROLADO if
	 * (Util.isCustomException(e)) throw e; // EN CASO DE ERROR NO CONTROLADO
	 * log.error("*** Exception EN [PostController.getPostProfileHome]");
	 * Util.printLogError(e); throw new InternalErrorException(e); } }
	 *
	 *
	 * } catch (Exception e) { // VALIDA SI ES ERROR CONTROLADO if
	 * (Util.isCustomException(e)) throw e; // EN CASO DE ERROR NO CONTROLADO
	 * log.error("*** Exception EN [PostController.getPostProfileHome]");
	 * Util.printLogError(e); throw new InternalErrorException(e); } }
	 **/

	/**
	 * @GetMapping("/tagpeople/") public ResponseEntity<PaginateResponse<PostDTO>>
	 * getPost() { try { String profileValue=""; if (Util.isEmpty(profileValue)) {
	 * throw new InvalidRequestException(Lists.mutable.of( new
	 * ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
	 * ApiErrorEnum.POST_NOT_FOUND.getMessage()))); } else { typePost = "profile";
	 * return new ResponseEntity<>( postService.getPostsByProfileId(profileValue,
	 * typePost, pageNumber, pageSize, lang), HttpStatus.OK); }
	 *
	 * } catch (Exception e) { // VALIDA SI ES ERROR CONTROLADO if
	 * (Util.isCustomException(e)) throw e; // EN CASO DE ERROR NO CONTROLADO
	 * log.error("*** Exception EN [PostController.getPostProfileHome]");
	 * Util.printLogError(e); throw new InternalErrorException(e); } }
	 **/

}
