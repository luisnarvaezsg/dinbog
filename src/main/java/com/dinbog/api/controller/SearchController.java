package com.dinbog.api.controller;

import com.dinbog.api.dto.ProfileSuggestedDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.IApiErrorService;
import com.dinbog.api.service.ISearchService;
import org.eclipse.collections.api.factory.Lists;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.PostDTO;
import com.dinbog.api.dto.ProfileBasicDTO;
import com.dinbog.api.dto.ProfileFollowBasicDTO;
import com.dinbog.api.dto.SearchDTO;
import com.dinbog.api.util.Util;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/v1/search")
@CrossOrigin(allowCredentials = "true")
public class SearchController {

	@Autowired
	private ISearchService searchService;

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private IApiErrorService errorService;

	@GetMapping("")
	public ResponseEntity<SearchDTO> readAll(
			@RequestParam() Integer pageNumber, @RequestParam() Integer pageSize,
			@RequestParam(required = false) Boolean findExact, @RequestParam() String query,
			@RequestParam(name = "lang", required = false) String lang) {

		return new ResponseEntity<>(searchService.readAll(pageNumber, pageSize, findExact, query, lang), HttpStatus.OK);

	}

	@GetMapping("/recomended")
	@ResponseBody
	public ResponseEntity<MutableList<ProfileFollowBasicDTO>> profilesRecomended(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize,	@RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(pageNumber) || Util.isEmpty(pageSize) || !(Util.isInteger(pageNumber))
				|| !(Util.isInteger(pageSize))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PAGE_NUMBER, lang)));
		}

		MutableList<ProfileFollowBasicDTO> listaProfile = searchService.findAllProfilesRandomNotInFollowers(jwtProvider.getUser(request), pageNumber, pageSize, lang);

		return new ResponseEntity<>(listaProfile, HttpStatus.CREATED);
	}

	@GetMapping("/talentByName")
	@ResponseBody
	public ResponseEntity<MutableList<ProfileFollowBasicDTO>> talentByName(@RequestParam("pageSize") Integer pageSize,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("name") String name, @RequestParam("lang") String lang,
			HttpServletRequest request) {

		if (Util.isEmpty(pageNumber) || Util.isEmpty(pageSize) || !(Util.isInteger(pageNumber))
				|| !(Util.isInteger(pageSize))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PAGE_NUMBER, lang)));
		}

		MutableList<ProfileFollowBasicDTO> listaProfile = searchService.findTalentByNameLike(jwtProvider.getUser(request),pageSize, pageNumber, name, lang );

		return new ResponseEntity<>(listaProfile, HttpStatus.CREATED);
	}

	@GetMapping("/{fullName}/suggested-list")
	public ResponseEntity<List<ProfileSuggestedDTO>> getActivatedProfileSuggested(
			@PathVariable("fullName") String fullName,
			@RequestParam("lang") String lang,
			HttpServletRequest request
	){
		if(Util.isEmpty(fullName)){
			throw  new InvalidRequestException(Lists.mutable.of( new ApiError(ApiErrorEnum.EMPTY_NAME.getCode(), ApiErrorEnum.EMPTY_NAME.getMessage())));
		}else {
			List<ProfileSuggestedDTO> suggested = searchService.getSuggestedProfileByUserName(jwtProvider.getUser(request),fullName);
			return new ResponseEntity<>(suggested, OK);
		}
	}

	@GetMapping("/companyByName")
	@ResponseBody
	public ResponseEntity<MutableList<ProfileBasicDTO>> companyByName(@RequestParam("pageSize") Integer pageSize,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("name") String name, @RequestParam("lang") String lang,
			HttpServletRequest request) {

		if (Util.isEmpty(pageNumber) || Util.isEmpty(pageSize) || !(Util.isInteger(pageNumber))
				|| !(Util.isInteger(pageSize))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PAGE_NUMBER, lang)));
		}

		MutableList<ProfileBasicDTO> listaProfile = searchService.findCompanyByNameLike(jwtProvider.getUser(request),pageSize, pageNumber, name, lang);

		return new ResponseEntity<>(listaProfile, HttpStatus.CREATED);
	}
	
	@GetMapping("/hashtag")
	@ResponseBody
	public ResponseEntity<MutableList<PostDTO>> byHashtag(@RequestParam("pageSize") Integer pageSize,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("hashtag") String hashtag,
			@RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(pageNumber) || Util.isEmpty(pageSize) || !(Util.isInteger(pageNumber))
				|| !(Util.isInteger(pageSize))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PAGE_NUMBER, lang)));
		}
		
		MutableList<PostDTO> listaPost = searchService.findHashtagLike(pageSize, pageNumber, 
				hashtag, jwtProvider.getUser(request), lang);

		return new ResponseEntity<>(listaPost, HttpStatus.CREATED);
	}
}
