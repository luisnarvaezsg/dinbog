package com.dinbog.api.controller;

import com.dinbog.api.dto.MensajeDTO;
import com.dinbog.api.dto.MenuDTO;
import com.dinbog.api.dto.MenuRequestDTO;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Menu;
import com.dinbog.api.service.IMessageService;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/messages")
@CrossOrigin(allowCredentials = "true")
public class MessageController {

	@Autowired
	private IMessageService messageService;

	@GetMapping("")
	public ResponseEntity<MutableList<MenuDTO>> readAll() {

		try {
			// return new ResponseEntity<>(menuService.readAll(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
		return null;
	}

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void createMessage(@RequestBody final MensajeDTO mensajeDTO, 
			@RequestParam("idDest") Long idDest, @RequestParam("lang") String lang, HttpServletRequest request) {

		messageService.createMessage(request, idDest, mensajeDTO, lang);
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/{id}")
	public ResponseEntity<Menu> update(@PathVariable("id") Long id, @RequestBody final MenuRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			//return new ResponseEntity<>(menuService.update(id, request), HttpStatus.ACCEPTED);
return null;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuController.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			//menuService.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuController.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	
}
