package com.dinbog.api.controller;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.AlbumTypeDTO;
import com.dinbog.api.dto.AlbumTypeRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.AlbumType;
import com.dinbog.api.service.IAlbumTypeService;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AlbumTypeMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/album-types")
@CrossOrigin(allowCredentials = "true")
public class AlbumTypeController {

	@Autowired
	private IAlbumTypeService albumTypeService;

	@PostMapping("")
	public ResponseEntity<AlbumTypeDTO> create(@RequestBody final AlbumTypeRequestDTO request) {

		try {
			AlbumType albumType=albumTypeService.create(request);
			return new ResponseEntity<>(AlbumTypeMapper.mapAlbumType(albumType), HttpStatus.CREATED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<AlbumTypeDTO>> readAll() {

		try {
			return new ResponseEntity<>(albumTypeService.readAll(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<AlbumTypeDTO> update(@PathVariable("id") Long id,
			@RequestBody final AlbumTypeRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(), ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			AlbumType albumType=albumTypeService.update(id, request);
			return new ResponseEntity<>(AlbumTypeMapper.mapAlbumType(albumType), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeController.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(), ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			albumTypeService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeController.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
