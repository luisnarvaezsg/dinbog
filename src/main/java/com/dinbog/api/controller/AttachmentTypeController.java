package com.dinbog.api.controller;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.AttachmentTypeBasicDTO;
import com.dinbog.api.dto.AttachmentTypeDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.AttachmentType;
import com.dinbog.api.service.IAttachmentTypeService;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AttachmentTypeMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/attachment-types")
@CrossOrigin(allowCredentials = "true")
public class AttachmentTypeController {

	@Autowired
	private IAttachmentTypeService attachmentTypeService;

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<AttachmentTypeDTO>> readAll() {

		try {
			return new ResponseEntity<>(attachmentTypeService.readAll(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("")
	public ResponseEntity<AttachmentTypeDTO> create(@RequestBody final AttachmentTypeBasicDTO request) {

		try {
			if (Util.isEmpty(request.getValue())) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_VALUE.getCode(),
								ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_VALUE.getMessage())));
			}
			AttachmentType attachmentType=attachmentTypeService.create(request);
			return new ResponseEntity<>(AttachmentTypeMapper.mapAttachmentType(attachmentType), HttpStatus.CREATED);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchById(@PathVariable("id") final Long id, @RequestBody final AttachmentTypeBasicDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_ID.getCode(),
								ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_ID.getMessage())));
			} else if (Util.isEmpty(request.getValue())) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_VALUE.getCode(),
								ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_VALUE.getMessage())));
			}
			attachmentTypeService.patchById(id, request);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeController.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") final Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_ID.getCode(),
								ApiErrorEnum.EMPTY_ATTACHMENT_TYPE_ID.getMessage())));
			}

			attachmentTypeService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeController.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
