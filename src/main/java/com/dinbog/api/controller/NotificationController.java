package com.dinbog.api.controller;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.firebase.PushNotificationRequest;
import com.dinbog.api.firebase.PushNotificationResponse;
import com.dinbog.api.service.INotificationService;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/notifications")
@CrossOrigin(allowCredentials = "true")
public class NotificationController {

	private static final String MESSAGE_OK = "Notification has been sent.";
	@Autowired
	private INotificationService notificationService;

	///////// BEGING: Este bloque de código es para enviar noticiaciones
	// push personalizad consumiendo los endpoint desde un portal-admin
	/////////////////////////////////////////////////////////////////////
	@SuppressWarnings("rawtypes")
	@PostMapping("/topic")
	public ResponseEntity sendNotification(@RequestBody PushNotificationRequest request) {
		notificationService.sendPushNotificationWithoutData(request);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), MESSAGE_OK),
				HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/token")
	public ResponseEntity sendTokenNotification(@RequestBody PushNotificationRequest request) {
		notificationService.sendPushNotificationToToken(request);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), MESSAGE_OK),
				HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/data")
	public ResponseEntity sendDataNotification(@RequestBody PushNotificationRequest request) {
		notificationService.sendPushNotification(request);
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), MESSAGE_OK),
				HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/example")
	public ResponseEntity sendSampleNotification() {
		notificationService.sendSamplePushNotification();
		return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), MESSAGE_OK),
				HttpStatus.OK);
	}

	///////// END: Este bloque de código es para enviar noticiaciones
	// push personalizad consumiendo los endpoint desde un portal-admin
	/////////////////////////////////////////////////////////////////////
	/**
	 *
	 * @param id
	 * @param isRead
	 */
	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchIsRead(@PathVariable("id") Long id, @RequestParam("") Integer isRead) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			if (Util.isEmpty(isRead)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GLOBAL_IS_READ.getCode(), ApiErrorEnum.EMPTY_GLOBAL_IS_READ.getMessage())));
			}
			notificationService.patchIsRead(id, isRead);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [NotificationController.updatePassword]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
