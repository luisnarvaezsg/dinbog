package com.dinbog.api.controller;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.FieldDTO;
import com.dinbog.api.dto.OptionDTO;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.service.IFieldService;
import com.dinbog.api.service.IOptionService;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/fields")
@CrossOrigin(allowCredentials = "true")
public class FieldController {

	@Autowired
	private IFieldService fieldService;

	@Autowired
	private IOptionService optionService;

	/**
	 * @param fieldId
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<FieldDTO>> readAll(@RequestParam(required = false) Long fieldId) {

		// VALIDAR CUAL ES LA FUNCION EXACTA DE ESTE ENDPOINT
		// SI FILTRO POR ID DEL FIELD, PARA QUE UNA LISTA DE FIELD? NO TIENE SENTIDO
		// GENERAR 2 ENDPOINT, ESTE SIN FILTRO, Y OTRO DE GET FIELD POR ID

		try {
			return new ResponseEntity<>(fieldService.readAll(fieldId), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [FieldController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param fieldId
	 * @return
	 */
	@GetMapping("/{fieldId}/options")
	public ResponseEntity<MutableList<OptionDTO>> readOptionsByFieldId(@PathVariable("fieldId") Long fieldId) {

		try {
			return new ResponseEntity<>(optionService.readOptionsByFieldId(fieldId), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [FieldController.readOptionsByFieldId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
