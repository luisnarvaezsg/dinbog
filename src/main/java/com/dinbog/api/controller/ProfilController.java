package com.dinbog.api.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.dinbog.api.dto.*;
import com.dinbog.api.models.entity.*;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.*;
import com.dinbog.api.util.mapper.*;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.files.FileUtils;

import lombok.extern.slf4j.Slf4j;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

/**
 * Se tuvo que cambiar el nombre de la clase ProfilController (eliminando la 'e'
 * de Profile) Esto debido a un conflicto en la ejecución de Tests con la clase:
 * [org.springframework.data.rest.webmvc.ProfileController]
 * 
 * @author Luis
 *
 */
@Slf4j
@RestController
@RequestMapping("/v1/profiles")
@CrossOrigin(allowCredentials = "true")
public class ProfilController {

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private IProfileCategoryTypeService profileCategoryTypeService;

	@Autowired
	private IProfileService profileService;

	@Autowired
	private IProfileAttachmentService profileAttachmentService;

	@Autowired
	private IAlbumService albumService;

	@Autowired
	private IProfileBlockService profileBlockService;

	@Autowired
	private IProfileShareService profileShareService;

	@Autowired
	private IProfileReportService profileReportService;

	@Autowired
	private IProfileRecommendationService profileRecommendationService;

	@Autowired
	private IProfileRequestConnectionService profileRequestConnectionService;

	@Autowired
	private INotificationService notificationService;

	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private FileUtils fileUtils;

	@Autowired
	private IPostService postService;

	@Autowired
	private IProfileWorkService profileWorkService;

	@Autowired
	private IAlbumTypeService albumTypeService;

	private static final Integer PAGE_DEFAULT = 1;

	private static final Integer SIZE_DEFAULT = 5;

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("/save")
	public ResponseEntity<ProfileDTO> saveProfile(@RequestBody final ProfileRequestDTO request,
			@RequestParam(name = "lang", required = false) String lang) {

		return new ResponseEntity<>(profileService.saveProfile(request, lang), CREATED);

	}

	/**
	 * @param urlName
	 * @return
	 */
	@GetMapping("/findbyurl/{urlName}/")
	public ResponseEntity<ProfileFullDTO> getProfileByUrlName(@PathVariable("urlName") String urlName,
			@RequestParam(name = "lang", required = false) String lang, HttpServletRequest request) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		User user = jwtProvider.getUser(request);

		if (Util.isEmpty(urlName)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
		} else {
			return new ResponseEntity<>(profileService.readFullByUrl(urlName, user, lang), OK);
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@GetMapping("/{urlName}/statistics")
	public ResponseEntity<ProfileStatisticsDTO> getProfileStatistics(@PathVariable("urlName") String urlName) {

		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				return new ResponseEntity<>(profileService.getProfileStatistics(urlName), OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.getProfileStatistics]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param search
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<PaginateResponse<ProfileDTO>> readAll(@RequestParam("") Integer pageNumber,
			@RequestParam(name = "lang", required = false) String lang, @RequestParam("") Integer pageSize,
			@RequestParam(required = false) String search) {

		try {
			return new ResponseEntity<>(profileService.readAll(pageNumber, pageSize, search, lang), OK);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@PatchMapping("/{urlName}")
	public ResponseEntity<ProfileFullDTO> editProfileFull(@PathVariable("urlName") String urlName,
			@RequestParam(name = "lang", required = false) String lang,
			@RequestBody final ProfileFullRequestDTO request) {

		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				return new ResponseEntity<>(profileService.editFullByUrl(urlName, request, lang), OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.editProfileFull]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @param urlFollow
	 */
	@PostMapping("/follow/{urlFollow}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createFollow(@PathVariable("urlFollow") String urlFollow, HttpServletRequest request,
			@RequestParam(name = "lang", required = false) String lang) {

		try {

			User user = jwtProvider.getUser(request);

			if (Util.isEmpty(urlFollow)) {
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
			}

			profileService.createFollow(user, urlFollow, lang);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.createFollow]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @param size
	 * @return
	 */
	@GetMapping("/{urlName}/suggested")
	public ResponseEntity<MutableList<ProfileSimpleDTO>> readSuggested(@PathVariable("urlName") String urlName,
			@RequestParam(name = "lang", required = false) String lang, @RequestParam(required = false) Integer size) {

		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else if (Util.isEmpty(size)) {
				throw new InvalidRequestException(Lists.mutable
						.of(new ApiError(ApiErrorEnum.EMPTY_SIZE.getCode(), ApiErrorEnum.EMPTY_SIZE.getMessage())));
			} else {

				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, lang));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(profileService.getSuggesteds(profileDTO.getId(), size, lang), OK);
				}
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.readSugested]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@GetMapping("/{urlName}/views")
	public ResponseEntity<MutableList<ProfileSimpleDTO>> readProfilesViewed(
			@RequestParam(name = "lang", required = false) String lang, @PathVariable("urlName") String urlName) {

		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, lang));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(profileService.getProfilesViewed(profileDTO.getId(), lang), OK);
				}
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.readProfilesViewed]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PostMapping("/{id}/attachments")
	public ResponseEntity<ProfileAttachment> createAttachment(@PathVariable("id") Long id,
			@RequestBody final ProfileAttachmentRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_PROFILE_ID.getCode(),
						ApiErrorEnum.EMPTY_PROFILE_ID.getMessage())));
			}

			return new ResponseEntity<>(profileAttachmentService.create(id, request), OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.createAttachment]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param lang
	 * @param profileId
	 * @return
	 */
	@GetMapping("/{profileId}/attachments")
	public ResponseEntity<MutableList<ProfileAttachmentDTO>> getAttachmentsByProfileId(
			@RequestParam(name = "lang", required = false) String lang,
			@PathVariable("profileId") final Long profileId) {

		try {
			if (Util.isEmpty(profileId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_PROFILE_ID.getCode(),
						ApiErrorEnum.EMPTY_PROFILE_ID.getMessage())));
			} else {
				if (profileService.readById(profileId, lang) == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				}
				return new ResponseEntity<>(profileAttachmentService.getAttachmentsByProfileId(profileId), OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.getAttachmentsByProfileId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Elimina un attachment asociado a un profile
	 * 
	 * @param urlName
	 * @param id
	 */
	@DeleteMapping("{urlName}/attachments/{attachmentId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAttachmentById(@PathVariable("urlName") String urlName,
			@PathVariable("attachmentId") Long attachmentId) {

		try {

			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			}

			if (Util.isEmpty(attachmentId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
			if (profileDTO == null) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			}

			profileAttachmentService.deleteByIdAndByProfileId(profileDTO.getId(), attachmentId);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.deleteAttachmentById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @param urlFollow
	 */
	@PostMapping("/unfollow/{urlUnFollow}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteFollow(@PathVariable("urlUnFollow") String urlUnFollow, HttpServletRequest request,
			@RequestParam(name = "lang", required = false) String lang) {

		try {

			User user = jwtProvider.getUser(request);

			if (Util.isEmpty(urlUnFollow)) {
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
			}

			profileService.deleteFollow(user, urlUnFollow, lang);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.unFollow]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param idProfile
	 * @param typeFollow
	 */
	@GetMapping("/type/{typeFollow}")
	public ResponseEntity<MutableList<ProfileFollowDTO>> getFollows(
			@RequestParam(name = "lang", required = false) String lang,
			@RequestParam(name = "urlname", required = true) String urlname,
			@RequestParam(name = "page", required = true) Integer page,
			@RequestParam(name = "size", required = true) Integer size, @PathVariable("typeFollow") String typeFollow,
			HttpServletRequest request) {
		if (page < 1) {
			page = PAGE_DEFAULT;
		}

		if (size < 1) {
			size = SIZE_DEFAULT;
		}

		String[] typesFollows = { "followings", "followers" };
		boolean contains = Arrays.stream(typesFollows).anyMatch(typeFollow::equals);

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		log.info(typeFollow);
		log.info("Mensaje ---->" + typeFollow);

		if (!contains) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FOLLOW_TYPE_NOTFOUND, lang)));
		} else {
			return new ResponseEntity<>(profileService.getFollowers(urlname, typeFollow, lang, page, size), OK);
		}
	}

	/**
	 * Crea un Social Account de un Profile
	 * 
	 * @param urlName
	 * @param request
	 * @return
	 */
	@PostMapping("/{urlName}/social-accounts")
	public ResponseEntity<SocialAccountDTO> createSocialAccount(@PathVariable("urlName") String urlName,
			@RequestBody @Valid final SocialAccountCreateDTO request,
			@RequestParam(name = "lang", required = false) String lang) {

		try {
			// VALIDA REQUEST
			MutableList<ApiError> errors = validateRequest(request, lang);
			if (errors.isEmpty()) {
				// BUSCA PROFILE
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
					SocialAccountDTO socialAccount = SocialAccountMapper
							.mapSocialAccount(profileService.createSocialAccount(request, profileDTO.getId()));
					return new ResponseEntity<>(socialAccount, CREATED);
				}

			} else {
				// ERROR EN REQUEST
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.INVALID_REQUEST_DATA.getCode(), ApiErrorEnum.INVALID_REQUEST_DATA.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.createSocialAccount]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	//

	/**
	 * Modifica parcialmente un Social Account de un Profile
	 * 
	 * @param urlName
	 * @param socialAccountId
	 * @param request
	 * @return
	 */
	@PatchMapping("/{urlName}/social-accounts/{socialAccountId}")
	public ResponseEntity<SocialAccountDTO> patchSocialAccount(@PathVariable("urlName") String urlName,
			@PathVariable("socialAccountId") Long socialAccountId, @RequestBody final SocialAccountPatchDTO request) {

		try {
			// BUSCA PROFILE
			ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
			if (profileDTO == null) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			} else {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				SocialAccountDTO socialAccount = SocialAccountMapper.mapSocialAccount(
						profileService.patchSocialAccount(request, profileDTO.getId(), socialAccountId));
				return new ResponseEntity<>(socialAccount, OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.patchSocialAccount]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@GetMapping("/categories")
	public ResponseEntity<MutableList<ProfileCategoryTypeDTO>> readAllCategories(
			@RequestParam(name = "lang", required = false) String lang,
			@RequestParam(name = "pType", required = true) Long pType,
			@RequestParam(name = "level", required = false) Long level,
			@RequestParam(name = "childs", required = true) boolean childs) {

		if (Util.isEmpty(level)) {
			level = ProfileConstant.CATEGORIES_SEARCH_LEVEL_BY_DEFAULT;
		}

		return new ResponseEntity<>(profileCategoryTypeService.findByLevelWithProfileType(level, pType, lang, childs),
				OK);

	}

	@PostMapping("/categories")
	public ResponseEntity<ProfileCategoryType> createCategories(
			@RequestBody final ProfileCategoryTypeRequestDTO request,
			@RequestParam(name = "lang", required = false) String lang) {

		MutableList<ApiError> errors = validateRequest(request, lang);
		if (errors.isEmpty()) {
			return new ResponseEntity<>(profileCategoryTypeService.create(request), CREATED);
		} else {
			throw new InvalidRequestException(errors);
		}

	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/categories/{id}")
	public ResponseEntity<ProfileCategoryType> updateCategories(@PathVariable("id") Long id,
			@RequestBody final ProfileCategoryTypeRequestDTO request) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			return new ResponseEntity<>(profileCategoryTypeService.update(id, request), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.updateCategories]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/categories/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteCategories(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			profileCategoryTypeService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.deleteCategories]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/{urlName}/connections")
	public ResponseEntity<ProfileRequestConnectionDTO> createConnection(@PathVariable("urlName") String urlName,
			@RequestBody @Valid final ProfileRequestConnectionRequestDTO request) {

		try {
			// BUSCA PROFILE
			ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
			if (profileDTO == null) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			} else {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(ProfileRequestConnectionMapper.mapProfileRequestConnection(
						profileRequestConnectionService.create(profileDTO.getId(), request)), CREATED);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.createConnection]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@GetMapping("/{urlName}/connections")
	public ResponseEntity<PaginateResponse<ProfileRequestConnectionDTO>> readConnectionAll(
			@PathVariable("urlName") String urlName, @RequestParam(required = false) Integer pageNumber,
			@RequestParam() Integer pageSize, @RequestParam(required = false) Long profileId,
			@RequestParam(required = false) Long profileRequestStatusId,
			@RequestParam(required = false) Long typeConnectionId,
			@RequestParam(name = "lang", required = false) String lang) {

		try {
			ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, lang));
			if (profileDTO == null) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			} else {
				PaginateResponse<ProfileRequestConnectionDTO> paginateResponse;
				if (!Util.isEmpty(pageNumber)) {
					paginateResponse = profileRequestConnectionService.readAll(pageNumber, pageSize, profileDTO.getId(),
							profileId, profileRequestStatusId, typeConnectionId, lang);
				} else {
					paginateResponse = profileRequestConnectionService.readAll(pageSize, profileDTO.getId(), profileId,
							profileRequestStatusId, typeConnectionId, lang);
				}

				return new ResponseEntity<>(paginateResponse, OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.readConnectionAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PutMapping("/{urlName}/connections/{id}")
	public ResponseEntity<ProfileRequestConnectionDTO> editConnection(@PathVariable("urlName") String urlName,
			@PathVariable("id") Long id, @Valid @RequestBody final ProfileRequestConnectionRequestDTO request) {

		try {

			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			// BUSCA PROFILE
			ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
			if (profileDTO == null) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			} else {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(
						ProfileRequestConnectionMapper.mapProfileRequestConnection(
								profileRequestConnectionService.edit(id, profileDTO.getId(), request)),
						HttpStatus.ACCEPTED);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.editConnection]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(SocialAccountCreateDTO request, String language) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getSocialAppId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_SOCIAL_APP, language));
		}
		if (Util.isEmpty(request.getData())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_DATA, language));
		}

		return apiErrors;
	}

	/**
	 * @param urlName
	 */
	@DeleteMapping("/{urlName}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProfile(@PathVariable("urlName") String urlName) {

		try {
			ProfileDTO profileOwner;
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				profileOwner = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileOwner == null)
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			}
			profileService.deleteProfile(profileOwner.getId());

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.deleteProfile]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param urlName
	 * @param id
	 */
	@PostMapping("/like/{urlProfile}")
	public void createLike(@PathVariable("urlProfile") String urlProfile, HttpServletRequest request,
			@RequestParam(name = "lang", required = false) String lang) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		try {

			User user = jwtProvider.getUser(request);

			if (Util.isEmpty(urlProfile)) {
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
			}

			profileService.createLike(user, urlProfile, lang);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.createLike]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param albumId
	 * @param id
	 */
	@PostMapping("/unlike/{urlProfile}")
	public void deleteLike(@PathVariable("urlProfile") String urlProfile, HttpServletRequest request,
			@RequestParam(name = "lang", required = false) String lang) {

		try {

			User user = jwtProvider.getUser(request);

			if (Util.isEmpty(urlProfile)) {
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
			}

			profileService.deleteLike(user, urlProfile, lang);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.unFollow]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 *
	 * @param urlName
	 */
	@GetMapping("/{urlName}/albums")
	public ResponseEntity<MutableList<AlbumDTO>> getAlbums(@RequestParam(name = "lang", required = false) String lang,
			@RequestParam(name = "albumType", required = true) Long albumType,
			@PathVariable("urlName") final String urlName) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		AlbumType albumTypeObj = new AlbumType();

		if (albumType != null) {

			albumTypeObj = albumTypeService.findById(albumType, lang);

		}

		if (Util.isEmpty(urlName)) {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));

		} else {

			Profile profileDTO = profileService.readByUrl(urlName, lang);

			if (profileDTO == null) {

				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

			} else {
				return new ResponseEntity<>(albumService.findByProfile(profileDTO, albumTypeObj, lang), OK);
			}
		}
	}

	@GetMapping("/block/{urlProfile}")
	public void createBlock(@PathVariable("urlProfile") String urlName, HttpServletRequest request,
			@RequestParam(name = "lang", required = false) String lang) {

		User user = jwtProvider.getUser(request);
		profileService.blockProfile(user, urlName, lang);

	}

	@GetMapping("/unblock/{urlProfile}")
	public void removeBlock(@PathVariable("urlProfile") String urlName, @RequestParam("lang") @NonNull String lang,
			HttpServletRequest request) {

		User user = jwtProvider.getUser(request);
		profileService.unblockProfile(user, urlName, lang);

	}

	/**
	 *
	 * @param urlName
	 */
	@GetMapping("/{urlName}/blocks")
	public ResponseEntity<List<ProfileBlockBasicDTO>> getBlocks(@PathVariable("urlName") final String urlName) {
		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				return new ResponseEntity<>(profileBlockService.getBlocks(urlName), OK);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.getBlocks]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param urlName
	 * @param id
	 */
	@DeleteMapping("/{urlName}/blocks/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteBlock(@PathVariable("urlName") String urlName, @PathVariable("id") Long id) {

		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					profileBlockService.delete(profileDTO.getId(), id);
				}
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.deleteBlock]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param urlName
	 * @param id
	 */
	@PostMapping("/{urlName}/share")
	public ResponseEntity<ProfileShareDTO> createShare(@PathVariable("urlName") final String urlName,
			@RequestBody final ProfileShareCreateDTO request) {
		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(ProfileShareMapper
							.mapEntityToDtoMapper(profileShareService.create(profileDTO.getId(), request)), CREATED);
				}
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.createShare]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param urlName
	 * @param id
	 */
	@PostMapping("/{urlName}/report")
	public ResponseEntity<ProfileReportDTO> createReport(@PathVariable("urlName") final String urlName,
			@RequestBody final ProfileReportCreateDTO request) {
		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(ProfileReportMapper
							.mapEntityToDTO(profileReportService.create(profileDTO.getId(), request)), CREATED);
				}
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.createReport]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param urlName
	 */
	@PostMapping("/{urlName}/recommendations")
	public ResponseEntity<ProfileRecommendationDTO> createRecommendation(@PathVariable("urlName") final String urlName,
			@RequestBody final ProfileRecommendationCreateDTO request) {
		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(ProfileRecommendationMapper
							.mapEntityToDTO(profileRecommendationService.create(profileDTO.getId(), request)), CREATED);
				}
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.createRecommendation]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 *
	 * @param urlName
	 */
	@GetMapping("/{urlName}/recommendations")
	public ResponseEntity<MutableList<ProfileRecommendationDTO>> getRecommendations(
			@RequestParam(name = "lang", required = false) String lang, @PathVariable("urlName") final String urlName) {
		try {

			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, lang));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(
							profileRecommendationService.getListProfileRecommendationByProfileId(profileDTO.getId()),
							OK);
				}
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.getRecommendation]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 *
	 * @param urlName
	 * @param id
	 */
	@PatchMapping("/{urlName}/recommendations/{id}")
	public ResponseEntity<ProfileRecommendationDTO> patchRecommendation(@PathVariable("urlName") final String urlName,
			@PathVariable("id") final Long id, @RequestBody final ProfileRecommendationEditDTO request) {
		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					return new ResponseEntity<>(
							ProfileRecommendationMapper.mapEntityToDTO(
									profileRecommendationService.patch(id, profileDTO.getId(), request)),
							HttpStatus.NO_CONTENT);
				}
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.getRecommendation]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param urlName
	 * @param id
	 */
	@DeleteMapping("/{urlName}/recommendations/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteRecommendation(@PathVariable("urlName") String urlName, @PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(urlName)) {
				throw new InvalidRequestException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.EMPTY_URL_NAME.getCode(), ApiErrorEnum.EMPTY_URL_NAME.getMessage())));
			} else {
				ProfileDTO profileDTO = ProfileMapper.mapProfile(profileService.readByUrl(urlName, null));
				if (profileDTO == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
				} else {
					profileRecommendationService.delete(id, profileDTO.getId());
				}
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileController.deleteRecommendation]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@PatchMapping("/notifications/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchIsReadNotification(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.NOTIFICATION_ID_EMPTY.getCode(),
								ApiErrorEnum.NOTIFICATION_ID_EMPTY.getMessage())));
			}

			notificationService.patchReadNotification(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.updatePassword]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/notifications")
	public ResponseEntity<PaginateResponse<NotificationScrollDTO>> getNotifications(@RequestParam() Integer pageNumber,
			@RequestParam() Integer pageSize) {

		try {
			return new ResponseEntity<>(notificationService.getAllByProfileId(pageNumber, pageSize), OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostController.getPostProfileHome]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}

	}

	@DeleteMapping("/notifications/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteNotification(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.NOTIFICATION_ID_EMPTY.getCode(),
								ApiErrorEnum.NOTIFICATION_ID_EMPTY.getMessage())));
			}

			notificationService.deleteNotificationById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserController.updatePassword]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/upload-book")
	public ResponseEntity<AlbumDTO> updoadBook(@RequestPart(required = false) MultipartFile cover,
			@RequestPart(required = true) MultipartFile[] photos, @RequestParam(required = true) Long profileId,
			@RequestParam(required = true) String title, @RequestParam(required = false) String description,
			@RequestParam(required = true) Boolean isPrivate,
			@RequestParam(name = "lang", required = false) String lang) {

		Album album = null;

		if (Util.isEmpty(profileId)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_ID, lang)));
		}

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		MultipartFile[] toUpload = fileUtils.validateFileAttach(photos);

		if (toUpload.length > 0) {

			album = profileService.updoadBook(profileId, photos, cover, title, description, isPrivate, lang);
			return new ResponseEntity<>(AlbumMapper.mapAlbum(album), CREATED);

		} else {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, lang)));
		}

	}

	@SuppressWarnings("null")
	@PostMapping("/upload-video")
	public ResponseEntity<ProfileAttachmentDTO> updoadVideo(@RequestPart(required = false) MultipartFile video,
			@RequestParam(required = true) Long profileId, @RequestParam(required = true) String title,
			@RequestParam(required = false) String description, @RequestParam(required = false) String videoLink,
			@RequestParam(required = true) Boolean isPrivate,
			@RequestParam(name = "lang", required = false) String lang) {

		ProfileAttachment profileAttachment = null;

		if (Util.isEmpty(profileId)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_ID, lang)));
		}

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		if (video != null && video.getSize() > 0 && (videoLink == null || Util.isEmpty(videoLink))) {

			profileAttachment = profileService.uploadVideo(profileId, video, null, title, description, isPrivate, lang);

		} else if (!Util.isEmpty(videoLink) && (video == null || video.getSize() == 0)) {

			profileAttachment = profileService.uploadVideoURL(profileId, videoLink, null, title, description, isPrivate,
					lang);

		} else {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, lang)));
		}

		return new ResponseEntity<>(ProfileAttachmentMapper.mapProfileAttachment(profileAttachment), CREATED);

	}

	@PostMapping("/upload-work")
	public ResponseEntity<ProfileWorkDTO> updoadWork(@RequestBody final ProfileWorkDTO request,
			@RequestParam(name = "lang", required = false) String lang) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		MutableList<ApiError> errors = validateProfileWorkRequest(request, lang);

		if (errors.isEmpty()) {

			return new ResponseEntity<>(profileService.uploadWork(request, lang), CREATED);

		} else {
			throw new InvalidRequestException(errors);
		}

	}

	@PostMapping("/upload-audio")
	public ResponseEntity<AlbumDTO> updoadAudioAlbum(@RequestPart(required = false) MultipartFile cover,
			@RequestParam(required = true) Long profileId, @RequestParam(required = true) String albumTitle,
			@RequestParam(required = false) String albumDescription, @RequestParam(required = true) Boolean isPrivate,
			@RequestParam(name = "lang", required = false) String lang) {

		Album album = null;

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		album = profileService.updoadAudioAlbum(profileId, cover, albumTitle, albumDescription, isPrivate, lang);
		return new ResponseEntity<>(AlbumMapper.mapAlbum(album), CREATED);
	}

	/**
	 * @param like
	 * @return
	 */
	@GetMapping("/findbyname/{like}/")
	public ResponseEntity<MutableList<ProfileBasicDTO>> getProfileByNameLike(
			@PathVariable(name = "like", required = true) String name,
			@RequestParam(name = "lang", required = false) String lang) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		if (Util.isEmpty(name)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_NAME, lang)));
		} else {
			return new ResponseEntity<>(profileService.readByNameLike(name, lang), OK);
		}
	}

	private MutableList<ApiError> validateRequest(ProfileCategoryTypeRequestDTO request, String language) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getProfileTypeId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_TYPE_ID, language));
		}

		return apiErrors;
	}

	private MutableList<ApiError> validateProfileWorkRequest(ProfileWorkDTO request, String language) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_ID, language));
		}

		return apiErrors;
	}

	@GetMapping("/{urlName}/posts")
	public ResponseEntity<MutableList<PostDTO>> getPosts(@RequestParam(name = "lang", required = false) String lang,
			@PathVariable("urlName") final String urlName, @RequestParam() Integer pageNumber,
			@RequestParam() Integer pageSize, HttpServletRequest request) {

		if (Util.isEmpty(lang)) {
			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		if (Util.isEmpty(urlName)) 
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));
		
		return new ResponseEntity<>(postService.findAllByProfile(request, urlName, pageNumber, pageSize, lang),OK);
		
	}

	@GetMapping("/{urlName}/works")
	public ResponseEntity<MutableList<ProfileWorkDTO>> getWorks(
			@RequestParam(name = "lang", required = false) String lang, @PathVariable("urlName") final String urlName) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		if (Util.isEmpty(urlName)) {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_URL_NAME, lang)));

		} else {

			Profile profileDTO = profileService.readByUrl(urlName, lang);

			if (profileDTO == null) {

				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

			} else {
				return new ResponseEntity<>(profileWorkService.findByProfile(profileDTO, lang), OK);
			}
		}
	}

	@PostMapping("/upload-audiofile")
	public ResponseEntity<AlbumDTO> updoadAudioFile(@RequestPart(required = false) MultipartFile cover,
			@RequestPart(required = true) MultipartFile audio, @RequestParam(required = true) Long albumId,
			@RequestParam(required = false) String audioTitle, @RequestParam(required = false) String audioDescription,
			@RequestParam(required = true) Boolean isPrivate,
			@RequestParam(name = "lang", required = false) String lang) {

		Album album = null;

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		Map<String, MultipartFile> toUpload = fileUtils.validateFileAttach(audio, cover);

		String metadata = fileUtils.createMetadata(audioTitle, audioDescription);

		if (toUpload.size() > 0) {

			album = profileService.updoadAudioFile(albumId, audio, cover, metadata, isPrivate, lang);
			return new ResponseEntity<>(AlbumMapper.mapAlbum(album), CREATED);

		} else {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_INVALID, lang)));
		}

	}

	@PostMapping("/save-filters")
	public void saveFilters(@RequestBody(required = true) final List<ProfileFilterValueDTO> filters,
			@RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		try {

			User user = jwtProvider.getUser(request);
			profileService.saveFilters(user, filters, lang);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfilController.saveFilters]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping("/update-images")
	public ResponseEntity<ProfileBasicDTO> initRecognition(
			@RequestPart(value = "file", required = true) MultipartFile photo,
			@RequestPart(value = "imageType", required = true) String imageType,
			@RequestParam(required = false) String lang, HttpServletRequest request) {

		String[] typesFollows = { "cover", "avatar" };
		boolean contains = Arrays.stream(typesFollows).anyMatch(imageType::equals);

		if (Util.isEmpty(lang)) {

			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}

		log.info(imageType);
		log.info("Mensaje ---->" + imageType);

		if (!contains) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FOLLOW_TYPE_NOTFOUND, lang)));
		} else {

			User user = jwtProvider.getUser(request);
			return new ResponseEntity<>(profileService.updateImagesProfile(user, photo, imageType, lang), OK);
		}

	}

}
