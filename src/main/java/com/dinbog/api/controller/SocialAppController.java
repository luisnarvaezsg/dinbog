package com.dinbog.api.controller;

import com.dinbog.api.dto.SocialAppCreateDTO;
import com.dinbog.api.dto.SocialAppDTO;
import com.dinbog.api.service.ISocialAppService;
import com.dinbog.api.util.mapper.SocialAppMapper;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/socialapps")
@CrossOrigin(allowCredentials = "true")
public class SocialAppController {

	@Autowired
	private ISocialAppService socialAppService;

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<SocialAppDTO>> readAll() {

		try {

			return new ResponseEntity<>(socialAppService.readAll(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<SocialAppDTO> create(@RequestBody final SocialAppCreateDTO request) {

		try {
			MutableList<ApiError> errors = validateRequest(request);
			if (errors.isEmpty()) {
				return new ResponseEntity<>(SocialAppMapper.mapSocialApp(socialAppService.create(request)), HttpStatus.CREATED);
			} else {
				throw new InvalidRequestException(errors);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 */
	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchById(@PathVariable("id") final Long id, @RequestBody final SocialAppCreateDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_SOCIAL_APP_ID.getCode(), ApiErrorEnum.EMPTY_SOCIAL_APP_ID.getMessage())));
			}

			socialAppService.patch(id, request);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppController.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") final Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_SOCIAL_APP_ID.getCode(), ApiErrorEnum.EMPTY_SOCIAL_APP_ID.getMessage())));
			}

			socialAppService.delete(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppController.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(SocialAppCreateDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getActive())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SOCIAL_IS_ACTIVE.getCode(),
					ApiErrorEnum.EMPTY_SOCIAL_IS_ACTIVE.getMessage()));
		}

		if (Util.isEmpty(request.getClientId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SOCIAL_APP_CLIENT_ID.getCode(),
					ApiErrorEnum.EMPTY_SOCIAL_APP_CLIENT_ID.getMessage()));
		}

		if (Util.isEmpty(request.getDomain())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SOCIAL_APP_DOMAIN.getCode(),
					ApiErrorEnum.EMPTY_SOCIAL_APP_DOMAIN.getMessage()));
		}

		if (Util.isEmpty(request.getKey())) {
			apiErrors.add(
					new ApiError(ApiErrorEnum.EMPTY_SOCIAL_KEY.getCode(), ApiErrorEnum.EMPTY_SOCIAL_KEY.getMessage()));
		}

		if (Util.isEmpty(request.getName())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SOCIAL_NAME.getCode(),
					ApiErrorEnum.EMPTY_SOCIAL_NAME.getMessage()));
		}

		if (Util.isEmpty(request.getSecret())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_SOCIAL_SECRET.getCode(),
					ApiErrorEnum.EMPTY_SOCIAL_SECRET.getMessage()));
		}

		return apiErrors;
	}
}
