package com.dinbog.api.controller;

import com.dinbog.api.dto.CommentsPostDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.*;
import com.dinbog.api.util.Util;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/v1/commentspost")
@CrossOrigin(allowCredentials = "true")
public class CommentsPostController {

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private IPostService postService;

	@Autowired
	private ICommentService iCommentService;

	@Autowired
	private IApiErrorService errorService;

	@PostMapping("/{id}/")
	@ResponseStatus(CREATED)
	public void createComment(@PathVariable("id") String id, @RequestParam("comment") String comment,
			@RequestParam("lang") String lang, HttpServletRequest request) {

		if (Util.isEmpty(id) || !(Util.isInteger(id))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_COMMENT_ID, lang)));
		}
		if (Util.isEmpty(comment)) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang)));
		}
		iCommentService.create(Long.parseLong(id), comment, jwtProvider.getUser(request), lang);
	}

	@PostMapping("/{id}/like")
	@ResponseStatus(CREATED)
	public void commentLike(@PathVariable("id") String id, @RequestParam("lang") String lang,
			HttpServletRequest request) {

		if (Util.isEmpty(id) || !(Util.isInteger(id))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_COMMENT_ID, lang)));
		}

		iCommentService.commentLike(Long.parseLong(id), jwtProvider.getUser(request), lang);
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}/dislike")
	@ResponseStatus(HttpStatus.OK)
	public void deleteCommentLikeById(@PathVariable(value = "id") Long id, @RequestParam("lang") String lang,
			HttpServletRequest request) {

		if (Util.isEmpty(id) || !(Util.isInteger(id))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_COMMENT_ID, lang)));
		}

		iCommentService.commentDisLike(id, lang);
	}

	@GetMapping("/get/{id}")
	@ResponseBody
	public ResponseEntity<String> getPost(@PathVariable("id") String id, @RequestParam("lang") String lang,
			HttpServletRequest request) {

		String url = postService.getPostURL(id);
		return new ResponseEntity<>(url, HttpStatus.OK);
	}

	// http://52.207.106.153:8083/api-1.0/v1/commentspost/100/comments/?lang=ES&pageNumber=0&pageSize=10
	@GetMapping("/{id}/comments")
	@ResponseBody
	public ResponseEntity<MutableList<CommentsPostDTO>> commentsByPost(@RequestParam("pageNumber") Integer pageNumber,
			@RequestParam("pageSize") Integer pageSize, @PathVariable("id") Long id, @RequestParam("lang") String lang,
			HttpServletRequest request) {

		if (Util.isEmpty(id) || !(Util.isInteger(id))) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_COMMENT_ID, lang)));
		}
		return new ResponseEntity<>(iCommentService.commentsByPost(id, request, lang, pageNumber, pageSize),
				HttpStatus.OK);
	}
}
