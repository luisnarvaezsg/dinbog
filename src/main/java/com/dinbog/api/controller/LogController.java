/**
 * 
 */
package com.dinbog.api.controller;

import javax.validation.Valid;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.LogCreateDTO;
import com.dinbog.api.dto.LogDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Log;
import com.dinbog.api.service.ILogService;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 *
 */
@Slf4j
@RestController
@RequestMapping("/v1/logs")
@CrossOrigin(allowCredentials = "true")
public class LogController {

	@Autowired
	private ILogService logService;

	/**
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @param userId
	 * @param message
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<PaginateResponse<LogDTO>> readAll(@RequestParam("") Integer pageNumber,
			@RequestParam("") Integer pageSize, @RequestParam(required = false) Long userId,
			@RequestParam(required = false) String message) {

		try {
			return new ResponseEntity<>(logService.readAll(pageNumber, pageSize, userId, message), HttpStatus.OK);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LogController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Crea un User (Proceso de registro)
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<Log> create(@RequestBody @Valid final LogCreateDTO request) {

		try {
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(logService.registraLog(request), HttpStatus.CREATED);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error en request EN [LogController.create]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LogController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	private MutableList<ApiError> validaRequest(LogCreateDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		// USER
		if (Util.isEmpty(request.getUserId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_USER_ID.getCode(), ApiErrorEnum.EMPTY_USER_ID.getMessage()));
		}
		// MESSAGE
		if (Util.isEmpty(request.getMessage())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_MESSAGE.getCode(), ApiErrorEnum.EMPTY_MESSAGE.getMessage()));
		}
		// IS ERROR
		if (Util.isEmpty(request.getIsError())) {
			apiErrors
					.add(new ApiError(ApiErrorEnum.EMPTY_IS_ERROR.getCode(), ApiErrorEnum.EMPTY_IS_ERROR.getMessage()));
		}
		// IS WARNING
		if (Util.isEmpty(request.getIsWarning())) {
			apiErrors.add(
					new ApiError(ApiErrorEnum.EMPTY_IS_WARNING.getCode(), ApiErrorEnum.EMPTY_IS_WARNING.getMessage()));
		}

		return apiErrors;
	}
}
