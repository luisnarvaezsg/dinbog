package com.dinbog.api.controller;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.security.jwt.JwtData;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.IUserService;
import com.dinbog.api.service.UserServiceImpl;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.template.BaseTemplates;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Slf4j
@Controller
@RequestMapping("/v1/password")
@CrossOrigin(allowCredentials = "true")
public class ResetPasswordController {


   private IUserService userService;
   private JwtProvider jwtTokenProvider;

   @Autowired
   public ResetPasswordController(UserServiceImpl userService,JwtProvider jwtTokenProvider ) {
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @GetMapping("/confirm-password/{token}")
    public String resetPasswordRequest(@PathVariable("token") String token){
       try {
           JwtData data = jwtTokenProvider.getJWTData(token);
           User user = userService.findByEmail(data.getEmail());
           if (Util.isEmpty(user) || userService.findUserByIdAndPasswordNull(data.getUserId()).isEmpty()) {

               return BaseTemplates.BAD_REQUEST.getLayer();
           }
           return BaseTemplates.NEW_PASSWORD.getLayer();
       }catch (Exception e){
           if (Util.isCustomException(e))
               throw e;
           log.error("*** Exception EN [UserController.resetPassword]");
           Util.printLogError(e);
           throw new InternalErrorException(e);
       }
    }

}
