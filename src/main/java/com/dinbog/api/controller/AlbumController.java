package com.dinbog.api.controller;

import com.dinbog.api.dto.*;
import com.dinbog.api.exception.*;
import com.dinbog.api.models.entity.*;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.service.*;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.files.FileUtils;
import com.dinbog.api.util.mapper.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/v1/albums")
@CrossOrigin(allowCredentials = "true")
@Slf4j
public class AlbumController {

	@Autowired
	private IAlbumService albumService;

	@Autowired
	private IAlbumCommentService albumCommentService;

	@Autowired
	private IAlbumLikeService albumLikeService;

	@Autowired
	private IAlbumAttachmentService albumAttachmentService;

	@Autowired
	private IAlbumShareService albumShareService;

	@Autowired
	private IAlbumReportService albumReportService;

	@Autowired
	private FileUtils fileUtils;

	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private IProfileService profileService;

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param albumTypeId
	 * @param search
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<PaginateResponse<AlbumDTO>> readAll(
			@RequestParam() Integer pageNumber,
			@RequestParam() Integer pageSize, @RequestParam() Long albumTypeId,
			@RequestParam(name = "lang", required = false) String lang,
			@RequestParam(required = false) String search) {
		try {
			return new ResponseEntity<>(albumService.readAll(pageNumber, pageSize, albumTypeId, search, lang), OK);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<AlbumDTO> createAlbum(@RequestBody final AlbumRequestDTO request) {

		try {
			MutableList<ApiError> errors = validateRequest(request);
			if (!errors.isEmpty()) {
				throw new InvalidRequestException(errors);
			} else {
				Album album = albumService.create(request);
				return new ResponseEntity<>(AlbumMapper.mapAlbum(album), HttpStatus.CREATED);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.createAlbum]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/{id}")
	public ResponseEntity<AlbumDTO> updateAlbum(@PathVariable("id") Long id,
			@RequestBody final AlbumRequestDTO request) {

		try {

			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(), ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			MutableList<ApiError> errors = validateRequest(request);
			if (!errors.isEmpty()) {
				throw new InvalidRequestException(errors);
			} else {
				Album album=albumService.update(id, request);
				return new ResponseEntity<>(AlbumMapper.mapAlbum(album), HttpStatus.ACCEPTED);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.updateAlbum]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(NO_CONTENT)
	public void deleteAlbum(@PathVariable("id") Long id) {
		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(), ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			} else {
				albumService.deleteById(id);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.deleteAlbum]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public ResponseEntity<AlbumDTO> getById(
			@RequestParam(name = "lang", required = false) String lang,
			@PathVariable(value = "id") Long id) {
		
		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
						ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())));
			} else {
				Album album=albumService.readById(id, lang);
				return new ResponseEntity<>(AlbumMapper.mapAlbum(album), OK);
			}
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.getById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	/**
	 * @param albumId
	 * @param id
	 */
	@DeleteMapping("/{albumId}/comments/{id}")
	@ResponseStatus(NO_CONTENT)
	public void deleteComment(@PathVariable("albumId") Long albumId, @PathVariable("id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			} else {
				if (albumService.readById(albumId, null) == null) {
					throw new InvalidRequestException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.ALBUM_NOT_FOUND.getCode(), ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())));
				} else {
					albumCommentService.deleteByAlbumIdAndById(albumId, id);
				}
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.deleteComment]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	@PostMapping("/{albumId}/attachments")
	public ResponseEntity<AlbumAttachmentDTO> createAttachment(@PathVariable("albumId") Long albumId,
			@RequestBody final AlbumAttachmentRequestDTO request) {

		try {

			MutableList<ApiError> errors = validateRequest(request);
			if (Util.isEmpty(albumId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			} else if (!errors.isEmpty()) {
				throw new InvalidRequestException(errors);
			} else {
				AlbumAttachment albumAttachment=albumAttachmentService.create(albumId, request);
				return new ResponseEntity<>(AlbumAttachmentMapper.mapAlbumAttachment(albumAttachment), HttpStatus.CREATED);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.createAttachment]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	@DeleteMapping("/{albumId}/attachments")
	@ResponseStatus(NO_CONTENT)
	public void deleteAttachment(@PathVariable("albumId") Long albumId) {

		try {
			if (Util.isEmpty(albumId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			} else {
				albumAttachmentService.deleteByAlbumId(albumId);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.deleteAttachment]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	@DeleteMapping("/{albumId}/likes/{id}")
	@ResponseStatus(NO_CONTENT)
	public void deleteLike(@PathVariable("albumId") Long albumId, @PathVariable("id") Long likeId) {

		try {
			if (Util.isEmpty(albumId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			} else if (Util.isEmpty(likeId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.ALBUM_LIKE_ID_EMPTY.getCode(), ApiErrorEnum.ALBUM_LIKE_ID_EMPTY.getMessage())));
			} else {
				albumLikeService.deleteByAlbumIdAndById(albumId, likeId);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.deleteLike]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(AlbumRequestDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_VALUE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_VALUE.getMessage()));
		}
		if (Util.isEmpty(request.getAlbumTypeId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_VALUE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_VALUE.getMessage()));
		}
		return apiErrors;
	}

		/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(AlbumShareCreateDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_VALUE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_VALUE.getMessage()));
		}
		if (Util.isEmpty(request.getAlbumId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_VALUE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_VALUE.getMessage()));
		}
		return apiErrors;
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(AlbumReportCreateDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_VALUE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_VALUE.getMessage()));
		}
		if (Util.isEmpty(request.getReportTypeId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
					ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage()));
		}
		if (Util.isEmpty(request.getDetail())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_VALUE.getCode(),
					ApiErrorEnum.EMPTY_GLOBAL_VALUE.getMessage()));
		}
		return apiErrors;
	}

	/**
	 * @param request
	 * @return
	 */
	@PostMapping(value = "/share", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AlbumShareDTO> createShare(@NonNull @RequestBody final AlbumShareCreateDTO request) {
		try {
			MutableList<ApiError> errors = validateRequest(request);
			if (!errors.isEmpty()) {
				throw new InvalidRequestException(errors);
			} else {
				AlbumShare albumShare = albumShareService.create(request);
				return new ResponseEntity<>(AlbumShareMapper.mapperEntityToDTO(albumShare), HttpStatus.CREATED);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.createShare]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping(value = "/{albumId}/report", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AlbumReportDTO> createReport(@NonNull @PathVariable final Long albumId,
			@NonNull @RequestBody final AlbumReportCreateDTO request) {
		try {
			if (Util.isEmpty(albumId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
						ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())));
			}
			MutableList<ApiError> errors = validateRequest(request);
			if (!errors.isEmpty()) {
				throw new InvalidRequestException(errors);
			} else {
				AlbumReport albumReport=albumReportService.create(albumId, request);
				return new ResponseEntity<>( AlbumReportMapper.mapEntityToDTO(albumReport), HttpStatus.CREATED);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumController.createShare]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	private MutableList<ApiError> validateRequest(AlbumAttachmentRequestDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getIsCover())) {
			apiErrors.add(
					new ApiError(
							ApiErrorEnum.EMPTY_IS_COVER.getCode(),
							ApiErrorEnum.EMPTY_IS_COVER.getMessage()
					)
			);
		}
		return apiErrors;
	}


	@PostMapping("/{id}/attach-file")
	@ResponseStatus(CREATED)
	public void uploadFileCover(
			@RequestPart("file") @NonNull MultipartFile[] file,
			@RequestPart("fileCover") MultipartFile fileCover,
			@PathVariable("id") @NonNull Long id,
			@RequestParam("lang") @NonNull String lang
	) {
		Optional<Album> album = albumService.findById(id);
        if (album.isPresent()) {
            MultipartFile[] toUpload = fileUtils.validateFileAttachAny(file);
			if ( toUpload.length > 0 && fileUtils.isValidAlbumAttach(file,fileCover,album.get().getAlbumType().getName()) ) {
                    albumService.uploadFileCover(toUpload, fileCover,album.get(), lang);
			} else {
				throw new InvalidRequestException(
						Lists.mutable.of(
                                errorService.getApiError(ApiErrorEnum.INVALID_ALBUM_ATTACH_FILE, lang)
						)
				);
			}
		} else {
			throw new InvalidRequestException(
					Lists.mutable.of(
					        errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang)
					)
			);
		}
	}

	@PutMapping("/{albumId}/add-like")
	@ResponseStatus(CREATED)
	public void createLike(
			@PathVariable("albumId") @NonNull Long albumId,
			@RequestParam("lang") String lang,
			HttpServletRequest request
	) {
		Optional<Album> album = albumService.findById(albumId);
		Optional<Profile> profile = profileService.findByUser(jwtProvider.getUser(request));
		if (album.isPresent() && profile.isPresent()) {
			Optional<AlbumLike> gotLike = albumLikeService.findLikeByProfileId(album.get(), profile.get());
			if(gotLike.isEmpty()){
				albumLikeService.create(album.get(),profile.get());
			}else{
				throw new ApiExceptionResponse(
						Lists.mutable.of(
								errorService.getApiError(ApiErrorEnum.EXISTING_ALBUM_LIKE, lang)
						)
				);
			}
		} else {
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang)
					)
			);
		}
	}

	@DeleteMapping("/{albumId}/remove-like")
	@ResponseStatus(OK)
	public void createDislike(
			@PathVariable("albumId") Long albumId,
			@RequestParam("lang") String lang,
			HttpServletRequest request
	) {
		Optional<Album> album = albumService.findById(albumId);
		Optional<Profile> profile = profileService.findByUser(jwtProvider.getUser(request));
		if (album.isPresent() && profile.isPresent()) {
			Optional<AlbumLike> gotLike = albumLikeService.findLikeByProfileId(album.get(), profile.get());
			if(gotLike.isPresent()){
				albumLikeService.remove(gotLike.get());
			}else{
				throw new ApiExceptionResponse(
						Lists.mutable.of(
								errorService.getApiError(ApiErrorEnum.ALBUM_LIKE_NOT_FOUND, lang)
						)
				);
			}
		} else {
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang)
					)
			);
		}
	}


	@PostMapping("/add-comment")
	@ResponseStatus(CREATED)
	public void createComment(@RequestBody final AlbumCommentRequestDTO albumComment,@RequestParam("lang") String lang) {

		MutableList<ApiError> errors = albumCommentService.validateModel(albumComment, lang);

		if (!errors.isEmpty()) throw new InvalidRequestException(errors);

		Optional<Album> album = albumService.findById(albumComment.getAlbumId());
		Optional<Profile> profile = profileService.findById(albumComment.getProfileId());

		if(album.isPresent() && profile.isPresent()){
			 albumCommentService.create(album.get(),profile.get(),albumComment);
		}else{
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang))
			);
		}

	}

	@DeleteMapping("/{commentAlbumId}/remove-comment")
	@ResponseStatus(OK)
	public void removeComment(
			@PathVariable("commentAlbumId") @NonNull Long commentsAlbumId,
			@RequestParam("lang") @NonNull String lang,
			HttpServletRequest request
	) {
		Optional<AlbumComment> albumComment = albumCommentService.getAlbumCommentById(commentsAlbumId);
		if (albumComment.isEmpty()) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang))
			);
		}

		albumCommentService.delete(albumComment.get());
	}

	@DeleteMapping("/{albumId}/delete")
	@ResponseStatus(OK)
	public void deleteAlbum(
			@PathVariable("albumId") @NonNull Long albumId,
			@RequestParam("lang") @NonNull String lang
	) {
		Optional<Album> album = albumService.findById(albumId);
		if (album.isEmpty()) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang))
			);
		}

		albumService.delete(album.get());
	}

	@DeleteMapping("/{albumAttachmentId}/remove-attachment")
	@ResponseStatus(OK)
	public void removeAlbumAttachment(
			@PathVariable("albumAttachmentId") @NonNull Long albumAttachmentId,
			@RequestParam("lang") @NonNull String lang
	) {
		Optional<AlbumAttachment> albumAttachment = albumAttachmentService.findById(albumAttachmentId);
		if (albumAttachment.isEmpty()) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND, lang))
			);
		}

		albumAttachmentService.delete(albumAttachment.get());

	}

	@PostMapping("/{albumId}/edit")
	@ResponseStatus(OK)
	public void editAlbum(
			@PathVariable("albumId") @NonNull Long albumId,
			@RequestBody final AlbumEditRequestDTO albumEditRequestDTO,
			@RequestParam("lang") String lang
	){
		MutableList<ApiError> errors = albumService.validateEditRequest(albumEditRequestDTO,lang);

		if(!errors.isEmpty()) throw new InvalidRequestException(errors);

		Optional<Album> album = albumService.findById(albumId);

		if(album.isEmpty()){
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang))
			);
		}

		albumService.updateAlbum(album.get(), albumEditRequestDTO);
	}
}
