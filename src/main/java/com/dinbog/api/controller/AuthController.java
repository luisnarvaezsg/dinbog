package com.dinbog.api.controller;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.dto.LoginDTO;
import com.dinbog.api.dto.LoginResponseDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.service.IApiErrorService;
import com.dinbog.api.service.IAuthService;
import com.dinbog.api.service.IUserService;
import com.dinbog.api.util.Util;


@RestController
@RequestMapping("/v1/auth")
@CrossOrigin(allowCredentials = "true")
public class AuthController {

	@Autowired
	private IAuthService authService;
	
	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private IUserService userService;


	/**
	 * @param request
	 * @return
	 */
	@PostMapping("/login")
	public ResponseEntity<LoginResponseDTO> login(@RequestBody final LoginDTO request,
												  @RequestParam(name="lang", required = false) String lang) {

			MutableList<ApiError> errors = validaLoginRequest(request, lang);
			if (errors.isEmpty()) {
				
				if(Util.isEmpty(lang)) {
					lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
				}

				return new ResponseEntity<>(authService.authenticateUser(request, lang), HttpStatus.OK);
			
			} else {
				throw new InvalidRequestException(errors);
			}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaLoginRequest(LoginDTO request, String lang) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getEmail())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_EMAIL, lang));
		}

		if (Util.isEmpty(request.getPassword())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PASSWORD, lang));
		}

		if(userService.findUserByIdAndPasswordNull(userService.findByEmail(request.getEmail()).getId()).isPresent()){
			apiErrors.add(errorService.getApiError(ApiErrorEnum.PASSWORD_ON_RECOVERY_TASK, lang));
		}

		return apiErrors;
	}
}
