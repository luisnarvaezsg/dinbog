package com.dinbog.api.controller;

import com.dinbog.api.service.IGenderService;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.GenderCreateDTO;
import com.dinbog.api.dto.GenderDTO;
import com.dinbog.api.dto.GenderEditDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Gender;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/gender")
@CrossOrigin(allowCredentials = "true")
public class GenderController {

	@Autowired
	private IGenderService genderService;

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<GenderDTO>> readAll(@RequestParam(name = "lang", required = false) String lang) {

		return new ResponseEntity<>(genderService.readAll(lang), HttpStatus.OK);
	
	}

	@PostMapping("")
	public ResponseEntity<Gender> create(@RequestBody final GenderCreateDTO request) {

		return new ResponseEntity<>(genderService.create(request), HttpStatus.CREATED);

	}
	

	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchById(@PathVariable("id") final Long id, @RequestBody final GenderEditDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GENDER_ID.getCode(), ApiErrorEnum.EMPTY_GENDER_ID.getMessage())));
			} else if (Util.isEmpty(request.getValue())) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GENDER_VALUE.getCode(), ApiErrorEnum.EMPTY_GENDER_VALUE.getMessage())));
			}
			genderService.patchById(id, request);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GenderController.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable("id") final Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMPTY_GENDER_ID.getCode(), ApiErrorEnum.EMPTY_GENDER_ID.getMessage())));
			}

			genderService.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GenderController.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
