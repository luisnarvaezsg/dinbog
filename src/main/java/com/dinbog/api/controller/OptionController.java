package com.dinbog.api.controller;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.OptionDTO;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.service.IOptionService;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/options")
@CrossOrigin(allowCredentials = "true")
public class OptionController {

	@Autowired
	private IOptionService optionService;

	/**
	 * @param fieldId
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<OptionDTO>> readAll(@RequestParam(required = false) Long fieldId) {

		try {
			return new ResponseEntity<>(optionService.readAll(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [OptionController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
