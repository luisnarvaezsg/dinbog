package com.dinbog.api.controller;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dinbog.api.dto.LanguageDTO;
import com.dinbog.api.dto.LanguageRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Language;
import com.dinbog.api.service.ILanguageService;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/languages")
@CrossOrigin(allowCredentials = "true")
public class LanguageController {

	@Autowired
	private ILanguageService languageService;

	/**
	 * @param request
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<Language> create(@RequestBody final LanguageRequestDTO request) {

		try {
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(languageService.create(request), HttpStatus.CREATED);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error validando el request en [LanguageController.create]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<MutableList<LanguageDTO>> read() {

		try {
			return new ResponseEntity<>(languageService.read(), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageController.readAll]");
			Util.printLogError(e);
			throw e;
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@PutMapping("/{id}")
	public ResponseEntity<Language> update(@PathVariable(value = "id") Long id,
			@RequestBody final LanguageRequestDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			// VALIDA REQUEST
			MutableList<ApiError> errors = validaRequest(request);
			if (errors.isEmpty()) {
				// LLAMADA A CLASE SERVICE Y RETORNA RESPUESTA
				return new ResponseEntity<>(languageService.update(id, request), HttpStatus.OK);
			} else {
				// ERROR EN REQUEST
				InvalidRequestException e = new InvalidRequestException(errors);
				log.error("*** Error validando el request en [LanguageController.update]");
				Util.printLogError(e);
				throw e;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageController.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}

			languageService.delete(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageController.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaRequest(LanguageRequestDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		// VALUE
		if (Util.isEmpty(request.getName())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_NAME.getCode(), ApiErrorEnum.EMPTY_NAME.getMessage()));
		}
		
		return apiErrors;
	}
}
