package com.dinbog.api.controller;

import java.util.Objects;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dinbog.api.aws.AWSBuckets;
import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.dto.AttachmentCommentDTO;
import com.dinbog.api.dto.AttachmentCommentEditDTO;
import com.dinbog.api.dto.AttachmentCommentResponseDTO;
import com.dinbog.api.dto.AttachmentDTO;
import com.dinbog.api.dto.AttachmentLikeDTO;
import com.dinbog.api.dto.AttachmentReportDTO;
import com.dinbog.api.dto.AttachmentReportRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentComment;
import com.dinbog.api.models.entity.AttachmentReport;
import com.dinbog.api.service.IApiErrorService;
import com.dinbog.api.service.IAttachmentCommentService;
import com.dinbog.api.service.IAttachmentService;
import com.dinbog.api.service.ICommentAttachmentService;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AttachmentCommentMapper;
import com.dinbog.api.util.mapper.AttachmentMapper;
import com.dinbog.api.util.mapper.AttachmentReportMapper;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/attachments")
@CrossOrigin(allowCredentials = "true")
public class AttachmentController {

	@Autowired
	private IAttachmentService attachmentService;

	@Autowired
	private IAttachmentCommentService attachmentCommentService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private ICommentAttachmentService commentAttachmentService ;

	/**
	 * @param file
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<AttachmentDTO> create(@RequestParam("file") MultipartFile file) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();
		try {
			String oldPath = file.getOriginalFilename();
			Optional<String> oOldPath = Optional.of(oldPath);
			Optional<String> typeFile = Optional.empty();
			
			if(!oOldPath.isEmpty()) {
				String[] spl = oOldPath.get().split("\\.");
				typeFile = Optional.of(spl[1]);
			}
			String param;
			boolean isExtensionImage = typeFile.get().matches("(jpe?g|png|gif)$");
			boolean isExtensionVideo = typeFile.get().matches("(avi|mp4)$");
			boolean isExtensionAudio = typeFile.get().matches("(mp3)$");

			log.info("is image: " + isExtensionImage);
			if (isExtensionImage) {
				param = "image";
			} else if (isExtensionVideo) {
				param = "video";
			} else if (isExtensionAudio) {
				param = "audio";
			} else {
				param = null;
			}

			switch (Objects.requireNonNull(param)) {
			case "image":
			case "video":
			case "audio":
				break;
			default:
				apiErrors
						.add(new ApiError(ApiErrorEnum.FILE_INVALID.getCode(), ApiErrorEnum.FILE_INVALID.getMessage()));
				throw new InvalidRequestException(apiErrors);
			}
			Attachment attachment=attachmentService.create(file, null,null, "EN",AWSBuckets.POSTS.getBucket());
			return new ResponseEntity<>(AttachmentMapper.mapAttachment(attachment), HttpStatus.CREATED);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentController.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param attachmentId
	 * @param request
	 */
	@PostMapping("/like")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void createLike(@RequestBody final AttachmentLikeDTO request,
						 @RequestParam(value = "lang") String lang) {

		if(Util.isEmpty(lang)) {
			
			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		
		MutableList<ApiError> errors = validateAttachmentLikeRequest(request, lang);
		
		if (errors.isEmpty()) {	

			attachmentService.createLike(request, lang);

		} else {
			
			throw new InvalidRequestException(errors);
		}
	}

	/**
	 * @param attachmentId
	 * @param request
	 * @return
	 */
	@PostMapping("/add-comment")
	public ResponseEntity<AttachmentCommentResponseDTO> addComment(@RequestBody final AttachmentCommentDTO request,
			 													   @RequestParam(value = "lang") String lang) {

		if(Util.isEmpty(lang)) {
				
			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		
		MutableList<ApiError> errors = validateRequest(request, lang);
		
		if (errors.isEmpty()) {
			
			AttachmentComment attachmentComment = attachmentCommentService.create(request, lang);
			AttachmentCommentResponseDTO response = AttachmentCommentMapper
					.mapAttachmentCommentResponse(attachmentComment);
			return new ResponseEntity<>(response, HttpStatus.CREATED);
			
		} else {
			throw new InvalidRequestException(errors);
		}

	}

	/**
	 * @param attachmentId
	 * @return
	 */
	@GetMapping("/{id}/comments")
	public ResponseEntity<MutableList<AttachmentCommentResponseDTO>> getComments(
			@PathVariable(value = "id") Long attachmentId, @RequestParam(value = "lang") String lang) {

		if (Util.isEmpty(attachmentId)) {
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_ATTACHMENT, lang)));
		}

		// LLAMA A SERVICE
		return new ResponseEntity<>(attachmentCommentService.getComments(attachmentId, lang), HttpStatus.OK);
		
	}

	/**
	 * @param attachmentId
	 * @param id
	 * @param request
	 * @return
	 */
	@PatchMapping("/{attachmentId}/comments/{id}")
	public ResponseEntity<AttachmentCommentResponseDTO> patchComment(@PathVariable("attachmentId") Long attachmentId,
			@PathVariable("id") Long id, @RequestBody final AttachmentCommentEditDTO request) {

		try {
			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_GLOBAL_ID.getCode(),
						ApiErrorEnum.EMPTY_GLOBAL_ID.getMessage())));
			}
			if (Util.isEmpty(attachmentId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.EMPTY_ATTACHMENT.getCode(),
						ApiErrorEnum.EMPTY_ATTACHMENT.getMessage())));
			}
			if (Util.isEmpty(request.getValue())) {
				throw new InvalidRequestException(Lists.mutable
						.of(new ApiError(ApiErrorEnum.EMPTY_VALUE.getCode(), ApiErrorEnum.EMPTY_VALUE.getMessage())));
			}

			// LLAMA AL SERVICE Y RETORNA OBJETO ACTUALIZADO
			AttachmentComment attachmentComment=attachmentCommentService.edit(id, request.getValue());
			return new ResponseEntity<>(AttachmentCommentMapper.mapAttachmentCommentResponse(attachmentComment), HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentController.patchComment]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param attachmentId
	 * @param id
	 */
	@DeleteMapping("/comments/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteComment(@PathVariable("id") Long id,
							  @RequestParam(value = "lang") String lang) {

		if (Util.isEmpty(id)) {
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_ATTACHMENT, lang)));
			}
			
			commentAttachmentService.delete(id, lang);

	}

	/**
	 * @param attachmentId
	 * @param request
	 */
	@DeleteMapping("/unlike")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteLike(@RequestBody final AttachmentLikeDTO request,
			 @RequestParam(value = "lang") String lang) {

		if(Util.isEmpty(lang)) {
		
			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		
		MutableList<ApiError> errors = validateAttachmentLikeRequest(request, lang);
		
		if (errors.isEmpty()) {	
		
			attachmentService.deleteLike(request, lang);
		
		} else {
		
			throw new InvalidRequestException(errors);
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param extra
	 * @return
	 */
	@GetMapping("")
	public ResponseEntity<PaginateResponse<AttachmentDTO>> readAll(@RequestParam() Integer pageNumber,
			@RequestParam() Integer pageSize, @RequestParam(required = false) String extra) {
		try {
			PaginateResponse<AttachmentDTO> paginateResponse = attachmentService.readAll(pageNumber, pageSize, extra);

			return new ResponseEntity<>(paginateResponse, HttpStatus.OK);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentController.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public ResponseEntity<AttachmentDTO> readById(@PathVariable(value = "id") Long id) {

		try {
			if (Util.isEmpty(id)) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.MISSING_REQUEST_PARAMETER.getCode(),
								ApiErrorEnum.MISSING_REQUEST_PARAMETER.getMessage())));
			} else {
				Attachment attachment=attachmentService.readById(id);
				return new ResponseEntity<>(AttachmentMapper.mapAttachment(attachment), HttpStatus.OK);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentController.readById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id") Long id) {

		try {

			if (Util.isEmpty(id)) {
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.MISSING_REQUEST_PARAMETER.getCode(),
								ApiErrorEnum.MISSING_REQUEST_PARAMETER.getMessage())));
			} else {
				// LLAMA AL SERVICE
				attachmentService.deleteById(id);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentController.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@PostMapping(value = "/{attachmentId}/reports", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AttachmentReportDTO> report(@NonNull @PathVariable final Long attachmentId,
			@NonNull @RequestBody final AttachmentReportRequestDTO request) {
		try {
			if (Util.isEmpty(attachmentId)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
						ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())));
			}
			MutableList<ApiError> errors = validateRequest(request);
			if (!errors.isEmpty()) {
				throw new InvalidRequestException(errors);
			} else {
				AttachmentReport attachmentReport=attachmentService.report(attachmentId, request);
				return new ResponseEntity<>(AttachmentReportMapper.mapAttachmentReport(attachmentReport), HttpStatus.CREATED);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentController.report]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(AttachmentCommentDTO request, String lang) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();
		
		if (Util.isEmpty(request.getAttachmentId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_ATTACHMENT, lang));
		}
					
		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_ID, lang));
		}
		
		if (Util.isEmpty(request.getValue())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_VALUE, lang));
		}

		return apiErrors;
	}

	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(AttachmentReportRequestDTO request) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(
					new ApiError(ApiErrorEnum.EMPTY_PROFILE_ID.getCode(), ApiErrorEnum.EMPTY_PROFILE_ID.getMessage()));
		}
		if (Util.isEmpty(request.getDetail())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_REPORT_DETAIL.getCode(),
					ApiErrorEnum.EMPTY_REPORT_DETAIL.getMessage()));
		}
		if (Util.isEmpty(request.getReportTypeId())) {
			apiErrors.add(new ApiError(ApiErrorEnum.EMPTY_REPORT_TYPE.getCode(),
					ApiErrorEnum.EMPTY_REPORT_TYPE.getMessage()));
		}

		return apiErrors;
	}
	
	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateAttachmentLikeRequest(AttachmentLikeDTO request, String lang) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();

		if (Util.isEmpty(request.getAttachmentId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_ATTACHMENT, lang));
		}
		if (Util.isEmpty(request.getProfileId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_ID, lang));
		}
		
		return apiErrors;
	}
}
