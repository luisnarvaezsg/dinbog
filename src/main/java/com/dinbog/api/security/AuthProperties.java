package com.dinbog.api.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @author Luis
 *
 */
@Data
@Configuration("authProperties")
public class AuthProperties {

	@Value("${auth.bearer_token}")
	private String bearerToken;

	@Value("${auth.header}")
	private String header;

	@Value("${auth.issuer}")
	private String issuer;

	@Value("${auth.secret_key}")
	private String secretKey;

	@Value("${auth.rol_user}")
	private String rolUser;

	@Value("${auth.sub}")
	private String sub;

	@Value("${auth.expiration_time}")
	private int expirationTime;
}
