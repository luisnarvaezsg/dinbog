package com.dinbog.api.security;

import java.util.Arrays;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.security.jwt.JwtEntryPointAccessDenied;
import com.dinbog.api.security.jwt.JwtEntryPointAuthentication;
import com.dinbog.api.security.jwt.JwtFilter;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 *
 */
@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	JwtEntryPointAccessDenied jwtEntryPointAccessDenied;

	@Autowired
	JwtEntryPointAuthentication jwtEntryPointAuthentication;

	@Autowired
	JwtFilter jwtAuthFilter;
	
	@Autowired
	DataSource dataSource;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	// Enable JDBC authentication
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder());
	}
	
	@Bean
	public JdbcUserDetailsManager jdbcUserDetailsManager() {
		JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
		jdbcUserDetailsManager.setDataSource(dataSource);
		return jdbcUserDetailsManager;
	}

	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }
	
	@Bean
	public DaoAuthenticationProvider authProvider() {
	    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	    authProvider.setUserDetailsService(jdbcUserDetailsManager());
	    authProvider.setPasswordEncoder(passwordEncoder());
	    return authProvider;
	}
	
	@Override
	protected void configure(HttpSecurity httpSecurity) {

		try {
			httpSecurity.cors().and().authorizeRequests()

					// REQUEST QUE NO REQUIEREN AUTHENTICATION
					// Login y crear usuario y profile
					.antMatchers(HttpMethod.POST, "/v1/auth/login", "/v1/users**",  "/v1/profiles",
							"/v1/aws/**","/v1/profiles/upload-book", "/v1/profiles/upload-video",
						 "/v1/profiles/upload-work","/v1/profiles/upload-audio","/v1/profiles/upload-audiofile").permitAll()
					// Token de confirmación via email
					.antMatchers(HttpMethod.GET, "/v1/users/emails/*").permitAll()
					// Listas: categorías profiles, genders, paises y ciudades
					.antMatchers(HttpMethod.GET, "/v1/profiles/categories/**", "/v1/gender", "/v1/countries",
							"/v1/countries/{id:\\d+}/cities","/v1/profiles/findbyname/{like}/**")
					.permitAll()
					// Crear attachment en el proceso de crear usuario y profile
					.antMatchers(HttpMethod.POST, "/v1/profiles/{id:\\d+}/attachments","/v1/profiles/save").permitAll()
					// Reset password
					.antMatchers(HttpMethod.POST, "/v1/users/password/reset").permitAll()
					.antMatchers(HttpMethod.GET,"/v1/password/confirm-password/**").permitAll()

					// REQUEST CON AUTHENTICATION
					.anyRequest().authenticated()
					// MANEJO DE EXCEPCIONES
					.and().exceptionHandling().accessDeniedHandler(jwtEntryPointAccessDenied)
					.authenticationEntryPoint(jwtEntryPointAuthentication)
					// CONFIGURACION DE CSRF
					.and().csrf().disable()
					// this disables session creation on Spring Security
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			//// AGREGA FILTER PARA PROCESAR CADA REQUEST
			httpSecurity.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

		} catch (Exception e) {
			log.error("ERROR EN SECURITY CONFIG" + e);
			throw new InternalErrorException(e);
		}
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {

		CorsConfiguration configuration = new CorsConfiguration();

		configuration.setAllowedOrigins(Arrays.asList("*"));
		configuration.setAllowCredentials(true); // Access-Control-Request-Headers
		configuration.setAllowedHeaders(Arrays.asList("Access-Control-Allow-Headers", "Content-Type",
				"Access-Control-Request-Headers", "Authorization", "X-Requested-With"));
		configuration.setAllowedMethods(Arrays.asList("DELETE", "GET", "POST", "PATCH", "PUT"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);

		return source;
	}
}
