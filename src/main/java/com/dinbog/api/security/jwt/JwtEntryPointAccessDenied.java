/**
 * 
 */
package com.dinbog.api.security.jwt;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ApiExceptionResponse;
import com.dinbog.api.exception.BadCredentialsException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Maneja la respuesta para AccessDeniedException
 * 
 * @author Luis
 *
 */
@Component
public class JwtEntryPointAccessDenied implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			AccessDeniedException e) throws IOException, ServletException {

		httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
		httpServletResponse.setContentType("application/json");

		ApiExceptionResponse exceptionResponse = new ApiExceptionResponse(BadCredentialsException.CODE,
				BadCredentialsException.MSG, Lists.mutable.of(new ApiError(ApiErrorEnum.REQUEST_NO_ACCESS.getCode(),
						ApiErrorEnum.REQUEST_NO_ACCESS.getMessage())),
				"AccessDeniedException: " + e.getMessage());

		OutputStream out = httpServletResponse.getOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(out, exceptionResponse);
		out.flush();
	}
}
