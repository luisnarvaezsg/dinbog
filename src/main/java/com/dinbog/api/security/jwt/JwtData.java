/**
 * 
 */
package com.dinbog.api.security.jwt;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class JwtData {

	private Long userId;
	private String email;
	private Date expirationDate;

	/**
	 * @param userId
	 * @param email
	 */
	public JwtData(Long userId, String email, Date expirationDate) {
		this.userId = userId;
		this.email = email;
		this.expirationDate = expirationDate;
	}
}
