package com.dinbog.api.security.jwt;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.BadCredentialsException;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.security.AuthProperties;
import com.dinbog.api.service.IApiErrorService;
import com.dinbog.api.service.IUserService;
import com.dinbog.api.util.Util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase para manejar todas las acciones relevantes sobre los JWT de la API
 * 
 * @author Luis
 *
 */
@Slf4j
@Component
public class JwtProvider {

	@Autowired
	AuthProperties authProperties;

	private static final String SUB = "sub";
	private static final String ISSUER = "iss";
	private static final String EMAIL = "email";
	private static final String USER_ID = "userId";
	private static final String FIRST_NAME= "firstName";
	private static final String LAST_NAME = "lastName";

	@Autowired
	private IUserService iuserService;
	
	@Autowired
	private IApiErrorService errorService;
	/**
	 * Crea un JWT Token para un usuario y su email
	 * 
	 * @param user
	 * @param email
	 * @return
	 */
	public String generateToken(User user, String email) {

		try {
			Map<String, Object> claims = new TreeMap<>();
			claims.put(EMAIL, email);
			claims.put(USER_ID, user.getId().toString());
			claims.put(FIRST_NAME, user.getFirstName());
			claims.put(LAST_NAME, user.getLastName());
			claims.put(SUB, authProperties.getSub());
			claims.put(ISSUER, authProperties.getIssuer());

			return Jwts.builder().setIssuedAt(new Date())
					.setExpiration(new Date(new Date().getTime() + authProperties.getExpirationTime()))
					.addClaims(claims).signWith(SignatureAlgorithm.HS512, authProperties.getSecretKey()).compact();

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [JwtProvider.generateToken]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Retorna los datos relevantes dentro de los claims del token
	 * 
	 * @param token
	 * @return
	 */
	public JwtData getJWTData(String token) {

		Claims claims = getClaims(token);

		log.debug("getJWTData - ID USER: " + claims.get(USER_ID).toString());

		return new JwtData(Long.valueOf(claims.get(USER_ID).toString()), claims.get(EMAIL).toString(),
				claims.getExpiration());
	}

	/**
	 * Extrae el userId de un token
	 * 
	 * @param token
	 * @return
	 */
	public Long getUserIdFromToken() {

		try {
			return Long.parseLong(this.getClaims(USER_ID).getSubject());
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR PARSEANDO EL TOKEN
			log.error("*** Exception EN [JwtProvider.getUserIdFromJWT]");
			Util.printLogError(e);
			throw new BadCredentialsException(Lists.mutable
					.of(new ApiError(ApiErrorEnum.TOKEN_INVALID.getCode(), ApiErrorEnum.TOKEN_INVALID.getMessage())));
		}
	}

	/**
	 * Indica si un token tiene la estructura esperada
	 * 
	 * @param token
	 * @return
	 */
	public boolean isTokenValid(String token) {

		if(token == null) {
			return false;
		}
		Claims claims = getClaims(token);

		if (claims.get(EMAIL) != null && claims.get(SUB) != null && claims.get(USER_ID) != null && claims.get(ISSUER) != null) {
			return true;
		}

		return false;
	}

	/**
	 * Devuelve los claims de un token
	 * 
	 * @param token
	 * @return
	 */
	private Claims getClaims(String token) {

		return Jwts.parser().requireIssuer(authProperties.getIssuer()).setSigningKey(authProperties.getSecretKey())
					.parseClaimsJws(token).getBody();
	}
	
	public User getUser(HttpServletRequest request) {
		try {
			String header = request.getHeader("Authorization");

		    if (header == null || !header.startsWith("Bearer ")) {
		    	throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.TOKEN_INVALID, null)));
		    }

		    String token = header.substring(7);

			Claims claims = getClaims(token);
			
			if (!Util.isEmpty(token) && isTokenValid(token)) {
				return iuserService.findById(Long.parseLong(claims.get(USER_ID).toString()))
						.orElseThrow(() -> new BadCredentialsException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),
								ApiErrorEnum.USER_NOT_FOUND.getMessage()))));
			}

		} catch (Exception ex) {
			log.error("------------ JwtFilter ERROR -----------------");
			log.error("Could not set user authentication in security context", ex);
			ex.getStackTrace();
		}
		return null;
	}
}
