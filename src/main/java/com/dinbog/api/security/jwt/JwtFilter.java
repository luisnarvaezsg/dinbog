package com.dinbog.api.security.jwt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.BadCredentialsException;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.models.entity.UserEmail;
import com.dinbog.api.security.AuthProperties;
import com.dinbog.api.repository.UserEmailRepository;
import com.dinbog.api.repository.UserRepository;
import com.dinbog.api.util.Util;


/**
 * Component para filtrar todos los request hacia la api que requieran
 * authenticarse
 * 
 * @author Luis
 *
 */
@Component
public class JwtFilter extends OncePerRequestFilter {

	@Autowired
	AuthProperties authProperties;

	@Autowired
	JwtProvider jwtTokenProvider;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserEmailRepository userEmailRepository;

	/**
	 *
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(null);
		String bearer = getBearerFromRequest(request);

		if (!Util.isEmpty(bearer) && jwtTokenProvider.isTokenValid(bearer)) {

			JwtData data = jwtTokenProvider.getJWTData(bearer);

			// VALIDA QUE EMAIL Y USER ID EXISTAN Y SEAN DEL MISMO USUARIO
			UserEmail userEmail = userEmailRepository.findByEmail(data.getEmail().toLowerCase()).orElseThrow(() -> new BadCredentialsException(
					Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),
							ApiErrorEnum.USER_NOT_FOUND.getMessage()))));
			
			User user = userEmail.getUser();
			
			if (!user.getId().equals(data.getUserId())) {
				throw new BadCredentialsException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.TOKEN_NO_MATCH.getCode(), ApiErrorEnum.TOKEN_NO_MATCH.getMessage())));
			}

			// Creating the list of granted-authorities
			List<GrantedAuthority> authorities = new ArrayList<>();
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(data, null,
					authorities);
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

			context.setAuthentication(authentication);
			
			}

		filterChain.doFilter(request, response);
	}

	/**
	 * @param request
	 * @return
	 */
	public String getBearerFromRequest(HttpServletRequest request) {

		String bearerToken = request.getHeader(authProperties.getHeader());
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7);
		}
		return null;
	}

	public JwtData getClaimsFromBearer(String bearerToken) {

		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			String tokenJwt = bearerToken.substring(7);
			return jwtTokenProvider.getJWTData(tokenJwt);
		}
		return null;
	}
}
