package com.dinbog.api.security.jwt;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ApiExceptionResponse;
import com.dinbog.api.exception.BadCredentialsException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Maneja la respuesta para AuthenticationException
 * 
 * @author Luis
 *
 */
@Component
public class JwtEntryPointAuthentication implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = -7858869558953243875L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse httpServletResponse,
			AuthenticationException authException) throws IOException {

		httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
		httpServletResponse.setContentType("application/json");

		ApiExceptionResponse exceptionResponse = new ApiExceptionResponse(BadCredentialsException.CODE,
				BadCredentialsException.MSG,
				Lists.mutable.of(new ApiError(ApiErrorEnum.REQUEST_NO_AUTH.getCode(),
						ApiErrorEnum.REQUEST_NO_AUTH.getMessage())),
				"AuthenticationException: " + authException.getMessage());

		OutputStream out = httpServletResponse.getOutputStream();
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(out, exceptionResponse);
		out.flush();
	}
}
