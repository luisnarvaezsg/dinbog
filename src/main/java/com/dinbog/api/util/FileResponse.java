package com.dinbog.api.util;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class FileResponse {
    private String name;
    private String type;
    private long size;

    public FileResponse(String name, String type, long size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

}

