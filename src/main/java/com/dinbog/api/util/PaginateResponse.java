package com.dinbog.api.util;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Luis
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class PaginateResponse<T> {

	private Long totalElements;
	private Integer totalPages;
	private Integer pageNumber;
	private Integer pageSize;
	private List<T> listElements;

	/**
	 * 
	 */
	public PaginateResponse() {
		super();
	}

	/**
	 * @param totalElements
	 * @param totalPages
	 * @param pageNumber
	 * @param pageSize
	 * @param listElements
	 */
	public PaginateResponse(Long totalElements, Integer totalPages, Integer pageNumber, Integer pageSize,
			List<T> listElements) {
		super();
		this.totalElements = totalElements;
		this.totalPages = totalPages;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.listElements = listElements;
	}
}
