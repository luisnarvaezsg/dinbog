/*
* Enum Class and It contains all accepted Media File types into project. Thus, further any new file type must be
* setting here up on corresponding file section.
*/
package com.dinbog.api.util.files;

import com.dinbog.api.util.ParameterEnum;

import java.util.Arrays;

public enum FilesContentTypes {

    //Image ContentTypes
    IMAGE("image"),
    JPG("image/jpeg"),
    PNG("image/png"),
    GIF("image/gif"),
    BMP("image/bmp"),
    TIFF("image/tiff"),
    SVG("image/svg+xml"),
    WEBP("image/webp"),

    //Video ContentTypes
    VIDEO("video"),
    AVI("video/avi"),
    MPEG("video/mpeg"),
    MP4("video/mp4"),
    OGG("video/ogg"),
    WEBM("video/webm"),
    QUICKTIME("video/quicktime"),
    XFLV("video/x-flv"),
    XMSWMV("video/x-ms-wmv"),

    //Audio ContentTypes
    AUDIO("audio"),
    AMP3("audio/mp3"),
    AMP4("audio/mp4"),
    AMPEG("audio/mpeg"),
    AOGG("audio/ogg"),
    FLAC("audio/flac"),
    WAVE("audio/vnd.wave"),
    VORBIS("audio/vorbis"),
    WAV("audio/wav"),
    WMA("audio/x-ms-wma");

    private final String content;

    FilesContentTypes(String content) {
        this.content = content;
    }
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     * @return
     */
    public static boolean findContent(String content){
        return  Arrays.stream(values()).anyMatch(type -> type.getContent().equalsIgnoreCase(content));
    }

    /**
     *
     * @param content
     * @return
     */
    public static boolean isAudio(String content){
        return AUDIO.content.equalsIgnoreCase( getType(content) );
    }

    /**
     *
     * @param content
     * @return
     */
    public static boolean isImage(String content){
        return IMAGE.content.equalsIgnoreCase( getType(content)  );
    }

    /**
     *
     * @param content
     * @return
     */
    public static boolean isVideo(String content){
        return VIDEO.content.equalsIgnoreCase( getType(content) );
    }

    /**
     *
     * @param content
     * @return
     */
    private static String getType(String content){
        return content.substring(0, content.indexOf(ParameterEnum.SLASH.getKey()));
    }

}
