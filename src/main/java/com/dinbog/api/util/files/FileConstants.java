package com.dinbog.api.util.files;

public enum FileConstants {

    MAX_FILE_SIZE(10485760),
    MAX_PILE_SIZE(10),
    ;

    private final long value;

    FileConstants(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
