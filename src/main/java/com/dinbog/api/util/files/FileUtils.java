/*
 * Util Class that enables to validate any file
 */
package com.dinbog.api.util.files;

import com.dinbog.api.aws.IS3Services;
import com.dinbog.api.util.AlbumTypes;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class FileUtils {

    private final IS3Services s3Service;

    @Autowired
    private FileUtils(IS3Services s3Service) {
        this.s3Service = s3Service;
    }

    /**
     *  Enables to validate whether a file fits of "Type" and returns a boolean primitive result
     * @param file MultipartFile type for evaluation
     * @param type Audio, Video, Image into accepted content type
     * @return boolean true or false
     */
    public  boolean isProperPostFileType(MultipartFile file, String type){
        return isValid(file) && isAudioOrImage(file,type);
    }

    private boolean isAudioOrImage(MultipartFile file, String type){
        return type.equalsIgnoreCase(FilesContentTypes.AUDIO.getContent())
               ? FilesContentTypes.isAudio( file.getContentType() )
               : FilesContentTypes.isImage( file.getContentType() );
    }

    public  boolean isProperPostMultiFileType(MultipartFile file){
        return isValid(file) && (FilesContentTypes.isImage(file.getContentType()) || FilesContentTypes.isVideo(file.getContentType()));
    }

    /**
     * Validates whether a MultipartFile fill the proper conditions accepted by the app.
     * @param file MultipartFile with the proper content Type
     * @return true or false
     */
    protected boolean isValid(MultipartFile file){
        return (file != null) && !file.isEmpty() && FilesContentTypes.findContent(file.getContentType()) && (file.getSize() <= FileConstants.MAX_FILE_SIZE.getValue());
    }


    /**
     *  Gets the returned name from S3 service to persisting it in the database
     * @param file MultipartFile with the proper content Type
     * @param bucketName Name of the assigned S3 bucket for storing it.
     * @return <String> New file name
     */
    public String getFileAttachedName(MultipartFile  file, String bucketName){
        return s3Service.uploadFile(file, bucketName);
    }

    /**
     *  Evaluates whether the MultipartFile Array by filtering both size and Audio content type it and gets the
     *  accepted ones by the service
     * @param files MultipartFile with the proper Audio content Type
     * @return MultipartFile Array with the Proper files to be persisted
     */
    public MultipartFile[] validateFileAttach(MultipartFile[] files){
        return Arrays.stream(files)
                .limit( FileConstants.MAX_PILE_SIZE.getValue() )
                .filter(
                file -> FilesContentTypes.findContent( file.getContentType() ) &&
                        !file.isEmpty() && ( file.getSize() <= FileConstants.MAX_FILE_SIZE.getValue()) &&
                        !isProperPostFileType( file,FilesContentTypes.AUDIO.getContent() )
        ).collect(Collectors.toList()).toArray(MultipartFile[]::new);
    }

    public Long validateFileAttach(List<MultipartFile> files){
        return files.stream().limit( FileConstants.MAX_PILE_SIZE.getValue() ).filter(
                file -> FilesContentTypes.findContent(
                        file.getContentType()
                ) && !file.isEmpty() && file.getSize() <= FileConstants.MAX_FILE_SIZE.getValue() && !isProperPostFileType(file,FilesContentTypes.AUDIO.getContent())
        ).count();
    }

    public boolean isValidAlbumAttach(MultipartFile[] file, MultipartFile fileCover, String albumType){
        if(albumType.equalsIgnoreCase(AlbumTypes.AUDIO.getName()) ){
               if( !fileCover.isEmpty() && !isProperPostFileType(fileCover,FilesContentTypes.IMAGE.getContent()) )
                   return false;
               return file.length == 1 && isProperPostFileType(file[0],FilesContentTypes.AUDIO.getContent());
        }else if (albumType.equalsIgnoreCase(AlbumTypes.BOOK.getName())){
            if(!fileCover.isEmpty()) return false;
            return Arrays.stream(file).allMatch(f -> isProperPostFileType(f, FilesContentTypes.IMAGE.getContent()));
        }else{
            return false;
        }
    }

    /**
     * Evaluates the audio file and its cover whether both filled with the conditions of the app.
     * @param file MultipartFile with the proper Audio content Type
     * @param cover MultipartFile with the proper Image content Type
     * @return MultipartFile Map with the Proper files to be persisted
     */
    public Map<String,MultipartFile> validateFileAttach(MultipartFile file, MultipartFile cover){
        Map<String,MultipartFile> audioCoverPost = new HashMap<>();
        if ( isProperPostFileType( file, FilesContentTypes.AUDIO.getContent() ) ){
            audioCoverPost.put(FileMapIndex.AUDIO.getKey(), file);
            audioCoverPost.put(FileMapIndex.COVER.getKey(), isProperPostFileType(cover,FilesContentTypes.IMAGE.getContent()) ? cover : null);
        }
        return  audioCoverPost;
    }

    public MultipartFile[] validateFileAttachWithBanned(MultipartFile[] files,String content){
        return Arrays.stream(files)
                .limit( FileConstants.MAX_PILE_SIZE.getValue() )
                .filter(
                        file -> FilesContentTypes.findContent( file.getContentType() ) &&
                                !file.isEmpty() && ( file.getSize() <= FileConstants.MAX_FILE_SIZE.getValue()) &&
                                !isProperPostFileType( file,content )
                ).collect(Collectors.toList()).toArray(MultipartFile[]::new);
    }

    public MultipartFile[] validateFileAttachAny(MultipartFile[] files){
        return Arrays.stream(files)
                .limit( FileConstants.MAX_PILE_SIZE.getValue() )
                .filter(
                        file -> FilesContentTypes.findContent( file.getContentType() ) &&
                                !file.isEmpty() && ( file.getSize() <= FileConstants.MAX_FILE_SIZE.getValue())
                ).collect( Collectors.toList() ).toArray(MultipartFile[]::new);
    }
    
    public String createMetadata(String...values) {
    	
    	ArrayList<String> metadata = new ArrayList<>();
    	
        for(int i = 0; i < values.length; i++){
            metadata.add(values[i]);
        }
        
        return new Gson().toJson(metadata);
        }
}
