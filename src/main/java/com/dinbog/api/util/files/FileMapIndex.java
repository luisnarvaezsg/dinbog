package com.dinbog.api.util.files;

public enum FileMapIndex {
    AUDIO("audio"),
    COVER("cover"),
    ;

    private final String key;

    FileMapIndex(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
