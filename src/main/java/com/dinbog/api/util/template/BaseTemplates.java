package com.dinbog.api.util.template;

public enum BaseTemplates {
    BAD_REQUEST("bad_request"),
    NEW_PASSWORD("confirm-password"),
    EMAIL("email_base"),
    ;

    private final String layer;

    BaseTemplates(String layer) {
        this.layer = layer;
    }

    public String getLayer() {
        return layer;
    }
}
