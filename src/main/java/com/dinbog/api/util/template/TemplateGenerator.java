package com.dinbog.api.util.template;

import com.dinbog.api.config.templates.ThymeleafTemplateConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import java.util.Map;

@Component
public class TemplateGenerator {

    private final ThymeleafTemplateConfig templateEmailConfig;


    @Autowired
    public TemplateGenerator(ThymeleafTemplateConfig templateEmailConfig) {
        this.templateEmailConfig = templateEmailConfig;
    }

    public String getTemplate(String decorator, Map<String,Object> body){
        Context bodyContext = new Context();
        bodyContext.setVariables(body);
        return templateEmailConfig.templateEngine().process(decorator,bodyContext);
    }
}
