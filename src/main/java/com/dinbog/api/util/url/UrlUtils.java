package com.dinbog.api.util.url;


import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;

@Component
public class UrlUtils {

    public boolean isValid(String url){
        String[] customSchemes = { UrlParams.ACCEPTED_ONLY.getValue() };
        UrlValidator validator = new UrlValidator(customSchemes);
        return validator.isValid(url);
    }
}
