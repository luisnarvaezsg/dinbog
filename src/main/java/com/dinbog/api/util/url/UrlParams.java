package com.dinbog.api.util.url;

public enum UrlParams {
    KEY("link"),
    ACCEPTED_ONLY("https"),
    ;
    private final String value;

    UrlParams(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
