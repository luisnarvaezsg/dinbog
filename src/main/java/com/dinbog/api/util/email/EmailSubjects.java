package com.dinbog.api.util.email;

public enum EmailSubjects {
    CONFIRM("Registro Usuario Dinbog"),
    REQUEST_NEW_PASSWORD("Dinbog Reset Password Request"),
    RESET_PASSWORD_SUCCESS("Dinbog Reset Password Success"),
    ;

    private final String subject;

    EmailSubjects(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }
}
