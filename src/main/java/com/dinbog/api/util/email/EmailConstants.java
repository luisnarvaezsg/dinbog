package com.dinbog.api.util.email;

public enum EmailConstants {
    ENCODING("UTF-8"),
    EXCHANGER("dinbog-broker"),
    QUEUE("email_queue"),
    TOPIC("email.created"),
    ROUTE_KEY("dinbog.email"),
    ;

    private final String value;

    EmailConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
