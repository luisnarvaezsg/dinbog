package com.dinbog.api.util.email;

import com.dinbog.api.util.Util;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class LogEmailService {

    public static void sendingLog(long startingAt, long endingAt, String to){
        log.info("Sending email to {} sending at {}  and outgoing at {} ", to ,startingAt, endingAt);
    }

    public static  void errorLogin(Exception e){
        Util.printLogError(e);
    }

    public static void infoLog(String msg){
        log.info(msg);
    }

    public static  void warnLog(String msg){
        log.warn(msg);
    }
}
