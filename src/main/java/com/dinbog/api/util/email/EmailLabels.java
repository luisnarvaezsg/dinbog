package com.dinbog.api.util.email;

public enum EmailLabels {
    COMFIRM("Hacer click para confirmar su correo: "),
    ;

    private final String label;

    EmailLabels(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
