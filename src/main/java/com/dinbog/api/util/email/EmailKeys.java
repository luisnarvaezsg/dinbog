package com.dinbog.api.util.email;

public enum EmailKeys {
    URL("url"),
    LABEL("label"),
    TEMPLATE("template"),
    ;

    private final String key;

    EmailKeys(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
