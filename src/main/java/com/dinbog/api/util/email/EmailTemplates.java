package com.dinbog.api.util.email;

public enum EmailTemplates {
    CONFIRM("confirm_email"),
    PASSWORD_RESET("password_reset"),
    PASS_RESET_SUCCESS("password_reset_success"),
    ;

    private final String template;

    EmailTemplates(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
}
