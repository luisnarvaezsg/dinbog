package com.dinbog.api.util;

/**
 * @author Luis
 */
public enum ParameterEnum {

	// KEYS PARA EMAILS
	URL_NEW_USER("URL_NEW_USER"),
	URL_NEW_EMAIL("URL_NEW_EMAIL"),

	// OTROS....
	BLANK(""),
	SLASH("/"),
	;

	private final String key;

	/**
	 * @param key
	 */
    ParameterEnum(String key) {
		this.key = key;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
}
