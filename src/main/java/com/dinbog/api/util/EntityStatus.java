package com.dinbog.api.util;

import org.springframework.lang.Nullable;

public enum EntityStatus {
    ACTIVATED(1,"Activated"),
    DELETED(0,"Deleted");

    private final int value;
    private final String label;

    EntityStatus(int value, String label) {
        this.value = value;
        this.label = label;
    }

    public int value(){
        return this.value;
    }
    public String getLabel(){
        return this.label;
    }

    public static EntityStatus valueOf(int code) {
        EntityStatus status = resolve(code);
        if (status == null) {
            throw new IllegalArgumentException("No matching constant for [" + code + "]");
        }
        return status;
    }

    @Nullable
    public static EntityStatus resolve(int code) {
        for (EntityStatus status : values()) {
            if (status.value == code) {
                return status;
            }
        }
        return null;
    }
}
