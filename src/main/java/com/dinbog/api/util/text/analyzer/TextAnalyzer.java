package com.dinbog.api.util.text.analyzer;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.collections.ListUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.miscellaneous.RemoveDuplicatesTokenFilter;
import org.apache.lucene.analysis.pattern.PatternTokenizerFactory;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class TextAnalyzer extends Analyzer  {

    private String text;
    private Regex regex;
    private PatternTokenizerFactory factory;
    private LowerCaseFilterFactory lowerCaseFilterFactory;
    private StringReader reader;

    /**
     * Augmented constructor's class
     * @param text Incoming text from any client or class consumer
     */
    public TextAnalyzer(String text) {
        this.text = text.toLowerCase();
    }

    /**
     *  Enables return all valid hashtag into any incoming text
     * @return List<String> with valid hashtags
     */
    public List<String> getHashTags(){
        return getTokensBy(Regex.HASHTAG);
    }

    /**
     * Enables return all valid mentions into any incoming text
     * @return List<String> with valid mentions
     */
    public List<Long> getMentions(){
        ListUtils<String> listUtils = new ListUtils<>( getTokensBy(Regex.MENTIONS) );
        return listUtils.listStringToLong();
    }

    /**
     * Enables processing the incoming text and return the valid Valid List, taking the
     * splitter and regex pattern as main delimiters.
     * @param regex Pattern for markups validation
     * @return List<String> of valid hashtags founded in the incoming  text.
     */
    protected List<String> getTokensBy(Regex regex){
        setRegex(regex);
        List<String> markups = new ArrayList<>();
        try {
            TokenStream stream = tokenStream(regex.name(), new StringReader(this.text));
            stream.reset();
            while (stream.incrementToken()) {
                markups.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        }catch (IOException e){
            Util.printLogError(e);
            throw new InternalErrorException(e);
        }
        return  getDistinctList(markups);
    }

    /**
     *  Remove any duplicated value on the collection
     * @param markUps String Collection
     * @return List without duplicated items
     */
    protected List<String> getDistinctList(List<String> markUps){
        return   markUps.stream().distinct().collect(Collectors.toList());
    }

    /**
     * Overridden method that enables to set all filters to perform the query by pattern matching.
     * @param s hypothetical name of the searching field.
     * @return Searching parameters.
     */
    @Override
    protected TokenStreamComponents createComponents(String s) {
        Map<String,String> parameters = new HashMap<>();
        parameters.put(PatternTokenizerFactory.PATTERN, this.regex.getRegex());
        parameters.put(PatternTokenizerFactory.GROUP, regex.getGroups());
        factory = new PatternTokenizerFactory(parameters);
        lowerCaseFilterFactory =  new LowerCaseFilterFactory(parameters);
        Tokenizer tokenizer = factory.create();
        RemoveDuplicatesTokenFilter filter = new RemoveDuplicatesTokenFilter(lowerCaseFilterFactory.create(tokenizer));
        return new TokenStreamComponents(tokenizer,filter);
    }
}
