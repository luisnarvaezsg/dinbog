package com.dinbog.api.util.text.analyzer;

public enum Regex {

    HASHTAG("#","([#][\\w_-]+){1,100}","1"),
    //MENTIONS("@","([@][{][{])((?<!-)(?<!\\d)[1-9][0-9]{1,15})([}][}])","3"),
    MENTIONS("@","[@][{][{]((?!0+)\\d{1,15})[}][}]","1"),
    ;

    private final String splitter;
    private final String regexpression;
    private final String groups;

    Regex(String splitter, String regex,String groups) {
        this.splitter = splitter;
        this.regexpression = regex;
        this.groups =  groups;
    }

    public String getSplitter() {
        return splitter;
    }

    public String getRegex() {
        return regexpression;
    }

    public String getGroups() {
        return groups;
    }
}
