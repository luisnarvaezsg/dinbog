/**
 * 
 */
package com.dinbog.api.util;

import java.net.InetAddress;

import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase para obtener datos del host en que se está corriendo actualmente
 * 
 * @author Luis
 *
 */
@Slf4j
@Getter
@Setter
@Component
public class HostProvider implements ApplicationListener<ServletWebServerInitializedEvent> {

	private String hostPort;
	private String hostName;
	private String hostAddress;

	@Override
	public void onApplicationEvent(final ServletWebServerInitializedEvent event) {

		try {
			// OBTIENE EL PUERTO
			this.hostPort = String.valueOf(event.getWebServer().getPort());

			// OBTIENE DATOS DEL HOST
			this.hostName = InetAddress.getLocalHost().getHostName();
			this.hostAddress = InetAddress.getLocalHost().getHostAddress();

			log.info("************************************");
			log.info("DINBOG API is running on:");
			log.info("");
			log.info(" HOST PORT > " + this.hostPort);
			log.info("   HOST IP > " + this.hostAddress);
			log.info(" HOST NAME > " + this.hostName);
			log.info("************************************");

		} catch (Exception e) {
			log.error("*** Exception EN [HostProvider.onApplicationEvent]");
			Util.printLogError(e);
		}
	}
}
