package com.dinbog.api.util;

import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiExceptionResponse;
import com.dinbog.api.exception.BadCredentialsException;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;

import lombok.extern.slf4j.Slf4j;

/**
 * Clase con métodos comunes
 *
 * @author Luis Santana
 * @version %I%, %G%
 * @since 11
 */
@Slf4j
public class Util {

	

	/**
	 *
	 */
	private Util() {
		super();
	}

	/**
	 * Método para mostrar por cónsola la información de una Excepción
	 *
	 * @param Exception
	 */
	public static Map<String, Object> printException(Exception e) {
		 
		try {
			Map<String, Object> body = new LinkedHashMap<>();
	        body.put("***   timestamp: ", LocalDateTime.now());
	        body.put("***   message", e.getMessage());
	        body.put("***   clase: " , e.getClass());
	        body.put("***   causa: " , e.getCause());
	        return body;
		} catch (Exception ex) {
			log.error("Error en [Util.printException(Exception e)] :");
			return null;
		}
	}
	public static Map<String, Object> printException(Exception e, MutableList<ApiError> apiErrors ) {
		 
		try {
			Map<String, Object> body = new LinkedHashMap<>();
	        body.put("***   timestamp: ", LocalDateTime.now());
	        body.put("***   message", e.getMessage());
	        body.put("***   clase: " , e.getClass());
	        body.put("***   causa: " , e.getCause());
	        body.put("***   errores: " , apiErrors);
	        return body;
		} catch (Exception ex) {
			log.error("Error en [Util.printLogError] :");
			return null;
		}
	}
	
	public static void printLogError(Exception e) {
		try {
			log.error("*** message: " + e.getMessage());
			log.error("***   clase: " + e.getClass());
			log.error("***   causa: " + e.getCause());
			log.error("*** StackTrace ");
			log.error(getStackTraceDetail(e.getStackTrace()));

		} catch (Exception ex) {
			log.error("Error en [Util.printLogError(Exception e)] :");
		}
	}
	
	public static void printLogError(Exception e, String message) {
		log.error(message);
		try {
			log.error("*** message: " + e.getMessage());
			log.error("***   clase: " + e.getClass());
			log.error("***   causa: " + e.getCause());
			log.error("*** StackTrace ");
			log.error(getStackTraceDetail(e.getStackTrace()));

		} catch (Exception ex) {
			log.error("Error en [Util.printLogError(Exception e, String message)] :");
		}
	}
	/**
	 * Devuelve un string con toda la información de una Excepción para el campo
	 * detail
	 *
	 * @param Exception
	 */
	public static String getExceptionDetail(Exception e) {

		try {
			StringBuilder response = new StringBuilder(" message: ").append(e.getMessage())
					.append(System.lineSeparator()).append(getStackTraceDetail(e.getStackTrace()));
			return response.toString();
		} catch (Exception ex) {
			log.error("Error en [Util.getExceptionDetail] :");
			printLogError(ex);
			return "";
		}
	}

	/**
	 * Devuelve un string con toda la información de una Excepción
	 *
	 * @param Exception
	 */
	public static String getTrowableDetail(Throwable e) {
		try {
			StringBuilder response = new StringBuilder(" message: ").append(e.getMessage())
					.append(System.lineSeparator()).append("   clase: ").append(e.getClass())
					.append(System.lineSeparator()).append("   causa: ").append(e.getClass())
					.append(System.lineSeparator()).append(getStackTraceDetail(e.getStackTrace()));

			return response.toString();
		} catch (Exception ex) {
			log.error("Error en [Util.getTrowableDetail] :");
			printLogError(ex);
			return "";
		}
	}

	/**
	 * Devuelve un string con la información del Stack Trace de las clases del
	 * proyecto
	 * 
	 * @param stackTrace
	 * @return
	 */
	public static String getStackTraceDetail(StackTraceElement[] stackTrace) {
		try {
			StringBuilder response = new StringBuilder("=== StackTrace: ").append(System.lineSeparator());

			for (StackTraceElement i : stackTrace) {
				if (i.getClassName().startsWith("com.dinbog"))
					response.append("===  ").append(i.getClassName()).append("  (").append(i.getMethodName())
							.append(" : ").append(i.getLineNumber()).append(")").append(System.lineSeparator());
			}
			return response.toString();
		} catch (Exception ex) {
			log.error("Error en [Util.getStackTraceDetail] :");
			printLogError(ex);
			return "";
		}
	}

	/**
	 * Devuelve un string con la información del Stack Trace (COMPLETO)
	 * 
	 * @param stackTrace
	 * @return
	 */
	public static String getStackTraceFull(StackTraceElement[] stackTrace) {
		try {
			StringBuilder response = new StringBuilder("=== StackTrace: ").append(System.lineSeparator());

			for (StackTraceElement i : stackTrace) {
				response.append("===  ").append(i.getClassName()).append("  (").append(i.getMethodName()).append(" : ")
						.append(i.getLineNumber()).append(")").append(System.lineSeparator());
			}
			return response.toString();
		} catch (Exception ex) {
			log.error("Error en [Util.getStackTraceFull] :");
			printLogError(ex);
			return "";
		}
	}

	/**
	 * Valida si un Exception es una instancia de uno de los Custom Exceptions de la
	 * API
	 * 
	 * @param e
	 * @return
	 */
	public static boolean isCustomException(Exception e) {

		boolean response = false;

		if (e instanceof InternalErrorException || e instanceof InvalidRequestException || e instanceof ApiExceptionResponse 
				|| e instanceof BadCredentialsException || e instanceof ResourceNotFoundException) {
			response = true;
		}

		return response;
	}

	/**
	 * Devuelve TRUE si un Collection ES [null] o NO TIENE contenido
	 *
	 * @param obj
	 * @return boolean
	 */
	public static boolean isEmpty(Collection<?> obj) {

		return obj == null || obj.isEmpty();
	}

	/**
	 * Devuelve TRUE si un String ES [null] o NO TIENE contenido
	 *
	 * @param string
	 * @return boolean
	 */
	public static boolean isEmpty(String string) {

		return string == null || string.trim().isEmpty();
	}

	/**
	 * Devuelve TRUE si un objeto ES [null] o NO TIENE contenido
	 *
	 * @param obj
	 * @return boolean
	 */
	public static boolean isEmpty(Object obj) {

		return obj == null || obj.toString().trim().isEmpty();
	}

	/**
	 * @param obj
	 * @return
	 */
	public static boolean isInteger(Object obj) {

		final String strNum = String.valueOf(obj).trim();
		return strNum.matches("-?\\d+?");
	}
	/**
	 * @param obj
	 * @return
	 */
	public static boolean isNumeric(Object obj) {

		final String strNum = String.valueOf(obj).trim();
		return strNum.matches("-?\\d+(\\.\\d+)?");
	}

	/**
	 * @param originalInput
	 * @return
	 */
	public static String encrypt(String originalInput) {

		try {
			return Base64.getEncoder().encodeToString(originalInput.getBytes());
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param encodedString
	 * @return
	 */
	public static String decrypt(String encodedString) {

		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedString);

			return new String(decodedBytes);
		} catch (Exception e) {
			return null;
		}
	}

	
	

	public static boolean isValidEmail(String email) {

		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);

		return matcher.matches();
	}
}
