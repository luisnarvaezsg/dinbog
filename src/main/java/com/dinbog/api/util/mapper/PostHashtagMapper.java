package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.PostHashtagDTO;
import com.dinbog.api.models.entity.PostHashtag;

@Component
public class PostHashtagMapper {

	private PostHashtagMapper() {
		super();
	}

	public static PostHashtagDTO mapPostHashtag(PostHashtag postHashtag, Boolean isSearch) {

		PostHashtagDTO postHashtagDTO = new PostHashtagDTO();
		postHashtagDTO.setId(postHashtag.getId());
		postHashtagDTO.setNameHasTag(postHashtag.getNameHasTag());
		if (Boolean.FALSE.equals(isSearch)) {
			postHashtagDTO.setPost(PostMapper.mapPost(null,postHashtag.getPost(),null, null,null,null, null,null,null,null,null));
		}
		postHashtagDTO.setCreated(postHashtag.getCreated());

		return postHashtagDTO;
	}
}
