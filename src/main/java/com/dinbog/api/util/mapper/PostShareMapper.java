package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.PostShareDTO;
import com.dinbog.api.models.entity.PostShare;

@Component
public class PostShareMapper {

	private PostShareMapper() {
		super();
	}

	public static PostShareDTO mapPostShare(PostShare postShare) {

		PostShareDTO postShareDTO = new PostShareDTO();
		postShareDTO.setId(postShare.getId());
		postShareDTO.setPostId(postShare.getPost().getId());
		postShareDTO.setProfileId(postShare.getProfile().getId());
		postShareDTO.setCreated(postShare.getCreated());

		return postShareDTO;
	}
}
