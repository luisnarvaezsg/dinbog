package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.TraslationDTO;
import com.dinbog.api.models.entity.Traslation;

@Component
public class TraslationMapper {
	
	private TraslationMapper() {
		super();
	}

	public static TraslationDTO mapTranslation(Traslation translation) {

		TraslationDTO translationDTO = new TraslationDTO();
		translationDTO.setKey(translation.getKey());
		translationDTO.setIsoCode(translation.getIsoCode());
		translationDTO.setValue(translation.getValue());
		translationDTO.setModule(translation.getModule());
		
		return translationDTO;
	}

}
