package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.ProfileRequestConnection;
import com.dinbog.api.dto.ProfileRequestConnectionDTO;

@Component
public class ProfileRequestConnectionMapper {

	private ProfileRequestConnectionMapper() {
		super();
	}

	public static ProfileRequestConnectionDTO mapProfileRequestConnection(ProfileRequestConnection requestConnection) {

		ProfileRequestConnectionDTO profileRequestConnectionDTO = new ProfileRequestConnectionDTO();
		profileRequestConnectionDTO.setId(requestConnection.getId());
		//profileRequestConnectionDTO.setOwner(ProfileMapper.mapProfile(requestConnection.getOwner()));
		//profileRequestConnectionDTO.setProfile(ProfileMapper.mapProfile(requestConnection.getProfile()));
		profileRequestConnectionDTO.setOwner(ProfileMapper.mapProfileBasic(requestConnection.getOwner()));
		profileRequestConnectionDTO.setProfile(ProfileMapper.mapProfileBasic(requestConnection.getProfile()));
		profileRequestConnectionDTO.setProfileRequestStatus(
				ProfileRequestStatusMapper.mapProfileRequestStatus(requestConnection.getProfileRequestStatus()));
		profileRequestConnectionDTO
				.setTypeConnection(ConnectionTypeMapper.mapConnectionType(requestConnection.getConnectionType()));
		profileRequestConnectionDTO.setCreated(requestConnection.getCreated());

		return profileRequestConnectionDTO;
	}
}
