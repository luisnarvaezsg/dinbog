package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.UserStatus;
import com.dinbog.api.dto.UserStatusBasicDTO;
import com.dinbog.api.dto.UserStatusDTO;

@Component
public class UserStatusMapper {

	/**
	 * 
	 */
	private UserStatusMapper() {
		super();
	}

	public static UserStatusDTO mapUserStatus(UserStatus userStatus) {

		UserStatusDTO userStatusDTO = new UserStatusDTO();

		userStatusDTO.setId(userStatus.getId());
		userStatusDTO.setName(userStatus.getName());
		userStatusDTO.setDescription(userStatus.getDescription());
		userStatusDTO.setIsActive(userStatus.getIsActive());
		userStatusDTO.setCreated(userStatus.getCreated());

		return userStatusDTO;
	}

	public static UserStatusBasicDTO mapUserStatusBasic(UserStatus userStatus) {

		UserStatusBasicDTO userStatusBasicDTO = new UserStatusBasicDTO();

		userStatusBasicDTO.setId(userStatus.getId());
		userStatusBasicDTO.setName(userStatus.getName());

		return userStatusBasicDTO;
	}
}
