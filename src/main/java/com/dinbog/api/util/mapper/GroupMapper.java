/**
 * 
 */
package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.Group;
import com.dinbog.api.dto.GroupBasicDTO;
import com.dinbog.api.dto.GroupDTO;
import com.dinbog.api.util.Util;

/**
 * @author Luis
 *
 */
@Component
public class GroupMapper {

	/**
	 * 
	 */
	private GroupMapper() {
		super();
	}

	public static GroupDTO mapGroup(Group group) {

		GroupDTO groupDTO = new GroupDTO();

		groupDTO.setId(group.getId());
		groupDTO.setName(group.getName());

		return groupDTO;
	}

	public static GroupBasicDTO mapGroupBasic(Group group) {

		GroupBasicDTO groupBasicDTO = new GroupBasicDTO();

		groupBasicDTO.setId(group.getId());
		groupBasicDTO.setDescription(group.getDescription());
		groupBasicDTO.setIsActive(group.getIsActive());
		groupBasicDTO.setCreated(group.getCreated());
		if (!Util.isEmpty(group.getName()))
			groupBasicDTO.setName(group.getName());

		return groupBasicDTO;
	}
}
