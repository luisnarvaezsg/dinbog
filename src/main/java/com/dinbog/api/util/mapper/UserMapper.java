package com.dinbog.api.util.mapper;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.User;
import com.dinbog.api.dto.UserCreateResponseDTO;
import com.dinbog.api.dto.UserDTO;
import com.dinbog.api.dto.GroupBasicDTO;

@Component
public class UserMapper {

	/**
	 * 
	 */
	private UserMapper() {
		super();
	}

	public static UserDTO mapUser(User user) {

		UserDTO userDTO = new UserDTO();

		userDTO.setId(user.getId());
		userDTO.setCreated(user.getCreated());
		userDTO.setLastLogin(user.getLastLogin());
		userDTO.setGroups(Lists.mutable.empty());
		userDTO.setUserStatus(UserStatusMapper.mapUserStatusBasic(user.getUserStatus()));

		return userDTO;
	}

	public static UserCreateResponseDTO mapUserCreate(User user) {

		UserCreateResponseDTO userDTO = new UserCreateResponseDTO();

		userDTO.setId(user.getId());
		userDTO.setFirstName(user.getFirstName());
		userDTO.setLastName(user.getLastName());
		userDTO.setId(user.getId());
		userDTO.setGroups(Lists.mutable.empty());
		userDTO.setUserStatus(UserStatusMapper.mapUserStatusBasic(user.getUserStatus()));

		return userDTO;
	}

	public static UserCreateResponseDTO mapUserCreate(User user, String email, MutableList<GroupBasicDTO> groups) {

		UserCreateResponseDTO userDTO = new UserCreateResponseDTO();

		userDTO.setId(user.getId());
		userDTO.setEmail(email);
		userDTO.setGroups(groups);
		userDTO.setUserStatus(UserStatusMapper.mapUserStatusBasic(user.getUserStatus()));

		return userDTO;
	}
}
