package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.UserGroup;
import com.dinbog.api.dto.UserGroupDTO;

@Component
public class UserGroupMapper {

	private UserGroupMapper() {
		super();
	}

	public static UserGroupDTO mapUserGroup(UserGroup userGroup) {
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		userGroupDTO.setId(userGroup.getId());
		userGroupDTO.setUser(UserMapper.mapUser(userGroup.getUser()));
		userGroupDTO.setGroup(GroupMapper.mapGroupBasic(userGroup.getGroup()));
		return userGroupDTO;
	}
}
