package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.CityDTO;
import com.dinbog.api.models.entity.City;

@Component
public class CityMapper {

	private CityMapper() {
		super();
	}

	public static CityDTO mapCity(City city) {
		CityDTO cityDTO = new CityDTO();
		cityDTO.setId(city.getId());
		cityDTO.setCode(city.getCode());
		cityDTO.setStateName(city.getStateName());
		cityDTO.setLatitude(city.getLatitude());
		cityDTO.setLongitude(city.getLongitude());
		cityDTO.setCountryId(city.getCountryId());
		cityDTO.setKey(city.getKey());

		return cityDTO;
	}
}
