package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.PostAttachmentDTO;
import com.dinbog.api.models.entity.PostAttachment;

@Component
public class PostAttachmentMapper {

	private PostAttachmentMapper() {
		super();
	}

	public static PostAttachmentDTO mapPostAttachment(PostAttachment postAttachment) {

		PostAttachmentDTO postAttachmentDTO = new PostAttachmentDTO();
		postAttachmentDTO.setId(postAttachment.getId());
		postAttachmentDTO.setPostId(postAttachment.getPost().getId());
		postAttachmentDTO.setAttachmentId(postAttachment.getAttachment().getId());

		return postAttachmentDTO;
	}

}
