package com.dinbog.api.util.mapper;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.ProfileCategoryDTO;
import com.dinbog.api.dto.ProfileFilterValueDTO;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileFilterValue;

@Component
public class ProfileCategoryMapper {
	
	
	private ProfileCategoryMapper() {
		super();
	}
	
	public static ProfileCategoryDTO mapProfileCategory(ProfileCategory profileCategory) {
		
		ProfileCategoryDTO profileCategoryDTO = new ProfileCategoryDTO();
		profileCategoryDTO.setId(profileCategory.getId());
		profileCategoryDTO.setProfileCategoryType(ProfileCategoryTypeMapper.mapProfileCategoryType(profileCategory.getProfileCategoryType(),false));
				
		if (!profileCategory.getProfileFiltersValues().isEmpty()) {
			
			ArrayList<ProfileFilterValueDTO> values = new ArrayList<>();
			for (ProfileFilterValue profileValue : profileCategory.getProfileFiltersValues()) {
				
				values.add(ProfileFilterValueMapper.mapProfileFilterValue(profileValue));
			}
			profileCategoryDTO.setProfileFiltersValues(values);
		}
		
		return profileCategoryDTO;
	}
}
