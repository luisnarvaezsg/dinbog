package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.AttachmentTypeDTO;
import com.dinbog.api.models.entity.AttachmentType;

@Component
public class AttachmentTypeMapper {

	private AttachmentTypeMapper() {
		super();
	}

	public static AttachmentTypeDTO mapAttachmentType(AttachmentType attachmentType) {

		AttachmentTypeDTO attachmentTypeDTO = new AttachmentTypeDTO();
		attachmentTypeDTO.setId(attachmentType.getId());
		attachmentTypeDTO.setValue(attachmentType.getValue());

		return attachmentTypeDTO;
	}
}
