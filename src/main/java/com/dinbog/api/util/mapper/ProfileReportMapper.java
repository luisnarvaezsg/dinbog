package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.ProfileReport;
import com.dinbog.api.dto.ProfileReportDTO;

@Component
public class ProfileReportMapper {

	ProfileReportMapper() {
		super();
	}

	public static ProfileReportDTO mapEntityToDTO(ProfileReport profileReport) {

		ProfileReportDTO profileReportDTO = new ProfileReportDTO();
		profileReportDTO.setId(profileReport.getId());
		profileReportDTO.setOwnerProfile(ProfileMapper.mapProfileSimple(profileReport.getOwnerProfile(),null,null,null,null,null));
		profileReportDTO.setProfile(ProfileMapper.mapProfileSimple(profileReport.getProfile(),null,null,null,null,null));
		profileReportDTO.setReportType(ReportTypeMapper.mapReportType(profileReport.getReportType()));
		profileReportDTO.setDetail(profileReport.getDetail());

		return profileReportDTO;
	}
}
