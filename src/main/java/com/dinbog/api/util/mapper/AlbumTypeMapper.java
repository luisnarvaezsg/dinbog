package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.AlbumTypeDTO;
import com.dinbog.api.models.entity.AlbumType;

@Component
public class AlbumTypeMapper {

	private AlbumTypeMapper() {
		super();
	}

	public static AlbumTypeDTO mapAlbumType(AlbumType albumType) {

		AlbumTypeDTO albumTypeDTO = new AlbumTypeDTO();
		albumTypeDTO.setId(albumType.getId());
		albumTypeDTO.setName(albumType.getName());
		albumTypeDTO.setDescription(albumType.getDescription());
		albumTypeDTO.setIsActive(albumType.getIsActive());
		albumTypeDTO.setCreated(albumType.getCreated());
		albumTypeDTO.setUpdated(albumType.getUpdated());

		return albumTypeDTO;
	}
}
