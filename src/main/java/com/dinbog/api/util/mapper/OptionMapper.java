package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.OptionDTO;
import com.dinbog.api.models.entity.Option;

@Component
public class OptionMapper {

	/**
	 *
	 */
	private OptionMapper() {
		super();
	}

	public static OptionDTO mapOption(Option option) {

		OptionDTO optionDTO = new OptionDTO();
		optionDTO.setId(option.getId());
		optionDTO.setValue(option.getValue());

		optionDTO.setField(FieldMapper.mapField(option.getField()));

		return optionDTO;
	}

}
