package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.AlbumAttachmentDTO;
import com.dinbog.api.dto.AlbumDTO;
import com.dinbog.api.models.entity.AlbumAttachment;

@Component
public class AlbumAttachmentMapper {

	private AlbumAttachmentMapper() {
		super();
	}

	public static AlbumAttachmentDTO mapAlbumAttachment(AlbumAttachment albumAttachment) {

		AlbumAttachmentDTO albumAttachmentDTO = new AlbumAttachmentDTO();
		albumAttachmentDTO.setId(albumAttachment.getId());
		albumAttachmentDTO.setIsCover(albumAttachment.getIsCover());
		AlbumDTO album = new AlbumDTO();
		album.setId(albumAttachment.getAlbum().getId());
		albumAttachmentDTO.setAttachment(AttachmentMapper.mapAttachment(albumAttachment.getAttachment()));

		return albumAttachmentDTO;
	}
}
