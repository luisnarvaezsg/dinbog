package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.ProfileRecommendation;
import com.dinbog.api.dto.ProfileRecommendationDTO;

@Component
public class ProfileRecommendationMapper {

	private ProfileRecommendationMapper() {
		super();
	}

	public static ProfileRecommendationDTO mapEntityToDTO(ProfileRecommendation profileRecommendation) {
		ProfileRecommendationDTO profileRecommendationDTO = new ProfileRecommendationDTO();
		profileRecommendationDTO.setId(profileRecommendation.getId());
		profileRecommendationDTO.setProfile(ProfileMapper.mapProfileSimple(profileRecommendation.getProfile(), null,null,null,null,null));
		profileRecommendationDTO
				.setOwnerProfile(ProfileMapper.mapProfileSimple(profileRecommendation.getOwnerProfile(), null,null,null,null,null));
		profileRecommendationDTO.setIsPublic(profileRecommendation.getIsPublic());
		profileRecommendationDTO.setValue(profileRecommendation.getValue());
		profileRecommendationDTO.setCreated(profileRecommendation.getCreated());
		return profileRecommendationDTO;
	}
}
