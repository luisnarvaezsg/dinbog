package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.SocialApp;
import com.dinbog.api.dto.SocialAppBasicDTO;
import com.dinbog.api.dto.SocialAppDTO;

@Component
public class SocialAppMapper {

	private SocialAppMapper() {
		super();
	}

	public static SocialAppDTO mapSocialApp(SocialApp socialApp) {

		SocialAppDTO socialAppDTO = new SocialAppDTO();
		socialAppDTO.setId(socialApp.getId());
		socialAppDTO.setName(socialApp.getName());
		socialAppDTO.setDomain(socialApp.getDomain());
		socialAppDTO.setActive(socialApp.getActive());
		socialAppDTO.setKey(socialApp.getKey());
		socialAppDTO.setSecret(socialApp.getSecret());
		socialAppDTO.setClientId(socialApp.getClientId());
		socialAppDTO.setCreated(socialApp.getCreated());
		socialAppDTO.setUpdated(socialApp.getUpdated());

		return socialAppDTO;
	}

	public static SocialAppBasicDTO mapSocialAppBasic(SocialApp socialApp) {

		SocialAppBasicDTO socialAppBasicDTO = new SocialAppBasicDTO();
		socialAppBasicDTO.setId(socialApp.getId());
		socialAppBasicDTO.setName(socialApp.getName());
		socialAppBasicDTO.setDomain(socialApp.getDomain());
		socialAppBasicDTO.setActive(socialApp.getActive());

		return socialAppBasicDTO;
	}
}
