package com.dinbog.api.util.mapper;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dinbog.api.dto.ProfileCategoryTypeDTO;
import com.dinbog.api.models.entity.ProfileCategoryType;
import com.dinbog.api.repository.TraslationRepository;

@Component
public class ProfileCategoryTypeMapper {
	
	@Autowired
	TraslationRepository translationRepository;
	
	private ProfileCategoryTypeMapper() {
		super();
	}
	
	public static ProfileCategoryTypeDTO mapProfileCategoryType(ProfileCategoryType profileCategoryType, boolean childs) {
		
		ProfileCategoryTypeDTO profileCategoryTypeDTO = new ProfileCategoryTypeDTO();
		profileCategoryTypeDTO.setId(profileCategoryType.getId());
		profileCategoryTypeDTO.setLevel(profileCategoryType.getLevel());
		profileCategoryTypeDTO.setKey(profileCategoryType.getKey());
		profileCategoryTypeDTO.setValue(profileCategoryType.getValue());
		
		if(childs) {
		
			MutableList<ProfileCategoryTypeDTO> child = Lists.mutable.of();
			
			profileCategoryType.getChild().forEach(j -> {
					ProfileCategoryTypeDTO result = new ProfileCategoryTypeDTO();
					result.setId(j.getId());
					result.setLevel(j.getLevel());
					result.setKey(j.getKey());
					result.setValue(j.getValue());
			
					child.add(result);
				
				});
			
			profileCategoryTypeDTO.setChild(child);
		}
		profileCategoryTypeDTO.setProfileType(ProfileTypeMapper.mapProfileType(profileCategoryType.getProfileType()));
		
		return profileCategoryTypeDTO;
	}
}
