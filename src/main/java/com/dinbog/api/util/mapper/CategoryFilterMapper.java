package com.dinbog.api.util.mapper;

import java.util.ArrayList;
import java.util.List;

import com.dinbog.api.dto.CategoryFilterDTO;
import com.dinbog.api.dto.FilterDTO;
import com.dinbog.api.dto.FilterTypeDTO;
import com.dinbog.api.dto.FilterValueDTO;
import com.dinbog.api.models.entity.CategoryFilter;
import com.dinbog.api.models.entity.Filter;

public class CategoryFilterMapper {

	public static CategoryFilterDTO mapCategoryFilter(CategoryFilter categoryFilter) {
		
		CategoryFilterDTO categoryFilterDTO = new CategoryFilterDTO();
		categoryFilterDTO.setId(categoryFilter.getId());
		categoryFilterDTO.setOrder(categoryFilter.getOrder());
		categoryFilterDTO.setStatus(categoryFilter.getStatus());
		
		if(categoryFilter.getFilter() != null) {
			
			Filter filter = categoryFilter.getFilter();
			FilterDTO filterDTO = new FilterDTO();
			filterDTO.setId(filter.getId());
			filterDTO.setName(filter.getName());
			filterDTO.setDescription(filter.getDescription());
			
			if(filter.getParent() != null) {
				FilterDTO filterparentDTO = new FilterDTO();
				filterparentDTO.setId(filter.getParent().getId());
				filterparentDTO.setName(filter.getParent().getName());
				filterparentDTO.setDescription(filter.getParent().getDescription());
				filterDTO.setParent(filterparentDTO);
			}
			
			FilterTypeDTO filterTypeDTO = new FilterTypeDTO();
			filterTypeDTO.setName(filter.getFilterType().getName());
			filterDTO.setFilterType(filterTypeDTO);
			
			if(!filter.getFiltersValues().isEmpty()) {
				List<FilterValueDTO> values = new ArrayList<>();
				filter.getFiltersValues().forEach(v -> values.add(FilterValueMapper.mapFilterValue(v)));
				filterDTO.setFiltersValues(values);
				
			}
			categoryFilterDTO.setFilter(filterDTO);
		}
		return categoryFilterDTO;
	}

}
