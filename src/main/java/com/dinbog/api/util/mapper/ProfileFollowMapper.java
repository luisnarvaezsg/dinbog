package com.dinbog.api.util.mapper;

import com.dinbog.api.models.entity.ProfileFollow;
import com.dinbog.api.dto.ProfileFollowDTO;

public class ProfileFollowMapper {

	private ProfileFollowMapper() {
		super();
	}

	public static ProfileFollowDTO mapFollowActionMapper(ProfileFollow profileFollow, String typeFollow) {
		ProfileFollowDTO profileFollowDTO = new ProfileFollowDTO();
		profileFollowDTO.setId(profileFollow.getId());
		profileFollowDTO.setProfile(ProfileMapper.mapProfileSimple(
				(typeFollow.equals("followings")) ? profileFollow.getProfile() : profileFollow.getOwnerProfile(), null, null,null,null,profileFollow.getProfile().getProfileCategories()));
		return profileFollowDTO;
	}
}
