package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.Membership;
import com.dinbog.api.dto.MembershipDTO;

@Component
public class MembershipMapper {

	private MembershipMapper() {
		super();
	}

	public static MembershipDTO mapMembership(Membership membership) {

		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setId(membership.getId());
		membershipDTO.setName(membership.getName());
		membershipDTO.setCreated(membership.getCreated());

		return membershipDTO;
	}
}
