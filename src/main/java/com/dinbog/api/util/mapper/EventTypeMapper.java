package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.EventTypeDTO;
import com.dinbog.api.models.entity.EventType;

@Component
public class EventTypeMapper {

	private EventTypeMapper() {
		super();
	}

	public static EventTypeDTO mapEventType(EventType eventType) {

		EventTypeDTO eventTypeDTO = new EventTypeDTO();
		eventTypeDTO.setId(eventType.getId());
		eventTypeDTO.setValue(eventType.getValue());
		
		return eventTypeDTO;
	}
}
