package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.ProfileNotificationDTO;
import com.dinbog.api.models.entity.ProfileNotification;

@Component
public class ProfileNotificationMapper {

	private ProfileNotificationMapper() {
		super();
	}

	/**
	 * @param profileNotification
	 * @return
	 */
	public static ProfileNotificationDTO mapProfileNotification(ProfileNotification profileNotification) {

		ProfileNotificationDTO profileNotificationDTO = new ProfileNotificationDTO();
		profileNotificationDTO.setId(profileNotification.getId());
		profileNotificationDTO.setMessage(profileNotification.getMessage());
		profileNotificationDTO.setObjectId(profileNotification.getObjectId());
		profileNotificationDTO.setOwnerProfile(ProfileMapper.mapProfileBasic(profileNotification.getOwnerProfile()));
		profileNotificationDTO.setToProfile(ProfileMapper.mapProfileBasic(profileNotification.getToProfile()));
		profileNotificationDTO.setCreated(profileNotification.getCreated());

		return profileNotificationDTO;
	}

}
