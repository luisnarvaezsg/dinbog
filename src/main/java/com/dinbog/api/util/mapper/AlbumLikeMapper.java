package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.AlbumLikeDTO;
import com.dinbog.api.models.entity.AlbumLike;

@Component
public class AlbumLikeMapper {

	private AlbumLikeMapper() {
		super();
	}

	public static AlbumLikeDTO mapAlbumLike(AlbumLike albumLike) {

		AlbumLikeDTO albumLikeDTO = new AlbumLikeDTO();

		albumLikeDTO.setId(albumLike.getId());
		albumLikeDTO.setAlbumId(albumLike.getAlbum().getId());
		albumLikeDTO.setProfileId(albumLike.getProfile().getId());
		albumLikeDTO.setCreated(albumLike.getCreated());

		return albumLikeDTO;
	}
}
