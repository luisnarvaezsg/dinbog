/**
 * 
 */
package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.SocialAccount;
import com.dinbog.api.dto.SocialAccountDTO;

/**
 * @author Luis
 *
 */
@Component
public class SocialAccountMapper {

	/**
	 * 
	 */
	private SocialAccountMapper() {
		super();
	}

	public static SocialAccountDTO mapSocialAccount(SocialAccount socialAccount) {

		SocialAccountDTO socialAccountDTO = new SocialAccountDTO();
		socialAccountDTO.setId(socialAccount.getId());
		socialAccountDTO.setData(socialAccount.getData());
		socialAccountDTO.setCreated(socialAccount.getCreated());
		socialAccountDTO.setLastLogin(socialAccount.getLastLogin());
		socialAccountDTO.setSocialApp(SocialAppMapper.mapSocialAppBasic(socialAccount.getSocialApp()));

		return socialAccountDTO;
	}
}
