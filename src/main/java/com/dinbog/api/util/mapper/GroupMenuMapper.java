package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.GroupMenu;
import com.dinbog.api.dto.GroupMenuDTO;

@Component
public class GroupMenuMapper {

	private GroupMenuMapper() {
		super();
	}

	public static GroupMenuDTO mapGroupMenu(GroupMenu groupMenu) {
		GroupMenuDTO groupMenuDTO = new GroupMenuDTO();
		groupMenuDTO.setId(groupMenu.getId());
		groupMenuDTO.setGroup(GroupMapper.mapGroupBasic(groupMenu.getGroup()));
		groupMenuDTO.setMenu(MenuMapper.mapMenu(groupMenu.getMenu()));
		return groupMenuDTO;
	}
}
