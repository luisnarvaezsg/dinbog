package com.dinbog.api.util.mapper;

import com.dinbog.api.dto.GenderDTO;
import com.dinbog.api.models.entity.Gender;

public class GenderMapper {

	private GenderMapper() {
		super();
	}

	public static GenderDTO mapGender(Gender gender) {

		GenderDTO genderDTO = new GenderDTO();
		genderDTO.setId(gender.getId());
		genderDTO.setKey(gender.getKey());
		genderDTO.setValue(gender.getValue());
		
		return genderDTO;
	}
}
