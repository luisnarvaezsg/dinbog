package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.FieldDTO;
import com.dinbog.api.models.entity.Field;

@Component
public class FieldMapper {

	/**
	 * 
	 */
	private FieldMapper() {
		super();
	}

	public static FieldDTO mapField(Field field) {

		FieldDTO fieldDTO = new FieldDTO();
		fieldDTO.setId(field.getId());
		fieldDTO.setCode(field.getCode());
		fieldDTO.setName(field.getName());

		return fieldDTO;
	}
}
