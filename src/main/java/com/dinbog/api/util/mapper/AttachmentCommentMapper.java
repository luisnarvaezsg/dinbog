package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.AttachmentCommentDTO;
import com.dinbog.api.dto.AttachmentCommentResponseDTO;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentComment;
import com.dinbog.api.models.entity.CommentAttachment;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.util.Util;

@Component
public class AttachmentCommentMapper {

	private AttachmentCommentMapper() {
		super();
	}

	public static AttachmentCommentDTO mapAttachmentComment(CommentAttachment attachmentComment) {

		AttachmentCommentDTO attachmentCommentDTO = new AttachmentCommentDTO();

		attachmentCommentDTO.setValue(attachmentComment.getValue());
		attachmentCommentDTO.setProfileId(attachmentComment.getProfile().getId());

		return attachmentCommentDTO;
	}

	public static AttachmentCommentResponseDTO mapAttachmentCommentResponse(AttachmentComment attachmentComment) {

		AttachmentCommentResponseDTO attachmentCommentResponseDTO = new AttachmentCommentResponseDTO();
		attachmentCommentResponseDTO.setId(attachmentComment.getId());
		if (!Util.isEmpty(attachmentComment.getCommentAttachment().getParent())) {
			attachmentCommentResponseDTO.setParentId(attachmentComment.getCommentAttachment().getParent().getId());
		}
		attachmentCommentResponseDTO.setProfileId(attachmentComment.getCommentAttachment().getProfile().getId());
		attachmentCommentResponseDTO.setAttachmentId(attachmentComment.getAttachment().getId());
		attachmentCommentResponseDTO.setValue(attachmentComment.getCommentAttachment().getValue());
		attachmentCommentResponseDTO.setCreated(attachmentComment.getCommentAttachment().getCreated());

		return attachmentCommentResponseDTO;
	}

	public static AttachmentComment requestToEntity(AttachmentCommentDTO request) {

		AttachmentComment attachmentComment = new AttachmentComment();

		if (!Util.isEmpty(request.getParentId())) {
			AttachmentComment parent = new AttachmentComment();
			parent.setId(request.getParentId());
		}

		Profile profile = new Profile();
		profile.setId(request.getProfileId());
		
		Attachment attachment = new Attachment();
		attachment.setId(request.getAttachmentId());
		attachmentComment.setAttachment(attachment);

		return attachmentComment;
	}
}
