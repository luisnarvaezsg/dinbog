package com.dinbog.api.util.mapper;

import com.dinbog.api.dto.FilterValueDTO;
import com.dinbog.api.models.entity.FilterValue;

public class FilterValueMapper {

	public static FilterValueDTO mapFilterValue(FilterValue filterValue) {
		
		FilterValueDTO filterValueDTO = new FilterValueDTO();
		filterValueDTO.setId(filterValue.getId());
		filterValueDTO.setDescription(filterValue.getDescription());
		filterValueDTO.setValue(filterValue.getValue());
		return filterValueDTO;
	}

}
