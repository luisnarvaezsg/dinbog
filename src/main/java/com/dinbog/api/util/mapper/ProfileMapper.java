package com.dinbog.api.util.mapper;

import java.util.ArrayList;
import java.util.List;

import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.dto.ProfileBasicDTO;
import com.dinbog.api.dto.ProfileCategoryDTO;
import com.dinbog.api.dto.ProfileCategoryTypeDTO;
import com.dinbog.api.dto.ProfileCompanyDTO;
import com.dinbog.api.dto.ProfileFollowBasicDTO;
import com.dinbog.api.dto.ProfileFullDTO;
import com.dinbog.api.dto.ProfileFullRequestDTO;
import com.dinbog.api.dto.ProfileRequestDTO;
import com.dinbog.api.dto.ProfileSimpleDTO;
import com.dinbog.api.dto.ProfileStatisticsDTO;
import com.dinbog.api.dto.ProfileTalentDTO;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCompany;
import com.dinbog.api.models.entity.ProfileCountry;
import com.dinbog.api.models.entity.ProfileDetail;
import com.dinbog.api.models.entity.ProfileTalent;
import com.dinbog.api.util.Util;

public class ProfileMapper {

	private ProfileMapper() {
		super();
	}

	/**
	 * @param profile
	 * @param langCode
	 * @return
	 */
	public static ProfileTalentDTO mapProfile(Profile profile) {

		ProfileTalentDTO profileDTO = new ProfileTalentDTO();
		profileDTO.setId(profile.getId());
		profileDTO.setUser(UserMapper.mapUser(profile.getUser()));
		profileDTO.setMembership(MembershipMapper.mapMembership(profile.getMembership()));
		profileDTO.setProfileType(ProfileTypeMapper.mapProfileType(profile.getProfileType()));
		profileDTO.setUrlName(profile.getUrlName());
		profileDTO.setCountConections(profile.getCountConections());
		profileDTO.setCountFollow(profile.getCountFollow());
		profileDTO.setCountFollowers(profile.getCountFollowers());
		profileDTO.setCountLike(profile.getCountLike());
		profileDTO.setCountPost(profile.getCountPost());
		profileDTO.setCountViews(profile.getCountViews());
		profileDTO.setPhoneNumber(profile.getPhoneNumber());
		profileDTO.setSearchableSince(profile.getSearchableSince());
		profileDTO.setIsSearchable(profile.getIsSearchable());
		profileDTO.setAvatarPath(profile.getAvatarPath());
		profileDTO.setCoverPath(profile.getCoverPath());
		profileDTO.setCreated(profile.getCreated());

		for (ProfileCategory category : profile.getProfileCategories()) {
			ProfileCategoryTypeDTO profileCategoryDTO = new ProfileCategoryTypeDTO();
			profileCategoryDTO.setId(category.getProfileCategoryType().getId());
			profileCategoryDTO.setValue(category.getProfileCategoryType().getValue());
			profileDTO.getCategories().add(profileCategoryDTO);
		}

		return profileDTO;

	}

	/**
	 * @param profile
	 * @param langCode
	 * @return
	 */
	public static ProfileSimpleDTO mapProfileSimple(Profile profile, String firstName, String lastName, String gender,
			String residenceCity, List<ProfileCategory> listaCat) {

		ProfileSimpleDTO profileSimpleDTO = new ProfileSimpleDTO();
		profileSimpleDTO.setUrlName(profile.getUrlName());
		profileSimpleDTO.setId(profile.getId());
		profileSimpleDTO.setFirstName(firstName);
		profileSimpleDTO.setLastName(lastName);
		profileSimpleDTO.setFullName(profile.getFullName());
		profileSimpleDTO.setGender(gender);
		profileSimpleDTO.setProfileType(ProfileTypeMapper.mapProfileType(profile.getProfileType()));
		profileSimpleDTO.setCountLike(profile.getCountLike());
		profileSimpleDTO.setCountFollow(profile.getCountFollow());
		profileSimpleDTO.setCountFollowers(profile.getCountFollowers());
		profileSimpleDTO.setCountConections(profile.getCountConections());
		profileSimpleDTO.setAvatarPath(profile.getAvatarPath());
		String categories = "";
		if ((listaCat != null) && !listaCat.isEmpty()) {
			for (ProfileCategory profileCategory : listaCat) {
				categories += profileCategory.getProfileCategoryType().getValue() + ", ";
			}
			
			int indice = categories.lastIndexOf(",");
			categories = categories.substring(0, indice);
			profileSimpleDTO.setCategories(categories);
	
		}
		if (profile.getProfileCountries() != null && !profile.getProfileCountries().isEmpty()) {
			for (ProfileCountry profileCountry : profile.getProfileCountries()) {
				if(profileCountry.getProfileCountryType().getId() == 2) {
					profileSimpleDTO.setResidenceCountry(profileCountry.getCountry().getName());
					profileSimpleDTO.setResidenceCountryISOCode(profileCountry.getCountry().getIsoCode());
					profileSimpleDTO.setResidenceCity(profileCountry.getCity().getStateName());
				}
			}
		}
		return profileSimpleDTO;
	}

	/**
	 * @param profile
	 * @return
	 */
	public static ProfileBasicDTO mapProfileBasic(Profile profile) {

		ProfileBasicDTO profileBasicDTO = new ProfileBasicDTO();
		profileBasicDTO.setProfileId(profile.getId());
		profileBasicDTO.setUrlName(profile.getUrlName());
		profileBasicDTO.setFullName(profile.getFullName());
		profileBasicDTO.setAvatarPath(profile.getAvatarPath());
		profileBasicDTO.setCoverPath(profile.getCoverPath());

		return profileBasicDTO;
	}

	/**
	 * @param profile
	 * @return
	 */
	public static ProfileFollowBasicDTO mapProfileFollowBasic(Profile profile, Boolean follow,
			List<ProfileCategory> listaCat) {

		ProfileFollowBasicDTO profileBasicDTO = new ProfileFollowBasicDTO();
		profileBasicDTO.setProfileId(profile.getId());
		profileBasicDTO.setUrlName(profile.getUrlName());
		profileBasicDTO.setFullName(profile.getFullName());
		profileBasicDTO.setAvatarPath(profile.getAvatarPath());
		profileBasicDTO.setFollowed(follow);
		String categories = "";
		if ((listaCat != null) && !listaCat.isEmpty()) {
			for (ProfileCategory profileCategory : listaCat) {
				categories += profileCategory.getProfileCategoryType().getValue() + ", ";
			}
		}
		if (!profile.getProfileCountries().isEmpty()) {

			for (ProfileCountry profileCountry : profile.getProfileCountries()) {

				if (! profileCountry.getProfileCountryType().getName().equalsIgnoreCase("Origin")) {
					profileBasicDTO.setNombrePais(CountryMapper.mapCounty(profileCountry.getCountry()).getName());
					profileBasicDTO.setNombreCiudad(CityMapper.mapCity(profileCountry.getCity()).getStateName());
					profileBasicDTO.setISO(CountryMapper.mapCounty(profileCountry.getCountry()).getIsoCode());
				}
			}
		}
		int indice = categories.lastIndexOf(",");
		categories = categories.substring(0, indice);
		profileBasicDTO.setCategories(categories);
		return profileBasicDTO;
	}

	/**
	 * @param profile
	 * @param profileDetails
	 * @return
	 */
	public static ProfileFullDTO mapProfileFull(Profile profile, List<ProfileDetail> profileDetails) {

		ProfileFullDTO profileFullDTO = new ProfileFullDTO();
		profileFullDTO.setId(profile.getId());
		profileFullDTO.setUser(UserMapper.mapUser(profile.getUser()));
		profileFullDTO.setMembership(MembershipMapper.mapMembership(profile.getMembership()));
		profileFullDTO.setProfileType(ProfileTypeMapper.mapProfileType(profile.getProfileType()));
		profileFullDTO.setUrlName(profile.getUrlName());
		profileFullDTO.setFullName(profile.getFullName());
		profileFullDTO.setCountConections(profile.getCountConections());
		profileFullDTO.setCountFollow(profile.getCountFollow());
		profileFullDTO.setCountFollowers(profile.getCountFollowers());
		profileFullDTO.setCountLike(profile.getCountLike());
		profileFullDTO.setCountPost(profile.getCountPost());
		profileFullDTO.setCountViews(profile.getCountViews());
		profileFullDTO.setSearchableSince(profile.getSearchableSince());
		profileFullDTO.setIsSearchable(profile.getIsSearchable());
		profileFullDTO.setAvatarPath(profile.getAvatarPath());

		if (!profileDetails.isEmpty()) {
			profileFullDTO.setProfileDetails(ProfileDetailMapper.mapProfileDetailBasicList(profileDetails));
		}

		if (!profile.getProfileCountries().isEmpty()) {

			for (ProfileCountry profileCountry : profile.getProfileCountries()) {

				if (profileCountry.getProfileCountryType().getName().equalsIgnoreCase("Origin")) {
					profileFullDTO.setBornCountry(CountryMapper.mapCounty(profileCountry.getCountry()));
					profileFullDTO.setBornCity(CityMapper.mapCity(profileCountry.getCity()));
				} else {
					profileFullDTO.setResidenceCountry(CountryMapper.mapCounty(profileCountry.getCountry()));
					profileFullDTO.setResidenceCity(CityMapper.mapCity(profileCountry.getCity()));
				}
			}
			profileFullDTO.setProfileDetails(ProfileDetailMapper.mapProfileDetailBasicList(profileDetails));
		}

		if (!profile.getProfileCategories().isEmpty()) {

			ArrayList<ProfileCategoryDTO> categories = new ArrayList<>();
			for (ProfileCategory profileCategory : profile.getProfileCategories()) {
				categories.add(ProfileCategoryMapper.mapProfileCategory(profileCategory));
			}
			profileFullDTO.setProfileCategories(categories);
		}

		profileFullDTO.setCreated(profile.getCreated());

		return profileFullDTO;
	}

	/**
	 * @param profile
	 * @return
	 */
	public static ProfileStatisticsDTO mapProfileStatistics(Profile profile) {

		ProfileStatisticsDTO profileStatisticsDTO = new ProfileStatisticsDTO();
		profileStatisticsDTO.setCountConections(profile.getCountConections());
		profileStatisticsDTO.setCountFollow(profile.getCountFollow());
		profileStatisticsDTO.setCountFollowers(profile.getCountFollowers());
		profileStatisticsDTO.setCountLike(profile.getCountLike());
		profileStatisticsDTO.setCountPost(profile.getCountPost());

		return profileStatisticsDTO;
	}

	/**
	 * @param profile
	 * @param request
	 * @return
	 */
	public static ProfileTalent requestToEntityTalent(ProfileRequestDTO request) {

		ProfileTalent profileTalent = new ProfileTalent();
		Profile profile = new Profile();
		profileTalent.setProfile(profile);

		profileTalent.getProfile().setUrlName(request.getUrlName());
		profileTalent.setBirthDate(request.getBirthDate());
		profileTalent.getProfile().setSearchableSince(request.getSearchableSince());
		profileTalent.getProfile().setIsSearchable(request.getIsSearchable());
		profileTalent.getProfile().setAvatarPath(request.getAvatarPath());
		profileTalent.getProfile().setCoverPath(request.getCoverPath());
		profileTalent.getProfile().setCountLike(0);
		profileTalent.getProfile().setCountFollow(0);
		profileTalent.getProfile().setCountFollowers(0);
		profileTalent.getProfile().setCountConections(0);
		profileTalent.getProfile().setCountPost(0);
		profileTalent.getProfile().setCountViews(0);
		profileTalent.getProfile().setRegistrationStep(0L);
		profileTalent.getProfile().setPhoneNumber(request.getPhoneNumber());

		return profileTalent;
	}

	/**
	 * @param request
	 * @return
	 */
	public static Profile requestToEntity(Profile profile, ProfileFullRequestDTO request) {

		// DATOS PRINCIPALES
		if (!Util.isEmpty(request.getUrlName())) {
			profile.setUrlName(request.getUrlName());
		}
		if (!Util.isEmpty(request.getIsSearchable())) {
			profile.setIsSearchable(request.getIsSearchable());
		}
		if (!Util.isEmpty(request.getSearchableSince())) {
			profile.setSearchableSince(request.getSearchableSince());
		}

		// CONTADORES
		if (!Util.isEmpty(request.getCountLike())) {
			profile.setCountLike(request.getCountLike());
		}
		if (!Util.isEmpty(request.getCountFollow())) {
			profile.setCountFollow(request.getCountFollow());
		}
		if (!Util.isEmpty(request.getCountFollowers())) {
			profile.setCountFollowers(request.getCountFollowers());
		}
		if (!Util.isEmpty(request.getCountConections())) {
			profile.setCountConections(request.getCountConections());
		}
		if (!Util.isEmpty(request.getCountPost())) {
			profile.setCountPost(request.getCountPost());
		}
		if (!Util.isEmpty(request.getCountViews())) {
			profile.setCountViews(request.getCountViews());
		}

		// OTROS DATOS
		if (!Util.isEmpty(request.getRegistrationStep())) {
			profile.setRegistrationStep(request.getRegistrationStep());
		}
		if (!Util.isEmpty(request.getAvatarPath())) {
			profile.setAvatarPath(request.getAvatarPath());
		}
		if (!Util.isEmpty(request.getCoverPath())) {
			profile.setCoverPath(request.getCoverPath());
		}

		return profile;
	}

	/**
	 * @param profile
	 * @param langCode
	 * @return
	 */
	public static ProfileTalentDTO mapProfileTalent(ProfileTalent profileTalent) {

		ProfileTalentDTO profileDTO = new ProfileTalentDTO();
		profileDTO.setId(profileTalent.getProfile().getId());
		profileDTO.setFullName(profileTalent.getProfile().getFullName());
		profileDTO.setFirstName(profileTalent.getFirstName());
		profileDTO.setLastName(profileTalent.getLastName());
		profileDTO.setFullName(profileTalent.getProfile().getFullName());
		profileDTO.setUser(UserMapper.mapUser(profileTalent.getProfile().getUser()));
		profileDTO.setMembership(MembershipMapper.mapMembership(profileTalent.getProfile().getMembership()));
		profileDTO.setProfileType(ProfileTypeMapper.mapProfileType(profileTalent.getProfile().getProfileType()));
		profileDTO.setUrlName(profileTalent.getProfile().getUrlName());
		profileDTO.setCountConections(profileTalent.getProfile().getCountConections());
		profileDTO.setCountFollow(profileTalent.getProfile().getCountFollow());
		profileDTO.setCountFollowers(profileTalent.getProfile().getCountFollowers());
		profileDTO.setCountLike(profileTalent.getProfile().getCountLike());
		profileDTO.setCountPost(profileTalent.getProfile().getCountPost());
		profileDTO.setCountViews(profileTalent.getProfile().getCountViews());
		profileDTO.setPhoneNumber(profileTalent.getProfile().getPhoneNumber());
		profileDTO.setSearchableSince(profileTalent.getProfile().getSearchableSince());
		profileDTO.setIsSearchable(profileTalent.getProfile().getIsSearchable());
		profileDTO.setAvatarPath(profileTalent.getProfile().getAvatarPath());
		profileDTO.setCoverPath(profileTalent.getProfile().getCoverPath());
		profileDTO.setCreated(profileTalent.getProfile().getCreated());
		profileDTO.setBirthDate(profileTalent.getBirthDate());

		for (ProfileCategory category : profileTalent.getProfile().getProfileCategories()) {
			ProfileCategoryTypeDTO profileCategoryDTO = new ProfileCategoryTypeDTO();
			profileCategoryDTO.setId(category.getProfileCategoryType().getId());
			profileCategoryDTO.setValue(category.getProfileCategoryType().getValue());
			profileDTO.getCategories().add(profileCategoryDTO);
		}

		profileDTO.setGender(GenderMapper.mapGender(profileTalent.getGender()));

		for (ProfileCountry countryProfile : profileTalent.getProfileCountries()) {
			if (countryProfile.getProfileCountryType().getId().equals(ProfileConstant.COUNTRY_ORIGIN_TYPE)) {
				profileDTO.setBornCountry(CountryMapper.mapCounty(countryProfile.getCountry()));
				profileDTO.setBornCity(CityMapper.mapCity(countryProfile.getCity()));
			} else {
				profileDTO.setResidenceCountry(CountryMapper.mapCounty(countryProfile.getCountry()));
				profileDTO.setResidenceCity(CityMapper.mapCity(countryProfile.getCity()));
			}

		}

		return profileDTO;

	}

	/**
	 * @param profile
	 * @param request
	 * @return
	 */
	public static ProfileCompany requestToEntityCompany(ProfileRequestDTO request) {

		ProfileCompany profileCompany = new ProfileCompany();
		Profile profile = new Profile();
		profileCompany.setProfile(profile);

		profileCompany.getProfile().setUrlName(request.getUrlName());
		profileCompany.getProfile().setSearchableSince(request.getSearchableSince());
		profileCompany.getProfile().setIsSearchable(request.getIsSearchable());
		profileCompany.getProfile().setAvatarPath(request.getAvatarPath());
		profileCompany.getProfile().setCoverPath(request.getCoverPath());
		profileCompany.getProfile().setCountLike(0);
		profileCompany.getProfile().setCountFollow(0);
		profileCompany.getProfile().setCountFollowers(0);
		profileCompany.getProfile().setCountConections(0);
		profileCompany.getProfile().setCountPost(0);
		profileCompany.getProfile().setCountViews(0);
		profileCompany.getProfile().setPhoneNumber(request.getPhoneNumber());

		return profileCompany;
	}

	/**
	 * @param profile
	 * @param langCode
	 * @return
	 */
	public static ProfileCompanyDTO mapProfileCompany(ProfileCompany profileCompany) {

		ProfileCompanyDTO profileDTO = new ProfileCompanyDTO();
		profileDTO.setId(profileCompany.getProfile().getId());
		profileDTO.setFullName(profileCompany.getProfile().getFullName());
		profileDTO.setDescription(profileCompany.getDescription());
		profileDTO.setUser(UserMapper.mapUser(profileCompany.getProfile().getUser()));
		profileDTO.setMembership(MembershipMapper.mapMembership(profileCompany.getProfile().getMembership()));
		profileDTO.setProfileType(ProfileTypeMapper.mapProfileType(profileCompany.getProfile().getProfileType()));
		profileDTO.setUrlName(profileCompany.getProfile().getUrlName());
		profileDTO.setCountConections(profileCompany.getProfile().getCountConections());
		profileDTO.setCountFollow(profileCompany.getProfile().getCountFollow());
		profileDTO.setCountFollowers(profileCompany.getProfile().getCountFollowers());
		profileDTO.setCountLike(profileCompany.getProfile().getCountLike());
		profileDTO.setCountPost(profileCompany.getProfile().getCountPost());
		profileDTO.setCountViews(profileCompany.getProfile().getCountViews());
		profileDTO.setPhoneNumber(profileCompany.getProfile().getPhoneNumber());
		profileDTO.setSearchableSince(profileCompany.getProfile().getSearchableSince());
		profileDTO.setIsSearchable(profileCompany.getProfile().getIsSearchable());
		profileDTO.setAvatarPath(profileCompany.getProfile().getAvatarPath());
		profileDTO.setCoverPath(profileCompany.getProfile().getCoverPath());
		profileDTO.setCreated(profileCompany.getProfile().getCreated());

		for (ProfileCategory category : profileCompany.getProfile().getProfileCategories()) {
			ProfileCategoryTypeDTO profileCategoryDTO = new ProfileCategoryTypeDTO();
			profileCategoryDTO.setId(category.getProfileCategoryType().getId());
			profileCategoryDTO.setValue(category.getProfileCategoryType().getValue());
			profileDTO.getCategories().add(profileCategoryDTO);
		}

		profileDTO.setCompanyName((profileCompany.getCompanyName()));
		profileDTO.setDescription(profileCompany.getDescription());

		for (ProfileCountry countryProfile : profileCompany.getProfileCountries()) {
			if (countryProfile.getProfileCountryType().getId().equals(ProfileConstant.COUNTRY_ORIGIN_TYPE)) {
				profileDTO.setHeadQuarterCountry(CountryMapper.mapCounty(countryProfile.getCountry()));
				profileDTO.setHeadQuarterCity(CityMapper.mapCity(countryProfile.getCity()));
			} else {
				profileDTO.getBranches().add(CountryMapper.mapCounty(countryProfile.getCountry()));
			}

		}

		return profileDTO;

	}
}
