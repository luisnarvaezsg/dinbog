package com.dinbog.api.util.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.UserEmail;
import com.dinbog.api.dto.UserEmailDTO;
import com.dinbog.api.util.Util;

@Component
public class UserEmailMapper {

	/**
	 * 
	 */
	private UserEmailMapper() {
		super();
	}

	public static UserEmailDTO mapUserEmail(UserEmail userEmail) {

		UserEmailDTO userEmailDTO = new UserEmailDTO();

		userEmailDTO.setId(userEmail.getId());
		userEmailDTO.setLevel(userEmail.getLevel());
		userEmailDTO.setEmail(userEmail.getEmail());
		userEmailDTO.setIsConfirmed(userEmail.getConfirmed());
		userEmailDTO.setCreated(userEmail.getCreated());

		return userEmailDTO;
	}

	public static List<UserEmailDTO> mapUserEmailList(List<UserEmail> userEmails) {

		List<UserEmailDTO> responseList = new ArrayList<>();

		if (!Util.isEmpty(userEmails))
			userEmails.forEach(i -> responseList.add(mapUserEmail(i)));

		return responseList;
	}
}
