package com.dinbog.api.util.mapper;

import com.dinbog.api.dto.NotificationActionDTO;
import com.dinbog.api.models.entity.NotificationAction;

public class NotificationActionMapper {

	private NotificationActionMapper() {
		super();
	}

	public static NotificationActionDTO mapEntityToDto(NotificationAction notificationAction) {

		NotificationActionDTO notificationActionDTO = new NotificationActionDTO();
		notificationActionDTO.setId(notificationAction.getId());
		notificationActionDTO.setValue(notificationAction.getValue());
		notificationActionDTO.setNotificationType(notificationAction.getNotificationType().getValue());

		return notificationActionDTO;
	}
}
