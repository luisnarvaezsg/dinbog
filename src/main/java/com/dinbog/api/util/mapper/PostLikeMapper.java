package com.dinbog.api.util.mapper;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.PostLikeResponseDTO;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostLike;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.util.Util;

@Component
public class PostLikeMapper {

	private PostLikeMapper() {
		super();
	}

	public static PostLikeResponseDTO mapPostLikeResponse(PostLike postLike) {

		PostLikeResponseDTO postLikeResponseDTO = new PostLikeResponseDTO();
		postLikeResponseDTO.setId(postLike.getId());
		postLikeResponseDTO.setPostId(postLike.getPost().getId());
		postLikeResponseDTO.setProfileId(postLike.getProfile().getId());
		postLikeResponseDTO.setCreated(postLike.getCreated());

		return postLikeResponseDTO;
	}

	public static PostLike requestToEntity(PostLikeResponseDTO request) {

		PostLike postLike = new PostLike();
		if (!Util.isEmpty(request.getId())) {
			postLike.setId(request.getId());
		}
		Post post = new Post();
		post.setId(request.getPostId());
		postLike.setPost(post);

		Profile profile = new Profile();
		profile.setId(request.getProfileId());
		postLike.setProfile(profile);

		postLike.setCreated(new Date());

		return postLike;
	}
}
