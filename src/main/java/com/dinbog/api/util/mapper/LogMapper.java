package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.LogDTO;
import com.dinbog.api.models.entity.Log;

@Component
public class LogMapper {

	/**
	 * 
	 */
	private LogMapper() {
		super();
	}

	public static LogDTO mapLog(Log log) {
		LogDTO logDTO = new LogDTO();
		logDTO.setId(log.getId());
		logDTO.setUserId(log.getUser().getId());
		logDTO.setIsError(log.getIsError());
		logDTO.setIsWarning(log.getIsWarning());
		logDTO.setMessage(log.getMessage());
		logDTO.setCreated(log.getCreated());

		return logDTO;
	}
}
