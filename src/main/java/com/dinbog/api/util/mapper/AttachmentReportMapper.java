package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.AttachmentReportDTO;
import com.dinbog.api.models.entity.AttachmentReport;

@Component
public class AttachmentReportMapper {

	AttachmentReportMapper() {
		super();
	}

	public static AttachmentReportDTO mapAttachmentReport(AttachmentReport attachmentReport) {

		AttachmentReportDTO attachmentReportDTO = new AttachmentReportDTO();
		attachmentReportDTO.setReportType(ReportTypeMapper.mapReportType(attachmentReport.getReportType()));
		attachmentReportDTO.setId(attachmentReport.getId());
		attachmentReportDTO.setDetail(attachmentReport.getDetail());
		attachmentReportDTO.setOwner(ProfileMapper.mapProfileSimple(attachmentReport.getOwner(),null,null,null,null,null));
		attachmentReportDTO.setAttachment(AttachmentMapper.mapAttachment(attachmentReport.getAttachment()));

		return attachmentReportDTO;
	}
}
