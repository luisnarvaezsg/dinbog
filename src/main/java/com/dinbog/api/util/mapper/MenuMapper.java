package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.Menu;
import com.dinbog.api.dto.MenuDTO;

@Component
public class MenuMapper {

	private MenuMapper() {
		super();
	}

	public static MenuDTO mapMenu(Menu menu) {
		MenuDTO menuDTO = new MenuDTO();
		menuDTO.setId(menu.getId());
		menuDTO.setName(menu.getName());
		menuDTO.setRoute(menu.getRoute());
		return menuDTO;
	}
}
