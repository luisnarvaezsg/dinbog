package com.dinbog.api.util.mapper;

import java.util.Date;
import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.util.StringUtils;

import com.dinbog.api.dto.PostBasicDTO;
import com.dinbog.api.dto.PostDTO;
import com.dinbog.api.dto.PostEditDTO;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.util.Util;

public class PostMapper {

	private PostMapper() {
		super();
	}

	public static PostDTO mapPost(Profile proUser, Post post, Boolean mine, Boolean liked, Long id, String firstname,
			String lastname, String gender, String residenceCity, List<Profile> listaProf,
			List<ProfileCategory> listaCat) {

		PostDTO postDTO = new PostDTO();

		postDTO.setId(post.getId());
		postDTO.setContent(post.getContent());
		postDTO.setProgramed(post.getProgramed());
		postDTO.setCountLike(post.getCountLike());
		postDTO.setCountComment(post.getCountComment());
		postDTO.setCreated(post.getCreated());
		postDTO.setProgramedDateGmt(post.getProgramedDateGmt());
		Profile profile = post.getProfile();
		if (post.getListaPostAttachment() == null || post.getListaPostAttachment().isEmpty()) {
			postDTO.setAttachments(Lists.mutable.empty());
		} else {
			postDTO.setAttachments(AttachmentMapper.mapPostAttachmentsList(post.getListaPostAttachment()));
		}
		if (post.getListaPostComment() == null || post.getListaPostComment().isEmpty()) {
			postDTO.setComments(Lists.mutable.empty());
		} else {
			postDTO.setComments(CommentsPostMapper.mapPostCommentsList(post.getListaPostComment(), proUser, listaProf));
		}

		postDTO.setProfile(
				ProfileMapper.mapProfileSimple(profile, firstname, lastname, gender, residenceCity, listaCat));
		postDTO.setLiked(liked);
		postDTO.setMine(mine);
		return postDTO;
	}

	public static PostBasicDTO mapPost(Post post) {

		PostBasicDTO postBasicDTO = new PostBasicDTO();

		postBasicDTO.setId(post.getId());
		postBasicDTO.setContent(post.getContent());
		postBasicDTO.setProgramed(post.getProgramed());

		return postBasicDTO;
	}

	public static Post requestEditToEntity(PostEditDTO request) {

		Post post = new Post();

		if (StringUtils.hasText(request.getContent())) {
			post.setContent(request.getContent());
		}

		if (!Util.isEmpty(request.getProgramed())) {
			post.setProgramed(request.getProgramed());
		}

		if (!Util.isEmpty(request.getProgramedDateGmt())) {
			post.setProgramedDateGmt(request.getProgramedDateGmt());
		}

		post.setCreated(new Date());

		return post;
	}
}
