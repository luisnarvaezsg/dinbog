package com.dinbog.api.util.mapper;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.CommentsPostDTO;
import com.dinbog.api.dto.ProfileBasicDTO;
import com.dinbog.api.models.entity.CommentsPost;
import com.dinbog.api.models.entity.PostComment;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.text.analyzer.TextAnalyzer;

@Service
public class CommentsPostMapper {

	public CommentsPostMapper() {
		super();
	}

	public static CommentsPostDTO mapCommentDTO(CommentsPost comment, Profile proUser, List<Profile> listaProf ) {

		CommentsPostDTO comD = new CommentsPostDTO();
		comD.setCountLike(comment.getCountLike());
		comD.setCreated(comment.getCreated());
		comD.setId(comment.getId());
		if (comment.getParent() != null)
			comD.setParentId(comment.getParent().getId());
		comD.setValue(comment.getValue());
		TextAnalyzer analyzer2 = new TextAnalyzer(comment.getValue());
		List<Long> mentions = analyzer2.getMentions();
		analyzer2.close();
		List<ProfileBasicDTO> listaMention=new ArrayList<>();
		if (!mentions.isEmpty()) {
			mentions.forEach(id->
				listaProf.forEach(profile->{
					if (id.longValue()==profile.getId().longValue()) {
						ProfileBasicDTO pbd= new ProfileBasicDTO();
						pbd.setFullName(profile.getFullName());
						pbd.setProfileId(profile.getId());
						pbd.setUrlName(profile.getUrlName());
						pbd.setAvatarPath(profile.getAvatarPath());
						listaMention.add(pbd);
					}
				}));
			comD.setListaMention(listaMention);
		}
		comD.setLiked(false);
		if (comment.getListaComment() != null && !comment.getListaComment().isEmpty()) {
			comD.setListaComment(mapCommentsList(comment.getListaComment(), proUser, listaProf));
		}
		if (comment.getListaLike() != null && !comment.getListaLike().isEmpty()) {
			if (comment.getListaLike().stream().anyMatch(
					commentPostLike -> commentPostLike.getProfile().getId().longValue() == proUser.getId().longValue()))
				comD.setLiked(true);
		}
		
		comD.setFullName(comment.getProfile().getFullName());
		comD.setAvatar(comment.getProfile().getAvatarPath());
		comD.setProfileId(comment.getProfile().getId());
		comD.setUrlName(comment.getProfile().getUrlName());

		return comD;
	}

	/**
	 * Mapea los comentarios del post
	 * 
	 * @param postCommentsList
	 * @return
	 */
	public static MutableList<CommentsPostDTO> mapPostCommentsList(List<PostComment> postCommentsList,
			Profile proUser, List<Profile> listaProf ) {

		MutableList<CommentsPostDTO> listaComments = Lists.mutable.empty();

		if (!Util.isEmpty(postCommentsList)) {
			Integer longitud=postCommentsList.size();
			if (longitud>3)
				longitud=3;
			for (int i=0;i<longitud;i++) 
				listaComments.add(mapCommentDTO(postCommentsList.get(i).getCommentsPost(), proUser, listaProf));
		}

		return listaComments;
	}

	public static MutableList<CommentsPostDTO> mapCommentsList(List<CommentsPost> commentsList, Profile proUser, List<Profile> listaProf ) {

		MutableList<CommentsPostDTO> listaComments = Lists.mutable.empty();

		if (!Util.isEmpty(commentsList)) {
			commentsList.forEach(comment -> listaComments.add(mapCommentDTO(comment, proUser, listaProf)));
		}

		return listaComments;
	}

}
