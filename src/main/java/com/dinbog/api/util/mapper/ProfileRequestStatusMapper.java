package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.ProfileRequestStatus;
import com.dinbog.api.dto.ProfileRequestStatusDTO;

@Component
public class ProfileRequestStatusMapper {

	private ProfileRequestStatusMapper() {
		super();
	}

	public static ProfileRequestStatusDTO mapProfileRequestStatus(ProfileRequestStatus profileRequestStatus) {

		ProfileRequestStatusDTO profileRequestStatusDTO = new ProfileRequestStatusDTO();
		profileRequestStatusDTO.setId(profileRequestStatus.getId());
		profileRequestStatusDTO.setValue( profileRequestStatus.getValue());
		return profileRequestStatusDTO;
	}
}
