package com.dinbog.api.util.mapper;

import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.models.entity.ProfileDetail;
import com.dinbog.api.dto.ProfileDetailBasicDTO;
import com.dinbog.api.dto.ProfileDetailDTO;
import com.dinbog.api.util.Util;

public class ProfileDetailMapper {

	private ProfileDetailMapper() {
		super();
	}

	/**
	 * @param profileDetail
	 * @return
	 */
	public static ProfileDetailDTO mapProfileDetail(ProfileDetail profileDetail) {

		ProfileDetailDTO profileDetailDTO = new ProfileDetailDTO();
		profileDetailDTO.setId(profileDetail.getId());
		if (!Util.isEmpty(profileDetail.getProfile())) {
			profileDetailDTO.setProfileId(profileDetail.getProfile().getId());
		}
		if (!Util.isEmpty(profileDetail.getFieldCategory())) {
			profileDetailDTO.setFieldCategoryId(profileDetail.getFieldCategory().getId());
		}
		profileDetailDTO.setValue(profileDetail.getValue());
		profileDetailDTO.setCreated(profileDetail.getCreated());

		return profileDetailDTO;
	}

	/**
	 * @param profileDetail
	 * @return
	 */
	public static ProfileDetailBasicDTO mapProfileDetailBasic(ProfileDetail profileDetail) {

		ProfileDetailBasicDTO profileDetailDTO = new ProfileDetailBasicDTO();
		profileDetailDTO.setId(profileDetail.getId());
		if (!Util.isEmpty(profileDetail.getFieldCategory())) {
			profileDetailDTO.setFieldCategoryId(profileDetail.getFieldCategory().getId());
		}
		profileDetailDTO.setValue(profileDetail.getValue());

		return profileDetailDTO;
	}

	/**
	 * @param profileDetails
	 * @return
	 */
	public static MutableList<ProfileDetailBasicDTO> mapProfileDetailBasicList(List<ProfileDetail> profileDetails) {

		MutableList<ProfileDetailBasicDTO> responseList = Lists.mutable.empty();

		if (!Util.isEmpty(profileDetails))
			profileDetails.forEach(i -> responseList.add(mapProfileDetailBasic(i)));

		return responseList;
	}

	/**
	 * @param profileDetails
	 * @return
	 */
	public static MutableList<ProfileDetailDTO> mapProfileDetailList(List<ProfileDetail> profileDetails) {

		MutableList<ProfileDetailDTO> responseList = Lists.mutable.empty();

		if (!Util.isEmpty(profileDetails))
			profileDetails.forEach(i -> responseList.add(mapProfileDetail(i)));

		return responseList;
	}
}
