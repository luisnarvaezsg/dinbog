package com.dinbog.api.util.mapper;

import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.ProfileBlockBasicDTO;
import com.dinbog.api.dto.ProfileBlockDTO;
import com.dinbog.api.models.entity.ProfileBlock;
import com.dinbog.api.util.Util;

public class ProfileBlockMapper {

	private ProfileBlockMapper() {
		super();
	}

	/**
	 * @param profileBlock
	 * @return
	 */
	public static ProfileBlockDTO mapProfileBlock(ProfileBlock profileBlock) {

		ProfileBlockDTO profileBlockDTO = new ProfileBlockDTO();
		profileBlockDTO.setId(profileBlock.getId());
		profileBlockDTO.setOwnerProfile(ProfileMapper.mapProfileSimple(profileBlock.getOwnerProfile(),null,null,null,null,null));
		profileBlockDTO.setProfile(ProfileMapper.mapProfileSimple(profileBlock.getProfile(), null,null,null,null,null));
		profileBlockDTO.setCreated(profileBlock.getCreated());

		return profileBlockDTO;
	}

	/**
	 * @param profileBlock
	 * @return
	 */
	public static ProfileBlockBasicDTO mapProfileBlockBasic(ProfileBlock profileBlock) {

		ProfileBlockBasicDTO profileBlockBasicDTO = new ProfileBlockBasicDTO();
		profileBlockBasicDTO.setBlockId(profileBlock.getId());
		profileBlockBasicDTO.setBlockedProfile(ProfileMapper.mapProfileBasic(profileBlock.getOwnerProfile()));

		return profileBlockBasicDTO;
	}

	/**
	 * @param profileBlock
	 * @return
	 */
	public static MutableList<ProfileBlockDTO> mapProfileBlockList(List<ProfileBlock> profileBlock) {

		MutableList<ProfileBlockDTO> responseList = Lists.mutable.empty();

		if (!Util.isEmpty(profileBlock))
			profileBlock.forEach(i -> responseList.add(mapProfileBlock(i)));

		return responseList;
	}

	/**
	 * @param profileBlock
	 * @return
	 */
	public static MutableList<ProfileBlockBasicDTO> mapProfileBlockBasicList(List<ProfileBlock> profileBlock) {

		MutableList<ProfileBlockBasicDTO> responseList = Lists.mutable.empty();

		if (!Util.isEmpty(profileBlock))
			profileBlock.forEach(i -> responseList.add(mapProfileBlockBasic(i)));

		return responseList;
	}
}
