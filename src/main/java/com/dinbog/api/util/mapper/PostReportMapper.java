package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.PostReportDTO;
import com.dinbog.api.models.entity.PostReport;

@Component
public class PostReportMapper {

	private PostReportMapper() {
		super();
	}

	public static PostReportDTO mapPostReport(PostReport postReport) {

		PostReportDTO postReportDTO = new PostReportDTO();
		postReportDTO.setId(postReport.getId());
		postReportDTO.setDetail(postReport.getDetail());
		postReportDTO.setPost(PostMapper.mapPost(postReport.getPost()));
		postReportDTO.setOwner(ProfileMapper.mapProfile(postReport.getOwner()));
		postReportDTO.setReportType(ReportTypeMapper.mapReportType(postReport.getReportType()));
		postReportDTO.setCreated(postReport.getCreated());

		return postReportDTO;
	}
}
