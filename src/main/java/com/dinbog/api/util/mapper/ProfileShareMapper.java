package com.dinbog.api.util.mapper;

import com.dinbog.api.models.entity.ProfileShare;
import com.dinbog.api.dto.ProfileShareDTO;

public class ProfileShareMapper {

	private ProfileShareMapper() {
		super();
	}

	public static ProfileShareDTO mapEntityToDtoMapper(ProfileShare profileShare) {

		ProfileShareDTO profileShareDTO = new ProfileShareDTO();
		profileShareDTO.setId(profileShare.getId());
		profileShareDTO.setOwnerProfile(ProfileMapper.mapProfileSimple(profileShare.getOwnerProfile(),null,null,null,null,null));
		profileShareDTO.setProfile(ProfileMapper.mapProfileSimple(profileShare.getProfile(),null,null,null,null,null));
		profileShareDTO.setCreated(profileShare.getCreated());

		return profileShareDTO;
	}
}
