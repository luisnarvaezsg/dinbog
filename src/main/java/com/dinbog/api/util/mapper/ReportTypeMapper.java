package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.ReportTypeDTO;
import com.dinbog.api.dto.ReportTypeRequestDTO;
import com.dinbog.api.dto.ReportTypeResponseDTO;
import com.dinbog.api.models.entity.ReportType;

@Component
public class ReportTypeMapper {

	private ReportTypeMapper() {
		super();
	}

	/**
	 * @param reportType
	 * @param langCode
	 * @return
	 */
	public static ReportTypeDTO mapReportType(ReportType reportType) {

		ReportTypeDTO reportTypeDTO = new ReportTypeDTO();
		reportTypeDTO.setId(reportType.getId());
				return reportTypeDTO;
	}

	/**
	 * @param request
	 * @return
	 */
	public static ReportType mapRequestToEntity(ReportTypeRequestDTO request) {

		ReportType reportType = new ReportType();

		reportType.setValue(request.getValue());
		reportType.setValueSpa(request.getValueSpa());
		reportType.setValueFra(request.getValueFra());
		reportType.setValueIta(request.getValueIta());
		reportType.setValuePor(request.getValuePor());
		reportType.setValueRus(request.getValueRus());
		reportType.setValueChi(request.getValueChi());

		return reportType;
	}

	/**
	 * @param reportType
	 * @return
	 */
	public static ReportTypeResponseDTO mapReportTypeResponse(ReportType reportType) {

		ReportTypeResponseDTO response = new ReportTypeResponseDTO();

		response.setId(reportType.getId());
		response.setValue(reportType.getValue());
		response.setValueSpa(reportType.getValueSpa());
		response.setValueFra(reportType.getValueFra());
		response.setValueIta(reportType.getValueIta());
		response.setValuePor(reportType.getValuePor());
		response.setValueRus(reportType.getValueRus());
		response.setValueChi(reportType.getValueChi());

		return response;
	}
}
