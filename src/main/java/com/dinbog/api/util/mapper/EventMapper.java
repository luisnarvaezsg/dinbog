package com.dinbog.api.util.mapper;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.EventDTO;
import com.dinbog.api.dto.EventRequestDTO;
import com.dinbog.api.dto.ProfileCategoryTypeDTO;
import com.dinbog.api.models.entity.Event;
import com.dinbog.api.models.entity.EventProfileCategoryType;
import com.dinbog.api.util.Util;

@Component
public class EventMapper {

	private EventMapper() {
		super();
	}

	public static EventDTO mapEvent(Event event) {
		EventDTO eventDTO = new EventDTO();
		eventDTO.setId(event.getId());
		eventDTO.setAddress(event.getAddress());
		eventDTO.setAgeFrom(event.getAgeFrom());
		eventDTO.setAgeTo(event.getAgeTo());
		eventDTO.setNameEvent(event.getNameEvent());
		eventDTO.setDescription(event.getDescription());
		eventDTO.setAnnouncementDateEvent(event.getAnnouncementDateEvent());
		eventDTO.setCountParticipant(event.getCountParticipant());
		eventDTO.setStartDateEvent(event.getStartDateEvent());
		eventDTO.setEndDateEvent(event.getEndDateEvent());
		//eventDTO.setCoverAttachment(AttachmentMapper.mapAttachmentBasic(event.getCoverAttachment()));
		eventDTO.setCoverAttachment(event.getCoverAttachment());
		
		eventDTO.setParticipantPrivate(event.getParticipantPrivate());
		eventDTO.setPositionX(event.getPositionX());
		eventDTO.setPositionY(event.getPositionY());
		eventDTO.setPublishDate(event.getPublishDate());
		eventDTO.setCreated(event.getCreated());
		eventDTO.setUpdated(event.getUpdated());
		eventDTO.setOwner(ProfileMapper.mapProfileBasic(event.getOwner()));
		if(event.getCity() != null) {
			eventDTO.setCity(CityMapper.mapCity(event.getCity()));
		}
		if(!event.getEventProfileCategoryTypes().isEmpty()) {
			ArrayList<ProfileCategoryTypeDTO> categories = new ArrayList<>();
			for (EventProfileCategoryType eventProfileCategoryType : event.getEventProfileCategoryTypes()) {
				categories.add(ProfileCategoryTypeMapper.mapProfileCategoryType(eventProfileCategoryType.getProfileCategoryType(), false));
			}
			eventDTO.setCategories(categories);
		}
		eventDTO.setCountry(CountryMapper.mapCounty(event.getCountry()));
		eventDTO.setEventType(EventTypeMapper.mapEventType(event.getEventType()));
		eventDTO.setEventStatus(EventStatusMapper.mapEventStatus(event.getEventStatus()));
		eventDTO.setProfileType(ProfileTypeMapper.mapProfileType(event.getProfileType()));
		return eventDTO;
	}

	public static Event requestToEntity(Event event, EventRequestDTO request) {

		if (!Util.isEmpty(request.getAddress())) {
			event.setAddress(request.getAddress());
		}

		if (!Util.isEmpty(request.getAgeFrom())) {
			event.setAgeFrom(request.getAgeFrom());
		}

		if (!Util.isEmpty(request.getAgeTo())) {
			event.setAgeTo(request.getAgeTo());
		}

		if (!Util.isEmpty(request.getAnnouncementDateEvent())) {
			event.setAnnouncementDateEvent(request.getAnnouncementDateEvent());
		}

		if (!Util.isEmpty(request.getCountParticipant())) {
			event.setCountParticipant(request.getCountParticipant());
		}

		if (!Util.isEmpty(request.getDescription())) {
			event.setDescription(request.getDescription());
		}

		if (!Util.isEmpty(request.getEndDateEvent())) {
			event.setEndDateEvent(request.getEndDateEvent());
		}

		event.setNameEvent(request.getNameEvent());

		if (!Util.isEmpty(request.getParticipantPrivate())) {
			event.setParticipantPrivate(request.getParticipantPrivate());
		}

		event.setPositionX(request.getPositionX());

		event.setPositionY(request.getPositionY());

		if (!Util.isEmpty(request.getPublishDate())) {
			event.setPublishDate(request.getPublishDate());
		}

		if (!Util.isEmpty(request.getStartDateEvent())) {
			event.setStartDateEvent(request.getStartDateEvent());
		}

		event.setCreated(new Date());

		event.setUpdated(new Date());

		return event;
	}
}
