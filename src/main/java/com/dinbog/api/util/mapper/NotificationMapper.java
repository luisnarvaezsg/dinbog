package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.NotificationBasicDTO;
import com.dinbog.api.dto.NotificationScrollDTO;
import com.dinbog.api.dto.ProfileNotificationDTO;
import com.dinbog.api.models.entity.Notification;

@Component
public class NotificationMapper {

	/**
	 *
	 */
	private NotificationMapper() {
		super();
	}

	/**
	 * @param notification
	 * @return
	 */
	public static NotificationBasicDTO mapNotificationBasic(Notification notification) {

		NotificationBasicDTO notificationBasicDTO = new NotificationBasicDTO();
		notificationBasicDTO.setId(notification.getId());
		notificationBasicDTO.setIsRead(notification.getIsRead());
		notificationBasicDTO.setOwnerUser(UserMapper.mapUser(notification.getOwnerUser()));
		notificationBasicDTO.setToUser(UserMapper.mapUser(notification.getToUser()));
		notificationBasicDTO.setProfileNotification(
				ProfileNotificationMapper.mapProfileNotification(notification.getProfileNotification()));
		notificationBasicDTO.setDateNotification(notification.getDateNotification());
		notificationBasicDTO.setCodeMessage(notification.getCodeMessage());
		notificationBasicDTO.setDateRead(notification.getDateRead());

		return notificationBasicDTO;
	}

	public static NotificationScrollDTO mapNotificationProfile(Notification notification) {

		NotificationScrollDTO notificationScrollDTO = new NotificationScrollDTO();
		notificationScrollDTO.setId(notification.getId());
		notificationScrollDTO.setIsRead(notification.getIsRead());
		notificationScrollDTO.setOwnerUser(UserMapper.mapUser(notification.getOwnerUser()));
		notificationScrollDTO.setToUser(UserMapper.mapUser(notification.getToUser()));
		notificationScrollDTO
				.setNotificationAction(NotificationActionMapper.mapEntityToDto(notification.getNotificationAction()));
		notificationScrollDTO.setNotificationDetail(mapNotificationEntities(notification));
		notificationScrollDTO.setDateNotification(notification.getDateNotification());
		notificationScrollDTO.setCodeMessage(notification.getCodeMessage());
		notificationScrollDTO.setDateRead(notification.getDateRead());

		return notificationScrollDTO;
	}

	private static ProfileNotificationDTO mapNotificationEntities(Notification notification) {

		ProfileNotificationDTO profileNotificationDTO = new ProfileNotificationDTO();
		if (notification.getAlbumNotification() != null) {
			profileNotificationDTO.setId(notification.getAlbumNotification().getId());
			profileNotificationDTO.setObjectId(notification.getAlbumNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getAlbumNotification().getOwnerProfile()));
			profileNotificationDTO
					.setToProfile(ProfileMapper.mapProfileBasic(notification.getAlbumNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getAlbumNotification().getMessage());
		} else if (notification.getAttachmentNotification() != null) {
			profileNotificationDTO.setId(notification.getAttachmentNotification().getId());
			profileNotificationDTO.setObjectId(notification.getAttachmentNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getAttachmentNotification().getOwnerProfile()));
			profileNotificationDTO.setToProfile(
					ProfileMapper.mapProfileBasic(notification.getAttachmentNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getAttachmentNotification().getMessage());
		} else if (notification.getConversationNotification() != null) {
			profileNotificationDTO.setId(notification.getConversationNotification().getId());
			profileNotificationDTO.setObjectId(notification.getConversationNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getConversationNotification().getOwnerProfile()));
			profileNotificationDTO.setToProfile(
					ProfileMapper.mapProfileBasic(notification.getConversationNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getConversationNotification().getMessage());
		} else if (notification.getEventNotification() != null) {
			profileNotificationDTO.setId(notification.getEventNotification().getId());
			profileNotificationDTO.setObjectId(notification.getEventNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getEventNotification().getOwnerProfile()));
			profileNotificationDTO
					.setToProfile(ProfileMapper.mapProfileBasic(notification.getEventNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getEventNotification().getMessage());
		} else if (notification.getPollNotification() != null) {
			profileNotificationDTO.setId(notification.getPollNotification().getId());
			profileNotificationDTO.setObjectId(notification.getPollNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getPollNotification().getOwnerProfile()));
			profileNotificationDTO
					.setToProfile(ProfileMapper.mapProfileBasic(notification.getPollNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getPollNotification().getMessage());
		} else if (notification.getPostNotification() != null) {
			profileNotificationDTO.setId(notification.getPostNotification().getId());
			profileNotificationDTO.setObjectId(notification.getPostNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getPostNotification().getOwnerProfile()));
			profileNotificationDTO
					.setToProfile(ProfileMapper.mapProfileBasic(notification.getPostNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getPostNotification().getMessage());
		} else if (notification.getProfileNotification() != null) {
			profileNotificationDTO.setId(notification.getProfileNotification().getId());
			profileNotificationDTO.setObjectId(notification.getProfileNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getProfileNotification().getOwnerProfile()));
			profileNotificationDTO
					.setToProfile(ProfileMapper.mapProfileBasic(notification.getProfileNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getProfileNotification().getMessage());
		} else if (notification.getUserNotification() != null) {
			profileNotificationDTO.setId(notification.getUserNotification().getId());
			profileNotificationDTO.setObjectId(notification.getUserNotification().getObjectId());
			profileNotificationDTO.setOwnerProfile(
					ProfileMapper.mapProfileBasic(notification.getUserNotification().getOwnerProfile()));
			profileNotificationDTO
					.setToProfile(ProfileMapper.mapProfileBasic(notification.getUserNotification().getToProfile()));
			profileNotificationDTO.setMessage(notification.getUserNotification().getMessage());
		}

		return profileNotificationDTO;
	}
}
