package com.dinbog.api.util.mapper;

import java.util.ArrayList;
import java.util.Date;

import com.dinbog.api.dto.AlbumAttachmentDTO;
import com.dinbog.api.dto.AlbumDTO;
import com.dinbog.api.dto.AlbumRequestDTO;
import com.dinbog.api.dto.ProfileSimpleDTO;
import com.dinbog.api.models.entity.Album;
import com.dinbog.api.util.Util;

public class AlbumMapper {

	private AlbumMapper() {
		super();
	}

	public static AlbumDTO mapAlbum(Album album) {

		AlbumDTO albumDTO = new AlbumDTO();

		albumDTO.setId(album.getId());
		albumDTO.setName(album.getName());
		albumDTO.setDescription(album.getDescription());

		if (!Util.isEmpty(album.getAlbumType()))
			albumDTO.setAlbumType(AlbumTypeMapper.mapAlbumType(album.getAlbumType()));
		if (!Util.isEmpty(album.getProfile())) {
			ProfileSimpleDTO profile = new ProfileSimpleDTO();
			profile.setId(album.getProfile().getId());
			albumDTO.setProfile(profile);
		}
		if (!Util.isEmpty(album.getAttachments())) {
			ArrayList<AlbumAttachmentDTO> attachments = new ArrayList<>();
			album.getAttachments().forEach(a -> attachments.add(AlbumAttachmentMapper.mapAlbumAttachment(a)));
			albumDTO.setAlbumAttachments(attachments);
		}
		albumDTO.setIsPrivate(album.getIsPrivate());
		albumDTO.setCreated(album.getCreated());

		return albumDTO;
	}

	public static Album requestToEntity(AlbumRequestDTO request) {

		Album album = new Album();

		album.setName(request.getName());
		album.setDescription(request.getDescription());
		album.setIsPrivate(request.getIsPrivate());
		album.setCreated(new Date());

		return album;
	}

	/**
	 * / public static Album requestToEntity(AlbumDTO request) {
	 *
	 * Album album = new Album();
	 *
	 * if (!Util.isEmpty(request.getId())) { album.setId(request.getId()); }
	 *
	 * album.setName(request.getName());
	 * album.setDescription(request.getDescription());
	 * album.setIsPrivate(request.getIsPrivate());
	 * album.setCreated(request.getCreated());
	 *
	 * return album; } /
	 **/
}
