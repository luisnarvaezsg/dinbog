package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.EventStatusDTO;
import com.dinbog.api.models.entity.EventStatus;

@Component
public class EventStatusMapper {

	private EventStatusMapper() {
		super();
	}

	public static EventStatusDTO mapEventStatus(EventStatus eventStatus) {

		EventStatusDTO eventStatusDTO = new EventStatusDTO();
		eventStatusDTO.setId(eventStatus.getId());
		eventStatusDTO.setValue(eventStatus.getValue());
		
		return eventStatusDTO;
	}
}
