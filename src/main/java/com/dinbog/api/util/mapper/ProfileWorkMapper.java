package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.ProfileWork;
import com.dinbog.api.dto.ProfileWorkDTO;

@Component
public class ProfileWorkMapper {

	private ProfileWorkMapper() {
		super();
	}

	/**
	 * @param profileType
	 * @return
	 */
	public static ProfileWorkDTO mapProfileWork(ProfileWork profileWork) {

		ProfileWorkDTO profileWorkDTO = new ProfileWorkDTO();
		profileWorkDTO.setId(profileWork.getId());
		profileWorkDTO.setClient(profileWork.getClient());
		profileWorkDTO.setDescription(profileWork.getDescription());
		profileWorkDTO.setYearJob(profileWork.getYearJob());
		profileWorkDTO.setCreated(profileWork.getCreated());
		profileWorkDTO.setProfileId(profileWork.getProfile().getId());
		
		
		return profileWorkDTO;
	}
}
