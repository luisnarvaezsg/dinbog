package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.ConnectionTypeDTO;
import com.dinbog.api.models.entity.ConnectionType;

@Component
public class ConnectionTypeMapper {

	private ConnectionTypeMapper() {
		super();
	}

	public static ConnectionTypeDTO mapConnectionType(ConnectionType connectionType) {

		ConnectionTypeDTO connectionTypeDTO = new ConnectionTypeDTO();
		connectionTypeDTO.setId(connectionType.getId());
		connectionTypeDTO.setValue( connectionType.getValue());

		return connectionTypeDTO;
	}
}
