package com.dinbog.api.util.mapper;

import com.dinbog.api.models.entity.Skill;
import com.dinbog.api.dto.SkillDTO;

public class SkillMapper {

	private SkillMapper() {
		super();
	}

	public static SkillDTO mapSkill(Skill skill) {

		SkillDTO skillDTO = new SkillDTO();
		skillDTO.setId(skill.getId());
		skillDTO.setName(skill.getName());
		skillDTO.setDescription(skill.getDescription());
		skillDTO.setIsActive(skill.getIsActive());
		skillDTO.setCreated(skill.getCreated());

		return skillDTO;
	}
}
