package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.ProfileType;
import com.dinbog.api.dto.ProfileTypeDTO;

@Component
public class ProfileTypeMapper {

	private ProfileTypeMapper() {
		super();
	}

	/**
	 * @param profileType
	 * @return
	 */
	public static ProfileTypeDTO mapProfileType(ProfileType profileType) {

		ProfileTypeDTO profileTypeDTO = new ProfileTypeDTO();
		profileTypeDTO.setId(profileType.getId());
		profileTypeDTO.setValue(profileType.getValue());
		
		return profileTypeDTO;
	}
}
