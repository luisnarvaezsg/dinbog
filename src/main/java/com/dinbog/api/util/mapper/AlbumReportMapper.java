package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.AlbumReportDTO;
import com.dinbog.api.models.entity.AlbumReport;

@Component
public class AlbumReportMapper {

	AlbumReportMapper() {
		super();
	}

	public static AlbumReportDTO mapEntityToDTO(AlbumReport albumReport) {

		AlbumReportDTO albumReportDTO = new AlbumReportDTO();
		albumReportDTO.setId(albumReport.getId());
		albumReportDTO.setAlbum(AlbumMapper.mapAlbum(albumReport.getAlbum()));
		albumReportDTO.setProfile(ProfileMapper.mapProfile(albumReport.getOwner()));
		albumReportDTO.setReportType(ReportTypeMapper.mapReportType(albumReport.getReportType()));

		return albumReportDTO;
	}
}
