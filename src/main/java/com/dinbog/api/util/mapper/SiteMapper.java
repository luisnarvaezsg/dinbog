package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.models.entity.Site;
import com.dinbog.api.dto.SiteDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class SiteMapper {

	SiteMapper() {
		super();
	}

	public static SiteDTO mapSite(Site site) {

		SiteDTO siteDTO = new SiteDTO();
		siteDTO.setId(site.getId());
		siteDTO.setActive(site.getActive());
		siteDTO.setName(site.getName());
		siteDTO.setDomain(site.getDomain());
		siteDTO.setCreated(site.getCreated());
		siteDTO.setUpdated(site.getUpdated());
		return siteDTO;
	}
}
