package com.dinbog.api.util.mapper;

import java.util.List;

import com.dinbog.api.util.ParameterEnum;
import com.dinbog.api.util.url.UrlParams;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.dto.AttachmentBasicDTO;
import com.dinbog.api.dto.AttachmentDTO;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.PostAttachment;
import com.dinbog.api.service.IAttachmentService;
import com.dinbog.api.util.Util;


@Component
public class AttachmentMapper {

	@Autowired
	IAttachmentService iAttachmentService;

	private AttachmentMapper() {
		super();
	}

	public static AttachmentDTO mapAttachment(Attachment attachment) {
		AttachmentDTO attachmentDTO = new AttachmentDTO();
		attachmentDTO.setId(attachment.getId());
		attachmentDTO.setCountAttachment(attachment.getCountAttachment());
		attachmentDTO.setPath( getProperPath(attachment) );
		attachmentDTO.setMetadata(attachment.getMetadata());
		attachmentDTO.setPurged(attachment.getPurged());
		attachmentDTO.setExtra(attachment.getExtra());
		attachmentDTO.setCreated(attachment.getCreated());
		attachmentDTO.setDatePurged(attachment.getDatePurged());
		attachmentDTO.setAttachmentType(AttachmentTypeMapper.mapAttachmentType(attachment.getAttachmentType()));

		return attachmentDTO;
	}

	public static AttachmentBasicDTO mapAttachmentBasic(Attachment attachment) {

		AttachmentBasicDTO attachmentBasicDTO = new AttachmentBasicDTO();
		attachmentBasicDTO.setId(attachment.getId());
		attachmentBasicDTO.setPath( getProperPath(attachment) );
		attachmentBasicDTO.setMetadata(getProperMetaData(attachment));
		attachmentBasicDTO.setPurged(attachment.getPurged());
		attachmentBasicDTO.setExtra(attachment.getExtra());
		attachmentBasicDTO.setCountComments(attachment.getCountComments());
		attachmentBasicDTO.setCountLikes(attachment.getCountLikes());
		attachmentBasicDTO.setAttachmentType(AttachmentTypeMapper.mapAttachmentType(attachment.getAttachmentType()));

		return attachmentBasicDTO;
	}

	public static MutableList<AttachmentBasicDTO> mapPostAttachmentsList(List<PostAttachment> postAttachmentsList) {

		MutableList<AttachmentBasicDTO> attachmentBasicDTOs = Lists.mutable.empty();

		if (!Util.isEmpty(postAttachmentsList)) {
			postAttachmentsList.forEach(postAttachment -> attachmentBasicDTOs
					.add(mapAttachmentBasic(postAttachment.getAttachment())));
		}

		return attachmentBasicDTOs;
	}

	private static  String getProperPath(Attachment attachment){
		return !attachment.getAttachmentType().getValue().equals(UrlParams.KEY.getValue())
				? CommonsConstant.PATH_IMAGE + attachment.getPath()
				: attachment.getPath();
	}

	private static String getProperMetaData(Attachment attachment){
		return (attachment.getMetadata() == null) || (attachment.getMetadata().isEmpty())? ParameterEnum.BLANK.getKey() : CommonsConstant.PATH_IMAGE + attachment.getMetadata();
	}

}
