package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.ProfileFilterValueDTO;
import com.dinbog.api.models.entity.ProfileFilterValue;

@Component
public class ProfileFilterValueMapper {

	private ProfileFilterValueMapper() {
		super();
	}

	public static ProfileFilterValueDTO mapProfileFilterValue(ProfileFilterValue profileFilterValue) {

		ProfileFilterValueDTO profileFilterValueDTO = new ProfileFilterValueDTO();
		profileFilterValueDTO.setId(profileFilterValue.getId());
		profileFilterValueDTO.setProfileCategoryId(profileFilterValue.getProfileCategory().getId());
		profileFilterValueDTO.setCategoryFilterId(profileFilterValue.getCategoryFilter().getId());
		profileFilterValueDTO.setDescription(profileFilterValue.getDescription());
		profileFilterValueDTO.setValue(profileFilterValue.getValue());
		
		return profileFilterValueDTO;
	}
}
