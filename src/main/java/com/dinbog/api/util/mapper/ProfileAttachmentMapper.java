package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.ProfileAttachmentDTO;
import com.dinbog.api.dto.ProfileAttachmentRequestDTO;
import com.dinbog.api.dto.ProfileSimpleDTO;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.ProfileAttachment;

@Component
public class ProfileAttachmentMapper {

	private ProfileAttachmentMapper() {
		super();
	}

	public static ProfileAttachmentDTO mapProfileAttachment(ProfileAttachment profileAttachment) {

		ProfileAttachmentDTO profileAttachmentDTO = new ProfileAttachmentDTO();
		profileAttachmentDTO.setId(profileAttachment.getId());
		profileAttachmentDTO.setAttachment(AttachmentMapper.mapAttachment(profileAttachment.getAttachment()));
		ProfileSimpleDTO profileSimpleDTO = new ProfileSimpleDTO();
		profileSimpleDTO.setId(profileAttachment.getProfile().getId());
		profileAttachmentDTO.setProfile(profileSimpleDTO);

		return profileAttachmentDTO;
	}

	public static ProfileAttachment requestToEntity(ProfileAttachmentRequestDTO request) {

		ProfileAttachment profileAttachment = new ProfileAttachment();

		Attachment attachment = new Attachment();
		attachment.setId(request.getAttachmentId());
		profileAttachment.setAttachment(attachment);

		return profileAttachment;
	}
}
