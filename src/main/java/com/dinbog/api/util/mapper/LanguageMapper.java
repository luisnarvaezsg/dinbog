package com.dinbog.api.util.mapper;

import com.dinbog.api.dto.LanguageDTO;
import com.dinbog.api.dto.LanguageRequestDTO;
import com.dinbog.api.models.entity.Language;

public class LanguageMapper {

	private LanguageMapper() {
		super();
	}

	/**
	 * @param language
	 * @return
	 */
	public static LanguageDTO mapLanguage(Language language) {

		LanguageDTO languageDTO = new LanguageDTO();
		languageDTO.setId(language.getId());
		languageDTO.setName(language.getName());
		languageDTO.setIsoCodeLanguage(language.getIsoCodeLanguage());

		return languageDTO;
	}

	/**
	 * @param request
	 * @return
	 */
	public static Language mapRequestToEntity(LanguageRequestDTO request) {

		Language language = new Language();

		language.setName(request.getName());
		language.setIsoCodeLanguage(request.getIsoCodeLanguage());

		return language;
	}
}
