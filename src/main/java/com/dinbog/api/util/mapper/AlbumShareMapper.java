package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;

import com.dinbog.api.dto.AlbumShareDTO;
import com.dinbog.api.models.entity.AlbumShare;

@Component
public class AlbumShareMapper {

	/**
	 * 
	 */
	private AlbumShareMapper() {
		super();
	}

	public static AlbumShareDTO mapperEntityToDTO(AlbumShare albumShare) {

		AlbumShareDTO albumShareDTO = new AlbumShareDTO();
		albumShareDTO.setId(albumShare.getId());
		albumShareDTO.setAlbum(AlbumMapper.mapAlbum(albumShare.getAlbum()));
		albumShareDTO.setProfile(ProfileMapper.mapProfile(albumShare.getOwnerProfile()));
		return albumShareDTO;
	}
}
