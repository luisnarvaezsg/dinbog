package com.dinbog.api.util.mapper;

import org.springframework.stereotype.Component;
import com.dinbog.api.dto.CountryDTO;
import com.dinbog.api.models.entity.Country;

@Component
public class CountryMapper {

	private CountryMapper() {
		super();
	}

	public static CountryDTO mapCounty(Country country) {
		CountryDTO countryDTO = new CountryDTO();
		countryDTO.setId(country.getId());
		countryDTO.setName(country.getName());
		countryDTO.setUnitMeasurement(country.getUnitMeasurement());
		countryDTO.setWearMeasurement(country.getWearMeasurement());
		countryDTO.setIsoCode(country.getIsoCode());
		countryDTO.setIsOnu(country.getIsOnu());
		countryDTO.setLanguage(country.getLanguageId());

		return countryDTO;
	}
}
