package com.dinbog.api.util.mapper;

import com.dinbog.api.models.entity.ProfileLike;
import com.dinbog.api.dto.ProfileLikeDTO;

public class ProfileLikeMapper {

	private ProfileLikeMapper() {
		super();
	}

	public static ProfileLikeDTO mapEntityToDtoMapper(ProfileLike profileLike) {

		ProfileLikeDTO profileLikeDTO = new ProfileLikeDTO();
		profileLikeDTO.setId(profileLike.getId());
		profileLikeDTO.setOwnerProfile(ProfileMapper.mapProfileSimple(profileLike.getProfileOwner(), null,null,null,null,null));
		profileLikeDTO.setProfile(ProfileMapper.mapProfileSimple(profileLike.getProfile(), null,null,null,null,null));
		profileLikeDTO.setCreated(profileLike.getCreated());

		return profileLikeDTO;
	}
}
