package com.dinbog.api.util;

import lombok.Getter;

@Getter
public enum AlbumTypes {
    AUDIO(2L,"audio","solo para contener archivos de musica"),
    BOOK(1L,"book","solo para contener archivos de imagenes"),
    VIDEO(3L,"video","solo para contener archivos de imagenes de trabajo artisticos"),
    ;
    private final Long id;
    private final String name;
    private final String description;

    AlbumTypes(Long id, String name,String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
