package com.dinbog.api.util;

public enum ProfileConnectionType {
    FRIENDSHIP(1L,"friendship"),
    PROFESSIONAL(2L,"professional"),
    ;

    private Long code;
    private String value;

    ProfileConnectionType(Long code, String value) {
        this.code = code;
        this.value = value;
    }

    public Long getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
