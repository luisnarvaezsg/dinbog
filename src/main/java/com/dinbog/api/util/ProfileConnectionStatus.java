package com.dinbog.api.util;

import org.springframework.data.repository.query.Param;

import java.lang.invoke.SwitchPoint;

public enum ProfileConnectionStatus {

    PENDING(1L,"Pending"),
    ACCEPTED(2l,"Accepted"),
    REJECTED(3l,"Rejected"),
    RESTRICTED(4L,"Restricted"),
    CANCELED(5L,"Canceled"),
    ;

    private final Long code;
    private final String value;

    ProfileConnectionStatus(Long code, String value) {
        this.code = code;
        this.value = value;
    }

    public Long getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static Long fitProfileStrategy(Long requestingCode){
        if (requestingCode.equals(ACCEPTED.getCode()) || requestingCode.equals(REJECTED.getCode()))
            return PENDING.getCode();
        if(requestingCode.equals(CANCELED.getCode()))
            return ACCEPTED.getCode();

        return null;
    }
}
