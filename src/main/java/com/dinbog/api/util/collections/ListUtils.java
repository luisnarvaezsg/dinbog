/*
 * Generic Class that performs operations upon generic List
 */
package com.dinbog.api.util.collections;

import java.util.List;
import java.util.stream.Collectors;

public class ListUtils<T> {

    private final List<T> list;

    /**
     * Generic Constructor
     * @param list Generic List type
     */
    public ListUtils(List<T> list){
        this.list = list;
    }


    /**
     * Public method that performs the cast from List<T> type into List<Long> type
     * @return Sorted List<Long>
     */
    public List<Long> listStringToLong(){
        return this.list.stream().map( t -> Long.parseLong( t.toString() ) ).sorted().collect(Collectors.toList() );
    }

    public boolean isNullOrEmpty(){
        return list == null || list.isEmpty();
    }

    public List<T> getList(){
        return this.list;
    }
}
