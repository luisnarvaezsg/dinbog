package com.dinbog.api.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.dinbog.api.dto.PostMultiFileDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3Object;
import com.dinbog.api.dto.PostDTO;
import com.dinbog.api.dto.PostEditDTO;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostComment;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;


public interface IPostService {

	void createPost(MultipartFile[] file, String comment, User user, String lang);

	void createPost(Map<String,MultipartFile> map, String comment, User user, String lang);

	void createPost(String url, String comment, User user, String lang);

	PostDTO getById(Long id, String lang);

	Post update(PostEditDTO request, Long postId);

	S3Object getPost(String keyName);

	String getPostURL(String keyName);

	PostDTO getPost(Long id, String lang, User user);

	MutableList<PostDTO> findAllByUser(User user, String lang);

	Optional<Post> findById(Long id, String lang);

	void postComment(Long id, String comentario, User user, String lang);

	void deleteById(Long id, String lang);

	void update(Long postId, String comment, User user, String lang);
	
	PostDTO buildPostDTO(Post post, Profile profUser, String lang);

	void createPost(PostMultiFileDTO postMultiFileDTO, User user, String lang);

	void save(Post post);

	ApiError getApiError(ApiErrorEnum errorCode, String lang);
	
	List<Profile> listaMention(List<PostComment> listaPostComment);

	MutableList<PostDTO> muro(User user, String lang, int pageNumber, int pageSize);

	MutableList<PostDTO> findAllByProfile(HttpServletRequest request, String urlName, Integer pageNumber,
			Integer pageSize, String lang);

}
