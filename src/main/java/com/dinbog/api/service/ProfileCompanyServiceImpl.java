package com.dinbog.api.service;
import java.util.Optional;
import java.util.Random;


import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.dto.ProfileRequestDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCompany;
import com.dinbog.api.models.entity.ProfileCountry;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.ProfileCompanyRepository;
import com.dinbog.api.util.mapper.ProfileMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileCompanyServiceImpl implements IProfileCompanyService {

	@Autowired
	private IProfileService profileService;

	@Autowired
	private IProfileTypeService profileTypeService;
	
	@Autowired
	private ProfileCompanyRepository profileCompanyRepository;

	@Autowired
	private IUserService userService;

	@Autowired
	private IMembershipService membershipService;

	@Autowired
	private ICountryService countryService;
	
	@Autowired
	private ICityService cityService;

	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private IProfileCategoryTypeService categoryTypeService;
	
	@Autowired
	private IProfileCountryTypeService profileCountryTypeService;
	



	private String generateUrlName(User user) {
		
		String firstName = user.getFirstName().toLowerCase().trim().replaceAll(" {1,}",".");
		
		String urlName = firstName;
				
		while(profileService.countProfileByUrlName(urlName) > 0) {

			Random random = new Random();
			Integer prefix = random.nextInt(9999);
			urlName = urlName.concat(prefix.toString());
		}
		
		return urlName;
		
	}
	
	@Override
	@Transactional()
	public ProfileCompany update(ProfileRequestDTO request, String language) {
		
		//Search Profile:
		ProfileCompany profileCompany = this.readById(request.getProfileId(), language);
		
		if (profileCompany.getId() < 1) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, language)));
		}
		
		profileCompany.getProfile().setSearchableSince(request.getSearchableSince());
		profileCompany.getProfile().setIsSearchable(request.getIsSearchable());
		profileCompany.getProfile().setAvatarPath(request.getAvatarPath());
		profileCompany.getProfile().setCoverPath(request.getCoverPath());
		profileCompany.getProfile().setPhoneNumber(request.getPhoneNumber());
		profileCompany.setDescription(request.getDescription());
		
		
		// MEMBERSHIP
		profileCompany.getProfile().setMembership(membershipService.findById(request.getMembershipId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.MEMBERSHIP_NOT_FOUND, language)))));
		
		profileCompany.getProfileCountries();
		//DELETE CATEGORIES
		int countriesSize = profileCompany.getProfileCountries().size();
		
		for(int i=0;i<countriesSize;i++)  {

			profileCompany.removeProfileCountries(profileCompany.getProfileCountries().get(0));
	    
		}
		
		// HEADQUARTER COUNTRY AND CITY
		ProfileCountry headQuarter = new ProfileCountry();
		headQuarter.setCountry(countryService.findById(request.getHeadQuarterCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
		headQuarter.setCity(cityService.findById(request.getHeadQuarterCityId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		headQuarter.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_ORIGIN_TYPE)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		headQuarter.setProfile(profileCompany.getProfile());
		profileCompany.addProfileCountries(headQuarter);
		
		//SAVE BRANCHES
		for (String branchId : request.getBranches()) {
			
			ProfileCountry branch = new ProfileCountry();
			branch.setCountry(countryService.findById(Long.parseLong(branchId), language)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
			branch.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_BRANCH_TYPE)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
			branch.setProfile(profileCompany.getProfile());
			profileCompany.addProfileCountries(branch);
		}
		
		
		profileCompany.getProfile().getProfileCategories();
		//DELETE CATEGORIES
		int listSize = profileCompany.getProfile().getProfileCategories().size();
		
		for(int i=0;i<listSize;i++)  {

			profileCompany.getProfile().removeProfileCategories(profileCompany.getProfile().getProfileCategories().get(0));
	    
		}
		
		//INSERT NEWS CATEGORIES
		for (String catId : request.getCategories()) {
			
			ProfileCategory profileCategory = new ProfileCategory();
			profileCategory.setProfile(profileCompany.getProfile());
			
			profileCategory.setProfileCategoryType(categoryTypeService.findById(Long.valueOf(catId))
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND, language)))));
			
			profileCompany.getProfile().addProfileCategories(profileCategory);
		}
		
		
		return profileCompany;
	}
	
	@Override
	@Transactional()
	public ProfileCompany createCompanyProfile(ProfileRequestDTO request, String language) {

		// CONSTRUYE ENTITY PROFILE
		ProfileCompany profileCompany = ProfileMapper.requestToEntityCompany(request);
		
		// USER
		profileCompany.getProfile().setUser(userService.findById(request.getUserId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.USER_NOT_FOUND, language)))));
		
		profileCompany.getProfile().setUrlName(generateUrlName(profileCompany.getProfile().getUser()));
		profileCompany.setCompanyName(profileCompany.getProfile().getUser().getFirstName());
		profileCompany.setDescription(request.getDescription());
		String fullName = profileCompany.getProfile().getUser().getFirstName().concat(" ").concat(profileCompany.getProfile().getUser().getLastName());
		profileCompany.getProfile().setFullName(fullName);
		
		if (profileService.countProfileByUrlName(request.getUrlName()) > 0) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_URL_NAME_ISSET, language)));
		}
		// MEMBERSHIP
		profileCompany.getProfile().setMembership(membershipService.findById(request.getMembershipId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.MEMBERSHIP_NOT_FOUND, language)))));
		// PROFILE TYPE
		profileCompany.getProfile().setProfileType(profileTypeService.findById(request.getProfileTypeId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_TYPE_NOT_FOUND, language)))));
		
		// HEADQUARTER COUNTRY AND CITY
		ProfileCountry headQuarter = new ProfileCountry();
		headQuarter.setCountry(countryService.findById(request.getHeadQuarterCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
		headQuarter.setCity(cityService.findById(request.getHeadQuarterCityId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		headQuarter.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_ORIGIN_TYPE)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		headQuarter.setProfile(profileCompany.getProfile());
		profileCompany.addProfileCountries(headQuarter);
		
		//SAVE BRANCHES
		for (String branchId : request.getBranches()) {
			
			ProfileCountry branch = new ProfileCountry();
			branch.setCountry(countryService.findById(Long.parseLong(branchId), language)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
			branch.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_BRANCH_TYPE)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
			branch.setProfile(profileCompany.getProfile());
			profileCompany.addProfileCountries(branch);
		}
		
		//SAVE CATEGORIES
		for (String catId : request.getCategories()) {
			
			ProfileCategory profileCategory = new ProfileCategory();
			profileCategory.setProfile(profileCompany.getProfile());
			
			profileCategory.setProfileCategoryType(categoryTypeService.findById(Long.valueOf(catId))
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND, language)))));
			
			profileCompany.getProfile().addProfileCategories(profileCategory);
		}
		
		profileCompany = profileCompanyRepository.save(profileCompany);
		profileCompany.getProfile().getProfileCategories();
		
		return profileCompany;
	}

	@Override
	@Transactional(readOnly = true)
	public ProfileCompany readById(Long id, String lang) {
		
		log.info("Valor a buscar id " + id.toString());
		Optional<ProfileCompany> profile = profileCompanyRepository.findByProfile(id);
		
		try {
			
			profile = profileCompanyRepository.findByProfile(id);
		
		}catch(Exception e) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}
		
		if(!profile.isPresent()) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}
		return profile.get();
	}	

}
