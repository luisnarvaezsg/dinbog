package com.dinbog.api.service;

import com.dinbog.api.dto.AlbumLikeRequestDTO;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.AlbumLike;
import com.dinbog.api.models.entity.Profile;

import java.util.List;
import java.util.Optional;

public interface IAlbumLikeService {

	/**
	 *
	 * @param request
	 * @return
	 * @throws InternalErrorException
	 */
	AlbumLike create(AlbumLikeRequestDTO request, Long albumId);

	/**
	 * @param albumId
	 * @param id
	 */
	void deleteByAlbumIdAndById(Long albumId, Long id);

	Optional<AlbumLike> findLikeByProfileId(Album album, Profile profile);


	void create(Album album, Profile profile);

	void remove(AlbumLike albumLike);

	Integer getCountLikes(Long id);

	void delete(Album album);

	List<AlbumLike> findAllByAlbum(Album album);

}
