package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.SocialAppCreateDTO;
import com.dinbog.api.dto.SocialAppDTO;
import com.dinbog.api.repository.SocialAppRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.SocialApp;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.SocialAppMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("SocialAppService")
public class SocialAppServiceImpl implements ISocialAppService {

	@Autowired
    private SocialAppRepository socialAppRepository;

	/**
	 * @param request
	 * @return
	 */
	@Override
    public SocialApp create(SocialAppCreateDTO request) {

		try {

			SocialApp socialApp = new SocialApp();
			socialApp.setActive(request.getActive());
			socialApp.setClientId(request.getClientId());
			socialApp.setDomain(request.getDomain());
			socialApp.setKey(request.getKey());
			socialApp.setName(request.getName());
			socialApp.setSecret(request.getSecret());
			return socialAppRepository.save(socialApp);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@Override
    public MutableList<SocialAppDTO> readAll() {

		try {
			List<SocialApp> socialAppList = socialAppRepository.findAll();

			if (socialAppList.isEmpty()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.SOCIALAPP_NO_RESULTS.getCode(), ApiErrorEnum.SOCIALAPP_NO_RESULTS.getMessage())));
			} else {
				MutableList<SocialAppDTO> socialAppDTOS = Lists.mutable.empty();
				socialAppList.forEach(i -> socialAppDTOS.add(SocialAppMapper.mapSocialApp(i)));

				return socialAppDTOS;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("Error en [SocialAppService.readAll] ");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 */
	@Override
    public void patch(Long id, SocialAppCreateDTO request) {

		try {
			Optional<SocialApp> optional = socialAppRepository.findById(id);

			if (optional.isPresent()) {
				SocialApp socialApp = optional.get();
				socialApp.setActive(request.getActive());
				socialApp.setClientId(request.getClientId());
				socialApp.setDomain(request.getDomain());
				socialApp.setKey(request.getKey());
				socialApp.setName(request.getName());
				socialApp.setSecret(request.getSecret());
				socialAppRepository.save(socialApp);
			} else {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getCode(),
								ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getMessage())));
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppService.patch]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
    public void delete(Long id) {

		try {
			socialAppRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getCode(),
									ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getMessage()))));

			socialAppRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SocialAppService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<SocialApp> findById(Long id) {
		
		return socialAppRepository.findById(id);
	}
}
