package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.ProfileType;

public interface IProfileTypeService {

	Optional<ProfileType> findById(Long id);

}