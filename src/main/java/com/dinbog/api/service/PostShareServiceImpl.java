package com.dinbog.api.service;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.PostShare;
import com.dinbog.api.repository.PostShareRepository;
import com.dinbog.api.util.Util;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PostShareServiceImpl implements IPostShareService {
	
	@Autowired
	private IPostService postService;
	
	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private PostShareRepository postShareRepository;
	
	/**
	 *
	 * @param postId
	 * @param profileId
	 * @return
	 */
	@Override
	public PostShare create(Long postId, Long profileId) {
		try {
			PostShare postShare = new PostShare();
			
			postShare.setProfile(profileService.findById(profileId)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			
			postShare.setPost(postService.findById(postId, CommonsConstant.LANGUAGE_BY_DEFAULT)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
									ApiErrorEnum.POST_NOT_FOUND.getMessage()))))
			);
			
			return postShareRepository.save(postShare);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostShareService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
