package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.PostLikeResponseDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ApiExceptionResponse;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostLike;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.PostLikeRepository;
import com.dinbog.api.repository.ProfileRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.PostLikeMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class PostLikeServiceImpl implements IPostLikeService {

	@Autowired
	private PostLikeRepository postLikeRepository;

	@Autowired
	private IPostService postService;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private IApiErrorService errorService;

	/**
	 *
	 * @param postId
	 * @param profileId
	 * @return
	 */
	@Override
	@Transactional
	public PostLikeResponseDTO create(Long postId, Long profileId, String lang) {

		try {
			PostLike postLike = new PostLike();

			Post post = postService.findById(postId, lang).orElseThrow(() -> new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
			Profile profile = profileRepository.findById(profileId).orElseThrow(() -> new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
			
			if (findByPostAndProfile(post, profile))
				throw new ApiExceptionResponse(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_LIKE_FOUND, lang)));

			postLike.setProfile(profile);
			
			post.setCountLike(post.getCountLike() + 1);
			postService.save(post);
			postLike.setPost(post);

			return PostLikeMapper.mapPostLikeResponse(postLikeRepository.save(postLike));

		} catch (Exception e) {
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostLikeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	@Transactional
	public Boolean deleteLike(Long postId, User user, String lang) {

			Post post = postService.findById(postId, lang).orElseThrow(() -> new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
			
			Profile profile;
			Optional<Profile> oProfile =  profileRepository.findByUser(user);
			if(oProfile.isPresent()) {
				profile = oProfile.get();
			}else {
				throw new ApiExceptionResponse(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
			}
			List<PostLike> lista = postLikeRepository.findByPostAndProfile(post, profile );
			
			if (lista==null || lista.isEmpty())
				throw new ApiExceptionResponse(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_LIKE_NOT_FOUND, lang)));

			PostLike postLike=lista.get(0);
			
			post.setCountLike(post.getCountLike() - 1);
			
			postService.save(post);
			
			postLikeRepository.delete(postLike);

			return true;

	}
	
	@Override
	public Boolean findByPostAndProfile(Post post, Profile profile) {
		List<PostLike> lista = postLikeRepository.findByPostAndProfile(post, profile);
		return ((lista != null) && !(lista.isEmpty()));
	}

	@Override
	public List<PostLike> findByPost(Post post) {
		
		return postLikeRepository.findByPost(post);
	}

	@Override
	@Transactional
	public void deleteByPost(Post post) {
		
		List<PostLike> listaPL=findByPost(post);
		if ((listaPL!=null)&& !listaPL.isEmpty())
			listaPL.forEach(postLike->postLikeRepository.delete(postLike));
	}
}
