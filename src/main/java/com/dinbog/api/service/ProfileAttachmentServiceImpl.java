package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.ProfileAttachmentDTO;
import com.dinbog.api.dto.ProfileAttachmentRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.ProfileAttachment;
import com.dinbog.api.repository.ProfileAttachmentRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileAttachmentMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileAttachmentServiceImpl implements IProfileAttachmentService {

	@Autowired
	private ProfileAttachmentRepository profileAttachmentRepository;

	@Autowired
	private IAttachmentService attachmentService;

	@Autowired
	private IProfileService profileService;

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public ProfileAttachment create(Long profileId, ProfileAttachmentRequestDTO request) {

		try {
			ProfileAttachment profileAttachment = new ProfileAttachment();

			profileAttachment.setAttachment(attachmentService.findById(request.getAttachmentId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage())))));

			profileAttachment
					.setProfile(profileService.findById(profileId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));

			return profileAttachmentRepository.save(profileAttachment);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileAttachmentService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param profileId
	 * @param id
	 */
	@Override
	public void deleteByIdAndByProfileId(Long profileId, Long attachmentId) {

		try {
			// valida profile
			profileService.findById(profileId).orElseThrow(() -> new InvalidRequestException(
					Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
							ApiErrorEnum.PROFILE_NOT_FOUND.getMessage()))));

			// valida attachment
			attachmentService.findById(attachmentId)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage()))));

			// VALIDA QUE EXISTA LA RELACION A ELIMINAR
			ProfileAttachment pa = profileAttachmentRepository.findByProfileAttachment(profileId, attachmentId)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_ATTACHMENT_NO_MATCH.getCode(),
									ApiErrorEnum.PROFILE_ATTACHMENT_NO_MATCH.getMessage()))));

			// ELIMINA EL ATTACHMENT DEL PROFILE
			profileAttachmentRepository.delete(pa);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileAttachmentService.deleteByIdAndByProfileId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param profileId
	 * @param id
	 */
	@Override
	public MutableList<ProfileAttachmentDTO> getAttachmentsByProfileId(Long profileId) {

		try {

			MutableList<ProfileAttachmentDTO> profileAttachmentDTOS = Lists.mutable.empty();
			log.info("Aplica cuando es PROFILE Y LOS FRIENDS");
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ProfileAttachment> criteriaQuery = criteriaBuilder.createQuery(ProfileAttachment.class);
			Root<ProfileAttachment> root = criteriaQuery.from(ProfileAttachment.class);
			criteriaQuery.where(criteriaBuilder.equal(root.get("profile"), profileId));
			Optional<List<ProfileAttachment>> resultsProfileFollow = Optional
					.ofNullable(entityManager.createQuery(criteriaQuery).getResultList());

			if (resultsProfileFollow.isPresent()) {
				resultsProfileFollow.get().forEach(x -> profileAttachmentDTOS.add(ProfileAttachmentMapper.mapProfileAttachment(x)));
			}
			return profileAttachmentDTOS;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileAttachmentService.getAttachmentsByProfileId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public ProfileAttachment create(ProfileAttachment profileAttachment) {

		try {
			
			return profileAttachmentRepository.save(profileAttachment);

		} catch (Exception e) {
			log.error("*** Exception EN [ProfileAttachmentService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

}
