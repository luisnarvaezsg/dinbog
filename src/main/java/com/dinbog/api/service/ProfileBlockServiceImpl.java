package com.dinbog.api.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.ProfileBlockBasicDTO;
import com.dinbog.api.dto.ProfileBlockCreateDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileBlock;
import com.dinbog.api.repository.ProfileBlockRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileBlockMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@Service
public class ProfileBlockServiceImpl implements IProfileBlockService {

	@Autowired
	private IProfileService profileService;

	@Autowired
	private ProfileBlockRepository profileBlockRepository;

	@Autowired
	private IApiErrorService errorService;

	/**
	 * @param profileId
	 * @param request
	 * @return
	 */
	@Override
	public ProfileBlockBasicDTO create(Long profileId, ProfileBlockCreateDTO request) {

		try {
			ProfileBlock profileBlock = new ProfileBlock();
			// PERSONA BLOQUEADA
			profileBlock.setOwnerProfile(profileService.findById(request.getOwnerProfileId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getMessage())))));
			// PERSONA QUE BLOQUEA
			profileBlock
					.setProfile(profileService.findById(profileId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			// VALIDA QUE YA NO ESTE PREVIAMENTE BLOQUEADO
			Optional<ProfileBlock> pf = profileBlockRepository.findBlockRelated(request.getOwnerProfileId(), profileId);

			if (!pf.isPresent()) {
				return ProfileBlockMapper.mapProfileBlockBasic(profileBlockRepository.save(profileBlock));
			} else {
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_ALREADY_BLOCKED.getCode(),
								ApiErrorEnum.PROFILE_ALREADY_BLOCKED.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileBlockService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@Override
	public List<ProfileBlockBasicDTO> getBlocks(String urlName) {

		try {
			// BUSCA EL PROFILE
			Optional<Profile> oProfile = profileService.findByUrlName(urlName);
			Profile profile;
			if (oProfile.isPresent()) {
				profile = oProfile.get();
			}else {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			}

			// BUSCA LOS BLOCKS
			Optional<List<ProfileBlock>> blocks = profileBlockRepository.findByProfile(profile.getId());
			if (!blocks.isPresent()) {
				return Collections.emptyList();
			} else {
				return ProfileBlockMapper.mapProfileBlockBasicList(blocks.get());
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileBlockService.getBlocks]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param profileId
	 * @param id
	 */
	@Override
	public void delete(Long profileId, Long id) {

		try {
			profileBlockRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_BLOCK_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_BLOCK_NOT_FOUND.getMessage()))));

			profileBlockRepository.deleteProfileBlock(id, profileId);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileBlockService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Boolean isProfileBlocked(Long ownerId, Long profileId) {
		return profileBlockRepository.findByOwnerProfile_IdAndProfile_Id(ownerId, profileId).isPresent();
	}

	@Override
	public Optional<ProfileBlock> findByOwnerAndProfile(Profile owner, Profile profileBlocked) {
		return profileBlockRepository.findByOwnerProfile_IdAndProfile_Id(owner.getId(), profileBlocked.getId());
	}

	@Override
	public void create(Profile owner, Profile profileBlocked) {
		ProfileBlock profileBlock = new ProfileBlock();
		profileBlock.setOwnerProfile(owner);
		profileBlock.setProfile(profileBlocked);
		profileBlockRepository.save(profileBlock);
	}

	@Override
	public void remove(ProfileBlock profileBlock) {
		profileBlock.setStatus(0);
		profileBlockRepository.save(profileBlock);
	}

	@Override
	public MutableList<ApiError> validateProfileBlockOperation(Optional<Profile> owner, Optional<Profile> profile,String lang){
		MutableList<ApiError> errors = null;
		if(profile.isEmpty()){
			errors = Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang));
		}
		if(owner.isEmpty()){
			errors = Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND, lang));
		}
		if (owner.isPresent() && profile.isPresent()) {
			if (owner.get().getId().equals(profile.get().getId())) {
				errors = Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FORBIDDEN_OPERATION, lang));
			} 
		}
		return errors;
	}

	@Override
	public void save(ProfileBlock profileBlock) {
		
		profileBlockRepository.save(profileBlock);
		
	}

}
