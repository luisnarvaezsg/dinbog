package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCategoryType;

public interface IProfileCategoryService {

	void save(ProfileCategory profileCategory);
	
	public List<ProfileCategory> findByProfileId(Long profileId, String lang);
	
	void delete(Long profileId);
	
	Optional<ProfileCategory> findById(Long profileCategoryId, String lang);

	Optional<ProfileCategory> findByProfileAndProfileCategoryType(Profile profile, ProfileCategoryType profileCategoryType);

}