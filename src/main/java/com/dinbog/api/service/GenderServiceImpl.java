package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.GenderCreateDTO;
import com.dinbog.api.dto.GenderDTO;
import com.dinbog.api.dto.GenderEditDTO;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Gender;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.GenderRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.GenderMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("GenderService")
public class GenderServiceImpl implements IGenderService {

	@Autowired
	private GenderRepository genderRepository;
	
	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private ILanguageService languageService;

	/**
	 * @param langCode
	 * @return
	 */
	@Override
	public MutableList<GenderDTO> readAll(String langCode) {
		
		langCode = languageService.validLanguage(langCode);
		
		List<Gender> genderList = genderRepository.findAll();

		if (genderList.isEmpty()) {
			throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
					ApiErrorEnum.GENDERS_NO_RESULTS.getCode(), ApiErrorEnum.GENDERS_NO_RESULTS.getMessage())));
		} else {
			
			MutableList<GenderDTO> genderDTOS = Lists.mutable.empty();
			for(Gender i : genderList) {
				
				Traslation traslation =  traslationService.findByKeyAndIsoCode(i.getKey(), langCode);
				
				if(traslation == null) {
					i.setValue(i.getKey());
				}else {
					i.setValue(traslation.getValue());
				}
				genderDTOS.add(GenderMapper.mapGender(i));
			}
			return genderDTOS;
		}

	}

	/**
	 * @param request
	 * @return
	 */
	@Override
	public Gender create(GenderCreateDTO request) {

		try {
			Gender gender = new Gender();
			gender.setValue(request.getValue());
			return genderRepository.save(gender);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GenderService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 */
	@Override
	public void patchById(Long id, GenderEditDTO request) {

		try {
			Optional<Gender> gender = genderRepository.findById(id);

			if (!gender.isPresent()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.GENDERS_NO_RESULTS.getCode(), ApiErrorEnum.GENDERS_NO_RESULTS.getMessage())));
			} else {

				gender.get().setValue(request.getValue());
				genderRepository.save(gender.get());
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GenderService.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {

		try {
			genderRepository.findById(id).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(new ApiError(
					ApiErrorEnum.GENDERS_NO_RESULTS.getCode(), ApiErrorEnum.GENDERS_NO_RESULTS.getMessage()))));

			genderRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GenderService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<Gender> findById(Long id) {
		
		return genderRepository.findById(id);
	}
}
