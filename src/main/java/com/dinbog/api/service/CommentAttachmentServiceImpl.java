/**
 * 
 */
package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.AttachmentComment;
import com.dinbog.api.models.entity.CommentAttachment;
import com.dinbog.api.repository.AttachmentCommentRepository;
import com.dinbog.api.repository.AttachmentRepository;
import com.dinbog.api.repository.CommentAttachmentRepository;
import com.dinbog.api.repository.ProfileRepository;


/**
 * @author Luis
 *
 */
@Service
@Transactional
public class CommentAttachmentServiceImpl implements ICommentAttachmentService {

	@Autowired
	AttachmentCommentRepository attachmentCommentRepository;

	@Autowired
	AttachmentRepository attachmentRepository;

	@Autowired
	ProfileRepository profileRepository;
	
	@Autowired
	IApiErrorService errorService;
	
	@Autowired
	CommentAttachmentRepository commentAttachmentRepository;

	
	@Override
	public void delete(Long id, String lang) {

		// VALIDA QUE EL COMMENT A ELIMINAR EXISTA
		CommentAttachment commentAttachment = commentAttachmentRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_COMMENT_NOT_FOUND, lang))));
		
		AttachmentComment attachmentComment = attachmentCommentRepository.findByCommentAttachment(commentAttachment)
				.orElse(null);
		
		if(attachmentComment != null) {
			// ELIMINA EL ATTACHMENT_COMMENT (si existe)
			attachmentCommentRepository.deleteById(attachmentComment.getId());
		}
		
		// ELIMINA EL COMMENT_ATTACHMENT 
		commentAttachmentRepository.deleteById(commentAttachment.getId());
		

	}


	@Override
	public Optional<CommentAttachment> findById(Long id) {
		
		return commentAttachmentRepository.findById(id);
	}


	@Override
	public void save(CommentAttachment commentAttachment) {
		
		commentAttachmentRepository.save(commentAttachment);
		
	}
	

	
}
