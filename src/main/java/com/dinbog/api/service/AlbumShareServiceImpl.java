package com.dinbog.api.service;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.AlbumShareCreateDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.AlbumShare;
import com.dinbog.api.repository.AlbumShareRepository;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlbumShareServiceImpl implements IAlbumShareService {

    @Autowired
    private AlbumShareRepository albumShareRepository;

    @Autowired
    private IProfileService profileService;

    @Autowired
    private IAlbumService albumService;

    @Override
	@Transactional(propagation = Propagation.REQUIRED)
    public AlbumShare create(AlbumShareCreateDTO request){
        try {
            AlbumShare albumShare = new AlbumShare();
            albumShare
                    .setOwnerProfile(profileService.findById(request.getProfileId())
                            .orElseThrow(() -> new InvalidRequestException(
                                    Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
                                            ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
            albumShare
                    .setAlbum(albumService.findById(request.getAlbumId())
                            .orElseThrow(() -> new InvalidRequestException(
                                    Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
                                            ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())))));

            return albumShareRepository.save(albumShare);
         } catch (Exception e) {
            // VALIDA SI ES ERROR CONTROLADO
            if (Util.isCustomException(e))
                throw e;
            // EN CASO DE ERROR NO CONTROLADO
            log.error("*** Exception EN [AlbumShareService.create]");
            Util.printLogError(e);
            throw new InternalErrorException(e);
        }
    }

}
