package com.dinbog.api.service;

import com.dinbog.api.models.entity.CommentsAlbum;
import com.dinbog.api.models.entity.CommentsAlbumMention;

import java.util.List;

public interface ICommentsAlbumMentionService {

    void create(CommentsAlbum commentsAlbum, List<Long> mentions);

    List<CommentsAlbumMention> findAllMentionsInComment(CommentsAlbum commentsAlbum);

    void delete(CommentsAlbumMention commentsAlbumMention);
}
