package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.repository.ProfileFollowRepository;
import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileFollow;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@Service
public class ProfileFollowServiceImpl implements IProfileFollowService {

	@Autowired
	private ProfileFollowRepository profileFollowRepository;

	@Autowired
	private IApiErrorService errorService;

	@Override
	public ProfileFollow getProfileFollowById(Long id) {

		try {

			return profileFollowRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_FOLLOW_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_FOLLOW_NOT_FOUND.getMessage()))));
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileFollowService.getProfileFollowById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void delete(Profile profileOwner, Profile profileFollow, String lang) {
		
		try {
			// VALIDA QUE LOS DOS PERFILES NO SEAN LOS MISMOS
			if (profileOwner.getId().compareTo(profileFollow.getId()) == 0)
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FOLLOWER_EQUALS_FOLLOWED, lang)));	
			
			// VALIDA QUE EL FOLLOW EXISTA PREVIAMENTE
			Optional<ProfileFollow> valida = profileFollowRepository.findByOwnerProfileAndProfile(profileOwner, profileFollow);
			
			if (!valida.isPresent()) {
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_FOLLOW_NOT_FOUND, lang)));	
			}else {

				profileFollowRepository.delete(valida.get());
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileLikeService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}



	@Override
	public ProfileFollow create(Profile profileOwner, Profile profileFollow, String lang) {
		
		// VALIDA QUE LOS DOS PERFILES NO SEAN LOS MISMOS
		if (profileOwner.getId().compareTo(profileFollow.getId()) == 0)
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FOLLOWER_EQUALS_FOLLOWED, lang)));

		// VALIDA QUE EL FOLLOW NO EXISTA PREVIAMENTE
		ProfileFollow pf;
		Optional<ProfileFollow> valida = profileFollowRepository.findByOwnerProfileAndProfile(profileOwner,	profileFollow);

		if (valida.isPresent()) {
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FOLLOW_EXISTS, lang)));
		}else {
				pf = new ProfileFollow();
				pf.setOwnerProfile(profileOwner);
				pf.setProfile(profileFollow);
		}
		
		profileFollowRepository.save(pf);
		
		return pf;
	}

	@Override
	public List<ProfileFollow> findAll() {
		
		return profileFollowRepository.findAll();
	}

	@Override
	public Optional<List<ProfileFollow>> findFollows(Profile ownerProfile, Pageable paging) {
		
		return profileFollowRepository.findByOwnerProfile(ownerProfile, paging);
	}

	@Override
	public Optional<List<ProfileFollow>> findFollowers(Profile profile, Pageable paging) {
		
		return profileFollowRepository.findByProfile(profile, paging);
	}

	@Override
	public Optional<ProfileFollow> findByOwnerProfileAndProfile(Profile profileOwner, Profile profile) {
		
		return profileFollowRepository.findByOwnerProfileAndProfile(profileOwner, profile);
	}

}
