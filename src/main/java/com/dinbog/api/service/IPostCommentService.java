package com.dinbog.api.service;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dinbog.api.models.entity.CommentsPost;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostComment;

public interface IPostCommentService {

	
	PostComment create(Post post, CommentsPost comment);

	List<PostComment> findByPost(Post post);
	
	void deleteByPost(Post post);
	
}