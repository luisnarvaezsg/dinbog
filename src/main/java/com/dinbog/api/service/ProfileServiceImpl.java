package com.dinbog.api.service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import com.dinbog.api.aws.Avatar;
import com.dinbog.api.aws.IFaceRecognitionService;
import com.dinbog.api.aws.IS3Services;
import com.dinbog.api.commons.AlbumTypeEnum;
import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.dto.*;
import com.dinbog.api.models.entity.*;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.repository.ProfileRepository;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileFollowMapper;
import com.dinbog.api.util.mapper.ProfileMapper;
import com.dinbog.api.util.mapper.ProfileWorkMapper;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileServiceImpl implements IProfileService {

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private IProfileTypeService profileTypeService;

	@Autowired
	private IMembershipService membershipService;

	@Autowired
	private ISocialAppService socialAppService;

	@Autowired
	private ISocialAccountService socialAccountService;

	@Autowired
	private IProfileFollowService profileFollowService;

	@Autowired
	private IProfileDetailService profileDetailService;

	@Autowired
	private IUserService userService;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private IAttachmentService attachmentService;

	@Autowired
	private IProfileAttachmentService profileAttachmentService;

	@Autowired
	private IProfileWorkService profileWorkService;

	@Autowired
	private IAlbumService albumService;

	@Autowired
	private IAlbumAttachmentService albumAttachmentService;

	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private IProfileLikeService profileLikeService;
	
	@Autowired
	private IProfileFilterValueService profileFilterValueService;
	
	@Autowired
	private IProfileTalentService profileTalentService;
	
	@Autowired
	private IProfileCompanyService profileCompanyService;
	
	@Autowired
	private IProfileCategoryService profileCategoryService;
	
	@Autowired
	private IProfileBlockService profileBlockService;
	
	@Autowired
	private IFaceRecognitionService faceRecognitionService;
	
	@Autowired
	private IS3Services s3Service;
	
	@Value("${aws.attachment.video.bucket}")
	private String bucketName;

	/**
	 * @param id
	 * @return
	 */
	@Override
	public Profile readById(Long id, String lang) {

		try {
			log.info("Valor a buscar id " + id.toString());
			Optional<Profile> profile = profileRepository.findById(id);
			log.info("Valor a profile por id" + profile.toString());

			if (!profile.isPresent()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));

			}
			return profile.get();

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.readById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@Override
	public Profile readByUrl(String urlName, String lang) {

		try {
			Profile profile = profileRepository.findByUrlName(urlName);
			
			if (profile == null) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
			}
			profile.getProfileCategories();
			return profile;

		} catch (Exception e) {
			throw new InternalErrorException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@Override
	public ProfileFullDTO readFullByUrl(String urlName, User user, String lang) {

		Profile profileOwner = getProfile(user, lang);
		Profile profile = profileRepository.findByUrlName(urlName);

		if (profile == null) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

		}
		profile.setProfileCategories(profileCategoryService.findByProfileId(profile.getId(), lang));
		ProfileFullDTO response = ProfileMapper.mapProfileFull(profile,
				profileDetailService.getBasicDetails(profile.getId()));
		response.setBlocked(isBlocked(profileOwner, profile));
		response.setLiked(isLiked(profileOwner, profile));
		response.setConnected(false);
		response.setFollowed(isFollowed(profileOwner, profile));
		response.setUser(userService.getById(response.getUser().getId()));

		return response;

	}

	@Override
	public ProfileStatisticsDTO getProfileStatistics(String urlName) {

		try {
			Profile profile = profileRepository.findByUrlName(urlName);

			if (profile == null) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			}

			return ProfileMapper.mapProfileStatistics(profile);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.getProfileStatistics]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param search
	 * @return
	 */
	@Override
	@Transactional
	public PaginateResponse<ProfileDTO> readAll(Integer pageNumber, Integer pageSize, String search, String lang) {

		try {
			MutableList<ProfileDTO> profileDTOS = Lists.mutable.empty();
			PageRequest request = PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id")));

			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Profile> criteriaQuery = criteriaBuilder.createQuery(Profile.class);
			Root<Profile> from = criteriaQuery.from(Profile.class);
			CriteriaQuery<Profile> select = criteriaQuery.select(from);

			MutableList<Predicate> predicates = Lists.mutable.empty();
			if (!Util.isEmpty(search)) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					ProfileFilterDTO dao = mapper.readValue(search, ProfileFilterDTO.class);

					// Valida el first or last name
					if (!Util.isEmpty(dao.getFirstName())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(from.get("firstName")),
										"%" + dao.getFirstName().toLowerCase() + "%")));
					}

					if (!Util.isEmpty(dao.getLastName())) {
						predicates.add(
								criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(from.get("lastName")),
										"%" + dao.getLastName().toLowerCase() + "%")));
					}

					if (!Util.isEmpty(dao.getGender()) && dao.getGender().isEmpty()) {

						predicates.add(criteriaBuilder.and(from.get("gender").in(dao.getGender())));
					}

					if (!Util.isEmpty(dao.getProfileTypeId())) {
						predicates.add(criteriaBuilder
								.and(criteriaBuilder.equal(from.get("profileType"), dao.getProfileTypeId())));
					}

					if (!Util.isEmpty(dao.getCreated())) {
						predicates
								.add(criteriaBuilder.and(criteriaBuilder.equal(from.get("created"), dao.getCreated())));
					}

					if (!Util.isEmpty(dao.getProfileCategoryTypeId())) {
						Subquery<ProfileCategory> subQuery = criteriaQuery.subquery(ProfileCategory.class);
						Root<ProfileCategory> fromProject = subQuery.from(ProfileCategory.class);

						subQuery.select(fromProject.get("profile")); // field to map with main-query
						subQuery.where(criteriaBuilder.and(criteriaBuilder.equal(fromProject.get("profileCategoryType"),
								dao.getProfileCategoryTypeId())));

						predicates.add(criteriaBuilder.in(from.get("id")).value(subQuery));
					}

				} catch (IOException ioEx) {
					log.error("#error: " + ioEx.getMessage());
				}
			}

			Predicate[] predicateArray = new Predicate[predicates.size()];
			predicates.toArray(predicateArray);
			select.where(predicateArray);

			select.orderBy(criteriaBuilder.desc(from.get("created")));

			TypedQuery<Profile> query = entityManager.createQuery(select);

			int totalRows = query.getResultList().size();

			query.setFirstResult((request).getPageNumber() * (request).getPageSize());
			query.setMaxResults((request).getPageSize());

			Page<Profile> page = new PageImpl<>(query.getResultList(), request, totalRows);
			if (page.getTotalElements() > 0L) {
				page.getContent().forEach(i -> log.info("Information I-->>> " + i.toString()));

				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), profileDTOS);
			} else {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NO_RESULTS.getCode(), ApiErrorEnum.PROFILE_NO_RESULTS.getMessage())));
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param urlName
	 * @return
	 */
	@Override
	public ProfileFullDTO editFullByUrl(String urlName, ProfileFullRequestDTO request, String lang) {

		try {
			// BUSCA EL PROFILE
			Profile profile = profileRepository.findByUrlName(urlName);
			if (profile == null) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			}
			// CONSTRUYE PROFILE CON REQUEST
			profile = ProfileMapper.requestToEntity(profile, request);
			// PROFILE TYPE (si aplica)
			if (!Util.isEmpty(request.getProfileTypeId())) {
				profile.setProfileType(profileTypeService.findById(request.getProfileTypeId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getCode(),
										ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getMessage())))));
			}
			// MEMBERSHIP (si aplica)
			if (!Util.isEmpty(request.getMembershipId())) {
				profile.setMembership(membershipService.findById(request.getMembershipId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.MEMBERSHIP_NOT_FOUND.getCode(),
										ApiErrorEnum.MEMBERSHIP_NOT_FOUND.getMessage())))));
			}
			// GUARDA CAMBIOS
			profile = profileRepository.save(profile);

			// ARMA RESPUESTA
			ProfileFullDTO response = ProfileMapper.mapProfileFull(profile,
					profileDetailService.getBasicDetails(profile.getId()));

			// BUSCA EL USER COMPLETO
			response.setUser(userService.getById(response.getUser().getId()));

			return response;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.editFullByUrl]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @param profileId
	 * @return
	 */
	@Override
	public SocialAccount createSocialAccount(SocialAccountCreateDTO request, Long profileId) {

		SocialAccount socialAccount = new SocialAccount();

		try {
			// ARMA EL SOCIAL ACCOUNT
			socialAccount.setData(request.getData());
			socialAccount.setCreated(new Date());
			socialAccount.setLastLogin(new Date());

			// PROFILE
			Optional<Profile> profile = profileRepository.findById(profileId);
			if (profile.isPresent()) {
				socialAccount.setProfile(profile.get());
			} else {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.PROFILE_NOT_FOUND.getCode(), ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())));
			}

			// SOCIAL APP
			Optional<SocialApp> socialApp = socialAppService.findById(request.getSocialAppId());
			if (socialApp.isPresent()) {
				socialAccount.setSocialApp(socialApp.get());
			} else {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.SOCIAL_APP_NOT_FOUND.getCode(), ApiErrorEnum.SOCIAL_APP_NOT_FOUND.getMessage())));
			}

			return socialAccountService.create(socialAccount);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.createSocialAccount]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param ownerProfileId
	 * @param profileId
	 */
	@Override
	public void createFollow(User user, String urlProfileFollow, String lang) {

		try {
			Profile profileOwner = getProfile(user, lang);
			Profile profileFollow = readByUrl(urlProfileFollow, lang);

			profileFollowService.create(profileOwner, profileFollow, lang);

			profileFollow.addFollower();
			profileRepository.saveAndFlush(profileFollow);
			profileOwner.addFollow();
			profileRepository.saveAndFlush(profileOwner);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.createFollow]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param ownerProfileId
	 */
	@Override
	public MutableList<ProfileFollowDTO> getFollowers(String urlname, String typeFollow, String lang, Integer page, Integer size) {

		try {

			Optional<Profile> owner = findByUrlName(urlname);
			MutableList<ProfileFollowDTO> response = Lists.mutable.empty();
			page = page - 1;
			
			if(owner.isPresent()) {
				Optional<List<ProfileFollow>> profilesFollows = Optional.of(new ArrayList<>());
				
				if (typeFollow.equals("followings")) {
					Pageable paging = PageRequest.of(page, size, Sort.by("profile.urlName").ascending());
					profilesFollows = profileFollowService.findFollows(owner.get(),paging);
				} else {
					Pageable paging = PageRequest.of(page, size, Sort.by("ownerProfile.urlName").ascending());
					profilesFollows = profileFollowService.findFollowers(owner.get(),paging);
				}
	
				if (profilesFollows.isPresent()) {
					profilesFollows.get().forEach(p -> 
					{
						p.getProfile().setProfileCategories(profileCategoryService.findByProfileId(p.getProfile().getId(), lang));
						ProfileFollowDTO pf = ProfileFollowMapper.mapFollowActionMapper(p, typeFollow);
						if (typeFollow.equals("followings")) {
							pf.getProfile().setBlocked(isBlocked(owner.get(), p.getProfile()));
							pf.getProfile().setLiked(isLiked(owner.get(),  p.getProfile()));
							pf.getProfile().setConnected(false);
							pf.getProfile().setFollowed(isFollowed(owner.get(),  p.getProfile()));
						}else {
							pf.getProfile().setBlocked(isBlocked(owner.get(), p.getOwnerProfile()));
							pf.getProfile().setLiked(isLiked(owner.get(),  p.getOwnerProfile()));
							pf.getProfile().setConnected(false);
							pf.getProfile().setFollowed(isFollowed(owner.get(),  p.getOwnerProfile()));
						}
						response.add(pf);
					});	}
				
				}else {
					throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
				}
			return response;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.getFollowers]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Trae una lista de usuarios sugeridos. Actualmente los profiles se eligen al
	 * azar
	 * 
	 * @param profileId
	 * @param size
	 * @return
	 */
	@Override
	public MutableList<ProfileSimpleDTO> getSuggesteds(Long profileId, Integer size, String lang) {

		try {
			// reemplazar este método con llamado a stored procedure que entregue
			// usuarios sugeridos

			MutableList<ProfileSimpleDTO> response = Lists.mutable.empty();

			// TRAE DATA PAGINADA AL AZAR
			Long totalRecords = profileRepository.count();
			long totalPages = (totalRecords % size == 0) ? (totalRecords / size) : ((totalRecords / size) + 1);
			int pageIndex = new Random().nextInt((int) totalPages);
			Page<Profile> randomPage = profileRepository.findAll(PageRequest.of(pageIndex, size));

			// SE DESORDENA DATA
			List<Profile> randomList = new ArrayList<>();
			if (randomPage.getTotalElements() > 0) {
				randomList = new ArrayList<>(randomPage.getContent());
				Collections.shuffle(randomList);
			}

			// CONSTRUYE RESPONSE
			randomList.forEach(p -> response.add(ProfileMapper.mapProfileSimple(p, null, null, null, null,null)));

			return response;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.getSuggesteds]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Trae los profiles vistos por un profile
	 * 
	 * @param profileId
	 * @return
	 */
	@Override
	public MutableList<ProfileSimpleDTO> getProfilesViewed(Long profileId, String lang) {

		MutableList<ProfileSimpleDTO> response = Lists.mutable.empty();

		try {
			Optional<List<Profile>> profiles = profileRepository.findByProfileView(profileId);

			if (profiles.isPresent()) {

				profiles.get().forEach(p -> {

					Optional<Profile> profile = profileRepository.findById(p.getId());

					if (profile.isPresent()) {
						response.add(ProfileMapper.mapProfileSimple(profile.get(), null, null, null, null,null));
					}
				});
			}
			return response;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.getProfilesViewed]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @param profileId
	 * @param socialAccountId
	 * @return
	 */
	@Override
	public SocialAccount patchSocialAccount(SocialAccountPatchDTO request, Long profileId, Long socialAccountId) {

		try {
			Boolean modificado = false;
			SocialAccount socialAccount = socialAccountService.findById(socialAccountId)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getCode(),
									ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getMessage()))));

			// VALIDA QUE EL SOCIAL ACCOUNT PERTENEZA AL PROFILE
			if (profileId.compareTo(socialAccount.getProfile().getId()) != 0) {
				// error
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NO_MATCH.getCode(),
								ApiErrorEnum.SOCIAL_ACCOUNT_NO_MATCH.getMessage())));
			}
			// SOCIAL APP
			if (!Util.isEmpty(request.getSocialAppId())
					&& request.getSocialAppId().compareTo(socialAccount.getSocialApp().getId()) != 0) {
				modificado = true;
				socialAccount.setSocialApp(socialAppService.findById(request.getSocialAppId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getCode(),
										ApiErrorEnum.SOCIAL_ACCOUNT_NOT_FOUND.getMessage())))));
			}
			// DATA
			if (!Util.isEmpty(request.getData()) && request.getData().compareTo(socialAccount.getData()) != 0) {
				modificado = true;
				socialAccount.setData(request.getData());
			}
			// LAST LOGIN
			if (!Util.isEmpty(request.getLastLogin())
					&& request.getLastLogin().compareTo(socialAccount.getLastLogin()) != 0) {
				modificado = true;
				socialAccount.setLastLogin(request.getLastLogin());
			}

			if (Boolean.TRUE.equals(modificado)) {
				// ACTUALIZA SOCIAL ACCOUNT
				return socialAccountService.create(socialAccount);
			} else {
				// SI NO HAY CAMPOS QUE MODIFICAR ARROJA UN ERROR
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NO_PATCH.getCode(),
								ApiErrorEnum.SOCIAL_ACCOUNT_NO_PATCH.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.patchSocialAccount]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param ownerProfileId
	 * @param profileId
	 */
	@Override
	public void deleteFollow(User user, String urlProfileFollow, String lang) {

		try {
			
			Profile profileOwner = getProfile(user, lang);
			Profile profileFollow = readByUrl(urlProfileFollow, lang);
			
			profileFollow.unFollower();
			profileRepository.save(profileFollow);
			profileOwner.unFollow();
			profileRepository.save(profileOwner);
			
			profileFollowService.delete(profileOwner, profileFollow, lang);		

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.deleteFollow]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	/**
	 * @param profileId
	 */
	@Override
	public void deleteProfile(Long profileId) {

		try {
			Optional<Profile> oProfile = Optional
					.ofNullable(profileRepository.findById(profileId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			if (oProfile.isPresent())
				profileRepository.deleteById(profileId);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.deleteProfile]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Album updoadBook(Long profileId, MultipartFile[] photos, MultipartFile cover, String title,
			String description, Boolean isPrivate, String lang) {

		Profile profile = new Profile();

		try {

			Optional<Profile> optional = profileRepository.findById(profileId);

			if (optional.isPresent())
				profile = optional.get();

		} catch (Exception e) {

			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}

		if (profile == null) {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

		}

		ProfileAttachment profileAttachment = new ProfileAttachment();

		Album album = new Album();
		album.setName(title);
		album.setDescription(description);
		album.setIsPrivate(isPrivate);
		album.setProfile(profile);
		album.setAlbumType(new AlbumType().setId(AlbumTypeEnum.BOOK.getAlbumTypeId()));
		album = albumService.save(album);

		// Insert the Cover:
		if (cover != null) {
			if (!cover.isEmpty()) {
				Attachment attachment = attachmentService.create(cover, cover, null, lang, this.bucketName);
				profileAttachment.setAttachment(attachment);
				profileAttachment.setProfile(profile);
				profileAttachmentService.create(profileAttachment);
				
				AlbumAttachment albumAttachment = new AlbumAttachment();
				albumAttachment.setAlbum(album);
				albumAttachment.setAttachment(attachment);
				albumAttachment.setIsCover(1L);
				albumAttachmentService.save(albumAttachment);
			}
		}

		profileRepository.save(profile);

		for (MultipartFile file : photos) {

			Attachment attachmentFor = attachmentService.create(file, cover, null, lang, this.bucketName);
			ProfileAttachment profileAttachmentFor = new ProfileAttachment();
			profileAttachmentFor.setAttachment(attachmentFor);
			profileAttachmentFor.setProfile(profile);
			profileAttachmentService.create(profileAttachmentFor);

			AlbumAttachment albumAttachmentFor = new AlbumAttachment();
			albumAttachmentFor.setAlbum(album);
			albumAttachmentFor.setAttachment(attachmentFor);
			albumAttachmentFor.setIsCover(0L);
			albumAttachmentService.save(albumAttachmentFor);

		}

		return album;

	}

	@Override
	public ProfileAttachment uploadVideo(Long profileId, MultipartFile file, MultipartFile cover, String title,
			String description, Boolean isPrivate, String lang) {

		Profile profile = new Profile();
		try {

			Optional<Profile> optional = profileRepository.findById(profileId);

			if (optional.isPresent())
				profile = optional.get();

		} catch (Exception e) {

			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}

		if (profile == null) {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

		}
		ProfileAttachment profileAttachment = new ProfileAttachment();

		Attachment attachment = attachmentService.create(file, cover, null, lang, this.bucketName);
		profileAttachment.setAttachment(attachment);
		profileAttachment.setProfile(profile);
		profileAttachment = profileAttachmentService.create(profileAttachment);

		Album album = new Album();
		album.setName(title);
		album.setDescription(description);
		album.setIsPrivate(isPrivate);
		album.setProfile(profile);
		album.setAlbumType(new AlbumType().setId(AlbumTypeEnum.VIDEO.getAlbumTypeId()));
		album = albumService.save(album);

		AlbumAttachment albumAttachment = new AlbumAttachment();
		albumAttachment.setAlbum(album);
		albumAttachment.setAttachment(attachment);
		albumAttachment.setIsCover(0L);
		albumAttachmentService.save(albumAttachment);

		return profileAttachment;

	}

	@Override
	public ProfileAttachment uploadVideoURL(Long profileId, String filePath, MultipartFile cover, String title,
			String description, Boolean isPrivate, String lang) {

		Profile profile = new Profile();

		try {

			Optional<Profile> optional = profileRepository.findById(profileId);

			if (optional.isPresent())
				profile = optional.get();

		} catch (Exception e) {

			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}

		if (profile == null) {

			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

		}

		ProfileAttachment profileAttachment = new ProfileAttachment();

		Attachment attachment = attachmentService.create(filePath, lang);
		profileAttachment.setAttachment(attachment);
		profileAttachment.setProfile(profile);
		profileAttachment = profileAttachmentService.create(profileAttachment);

		Album album = new Album();
		album.setName(title);
		album.setDescription(description);
		album.setIsPrivate(isPrivate);
		album.setProfile(profile);
		album.setAlbumType(new AlbumType().setId(AlbumTypeEnum.VIDEO.getAlbumTypeId()));
		album = albumService.save(album);

		AlbumAttachment albumAttachment = new AlbumAttachment();
		albumAttachment.setAlbum(album);
		albumAttachment.setAttachment(attachment);
		albumAttachment.setIsCover(0L);

		albumAttachmentService.save(albumAttachment);

		return profileAttachment;

	}

	@Override
	@Transactional
	public ProfileWorkDTO uploadWork(ProfileWorkDTO profileWorkDTO, String lang) {

		ProfileWork profileWork = null;

		Profile profile;

		Optional<Profile> optional = profileRepository.findById(profileWorkDTO.getProfileId());

		if (optional.isPresent()) {

			profile = optional.get();

		} else {

			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}

		if (Objects.isNull(profileWorkDTO.getId()) || profileWorkDTO.getId() < 1) {

			profileWork = new ProfileWork();

		} else {

			profileWork = profileWorkService.findById(profileWorkDTO.getId());

			if (Objects.isNull(profileWork.getId())) {

				throw new ResourceNotFoundException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_WORK_NOT_FOUND, lang)));
			}

		}

		profileWork.setClient(profileWorkDTO.getClient());
		profileWork.setDescription(profileWorkDTO.getDescription());
		profileWork.setYearJob(profileWorkDTO.getYearJob());
		profileWork.setProfile(profile);

		if (Objects.isNull(profileWorkDTO.getId()) || profileWorkDTO.getId() < 1) {
			profileWorkService.save(profileWork);
		}

		return ProfileWorkMapper.mapProfileWork(profileWork);

	}

	@Override
	public Album updoadAudioFile(Long albumId, MultipartFile audio, MultipartFile cover,
								 String metadata, Boolean isPrivate, String language) {

		Album album;

		Optional<Album> optional = albumService.findById(albumId);

		if (optional.isPresent()) {
			album = optional.get();

		} else {

			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, language)));
		}

		if (cover != null) {

			if (!cover.isEmpty()) {

				// Insert the Cover:
				Attachment attachment = attachmentService.create(cover, cover, null, language, this.bucketName);

				AlbumAttachment albumAttachment = new AlbumAttachment();
				albumAttachment.setAlbum(album);
				albumAttachment.setAttachment(attachment);
				albumAttachment.setIsCover(1L);

				albumAttachmentService.save(albumAttachment);
			}
		}

		Attachment attachmentAudio = attachmentService.create(audio, cover, metadata, language, this.bucketName);
		AlbumAttachment albumAttachmentAudio = new AlbumAttachment();
		albumAttachmentAudio.setAlbum(album);
		albumAttachmentAudio.setAttachment(attachmentAudio);
		albumAttachmentAudio.setIsCover(0L);

		albumAttachmentService.save(albumAttachmentAudio);

		return album;

	}

	@Override
	public MutableList<ProfileBasicDTO> readByNameLike(String name, String lang) {

		MutableList<ProfileBasicDTO> response = Lists.mutable.empty();
		List<Profile> profiles = new ArrayList<>();

		try {
			Optional<List<Profile>> optional = profileRepository.findByFullNameContainingIgnoreCase(name);

			if (optional.isPresent()) {
				profiles = optional.get();
				profiles.forEach(p -> response.add(ProfileMapper.mapProfileBasic(p)));
			}

		} catch (Exception e) {

			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}

		return response;
	}

	@Override
	public Optional<Profile> findById(Long id) {
		return profileRepository.findById(id);
	}

	@Override
	public Optional<List<ProfileSuggestedDTO>> getSuggestedProfileByUserName(User user, String name) {
		List<ProfileSuggestedDTO> suggested = new ArrayList<>();
		Optional<Profile> profile = profileRepository.findByUser(user);
		if (profile.isPresent()) {

			Stream<Supplier<Optional<List<Profile>>>> queries = Stream.of(
					() -> profileRepository.findAllSuggestedFollowersProfilesActivatedByFullName(profile.get().getId(),
							name.toLowerCase()),
					() -> profileRepository.findAllSuggestedOthersProfilesActivatedByFullName(profile.get().getId(),
							name.toLowerCase()));

			for (CompletableFuture<Optional<List<Profile>>> optionalCompletableFuture : queries
					.map(CompletableFuture::supplyAsync).collect(Collectors.toList())) {
				Optional<List<Profile>> join = optionalCompletableFuture.join();
				join.ifPresent(profiles -> suggested.addAll(profiles.stream().parallel().distinct()
						.map(ProfileSuggestedDTO::new).collect(Collectors.toList())));
			}
		}
		return Optional.of(suggested);
	}



	@Override
	public Album updoadAudioAlbum(Long profileId, MultipartFile cover, String title, String description,
			Boolean isPrivate, String language) {

		ProfileAttachment profileAttachment = new ProfileAttachment();

		Profile profile;

		Optional<Profile> optional = profileRepository.findById(profileId);

		if (optional.isPresent()) {
			profile = optional.get();
		} else {
			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, language)));
		}

		Album album = new Album();
		album.setName(title);
		album.setDescription(description);
		album.setIsPrivate(isPrivate);
		album.setProfile(profile);
		album.setAlbumType(new AlbumType().setId(AlbumTypeEnum.AUDIO.getAlbumTypeId()));
		album = albumService.save(album);

		if (cover != null) {

			if (!cover.isEmpty()) {

				// Insert the Cover:
				Attachment attachment = attachmentService.create(cover, cover, null, language, this.bucketName);
				profileAttachment.setAttachment(attachment);
				profileAttachment.setProfile(profile);
				profileAttachmentService.create(profileAttachment);
				profileRepository.save(profile);

				AlbumAttachment albumAttachment = new AlbumAttachment();
				albumAttachment.setAlbum(album);
				albumAttachment.setAttachment(attachment);
				albumAttachment.setIsCover(1L);

				albumAttachmentService.save(albumAttachment);
			}
		}

		return album;

	}

	@Override
	public Optional<Profile> findByUser(User user) {
		return profileRepository.findByUser(user);
	}


	@Override
	public Optional<Profile> findByUrlName(String urlName) {
		return profileRepository.findProfileByUrlName(urlName);
	}
	
	@Override
	public ProfileSimpleDTO buildProfileSimpleDTO(Profile profile, String lang) {
		ProfileTalent pt = profileTalentService.findByProfile(profile).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		String residenceCity = "";
		/**
		if (pt.getProfileCountries() != null && pt.getProfileCountries().size() > 0) {
			for (ProfileCountry profileCountry : pt.getProfileCountries()) {
				if (profileCountry.getProfileCountryType().getName().equals("Branch"))
					residence_city = profileCountry.getCountry().getName();
				else {
					if (residence_city.equals(""))
						residence_city = profileCountry.getCountry().getName();
				}

			}

		}
		**/
		List<ProfileCategory> listaCat=profileCategoryService.findByProfileId(profile.getId(), lang);
		return ProfileMapper.mapProfileSimple(profile, pt.getFirstName(), pt.getLastName(), pt.getGender().getValue(),
				residenceCity, listaCat);
	}

	@Override
	public void createLike(User user, String urlProfile, String lang) {

		try {
			
			Profile profileOwner = getProfile(user, lang);
			Profile profileLike = readByUrl(urlProfile, lang);

			profileLikeService.create(profileOwner, profileLike, lang);

			profileLike.addLike();
			profileRepository.saveAndFlush(profileLike);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.createLike]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void deleteLike(User user, String urlProfile, String lang) {

		try {
			Profile profileOwner =  getProfile(user, lang);
			Profile profileLike = readByUrl(urlProfile, lang);

			profileLikeService.delete(profileOwner, profileLike, lang);

			profileLike.unLike();
			profileRepository.saveAndFlush(profileLike);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileService.deleteLike]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void saveFilters(User user, List<ProfileFilterValueDTO> filters, String lang) {
		
		Profile profile = getProfile(user, lang);
		
		profileFilterValueService.saveFilters(profile, filters, lang);
	}
	
	@Override
	public Profile getProfile(User user, String lang) {
		
		Optional<Profile> oProfile = findByUser(user);
		
		if (oProfile.isPresent()) {

			return oProfile.get();

		} else {
			
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));

		}
	}

	@Override
	public ProfileDTO saveProfile(ProfileRequestDTO profileRequest, String lang) {
		
		if(Util.isEmpty(lang)) {
			
			lang = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		
		MutableList<ApiError> errors = validateRequest(profileRequest, lang);
		
		if (!errors.isEmpty()) {
			
			throw new InvalidRequestException(errors);
			
		} 
		
		ProfileDTO profile = new ProfileDTO();
		
		if(profileRequest.getProfileId() == null || profileRequest.getProfileId() < 1 ) {
			
			if(profileRequest.getProfileTypeId().equals(ProfileConstant.TALENT_PROFILE_TYPE)) {
				profile = ProfileMapper.mapProfileTalent(profileTalentService.createTalentProfile(profileRequest, lang));
			
			}else if(profileRequest.getProfileTypeId().equals(ProfileConstant.COMPANY_PROFILE_TYPE)){
			
				profile = ProfileMapper.mapProfileCompany(profileCompanyService.createCompanyProfile(profileRequest, lang));
			
			}
		}else {
			
			if(profileRequest.getProfileTypeId().equals(ProfileConstant.TALENT_PROFILE_TYPE)) {
				
				profile = ProfileMapper.mapProfileTalent(profileTalentService.update(profileRequest, lang));
			
			}else if(profileRequest.getProfileTypeId().equals(ProfileConstant.COMPANY_PROFILE_TYPE)){
			
				profile = ProfileMapper.mapProfileCompany(profileCompanyService.update(profileRequest, lang));
			
			}	
		}
	
		return profile;

	}

	@Override
	public Long countProfileByUrlName(String urlName) {
		
		return profileRepository.countProfileByUrlName(urlName);
	}

	@Override
	public Long count() {
		
		return profileRepository.count();
	}

	@Override
	public Optional<List<Profile>> findAllProfilesRandomNotInFollowers(Long id, Pageable pageable) {
		
		return profileRepository.findAllProfilesRandomNotInFollowers(id, pageable);
	}
	
	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validateRequest(ProfileRequestDTO request, String language) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();
		
		if(request.getProfileTypeId().equals(ProfileConstant.TALENT_PROFILE_TYPE)) {
			
			if (Util.isEmpty(request.getBirthDate())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_BIRTH_DATE, language));
			}
		
			if (Util.isEmpty(request.getGenderId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_GENDER_ID, language));
			}
	
			if (Util.isEmpty(request.getBornCountryId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_BORN_COUNTRY, language));
			}
	
			if (Util.isEmpty(request.getBornCityId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_BORN_CITY, language));
			}
	
			if (Util.isEmpty(request.getResidenceCountryId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_RESIDENCE_COUNTRY, language));
			}
			
			if (Util.isEmpty(request.getResidenceCityId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_RESIDENCE_CITY, language));
			}
		}
		
		if(request.getProfileTypeId().equals(ProfileConstant.COMPANY_PROFILE_TYPE)) {
			
			if (Util.isEmpty(request.getHeadQuarterCountryId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_HEADQUARTER_COUNTRY, language));
			}
	
			if (Util.isEmpty(request.getHeadQuarterCityId())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_HEADQUARTER_CITY, language));
			}
	
		}
	
		if (Util.isEmpty(request.getUserId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_USER_ID, language));
		}
		
		if (Util.isEmpty(request.getIsSearchable())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_IS_SEARCHABLE, language));
		}

		if (Util.isEmpty(request.getProfileTypeId())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_TYPE_ID, language));
		}

		return apiErrors;
	}

	private Boolean isFollowed(Profile profileOwner, Profile profile) {
		
		Boolean isFollowed = false;
		Optional<ProfileFollow> profileFollow = profileFollowService.findByOwnerProfileAndProfile(profileOwner, profile);
		
		if(profileFollow.isPresent()) {
			isFollowed = true;
		}
		return isFollowed;
	}
	
	private Boolean isBlocked(Profile profileOwner, Profile profile) {
		
		Boolean isBlocked = false;
		Optional<ProfileBlock> profileBlock = profileBlockService.findByOwnerAndProfile(profileOwner, profile);
		
		if(profileBlock.isPresent() && profileBlock.get().getStatus() == 1) {
			
			isBlocked = true;
		}
		return isBlocked;
	}

	@Override
	public void blockProfile(User user, String urlName, String lang) {
		
		Optional<Profile> profile = findByUrlName(urlName);
		Optional<Profile> owner = findByUser(user);
		
		MutableList<ApiError> errors = profileBlockService.validateProfileBlockOperation(owner,profile,lang);
		if(errors != null){
			throw new InvalidRequestException(errors);
		}
		
		if(profile.isPresent() && owner.isPresent()) {
			
			if(!profileBlockService.isProfileBlocked(owner.get().getId(),profile.get().getId())){
				profileBlockService.create(owner.get(), profile.get());
			}else{
				Optional<ProfileBlock> oProfileBlock = profileBlockService.findByOwnerAndProfile(owner.get(), profile.get());
				
				if(oProfileBlock.isPresent()) {
					ProfileBlock profileBlock = oProfileBlock.get();
					profileBlock.setStatus(1);
					profileBlockService.save(profileBlock);
				}
			}
		}else {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}
	}
	
	@Override
	public void unblockProfile(User user, String urlName, String lang) {
		
		Optional<Profile> profile = findByUrlName(urlName);
		Optional<Profile> owner = findByUser(user);

		MutableList<ApiError> errors = profileBlockService.validateProfileBlockOperation(owner,profile,lang);

		if(errors != null){
			throw new InvalidRequestException(errors);
		}

		if(profile.isPresent() && owner.isPresent()) {

			Optional<ProfileBlock> profileBlock = profileBlockService.findByOwnerAndProfile(owner.get(),profile.get());

			if(profileBlock.isPresent()){
				profileBlockService.remove(profileBlock.get());
			}else{
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_BLOCK_NOT_FOUND, lang))
				);
			}
		}else {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}

	}

	@Override
	public ProfileBasicDTO updateImagesProfile(User user, MultipartFile photo, String imageType, String lang) {
		
		Optional<Profile> oProfile = findByUser(user);
		if(oProfile.isPresent()){
			
			Profile profile = oProfile.get();
			String message = "";
			
			if (imageType.equals("avatar")) {
				Avatar avatar = faceRecognitionService.initFaceRecognition(photo, profile.getProfileType().getId());
				message = avatar.getMessage();
				if(avatar.isValidAvatar()) {
					profile.setAvatarPath(avatar.getUrlFile());
				}
			} else {
				String path = s3Service.uploadFile(photo);
				profile.setCoverPath(path);
			}
			profileRepository.save(profile);
			ProfileBasicDTO profileDTO =  ProfileMapper.mapProfileBasic(profile);
			profileDTO.setMessage(message);
			return profileDTO;
		}else{
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))
			);
		}
	}
	
	private Boolean isLiked(Profile profileOwner, Profile profile) {
		
		Boolean isLiked = false;
		Optional<ProfileLike> profileLike = profileLikeService.findByOwnerAndProfile(profileOwner, profile);
		
		if(profileLike.isPresent()) {
			
			isLiked = true;
		}
		return isLiked;
	}

	
}
