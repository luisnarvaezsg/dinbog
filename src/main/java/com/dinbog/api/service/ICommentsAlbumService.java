package com.dinbog.api.service;

import com.dinbog.api.dto.AlbumCommentRequestDTO;
import com.dinbog.api.models.entity.CommentsAlbum;
import com.dinbog.api.models.entity.Profile;

import java.util.List;


public interface ICommentsAlbumService {

    CommentsAlbum create(Profile profile, AlbumCommentRequestDTO requestDTO);

    List<Long> getMentions(String comment);

    List<CommentsAlbum> getCommentsAlbumByParentId(Long id);

    void delete(CommentsAlbum commentsAlbum);

}
