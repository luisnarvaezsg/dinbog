package com.dinbog.api.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.GroupBasicDTO;
import com.dinbog.api.dto.GroupRequestDTO;
import com.dinbog.api.repository.GroupRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Group;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.GroupMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GroupServiceImpl implements IGroupService {

	@Autowired
	GroupRepository groupRepository;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public GroupBasicDTO create(GroupRequestDTO request) {
		try {
			Group group = new Group();
			group.setName(request.getName());
			group.setDescription(request.getDescription());
			group.setIsActive(request.getIsActive());
			group.setCreated(new Date());
			return GroupMapper.mapGroupBasic(groupRepository.save(group));
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@Override
	public MutableList<GroupBasicDTO> readAll() {
		try {
			MutableList<GroupBasicDTO> groupDTOS = Lists.mutable.of();
			List<Group> group = groupRepository.findAll();
			if (group.isEmpty()) {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getCode(),
								ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getMessage())));
			}
			group.forEach(i -> groupDTOS.add(GroupMapper.mapGroupBasic(i)));
			return groupDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public GroupBasicDTO update(Long id, GroupRequestDTO request) {
		try {
			Group group = groupRepository.findById(id).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(
					new ApiError(ApiErrorEnum.GROUP_NOT_FOUND.getCode(), ApiErrorEnum.GROUP_NOT_FOUND.getMessage()))));
			group.setName(request.getName());
			group.setDescription(request.getDescription());
			group.setIsActive(request.getIsActive());
			group.setCreated(new Date());
			return GroupMapper.mapGroupBasic(groupRepository.save(group));
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {
		try {
			groupRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<Group> findById(Long id) {
		
		return groupRepository.findById(id);
	}

	@Override
	public Optional<List<Group>> findByUser(Long id) {
		
		return groupRepository.findByUser(id);
	}

}
