package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.CommentAttachment;

public interface ICommentAttachmentService {

	public void delete(Long id, String lang);

	public Optional<CommentAttachment> findById(Long parentId);

	public void save(CommentAttachment commentAttachment);

}