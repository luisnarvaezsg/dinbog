package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.EventStatus;

public interface IEventStatusService {

	Optional<EventStatus> findById(Long id, String lang);

}
