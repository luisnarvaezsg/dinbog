package com.dinbog.api.service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.models.entity.Post;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.PostHashtagDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.PostHashtag;
import com.dinbog.api.repository.PostHashtagRepository;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.PostHashtagMapper;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;

@Slf4j
@Transactional
@Service
public class PostHashtagServiceImpl implements IPostHashtagService {

	@Autowired
	private PostHashtagRepository postHashtagRepository;

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param nameHashTag
	 * @return
	 */
	@Override
	public PaginateResponse<PostHashtagDTO> readAll(Integer pageNumber, Integer pageSize, Boolean typeQuery,
			String nameHashTag, Boolean isSearch, String lang) {

		try {
			Page<PostHashtag> page = postHashtagRepository.findAll(new Specification<PostHashtag>() {
				private static final long serialVersionUID = -5273204285845443765L;

				@Override
				public Predicate toPredicate(Root<PostHashtag> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					MutableList<Predicate> predicates = Lists.mutable.empty();
					if (!Util.isEmpty(nameHashTag)) {
						if (!Util.isEmpty(typeQuery)) {
							Predicate criteria = (typeQuery)
									? criteriaBuilder.and(criteriaBuilder.equal(root.get("nameHasTag"), nameHashTag))
									: criteriaBuilder
											.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("nameHasTag")),
													"%" + nameHashTag.toLowerCase() + "%"));

							predicates.add(criteria);
						} else {
							predicates.add(criteriaBuilder
									.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("nameHasTag")),
											"%" + nameHashTag.toLowerCase() + "%")));
						}
					}

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id"))));

			if (page.getTotalElements() > 0L) {
				MutableList<PostHashtagDTO> postHashtagDTOS = Lists.mutable.of();
				Boolean isSearchs = (!Util.isEmpty(isSearch)) ? isSearch : false;
				page.getContent()
						.forEach(i -> postHashtagDTOS.add(PostHashtagMapper.mapPostHashtag(i, isSearchs)));

				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), postHashtagDTOS);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.POST_HASHTAG_NO_RESULTS.getCode(),
								ApiErrorEnum.POST_HASHTAG_NO_RESULTS.getMessage())));
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostHashtagService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void addHashTags(Post post, List<String> hashTags) {
		hashTags.forEach( hashTag -> {
			try{
				PostHashtag postHashtag = new PostHashtag(post,hashTag,new Date());
				postHashtagRepository.save(postHashtag);
			}catch (Exception e){
				Util.printLogError(e);
				throw new InternalErrorException(e);
			}
		});
	}

	@Override
	public List<PostHashtag> findByPost(Post post) {
		
		return postHashtagRepository.findByPost(post);
	}

	@Override
	@Transactional
	public void deleteByPost(Post post) {
		
		List<PostHashtag> listaPH=findByPost(post);
		if ((listaPH!=null)&& !listaPH.isEmpty())
			listaPH.forEach(postHashtagRepository::delete);
	}

	@Override
	public Page<PostHashtag> findAll(Specification<PostHashtag> specification, PageRequest of) {
		
		return postHashtagRepository.findAll(specification, of);
	}

	@Override
	public Page<PostHashtag> findByNameHasTagContainingIgnoreCase(Pageable pageable, String hashtag) {
		
		return postHashtagRepository.findByNameHasTagContainingIgnoreCase(pageable, hashtag);
	}
}
