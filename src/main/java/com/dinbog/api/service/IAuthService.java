package com.dinbog.api.service;

import com.dinbog.api.dto.ConfirmEmailDTO;
import com.dinbog.api.dto.LoginDTO;
import com.dinbog.api.dto.LoginResponseDTO;

public interface IAuthService {

	/**
	 * Devuelve la respuesta correcta del login, si el password enviado en el
	 * request coincide con el que esta almacenado
	 * 
	 * @param request
	 * @return
	 */
	LoginResponseDTO authenticateUser(LoginDTO request, String language);

	/**
	 * @param token
	 */
	ConfirmEmailDTO confirmEmailToken(String token);

}