package com.dinbog.api.service;

import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.OptionDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Option;
import com.dinbog.api.repository.OptionRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.OptionMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("OptionService")
public class OptionServiceImpl implements IOptionService {

	@Autowired
	private OptionRepository optionRepository;

	/**
	 * @return
	 */
	@Override
	public MutableList<OptionDTO> readAll() {

		try {
			List<Option> optionList = optionRepository.findAll();

			if (optionList.isEmpty()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.OPTIONS_NO_RESULTS.getCode(), ApiErrorEnum.OPTIONS_NO_RESULTS.getMessage())));
			} else {
				MutableList<OptionDTO> optionDTOS = Lists.mutable.of();

				optionList.forEach(i -> optionDTOS.add(OptionMapper.mapOption(i)));
				return optionDTOS;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [OptionService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param fieldId
	 * @return
	 */
	@Override
	public MutableList<OptionDTO> readOptionsByFieldId(Long fieldId) {

		try {
			List<Option> optionList = optionRepository.findByFieldId(fieldId);

			if (optionList.isEmpty()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.OPTIONS_NO_RESULTS.getCode(), ApiErrorEnum.OPTIONS_NO_RESULTS.getMessage())));
			} else {
				MutableList<OptionDTO> optionDtos = Lists.mutable.of();
				optionList.forEach(i -> optionDtos.add(OptionMapper.mapOption(i)));

				return optionDtos;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [OptionService.readOptionsByFieldId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
