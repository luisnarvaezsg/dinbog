package com.dinbog.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Membership;
import com.dinbog.api.repository.MembershipRepository;


@Transactional
@Service
public class MembershipServiceImpl implements IMembershipService {

	@Autowired
	private MembershipRepository membershipRepository;
	
	@Override
	public Optional<Membership> findById(Long id) {
		
		return membershipRepository.findById(id);
	}

}
