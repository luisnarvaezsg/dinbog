package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.CountryDTO;
import com.dinbog.api.dto.CountryRequestDTO;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.Country;

public interface ICountryService {

	/**
	 * @param request
	 * @return
	 */
	Country create(CountryRequestDTO request);

	/**
	 * @param languageId
	 * @return
	 * @throws InternalErrorException
	 */
	MutableList<CountryDTO> readAll(String langCode);

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	Country update(Long id, CountryRequestDTO request);

	/**
	 * @param id
	 */
	void deleteById(Long id);
	
	Optional<Country> findById(Long id, String lang);

}