package com.dinbog.api.service;

import java.util.List;

import com.dinbog.api.dto.ProfileFilterValueDTO;
import com.dinbog.api.models.entity.Profile;

public interface IProfileFilterValueService {

	void saveFilters(Profile profile, List<ProfileFilterValueDTO> filters, String lang);
	
	List<ProfileFilterValueDTO> getFilters();

}