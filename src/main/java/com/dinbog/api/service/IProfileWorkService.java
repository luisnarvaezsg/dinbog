package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.ProfileWorkDTO;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileWork;

public interface IProfileWorkService {

	ProfileWork save(ProfileWork profileWork);

	ProfileWork findById(Long id);

	MutableList<ProfileWorkDTO> findByProfile(Profile profile, String lang);
}