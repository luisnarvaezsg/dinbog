package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.Membership;

public interface IMembershipService {

	Optional<Membership> findById(Long id);

}