package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileReportCreateDTO;
import com.dinbog.api.repository.ProfileReportRepository;
import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.ProfileReport;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileReportServiceImpl implements IProfileReportService {

	@Autowired
	private ProfileReportRepository profileReportRepository;

	@Autowired
	private IProfileService profileService;

	@Autowired
	private IReportTypeService reportTypeService;

	@Override
    public ProfileReport create(Long profileId, ProfileReportCreateDTO request) {
		try {
			ProfileReport profileReport = new ProfileReport();
			profileReport.setOwnerProfile(profileService.findById(request.getOwnerProfileId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getMessage())))));
			profileReport
					.setProfile(profileService.findById(profileId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			profileReport.setReportType(reportTypeService.findById(request.getReportTypeId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage())))));
			profileReport.setDetail(request.getDetail());

			return profileReportRepository.save(profileReport);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileReportService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}

	}

}
