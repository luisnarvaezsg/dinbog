package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.FieldDTO;

public interface IFieldService {

	/**
	 * @param fieldId
	 * @return
	 */
	MutableList<FieldDTO> readAll(Long fieldId);

}