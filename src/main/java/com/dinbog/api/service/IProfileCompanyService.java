package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileRequestDTO;
import com.dinbog.api.models.entity.ProfileCompany;

public interface IProfileCompanyService {

	ProfileCompany update(ProfileRequestDTO request, String language);

	ProfileCompany createCompanyProfile(ProfileRequestDTO request, String language);
	
	ProfileCompany readById(Long id, String language);

}