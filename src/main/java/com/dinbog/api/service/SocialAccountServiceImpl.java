package com.dinbog.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.SocialAccount;
import com.dinbog.api.repository.SocialAccountRepository;


@Transactional
@Service
public class SocialAccountServiceImpl implements ISocialAccountService  {

	@Autowired
	private SocialAccountRepository socialAccountRepository;
	
	@Override
	public Optional<SocialAccount> findById(Long id) {
		
		return socialAccountRepository.findById(id);
	}
	
	@Override
	public SocialAccount create(SocialAccount socialAccount) {
		
		return socialAccountRepository.save(socialAccount);
	}

}
