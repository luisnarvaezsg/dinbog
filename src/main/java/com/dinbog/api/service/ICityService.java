package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.CityDTO;
import com.dinbog.api.dto.CityRequestDTO;
import com.dinbog.api.models.entity.City;

public interface ICityService {

	/**
	 * @param countryId
	 * @param request
	 * @return
	 */
	City create(Long countryId, CityRequestDTO request);

	/**
	 * @param id
	 * @param countryId
	 * @param request
	 * @return
	 */
	City update(Long id, Long countryId, CityRequestDTO request);

	/**
	 * @param countryId
	 * @param id
	 */
	void deleteByCountry(Long countryId, Long id);

	/**
	 * @param countryId
	 * @return
	 */
	MutableList<CityDTO> getCities(Long countryId, String langCode);
	
	Optional<City> findById(Long id, String langCode);

}