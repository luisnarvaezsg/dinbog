package com.dinbog.api.service;


import java.util.List;

import com.dinbog.api.dto.PostLikeResponseDTO;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostLike;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;

public interface IPostLikeService {

	/**
	 *
	 * @param postId
	 * @param profileId
	 * @return
	 */
	PostLikeResponseDTO create(Long postId, Long profileId, String lang);

	Boolean findByPostAndProfile(Post post, Profile profile);
	
	List<PostLike> findByPost(Post post);
	
	void deleteByPost(Post post);

	Boolean deleteLike(Long postId, User user, String lang);

}