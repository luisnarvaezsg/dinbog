package com.dinbog.api.service;



import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRecent;

public interface IProfileRecentService {
  
    public Optional<List<ProfileRecent>> findByOwnerProfileAndCreatedAfter(Profile profile,Date dateTagged);  

}
