package com.dinbog.api.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.UserGroupDTO;
import com.dinbog.api.dto.UserGroupRequestDTO;
import com.dinbog.api.repository.UserGroupRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.UserGroup;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.UserGroupMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserGroupServiceImpl implements IUserGroupService {

	@Autowired
	private UserGroupRepository userGroupRepository;

	@Autowired
	private IGroupService groupService;

	@Autowired
	private IUserService userService;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public MutableList<UserGroupDTO> create(Long id, UserGroupRequestDTO request) {
		try {

			MutableList<UserGroup> userGroups = Lists.mutable.of();
			MutableList<UserGroupDTO> userGroupDTOS = Lists.mutable.of();
			request.getUsers().forEach(i -> {
				UserGroup userGroup = new UserGroup();
				userGroup
						.setGroup(groupService.findById(id)
								.orElseThrow(() -> new InternalErrorException(
										Lists.mutable.of(new ApiError(ApiErrorEnum.GROUP_NOT_FOUND.getCode(),
												ApiErrorEnum.GROUP_NOT_FOUND.getMessage())))));

				userGroup
						.setUser(userService.findById(i)
								.orElseThrow(() -> new InternalErrorException(
										Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),
												ApiErrorEnum.USER_NOT_FOUND.getMessage())))));

				userGroups.add(userGroup);
			});

			userGroupRepository.saveAll(userGroups).forEach(i -> userGroupDTOS.add(UserGroupMapper.mapUserGroup(i)));
			return userGroupDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param userId
	 */
	@Override
	@Transactional
	public void deleteGroupByUser(Long userId) {
		try {
			userGroupRepository.deleteByUser(userId);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.deleteGroupByUser]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public MutableList<UserGroupDTO> readByGroupId(Long id) {
		try {
			MutableList<UserGroupDTO> userGroupDTOS = Lists.mutable.of();
			List<UserGroup> userGroups = userGroupRepository.findAll(new Specification<UserGroup>() {
				private static final long serialVersionUID = 8271630397268007029L;

				@Override
				public Predicate toPredicate(Root<UserGroup> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					MutableList<Predicate> predicates = Lists.mutable.empty();
					if (!Util.isEmpty(id)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("group"), id)));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			});

			if (userGroups.isEmpty()) {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getCode(),
								ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getMessage())));
			}
			userGroups.forEach(i -> userGroupDTOS.add(UserGroupMapper.mapUserGroup(i)));
			return userGroupDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.readByGroupId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	@Transactional
	public void deleteByGroupId(Long id) {
		try {
			userGroupRepository.deleteByGroup(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.deleteByGroupId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	@Transactional
	public void deleteByUser(Long id) {
		
		userGroupRepository.deleteByUser(id);
		
	}

	@Override
	@Transactional
	public void save(UserGroup userGroup) {
		
		userGroupRepository.save(userGroup);
		
	}

}
