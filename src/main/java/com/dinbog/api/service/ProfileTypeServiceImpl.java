package com.dinbog.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileType;
import com.dinbog.api.repository.ProfileTypeRepository;


@Transactional
@Service
public class ProfileTypeServiceImpl implements IProfileTypeService {

	@Autowired
	private ProfileTypeRepository profileTypeRepository;
	
	@Override
	public Optional<ProfileType> findById(Long id) {
		
		return profileTypeRepository.findById(id);
	}

}
