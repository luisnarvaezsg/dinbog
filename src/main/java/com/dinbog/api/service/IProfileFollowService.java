package com.dinbog.api.service;



import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileFollow;

public interface IProfileFollowService {
	
    ProfileFollow getProfileFollowById(Long id);

    void delete(Profile profileOwner, Profile profileFollow, String lang);

    ProfileFollow create(Profile profileOwner, Profile profileFollow, String lang);
    
    List<ProfileFollow> findAll();
    
    Optional<List<ProfileFollow>> findFollows(Profile ownerProfile, Pageable paging);

    Optional<List<ProfileFollow>> findFollowers(Profile profile, Pageable paging);
    
    Optional<ProfileFollow> findByOwnerProfileAndProfile(Profile profileOwner, Profile profile);
}
