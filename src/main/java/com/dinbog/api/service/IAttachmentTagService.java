package com.dinbog.api.service;

import com.dinbog.api.dto.TagDTO;
import com.dinbog.api.models.entity.Attachment;

import java.util.List;

public interface IAttachmentTagService {

    void create(List<TagDTO> tags, Attachment attachment, String lang);

    void deleteByAttachment(Attachment attachment);

}
