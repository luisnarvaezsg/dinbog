package com.dinbog.api.service;



import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.CommentsPostDTO;
import com.dinbog.api.models.entity.CommentsPost;
import com.dinbog.api.models.entity.CommentsPostLike;
import com.dinbog.api.models.entity.User;

public interface ICommentService {

	CommentsPost create(Long id, String comentario, User user, String lang);

	CommentsPostLike commentLike(Long id, User user, String lang);

	void commentDisLike(Long id, String lang);

	List<CommentsPostLike> findByCommentsPost(CommentsPost comment);
	
	void save(CommentsPost comment);

	void deleteComments(CommentsPost comment);

	MutableList<CommentsPostDTO> commentsByPost(Long id, HttpServletRequest request, String lang, int pageNumber,
			int pageSize);


}
