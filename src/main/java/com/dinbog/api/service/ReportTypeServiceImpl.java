package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.ReportTypeDTO;
import com.dinbog.api.dto.ReportTypeRequestDTO;
import com.dinbog.api.dto.ReportTypeResponseDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.ReportType;
import com.dinbog.api.repository.ReportTypeRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ReportTypeMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("ReportTypeService")
public class ReportTypeServiceImpl implements IReportTypeService {

	@Autowired
	private ReportTypeRepository reportTypeRepository;

	/**
	 * Metodo para crear un report type
	 * 
	 * @param request
	 * @return
	 */
	@Override
	public ReportTypeResponseDTO create(ReportTypeRequestDTO request) {

		try {
			return ReportTypeMapper
					.mapReportTypeResponse(reportTypeRepository.save(ReportTypeMapper.mapRequestToEntity(request)));

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Metodo para buscar todos los report type e indicar su nombre de acuerdo al
	 * idioma indicado
	 * 
	 * @param langCode
	 * @return
	 */
	@Override
	public MutableList<ReportTypeDTO> read(String langCode) {

		try {
			MutableList<ReportTypeDTO> reportTypes = Lists.mutable.empty();
			reportTypeRepository.findAll().forEach(i -> reportTypes.add(ReportTypeMapper.mapReportType(i)));

			if (reportTypes.isEmpty()) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_TYPE_NO_RESULTS.getCode(),
								ApiErrorEnum.ATTACHMENT_TYPE_NO_RESULTS.getMessage())));
			} else {
				return reportTypes;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeService.read]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Método para actualizar un report type
	 * 
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public ReportTypeResponseDTO update(Long id, ReportTypeRequestDTO request) {

		try {
			// VALIDA QUE REPORT TYPE EXISTA
			reportTypeRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage()))));

			// ACTUALIZA REPORT TYPE CON DATA RECIBIDA
			return ReportTypeMapper.mapReportTypeResponse(
					reportTypeRepository.save(ReportTypeMapper.mapRequestToEntity(request).setId(id)));

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Borra un report type por su id
	 *
	 * @param id
	 * @return
	 */
	@Override
	public void delete(Long id) {

		try {
			// VALIDAR si existe
			reportTypeRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage()))));

			// ELIMINA REPORT TYPE
			reportTypeRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ReportTypeService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<ReportType> findById(Long reportTypeId) {
		
		return reportTypeRepository.findById(reportTypeId);
	}
}
