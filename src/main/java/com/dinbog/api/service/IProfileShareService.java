package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileShareCreateDTO;
import com.dinbog.api.models.entity.ProfileShare;

public interface IProfileShareService {
    ProfileShare create(Long profileId, ProfileShareCreateDTO request);
}
