package com.dinbog.api.service;

import com.dinbog.api.models.entity.CommentsAlbum;
import com.dinbog.api.models.entity.CommentsAlbumMention;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.repository.CommentsAlbumMentionRepository;
import com.dinbog.api.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentAlbumMentionServiceImpl implements ICommentsAlbumMentionService{

    private final CommentsAlbumMentionRepository commentsAlbumMentionRepository;
    private final ProfileRepository profileRepository;

    @Autowired
    public CommentAlbumMentionServiceImpl(CommentsAlbumMentionRepository commentsAlbumMentionRepository, ProfileRepository profileRepository) {
        this.commentsAlbumMentionRepository = commentsAlbumMentionRepository;
        this.profileRepository = profileRepository;
    }

    @Override
    public void create(CommentsAlbum commentsAlbum, List<Long> mentions) {
        mentions.forEach( mention ->{
            Optional<Profile> profile = profileRepository.findById(mention);
            if( profile.isPresent() ) {
                CommentsAlbumMention commentsAlbumMention =  new CommentsAlbumMention(commentsAlbum,profile.get());
                commentsAlbumMentionRepository.save(commentsAlbumMention);
            }
        });
    }

    @Override
    public List<CommentsAlbumMention> findAllMentionsInComment(CommentsAlbum commentsAlbum) {
        return commentsAlbumMentionRepository.findAllByCommentsAlbum(commentsAlbum);
    }

    @Override
    public void delete(CommentsAlbumMention commentsAlbumMention) {
        commentsAlbumMentionRepository.delete(commentsAlbumMention);
    }
}
