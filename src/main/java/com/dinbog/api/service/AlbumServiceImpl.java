package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.aws.AWSBuckets;
import com.dinbog.api.dto.AlbumEditRequestDTO;
import com.dinbog.api.models.entity.*;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.AlbumDTO;
import com.dinbog.api.dto.AlbumRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.repository.AlbumRepository;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AlbumMapper;


import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
public class AlbumServiceImpl implements IAlbumService {

	@Autowired
	private AlbumRepository albumRepository;

	@Autowired
	private IAlbumTypeService albumTypeService;

	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private IAttachmentService iAttachmentService;

	@Autowired
	private IAlbumAttachmentService albumAttachmentService;

	@Autowired
	private IAlbumCommentService albumCommentService;

	@Autowired
	private IAlbumLikeService albumLikeService;


	/**
	 * @param request
	 * @return
	 */
	@Override
	public Album create(AlbumRequestDTO request) {

		try {
			Album album = AlbumMapper.requestToEntity(request);
			album.setProfile(
					profileService.findById(request.getProfileId())
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			album.setAlbumType(albumTypeService.findById(request.getAlbumTypeId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.ALBUM_TYPE_NOT_FOUND.getMessage())))));
			return (albumRepository.save(album));
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public Album update(Long id, AlbumRequestDTO request) {

		try {
			Boolean modificado = false;
			Album album = albumRepository.findById(id).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(
					new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(), ApiErrorEnum.ALBUM_NOT_FOUND.getMessage()))));

			// NAME
			if (!Util.isEmpty(request.getName()) && request.getName().compareTo(album.getName()) != 0) {
				modificado = true;
				album.setName(request.getName());
			}

			// DESCRIPTION
			if (!Util.isEmpty(request.getDescription())
					&& request.getDescription().compareTo(album.getDescription()) != 0) {
				modificado = true;
				album.setDescription(request.getDescription());
			}

			// IS PRIVATE
			if (!Util.isEmpty(request.getIsPrivate()) && request.getIsPrivate().compareTo(album.getIsPrivate()) != 0) {
				modificado = true;
				album.setIsPrivate(request.getIsPrivate());
			}

			// PROFILE
			if (!Util.isEmpty(request.getProfileId())
					&& request.getProfileId().compareTo(album.getProfile().getId()) != 0) {
				modificado = true;
				album.setProfile(profileService.findById(request.getProfileId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
										ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			}

			// ALBUM TYPE
			if (!Util.isEmpty(request.getAlbumTypeId())
					&& request.getAlbumTypeId().compareTo(album.getAlbumType().getId()) != 0) {
				modificado = true;
				album.setAlbumType(albumTypeService.findById(request.getAlbumTypeId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_TYPE_NOT_FOUND.getCode(),
										ApiErrorEnum.ALBUM_TYPE_NOT_FOUND.getMessage())))));
			}

			if (Boolean.TRUE.equals(modificado)) {
				// ACTUALIZA ALBUM
				return (albumRepository.save(album));
			} else {
				// SI NO HAY CAMPOS QUE MODIFICAR ARROJA UN ERROR
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.SOCIAL_ACCOUNT_NO_PATCH.getCode(),
								ApiErrorEnum.SOCIAL_ACCOUNT_NO_PATCH.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumService.edit]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param albumTypeId
	 * @param searchName
	 * @return
	 */
	@Override
	public PaginateResponse<AlbumDTO> readAll(Integer pageNumber, Integer pageSize, Long albumTypeId,
	                                          String searchName, String lang) {
		try {
			Page<Album> page = albumRepository.findAll(new Specification<Album>() {
				private static final long serialVersionUID = -4639637551471002240L;

				@Override
				public Predicate toPredicate(Root<Album> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					MutableList<Predicate> predicates = Lists.mutable.empty();
					if (!Util.isEmpty(albumTypeId)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("albumType"), albumTypeId)));
					}

					if (!Util.isEmpty(searchName)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
								"%" + searchName.toLowerCase() + "%")));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id"))));

			if (page.getTotalElements() > 0L) {
				MutableList<AlbumDTO> albumDTOS = Lists.mutable.of();
				page.getContent().forEach(i -> albumDTOS.add(AlbumMapper.mapAlbum(i)));
				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), albumDTOS);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.ALBUM_NO_RESULTS.getCode(), ApiErrorEnum.ALBUM_NO_RESULTS.getMessage())));
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumService.readById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public Album readById(Long id, String lang) {
		
		try {
			Optional<Album> album = albumRepository.findById(id);
			// Sustituye a album.isEmpty()
			if (!album.isPresent()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.ALBUM_NOT_FOUND.getCode(), ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())));
			}
			return (album.get());
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumService.readById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {
		try {
			albumRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	

	@Override
	public MutableList<AlbumDTO> findByProfile(Profile profile, AlbumType albumType, String lang) {
		
		MutableList<AlbumDTO> albumDTOs = Lists.mutable.empty();
			
		List<Album> albums = albumRepository.findByProfileAndAlbumType(profile, albumType);
			
		if (albums.isEmpty()) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ALBUM_LIST_EMPTY, lang)));
		}else {
			albums.forEach(a -> albumDTOs.add(AlbumMapper.mapAlbum(a)));
		}
	
		return albumDTOs;
		
	}

	@Override
	public void uploadFileCover(MultipartFile[] files, MultipartFile fileCover, Album album, String lang) {
		for( MultipartFile file: files) {
			Attachment attachment = iAttachmentService.create(file,fileCover,null, lang, AWSBuckets.POSTS.getBucket());
			if (attachment != null) {
				albumAttachmentService.create(album, attachment);
			}else {
				throw new ResourceNotFoundException(
						Lists.mutable.of(
								errorService.getApiError(ApiErrorEnum.ATTACHMENT_TYPE_NO_RESULTS, lang)
						)
				);
			}
		}
	}

	@Override
	public Optional<Album> findById(Long id) {
		return albumRepository.findById(id);
	}

	@Override
	@Transactional
	public void delete(Album album) {
		albumCommentService.deleteByAlbum(album);
		albumLikeService.delete(album);
		albumAttachmentService.delete(album);
		albumRepository.delete(album);
	}

    @Override
    public MutableList<ApiError> validateEditRequest(AlbumEditRequestDTO albumEditRequestDTO,String lang) {
		MutableList<ApiError> errors = Lists.mutable.empty();
		if(albumEditRequestDTO.getName().isEmpty() || albumEditRequestDTO.getName() == null )
			errors.add( errorService.getApiError(ApiErrorEnum.EMPTY_NAME, lang));
		if(albumEditRequestDTO.getDescription().isEmpty() || albumEditRequestDTO.getDescription() == null )
			errors.add( errorService.getApiError(ApiErrorEnum.EMPTY_DESCRIPTION, lang));
		if(albumEditRequestDTO.getIsPrivate() == null)
			errors.add( errorService.getApiError(ApiErrorEnum.EMPTY_IS_PRIVATE, lang));

        return errors;
    }

	@Override
	public void updateAlbum(Album album, AlbumEditRequestDTO requestDTO) {
		album.setName(requestDTO.getName());
		album.setDescription( requestDTO.getDescription());
		album.setIsPrivate( requestDTO.getIsPrivate());

		albumRepository.save(album);
	}

	@Override
	public Album save(Album album) {
		
		return albumRepository.save(album);
	}
}
