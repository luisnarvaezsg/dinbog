package com.dinbog.api.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.CountryDTO;
import com.dinbog.api.dto.CountryRequestDTO;
import com.dinbog.api.dto.SortCountryByName;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Country;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.CountryRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.CountryMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("CountryService")
public class CountryServiceImpl implements ICountryService {

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private ILanguageService languageService;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public Country create(CountryRequestDTO request) {
		try {
			Country country = new Country();
			country.setName(request.getName());
			country.setIsoCode(request.getIsoCode());
			country.setIsOnu(request.getIsOnu());
			country.setWearMeasurement(request.getWearMeasurement());
			country.setUnitMeasurement(request.getUnitMeasurement());
			country.setLanguageId(request.getLanguageId());
			return countryRepository.save(country);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param languageId
	 * @return
	 * @throws InternalErrorException
	 */
	@Override
	public MutableList<CountryDTO> readAll(String langCode) {
		
		langCode = languageService.validLanguage(langCode);

		List<Country> countryList = countryRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));

		if (countryList.isEmpty()) {
		
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRIES_NO_RESULTS, langCode)));
		
		} else {
			
			MutableList<CountryDTO> countryDTOS = Lists.mutable.empty();
			
			for(Country i : countryList) {
			
				Traslation traslation =  traslationService.findByKeyAndIsoCode(i.getKey(), langCode);
				if (traslation == null) {
					
					i.setName(i.getKey());
				
				}else {
					i.setName(traslation.getValue());
				}
				countryDTOS.add(CountryMapper.mapCounty(i));
					
			}
			Collections.sort(countryDTOS, new SortCountryByName());

		    return countryDTOS;
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public Country update(Long id, CountryRequestDTO request) {
		try {
			Country country = countryRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(org.eclipse.collections.api.factory.Lists.mutable
							.of(new ApiError(ApiErrorEnum.COUNTRY_NO_FOUND.getCode(),
									ApiErrorEnum.COUNTRY_NO_FOUND.getMessage()))));

			if (!Util.isEmpty(request.getName())) {
				country.setName(request.getName());
			}

			if (!Util.isEmpty(request.getIsoCode())) {
				country.setIsoCode(request.getIsoCode());
			}

			if (!Util.isEmpty(request.getIsOnu())) {
				country.setIsOnu(request.getIsOnu());
			}

			if (!Util.isEmpty(request.getUnitMeasurement())) {
				country.setWearMeasurement(request.getWearMeasurement());
			}

			if (!Util.isEmpty(request.getWearMeasurement())) {
				country.setUnitMeasurement(request.getUnitMeasurement());
			}

			if (!Util.isEmpty(request.getLanguageId())) {
				country.setLanguageId(request.getLanguageId());
			}

			return countryRepository.save(country);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}

	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {
		try {
			countryRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CountryService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public Optional<Country> findById(Long id, String langCode) {
		
		Optional<Country> country = Optional.empty();
		
		langCode = languageService.validLanguage(langCode);
		
		try {
			
			country = countryRepository.findById(id);
			if(country.isPresent()) {
				Traslation traslation =  traslationService.findByKeyAndIsoCode(country.get().getKey(), langCode);
				if(traslation == null) {
					country.get().setName(country.get().getKey());	
				}else {
					country.get().setName(traslation.getValue());
				}
			}
			
		} catch (Exception e) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NO_FOUND, langCode)));
		}
		
		return country;
	}
}
