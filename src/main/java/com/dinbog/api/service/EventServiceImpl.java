package com.dinbog.api.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dinbog.api.commons.EventStatusEnum;
import com.dinbog.api.dto.EventDTO;
import com.dinbog.api.dto.EventRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Event;
import com.dinbog.api.models.entity.EventProfileCategoryType;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.EventRepository;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.EventMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EventServiceImpl implements IEventService {
	
	@Autowired
	private EventRepository eventRepository;
	
	@Autowired
	private IEventTypeService eventTypeService;
	
	@Autowired
	private IEventStatusService eventStatusService;
	
	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private ICountryService countryService;
	
	@Autowired
	private ICityService cityService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private IProfileCategoryTypeService categoryTypeService;
	
	
	
	/**
	 * @param request
	 * @return
	 */
	@Override
	public Event create(EventRequestDTO request, User user, String lang) {
		
		try {
			
			Event event = EventMapper.requestToEntity(new Event(), request);
			
			Profile profileOwner = profileService.getProfile(user, lang);
			event.setOwner(profileOwner);
			event.setProfileType(profileOwner.getProfileType());
			
			event.setEventStatus(eventStatusService.findById(EventStatusEnum.DRAFT.getEventStatusId(), lang).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EVENT_STATUS_NOT_FOUND, lang)))));	
			
			event.setEventType(eventTypeService.findById(request.getEventTypeId(), lang).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EVENT_TYPE_NOT_FOUND, lang)))));	
			
			event.setCountry(countryService.findById(request.getCountryId(), lang).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, lang)))));
			
			if(request.getCityId() != null) {
			
				event.setCity(cityService.findById(request.getCityId(), lang).orElseThrow(
						() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, lang)))));	
				
			}
			
			//SAVE CATEGORIES
			for (String catId : request.getCategories()) {
				
				EventProfileCategoryType profileCategory = new EventProfileCategoryType();
				profileCategory.setEvent(event);
				profileCategory.setProfileCategoryType(categoryTypeService.findById(Long.valueOf(catId))
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND, lang)))));
				
				event.addEventProfileCategory(profileCategory);
			}
			//SAVE ATTACHMENT COVER
			//event.setCoverAttachment(attachmentService.create(request.getCoverAttachment(), lang));
			event.setCoverAttachment(request.getCoverAttachment());
			
			eventRepository.save(event);
			
			for (EventProfileCategoryType eventProfileCategoryType : event.getEventProfileCategoryTypes()) {
				categoryTypeService.traslateProfileCategoryType(eventProfileCategoryType.getProfileCategoryType(), lang);
			}
			
			return event;
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [EventService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param search
	 * @return
	 */
	@Override
	public PaginateResponse<EventDTO> readAll(Integer pageNumber, Integer pageSize, String search, String lang) {
		try {
			Page<Event> page = eventRepository.findAll(
					new Specification<Event>() {
						private static final long serialVersionUID = 7779202731322693208L;
						
						@Override
						public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
							MutableList<Predicate> predicates = Lists.mutable.empty();
							if (!Util.isEmpty(search)) {
								// Busqueda sensitiva de nombre de eventos
								predicates.add(criteriaBuilder.and(
										criteriaBuilder.like(criteriaBuilder.lower(root.get("nameEvent")), "%" + search.toLowerCase() + "%"))
								);
							}
							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					},
					PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id"))));
			
			if (page.getTotalElements() > 0L) {
				MutableList<EventDTO> eventDTOS = Lists.mutable.of();
				
				page.getContent().forEach(i -> eventDTOS.add(EventMapper.mapEvent(i)));
				
				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), eventDTOS);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EVENT_NO_RESULTS.getCode(), ApiErrorEnum.EVENT_NO_RESULTS.getMessage())));
			}
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [EventService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	/**
	 * @param id
	 * @return
	 */
	@Override
	public Event readById(Long id, String lang) {
		try {
			
			return eventRepository.findById(id).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.EVENT_NOT_FOUND.getCode(),
									ApiErrorEnum.EVENT_NOT_FOUND.getMessage()))));
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [EventService.edit]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public Event edit(Long id, EventRequestDTO request) {
		try {
			Event event = EventMapper.requestToEntity(eventRepository.findById(id).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.EVENT_NOT_FOUND.getCode(),
									ApiErrorEnum.EVENT_NOT_FOUND.getMessage())))
			), request);
			
			event.setEventStatus(eventStatusService.findById(request.getEventStatusId(), "EN").orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.EVENT_STATUS_NOT_FOUND.getCode(),
									ApiErrorEnum.EVENT_STATUS_NOT_FOUND.getMessage())))
			));
			
			event.setEventType(eventTypeService.findById(request.getEventTypeId(), "EN").orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.EVENT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.EVENT_TYPE_NOT_FOUND.getMessage())))
			));
			
			//event.setProfileType(profileTypeService.findById(request.getProfileTypeId()).orElseThrow(
			//		() -> new InvalidRequestException(
			//				Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getCode(),
			//						ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getMessage())))
			//));
			
			event.setCountry(countryService.findById(request.getCountryId(),"EN").orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.COUNTRY_NOT_FOUND.getCode(),
									ApiErrorEnum.COUNTRY_NOT_FOUND.getMessage())))
			));
			
			event.setCity(cityService.findById(request.getCityId(), "EN").orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.CITY_NOT_FOUND.getCode(),
									ApiErrorEnum.CITY_NOT_FOUND.getMessage())))
			));
			
			//event.setOwner(profileService.findById(request.getOwnerId()).orElseThrow(
			//		() -> new InvalidRequestException(
			//				Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
			//						ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))
			//));
			return eventRepository.save(event);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [EventService.edit]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {
		try {
			eventRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [EventService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public List<Event> findByOwnerId(Long id) {
		
		return eventRepository.findByOwnerId(id);
	}
}
