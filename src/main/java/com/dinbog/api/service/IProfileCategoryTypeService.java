package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.ProfileCategoryTypeDTO;
import com.dinbog.api.dto.ProfileCategoryTypeRequestDTO;
import com.dinbog.api.models.entity.ProfileCategoryType;

public interface IProfileCategoryTypeService {

	ProfileCategoryType create(ProfileCategoryTypeRequestDTO request);

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	ProfileCategoryType update(Long id, ProfileCategoryTypeRequestDTO request);

	
	/**
	 * @param id
	 */
	void deleteById(Long id);
	
	
	MutableList<ProfileCategoryTypeDTO> findByLevelWithProfileType(Long level, Long profileType, String lang, boolean childs);
	
	Optional<ProfileCategoryType> findById(Long id);
	
	void traslateProfileCategoryType(ProfileCategoryType profileCategory, String lang);

}