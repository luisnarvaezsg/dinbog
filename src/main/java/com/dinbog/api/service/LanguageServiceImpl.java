package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.dto.LanguageDTO;
import com.dinbog.api.dto.LanguageRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Language;
import com.dinbog.api.repository.LanguageRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.LanguageMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LanguageServiceImpl implements ILanguageService {

	@Autowired
	private LanguageRepository languageRepository;

	/**
	 * Metodo para crear un language
	 * 
	 * @param request
	 * @return
	 */
	@Override
	public Language create(LanguageRequestDTO request) {

		try {
			return languageRepository.save(LanguageMapper.mapRequestToEntity(request));

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@Override
	public MutableList<LanguageDTO> read() {

		MutableList<LanguageDTO> languages = Lists.mutable.empty();
		languageRepository.findAll().forEach(i -> languages.add(LanguageMapper.mapLanguage(i)));

		if (languages.isEmpty()) {
			throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
					ApiErrorEnum.LANGUAGES_NO_RESULTS.getCode(), ApiErrorEnum.LANGUAGES_NO_RESULTS.getMessage())));
		} else {
			return languages;
		}
	}

	/**
	 * Método para actualizar un language
	 * 
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public Language update(Long id, LanguageRequestDTO request) {

		try {
			// VALIDA QUE LANGUAGE EXISTA
			languageRepository.findById(id).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(new ApiError(
					ApiErrorEnum.LANGUAGE_NOT_FOUND.getCode(), ApiErrorEnum.LANGUAGE_NOT_FOUND.getMessage()))));

			// ACTUALIZA LANGUAGE CON DATA RECIBIDA
			return languageRepository.save(LanguageMapper.mapRequestToEntity(request).setId(id));

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Borra un language por su id
	 *
	 * @param id
	 * @return
	 */
	@Override
	public void delete(Long id) {

		try {
			// VALIDAR si existe
			languageRepository.findById(id).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(new ApiError(
					ApiErrorEnum.LANGUAGE_NOT_FOUND.getCode(), ApiErrorEnum.LANGUAGE_NOT_FOUND.getMessage()))));

			// ELIMINA LANGUAGE
			languageRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LanguageService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public String validLanguage(String isoCode) {

		boolean exists = false;
		
		if (!isoCode.isEmpty()) {
		
			exists = languageRepository.existsByIsoCodeLanguage(isoCode);
		}

		if(!exists) {
			isoCode = CommonsConstant.LANGUAGE_BY_DEFAULT;
		}
		
		return isoCode;
	}
	
	
}
