package com.dinbog.api.service;

import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.TraslationDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.TraslationRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.TraslationMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("TraslationService")
public class TraslationServiceImpl implements ITraslationService {
	
	@Autowired
	private TraslationRepository traslationRepository;
	
	@Override
	public Traslation findByKeyAndIsoCode(String key, String isoCode){
		return traslationRepository.findByKeyAndIsoCode(key, isoCode);
	}
	
	
	@Override
	public MutableList<TraslationDTO> findByIsoCode(String isoCode) {

		try {
			List<Traslation> translations = traslationRepository.findByIsoCode(isoCode);
			
			if (translations.isEmpty()) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_CATEGORY_NO_RESULTS.getCode(),
								ApiErrorEnum.PROFILE_CATEGORY_NO_RESULTS.getMessage())));
			} else {
				MutableList<TraslationDTO> translationDTOS = Lists.mutable.empty();
				translations
						.forEach(i -> translationDTOS.add(TraslationMapper.mapTranslation(i)));
				
				return translationDTOS;
			}
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileCategoryTypeService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}

	}

}
