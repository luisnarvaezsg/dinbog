package com.dinbog.api.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import com.dinbog.api.dto.MensajeDTO;
import com.dinbog.api.models.entity.Message;
import com.dinbog.api.models.entity.Profile;

public interface IMessageService {
	
	
	Page<Message> findAllByProfileSrc(@Param("profile") Profile profileSrc, Pageable pageable);
	
	Page<Message> findAllByProfileDest(@Param("profile") Profile profileDest, Pageable pageable);
	
	void deleteMessage(Message message);

	Message createMessage(HttpServletRequest request, Long idDest, MensajeDTO mensajeDTO, String lang);

}
