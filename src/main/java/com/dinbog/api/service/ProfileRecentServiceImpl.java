package com.dinbog.api.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.dinbog.api.repository.ProfileRecentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRecent;

@Transactional
@Service
public class ProfileRecentServiceImpl implements IProfileRecentService {

	@Autowired
	private ProfileRecentRepository profileRecentRepository;

	

	@Override
	public Optional<List<ProfileRecent>> findByOwnerProfileAndCreatedAfter(Profile profile,Date dateTagged){
		return profileRecentRepository.findByOwnerProfileAndCreatedAfter(profile, dateTagged);
	}

}
