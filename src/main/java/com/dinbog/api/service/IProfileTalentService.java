package com.dinbog.api.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dinbog.api.dto.ProfileFollowBasicDTO;
import com.dinbog.api.dto.ProfileRequestDTO;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileTalent;

public interface IProfileTalentService {

	ProfileTalent update(ProfileRequestDTO request, String language);

	ProfileTalent createTalentProfile(ProfileRequestDTO request, String language);
	
	ProfileTalent readById(Long id, String lang);

	Optional<ProfileTalent> findByProfile(Profile profile);

	Page<Profile> findByFullNameCompanyContainingIgnoreCase(Profile profile, Pageable pageable, String name);

	Page<Profile> findByFullNameContainingIgnoreCase(Profile profile, Pageable pageable, String name);

	ProfileFollowBasicDTO mapProfileFollow(Profile profileOwner, Profile p, String lang);
}