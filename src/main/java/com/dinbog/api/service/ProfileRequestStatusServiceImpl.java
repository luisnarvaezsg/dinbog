package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.repository.ProfileRequestStatusRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileRequestStatus;
/**
 * @author Luis
 *
 */
@Transactional
@Service
public class ProfileRequestStatusServiceImpl implements IProfileRequestStatusService {

	@Autowired
	private ProfileRequestStatusRepository profileRequestStatusRepository;

	@Override
	public Optional<ProfileRequestStatus> findById(Long id) {
		
		return profileRequestStatusRepository.findById(id);
	}

}
