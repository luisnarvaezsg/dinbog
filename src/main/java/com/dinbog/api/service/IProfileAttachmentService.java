package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.ProfileAttachmentDTO;
import com.dinbog.api.dto.ProfileAttachmentRequestDTO;
import com.dinbog.api.models.entity.ProfileAttachment;

public interface IProfileAttachmentService {

	/**
	 * @param request
	 * @return
	 */
	ProfileAttachment create(Long profileId, ProfileAttachmentRequestDTO request);

	/**
	 * @param profileId
	 * @param id
	 */
	void deleteByIdAndByProfileId(Long profileId, Long attachmentId);

	/**
	 * @param profileId
	 * @param id
	 */
	MutableList<ProfileAttachmentDTO> getAttachmentsByProfileId(Long profileId);
	
	public ProfileAttachment create(ProfileAttachment profileAttachment);

}