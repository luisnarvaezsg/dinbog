package com.dinbog.api.service;

import com.dinbog.api.repository.ProfileFilterValueRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.ProfileFilterValueDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.CategoryFilter;
import com.dinbog.api.models.entity.Filter;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCategoryType;
import com.dinbog.api.models.entity.ProfileFilterValue;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileFilterValueMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileFilterValueServiceImpl implements IProfileFilterValueService{

	@Autowired
	private ProfileFilterValueRepository filterValueRepository;
	
	@Autowired
	private ICategoryFilterService categoryFilterService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private IProfileCategoryService profileCategoryService;

	@Override
	public void saveFilters(Profile profile, List<ProfileFilterValueDTO> filters, String lang) {
		
		try {
			
			for (ProfileFilterValueDTO profileFilterValueDTO : filters) {
				
				
				//Validate Category Filter:
				CategoryFilter categoryFilter = categoryFilterService.findByProfileCategoryTypeIdAndFilter
												(profileFilterValueDTO.getCategoryId(), new Filter(profileFilterValueDTO.getFilterId()), lang)
												.orElseThrow(
														() -> new ResourceNotFoundException(
																Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CATEGORY_FILTER_NOT_FOUND, lang))));
				
				ProfileCategory profileCategory = profileCategoryService.findByProfileAndProfileCategoryType
												(profile, new ProfileCategoryType(profileFilterValueDTO.getCategoryId()))
												.orElseThrow(
														() -> new ResourceNotFoundException(
																Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND, lang))));

				
				Optional<ProfileFilterValue> oProfileFilterValue = filterValueRepository.findByCategoryFilterAndProfileCategory(categoryFilter, profileCategory);
				ProfileFilterValue profileFilterValue;
				
				if (oProfileFilterValue.isPresent()) {

					profileFilterValue =  oProfileFilterValue.get();

				} else {
					profileFilterValue = new ProfileFilterValue();
					profileFilterValue.setCategoryFilter(categoryFilter);
					profileFilterValue.setProfileCategory(profileCategory);
				}

				profileFilterValue.setDescription(profileFilterValueDTO.getDescription());
				profileFilterValue.setValue(profileFilterValueDTO.getValue());
				profileFilterValue.setStatus(1L);
				filterValueRepository.save(profileFilterValue);
			}
						
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public List<ProfileFilterValueDTO> getFilters() {
		
		try {
			
			List<ProfileFilterValue> filters = filterValueRepository.findAll();
			
			List<ProfileFilterValueDTO> filtersDTO = new ArrayList<>();
			filters.forEach(filter -> filtersDTO.add(ProfileFilterValueMapper.mapProfileFilterValue(filter)));
			return filtersDTO;
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserGroupService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

}
