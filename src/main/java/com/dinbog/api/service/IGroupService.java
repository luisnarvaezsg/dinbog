package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.GroupBasicDTO;
import com.dinbog.api.dto.GroupRequestDTO;
import com.dinbog.api.models.entity.Group;

public interface IGroupService {

	/**
	 * @param request
	 * @return
	 */
	GroupBasicDTO create(GroupRequestDTO request);

	/**
	 * @return
	 */
	MutableList<GroupBasicDTO> readAll();

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	GroupBasicDTO update(Long id, GroupRequestDTO request);

	/**
	 * @param id
	 */
	void deleteById(Long id);

	Optional<Group> findById(Long id);

	Optional<List<Group>> findByUser(Long id);

}