package com.dinbog.api.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.GroupMenuDTO;
import com.dinbog.api.dto.GroupMenuRequestDTO;
import com.dinbog.api.repository.GroupMenuRepository;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.GroupMenu;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.GroupMenuMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GroupMenuServiceImpl implements IGroupMenuService {

	@Autowired
	private GroupMenuRepository groupMenuRepository;

	@Autowired
	private IGroupService groupService;

	@Autowired
	private IMenuService menuService;

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public MutableList<GroupMenuDTO> create(Long id, GroupMenuRequestDTO request) {
		try {
			MutableList<GroupMenu> groupMenus = Lists.mutable.of();
			MutableList<GroupMenuDTO> groupMenuDTOS = Lists.mutable.of();
			request.getMenus().forEach(i -> {
				GroupMenu groupMenu = new GroupMenu();

				groupMenu.setGroup(groupService.findById(id)
						.orElseThrow(() -> new InternalErrorException(org.eclipse.collections.api.factory.Lists.mutable
								.of(new ApiError(ApiErrorEnum.GROUP_NOT_FOUND.getCode(),
										ApiErrorEnum.GROUP_NOT_FOUND.getMessage())))));

				groupMenu.setMenu(menuService.findById(i)
						.orElseThrow(() -> new InternalErrorException(org.eclipse.collections.api.factory.Lists.mutable
								.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),
										ApiErrorEnum.USER_NOT_FOUND.getMessage())))));
				groupMenus.add(groupMenu);
			});

			groupMenuRepository.saveAll(groupMenus).forEach(i -> groupMenuDTOS.add(GroupMenuMapper.mapGroupMenu(i)));
			return groupMenuDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupMenuService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public MutableList<GroupMenuDTO> readByGroupId(Long id) {
		try {
			MutableList<GroupMenuDTO> groupMenuDTOS = Lists.mutable.of();
			List<GroupMenu> groupMenus = groupMenuRepository.findAll(new Specification<GroupMenu>() {
				private static final long serialVersionUID = 5478652897497539269L;

				@Override
				public Predicate toPredicate(Root<GroupMenu> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					MutableList<Predicate> predicates = org.eclipse.collections.api.factory.Lists.mutable.empty();
					if (!Util.isEmpty(id)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("group"), id)));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			});

			if (groupMenus.isEmpty()) {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(org.eclipse.collections.api.factory.Lists.mutable
						.of(new ApiError(ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getCode(),
								ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getMessage())));
			}

			groupMenus.forEach(i -> groupMenuDTOS.add(GroupMenuMapper.mapGroupMenu(i)));

			return groupMenuDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupMenuService.readByGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	@Transactional
	public void deleteByGroup(Long id) {
		try {
			groupMenuRepository.deleteByGroup(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [GroupMenuService.deleteByGroup]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

}
