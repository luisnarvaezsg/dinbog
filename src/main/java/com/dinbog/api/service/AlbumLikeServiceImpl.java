package com.dinbog.api.service;

import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.Profile;
import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.AlbumLikeRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.AlbumLike;
import com.dinbog.api.repository.AlbumLikeRepository;
import com.dinbog.api.util.Util;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class AlbumLikeServiceImpl implements IAlbumLikeService {

	private final AlbumLikeRepository albumLikeRepository;

	private final IAlbumService albumService;

	private final IProfileService profileService;

	@Autowired
	public AlbumLikeServiceImpl(AlbumLikeRepository albumLikeRepository, IAlbumService albumService, IProfileService profileService) {
		this.albumLikeRepository = albumLikeRepository;
		this.albumService = albumService;
		this.profileService = profileService;
	}


	public AlbumLike create(AlbumLikeRequestDTO request, Long albumId) {
		try {
			AlbumLike albumLike = new AlbumLike();
			albumLike
					.setProfile(profileService.findById(request.getProfileId())
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			albumLike
					.setAlbum(albumService.findById(albumId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
											ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())))));

			return  albumLikeRepository.save(albumLike);
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumLikeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param albumId
	 * @param id
	 */
	@Override
	@Transactional
	public void deleteByAlbumIdAndById(Long albumId, Long id) {

		try {
			// BUSCA COMMENT A ELIMINAR
			AlbumLike albumLike = albumLikeRepository.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_LIKE_NOT_FOUND.getCode(),
									ApiErrorEnum.ALBUM_LIKE_NOT_FOUND.getMessage()))));

			if (albumId.equals(albumLike.getAlbum().getId())) {
				albumLikeRepository.deleteLikeByAlbumIdAndById(albumId, id);
			} else {
				// SI NO COINCIDE ID ALBUM LIKE INDICADO VS ID ALBUM OBTENIDO
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.ALBUM_LIKE_NOT_FOUND.getCode(), ApiErrorEnum.ALBUM_LIKE_NOT_FOUND.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumCommentService.deleteByAlbumIdAndById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}

	}

	@Override
	public Optional<AlbumLike> findLikeByProfileId(Album album, Profile profile) {
		return albumLikeRepository.findByAlbumAndProfile(album,profile);
	}

	@Override
	public void create(Album album, Profile profile) {
		AlbumLike albumLike = new AlbumLike();
		albumLike.setAlbum( album );
		albumLike.setProfile( profile );
		albumLikeRepository.save( albumLike );
		updateCountLikes(album);
	}

	@Override
	public void remove(AlbumLike albumLike) {
		Album album = albumLike.getAlbum();
		albumLikeRepository.delete(albumLike);
		updateCountLikes(album);
	}

	@Override
	public Integer getCountLikes(Long id) {
		return albumLikeRepository.findAlbumLikesByAlbum_Id(id).size();
	}

	@Override
	@Transactional
	public void delete(Album album) {
		List<AlbumLike> albumLikes = findAllByAlbum(album);
		if(!albumLikes.isEmpty())
			albumLikes.forEach( albumLikeRepository::delete );
	}

	@Override
	public List<AlbumLike> findAllByAlbum(Album album) {
		return albumLikeRepository.findAllByAlbum(album);
	}

	private void updateCountLikes(Album album){
		album.setCountLike( getCountLikes( album.getId()));
		albumService.save(album);
	}
}
