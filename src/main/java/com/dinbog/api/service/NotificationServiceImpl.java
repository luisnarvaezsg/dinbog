package com.dinbog.api.service;

import static com.dinbog.api.notification.Notification.READ;
import static com.dinbog.api.notification.Notification.SEND;
import static com.dinbog.api.notification.Notification.UNREAD;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.NotificationScrollDTO;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.firebase.FCMService;
import com.dinbog.api.firebase.PushNotificationRequest;
import com.dinbog.api.models.entity.Notification;
import com.dinbog.api.notification.NotificationScheduler;
import com.dinbog.api.repository.NotificationRepository;
import com.dinbog.api.security.jwt.JwtData;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.NotificationMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@Service
public class NotificationServiceImpl implements INotificationService {
	
	private static final String MESSAGE = "!Interrupted!!";

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private FCMService fcmService;

	@Value("#{${app.notifications.defaults}}")
	private Map<String, String> defaults;

	@Autowired
	private NotificationScheduler notificationScheduler;

	@Autowired
	private EntityManager entityManager;

	
	/**
	 * @param id
	 * @param isRead
	 */
	@Override
	public void patchIsRead(Long id, Integer isRead) {
		try {
			Notification notification = notificationRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.NOTIFICATION_NOT_FOUND.getCode(),
									ApiErrorEnum.NOTIFICATION_NOT_FOUND.getMessage()))));
			notification.setIsRead(isRead);
			notificationRepository.save(notification);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [NotificationService.patchIsRead]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	// este metodo es una prueba de concepto
	@Override
	public void sendPushNotificationPrueba() {

		List<Notification> notifications = notificationRepository.findAll(new Specification<Notification>() {
			private static final long serialVersionUID = -4113407287832021489L;

			@Override
			public Predicate toPredicate(Root<Notification> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				MutableList<Predicate> predicates = Lists.mutable.empty();
				predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isRead"), UNREAD.getCode())));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		});

		notifications.forEach(i -> {
			PushNotificationRequest request = new PushNotificationRequest();
			String message;
			String token;
			// PROFILE NOTIFICATION
			if (!Util.isEmpty(i.getProfileNotification())) {
				log.info("****Seteando message: ");
				message = i.getPostNotification().getOwnerProfile().getUrlName() + " make post_like in your profile";
				log.info("***** message: " + message);
				token = i.getProfileNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}
			// POST NOTIFICATION
			if (!Util.isEmpty(i.getPostNotification())) {
				log.info("****Seteando message: ");

				message = i.getPostNotification().getOwnerProfile().getUrlName() + " make post_like in your profile";

				log.info("***** message: " + message);
				token = i.getPostNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}
			// ALBUM NOTIFICATION
			if (!Util.isEmpty(i.getAlbumNotification())) {
				log.info("****Seteando message: ");

				message = i.getAlbumNotification().getOwnerProfile().getUrlName() + " make album_like in your profile";

				log.info("***** message: " + message);
				token = i.getAlbumNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}

			// USER NOTIFICATION
			if (!Util.isEmpty(i.getUserNotification())) {
				log.info("****Seteando message: ");

				message = i.getUserNotification().getOwnerProfile().getUrlName() + " make user in your profile";

				log.info("***** message: " + message);
				token = i.getUserNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}

			// EVENT NOTIFICATION
			if (!Util.isEmpty(i.getEventNotification())) {
				log.info("****Seteando message: ");

				message = i.getEventNotification().getOwnerProfile().getUrlName() + " make event in your profile";

				log.info("***** message: " + message);
				token = i.getEventNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}

			// ATTACHMENT NOTIFICATION
			if (!Util.isEmpty(i.getAttachmentNotification())) {
				log.info("****Seteando message: ");

				message = i.getAttachmentNotification().getOwnerProfile().getUrlName()
						+ " make attachment in your profile";

				log.info("***** message: " + message);
				token = i.getAttachmentNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}

			// POLL NOTIFICATION
			if (!Util.isEmpty(i.getPollNotification())) {
				log.info("****Seteando message: ");

				message = i.getPollNotification().getOwnerProfile().getUrlName() + " make poll in your profile";

				log.info("***** message: " + message);
				token = i.getPollNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}

			// CONVERSATION NOTIFICATION
			if (!Util.isEmpty(i.getConversationNotification())) {
				log.info("****Seteando message: ");

				message = i.getConversationNotification().getOwnerProfile().getUrlName()
						+ " make conversation in your profile";

				log.info("***** message: " + message);
				token = i.getConversationNotification().getToProfile().getTokenFireBase();
				if (!Util.isEmpty(token)) {
					request.setToken(token);
					request.setMessage(message);
					request.setTopic(i.getId().toString());
					request.setTitle("Breaking");

					sendPushNotificationToToken(request);
					patchIsRead(i.getId(), SEND.getCode());
				}
			}

		});
	}

	/**
	 * @param id
	 */
	@Override
	public void patchReadNotification(Long id) {

		try {
			Optional<Notification> notification = notificationRepository.findById(id);

			if (!notification.isPresent()) {
				log.error("[NotificationService.patchNotification] No se encontro la notificacion con el ID: " + id);
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.NOTIFICATION_NOT_FOUND.getCode(),
								ApiErrorEnum.NOTIFICATION_NOT_FOUND.getMessage())));
			} else {

				notification.get().setIsRead(READ.getCode());
				notification.get().setDateRead(new Date());

				notificationRepository.save(notification.get());
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [NotificationService.patchReadNotification]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteNotificationById(Long id) {

		try {
			Optional<Notification> notification = notificationRepository.findById(id);
			if (!notification.isPresent()) {
				log.error("[NotificationService.patchNotification] No se encontro la notificacion con el ID: " + id);
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.NOTIFICATION_NOT_FOUND.getCode(),
								ApiErrorEnum.NOTIFICATION_NOT_FOUND.getMessage())));
			} else {
				notificationRepository.deleteById(notification.get().getId());
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [NotificationService.deleteNotificationById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@Override
	public PaginateResponse<NotificationScrollDTO> getAllByProfileId(Integer pageNumber, Integer pageSize) {

		try {
			SecurityContext auth = SecurityContextHolder.getContext();
			// Long userId
			JwtData loggedUser = new JwtData();
			try {
				loggedUser = (JwtData) auth.getAuthentication().getPrincipal();
			} catch (Exception e) {
				log.error(" err___ " + e.getMessage());
			}

			Long userId = loggedUser.getUserId();

			log.info("Long userId  " + loggedUser.getUserId());
			MutableList<NotificationScrollDTO> responseNotificationDTOS = Lists.mutable.empty();
			PageRequest request = PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id")));

			// Query Principal
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Notification> criteriaQuery = criteriaBuilder.createQuery(Notification.class);
			Root<Notification> from = criteriaQuery.from(Notification.class);
			CriteriaQuery<Notification> select = criteriaQuery.select(from);
			select.where(criteriaBuilder.equal(from.get("toUser"), userId));
			select.orderBy(criteriaBuilder.desc(from.get("dateNotification")));

			TypedQuery<Notification> query = entityManager.createQuery(select);

			int totalRows = query.getResultList().size();

			Pageable pageable = request;
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
			query.setMaxResults(pageable.getPageSize());

			Page<Notification> page = new PageImpl<>(query.getResultList(), pageable, totalRows);

			page.getContent().forEach(i -> {
				log.info("Information I-->>> " + i.toString());
				responseNotificationDTOS.add(NotificationMapper.mapNotificationProfile(i));
			});

			return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
					page.getSize(), responseNotificationDTOS);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [NotificationService.getAllByProfileId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 */
	///////// BEGING: Este bloque de código es para enviar noticiaciones
	// push personalizad consumiendo los endpoint desde un portal-admin
	/////////////////////////////////////////////////////////////////////

	/**
	 *
	 */
	@Override
	public void sendSamplePushNotification() {
		notificationScheduler.sendSamplePushNotification();
	}

	/**
	 * @param request
	 */
	@Override
	public void sendPushNotification(PushNotificationRequest request) {
		try {
			fcmService.sendMessage(getSamplePayloadData(), request);
		} catch (InterruptedException | ExecutionException e) {
			log.warn(MESSAGE, e);
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * @param request
	 */
	@Override
	public void sendPushNotificationWithoutData(PushNotificationRequest request) {
		try {
			fcmService.sendMessageWithoutData(request);
		} catch (InterruptedException | ExecutionException e) {
			log.warn(MESSAGE, e);
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * @param request
	 */
	@Override
	public void sendPushNotificationToToken(PushNotificationRequest request) {
		try {
			fcmService.sendMessageToToken(request);
		} catch (InterruptedException | ExecutionException e) {
			log.warn(MESSAGE, e);
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * @return
	 */
	private Map<String, String> getSamplePayloadData() {
		Map<String, String> pushData = new HashMap<>();
		pushData.put("messageId", defaults.get("payloadMessageId"));
		pushData.put("text", defaults.get("payloadData") + " " + LocalDateTime.now());
		return pushData;
	}

	///////// END: Este bloque de código es para enviar noticiaciones
	// push personalizad consumiendo los endpoint desde un portal-admin
	/////////////////////////////////////////////////////////////////////
}
