package com.dinbog.api.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.dto.*;
import com.dinbog.api.exception.*;
import com.dinbog.api.models.vo.Email;
import com.dinbog.api.repository.UserRepository;
import com.dinbog.api.security.jwt.JwtFilter;
import com.dinbog.api.util.email.*;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dinbog.api.models.entity.Group;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.models.entity.UserEmail;
import com.dinbog.api.models.entity.UserGroup;
import com.dinbog.api.models.entity.UserStatus;
import com.dinbog.api.security.jwt.JwtData;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.GroupMapper;
import com.dinbog.api.util.mapper.UserEmailMapper;
import com.dinbog.api.util.mapper.UserMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 */
@Slf4j
@Service("UserService")
public class UserServiceImpl implements IUserService {

	private static final Integer MAIN_EMAIL_LEVEL = 1;

	@Autowired
	private JmsProducerImpl jmsProducer;

	@Autowired
	private JwtProvider jwtTokenProvider;

	@Autowired
	private JwtFilter jwtFilter;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private IUserEmailService userEmailService;

	@Autowired
	private IUserStatusService userStatusService;

	@Autowired
	private IGroupService groupService;

	@Autowired
	private IUserGroupService userGroupService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private IEmailServiceImpl emailUtilsService;
	
	@Autowired
	private IAuthService authService;
	

	/**
	 * Crea un usuario
	 * 
	 * @param request
	 * @return
	 */
	@Override
	@Transactional
	public UserCreateResponseDTO create(UserCreateDTO request, final String lang) {
		
		
		
		MutableList<ApiError> errors = validaRequest(request, lang);
		
		if (!errors.isEmpty()) {
			throw new InvalidRequestException(errors);
		}

		UserCreateResponseDTO response;
		User user = new User();

		try {
			// ARMA EL USUARIO
			user.setFirstName(request.getFirstName());
			user.setLastName(request.getLastName());
			// VALIDA ENCRIPTACION PASSWORD
			String encodedPassword = bCryptPasswordEncoder.encode(request.getPassword());
			user.setPassword(encodedPassword);
			user.setCreated(new Date());
			user.setLastLogin(new Date());
			
			request.setUserStatusId(1L);
			GroupBasicDTO groupBasic = new GroupBasicDTO();
			groupBasic.setId(1L);
			groupBasic.setDescription("Grupo de administradores");
			groupBasic.setName("ADMIN");
			groupBasic.setIsActive(true);
			ArrayList<GroupBasicDTO> groups = new ArrayList<>();
			groups.add(groupBasic);
			request.setGroups(groups);
			
			user.setUserStatus(
					userStatusService.findById(request.getUserStatusId())
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(errorService.getApiError(ApiErrorEnum.STATUS_NOT_FOUND, lang)))));

			// GUARDA USUARIO EN BD
			user = save(user);
			response = UserMapper.mapUserCreate(user);
			response.setToken(jwtTokenProvider.generateToken(user, request.getEmail().toLowerCase()));
			// ASOCIA USUARIO CON GRUPOS
			for (GroupBasicDTO i : request.getGroups()) {

				Group group = groupService.findById(i.getId()).orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.GROUP_NOT_FOUND, lang))));

				UserGroup userGroup = new UserGroup();
				userGroup.setGroup(group);
				userGroup.setUser(user);
				userGroupService.save(userGroup);

				response.getGroups().add(GroupMapper.mapGroupBasic(group));
			}

			// GUARDA EMAIL PRINCIPAL
			UserEmail userEmail = new UserEmail();
			userEmail.setUser(user);
			userEmail.setLevel(MAIN_EMAIL_LEVEL);
			userEmail.setEmail(request.getEmail().toLowerCase());
			userEmail.setConfirmed(false);
			userEmail.setToken(jwtTokenProvider.generateToken(user, request.getEmail()));
			userEmailService.save(userEmail);

			jmsProducer.sendToQueue(EmailConstants.TOPIC.getValue(),new Email(request.getEmail(), EmailSubjects.CONFIRM.getSubject(), emailUtilsService.setConfirmEmailBody(userEmail.getToken())), lang);

			response.setEmail(request.getEmail().toLowerCase());
			response.setEmailToken(userEmail.getToken());

			return response;

		} catch (Exception e) {
			
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Manda una lista de usuarios paginada. Permite filtrar (opcional) si los
	 * usuarios son activos o no
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	@Override
	public PaginateResponse<UserDTO> getAll(Integer pageNumber, Integer pageSize, Integer statusId) {

		try {
			Page<User> page = userRepository.findAll(new Specification<User>() {

				private static final long serialVersionUID = 2054671764906023538L;

				@Override
				public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

					MutableList<Predicate> predicates = Lists.mutable.empty();
					// active
					if (!Util.isEmpty(statusId)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("userStatus"), statusId)));
					}

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}

			}, PageRequest.of(pageNumber - 1, pageSize, Sort.by(Order.desc("id"))));

			if (page.getTotalElements() > 0L) {

				MutableList<UserDTO> users = Lists.mutable.empty();

				for (User user : page.getContent()) {

					users.add(UserMapper.mapUser(user).setEmails(UserEmailMapper
							.mapUserEmailList(userEmailService.findByUserId(user.getId()).orElse(null))));
				}

				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), users);

			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.USER_NO_RESULTS.getCode(), ApiErrorEnum.USER_NO_RESULTS.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.getAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Busca un usuario por su id
	 *
	 * @param id
	 * @return
	 */
	@Override
	public UserDTO getById(Long id) {

		try {
			Optional<User> user = userRepository.findById(id);

			if (user.isPresent()) {

				return construyeUserDTO(user.get());

			} else {
				log.error("[UserService.getById] No se encontro User con el ID: " + id);
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.getById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Busca un usuario por su email
	 *
	 * @param email
	 * @return
	 */
	@Override
	public UserDTO getByEmail(String email) {

		try {
			Optional<UserEmail> userEmail = userEmailService.findByEmail(email);

			if (userEmail.isPresent()) {

				return construyeUserDTO(userEmail.get().getUser());

			} else {
				log.error("[UserService.getByEmail] No se encontro User con el email: " + email);
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.getByEmail]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param request
	 * @return
	 */
	@Override
	public UserDTO update(Long userId, UserEditDTO request) {

		try {
			// BUSCA USER
			User user = userRepository.findById(userId).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(
					new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage()))));

			// ACTUALIZA STATUS (SI APLICA)
			if (!Util.isEmpty(request.getUserStatusId())) {
				user.setUserStatus(userStatusService.findById(request.getUserStatusId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.STATUS_NOT_FOUND.getCode(),
										ApiErrorEnum.STATUS_NOT_FOUND.getMessage())))));
			}
			// ACTUALIZA GRUPOS (SI APLICA)
			MutableList<GroupBasicDTO> groupsDTO = Lists.mutable.empty();
			if (!Util.isEmpty(request.getGroups())) {
				// BORRA GRUPOS ANTERIORES DEL USER
				userGroupService.deleteByUser(userId);

				// ASOCIA USUARIO CON GRUPOS
				for (GroupBasicDTO i : request.getGroups()) {

					Group group = groupService.findById(i.getId())
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.GROUP_NOT_FOUND.getCode(),
											ApiErrorEnum.GROUP_NOT_FOUND.getMessage()))));

					UserGroup userGroup = new UserGroup();
					userGroup.setGroup(group);
					userGroup.setUser(user);
					userGroupService.save(userGroup);

					groupsDTO.add(GroupMapper.mapGroupBasic(group));
				}

			} else {
				// SI NO SE ACTUALIZA, SE BUSCA LOS GRUPOS ACTUALES PARA RESPONSE
				List<Group> groups = groupService.findByUser(userId)
						.orElseThrow(() -> new InternalErrorException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.USER_WITHOUT_GROUP.getCode(),
										ApiErrorEnum.USER_WITHOUT_GROUP.getMessage()))));

				groups.forEach(u -> groupsDTO.add(GroupMapper.mapGroupBasic(u)));
			}

			return UserMapper.mapUser(userRepository.saveAndFlush(user)).setGroups(groupsDTO).setEmails(
					UserEmailMapper.mapUserEmailList(userEmailService.findByUserId(user.getId()).orElse(null)));

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param user
	 * @return
	 */
	private UserDTO construyeUserDTO(User user) {

		try {
			UserDTO response = UserMapper.mapUser(user);

			// BUSCA GRUPOS
			Optional<List<Group>> grupos = groupService.findByUser(user.getId());
			if (grupos.isPresent()) {

				grupos.get().forEach(i -> {

					Optional<Group> group = groupService.findById(i.getId());

					if (group.isPresent()) {
						response.getGroups().add(GroupMapper.mapGroupBasic(group.get()));
					}
				});
			}

			// BUSCA EMAILS
			response.setEmails(
					UserEmailMapper.mapUserEmailList(userEmailService.findByUserId(user.getId()).orElse(null)));

			return response;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.construyeUserDTO]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Indica si un email existe o no asociado a un usuario
	 * 
	 * @param email
	 * @return
	 */
	@Override
	public boolean userEmailExists(String email) {
		try {

			return userEmailService.findByEmail(email).isPresent();

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.userEmailExists]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	//  DETERMINAR LAS REGLAS DE NEGOCIO PARA MANEJO DE EMAIL:
	// - SI BORRO EMAIL PRINCIPAL QUE PASA?
	// - SI EDITO PRINCIPAL Y LE CAMBIO LEVEL QUE PASA?
	// - SI EDITO SECUNDARIO Y LE PONGO NIVEL 1, QUE PASA?

	/**
	 * @param userId
	 * @param request
	 * @return
	 */
	@Override
	public UserEmail createEmail(Long userId, UserEmailRequestDTO request) {

		UserEmail response = new UserEmail();

		// BUSCA USER
		User user = userRepository.findById(userId).orElseThrow(() -> new InvalidRequestException(Lists.mutable
				.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage()))));
		response.setUser(user);

		// DATOS DEL EMAIL
		response.setLevel(request.getLevel());
		response.setEmail(request.getEmail());

		// DATOS PARA CONFIRMACION
		response.setConfirmed(false);
		response.setToken(jwtTokenProvider.generateToken(user, request.getEmail()));

		response = userEmailService.saveAndFlush(response);

		// AJUSTA OTROS EMAILS DEL USER
		ajusteLevelsMail(userId, response.getId(), request.getLevel());

		// RETORNA OBJETO RESPONSE
		return response;
	}

	/**
	 * Metodo para ajustar el campo 'level' de un grupo de emails de un user, luego
	 * de agregar/ajustar uno
	 * 
	 * @param userId
	 * @param id
	 * @param level
	 * @return
	 */
	private List<UserEmail> ajusteLevelsMail(Long userId, Long id, Integer level) {

		// BUSCA EMAILS DEL USER PARA AJUSTE
		List<UserEmail> emails = userEmailService.listLevelEdit(userId, id).orElseThrow(
				() -> new InternalErrorException(Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NO_EMAILS.getCode(),
						ApiErrorEnum.USER_NO_EMAILS.getMessage()))));

		// AJUSTA RESTANTE EMAILS
		int i = 1;
		for (UserEmail e : emails) {
			if (MAIN_EMAIL_LEVEL.equals(level)) {
				// LEVEL ASIGNADO ES DE PRINCIPAL (SOLO PUEDE HABER UNO)
				userEmailService.saveAndFlush(e.setLevel(MAIN_EMAIL_LEVEL + i));
			} else {
				// AJUSTA LEVELS IGUAL O MAYORES
				// se edita directo a un level menor, revisar y ajustar
				if (i >= level) {
					userEmailService.saveAndFlush(e.setLevel(1 + i));
				}
			}
			i++;
		}

		return emails;
	}

	/**
	 * @param userId
	 * @param id
	 * @param level
	 * @return
	 */
	@Override
	public List<UserEmailDTO> editEmail(Long userId, Long id, Integer level) {

		UserEmail userEmail = userEmailService.findById(id)
				.orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMAIL_NOT_FOUND.getCode(), ApiErrorEnum.EMAIL_NOT_FOUND.getMessage()))))
				.setLevel(level);

		userEmailService.saveAndFlush(userEmail);

		// AJUSTA OTROS EMAILS DEL USER
		ajusteLevelsMail(userId, id, level);

		// BUSCA EMAILS DEL USUARIO
		List<UserEmail> emails = userEmailService.findByUserId(userId).orElseThrow(
				() -> new InternalErrorException(Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NO_EMAILS.getCode(),
						ApiErrorEnum.USER_NO_EMAILS.getMessage()))));

		return UserEmailMapper.mapUserEmailList(emails);
	}

	/**
	 * Trae los emails de un usuario
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public List<UserEmailDTO> getEmails(Long userId) {
		// VALIDA QUE USER EXISTA
		if (userRepository.findById(userId).isPresent()) {

			// BUSCA EMAILS DEL USUARIO
			List<UserEmail> emails = userEmailService.findByUserId(userId)
					.orElseThrow(() -> new InternalErrorException(Lists.mutable.of(new ApiError(
							ApiErrorEnum.USER_NO_EMAILS.getCode(), ApiErrorEnum.USER_NO_EMAILS.getMessage()))));

			return UserEmailMapper.mapUserEmailList(emails);
		} else {
			//
			log.error("[UserService.getEmails] No se encontro User con el ID: " + userId);
			throw new InvalidRequestException(Lists.mutable
					.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(), ApiErrorEnum.USER_NOT_FOUND.getMessage())));
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteEmail(Long id) {

		try {
			UserEmail userEmail = userEmailService.findById(id).orElseThrow(() -> new InvalidRequestException(
					Lists.mutable.of(new ApiError(ApiErrorEnum.EMAIL_NOT_FOUND.getCode(),
							ApiErrorEnum.EMAIL_NOT_FOUND.getMessage()))));

			if (userEmail.getLevel().equals(MAIN_EMAIL_LEVEL)) {
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.EMAIL_MAIN_NO_DELETE.getCode(), ApiErrorEnum.EMAIL_MAIN_NO_DELETE.getMessage())));
			}

			// ELIMINA USER
			userEmailService.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.deleteEmail]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	/**
	 * Deja en blanco el password de un usuario (reset)
	 *
	 *
	 */
	@Override
	public void resetPassword(User user, String email, HttpServletRequest request) {

		try {

			if(user.getPassword() != null && !user.getPassword().isEmpty()) {
				user.setPassword(null);
				userRepository.save(user);
			}

            String token = jwtTokenProvider.generateToken(user, email);
			
			jmsProducer.sendToQueue(
					EmailConstants.TOPIC.getValue(),
					new Email(
							email,
							EmailSubjects.REQUEST_NEW_PASSWORD.getSubject(),
							emailUtilsService.setRequestResetPassword(request,token)
					),
					null
			);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.resetPassword]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Borra un usuario por su id
	 *
	 * @param id
	 * @return
	 */
	@Override
	public void deleteById(Long id) {

		try {
			// ELIMINA USER
			userRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	/**
	 *
	 * @param userId
	 */
	@Override
	public void deleteEmailByUserId(Long userId) {
		try {
			userEmailService.deleteByUser(userId);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserService.deleteEmailByUserId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public Optional<UserEmail> findByToken(String token){
		
		token = token.replaceFirst("Bearer", "").stripLeading();
		return userEmailService.findByToken(token);
		
	}

	@Override
	public Optional<User> findById(Long id) {
		return userRepository.findById(id);
	}

	@Override
	public User findByEmail(String email) {
		return userEmailService.findByEmail(email).orElseThrow(() -> new BadCredentialsException(
				Lists.mutable.of(errorService.getApiError(
						ApiErrorEnum.USER_NOT_FOUND, null)
				)
		)
		).getUser();
	}

	@Override
	public Optional<User> findUserByIdAndPasswordNull(Long id) {
		return  userRepository.findUserByIdAndPasswordNull(id);
	}
	
	/**
	 * @param request
	 * @return
	 */
	private MutableList<ApiError> validaRequest(UserCreateDTO request, String lang) {

		MutableList<ApiError> apiErrors = Lists.mutable.empty();
		
		if(request.getProfileTypeId() == null || request.getProfileTypeId() < 1L ) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_TYPE_ID, lang));
		}else if(request.getProfileTypeId().equals(ProfileConstant.TALENT_PROFILE_TYPE)) {
			// LAST NAME
			if (Util.isEmpty(request.getLastName())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_LAST_NAME, lang));
			}
			
		}

		// EMAIL
		if (Util.isEmpty(request.getEmail())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_EMAIL, lang));
		} else {
			// VALIDA QUE EMAIL NO EXISTA
			if (userEmailExists(request.getEmail().toLowerCase())) {
				apiErrors.add(errorService.getApiError(ApiErrorEnum.USER_EMAIL_EXISTS, lang));
			}
		}
		// PASSWORD
		if (Util.isEmpty(request.getPassword())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PASSWORD, lang));
		}
		
		// FIRST NAME
		if (Util.isEmpty(request.getFirstName())) {
			apiErrors.add(errorService.getApiError(ApiErrorEnum.EMPTY_FIRST_NAME, lang));
		}
		
		return apiErrors;
	}

	public ConfirmEmailDTO confirmEmailToken(String token) {
		
		return authService.confirmEmailToken(token);
	}
	
	public  UserStatus createUserStatus(UserStatusRequestDTO request) {
		
		return userStatusService.create(request);
	
	}
	
	
	public MutableList<UserStatusDTO> readAllUserStatus(){
		
		return userStatusService.readAll();
	}
	
	public UserStatus readUserStatusById(Long id) {
		
		return userStatusService.readById(id);
		
	}
	
	public UserStatus updateUserStatus(Long id, UserStatusRequestDTO request) {
		
		return userStatusService.update(id, request);
	}
	
	public void deleteUserStatusById(Long id) {
		
		userStatusService.deleteById(id);
	}
	
	@Transactional
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public void patchPassword(UserPatchPasswordDTO userPatchPasswordDTO, HttpServletRequest request,String lang) {

		MutableList<ApiError> errors = validateNewPasswordRequest(userPatchPasswordDTO,lang);

		if(!errors.isEmpty()) throw new InvalidRequestException(errors);

		String bearer = jwtFilter.getBearerFromRequest(request);
		JwtData data = jwtTokenProvider.getJWTData(bearer);

		User user = findByEmail(data.getEmail());
		if (user != null) {
			user.setPassword(bCryptPasswordEncoder.encode(userPatchPasswordDTO.getNewPassword()));
			userRepository.save(user);
			 jmsProducer.sendToQueue(
					EmailConstants.TOPIC.getValue(),
					new Email(
							data.getEmail(),
							EmailSubjects.RESET_PASSWORD_SUCCESS.getSubject(),
							emailUtilsService.setResetPasswordSuccess()
					),
					lang
			);
		} else {
			throw new ResourceNotFoundException(
					Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),lang)
					));
		}

	}

	private MutableList<ApiError> validateNewPasswordRequest(UserPatchPasswordDTO userPatchPasswordDTO,String lang){
		MutableList<ApiError> errors = Lists.mutable.empty();
		if(userPatchPasswordDTO.getNewPassword().isEmpty() || userPatchPasswordDTO.getNewPassword() == null)
			errors.add(errorService.getApiError(ApiErrorEnum.EMPTY_NEW_PASSWORD,lang));
		if(userPatchPasswordDTO.getConfirmationPassword().isEmpty() || userPatchPasswordDTO.getConfirmationPassword() == null)
			errors.add(errorService.getApiError(ApiErrorEnum.EMPTY_CONFIRM_PASSWORD,lang));
		if(!userPatchPasswordDTO.getNewPassword().equals(userPatchPasswordDTO.getConfirmationPassword()))
			errors.add(errorService.getApiError(ApiErrorEnum.NEW_CONFIRM_PASSWORD_NOT_EQUAL,lang));
		return  errors;
	}
}
