/**
 * 
 */
package com.dinbog.api.service;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.LogCreateDTO;
import com.dinbog.api.dto.LogDTO;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Log;
import com.dinbog.api.repository.LogRepository;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.LogMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 *
 */
@Slf4j
@Service
public class LogServiceImpl implements ILogService {

	@Autowired
	private LogRepository logRepository;

	@Autowired
	private IUserService userService;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public Log registraLog(LogCreateDTO request) {

		try {
			Log log = new Log();
			log.setUser(
					userService.findById(request.getUserId())
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),
											ApiErrorEnum.USER_NOT_FOUND.getMessage())))));
			log.setIsError(request.getIsError());
			log.setIsWarning(request.getIsWarning());
			log.setMessage(request.getMessage());
			log.setCreated(new Date());

			return logRepository.save(log);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LogService.registraLog]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @param userId
	 * @param message
	 * @return
	 */
	@Override
	public PaginateResponse<LogDTO> readAll(Integer pageNumber, Integer pageSize, Long userId, String message) {
		try {
			MutableList<LogDTO> logDTOS = Lists.mutable.empty();

			Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
			Page<Log> page = logRepository.findAll(new Specification<Log>() {
				private static final long serialVersionUID = 5787195586470305631L;

				@Override
				public Predicate toPredicate(Root<Log> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					MutableList<Predicate> predicates = Lists.mutable.empty();
					if (!Util.isEmpty(userId)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("user"), userId)));
					}

					if (!Util.isEmpty(message)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder
								.like(criteriaBuilder.lower(root.get("message")), "%" + message.toLowerCase() + "%")));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			}, pageable);

			if (page.getTotalElements() > 0L) {
				page.getContent().forEach(i -> logDTOS.add(LogMapper.mapLog(i)));

				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), logDTOS);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.LOG_NO_RESULTS.getCode(), ApiErrorEnum.LOG_NO_RESULTS.getMessage())));
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [LogService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
