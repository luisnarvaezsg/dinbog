package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.ProfileRequestStatus;

public interface IProfileRequestStatusService {

	Optional<ProfileRequestStatus> findById(Long profileRequestStatusId);

}
