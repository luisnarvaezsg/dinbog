package com.dinbog.api.service;

import com.dinbog.api.dto.*;
import com.dinbog.api.models.entity.*;
import com.dinbog.api.util.PaginateResponse;


import org.eclipse.collections.api.list.MutableList;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface IProfileService {
	
	Profile readById(Long id, String lang);

    Profile readByUrl(String urlName, String lang);

    Optional<Profile> findByUrlName(String urlName);

    ProfileFullDTO readFullByUrl(String urlName, User user, String lang);

    ProfileStatisticsDTO getProfileStatistics(String urlName);

    @Transactional(readOnly = true)
    PaginateResponse<ProfileDTO> readAll(Integer pageNumber, Integer pageSize, String search, String lang);

    ProfileFullDTO editFullByUrl(String urlName, ProfileFullRequestDTO request, String lang);

    SocialAccount createSocialAccount(SocialAccountCreateDTO request, Long profileId);

    void createFollow(User user, String urlProfileFollow, String lang);

    MutableList<ProfileFollowDTO> getFollowers(String urlname, String typeFollow, String lang, Integer page, Integer size);

    MutableList<ProfileSimpleDTO> getSuggesteds(Long profileId, Integer size, String lang);

    MutableList<ProfileSimpleDTO> getProfilesViewed(Long profileId, String lang);

    SocialAccount patchSocialAccount(SocialAccountPatchDTO request, Long profileId, Long socialAccountId);

    void deleteProfile(Long profileId);
    
    Album updoadBook(Long profileId, MultipartFile[] photos, MultipartFile cover, String title, String description, Boolean isPrivate, String language);
    
    ProfileAttachment uploadVideo(Long profileId, MultipartFile file, MultipartFile cover, String title, String description, Boolean isPrivate, String lang);
    
    ProfileAttachment uploadVideoURL(Long profileId, String filePath, MultipartFile cover, String title, String description, Boolean isPrivate, String lang);
    
    ProfileWorkDTO uploadWork(ProfileWorkDTO profileWorkDTO, String lang);
    
    Album updoadAudioAlbum(Long profileId, MultipartFile cover, String title, 
						   String description, Boolean isPrivate, String language);
    
    MutableList<ProfileBasicDTO> readByNameLike(String name, String lang);

    Optional<Profile> findById(Long id);

    Optional<List<ProfileSuggestedDTO>> getSuggestedProfileByUserName(User user, String name);

    Album updoadAudioFile(Long albumId, MultipartFile audio, MultipartFile cover,  
						  String metadata, Boolean isPrivate, String language);
    
    Optional<Profile> findByUser(User user);
    
    void deleteFollow(User user, String urlProfileFollow, String lang);

	ProfileSimpleDTO buildProfileSimpleDTO(Profile profile, String lang);
	
	void createLike(User user, String urlProfile, String lang);
	
	void deleteLike(User user, String urlProfile, String lang);

	void saveFilters(User user, List<ProfileFilterValueDTO> filters, String lang);
	
	ProfileDTO saveProfile(ProfileRequestDTO profile, String lang);

	Long countProfileByUrlName(String urlName);

	Long count();

	Optional<List<Profile>> findAllProfilesRandomNotInFollowers(Long id, Pageable pageable);
	
	Profile getProfile(User user, String lang);

	void blockProfile(User user, String urlName, String lang);
	
	void unblockProfile(User user, String urlName, String lang);

	ProfileBasicDTO updateImagesProfile(User user, MultipartFile photo, String imageType, String lang);

}
