package com.dinbog.api.service;

import com.dinbog.api.dto.PostDTO;
import com.dinbog.api.dto.ProfileBasicDTO;
import com.dinbog.api.dto.ProfileFollowBasicDTO;
import com.dinbog.api.dto.ProfileSuggestedDTO;
import com.dinbog.api.dto.SearchDTO;
import com.dinbog.api.models.entity.User;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ISearchService {
    @Transactional
    SearchDTO readAll(Integer pageNumber, Integer pageSize, Boolean findExact, String search, String lang);

    SearchDTO pagePost(Integer pageNumber, Integer pageSize, Boolean findExact, String searchs, String lang);

    @Transactional
    SearchDTO pageProfile(Integer pageNumber, Integer pageSize, Boolean findExact, String search, String lang);

	MutableList<ProfileFollowBasicDTO> findTalentByNameLike(User user, Integer pageSize, Integer pageNumber, String name, String lang);

	MutableList<PostDTO> findHashtagLike(Integer pageSize, Integer pageNumber, String hashtag, User user, String lang);

    List<ProfileSuggestedDTO> getSuggestedProfileByUserName(User user, String name);

	MutableList<ProfileBasicDTO> findCompanyByNameLike(User user, Integer pageSize, Integer pageNumber, String name,
			String lang);

	MutableList<ProfileFollowBasicDTO> findAllProfilesRandomNotInFollowers(User user, Integer pageNumber,
			Integer pageSize, String lang);

}
