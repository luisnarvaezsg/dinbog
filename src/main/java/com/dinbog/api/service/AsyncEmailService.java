package com.dinbog.api.service;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.vo.Email;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.email.LogEmailService;
import com.dinbog.api.util.template.BaseTemplates;
import com.dinbog.api.util.template.TemplateGenerator;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Service
public class AsyncEmailService {

    private final JavaMailSender mailSender;
    private final TemplateGenerator templateGenerator;

    @Autowired
    public AsyncEmailService(JavaMailSender mailSender, TemplateGenerator templateGenerator) {
        this.mailSender = mailSender;
        this.templateGenerator = templateGenerator;
    }

    @SneakyThrows
    public void sendEmail(Email email) {
        try {
            final MimeMessage mailMessage = mailSender.createMimeMessage();
            final MimeMessageHelper helper = new MimeMessageHelper(mailMessage,MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
            helper.setTo(email.getTo());
            helper.setSubject(email.getSubject());
            helper.setText( templateGenerator.getTemplate(BaseTemplates.EMAIL.getLayer(), email.getBodyMap()), true );
            mailSender.send(mailMessage);
        }catch (MessagingException me){
            LogEmailService.errorLogin(me);
            if( Util.isCustomException(me)) throw me;
            else throw new InternalErrorException(me);
        }
    }
}
