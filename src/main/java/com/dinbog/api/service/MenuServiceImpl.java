package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.MenuDTO;
import com.dinbog.api.dto.MenuRequestDTO;
import com.dinbog.api.repository.MenuRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Menu;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.MenuMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MenuServiceImpl implements IMenuService {

	@Autowired
	private MenuRepository menuRepository;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public Menu create(MenuRequestDTO request) {

		try {
			Menu menu = new Menu();
			menu.setName(request.getName());
			menu.setRoute(request.getRoute());
			menu.setIsAvaliable(request.getIsAvailable());
			return menuRepository.save(menu);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public Menu update(Long id, MenuRequestDTO request) {

		try {
			Menu menu = menuRepository.findById(id).orElseThrow(() -> new InternalErrorException(Lists.mutable.of(
					new ApiError(ApiErrorEnum.MENU_NOT_FOUND.getCode(), ApiErrorEnum.MENU_NOT_FOUND.getMessage()))));
			menu.setName(request.getName());
			menu.setRoute(request.getRoute());
			menu.setIsAvaliable(request.getIsAvailable());
			return menuRepository.save(menu);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @return
	 */
	@Override
	public MutableList<MenuDTO> readAll() {

		try {
			MutableList<MenuDTO> menuDTOS = Lists.mutable.of();
			List<Menu> menus = menuRepository.findAll();
			if (menus.isEmpty()) {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getCode(),
								ApiErrorEnum.GLOBAL_LIST_NO_RESULTS.getMessage())));
			}
			menus.forEach(i -> menuDTOS.add(MenuMapper.mapMenu(i)));
			return menuDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {

		try {
			menuRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [MenuService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<Menu> findById(Long id) {
		
		return menuRepository.findById(id);
	}

}
