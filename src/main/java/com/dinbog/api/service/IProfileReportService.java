package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileReportCreateDTO;
import com.dinbog.api.models.entity.ProfileReport;

public interface IProfileReportService {
    ProfileReport create(Long profileId, ProfileReportCreateDTO request);
}
