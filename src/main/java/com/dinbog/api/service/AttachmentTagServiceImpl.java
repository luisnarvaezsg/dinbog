package com.dinbog.api.service;

import com.dinbog.api.dto.TagDTO;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentTag;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.repository.AttachmentTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AttachmentTagServiceImpl implements  IAttachmentTagService {

    private final IProfileService profileService;
    private final AttachmentTagRepository repository;

    @Autowired
    public AttachmentTagServiceImpl(IProfileService profileService, AttachmentTagRepository repository) {
        this.profileService = profileService;
        this.repository =  repository;
    }

    @Override
    public void create(List<TagDTO> tags, Attachment attachment, String lang) {
        for(TagDTO tag :tags) {
            Optional<Profile> profile = profileService.findById(tag.getProfileId());
            if(profile.isPresent()) {
                AttachmentTag attachmentTag = new AttachmentTag(profile.get(),attachment,tag.getPosX(), tag.getPosY());
                repository.save(attachmentTag);
            }
        }
    }

    @Override
    public void deleteByAttachment(Attachment attachment) {
        Optional<List<AttachmentTag>> tags = repository.findAttachmentTagByAttachment(attachment);
        tags.ifPresent(attachmentTags -> attachmentTags.forEach(repository::delete));
    }
}
