package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.ProfileCountryType;

public interface IProfileCountryTypeService {

	Optional<ProfileCountryType> findById(Long id);

}
