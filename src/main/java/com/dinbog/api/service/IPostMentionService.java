package com.dinbog.api.service;

import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostMention;

import java.util.List;

public interface IPostMentionService {

    void addPostMention(Post post, List<Long> mentions);
    
    List<PostMention> findByPost(Post post);
    
    void deleteByPost(Post post);
}
