package com.dinbog.api.service;

import java.util.Date;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.ConfirmEmailDTO;
import com.dinbog.api.dto.LoginDTO;
import com.dinbog.api.dto.LoginResponseDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.BadCredentialsException;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.models.entity.UserEmail;
import com.dinbog.api.security.jwt.JwtData;
import com.dinbog.api.security.jwt.JwtProvider;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.UserEmailMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("AuthServiceImpl")
public class AuthServiceImpl implements IAuthService  {

	@Autowired
	private JwtProvider jwtTokenProvider;

	@Autowired
	private IUserEmailService userEmailService;
	
	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private IApiErrorService errorService;

	/**
	 * Devuelve la respuesta correcta del login, si el password enviado en el
	 * request coincide con el que esta almacenado
	 * 
	 * @param request
	 * @return
	 */
	@Override
	public LoginResponseDTO authenticateUser(LoginDTO request, String language) {

		LoginResponseDTO loginResponse = new LoginResponseDTO();
		
		UserEmail userEmail = userEmailService.findByEmail(request.getEmail().toLowerCase()).orElseThrow(() -> new BadCredentialsException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.USER_NOT_FOUND, null))));
		
		User user = userEmail.getUser();

		if (Util.isEmpty(user.getPassword())) {
			// PASSWORD ES NULL - ESTA RESETEADO, NO PUEDE LOGUEAR
			throw new BadCredentialsException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.USER_RESET_NO_LOGIN, language)));
		}
		boolean isPasswordMatch = bCryptPasswordEncoder.matches(request.getPassword(), user.getPassword());
		
		if (isPasswordMatch) {
			// LOGIN EXITOSO
			loginResponse.setToken(jwtTokenProvider.generateToken(user, request.getEmail().toLowerCase()));
			loginResponse.setUserId(user.getId());
			
			// BUSCA URL PROFILE
			Optional<Profile> profile = profileService.findByUser(user);
			if(profile.isPresent()) {
				loginResponse.setProfileUrl(profile.get().getUrlName());
				loginResponse.setProfileId(profile.get().getId());
				loginResponse.setAvatarPath(profile.get().getAvatarPath());
				loginResponse.setProfileTypeId(profile.get().getProfileType().getId());
			}

			return loginResponse;
		} else {
			// PASSWORD INCORRECTO
			throw new BadCredentialsException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.LOGIN_PASSWORD_INVALID, null)));
		}

	}

	/**
	 * @param token
	 */
	@Override
	public ConfirmEmailDTO confirmEmailToken(String token) {

		try {
			JwtData data = jwtTokenProvider.getJWTData(token);

			// VALIDA SI EL TOKEN EXPIRÓ
			if (data.getExpirationDate().after(new Date(System.currentTimeMillis() * 1000))) {
				throw new BadCredentialsException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.TOKEN_EXPIRED.getCode(), ApiErrorEnum.TOKEN_EXPIRED.getMessage())));
			}

			// BUSCA EL EMAIL A CONFIRMAR EN EL TOKEN
			UserEmail userEmail = userEmailService.findByEmail(data.getEmail())
					.orElseThrow(() -> new BadCredentialsException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.EMAIL_TOKEN_NOT_FOUND.getCode(),
									ApiErrorEnum.EMAIL_TOKEN_NOT_FOUND.getMessage()))));

			// VALIDA SEA EXACTAMENTE EL MISMO TOKEN
			if (!token.contentEquals(userEmail.getToken())) {
				throw new BadCredentialsException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.TOKEN_INVALID.getCode(), ApiErrorEnum.TOKEN_INVALID.getMessage())));
			}

			// MARCA COMO CONFIRMADO EL EMAIL
			userEmail.setConfirmed(true);
			userEmail.setToken(null);
			userEmailService.save(userEmail);

			// ENVIO RESPUESTA
			return new ConfirmEmailDTO(userEmail.getUser().getId(), UserEmailMapper.mapUserEmail(userEmail));

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AuthService.confirmEmailToken]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
