package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileRecommendationCreateDTO;
import com.dinbog.api.dto.ProfileRecommendationDTO;
import com.dinbog.api.dto.ProfileRecommendationEditDTO;
import com.dinbog.api.models.entity.ProfileRecommendation;
import org.eclipse.collections.api.list.MutableList;

public interface IProfileRecommendationService {
    ProfileRecommendation create(Long profileId, ProfileRecommendationCreateDTO request);

    ProfileRecommendation patch(Long id, Long profileId, ProfileRecommendationEditDTO request);

    MutableList<ProfileRecommendationDTO> getListProfileRecommendationByProfileId(Long profileId);

    ProfileRecommendation getProfileRecommendationById(Long id);

    void delete(Long id, Long profileId);
}
