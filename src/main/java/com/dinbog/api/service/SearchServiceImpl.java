package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.*;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.ProfileRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.EventDTO;
import com.dinbog.api.dto.PostDTO;
import com.dinbog.api.dto.PostHashtagDTO;
import com.dinbog.api.dto.ProfileBasicDTO;
import com.dinbog.api.dto.ProfileSimpleDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.PostHashtag;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.EventMapper;
import com.dinbog.api.util.mapper.PostHashtagMapper;
import com.dinbog.api.util.mapper.ProfileMapper;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Slf4j
@Service
public class SearchServiceImpl implements ISearchService {

	private static final String URL_NAME = "urlName";

	private static final String NAME_HASTAG = "nameHasTag";

	private static final String CREATED = "created";

	@Autowired
	private IProfileService profileService;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private IProfileTalentService profileTalentService;

	@Autowired
	private IPostService iPostService;

	@Autowired
	private IPostHashtagService postHashtagService;

	@Autowired
	private IEventService eventService;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private IApiErrorService errorService;

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param findExact
	 * @param search
	 * @return
	 */
	@Override
	@Transactional
	public SearchDTO readAll(Integer pageNumber, Integer pageSize, Boolean findExact, String search, String lang) {
		try {
			int index = search.indexOf("#");
			log.info("#index: " + index);
			SearchDTO page;
			if (index != -1) {
				log.info("#__index: " + index);
				page = pagePost(pageNumber, pageSize, findExact, search, lang);
			} else {
				page = pageProfile(pageNumber, pageSize, findExact, search, lang);
			}
			return page;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SearchService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param findExact
	 * @param searchs
	 * @param lang
	 * @return
	 */
	@Override
	public SearchDTO pagePost(Integer pageNumber, Integer pageSize, Boolean findExact, String searchs, String lang) {
		SearchDTO searchDTOS = new SearchDTO();
		String search = searchs.replace("#", "");
		log.info("#_search: " + search);
		Page<PostHashtag> page = postHashtagService.findAll(new Specification<PostHashtag>() {
			private static final long serialVersionUID = -6913209024832379673L;

			@Override
			public Predicate toPredicate(Root<PostHashtag> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder criteriaBuilder) {
				MutableList<Predicate> predicates = Lists.mutable.empty();

				if (!Util.isEmpty(search)) {
					if (findExact) {

						Predicate criteria = (findExact)
								? criteriaBuilder.and(criteriaBuilder.equal(root.get(NAME_HASTAG), search))
								: criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get(NAME_HASTAG)),
										"%" + search.toLowerCase() + "%"));

						predicates.add(criteria);

					} else {
						predicates.add(criteriaBuilder.and(criteriaBuilder
								.like(criteriaBuilder.lower(root.get(NAME_HASTAG)), "%" + search.toLowerCase() + "%")));
					}
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		}, PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id"))));
		MutableList<PostHashtagDTO> postHashtagDTOS = Lists.mutable.of();
		page.getContent().forEach(i -> postHashtagDTOS.add(PostHashtagMapper.mapPostHashtag(i, false)));
		searchDTOS.setPost(postHashtagDTOS);
		searchDTOS.setTotalPages(page.getTotalPages());
		searchDTOS.setTotalElements(page.getTotalElements());
		searchDTOS.setPageNumber(page.getNumber() + 1);
		searchDTOS.setPageSize(page.getSize());

		return searchDTOS;
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param findExact
	 * @param search
	 * @param lang
	 * @return
	 */
	@Override
	@Transactional
	public SearchDTO pageProfile(Integer pageNumber, Integer pageSize, Boolean findExact, String search, String lang) {
		SearchDTO searchDTOS = new SearchDTO();
		MutableList<Predicate> predicates = Lists.mutable.empty();
		PageRequest request = PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id")));
		log.info("#_0_1_search: " + search);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Profile> criteriaQuery = criteriaBuilder.createQuery(Profile.class);
		Root<Profile> from = criteriaQuery.from(Profile.class);
		CriteriaQuery<Profile> select = criteriaQuery.select(from);
		if (!Util.isEmpty(search)) {
			if (findExact) {

				Predicate criteria = (findExact)
						? criteriaBuilder.and(criteriaBuilder.equal(from.get(URL_NAME), search))
						: criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(from.get(URL_NAME)),
								"%" + search.toLowerCase() + "%"));

				predicates.add(criteria);

			} else {
				predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(from.get(URL_NAME)),
						"%" + search.toLowerCase() + "%")));
			}
		}

		Predicate[] predicateArray = new Predicate[predicates.size()];
		predicates.toArray(predicateArray);
		select.where(predicateArray);

		select.orderBy(criteriaBuilder.desc(from.get(CREATED)));

		TypedQuery<Profile> query = entityManager.createQuery(select);
		int totalRows = query.getResultList().size();

		query.setFirstResult((request).getPageNumber() * (request).getPageSize());
		query.setMaxResults((request).getPageSize());

		Page<Profile> page = new PageImpl<>(query.getResultList(), request, totalRows);
		MutableList<ProfileSimpleDTO> profileDTOS = Lists.mutable.of();
		MutableList<EventDTO> eventDTOS = Lists.mutable.of();
		page.getContent().forEach(i -> {
			profileDTOS.add(ProfileMapper.mapProfileSimple(i, null, null, null, null,null));
			eventService.findByOwnerId(i.getId()).forEach(j -> eventDTOS.add(EventMapper.mapEvent(j)));
		});
		searchDTOS.setEvent(eventDTOS);
		searchDTOS.setProfile(profileDTOS);
		searchDTOS.setTotalPages(page.getTotalPages());
		searchDTOS.setTotalElements(page.getTotalElements());
		searchDTOS.setPageNumber(page.getNumber() + 1);
		searchDTOS.setPageSize(page.getSize());
		return searchDTOS;
	}

	@Override
	public List<ProfileSuggestedDTO> getSuggestedProfileByUserName(User user, String name) {
		List<ProfileSuggestedDTO> suggested = new ArrayList<>();
		Optional<Profile> profile = profileService.findByUser(user);
		if (profile.isPresent()) {

			Stream<Supplier<Optional<List<Profile>>>> queries = Stream.of(
					() -> profileRepository.findAllSuggestedFollowersProfilesActivatedByFullName(profile.get().getId(),
							name.toLowerCase()),
					() -> profileRepository.findAllSuggestedOthersProfilesActivatedByFullName(profile.get().getId(),
							name.toLowerCase()));

			for (CompletableFuture<Optional<List<Profile>>> optionalCompletableFuture : queries
					.map(CompletableFuture::supplyAsync).collect(Collectors.toList())) {
				Optional<List<Profile>> join = optionalCompletableFuture.join();
				join.ifPresent(profiles -> suggested.addAll(profiles.stream().parallel().distinct()
						.map(ProfileSuggestedDTO::new).collect(Collectors.toList())));
			}
		}
		return suggested;
	}

	@Override
	public MutableList<ProfileFollowBasicDTO> findAllProfilesRandomNotInFollowers(User user, Integer pageNumber, Integer pageSize, String lang) {

		Profile profileOwner = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));

		MutableList<ProfileFollowBasicDTO> response = Lists.mutable.empty();

		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.asc(CREATED)));
		Optional<List<Profile>> profiles = profileService.findAllProfilesRandomNotInFollowers(profileOwner.getId(), pageable);

		if (profiles.isPresent()) {
			List<Profile> listaP = profiles.get();
			
			listaP.stream().distinct().
			forEach(p -> response.add(profileTalentService.mapProfileFollow(profileOwner, p, lang)));
		}
		return response;

	}

	@Override
	public MutableList<ProfileFollowBasicDTO> findTalentByNameLike(User user, Integer pageSize, Integer pageNumber,
			String name, String lang) {

		MutableList<ProfileFollowBasicDTO> response = Lists.mutable.empty();

		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc(CREATED)));
		Profile profileOwner = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		Page<Profile> listaProfiles = profileTalentService.findByFullNameContainingIgnoreCase(profileOwner, pageable, name);
		if (!listaProfiles.isEmpty()) {
			listaProfiles.forEach(p -> response.add(profileTalentService.mapProfileFollow(profileOwner, p, lang)));
		}
		return response;
	}

	@Override
	public MutableList<ProfileBasicDTO> findCompanyByNameLike(User user, Integer pageSize, Integer pageNumber,
			String name, String lang) {

		MutableList<ProfileBasicDTO> response = Lists.mutable.empty();
		Profile profile = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc(CREATED)));
		Page<Profile> listaProfiles = profileTalentService.findByFullNameCompanyContainingIgnoreCase(profile, pageable,
				name);
		if (!listaProfiles.isEmpty()) {
			listaProfiles.forEach(p -> response.add(ProfileMapper.mapProfileBasic(p)));
		}
		return response;
	}

	@Override
	@Transactional
	public MutableList<PostDTO> findHashtagLike(Integer pageSize, Integer pageNumber, String hashtag, User user,
			String lang) {

		MutableList<PostDTO> response = Lists.mutable.empty();
		Profile profile = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));

		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc(CREATED)));
		Page<PostHashtag> listaPosts = postHashtagService.findByNameHasTagContainingIgnoreCase(pageable, hashtag);
		if (!listaPosts.isEmpty()) {
			listaPosts.forEach(postHash -> response.add(iPostService.buildPostDTO(postHash.getPost(), profile, lang)));
		}
		return response;
	}
}
