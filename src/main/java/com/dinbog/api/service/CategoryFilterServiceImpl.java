package com.dinbog.api.service;

import com.dinbog.api.repository.CategoryFilterRepository;

import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.CategoryFilterDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.CategoryFilter;
import com.dinbog.api.models.entity.Filter;
import com.dinbog.api.models.entity.ProfileCategoryType;
import com.dinbog.api.util.mapper.CategoryFilterMapper;


@Service
public class CategoryFilterServiceImpl implements ICategoryFilterService{

	@Autowired
	private CategoryFilterRepository categoryFilterRepository;
	
	@Autowired
	private IApiErrorService errorService;

	
	@Override
	public MutableList<CategoryFilterDTO> findByProfileCategoryType(ProfileCategoryType profileCategoryType, String lang) {

		MutableList<CategoryFilterDTO> response = Lists.mutable.empty();
		
		try {
			List<CategoryFilter> filters = categoryFilterRepository.findByProfileCategoryTypeId(profileCategoryType.getId());
			
			if(!filters.isEmpty()) {
				filters.forEach(f -> response.add(CategoryFilterMapper.mapCategoryFilter(f)));
			}
		
		}catch(Exception e){
			
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CATEGORY_FILTER_NOT_FOUND, lang)));
		}
		
		return response;

	}
	
	@Override
	public Optional<CategoryFilter> findByProfileCategoryTypeIdAndFilter(Long profileCategoryTypeId, Filter filter, String lang) {

		try {
			
			return categoryFilterRepository.findByProfileCategoryTypeIdAndFilter(profileCategoryTypeId, filter);
			
		}catch(Exception e){
			
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CATEGORY_FILTER_NOT_FOUND, lang)));
		}
	}
}
