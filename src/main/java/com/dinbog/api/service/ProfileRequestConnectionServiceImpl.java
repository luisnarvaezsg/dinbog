package com.dinbog.api.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.ProfileConnectionRequestingDTO;
import com.dinbog.api.dto.ProfileRequestConnectionDTO;
import com.dinbog.api.dto.ProfileRequestConnectionRequestDTO;
import com.dinbog.api.models.entity.ConnectionType;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRequestStatus;
import com.dinbog.api.repository.ProfileRequestConnectionRepository;
import com.dinbog.api.util.ProfileConnectionStatus;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.ProfileRequestConnection;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileRequestConnectionMapper;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileRequestConnectionServiceImpl implements IProfileRequestConnectionService {

	private final IProfileService profileService;
	private final IProfileRequestStatusService profileRequestStatusService;
	private final IConnectionTypeService connectionTypeService;
	private final ProfileRequestConnectionRepository profileRequestConnectionRepository;
	private final IApiErrorService errorService;

	private Optional<Profile> profile;
	private Optional<ConnectionType> connectionType;

	@Autowired
	public ProfileRequestConnectionServiceImpl(
			IProfileService profileService,
			IProfileRequestStatusService profileRequestStatusService,
			IConnectionTypeService connectionTypeService,
			ProfileRequestConnectionRepository profileRequestConnectionRepository,
			IApiErrorService errorService
	) {
		this.profileService = profileService;
		this.profileRequestStatusService = profileRequestStatusService;
		this.connectionTypeService = connectionTypeService;
		this.profileRequestConnectionRepository = profileRequestConnectionRepository;
		this.errorService = errorService;
	}

	/**
	 * @param ownerProfileId
	 * @param request
	 * @return
	 */
	@Override
    public ProfileRequestConnection create(Long ownerProfileId, ProfileRequestConnectionRequestDTO request) {

		try {
			ProfileRequestConnection profileRequestConnection = new ProfileRequestConnection();
			// profile
			profileRequestConnection
					.setProfile(profileService.findById(request.getProfileId())
							.orElseThrow(() -> new InternalErrorException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));

			// owner
			profileRequestConnection
					.setOwner(profileService.findById(ownerProfileId)
							.orElseThrow(() -> new InternalErrorException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));

			profileRequestConnection.setProfileRequestStatus(profileRequestStatusService
					.findById(request.getProfileRequestStatusId())
					.orElseThrow(() -> new InternalErrorException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND.getMessage())))));

			profileRequestConnection.setConnectionType(connectionTypeService.findById(request.getTypeConnectionId())
					.orElseThrow(() -> new InternalErrorException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.CONNECTION_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.CONNECTION_TYPE_NOT_FOUND.getMessage())))));

			profileRequestConnection.setCreated(new Date());

			return profileRequestConnectionRepository.save(profileRequestConnection);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRequestConnectionService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param ownerProfileId
	 * @param request
	 * @return
	 */
	@Override
    public ProfileRequestConnection edit(Long id, Long ownerProfileId, ProfileRequestConnectionRequestDTO request) {

		try {
			Boolean modificado = false;

			ProfileRequestConnection profileRequestConnection = profileRequestConnectionRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NOT_FOUND.getMessage()))));

			// VALIDA QUE EL OWNER PERTENEZA AL PROFILE
			if (ownerProfileId.compareTo(profileRequestConnection.getOwner().getId()) != 0) {
				// error
				throw new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NO_MATCH.getCode(),
								ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NO_MATCH.getMessage())));
			}

			// PROFILE
			if (!Util.isEmpty(request.getProfileId())
					&& request.getProfileId().compareTo(profileRequestConnection.getProfile().getId()) != 0) {
				modificado = true;
				profileRequestConnection.setProfile(profileService.findById(request.getProfileId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
										ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			}

			// PROFILE OWNER
			if (!Util.isEmpty(ownerProfileId)
					&& ownerProfileId.compareTo(profileRequestConnection.getOwner().getId()) != 0) {
				modificado = true;
				profileRequestConnection.setOwner(profileService.findById(ownerProfileId)
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
										ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			}

			// PROFILE REQUEST STATUS
			if (!Util.isEmpty(request.getProfileRequestStatusId()) && request.getProfileRequestStatusId()
					.compareTo(profileRequestConnection.getProfileRequestStatus().getId()) != 0) {
				modificado = true;
				profileRequestConnection.setProfileRequestStatus(profileRequestStatusService
						.findById(request.getProfileRequestStatusId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND.getCode(),
										ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND.getMessage())))));
			}

			// TYPE CONNECTION
			if (!Util.isEmpty(request.getTypeConnectionId()) && request.getTypeConnectionId()
					.compareTo(profileRequestConnection.getConnectionType().getId()) != 0) {
				modificado = true;
				profileRequestConnection
						.setConnectionType(connectionTypeService.findById(request.getTypeConnectionId())
								.orElseThrow(() -> new InvalidRequestException(
										Lists.mutable.of(new ApiError(ApiErrorEnum.CONNECTION_TYPE_NOT_FOUND.getCode(),
												ApiErrorEnum.CONNECTION_TYPE_NOT_FOUND.getMessage())))));
			}

			if (Boolean.TRUE.equals(modificado)) {
				// ACTUALIZA CONNECTION
				return profileRequestConnectionRepository.save(profileRequestConnection);
			} else {
				// SI NO HAY CAMPOS QUE MODIFICAR ARROJA UN ERROR
				throw new InvalidRequestException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.CONNECTION_NO_PATCH.getCode(), ApiErrorEnum.CONNECTION_NO_PATCH.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRequestConnectionService.edit]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param pageSize
	 * @param profileId
	 * @param profileRequestStatusId
	 * @param typeConnectionId
	 * @return
	 */
	@Override
    public PaginateResponse<ProfileRequestConnectionDTO> readAll(Integer pageSize, Long ownerId, Long profileId,
                                                                 Long profileRequestStatusId, Long typeConnectionId, String lang) {
		
		try {
			// TRAE DATA PAGINADA AL AZAR
			Long totalRecords = profileService.count();
			long totalPages = (totalRecords % pageSize == 0) ? (totalRecords / pageSize)
					: ((totalRecords / pageSize) + 1);
			int pageIndex = new Random().nextInt((int) totalPages);
			Page<ProfileRequestConnection> page = profileRequestConnectionRepository
					.findAll(new Specification<ProfileRequestConnection>() {
						private static final long serialVersionUID = 6858002684842819611L;

						@Override
						public Predicate toPredicate(Root<ProfileRequestConnection> root,
								CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
							MutableList<Predicate> predicates = Lists.mutable.empty();

							if (!Util.isEmpty(ownerId)) {
								predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("owner"), ownerId)));
							}

							if (!Util.isEmpty(profileId)) {
								predicates.add(
										criteriaBuilder.and(criteriaBuilder.equal(root.get("profile"), profileId)));
							}

							if (!Util.isEmpty(profileRequestStatusId)) {
								predicates.add(criteriaBuilder.and(criteriaBuilder
										.equal(root.get("profileRequestStatus"), profileRequestStatusId)));
							}

							if (!Util.isEmpty(typeConnectionId)) {
								predicates.add(criteriaBuilder
										.and(criteriaBuilder.equal(root.get("typeConnection"), typeConnectionId)));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, PageRequest.of(pageIndex, pageSize));

			if (page.getTotalElements() > 0L) {

				MutableList<ProfileRequestConnectionDTO> profileRequestConnections = Lists.mutable.empty();
				page.getContent().forEach(i -> profileRequestConnections
						.add(ProfileRequestConnectionMapper.mapProfileRequestConnection(i)));
				
				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), profileRequestConnections);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NO_RESULTS.getCode(),
								ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NO_RESULTS.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRequestConnectionService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param profileId
	 * @param profileRequestStatusId
	 * @param typeConnectionId
	 * @return
	 */
	@Override
    public PaginateResponse<ProfileRequestConnectionDTO> readAll(Integer pageNumber, Integer pageSize, Long ownerId,
                                                                 Long profileId, Long profileRequestStatusId, Long typeConnectionId, String lang) {
		
		try {

			Page<ProfileRequestConnection> page = profileRequestConnectionRepository
					.findAll(new Specification<ProfileRequestConnection>() {
						private static final long serialVersionUID = 5450110158172940755L;

						@Override
						public Predicate toPredicate(Root<ProfileRequestConnection> root,
								CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
							MutableList<Predicate> predicates = Lists.mutable.empty();

							if (!Util.isEmpty(ownerId)) {
								predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("owner"), ownerId)));
							}

							if (!Util.isEmpty(profileId)) {
								predicates.add(
										criteriaBuilder.and(criteriaBuilder.equal(root.get("profile"), profileId)));
							}

							if (!Util.isEmpty(profileRequestStatusId)) {
								predicates.add(criteriaBuilder.and(criteriaBuilder
										.equal(root.get("profileRequestStatus"), profileRequestStatusId)));
							}

							if (!Util.isEmpty(typeConnectionId)) {
								predicates.add(criteriaBuilder
										.and(criteriaBuilder.equal(root.get("typeConnection"), typeConnectionId)));
							}

							return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
						}
					}, PageRequest.of(pageNumber - 1, pageSize, Sort.by(Sort.Order.desc("id"))));
			if (page.getTotalElements() > 0L) {
				MutableList<ProfileRequestConnectionDTO> profileRequestConnections = Lists.mutable.empty();

				page.getContent().forEach(i -> profileRequestConnections
						.add(ProfileRequestConnectionMapper.mapProfileRequestConnection(i)));
				
				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), profileRequestConnections);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NO_RESULTS.getCode(),
								ApiErrorEnum.PROFILE_REQUEST_CONNECTION_NO_RESULTS.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRequestConnectionService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	@Override
	public MutableList<ApiError> validateRequest(ProfileConnectionRequestingDTO connectionRequestingDTO,String lang,Long ownerId) {

		MutableList<ApiError> errors =	Lists.mutable.empty();

		if( connectionRequestingDTO.getProfileId() == null ) {
			errors.add(errorService.getApiError(ApiErrorEnum.EMPTY_PROFILE_ID, lang));
		}

		if( connectionRequestingDTO.getProfileId().equals(ownerId) ) {
			errors.add(errorService.getApiError(ApiErrorEnum.PROFILE_MUST_NOT_EQUAL_OWNER, lang));
		}else{
			this.profile = profileService.findById(connectionRequestingDTO.getProfileId());

			if(this.profile.isEmpty()) errors.add(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang));

			List<ProfileRequestConnection> activeRequest = findActiveRequestByProfileId(connectionRequestingDTO.getProfileId(),ownerId);

			if (!activeRequest.isEmpty()){

				if(activeRequest.stream().anyMatch(
							status ->
							status.getProfileRequestStatus().getId().equals(ProfileConnectionStatus.ACCEPTED.getCode()) ||
							status.getProfileRequestStatus().getId().equals(ProfileConnectionStatus.PENDING.getCode())
					)
				) errors.add(errorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_ACTIVE_ALREADY, lang));
				if(activeRequest.stream().anyMatch(
						status -> status.getProfileRequestStatus().getId().equals(ProfileConnectionStatus.RESTRICTED.getCode())
				     )
				) errors.add(errorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_BANNED, lang));
			}

		}

		if(connectionRequestingDTO.getTypeConnectionId() == null) {
			errors.add(errorService.getApiError(ApiErrorEnum.CONNECTION_TYPE_ID_EMPTY, lang));
		}else{
			this.connectionType = connectionTypeService.findById(connectionRequestingDTO.getTypeConnectionId());

			if(this.connectionType.isEmpty())
				errors.add(errorService.getApiError(ApiErrorEnum.CONNECTION_TYPE_NOT_FOUND, lang));
		}

		return errors;
	}

	@Override
	public void create(Profile profileOwner, ProfileConnectionRequestingDTO profileConnectionRequestingDTO,String lang) {

		MutableList<ApiError> errors = validateRequest(profileConnectionRequestingDTO, lang, profileOwner.getId());

		if( !errors.isEmpty() ) throw new InvalidRequestException(errors);

		ProfileRequestConnection profileRequestConnection =  new ProfileRequestConnection();

		this.profile.ifPresent(profileRequestConnection::setProfile);

		this.connectionType.ifPresent(profileRequestConnection::setConnectionType);

		profileRequestConnection.setOwner(profileOwner);

		Optional<ProfileRequestStatus> requestStatus = profileRequestStatusService.findById(ProfileConnectionStatus.PENDING.getCode());

		if( requestStatus.isEmpty() )
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND, lang)
					)
			);

		profileRequestConnection.setProfileRequestStatus( requestStatus.get());

		profileRequestConnectionRepository.save(profileRequestConnection);

	}

	@Override
	public List<ProfileRequestConnection> findActiveRequestByProfileId(Long profileId, Long ownerId){
		return profileRequestConnectionRepository.findProfileRequestConnectionByProfile_IdAndOwner_Id(profileId, ownerId);
	}

	@Override
	public Optional<ProfileRequestConnection> findActiveRequestConnectionByIdAndOwnerId(Long id, Long ownerId) {
		return profileRequestConnectionRepository.findByIdAndOwnerIdAndStatusActiveOrPending(id, ownerId);
	}

	@Override
	public void editingConnection(ProfileRequestConnection profileRequestConnection, Long statusCode, String lang) {

		Optional<ProfileRequestStatus> requestStatus = profileRequestStatusService.findById(statusCode);

		if( requestStatus.isEmpty() )
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND, lang)
					)
			);

		profileRequestConnection.setProfileRequestStatus( requestStatus.get());

		profileRequestConnectionRepository.save(profileRequestConnection);
	}

	@Override
	public Optional<ProfileRequestConnection> findConnectionByIdProfileIdAndRequestStatus(Long id, Long profileId, Long requestStatus) {
		return profileRequestConnectionRepository.findByIdAndProfile_IdAndProfileRequestStatus_Id(id,profileId,requestStatus);
	}

	/*@Override
	public MutableList<ProfileRequestConnectionDTO> findAllPendingByProfileId(Long profileId, Integer index, Integer limit, String lang) {

		Pageable pagination = PageRequest.of(index,limit);

		Optional<List<ProfileRequestConnection>> list = profileRequestConnectionRepository
				.findAllByProfile_IdAndProfileRequestStatus_Id(profileId,ProfileConnectionStatus.PENDING.getCode(),pagination);

		if(list.isEmpty())
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND, lang)
					)
			);

		return getRequestedList(list.get());
	}*/

	@Override
	public MutableList<ProfileRequestConnectionDTO> findAllConnectionsByRequestStatus(Profile profile, Long statusCode, Integer index, Integer limit, String lang) {

		Optional<ProfileRequestStatus> requestStatus = profileRequestStatusService.findById(statusCode);
		if( requestStatus.isEmpty() )
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.PROFILE_REQUEST_CONNECTION_STATUS_NOT_FOUND, lang)
					)
			);

		Pageable pagination = PageRequest.of(index,limit);

		List<ProfileRequestConnection> preList = getQueryByStatusCode(profile,requestStatus.get(),pagination);

		if(preList.isEmpty())
			throw new InvalidRequestException(
					Lists.mutable.of(
							errorService.getApiError(ApiErrorEnum.LIST_REQUEST_CONNECTION_EMPTY, lang)
					)
			);

		return getRequestedList(preList);
	}

	private MutableList<ProfileRequestConnectionDTO> getRequestedList(List<ProfileRequestConnection> query){
		MutableList<ProfileRequestConnectionDTO> requestedList = Lists.mutable.empty();
		query.forEach( row ->{
			requestedList.add( ProfileRequestConnectionMapper.mapProfileRequestConnection(row) );
		});
		return requestedList;
	}

	private List<ProfileRequestConnection> getQueryByStatusCode( Profile profile,ProfileRequestStatus requestStatus,Pageable pagination){
		List<ProfileRequestConnection> fetchData = new ArrayList<>();
		if( requestStatus.getId().equals( ProfileConnectionStatus.ACCEPTED.getCode() ) )
			fetchData = profileRequestConnectionRepository.findAllConnectionsByStatusRequest(
					profile.getId(),requestStatus.getId(),pagination
			);
		else if(requestStatus.getId().equals( ProfileConnectionStatus.PENDING.getCode() ))
			fetchData = profileRequestConnectionRepository.findAllByProfile_IdAndProfileRequestStatus_Id(
					profile.getId(),requestStatus.getId(),pagination
			);

		return fetchData;
	}
}
