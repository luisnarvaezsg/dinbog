package com.dinbog.api.service;

import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.vo.Email;
import com.dinbog.api.util.email.LogEmailService;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JmsProducerImpl implements JmsProducer {

    private final JmsTemplate jmsTemplate;
    
    @Autowired
    IApiErrorService errorService;

    @Autowired
    public JmsProducerImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Value("${email.notification.active}")
    private boolean emailNotificationActive;
    
    @Override
    public void sendToQueue(String topic, Email email, String lang) {
    	try {
    		long startingAt = System.currentTimeMillis();
    		if(emailNotificationActive) {
    			jmsTemplate.convertAndSend(topic, email);
    		}
	        long endingAt = System.currentTimeMillis();
	        LogEmailService.sendingLog(startingAt,endingAt,email.getTo());
    	}catch(Exception e) {
    		throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.MAIL_SEND_ERROR, lang))
            );
    	}
    }
}
