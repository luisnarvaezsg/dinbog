package com.dinbog.api.service;

import com.dinbog.api.dto.PostReportRequestDTO;
import com.dinbog.api.models.entity.PostReport;

public interface IPostReportService {

	/**
	 * @param postId
	 * @param request
	 * @return
	 */
	PostReport create(Long postId, PostReportRequestDTO request);

}