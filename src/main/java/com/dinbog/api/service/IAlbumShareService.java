package com.dinbog.api.service;


import com.dinbog.api.dto.AlbumShareCreateDTO;
import com.dinbog.api.models.entity.AlbumShare;

public interface IAlbumShareService {

	AlbumShare create(AlbumShareCreateDTO request);

}