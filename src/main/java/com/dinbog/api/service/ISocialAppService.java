package com.dinbog.api.service;

import com.dinbog.api.dto.SocialAppCreateDTO;
import com.dinbog.api.dto.SocialAppDTO;
import com.dinbog.api.models.entity.SocialApp;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

public interface ISocialAppService {
	
    SocialApp create(SocialAppCreateDTO request);

    MutableList<SocialAppDTO> readAll();

    void patch(Long id, SocialAppCreateDTO request);

    void delete(Long id);
    
    Optional<SocialApp> findById(Long id);
}
