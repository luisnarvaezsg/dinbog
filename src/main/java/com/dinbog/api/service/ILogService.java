package com.dinbog.api.service;

import com.dinbog.api.dto.LogCreateDTO;
import com.dinbog.api.dto.LogDTO;
import com.dinbog.api.models.entity.Log;
import com.dinbog.api.util.PaginateResponse;

public interface ILogService {

	/**
	 * @param request
	 * @return
	 */
	Log registraLog(LogCreateDTO request);

	/**
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @param userId
	 * @param message
	 * @return
	 */
	PaginateResponse<LogDTO> readAll(Integer pageNumber, Integer pageSize, Long userId, String message);

}