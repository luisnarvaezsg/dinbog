package com.dinbog.api.service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.Profile;
import org.eclipse.collections.api.list.MutableList;
import com.dinbog.api.dto.AlbumCommentRequestDTO;
import com.dinbog.api.models.entity.AlbumComment;

import java.util.List;
import java.util.Optional;

public interface IAlbumCommentService {

	
	/**
	 * @param albumId
	 * @param id
	 */
	void deleteByAlbumIdAndById(Long albumId, Long id);

	void create(Album album, Profile profile, AlbumCommentRequestDTO request);

	MutableList<ApiError> validateModel(AlbumCommentRequestDTO requestDTO, String lang);

	Optional<AlbumComment> getAlbumCommentById(Long id);

	void delete(AlbumComment albumComment);

	List<AlbumComment> findAllByAlbum(Album album);

	void deleteByAlbum(Album album);
}
