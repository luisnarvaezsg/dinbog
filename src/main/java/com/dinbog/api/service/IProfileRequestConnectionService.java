package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileConnectionRequestingDTO;
import com.dinbog.api.dto.ProfileRequestConnectionDTO;
import com.dinbog.api.dto.ProfileRequestConnectionRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileRequestConnection;
import com.dinbog.api.util.PaginateResponse;
import org.eclipse.collections.api.list.MutableList;

import java.util.List;
import java.util.Optional;

public interface IProfileRequestConnectionService {

    ProfileRequestConnection create(Long ownerProfileId, ProfileRequestConnectionRequestDTO request);

    ProfileRequestConnection edit(Long id, Long ownerProfileId, ProfileRequestConnectionRequestDTO request);



    PaginateResponse<ProfileRequestConnectionDTO> readAll(Integer pageSize, Long ownerId, Long profileId,
                                                          Long profileRequestStatusId, Long typeConnectionId, String lang);

    PaginateResponse<ProfileRequestConnectionDTO> readAll(Integer pageNumber, Integer pageSize, Long ownerId,
                                                          Long profileId, Long profileRequestStatusId, Long typeConnectionId, String lang);


    MutableList<ApiError> validateRequest(ProfileConnectionRequestingDTO connectionRequestingDT,String lang, Long ownerId);

    void create(Profile profileOwner, ProfileConnectionRequestingDTO profileConnectionRequestingDTO,String lang);

   List<ProfileRequestConnection> findActiveRequestByProfileId(Long profileId, Long ownerId);

   Optional<ProfileRequestConnection> findActiveRequestConnectionByIdAndOwnerId(Long id, Long ownerId);

   void editingConnection(ProfileRequestConnection profileRequestConnection,Long statusCode, String lang);

   Optional<ProfileRequestConnection> findConnectionByIdProfileIdAndRequestStatus(Long id, Long profileId, Long requestStatus);

  // MutableList<ProfileRequestConnectionDTO> findAllPendingByProfileId(Long profileId, Integer index, Integer limit,String lang);

   MutableList<ProfileRequestConnectionDTO> findAllConnectionsByRequestStatus(Profile profile, Long statusCode, Integer index, Integer limit, String lang);

}
