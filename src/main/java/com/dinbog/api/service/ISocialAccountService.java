package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.SocialAccount;

public interface ISocialAccountService {

	Optional<SocialAccount> findById(Long id);

	SocialAccount create(SocialAccount socialAccount);

}