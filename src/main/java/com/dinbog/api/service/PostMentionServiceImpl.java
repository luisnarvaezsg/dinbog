package com.dinbog.api.service;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostMention;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.repository.PostMentionRepository;
import com.dinbog.api.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PostMentionServiceImpl implements IPostMentionService {

    private final PostMentionRepository postMentionRepository;
    private final IProfileService profileService;

    @Autowired
    public PostMentionServiceImpl(PostMentionRepository repository, IProfileService profileService) {
        this.postMentionRepository = repository;
        this.profileService =  profileService;
    }

    @Override
    public void addPostMention(Post post, List<Long> mentions) {
        if(!mentions.isEmpty()){
            mentions.forEach( mention -> {
                try {
                    Optional<Profile> profile = profileService.findById(mention);
                    if( profile.isPresent() ) {
                        PostMention postMention = new PostMention(post, profile.get(),new Date());
                        postMentionRepository.save(postMention);
                    }
                }catch (Exception e){
                    Util.printLogError(e);
                    throw new InternalErrorException(e);
                }
            });
        }
    }

	@Override
	public List<PostMention> findByPost(Post post) {
		
		return postMentionRepository.findByPost(post);
	}

	@Override
	@Transactional
	public void deleteByPost(Post post) {
		
		List<PostMention> listaPM=findByPost(post);
		if ((listaPM!=null)&& !listaPM.isEmpty()) {
            listaPM.forEach(postMentionRepository::delete);
        }
	}
}
