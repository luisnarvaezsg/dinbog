package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileLike;

public interface IProfileLikeService {
	
    ProfileLike getProfileLikeById(Long id);

    void delete(Profile profileOwner, Profile profile, String lang);

    void create(Profile profileLikeOwner, Profile profileLike, String lang);

	Optional<ProfileLike> findByOwnerAndProfile(Profile profileOwner, Profile profile);
}
