package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.AttachmentTypeBasicDTO;
import com.dinbog.api.dto.AttachmentTypeDTO;
import com.dinbog.api.models.entity.AttachmentType;

public interface IAttachmentTypeService {

	/**
	 *
	 * @return
	 */
	MutableList<AttachmentTypeDTO> readAll();

	AttachmentType create(AttachmentTypeBasicDTO request);

	AttachmentType patchById(Long id, AttachmentTypeBasicDTO request);

	void deleteById(Long id);

}