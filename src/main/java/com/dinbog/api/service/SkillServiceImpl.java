package com.dinbog.api.service;

import java.util.List;

import com.dinbog.api.dto.SkillDTO;
import com.dinbog.api.repository.SkillRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Skill;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.SkillMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("SkillService")
public class SkillServiceImpl implements ISkillService {

	@Autowired
	private SkillRepository skillRepository;

	/**
	 * @return
	 */
	@Override
	public MutableList<SkillDTO> readAll() {
		try {
			List<Skill> skillList;
			skillList = skillRepository.findAll();

			if (skillList.isEmpty()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.SKILLS_NO_RESULTS.getCode(), ApiErrorEnum.SKILLS_NO_RESULTS.getMessage())));
			} else {
				MutableList<SkillDTO> skillDTOS = Lists.mutable.of();
				skillList.forEach(i -> skillDTOS.add(SkillMapper.mapSkill(i)));

				return skillDTOS;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SkillService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
