package com.dinbog.api.service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;

public interface IApiErrorService {

	ApiError getApiError(ApiErrorEnum errorCode, String language);

}