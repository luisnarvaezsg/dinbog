package com.dinbog.api.service;

import com.dinbog.api.repository.ProfileWorkRepository;

import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.ProfileWorkDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileWork;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileWorkMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileWorkServiceImpl implements IProfileWorkService  {

	@Autowired
	private ProfileWorkRepository profileWorkRepository;
	
	@Autowired
	private IApiErrorService errorService;

	
	@Override
	public ProfileWork save(ProfileWork profileWork) {

		try {
			
			return profileWorkRepository.save(profileWork);
		
		} catch (Exception e) {
			log.error("*** Exception EN [ProfileWorkeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public ProfileWork findById(Long id) {

		try {
			
			Optional<ProfileWork> optional = profileWorkRepository.findById(id);
			ProfileWork profile = new ProfileWork();
			
			if(optional.isPresent())
				profile = optional.get();
			
			return profile;
		
		} catch (Exception e) {
			log.error("*** Exception EN [ProfileWorkService.findById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public MutableList<ProfileWorkDTO> findByProfile(Profile profile, String lang) {

		MutableList<ProfileWorkDTO> response = Lists.mutable.empty();
		
		try {
			List<ProfileWork> profileWorks = profileWorkRepository.findByProfile(profile);
			
			if(!profileWorks.isEmpty()) {
				profileWorks.forEach(p -> response.add(ProfileWorkMapper.mapProfileWork(p)));
			}
		
		}catch(Exception e){
			
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_WORK_NOT_FOUND, lang)));
		}
		
		return response;

	}
}
