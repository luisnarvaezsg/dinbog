package com.dinbog.api.service;

import com.dinbog.api.models.entity.PostShare;

public interface IPostShareService {

	/**
	 *
	 * @param postId
	 * @param profileId
	 * @return
	 */
	PostShare create(Long postId, Long profileId);

}