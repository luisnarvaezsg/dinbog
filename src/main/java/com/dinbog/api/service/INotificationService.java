package com.dinbog.api.service;

import com.dinbog.api.dto.NotificationScrollDTO;
import com.dinbog.api.firebase.PushNotificationRequest;
import com.dinbog.api.util.PaginateResponse;

public interface INotificationService {

	/**
	 * @param id
	 * @param isRead
	 */
	void patchIsRead(Long id, Integer isRead);

	// este metodo es una prueba de concepto
	void sendPushNotificationPrueba();

	/**
	 * @param id
	 */
	void patchReadNotification(Long id);

	/**
	 * @param id
	 */
	void deleteNotificationById(Long id);

	/**
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	PaginateResponse<NotificationScrollDTO> getAllByProfileId(Integer pageNumber, Integer pageSize);

	/**
	 *
	 */
	void sendSamplePushNotification();

	/**
	 * @param request
	 */
	void sendPushNotification(PushNotificationRequest request);

	/**
	 * @param request
	 */
	void sendPushNotificationWithoutData(PushNotificationRequest request);

	/**
	 * @param request
	 */
	void sendPushNotificationToToken(PushNotificationRequest request);

}