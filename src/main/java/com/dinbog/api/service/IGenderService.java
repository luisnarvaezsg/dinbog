package com.dinbog.api.service;

import com.dinbog.api.dto.GenderCreateDTO;
import com.dinbog.api.dto.GenderDTO;
import com.dinbog.api.dto.GenderEditDTO;
import com.dinbog.api.models.entity.Gender;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

public interface IGenderService {
    
	MutableList<GenderDTO> readAll(String langCode);

    Gender create(GenderCreateDTO request);

    void patchById(Long id, GenderEditDTO request);

    void deleteById(Long id);

	Optional<Gender> findById(Long genderId);
}
