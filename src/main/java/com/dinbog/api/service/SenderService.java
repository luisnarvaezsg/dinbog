package com.dinbog.api.service;

import com.dinbog.api.models.vo.Email;

public interface SenderService {

    void senderMessage(Email email);
}
