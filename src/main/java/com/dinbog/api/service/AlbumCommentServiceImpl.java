package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;


import com.dinbog.api.models.entity.CommentsAlbum;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.AlbumCommentRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.AlbumComment;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.repository.AlbumCommentRepository;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class AlbumCommentServiceImpl implements IAlbumCommentService {

	@Autowired
	private AlbumCommentRepository albumCommentRepository;

	@Autowired
	private ICommentsAlbumService commentsAlbumService;

	@Autowired
	private IApiErrorService errorService;

	/**
	 * @param albumId
	 * @param id
	 */
	@Transactional
	public void deleteByAlbumIdAndById(Long albumId, Long id) {

		try {
			// BUSCA COMMENT A ELIMINAR
			AlbumComment albumComment = albumCommentRepository.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_COMMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ALBUM_COMMENT_NOT_FOUND.getMessage()))));

			if (albumId.equals(albumComment.getAlbum().getId())) {
				albumCommentRepository.deleteByAlbumIdAndById(albumId, id);
			} else {
				// SI NO COINCIDE ID ALBUM INDICADO VS ID ALBUM OBTENIDO
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.ALBUM_NOT_FOUND.getCode(), ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumCommentService.deleteByAlbumIdAndById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	@Transactional
	public void create(Album album, Profile profile, AlbumCommentRequestDTO request) {
		CommentsAlbum commentsAlbum = commentsAlbumService.create(profile,request);
		AlbumComment albumComment =  new AlbumComment();
		albumComment.setAlbum(album);
		albumComment.setCommentsAlbum(commentsAlbum);
		albumCommentRepository.save(albumComment);
	}

	@Override
	public MutableList<ApiError> validateModel(AlbumCommentRequestDTO requestDTO,String lang) {
		MutableList<ApiError> errors =	Lists.mutable.empty();
		if(requestDTO.getValue().length() == 0 || requestDTO.getValue() == null ){
			errors.add(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang));
		}
		if (requestDTO.getAlbumId() == null){
			errors.add(errorService.getApiError(ApiErrorEnum.ALBUM_NOT_FOUND, lang));
		}
		if(requestDTO.getProfileId()== null){
			errors.add(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang));
		}
		return errors;
	}

	@Override
	public Optional<AlbumComment> getAlbumCommentById(Long id) {
		return albumCommentRepository.findById(id);
	}

	@Override
	@Transactional
	public void delete(AlbumComment albumComment) {
		List<CommentsAlbum> children = commentsAlbumService.getCommentsAlbumByParentId(albumComment.getCommentsAlbum().getId());
		if(!children.isEmpty()) {
			children.forEach(child -> commentsAlbumService.delete(child));
		}
		albumCommentRepository.delete(albumComment);
		commentsAlbumService.delete(albumComment.getCommentsAlbum());
	}

	@Override
	public List<AlbumComment> findAllByAlbum(Album album) {
		return  albumCommentRepository.findAllByAlbum(album);
	}

	@Override
	@Transactional
	public void deleteByAlbum(Album album) {
		List<AlbumComment> albumComments = findAllByAlbum( album );
		if(!albumComments.isEmpty())
			albumComments.forEach(this::delete);
	}
}
