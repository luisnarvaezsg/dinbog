package com.dinbog.api.service;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCategoryType;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.ProfileCategoryRepository;



@Service
public class ProfileCategoryServiceImpl implements IProfileCategoryService {
	
	@Autowired
	private ProfileCategoryRepository categoryRepository;
	
	@Autowired
	private ILanguageService languageService;
	
	@Autowired
	private ITraslationService traslationService;
	
	@Override
	public void save(ProfileCategory profileCategory) {
		
		categoryRepository.save(profileCategory);
		
	}

	@Override
	public List<ProfileCategory> findByProfileId(Long profileId, String lang) {
		
		List<ProfileCategory> profileCategories =  categoryRepository.findByProfileId(profileId);
		
		if (!profileCategories.isEmpty()) {
			
			for (ProfileCategory profileCategory : profileCategories) {
				
				traslateProfileCategory(profileCategory, lang);
			}
			
		} 
		return profileCategories;
	}

	@Override
	@Transactional
	public void delete(Long categoryProfileId) {
		
		categoryRepository.deleteById(categoryProfileId);
		
	}

	@Override
	public Optional<ProfileCategory> findById(Long profileCategoryId, String lang) {
		
		return categoryRepository.findById(profileCategoryId);
		
	}

	@Override
	public Optional<ProfileCategory> findByProfileAndProfileCategoryType(Profile profile,
			ProfileCategoryType profileCategoryType) {
		
		return categoryRepository.findByProfileAndProfileCategoryType(profile, profileCategoryType);
	}

	
	private void traslateProfileCategory(ProfileCategory profileCategory, String lang) {
		
		lang = languageService.validLanguage(lang);
		
		Traslation traslation =  traslationService.findByKeyAndIsoCode(profileCategory.getProfileCategoryType().getKey(), lang);
		
		if(traslation == null) {
			profileCategory.getProfileCategoryType().setValue(profileCategory.getProfileCategoryType().getKey());
		}else {
			profileCategory.getProfileCategoryType().setValue(traslation.getValue());
		}

	}
		
}
