package com.dinbog.api.service;

import java.net.URL;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import com.dinbog.api.dto.*;
import com.dinbog.api.models.entity.*;
import com.dinbog.api.util.ParameterEnum;
import com.dinbog.api.util.collections.ListUtils;
import com.dinbog.api.util.files.FileUtils;
import com.dinbog.api.util.text.analyzer.TextAnalyzer;
import com.dinbog.api.util.files.FileMapIndex;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.dinbog.api.aws.AWSBuckets;
import com.dinbog.api.aws.IS3Services;
import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ApiExceptionResponse;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.repository.PostRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.PostMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class PostServiceImpl implements IPostService {

	private static final String CREATED = "created";
	
	@Autowired
	private IS3Services s3Service;

	@Autowired
	private IAttachmentService iAttachmentService;

	@Autowired
	private IPostLikeService iPostLikeService;

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private IProfileService profileService;

	@Autowired
	private IProfileTalentService profileTalentService;

	@Autowired
	private IProfileCategoryService profileCategoryService;

	@Autowired
	private ICommentService iCommentService;

	@Autowired
	private IPostAttachmentService iPostAttachmentService;

	@Autowired
	private IPostCommentService iPostCommentService;

	@Autowired
	private IApiErrorService errorService;

	@Autowired
	private IPostHashtagService iPostHashtagService;

	@Autowired
	private IPostMentionService iPostMentionService;

	@Autowired
	private IAttachmentTagService iAttachmentTagService;

	@Autowired
	private FileUtils fileUtils;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private IJwtService iJwtService;

	@Override
	public void createPost(PostMultiFileDTO postMultiFileDTO, User user, String lang) {
		Post post = addPost(postMultiFileDTO.getComment(), user, lang);
		if (!Util.isEmpty(postMultiFileDTO.getComment()))
			addUserHashtagsMentions(post, postMultiFileDTO.getComment());
		for (AttachmentMultiFileDTO attached : postMultiFileDTO.getAttachments()) {
			if (fileUtils.isProperPostMultiFileType(attached.getFile())) {
				Attachment attachment = iAttachmentService.create(attached.getFile(), null, null,
						postMultiFileDTO.getLanguage(), AWSBuckets.POSTS.getBucket());
				ListUtils<TagDTO> tags = new ListUtils<>(attached.getTags());
				if (!tags.isNullOrEmpty()) {
					iAttachmentTagService.create(tags.getList(), attachment, postMultiFileDTO.getLanguage());
				}
				iPostAttachmentService.create(post, attachment);
			}
		}
	}

	/**
	 * Crea un post de imagen y/o video, pueden ser varios archivos simultaneos
	 */
	public void createPost(MultipartFile[] file, String comment, User user, String lang) {
		Post post = addPost(comment, user, lang);
		if (!Util.isEmpty(comment))
			addUserHashtagsMentions(post, comment);
		Arrays.stream(file).forEach(toUpload -> {
			Attachment attachment = iAttachmentService.create(toUpload, null, null, lang, AWSBuckets.POSTS.getBucket());
			iPostAttachmentService.create(post, attachment);
		});
	}

	@Override
	@Transactional
	/**
	 * Crea un post especialmente para un archivo de audio
	 */
	public void createPost(Map<String, MultipartFile> files, String comment, User user, String lang) {
		Post post = addPost(comment, user, lang);
		if (!Util.isEmpty(comment))
			addUserHashtagsMentions(post, comment);
		Attachment attachment = iAttachmentService.create(files.get(FileMapIndex.AUDIO.getKey()),
				files.get(FileMapIndex.COVER.getKey()), null, lang, AWSBuckets.POSTS.getBucket());
		iPostAttachmentService.create(post, attachment);
	}

	@Override
	/**
	 * Crea un post solo con un link o url
	 */
	public void createPost(String url, String comment, User user, String lang) {
		Post post = addPost(comment, user, lang);
		if (!Util.isEmpty(comment))
			addUserHashtagsMentions(post, comment);
		Attachment attachment = iAttachmentService.create(url, lang);
		iPostAttachmentService.create(post, attachment);

	}

	@Override
	/**
	 * Crea un post con un comentario
	 */
	@Transactional
	public void postComment(Long id, String comentario, User user, String lang) {

		Post post = findById(id, lang).orElseThrow(() -> new ApiExceptionResponse(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
		CommentsPost comment = new CommentsPost();
		comment.setCreated(new Date());
		comment.setCountLike(0);
		comment.setParent(null);
		Profile profile = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		comment.setProfile(profile);
		comment.setValue(comentario);
		iCommentService.save(comment);
		iPostCommentService.create(post, comment);
		post.setCountComment(post.getCountComment() + 1);
		postRepository.save(post);
	}

	@Override
	public S3Object getPost(String keyName) {

		return this.s3Service.downloadFile(keyName, CommonsConstant.BUCKET_NAME);

	}

	@Override
	public PostDTO buildPostDTO(Post post, Profile profUser, String lang) {
		Boolean liked = iPostLikeService.findByPostAndProfile(post, profUser);
		Boolean mine = (profUser.getId().longValue() == post.getProfile().getId().longValue());
		ProfileTalent pt = profileTalentService.findByProfile(post.getProfile())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		String residenceCity = "";
		if (pt.getProfileCountries() != null && !pt.getProfileCountries().isEmpty()) {
			for (ProfileCountry profileCountry : pt.getProfileCountries()) {
				if (profileCountry.getProfileCountryType().getName().equals("Branch"))
					residenceCity = profileCountry.getCountry().getName();
				else {
					if (residenceCity.equals(""))
						residenceCity = profileCountry.getCountry().getName();
				}
			}
		}
		List<Profile> listaProf = new ArrayList<>();
		if (post.getListaPostComment() != null && !post.getListaPostComment().isEmpty()) {
			listaProf = listaMention(post.getListaPostComment());
		}
		List<ProfileCategory> listaCat = profileCategoryService.findByProfileId(post.getProfile().getId(), lang);
		return PostMapper.mapPost(profUser, post, mine, liked, post.getProfile().getId(), pt.getFirstName(),
				pt.getLastName(), pt.getGender().getValue(), residenceCity, listaProf, listaCat);
	}

	private MutableList<PostDTO> procesaPosts(Page<Post> listaPosts, Profile profUser, String lang) {
		MutableList<PostDTO> responsePostDTOS = Lists.mutable.empty();
		listaPosts.forEach(post -> responsePostDTOS.add(buildPostDTO(post, profUser, lang)));
		return responsePostDTOS;
	}

	public List<Profile> listaMention(List<PostComment> listaPostComment) {
		List<Profile> listaProf = new ArrayList<>();

		listaPostComment.forEach(postComment -> listaProf.addAll(listaValue(postComment.getCommentsPost())));
		return listaProf;
	}

	private List<Profile> listaValue(CommentsPost comment) {
		List<Profile> listaProf = new ArrayList<>();
		TextAnalyzer analyzer2 = new TextAnalyzer(comment.getValue());
		List<Long> mentions = analyzer2.getMentions();
		analyzer2.close();
		if (!mentions.isEmpty()) {
			mentions.forEach(id -> listaProf.add(profileService.findById(id).get()));
		}
		List<CommentsPost> listaComment = comment.getListaComment();
		if (listaComment != null && !listaComment.isEmpty())
			listaComment.forEach(commentL -> listaProf.addAll(listaValue(commentL)));
		return listaProf;
	}

	@Override
	public PostDTO getPost(Long id, String lang, User user) {

		Post post = postRepository.findById(id).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
		Profile profile = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));

		return buildPostDTO(post, profile, lang);
	}

	@Override
	public String getPostURL(String objectKey) {

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion("us-east-2")
				.withCredentials(new ProfileCredentialsProvider()).build();

		// Set the presigned URL to expire after one hour.
		java.util.Date expiration = new java.util.Date();
		long expTimeMillis = expiration.getTime();
		expTimeMillis += 1000 * 60 * 60;
		expiration.setTime(expTimeMillis);

		// Generate the presigned URL.
		log.info("Generating pre-signed URL.");
		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
				CommonsConstant.BUCKET_NAME, objectKey).withMethod(HttpMethod.GET).withExpiration(expiration);
		URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

		log.info("Pre-Signed URL: " + url.toString());
		return url.toString();

	}

	@Override
	public MutableList<PostDTO> muro(User user, String lang, int pageNumber, int pageSize) {

		Profile profUser = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc(CREATED)));
		Page<Post> listaPosts = postRepository.findAll(pageable);

		return procesaPosts(listaPosts, profUser, lang);
	}

	@Override
	public MutableList<PostDTO> findAllByUser(User user, String lang) {

		Profile profUser = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		return findAllUserUrl(profUser, profUser, lang, 0, 10);

	}

	@Override
	public MutableList<PostDTO> findAllByProfile(HttpServletRequest request,String urlName, Integer pageNumber, Integer pageSize, String lang) {

		User user = iJwtService.getUser(request);
		Profile profUser = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		Profile profile = profileService.readByUrl(urlName, lang);
		return findAllUserUrl(profile, profUser, lang, pageNumber, pageSize);

	}

	private  MutableList<PostDTO> findAllUserUrl(Profile profile, Profile profUser, String lang, Integer pageNumber, Integer pageSize){
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc(CREATED)));
		Page<Post> listaPosts = postRepository.findAllByProfile(profile, pageable);

		return procesaPosts(listaPosts, profUser, lang);
	}

	@Override
	@Transactional
	public void deleteById(Long id, String lang) {

		Post post = findById(id, lang).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));

		iPostAttachmentService.deleteByPost(post);
		iPostLikeService.deleteByPost(post);
		iPostCommentService.deleteByPost(post);
		iPostHashtagService.deleteByPost(post);
		iPostMentionService.deleteByPost(post);

		postRepository.delete(post);
	}

	@Override
	public Optional<Post> findById(Long id, String lang) {

		Optional<Post> postO = postRepository.findById(id);
		if (postO.isPresent())
			return postO;
		else
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang)));
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public PostDTO getById(Long id, String lang) {

		try {
			Optional<Post> post = postRepository.findById(id);

			if (post.isPresent()) {
				return PostMapper.mapPost(null, post.get(), null, null, null, null, null, null, null, null, null);
			} else {
				log.error("[PostService.getById] No se encontro Post con el ID: " + id);
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostService.getById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void update(Long postId, String comment, User user, String lang) {

		Post post = postRepository.findById(postId).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
		Profile proUser = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		if (post.getProfile().getId().longValue() == proUser.getId().longValue())
			post.setContent(comment);
		else
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND, lang)));
		postRepository.saveAndFlush(post);
	}

	/**
	 * @param request
	 * @return
	 */
	@Override
	public Post update(PostEditDTO request, Long postId) {

		try {
			Post post = postRepository.findById(postId).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(
					new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(), ApiErrorEnum.POST_NOT_FOUND.getMessage()))));

			// ACTUALIZA PROFILE SI APLICA
			if (!Util.isEmpty(request.getProfileId())) {
				post.setProfile(profileService.findById(request.getProfileId())
						.orElseThrow(() -> new InternalErrorException(
								Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
										ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			}
			// ACTUALIZA content
			if (!Util.isEmpty(request.getContent())) {
				post.setContent(request.getContent());
			}
			// ACTUALIZA programed
			if (!Util.isEmpty(request.getProgramed())) {
				post.setProgramed(request.getProgramed());
			}
			// ACTUALIZA countLike
			if (!Util.isEmpty(request.getCountLike())) {
				// post.setCountLike(request.getCountLike());
			}
			// ACTUALIZA programedDateGmt
			if (!Util.isEmpty(request.getProgramedDateGmt())) {
				post.setProgramedDateGmt(request.getProgramedDateGmt());
			}

			return postRepository.saveAndFlush(post);

		} catch (Exception e) {
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	protected Post addPost(String comment, User user, String lang) {
		try {
			Post post = new Post();
			post.setCreated(new Date());
			post.setContent(comment == null ? ParameterEnum.BLANK.getKey() : comment);
			Profile profile = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
			post.setProfile(profile);
			postRepository.save(post);
			return post;
		} catch (Exception e) {
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Protected void method that gets and persist whether or not valid both
	 * hashtags and user mentions into any post(multiFile,audio-cover,link)
	 * performed by another logged user
	 * 
	 * @param post    Post instance
	 * @param comment Description related to the post
	 */
	protected void addUserHashtagsMentions(Post post, String comment) {

		TextAnalyzer analyzer = new TextAnalyzer(comment);
		List<String> hashTags = analyzer.getHashTags();
		analyzer.close();
		if (!hashTags.isEmpty()) {
			iPostHashtagService.addHashTags(post, hashTags);
		}

		TextAnalyzer analyzer2 = new TextAnalyzer(comment);
		List<Long> mentions = analyzer2.getMentions();
		analyzer2.close();
		if (!mentions.isEmpty()) {
			iPostMentionService.addPostMention(post, mentions);
		}
	}
	
	@Override
	public void save(Post post) {

		postRepository.save(post);

	}

	@Override
	public ApiError getApiError(ApiErrorEnum errorCode, String lang) {
		return errorService.getApiError(errorCode, lang);
	}
}
