package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dinbog.api.dto.AlbumTypeDTO;
import com.dinbog.api.dto.AlbumTypeRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.AlbumType;
import com.dinbog.api.repository.AlbumTypeRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AlbumTypeMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("AlbumTypeServiceImpl")
public class AlbumTypeServiceImpl implements IAlbumTypeService {

	@Autowired
	private AlbumTypeRepository albumTypeRepository;
	
	@Autowired
	private IApiErrorService errorService;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public AlbumType create(AlbumTypeRequestDTO request) {
		try {
			AlbumType albumType = new AlbumType();

			albumType.setName(request.getName());

			albumType.setDescription(request.getDescription());

			if (!Util.isEmpty(request.getIsActive())) {
				albumType.setIsActive(request.getIsActive());
			}

			return albumTypeRepository.save(albumType);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @return
	 */
	@Override
	public MutableList<AlbumTypeDTO> readAll() {

		try {
			MutableList<AlbumTypeDTO> albumTypeDTOS = Lists.mutable.empty();
			albumTypeRepository.findAll().forEach(i -> albumTypeDTOS.add(AlbumTypeMapper.mapAlbumType(i)));

			if (albumTypeDTOS.isEmpty()) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_TYPE_NO_RESULTS.getCode(),
								ApiErrorEnum.ALBUM_TYPE_NO_RESULTS.getMessage())));
			} else {
				return albumTypeDTOS;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public AlbumType update(Long id, AlbumTypeRequestDTO request) {
		try {
			AlbumType albumType = albumTypeRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(org.eclipse.collections.api.factory.Lists.mutable
							.of(new ApiError(ApiErrorEnum.ALBUM_TYPE_NO_FOUND.getCode(),
									ApiErrorEnum.ALBUM_TYPE_NO_FOUND.getMessage()))));

			if (StringUtils.hasText(request.getName())) {
				albumType.setName(request.getName());
			}

			if (StringUtils.hasText(request.getDescription())) {
				albumType.setDescription(request.getDescription());
			}

			if (!Util.isEmpty(request.getIsActive())) {
				albumType.setIsActive(request.getIsActive());
			}
			return 	albumTypeRepository.save(albumType);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {
		try {
			albumTypeRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumTypeService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	@Override
	public AlbumType findById(Long id, String lang) {
		
		Optional<AlbumType> album = albumTypeRepository.findById(id);
			
		if (!album.isPresent()) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ALBUM_TYPE_NO_FOUND, lang)));
		}
		return album.get();
	}

	@Override
	public Optional<AlbumType> findById(Long albumTypeId) {
		
		return albumTypeRepository.findById(albumTypeId);
	}
}
