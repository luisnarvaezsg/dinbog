package com.dinbog.api.service;

import java.util.Date;
import java.util.Optional;

import com.dinbog.api.dto.UserStatusDTO;
import com.dinbog.api.dto.UserStatusRequestDTO;
import com.dinbog.api.repository.UserStatusRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.UserStatus;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.UserStatusMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserStatusServiceImpl implements IUserStatusService {

	@Autowired
	private UserStatusRepository userStatusRepository;

	/**
	 * @param request
	 * @return
	 */
	@Override
	public UserStatus create(UserStatusRequestDTO request) {

		try {
			UserStatus userStatus = new UserStatus();
			userStatus.setName(request.getName());
			userStatus.setDescription(request.getDescription());
			userStatus.setIsActive(request.getIsActive());
			userStatus.setCreated(new Date());
			return userStatusRepository.save(userStatus);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserStatusService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 *
	 * @return
	 */
	@Override
	public MutableList<UserStatusDTO> readAll() {

		try {
			MutableList<UserStatusDTO> userStatusDTOS = Lists.mutable.of();
			userStatusRepository.findAll().forEach(i -> userStatusDTOS.add(UserStatusMapper.mapUserStatus(i)));
			return userStatusDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserStatusService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public UserStatus readById(Long id) {

		return  userStatusRepository.findById(id)
				.orElseThrow(() -> new InternalErrorException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.USER_STATUS_NOT_FOUND.getCode(),
								ApiErrorEnum.USER_STATUS_NOT_FOUND.getMessage()))));
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public UserStatus update(Long id, UserStatusRequestDTO request) {

		try {
			UserStatus userStatus = userStatusRepository.findById(id)
					.orElseThrow(() -> new InternalErrorException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.USER_STATUS_NOT_FOUND.getCode(),
									ApiErrorEnum.USER_STATUS_NOT_FOUND.getMessage()))));
			userStatus.setName(request.getName());
			userStatus.setDescription(request.getDescription());
			userStatus.setIsActive(request.getIsActive());
			userStatus.setCreated(new Date());
			return userStatusRepository.save(userStatus);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserStatusService.update]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {

		try {
			userStatusRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [UserStatusService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<UserStatus> findById(Long id) {
		
		return userStatusRepository.findById(id);
	}
}
