package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.ProfileBlockBasicDTO;
import com.dinbog.api.dto.ProfileBlockCreateDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileBlock;

public interface IProfileBlockService {

	/**
	 * @param profileId
	 * @param request
	 * @return
	 */
	ProfileBlockBasicDTO create(Long profileId, ProfileBlockCreateDTO request);

	/**
	 * @param urlName
	 * @return
	 */
	List<ProfileBlockBasicDTO> getBlocks(String urlName);

	/**
	 * @param profileId
	 * @param id
	 */
	void delete(Long profileId, Long id);

	Boolean isProfileBlocked(Long ownerId, Long profileId);

	void create(Profile owner, Profile profileBlocked);

	void remove(ProfileBlock profileBlock);

	Optional<ProfileBlock> findByOwnerAndProfile(Profile owner, Profile profileBlocked);
	
	MutableList<ApiError> validateProfileBlockOperation(Optional<Profile> owner, Optional<Profile> profile,String lang);

	void save(ProfileBlock profileBlock);
}
