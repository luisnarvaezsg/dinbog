package com.dinbog.api.service;

import com.dinbog.api.dto.UserGroupDTO;
import com.dinbog.api.dto.UserGroupRequestDTO;
import com.dinbog.api.models.entity.UserGroup;

import org.eclipse.collections.api.list.MutableList;
import org.springframework.transaction.annotation.Transactional;

public interface IUserGroupService {
    MutableList<UserGroupDTO> create(Long id, UserGroupRequestDTO request);

    @Transactional
    void deleteGroupByUser(Long userId);

    MutableList<UserGroupDTO> readByGroupId(Long id);

    @Transactional
    void deleteByGroupId(Long id);

    @Transactional
	void deleteByUser(Long id);

	void save(UserGroup userGroup);
}
