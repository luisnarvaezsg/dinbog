package com.dinbog.api.service;

import com.dinbog.api.dto.AlbumEditRequestDTO;
import com.dinbog.api.exception.ApiError;
import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.AlbumDTO;
import com.dinbog.api.dto.AlbumRequestDTO;
import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.AlbumType;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.util.PaginateResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface IAlbumService {

	Album create(AlbumRequestDTO request);

	Album update(Long id, AlbumRequestDTO request);

	PaginateResponse<AlbumDTO> readAll(Integer pageNumber, Integer pageSize, Long albumTypeId, String searchName,
			String lang);

	Album readById(Long id, String lang);

	void deleteById(Long id);


	MutableList<AlbumDTO> findByProfile(Profile profile, AlbumType albumType, String lang);

	void uploadFileCover(MultipartFile[] files,MultipartFile fileCover,Album album,String lang);

	Optional<Album> findById(Long id);

    void delete(Album album);

    MutableList<ApiError> validateEditRequest(AlbumEditRequestDTO albumEditRequestDTO,String lang);

    void updateAlbum(Album album, AlbumEditRequestDTO requestDTO);
    
    Album save(Album album);
}
