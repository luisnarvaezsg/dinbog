package com.dinbog.api.service;

import com.dinbog.api.dto.SkillDTO;
import org.eclipse.collections.api.list.MutableList;

public interface ISkillService {
    MutableList<SkillDTO> readAll();
}
