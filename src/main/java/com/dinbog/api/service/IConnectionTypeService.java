package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.ConnectionType;

public interface IConnectionTypeService {

	Optional<ConnectionType> findById(Long typeConnectionId);

}
