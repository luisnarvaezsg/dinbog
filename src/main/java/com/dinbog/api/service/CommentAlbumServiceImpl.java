package com.dinbog.api.service;

import com.dinbog.api.dto.AlbumCommentRequestDTO;
import com.dinbog.api.models.entity.CommentsAlbum;
import com.dinbog.api.models.entity.CommentsAlbumMention;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.repository.CommentsAlbumRepository;
import com.dinbog.api.util.text.analyzer.TextAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentAlbumServiceImpl implements ICommentsAlbumService {


    private final CommentsAlbumRepository commentsAlbumRepository;
    private final ICommentsAlbumMentionService commentsAlbumMentionService;

    @Autowired
    public CommentAlbumServiceImpl(CommentsAlbumRepository commentsAlbumRepository, ICommentsAlbumMentionService commentsAlbumMentionService) {
        this.commentsAlbumRepository = commentsAlbumRepository;
        this.commentsAlbumMentionService = commentsAlbumMentionService;
    }


    @Override
    public CommentsAlbum create(Profile profile, AlbumCommentRequestDTO requestDTO) {
        CommentsAlbum commentsAlbum =  new CommentsAlbum();
        commentsAlbum.setProfile(profile);
        commentsAlbum.setParent(null);
        commentsAlbum.setCountLike(0);
        commentsAlbum.setValue( requestDTO.getValue());
        commentsAlbum.setCreated(new Date());
        commentsAlbum = commentsAlbumRepository.save(commentsAlbum);

        List<Long> mentions = getMentions(commentsAlbum.getValue());

        if(!mentions.isEmpty()) commentsAlbumMentionService.create(commentsAlbum,mentions);

        return commentsAlbum;
    }

    @Override
    public List<Long> getMentions(String comment) {
        @SuppressWarnings("resource")
		TextAnalyzer analyzer = new TextAnalyzer(comment);
        return analyzer.getMentions();
    }

    @Override
    public List<CommentsAlbum> getCommentsAlbumByParentId(Long id) {
        return commentsAlbumRepository.findAllByParent_Id(id);
    }

    @Override
    public void delete(CommentsAlbum commentsAlbum) {
        List<CommentsAlbumMention> mentions = commentsAlbumMentionService.findAllMentionsInComment(commentsAlbum);
        if(!mentions.isEmpty()){
            mentions.forEach(commentsAlbumMentionService::delete);
        }
        commentsAlbumRepository.delete(commentsAlbum);
    }
}
