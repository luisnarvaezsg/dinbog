package com.dinbog.api.service;

import com.dinbog.api.dto.PostHashtagDTO;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostHashtag;
import com.dinbog.api.util.PaginateResponse;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface IPostHashtagService {

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param nameHashTag
	 * @return
	 */
	PaginateResponse<PostHashtagDTO> readAll(Integer pageNumber, Integer pageSize, Boolean typeQuery,
			String nameHashTag, Boolean isSearch, String lang);



	void addHashTags(Post post, List<String> hashTags);
	
	List <PostHashtag> findByPost(Post post);
	
	void deleteByPost(Post post);

	Page<PostHashtag> findAll(Specification<PostHashtag> specification, PageRequest of);

	Page<PostHashtag> findByNameHasTagContainingIgnoreCase(Pageable pageable, String hashtag);

}
