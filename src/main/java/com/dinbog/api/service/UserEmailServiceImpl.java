package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.models.entity.UserEmail;
import com.dinbog.api.repository.UserEmailRepository;


@Service("UserEmailService")
public class UserEmailServiceImpl implements IUserEmailService {

	@Autowired
	private UserEmailRepository userEmailRepository;

	
	@Override
	public Optional<UserEmail> findByEmail(String email) {
		
		return userEmailRepository.findByEmail(email);
	}


	@Override
	@Transactional
	public void save(UserEmail userEmail) {
		
		userEmailRepository.save(userEmail);
		
	}


	@Override
	public void deleteByUser(Long id) {
		
		userEmailRepository.deleteById(id);
		
	}

	@Override
	public Optional<List<UserEmail>> findByUserId(Long id) {
		
		return userEmailRepository.findByUserId(id);
	}


	@Override
	public UserEmail saveAndFlush(UserEmail userEmail) {
		
		return userEmailRepository.saveAndFlush(userEmail);
	}


	@Override
	public Optional<List<UserEmail>> listLevelEdit(Long userId, Long id) {
		
		return userEmailRepository.listLevelEdit(userId, id);
	}


	@Override
	public Optional<UserEmail> findById(Long id) {
		
		return userEmailRepository.findById(id);
	}


	@Override
	public void deleteById(Long id) {
		
		userEmailRepository.deleteById(id);
		
	}


	@Override
	public Optional<UserEmail> findByToken(String token) {
		
		return userEmailRepository.findByToken(token);
	}

}
