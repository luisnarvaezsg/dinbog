package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.dinbog.api.models.entity.UserEmail;

public interface IUserEmailService {

	Optional<UserEmail> findByEmail(String lowerCase);

	@Transactional
	void save(UserEmail userEmail);

	void deleteByUser(Long id);

	Optional<List<UserEmail>> findByUserId(Long id);

	UserEmail saveAndFlush(UserEmail response);

	Optional<List<UserEmail>> listLevelEdit(Long userId, Long id);

	Optional<UserEmail> findById(Long id);

	void deleteById(Long id);

	Optional<UserEmail> findByToken(String token);

}
