package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.AttachmentCommentDTO;
import com.dinbog.api.dto.AttachmentCommentResponseDTO;
import com.dinbog.api.models.entity.AttachmentComment;
import com.dinbog.api.models.entity.CommentAttachment;

public interface IAttachmentCommentService {

	/**
	 * @param attachmentId
	 * @param request
	 * @return
	 */
	AttachmentComment create(AttachmentCommentDTO request, String lang);

	/**
	 * Trae todos los comments de un attachment
	 * 
	 * @param attachmentId
	 * @return
	 */
	MutableList<AttachmentCommentResponseDTO> getComments(Long attachmentId, String lang);

	/**
	 * Edita un coment
	 * 
	 * @param id
	 * @param value
	 * @return
	 */
	AttachmentComment edit(Long id, String value);

	public AttachmentComment findByCommentAttachent(CommentAttachment commentAttachment, String lang);
	

}