package com.dinbog.api.service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IEmailService {

    String getUrlRegistration(String token);

    String getResetPasswordUrl(HttpServletRequest request, String token);

    Map<String,Object> setConfirmEmailBody(String token);

    Map<String, Object> setRequestResetPassword(HttpServletRequest request, String token);

    Map<String,Object> setResetPasswordSuccess();
}
