package com.dinbog.api.service;

import com.dinbog.api.dto.ProfileShareCreateDTO;
import com.dinbog.api.repository.ProfileShareRepository;
import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.ProfileShare;
import com.dinbog.api.util.Util;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileShareServiceImpl implements IProfileShareService {

	@Autowired
	private ProfileShareRepository profileShareRepository;

	@Autowired
	private IProfileService profileService;

	@Override
	public ProfileShare create(Long profileId, ProfileShareCreateDTO request) {

		try {
			ProfileShare profileShare = new ProfileShare();
			profileShare.setOwnerProfile(profileService.findById(request.getOwnerProfileId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getMessage())))));
			profileShare
					.setProfile(profileService.findById(profileId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));

			return profileShareRepository.save(profileShare);
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileShareService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
