package com.dinbog.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.CommentsPost;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostComment;
import com.dinbog.api.repository.PostCommentRepository;

@Service("PostCommentService")
public class PostCommentServiceImpl implements IPostCommentService {

	@Autowired
	private PostCommentRepository postCommentRepository;

	@Override
	public PostComment create(Post post, CommentsPost commentsPost) {

		PostComment postComment = new PostComment();
		postComment.setCommentsPost(commentsPost);
		postComment.setPost(post);
		postCommentRepository.save(postComment);

		return postComment;
	}

	@Override
	public List<PostComment> findByPost(Post post) {
		
		return postCommentRepository.findByPost(post);
	}

	@Override
	@Transactional
	public void deleteByPost(Post post) {
		
		List<PostComment> listaPC = findByPost(post);
		if ((listaPC != null) && (listaPC.size() > 0)) {
			listaPC.forEach(postcomment -> 	postCommentRepository.delete(postcomment));
		}
	}

}
