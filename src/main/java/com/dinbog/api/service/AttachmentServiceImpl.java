package com.dinbog.api.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.dinbog.api.models.entity.*;
import com.dinbog.api.util.files.FileUtils;
import com.dinbog.api.util.url.UrlParams;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.dinbog.api.dto.AttachmentDTO;
import com.dinbog.api.dto.AttachmentLikeDTO;
import com.dinbog.api.dto.AttachmentReportRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.repository.AttachmentCommentRepository;
import com.dinbog.api.repository.AttachmentLikeRepository;
import com.dinbog.api.repository.AttachmentReportRepository;
import com.dinbog.api.repository.AttachmentRepository;
import com.dinbog.api.repository.AttachmentTypeRepository;
import com.dinbog.api.repository.ParameterRepository;
import com.dinbog.api.repository.ProfileRepository;
import com.dinbog.api.repository.ReportTypeRepository;
import com.dinbog.api.util.PaginateResponse;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AttachmentMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("AttachmentServiceImpl")
public class AttachmentServiceImpl implements IAttachmentService {

	@Autowired
	AttachmentRepository attachmentRepository;

	@Autowired
	AttachmentTypeRepository attachmentTypeRepository;

	@Autowired
	AttachmentLikeRepository attachmentLikeRepository;

	@Autowired
	AttachmentCommentRepository attachmentCommentRepository;

	@Autowired
	AttachmentReportRepository attachmentReportRepository;

	@Autowired
	IPostService postService;
	
	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	ReportTypeRepository reportTypeRepository;

	@Autowired
	ParameterRepository parameterRepository;
	
	@Autowired
	IApiErrorService errorService;

	@Autowired
	FileUtils fileUtils;


	@Value("${image.dir.key}")
	private String imgDirKey;

	@Value("${image.url.key}")
	private String imgUrlKey;


	@Override
	public Attachment create(MultipartFile file, MultipartFile cover, String metadata, String lang, String bucketName) {

		Attachment attachment = new Attachment();

		try {
			String oldPath = file.getOriginalFilename();
			AttachmentType attachmentType = findType(file, lang);
			attachment.setPath ( fileUtils.getFileAttachedName(file, bucketName) );
			attachment.setMetadata(metadata);
			attachment.setAttachmentType(attachmentType);
			attachment.setExtra(oldPath);
			attachment.setPurged(false);
			attachment.setCountAttachment(attachmentRepository.count());
			attachment.setDatePurged(new Date());
			attachment.setCreated(new Date());
			attachment.setStatus(1);

			return attachmentRepository.save(attachment);
		
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Attachment create(String url, String lang) {
		try{
			Attachment attachment = new Attachment();
			attachment.setPath(url);
			attachment.setExtra(url);
			attachment.setAttachmentType( attachmentTypeRepository.findByValue(UrlParams.KEY.getValue()).orElse(null) );
			attachment.setPurged(false);
			attachment.setCountAttachment(attachmentRepository.count());
			attachment.setDatePurged(new Date());
			attachment.setCreated(new Date());
			attachment.setStatus(1);

			return attachmentRepository.save(attachment);

		}catch (Exception e){
			if (Util.isCustomException(e))
				throw e;
			log.error("*** Exception EN [AttachmentService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	@Override
	public void createLike(AttachmentLikeDTO attachmentLikeDTO, String language) {

		
		// VALIDA QUE NO EXISTA LIKE CON MISMA DATA
		Optional<List<AttachmentLike>> valida = attachmentLikeRepository.
							findByProfileAttachment(attachmentLikeDTO.getProfileId(), attachmentLikeDTO.getAttachmentId());
		
		if (!valida.isPresent()) {

			AttachmentLike attachmentLike = new AttachmentLike();
			
			attachmentLike.setProfile(profileRepository.findById(attachmentLikeDTO.getProfileId())
						.orElseThrow(() ->  new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, language)))));
			
			Attachment attachment = new Attachment();
			try {
				Optional<Attachment> optional = attachmentRepository.findById(attachmentLikeDTO.getAttachmentId());
				if(optional.isPresent())
					attachment = optional.get();
			
			}catch(Exception e){
				
				throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND, language)));
			}
			
			
			if(attachment == null) {
				throw new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND, language)));
				
			}else {
				attachment.addLike();
				attachmentLike.setAttachment(attachment);
			}
			
			attachmentLikeRepository.save(attachmentLike);
		
		} else {
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_LIKE_EXISTS, language)));
		}

	}


	@Override
	public void deleteLike(AttachmentLikeDTO attachmentLikeDTO, String language) {

		Optional<List<AttachmentLike>> valida = attachmentLikeRepository.
				findByProfileAttachment(attachmentLikeDTO.getProfileId(), attachmentLikeDTO.getAttachmentId());

		if (valida.isPresent()) {
		
		AttachmentLike attachmentLike = new AttachmentLike();
		
		attachmentLike.setProfile(profileRepository.findById(attachmentLikeDTO.getProfileId())
					.orElseThrow(() ->  new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, language)))));
		
		Optional<Attachment> optional = attachmentRepository.findById(attachmentLikeDTO.getAttachmentId());
		
		Attachment attachment = new Attachment();
		
		if(optional.isPresent())
			attachment = optional.get();
		
		if(attachment == null) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND, language)));
			
		}else {
			attachment.unLike();
			attachmentLike.setAttachment(attachment);
		}
		
		attachmentLikeRepository.delete(attachmentLike);
		
		} else {
			
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_LIKE_NOT_FOUND, language)));
		
		}
	}

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param extra
	 * @return
	 */
	@Override
	public PaginateResponse<AttachmentDTO> readAll(Integer pageNumber, Integer pageSize, String extra) {

		try {
			MutableList<AttachmentDTO> attachmentDTOS = Lists.mutable.empty();

			Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");

			Page<Attachment> page = attachmentRepository.findAll(new Specification<>() {
				private static final long serialVersionUID = -2276935649215910317L;

				@Override
				public Predicate toPredicate(Root<Attachment> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {
					MutableList<Predicate> predicates = Lists.mutable.empty();
					if (!Util.isEmpty(extra)) {
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("extra"), extra)));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}

				
			}, pageable);

			if (page.getTotalElements() > 0L) {
				page.getContent().forEach(i -> attachmentDTOS.add(AttachmentMapper.mapAttachment(i)));

				return new PaginateResponse<>(page.getTotalElements(), page.getTotalPages(), page.getNumber() + 1,
						page.getSize(), attachmentDTOS);
			} else {
				// 404 - NO HAY RESULTADOS
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NO_RESULTS.getCode(),
								ApiErrorEnum.ATTACHMENT_NO_RESULTS.getMessage())));
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public Attachment readById(Long id) {

			return attachmentRepository.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage()))));
	}

	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {

		try {
			attachmentRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public AttachmentReport report(Long id, AttachmentReportRequestDTO request) {

		try {
			AttachmentReport attachmentReport = new AttachmentReport();

			// REPORT TYPE
			attachmentReport.setReportType(reportTypeRepository.findById(request.getReportTypeId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage())))));
			// DETAIL
			attachmentReport.setDetail(request.getDetail());
			// PROFILE
			attachmentReport
					.setOwner(profileRepository.findById(request.getProfileId())
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			// ATTACHMENT
			attachmentReport.setAttachment(attachmentRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage())))));

			return attachmentReportRepository.save(attachmentReport);
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentService.report]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param file
	 */
	/**
	private void storeFile(MultipartFile file) {

		try {
			ParameterDTO parameterDTO = findByParams(imgDirKey);
			Path path = Paths.get(parameterDTO.getValue());
			if (Files.exists(path)) {
				log.info("** La ruta existe **");
				log.info("filename: " + file.getOriginalFilename());
				String fileName = file.getOriginalFilename();
//				Files.copy(file.getInputStream(), Paths.get(imgDirKey + "/" + fileName));
				Files.copy(file.getInputStream(), Paths.get(parameterDTO.getValue() + "/" + fileName));
			} else {
				log.info("**** No existe la ruta ***");
//				File file_directory = new File(imgDirKey);
				File file_directory = new File(parameterDTO.getValue());
				Boolean bool_mkdir = file_directory.mkdir();
				if (bool_mkdir) {
					log.info("ruta de archivos creada");
					storeFile(file);
				} else {
					String error_directory = "*** Error Al Crear la ruta del archivo ***";
					log.error(error_directory);
				}
			}
		} catch (IOException e) {
			// IOException
			log.error("*** IOException EN [AttachmentService.storeFile]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentService.storeFile]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	private ParameterDTO findByParams(String key) {
		ParameterDTO parameterDTO = new ParameterDTO();
		Parameter parameter = parameterRepository.findByKey(key).orNull();
		parameterDTO.setKey(parameter.getKey());
		parameterDTO.setValue(parameter.getValue());
		return parameterDTO;
	}
	**/
	
	@Override
	public AttachmentType findType(MultipartFile file,String lang) {
		
		String value = file.getContentType();
		Optional<String> oValue = Optional.of(value);
		Optional<AttachmentType> optional = Optional.empty();
		
		AttachmentType type = null;
		
		if(oValue.isPresent()) 
			optional = attachmentTypeRepository.findByValue(oValue.get().substring(0, oValue.get().indexOf("/")));
		
			if(optional.isPresent()) {
				type = optional.get();
				return type;
			}else {
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.FILE_TYPE_INVALID, lang)));
			}
	}
	
	@Override
	public String findURL(String attachmentPath) {
		return postService.getPostURL(attachmentPath);
	}

    @Override
	@Transactional
    public void delete(Attachment attachment) {
		List<AttachmentComment> attachmentComments = attachmentCommentRepository.findAllByAttachment(attachment);
		if(!attachmentComments.isEmpty())
			attachmentComments.forEach(attachmentCommentRepository::delete);
		List<AttachmentLike> attachmentLikes = attachmentLikeRepository.findAllByAttachment(attachment);
		if(!attachmentLikes.isEmpty())
			attachmentLikes.forEach(attachmentLikeRepository::delete);
        attachment.setStatus(0);
		attachmentRepository.save(attachment);
    }
    
    /**
	 * @param id
	 */
	@Override
	public Optional<Attachment> findById(Long id) {

		try {
			return attachmentRepository.findById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

}
