package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.models.entity.EventType;

public interface IEventTypeService {

	Optional<EventType> findById(Long eventTypeId, String lang);

}
