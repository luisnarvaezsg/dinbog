package com.dinbog.api.service;

import com.dinbog.api.dto.AlbumAttachmentRequestDTO;
import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.AlbumAttachment;
import com.dinbog.api.models.entity.Attachment;

import java.util.List;
import java.util.Optional;

public interface IAlbumAttachmentService {

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	AlbumAttachment create(Long id, AlbumAttachmentRequestDTO request);

	/**
	 * @param id
	 */
	void deleteByAlbumId(Long id);

	void create(Album album, Attachment attachment);

	void delete(Album album);

	List<AlbumAttachment> findAllByAlbum(Album album);

	void delete(AlbumAttachment albumAttachment);

	Optional<AlbumAttachment> findById(Long id);

	AlbumAttachment save(AlbumAttachment album);


}
