package com.dinbog.api.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.CityDTO;
import com.dinbog.api.dto.CityRequestDTO;
import com.dinbog.api.dto.SortCityByName;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.City;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.CityRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.CityMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("CityServiceImpl")
public class CityServiceImpl implements ICityService {

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private ILanguageService languageService;
	

	/**
	 * @param countryId
	 * @param request
	 * @return
	 */
	@Override
	public City create(Long countryId, CityRequestDTO request) {
		try {
			City city = new City();
			city.setCountryId(request.getCountryId());
			city.setCode(request.getCode());
			city.setLatitude(request.getLatitude());
			city.setLongitude(request.getLongitude());
			city.setStateName(request.getStateName());
			return cityRepository.save(city);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CityService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param id
	 * @param countryId
	 * @param request
	 * @return
	 */
	@Override
	public City update(Long id, Long countryId, CityRequestDTO request) {
		try {
			City city = cityRepository.findById(id).orElseThrow(
					() -> new InvalidRequestException(org.eclipse.collections.api.factory.Lists.mutable.of(new ApiError(
							ApiErrorEnum.CITY_NO_FOUND.getCode(), ApiErrorEnum.CITY_NO_FOUND.getMessage()))));

			city.setCode(request.getCode());

			city.setLatitude(request.getLatitude());

			city.setLongitude(request.getLongitude());

			city.setStateName(request.getStateName());
			return cityRepository.save(city);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CityService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param countryId
	 * @param id
	 */
	@Override
	@Transactional
	public void deleteByCountry(Long countryId, Long id) {
		try {
			cityRepository.deleteByCountry(countryId, id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [CityService.deleteCity]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * @param countryId
	 * @return
	 */
	@Override
	public MutableList<CityDTO> getCities(Long countryId, String langCode) {
		
		langCode = languageService.validLanguage(langCode);

		List<City> cityList = cityRepository.findByCountryIdOrderByStateName(countryId);

		if (cityList.isEmpty()) {
		
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITIES_NO_RESULTS, langCode)));
		
		} else {
			
			MutableList<CityDTO> cityDTOS = Lists.mutable.empty();
			
			for(City i : cityList) {
				
				Traslation traslation =  traslationService.findByKeyAndIsoCode(i.getKey(), langCode);
				
				if (traslation == null) {
					
					i.setStateName(i.getKey());
				
				} else {
					i.setStateName(traslation.getValue());
				}
				cityDTOS.add(CityMapper.mapCity(i));
					
			}
			Collections.sort(cityDTOS, new SortCityByName());

		    return cityDTOS;
		}
	}
	
	@Override
	public Optional<City> findById(Long id, String langCode) {
		
		Optional<City> city = Optional.empty();
		langCode = languageService.validLanguage(langCode);
		
		try {
			
			city = cityRepository.findById(id);
			
			if(city.isPresent()) {
				Traslation traslation =  traslationService.findByKeyAndIsoCode(city.get().getKey(), langCode);
				if(traslation == null) {
					city.get().setStateName(city.get().getKey());	
				}else {
					city.get().setStateName(traslation.getValue());	
				}
			}
		} catch (Exception e) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, langCode)));
		}
		
		return city;
	}
}
