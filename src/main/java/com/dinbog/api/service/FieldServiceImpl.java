package com.dinbog.api.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.FieldDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Field;
import com.dinbog.api.repository.FieldRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.FieldMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * FieldService
 */
@Slf4j
@Service("FieldService")
public class FieldServiceImpl implements IFieldService {

	@Autowired
	private FieldRepository fieldRepository;

	/**
	 * @param fieldId
	 * @return
	 */
	@Override
	public MutableList<FieldDTO> readAll(Long fieldId) {

		try {
			List<Field> fieldList = fieldRepository.findAll(new Specification<Field>() {

				private static final long serialVersionUID = -3453672712416545058L;

				@Override
				public Predicate toPredicate(Root<Field> root, CriteriaQuery<?> criteriaQuery,
						CriteriaBuilder criteriaBuilder) {

					MutableList<Predicate> predicates = Lists.mutable.empty();

					if (!Util.isEmpty(fieldId)) {
						fieldRepository.findById(fieldId).orElse(null);
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("id"), fieldId)));
					}
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			});

			if (fieldList.isEmpty()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.FIELDS_NO_RESULTS.getCode(), ApiErrorEnum.FIELDS_NO_RESULTS.getMessage())));
			} else {
				MutableList<FieldDTO> fieldDTOS = Lists.mutable.empty();
				fieldList.forEach(i -> fieldDTOS.add(FieldMapper.mapField(i)));
				return fieldDTOS;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [FieldService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

}