package com.dinbog.api.service;

import com.dinbog.api.dto.UserStatusDTO;
import com.dinbog.api.dto.UserStatusRequestDTO;
import com.dinbog.api.models.entity.UserStatus;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

public interface IUserStatusService {
	
    UserStatus create(UserStatusRequestDTO request);

    MutableList<UserStatusDTO> readAll();

    UserStatus readById(Long id);

    UserStatus update(Long id, UserStatusRequestDTO request);

    void deleteById(Long id);

	Optional<UserStatus> findById(Long id);
}
