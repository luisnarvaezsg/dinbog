package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.MenuDTO;
import com.dinbog.api.dto.MenuRequestDTO;
import com.dinbog.api.models.entity.Menu;

public interface IMenuService {

	/**
	 * @param request
	 * @return
	 */
	Menu create(MenuRequestDTO request);

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	Menu update(Long id, MenuRequestDTO request);

	/**
	 *
	 * @return
	 */
	MutableList<MenuDTO> readAll();

	/**
	 * @param id
	 */
	void deleteById(Long id);

	Optional<Menu> findById(Long i);

}