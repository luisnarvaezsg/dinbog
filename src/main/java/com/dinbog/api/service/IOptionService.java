package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.OptionDTO;

public interface IOptionService {

	/**
	 * @return
	 */
	MutableList<OptionDTO> readAll();

	/**
	 * @param fieldId
	 * @return
	 */
	MutableList<OptionDTO> readOptionsByFieldId(Long fieldId);

}