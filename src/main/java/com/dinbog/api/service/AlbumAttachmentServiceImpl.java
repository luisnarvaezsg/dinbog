package com.dinbog.api.service;

import com.dinbog.api.models.entity.Album;
import com.dinbog.api.models.entity.Attachment;

import java.util.List;
import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.AlbumAttachmentRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.AlbumAttachment;
import com.dinbog.api.repository.AlbumAttachmentRepository;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlbumAttachmentServiceImpl implements IAlbumAttachmentService {
	
	@Autowired
	private IAlbumService albumService;
	
	@Autowired
	private IAttachmentService attachmentService;
	
	@Autowired
	private AlbumAttachmentRepository albumAttachmentRepository;
	
	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	public AlbumAttachment create(Long id, AlbumAttachmentRequestDTO request) {
		try {
			AlbumAttachment albumAttachment = new AlbumAttachment();
			albumAttachment.setIsCover(request.getIsCover());
			albumAttachment.setAlbum(
					albumService.findById(id).orElseThrow(
							() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
											ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())))
					)
			);
			albumAttachment.setAttachment(
					attachmentService.findById(request.getAttachmentId()).orElseThrow(
							() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
											ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage())))
					)
			);
			return (albumAttachmentRepository.save(albumAttachment));
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumAttachmentService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}


	
	/**
	 * @param id
	 */
	@Override
	@Transactional
	public void deleteByAlbumId(Long id) {
		try {
			albumAttachmentRepository.deleteByAlbumId(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AlbumAttachmentService.deleteByAlbumId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void create(Album album, Attachment attachment) {
		AlbumAttachment albumAttachment = new AlbumAttachment();
		albumAttachment.setAlbum(album);
		albumAttachment.setAttachment(attachment);
		albumAttachmentRepository.save(albumAttachment);
	}



	@Override
	public void delete(Album album) {
		
		albumAttachmentRepository.deleteByAlbumId(album.getId());
		
	}



	@Override
	public List<AlbumAttachment> findAllByAlbum(Album album) {
		
		return albumAttachmentRepository.findAllByAlbum(album);
	}



	@Override
	public void delete(AlbumAttachment albumAttachment) {
		
		albumAttachmentRepository.delete(albumAttachment);
		
	}



	@Override
	public Optional<AlbumAttachment> findById(Long id) {
		
		return albumAttachmentRepository.findById(id);
	}



	@Override
	public AlbumAttachment save(AlbumAttachment album) {
		
		return albumAttachmentRepository.save(album);
	}
}
