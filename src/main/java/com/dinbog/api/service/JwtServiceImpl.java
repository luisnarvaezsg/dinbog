/**
 * 
 */
package com.dinbog.api.service;


import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.security.jwt.JwtProvider;



/**
 * @author Luis
 *
 */
@Service
public class JwtServiceImpl implements IJwtService {

	@Autowired
	private JwtProvider jwtProvider;

	@Override
	public User getUser(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return jwtProvider.getUser(request);
	}

}
