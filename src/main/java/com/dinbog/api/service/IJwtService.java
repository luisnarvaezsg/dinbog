package com.dinbog.api.service;

import com.dinbog.api.models.entity.User;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;

@Service
public interface IJwtService {
	public User getUser(HttpServletRequest request);
}
