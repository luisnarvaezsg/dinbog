package com.dinbog.api.service;

import com.dinbog.api.dto.AlbumReportCreateDTO;
import com.dinbog.api.models.entity.AlbumReport;

public interface IAlbumReportService {

	AlbumReport create(Long albumId, AlbumReportCreateDTO request);

}