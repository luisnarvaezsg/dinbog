package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.repository.ProfileCategoryTypeRepository;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.ProfileCategoryTypeDTO;
import com.dinbog.api.dto.ProfileCategoryTypeRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.ProfileCategoryType;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileCategoryTypeMapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class ProfileCategoryTypeServiceImpl implements IProfileCategoryTypeService {
	
	@Autowired
	private ProfileCategoryTypeRepository profileCategoryTypeRepository;
	
	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private IProfileTypeService profileTypeService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private ILanguageService languageService;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public ProfileCategoryType create(ProfileCategoryTypeRequestDTO request) {
		try {
			ProfileCategoryType profileCategoryType = new ProfileCategoryType();
			
			profileCategoryType.setProfileType(profileTypeService.findById(request.getProfileTypeId())
					.orElseThrow(
							() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getMessage())))
					));
			
			if (!Util.isEmpty(request.getParentId())) {
				profileCategoryType.setParent(
						profileCategoryTypeRepository.findById(request.getParentId()).orElseThrow(
								() -> new InvalidRequestException(
										Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND.getCode(),
												ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND.getMessage())))
						)
				);
			}
			
			profileCategoryType.setValue(request.getValue());
			
			return profileCategoryTypeRepository.save(profileCategoryType);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileCategoryTypeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	/**
	 * @param id
	 * @param request
	 * @return
	 */
	@Override
	@Transactional
	public ProfileCategoryType update(Long id, ProfileCategoryTypeRequestDTO request) {
		try {
			ProfileCategoryType profileCategoryType = profileCategoryTypeRepository.findById(id).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND.getMessage())))
			);
			
			if (!Util.isEmpty(request.getParentId())) {
				profileCategoryType.setParent(
						profileCategoryTypeRepository.findById(request.getParentId()).orElseThrow(
								() -> new InvalidRequestException(
										Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND.getCode(),
												ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND.getMessage())))
						)
				);
			}
			
			profileCategoryType.setProfileType(profileTypeService.findById(request.getProfileTypeId()).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_TYPE_NOT_FOUND.getMessage())))
			));
			
			profileCategoryType.setValue(request.getValue());
						
			return profileCategoryTypeRepository.save(profileCategoryType);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileCategoryTypeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
	
	/**
	 * @param id
	 */
	@Override
	public void deleteById(Long id) {
		try {
			profileCategoryTypeRepository.deleteById(id);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileCategoryTypeService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public MutableList<ProfileCategoryTypeDTO> findByLevelWithProfileType(Long level, Long id, String lang, boolean childs) {
		
		lang = languageService.validLanguage(lang);
		
		List<ProfileCategoryType> profileCategoryTypes =  profileCategoryTypeRepository.findByLevelWithProfileType(level, id);
		
		if (profileCategoryTypes.isEmpty()) {
			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_NO_RESULTS, lang)));
		} else {
			MutableList<ProfileCategoryTypeDTO> profileCategoryTypeDTOS = Lists.mutable.empty();
			for(ProfileCategoryType profileCategoryType : profileCategoryTypes) {
				
				Traslation traslation =  traslationService.findByKeyAndIsoCode(profileCategoryType.getKey(), lang);
				if(traslation == null) {
					profileCategoryType.setValue(profileCategoryType.getKey());
				}else {
					profileCategoryType.setValue(traslation.getValue());
				}
				
				if(childs) {
					for(ProfileCategoryType profileCategoryTypeChild : profileCategoryType.getChild()) {
						traslation =  traslationService.findByKeyAndIsoCode(profileCategoryTypeChild.getKey(), lang);
						if(traslation == null) {
							profileCategoryTypeChild.setValue(profileCategoryTypeChild.getKey());
						}else {
							profileCategoryTypeChild.setValue(traslation.getValue());
						}
					}
				}
				profileCategoryTypeDTOS.add(ProfileCategoryTypeMapper.mapProfileCategoryType(profileCategoryType,childs));
			}
			return profileCategoryTypeDTOS;
		}
	}

	@Override
	public Optional<ProfileCategoryType>  findById(Long id) {
		
		return profileCategoryTypeRepository.findById(id); 
	}
	
	@Override
	public void traslateProfileCategoryType(ProfileCategoryType profileCategory, String lang) {
		
		lang = languageService.validLanguage(lang);
		
		Traslation traslation =  traslationService.findByKeyAndIsoCode(profileCategory.getKey(), lang);
		
		if(traslation == null) {
			profileCategory.setValue(profileCategory.getKey());
		}else {
			profileCategory.setValue(traslation.getValue());
		}

	}
}
