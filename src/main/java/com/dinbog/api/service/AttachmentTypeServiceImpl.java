package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.AttachmentTypeBasicDTO;
import com.dinbog.api.dto.AttachmentTypeDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.AttachmentType;
import com.dinbog.api.repository.AttachmentTypeRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AttachmentTypeMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("AttachmentTypeServiceImpl")
public class AttachmentTypeServiceImpl implements IAttachmentTypeService {

	@Autowired
	private AttachmentTypeRepository attachmentTypeRepository;

	/**
	 *
	 * @return
	 */
	@Override
	public MutableList<AttachmentTypeDTO> readAll() {

		try {
			MutableList<AttachmentTypeDTO> attachmentTypeDTOS = Lists.mutable.empty();
			attachmentTypeRepository.findAll()
					.forEach(i -> attachmentTypeDTOS.add(AttachmentTypeMapper.mapAttachmentType(i)));

			if (attachmentTypeDTOS.isEmpty()) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_TYPE_NO_RESULTS.getCode(),
								ApiErrorEnum.ATTACHMENT_TYPE_NO_RESULTS.getMessage())));
			} else {
				return attachmentTypeDTOS;
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeService.readAll]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public AttachmentType create(AttachmentTypeBasicDTO request) {
		try {

			AttachmentType attachmentType = new AttachmentType();
			attachmentType.setValue(request.getValue());

			return attachmentTypeRepository.save(attachmentType);
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public AttachmentType patchById(Long id, AttachmentTypeBasicDTO request) {
		try {
			Optional<AttachmentType> attachmentType = attachmentTypeRepository.findById(id);

			if (! attachmentType.isPresent()) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_TYPE_NOT_FOUND.getCode(),
								ApiErrorEnum.ATTACHMENT_TYPE_NOT_FOUND.getMessage())));
			} else {
				attachmentType.get().setValue(request.getValue());
				return attachmentTypeRepository.save(attachmentType.get());
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeService.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		try {

			attachmentTypeRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_TYPE_NOT_FOUND.getMessage()))));

			attachmentTypeRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentTypeService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
