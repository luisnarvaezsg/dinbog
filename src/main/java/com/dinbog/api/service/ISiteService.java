package com.dinbog.api.service;

import com.dinbog.api.dto.SiteCreateDTO;
import com.dinbog.api.dto.SiteDTO;
import com.dinbog.api.models.entity.Site;
import org.eclipse.collections.api.list.MutableList;

public interface ISiteService {
    MutableList<SiteDTO> readAll();

    Site create(SiteCreateDTO request);

    void patchById(Long id, SiteCreateDTO request);

    void deleteById(Long id);
}
