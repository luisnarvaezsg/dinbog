package com.dinbog.api.service;
import java.util.List;
import java.util.Optional;
import java.util.Random;


import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.commons.ProfileConstant;
import com.dinbog.api.dto.ProfileFollowBasicDTO;
import com.dinbog.api.dto.ProfileRequestDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileCategory;
import com.dinbog.api.models.entity.ProfileCountry;
import com.dinbog.api.models.entity.ProfileTalent;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.ProfileTypeRepository;
import com.dinbog.api.repository.ProfileTalentRepository;
import com.dinbog.api.util.mapper.ProfileMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileTalentServiceImpl implements IProfileTalentService {

	@Autowired
	private ProfileTypeRepository profileTypeRepository;
	
	@Autowired
	private ProfileTalentRepository profileTalentRepository;
	
	@Autowired
	private IProfileFollowService profileFollowService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IGenderService genderService;

	@Autowired
	private IMembershipService membershipService;

	@Autowired
	private ICountryService countryService;
	
	@Autowired
	private ICityService cityService;

	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private IProfileCategoryService profileCategoryService;
	
	@Autowired
	private IProfileCategoryTypeService categoryTypeService;
	
	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private IProfileCountryTypeService profileCountryTypeService;


	private String generateUrlName(User user) {
		
		String firstName = user.getFirstName().toLowerCase().trim().replaceAll(" {1,}",".");
		String lastName = user.getLastName().toLowerCase().trim().replaceAll(" {1,}",".");
		
		String urlName = firstName.concat(".").concat(lastName);
				
		while(profileService.countProfileByUrlName(urlName) > 0) {

			Random random = new Random();
			Integer prefix = random.nextInt(9999);
			urlName = urlName.concat(prefix.toString());
		}
		
		return urlName;
		
	}
	
	@Override
	@Transactional()
	public ProfileTalent update(ProfileRequestDTO request, String language) {
		
		//Search Profile:
		ProfileTalent profileTalent = this.readById(request.getProfileId(), language);
		
		if (profileTalent.getId() < 1) {
			throw new ResourceNotFoundException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, language)));
		}
		
		profileTalent.setBirthDate(request.getBirthDate());
		profileTalent.getProfile().setSearchableSince(request.getSearchableSince());
		profileTalent.getProfile().setIsSearchable(request.getIsSearchable());
		profileTalent.getProfile().setAvatarPath(request.getAvatarPath());
		profileTalent.getProfile().setCoverPath(request.getCoverPath());
		profileTalent.getProfile().setPhoneNumber(request.getPhoneNumber());
		
		// MEMBERSHIP
		profileTalent.getProfile().setMembership(membershipService.findById(request.getMembershipId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.MEMBERSHIP_NOT_FOUND, language)))));
		// GENDER
		profileTalent.setGender(
				genderService.findById(request.getGenderId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.GENDER_NOT_FOUND, language)))));
		
		//DELETE CATEGORIES
		profileTalent.getProfileCountries();
		int countriesSize = profileTalent.getProfileCountries().size();
		
		for(int i=0;i<countriesSize;i++)  {

			profileTalent.removeProfileCountries(profileTalent.getProfileCountries().get(0));
	    
		}
		//BORNCOUNTRY COUNTRY AND CITY
		ProfileCountry bornCountry = new ProfileCountry();
		bornCountry.setCountry(countryService.findById(request.getBornCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
		bornCountry.setCity(cityService.findById(request.getBornCityId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		bornCountry.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_ORIGIN_TYPE)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		bornCountry.setProfile(profileTalent.getProfile());
		profileTalent.addProfileCountries(bornCountry);
		
		
		//RESIDENCE COUNTRY AND CITY
		ProfileCountry residenceCountry = new ProfileCountry();
		residenceCountry.setCountry(countryService.findById(request.getResidenceCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
		residenceCountry.setCity(cityService.findById(request.getResidenceCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		residenceCountry.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_RESIDENCE_TYPE)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		residenceCountry.setProfile(profileTalent.getProfile());
		profileTalent.addProfileCountries(residenceCountry);
		
		
		profileTalent.getProfile().getProfileCategories();
		//DELETE CATEGORIES
		int listSize = profileTalent.getProfile().getProfileCategories().size();
		
		for(int i=0;i<listSize;i++)  {

			profileTalent.getProfile().removeProfileCategories(profileTalent.getProfile().getProfileCategories().get(0));
	    
		}
		
		//INSERT NEWS CATEGORIES
		for (String catId : request.getCategories()) {
			
			ProfileCategory profileCategory = new ProfileCategory();
			profileCategory.setProfile(profileTalent.getProfile());
			
			profileCategory.setProfileCategoryType(categoryTypeService.findById(Long.valueOf(catId))
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND, language)))));
			
			profileTalent.getProfile().addProfileCategories(profileCategory);
		}
		
		
		return profileTalent;
	}
	
	@Override
	@Transactional()
	public ProfileTalent createTalentProfile(ProfileRequestDTO request, String language) {

		// CONSTRUYE ENTITY PROFILE
		ProfileTalent profileTalent = ProfileMapper.requestToEntityTalent(request);
		
		// USER
		profileTalent.getProfile().setUser(userService.findById(request.getUserId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.USER_NOT_FOUND, language)))));
		
		profileTalent.getProfile().setUrlName(generateUrlName(profileTalent.getProfile().getUser()));
		profileTalent.setFirstName(profileTalent.getProfile().getUser().getFirstName());
		profileTalent.setLastName(profileTalent.getProfile().getUser().getLastName());
		String fullName = profileTalent.getProfile().getUser().getFirstName().concat(" ").concat(profileTalent.getProfile().getUser().getLastName());
		profileTalent.getProfile().setFullName(fullName);
		
		if (profileService.countProfileByUrlName(request.getUrlName()) > 0) {
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_URL_NAME_ISSET, language)));
		}
		// MEMBERSHIP
		profileTalent.getProfile().setMembership(membershipService.findById(request.getMembershipId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.MEMBERSHIP_NOT_FOUND, language)))));
		// GENDER
		profileTalent.setGender(
				genderService.findById(request.getGenderId())
						.orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.GENDER_NOT_FOUND, language)))));
		// PROFILE TYPE
		profileTalent.getProfile().setProfileType(profileTypeRepository.findById(request.getProfileTypeId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_TYPE_NOT_FOUND, language)))));
		
		//BORNCOUNTRY COUNTRY AND CITY
		ProfileCountry bornCountry = new ProfileCountry();
		bornCountry.setCountry(countryService.findById(request.getBornCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
		bornCountry.setCity(cityService.findById(request.getBornCityId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		bornCountry.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_ORIGIN_TYPE)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		bornCountry.setProfile(profileTalent.getProfile());
		profileTalent.addProfileCountries(bornCountry);
		
		
		//RESIDENCE COUNTRY AND CITY
		ProfileCountry residenceCountry = new ProfileCountry();
		residenceCountry.setCountry(countryService.findById(request.getResidenceCountryId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COUNTRY_NOT_FOUND, language)))));
		residenceCountry.setCity(cityService.findById(request.getResidenceCityId(), language)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		residenceCountry.setProfileCountryType(profileCountryTypeService.findById(ProfileConstant.COUNTRY_RESIDENCE_TYPE)
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.CITY_NOT_FOUND, language)))));
		residenceCountry.setProfile(profileTalent.getProfile());
		profileTalent.addProfileCountries(residenceCountry);
		
		
		//SAVE CATEGORIES
		for (String catId : request.getCategories()) {
			
			ProfileCategory profileCategory = new ProfileCategory();
			profileCategory.setProfile(profileTalent.getProfile());
			
			profileCategory.setProfileCategoryType(categoryTypeService.findById(Long.valueOf(catId))
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_CATEGORY_TYPE_NOT_FOUND, language)))));
			
			profileTalent.getProfile().addProfileCategories(profileCategory);
		}
		
		profileTalent = profileTalentRepository.save(profileTalent);
		profileTalent.getProfile().getProfileCategories();
		
		return profileTalent;
	}

	@Override
	@Transactional(readOnly = true)
	public ProfileTalent readById(Long id, String lang) {
		
		log.info("Valor a buscar id " + id.toString());
		Optional<ProfileTalent> profile = Optional.empty();
		
		try {
			
			profile = profileTalentRepository.findByProfile(id);
		
		}catch(Exception e) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}
		
		if(!profile.isPresent()) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)));
		}
		return profile.get();
	}

	@Override
	public Optional<ProfileTalent> findByProfile(Profile profile) {
		
		return profileTalentRepository.findByProfile(profile);
	}

	@Override
	public Page<Profile> findByFullNameContainingIgnoreCase(Profile profile, Pageable pageable, String name) {
		
		return profileTalentRepository.findByFullNameContainingIgnoreCase(profile.getId(), pageable, name);
	}

	@Override
	public Page<Profile> findByFullNameCompanyContainingIgnoreCase(Profile profile, Pageable pageable, String name) {
		
		return profileTalentRepository.findByFullNameCompanyContainingIgnoreCase(profile.getId(), pageable, name);
	}

	@Override
	public ProfileFollowBasicDTO mapProfileFollow(Profile profileOwner, Profile p, String lang) {
		List<ProfileCategory> listaCat=profileCategoryService.findByProfileId(p.getId(), lang);
		return (ProfileMapper.mapProfileFollowBasic(p,
				profileFollowService.findByOwnerProfileAndProfile(profileOwner, p).isPresent(),listaCat));
	}


}
