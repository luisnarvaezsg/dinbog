package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.GroupMenuDTO;
import com.dinbog.api.dto.GroupMenuRequestDTO;

public interface IGroupMenuService {

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	MutableList<GroupMenuDTO> create(Long id, GroupMenuRequestDTO request);

	/**
	 * @param id
	 * @return
	 */
	MutableList<GroupMenuDTO> readByGroupId(Long id);

	/**
	 * @param id
	 */
	void deleteByGroup(Long id);

}