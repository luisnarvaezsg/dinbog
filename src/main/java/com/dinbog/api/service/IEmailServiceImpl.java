package com.dinbog.api.service;

import com.dinbog.api.repository.ParameterRepository;
import com.dinbog.api.util.ParameterEnum;
import com.dinbog.api.util.email.EmailKeys;
import com.dinbog.api.util.email.EmailLabels;
import com.dinbog.api.util.email.EmailTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
public class IEmailServiceImpl implements IEmailService {

    private ParameterRepository parameterRepository;

    @Autowired
    public IEmailServiceImpl(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    @Override
    public String getUrlRegistration(String token) {
        return  parameterRepository.findByKey(ParameterEnum.URL_NEW_USER.getKey()).get().getValue().concat(token);
    }

    @Override
    public String getResetPasswordUrl(HttpServletRequest request, String token) {
        return request.getRequestURL().toString().replaceAll(request.getRequestURI(),request.getContextPath()).concat("/v1/password/confirm-password/").concat(token);
    }

    @Override
    public Map<String, Object> setConfirmEmailBody(String token) {
        Map<String,Object> bodyMap = new HashMap<>();
        bodyMap.put(EmailKeys.LABEL.getKey(), EmailLabels.COMFIRM.getLabel());
        bodyMap.put(EmailKeys.URL.getKey(), getUrlRegistration(token));
        bodyMap.put(EmailKeys.TEMPLATE.getKey(), EmailTemplates.CONFIRM.getTemplate());
        return bodyMap;
    }

    @Override
    public Map<String, Object> setRequestResetPassword(HttpServletRequest request, String token) {
        Map<String,Object> bodyMap = new HashMap<>();
        bodyMap.put(EmailKeys.URL.getKey(), getResetPasswordUrl( request, token) );
        bodyMap.put(EmailKeys.TEMPLATE.getKey(), EmailTemplates.PASSWORD_RESET.getTemplate());
        return bodyMap;
    }

    @Override
    public Map<String, Object> setResetPasswordSuccess() {
        Map<String,Object> bodyMap = new HashMap<>();
        bodyMap.put(EmailKeys.TEMPLATE.getKey(), EmailTemplates.PASS_RESET_SUCCESS.getTemplate());
        return bodyMap;
    }
}
