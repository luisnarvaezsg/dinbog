package com.dinbog.api.service;


import java.util.List;

import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostAttachment;

public interface IPostAttachmentService {

	/**
	 * @param request
	 * @return
	 */
	PostAttachment create(Long postId, Long attachmentId);
	
	PostAttachment create(Post post, Attachment attachment);

	List<PostAttachment> findByPost(Post post);
	
	void deleteByPost(Post post);

	void deleteByPostAttachment(PostAttachment postAttach);
	
	PostAttachment save(PostAttachment posta);

}