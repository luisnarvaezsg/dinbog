package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.repository.ProfileLikeRepository;
import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.ProfileLike;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional
@Service
public class ProfileLikeServiceImpl implements IProfileLikeService {

	@Autowired
	private ProfileLikeRepository profileLikeRepository;

	@Autowired
	private IApiErrorService errorService;

	@Override
	public ProfileLike getProfileLikeById(Long id) {

		try {

			return profileLikeRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_LIKE_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_LIKE_NOT_FOUND.getMessage()))));
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileLikeService.getProfileLikeById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void delete(Profile profileOwner, Profile profile, String lang) {
		
		try {
			
			Optional<ProfileLike> pf = profileLikeRepository.findByProfileOwnerAndProfile(profileOwner, profile);
			
			if (!pf.isPresent()) {
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_LIKE_NOT_FOUND, lang)));
						
			} else {
				profileLikeRepository.delete(pf.get());
			}
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileLikeService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void create(Profile profileOwner, Profile profile, String lang) {
		
		try {
		
			Optional<ProfileLike> pf = profileLikeRepository.findByProfileOwnerAndProfile(profileOwner, profile);
			
			if (pf.isPresent()) {
				throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_LIKE_FOUND, lang)));
						
			} else {
				ProfileLike profileLike = new ProfileLike();
				profileLike.setProfileOwner(profileOwner);
				profileLike.setProfile(profile);
				profileLikeRepository.save(profileLike);
			}
			
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileLikeService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Optional<ProfileLike> findByOwnerAndProfile(Profile profileOwner, Profile profile) {
		
		return profileLikeRepository.findByProfileOwnerAndProfile(profileOwner, profile);
	}

}
