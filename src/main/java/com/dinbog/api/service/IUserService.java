package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.ConfirmEmailDTO;
import com.dinbog.api.dto.UserCreateDTO;
import com.dinbog.api.dto.UserCreateResponseDTO;
import com.dinbog.api.dto.UserDTO;
import com.dinbog.api.dto.UserEditDTO;
import com.dinbog.api.dto.UserEmailDTO;
import com.dinbog.api.dto.UserEmailRequestDTO;
import com.dinbog.api.dto.UserPatchPasswordDTO;
import com.dinbog.api.dto.UserStatusDTO;
import com.dinbog.api.dto.UserStatusRequestDTO;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.models.entity.UserEmail;
import com.dinbog.api.models.entity.UserStatus;
import com.dinbog.api.util.PaginateResponse;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.eclipse.collections.api.list.MutableList;

public interface IUserService {

	@Transactional
	UserCreateResponseDTO create(UserCreateDTO request, String lang);

	/**
	 * Manda una lista de usuarios paginada. Permite filtrar (opcional) si los
	 * usuarios son activos o no
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param active
	 * @return
	 */
	PaginateResponse<UserDTO> getAll(Integer pageNumber, Integer pageSize, Integer statusId);

	/**
	 * Busca un usuario por su id
	 *
	 * @param id
	 * @return
	 */
	UserDTO getById(Long id);

	/**
	 * Busca un usuario por su email
	 *
	 * @param email
	 * @return
	 */
	UserDTO getByEmail(String email);

	/**
	 * @param request
	 * @return
	 */
	UserDTO update(Long userId, UserEditDTO request);

	/**
	 * Indica si un email existe o no asociado a un usuario
	 * 
	 * @param email
	 * @return
	 */
	boolean userEmailExists(String email);

	/**
	 * @param userId
	 * @param request
	 * @return
	 */
	UserEmail createEmail(Long userId, UserEmailRequestDTO request);

	/**
	 * @param userId
	 * @param id
	 * @param level
	 * @return
	 */
	List<UserEmailDTO> editEmail(Long userId, Long id, Integer level);

	/**
	 * Trae los emails de un usuario
	 * 
	 * @param userId
	 * @return
	 */
	List<UserEmailDTO> getEmails(Long userId);

	/**
	 * @param id
	 */
	void deleteEmail(Long id);

	/**
	 * Actualiza el password de un usuario
	 *
	 * @param request
	 */
	void patchPassword(UserPatchPasswordDTO userPatchPasswordDTO, HttpServletRequest request,String lang);

	/**
	 * Deja en blanco el password de un usuario (reset)
	 *
	 * @param request
	 */
	void resetPassword(User user, String email, HttpServletRequest request);

	/**
	 * Borra un usuario por su id
	 *
	 * @param id
	 * @return
	 */
	void deleteById(Long id);

	/**
	 *
	 * @param userId
	 */
	void deleteEmailByUserId(Long userId);

	Optional<UserEmail> findByToken(String token);

	Optional<User> findById(Long id);

	User findByEmail(String email);

	Optional<User> findUserByIdAndPasswordNull(Long id);
	
	ConfirmEmailDTO confirmEmailToken(String token);
	
	UserStatus createUserStatus(UserStatusRequestDTO request);
	
	MutableList<UserStatusDTO> readAllUserStatus();
	
	UserStatus readUserStatusById(Long id);
	
	UserStatus updateUserStatus(Long id, UserStatusRequestDTO request);
	
	void deleteUserStatusById(Long id);
}
