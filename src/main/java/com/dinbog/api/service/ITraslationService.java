package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.TraslationDTO;
import com.dinbog.api.models.entity.Traslation;

public interface ITraslationService {

	Traslation findByKeyAndIsoCode(String key, String isoCode);

	MutableList<TraslationDTO> findByIsoCode(String isoCode);

}