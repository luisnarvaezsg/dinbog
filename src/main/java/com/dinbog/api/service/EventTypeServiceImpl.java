package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.EventType;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.EventTypeRepository;

@Service("EventTypeService")
public class EventTypeServiceImpl implements IEventTypeService {

	@Autowired
	private EventTypeRepository eventTypeRepository;
	
	@Autowired
	private ILanguageService languageService;
	
	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private IApiErrorService errorService;

	@Override
	public Optional<EventType> findById(Long eventTypeId, String lang) {
		
		Optional<EventType> eventType = Optional.empty();
		
		lang = languageService.validLanguage(lang);
		
		try {
			
			eventType = eventTypeRepository.findById(eventTypeId);
			
			if(eventType.isPresent()) {
				
				Traslation traslation =  traslationService.findByKeyAndIsoCode(eventType.get().getKey(), lang);
				if(traslation == null) {
					eventType.get().setValue(eventType.get().getKey());	
				}else {
					eventType.get().setValue(traslation.getValue());
				}
			}
			
		} catch (Exception e) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EVENT_TYPE_NOT_FOUND, lang)));
		}
		
		return eventType;
	}

}
