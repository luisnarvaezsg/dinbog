package com.dinbog.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.CommentsPostDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.BadCredentialsException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.CommentsPost;
import com.dinbog.api.models.entity.CommentsPostLike;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostComment;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.CommentsPostLikeRepository;
import com.dinbog.api.repository.CommentsPostRepository;
import com.dinbog.api.util.mapper.CommentsPostMapper;

@Service("CommentServiceImpl")
public class CommentServiceImpl implements ICommentService {

	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private IPostService iPostService;

	@Autowired
	private CommentsPostRepository commentRepository;

	@Autowired
	private CommentsPostLikeRepository commentLikeRepository;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private IJwtService  iJwtService;
	
	@Override
	public CommentsPost create(Long id, String comentario, User user, String lang) {

		CommentsPost comment = new CommentsPost();

		CommentsPost parent = commentRepository.findById(id).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(new ApiError(ApiErrorEnum.COMMENT_NOT_FOUND.getCode(),
						ApiErrorEnum.COMMENT_NOT_FOUND.getMessage()))));
		comment.setCreated(new Date());
		comment.setCountLike(0);
		comment.setParent(parent);
		comment.setProfile(profileService.findByUser(user).orElseThrow(
				() -> new BadCredentialsException(Lists.mutable.of(new ApiError(ApiErrorEnum.USER_NOT_FOUND.getCode(),
						ApiErrorEnum.USER_NOT_FOUND.getMessage())))));
		comment.setValue(comentario);
		commentRepository.save(comment);
		return comment;
	}

	@Override
	public CommentsPostLike commentLike(Long id, User user, String lang) {

		CommentsPostLike cpl = new CommentsPostLike();
		CommentsPost comment = commentRepository.findById(id).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang))));
		Profile profile = profileService.findByUser(user).orElseThrow(() -> new BadCredentialsException(Lists.mutable
				.of(errorService.getApiError(ApiErrorEnum.USER_NOT_FOUND, lang))));
		
		List<CommentsPostLike> lista = commentLikeRepository.findByCommentsPostAndProfile(comment, profile);
		if (!lista.isEmpty())
			throw new InvalidRequestException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_LIKE_EXIST, lang)));
		cpl.setProfile(profile);
		cpl.setCommentsPost(comment);
		cpl.setCreated(new Date());
		if (comment.getCountLike() == null)
			comment.setCountLike(0);
		comment.setCountLike(comment.getCountLike() + 1);
		commentRepository.save(comment);
		commentLikeRepository.save(cpl);
		
		return cpl;
	}

	@Override
	public void commentDisLike(Long id, String lang) {

		CommentsPost commentsPost = commentRepository.findById(id).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.COMMENT_NOT_FOUND, lang))));
		List<CommentsPostLike> lista = commentLikeRepository.findByCommentsPost(commentsPost);
		if (! lista.isEmpty()) {
			commentLikeRepository.delete(lista.get(0));
			commentsPost.setCountLike(commentsPost.getCountLike() - 1);
		}

	}
	
	@Override
	public List<CommentsPostLike> findByCommentsPost(CommentsPost comment)
	{
		return commentLikeRepository.findByCommentsPost(comment);
	}

	@Override
	public void save(CommentsPost comment) {
		
		commentRepository.save(comment);
	}
	
	@Override
	public void deleteComments(CommentsPost commentPost) {
		List<CommentsPostLike> listaLike=commentPost.getListaLike();
		List<CommentsPost> listaComment=commentPost.getListaComment();
		if ((listaLike!=null)&& !listaLike.isEmpty()) {
			listaLike.forEach(commentLike-> commentLikeRepository.delete(commentLike) );
		}
		if ((listaComment!=null)&& !listaComment.isEmpty()) {
			listaComment.forEach(comment-> commentRepository.delete(comment) );
		}
		commentRepository.delete(commentPost);
	}
	
	@Override
	@Transactional
	public MutableList<CommentsPostDTO> commentsByPost(Long id, HttpServletRequest request, String lang, int pageNumber, int pageSize) {
		
		User user = iJwtService.getUser(request);
		Post post=iPostService.findById(id, lang).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.POST_NOT_FOUND, lang))));
		Profile profile = profileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
				Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc("created")));
		Page<PostComment> listaPostComments = new PageImpl<>(post.getListaPostComment(), pageable, post.getListaPostComment().size());
		List<Profile> listaProf = new ArrayList<>();
		if (listaPostComments!= null && !(listaPostComments.isEmpty()) ) {
			listaProf = iPostService.listaMention(listaPostComments.toList());
		}
		
		return CommentsPostMapper.mapPostCommentsList(listaPostComments.toList(), profile, listaProf);
	}


	

	
}
