package com.dinbog.api.service;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.vo.Email;
import com.dinbog.api.util.email.LogEmailService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class SenderServiceImpl implements SenderService {

    private AsyncEmailService emailService;

    @Autowired
    public SenderServiceImpl(AsyncEmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    @SneakyThrows
    @JmsListener(destination = "email.created")
    public void senderMessage(@Payload Email email) {
        try {
            emailService.sendEmail(email);
        }catch (Exception ex){
            LogEmailService.errorLogin(ex);
           throw new InternalErrorException(ex);
        }
    }
}
