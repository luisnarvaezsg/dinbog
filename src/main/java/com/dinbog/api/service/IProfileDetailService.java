package com.dinbog.api.service;

import com.dinbog.api.models.entity.ProfileDetail;

import java.util.List;


public interface IProfileDetailService {
	List<ProfileDetail> getBasicDetails(Long profileId);
}
