package com.dinbog.api.service;

import java.util.List;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.Post;
import com.dinbog.api.models.entity.PostAttachment;
import com.dinbog.api.repository.PostAttachmentRepository;

@Service("PostAttachmentService")
public class PostAttachmentServiceImpl implements IPostAttachmentService {

	@Autowired
	private PostAttachmentRepository postAttachmentRepository;

	@Autowired
	private IPostService postService;

	@Autowired
	private IAttachmentService attachmentService;

	@Override
	public PostAttachment create(Post post, Attachment attachment) {
		
		PostAttachment postAttachment = new PostAttachment();
		postAttachment.setAttachment(attachment);
		postAttachment.setPost(post);
		postAttachmentRepository.save(postAttachment);
		
		return postAttachment;
	}
	
	
	/**
	 * @return
	 */
	@Override
	public PostAttachment create(Long postId, Long attachmentId) {

		PostAttachment postAttachment = new PostAttachment();

			postAttachment
					.setPost(postService.findById(postId, "EN")
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
											ApiErrorEnum.POST_NOT_FOUND.getMessage())))));

			postAttachment.setAttachment(attachmentService.findById(attachmentId)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage())))));

			return postAttachmentRepository.save(postAttachment);

		
	}

	/**
	 * @param id
	 */
	/**@Override
	@Transactional
	public void deleteByPostId(Long id) {

		try {
			postAttachmentRepository.deleteByPostId(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostAttachmentService.deleteByPostId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}**/
	
	@Override
	public void deleteByPostAttachment(PostAttachment postAttach) {
		postAttachmentRepository.delete(postAttach);
	}


	@Override
	public List<PostAttachment> findByPost(Post post) {
		
		return postAttachmentRepository.findByPost(post);
	}


	@Override
	@Transactional
	public void deleteByPost(Post post) {
		
		List<PostAttachment> listaPA=findByPost(post);
		if ( listaPA!=null && !listaPA.isEmpty() )
			listaPA.forEach(postAttachmentRepository::delete);
	}


	@Override
	public PostAttachment save(PostAttachment posta) {
		return postAttachmentRepository.save(posta);
	}


}
