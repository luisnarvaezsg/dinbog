/**
 * 
 */
package com.dinbog.api.service;

import java.util.Objects;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.AttachmentCommentDTO;
import com.dinbog.api.dto.AttachmentCommentResponseDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentComment;
import com.dinbog.api.models.entity.CommentAttachment;
import com.dinbog.api.repository.AttachmentCommentRepository;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.AttachmentCommentMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 *
 */
@Slf4j
@Service
@Transactional
public class AttachmentCommentServiceImpl implements IAttachmentCommentService {

	@Autowired
	private AttachmentCommentRepository attachmentCommentRepository;

	@Autowired
	private ICommentAttachmentService commentAttachmentService;
	
	@Autowired
	private IAttachmentService attachmentService;

	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private IApiErrorService errorService;

	/**
	 * @param attachmentId
	 * @param request
	 * @return
	 */
	@Override
	public AttachmentComment create(AttachmentCommentDTO request, String lang) {

		CommentAttachment commentAttachment = new CommentAttachment();
			
		//SAVE THE COMMENT 
		commentAttachment.setProfile(profileService.findById(request.getProfileId())
						 .orElseThrow(() -> new InvalidRequestException(
								Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang)))));
		commentAttachment.setValue(request.getValue());
		
		if(!Objects.isNull(request.getParentId())) {
			
			commentAttachment.setParent(commentAttachmentService.findById(request.getParentId())
					 .orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(errorService.getApiError(ApiErrorEnum.ATTACHMENT_COMMENT_NOT_FOUND, lang)))));
			
		}
		
		commentAttachmentService.save(commentAttachment);
		
		//SAVE ATTACHMENT-COMMENT RELATION
		AttachmentComment attachmentComment = new AttachmentComment();
		attachmentComment.setCommentAttachment(commentAttachment);
		
		attachmentComment.setAttachment(attachmentService.findById(request.getAttachmentId())
				.orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_NOT_FOUND.getCode(),
								ApiErrorEnum.ATTACHMENT_NOT_FOUND.getMessage())))));
		
		
		return attachmentCommentRepository.save(attachmentComment);
		
	}

	/**
	 * Trae todos los comments de un attachment
	 * 
	 * @param attachmentId
	 * @return
	 */
	@Override
	public MutableList<AttachmentCommentResponseDTO> getComments(Long attachmentId, String lang) {

		try {
			
			MutableList<AttachmentCommentResponseDTO> response = Lists.mutable.empty();
			log.info("AttachmentId: "+attachmentId);
			Attachment attachment = new Attachment();
			attachment.setId(attachmentId);
			
			attachmentCommentRepository.findByAttachmentAndStatus(attachment,1).orElse(Lists.mutable.empty())
					.forEach(c -> response.add(AttachmentCommentMapper.mapAttachmentCommentResponse(c)));

			return response;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentCommentService.edit]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	/**
	 * Edita un coment
	 * 
	 * @param id
	 * @param value
	 * @return
	 */
	@Override
	public AttachmentComment edit(Long id, String value) {

		try {
			// BUSCA COMMENT A EDITAR
			AttachmentComment attachmentComment = attachmentCommentRepository.findById(id)
					.orElseThrow(() -> new ResourceNotFoundException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.ATTACHMENT_COMMENT_NOT_FOUND.getCode(),
									ApiErrorEnum.ATTACHMENT_COMMENT_NOT_FOUND.getMessage()))));

			return attachmentCommentRepository.save(attachmentComment);
		
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [AttachmentCommentService.edit]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	
	@Override
	public AttachmentComment findByCommentAttachent(CommentAttachment commentAttachment, String lang) {

		return attachmentCommentRepository.findByCommentAttachment(commentAttachment)
				.orElse(null);
		
	}
}
