package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.AlbumTypeDTO;
import com.dinbog.api.dto.AlbumTypeRequestDTO;
import com.dinbog.api.models.entity.AlbumType;

public interface IAlbumTypeService {

	/**
	 * @param request
	 * @return
	 */
	AlbumType create(AlbumTypeRequestDTO request);

	/**
	 * @return
	 */
	MutableList<AlbumTypeDTO> readAll();

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	AlbumType update(Long id, AlbumTypeRequestDTO request);

	/**
	 * @param id
	 */
	void deleteById(Long id);
	
	AlbumType findById(Long id, String lang);

	Optional<AlbumType> findById(Long albumTypeId);

}