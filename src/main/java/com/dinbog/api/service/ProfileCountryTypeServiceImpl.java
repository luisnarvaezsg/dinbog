package com.dinbog.api.service;

import java.util.Optional;

import com.dinbog.api.repository.ProfileCountryTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.models.entity.ProfileCountryType;
/**
 * @author Luis
 *
 */
@Transactional
@Service
public class ProfileCountryTypeServiceImpl implements IProfileCountryTypeService {

	@Autowired
	private ProfileCountryTypeRepository profileCountryRepository;

	@Override
	public Optional<ProfileCountryType> findById(Long id) {
		
		return profileCountryRepository.findById(id);
	}

}
