package com.dinbog.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.models.entity.ConnectionType;
import com.dinbog.api.repository.ConnectionTypeRepository;

@Service
public class ConnectionTypeServiceImpl implements IConnectionTypeService {

	@Autowired
	private ConnectionTypeRepository connectionTypeRepository;

	@Override
	public Optional<ConnectionType> findById(Long id) {
		
		return connectionTypeRepository.findById(id);
	}

	}
