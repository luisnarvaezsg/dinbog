package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.CategoryFilterDTO;
import com.dinbog.api.models.entity.CategoryFilter;
import com.dinbog.api.models.entity.Filter;
import com.dinbog.api.models.entity.ProfileCategoryType;

public interface ICategoryFilterService {

	MutableList<CategoryFilterDTO> findByProfileCategoryType(ProfileCategoryType profileCategoryType, String lang);

	Optional<CategoryFilter> findByProfileCategoryTypeIdAndFilter(Long profileCategoryTypeId, Filter filter, String lang);
}