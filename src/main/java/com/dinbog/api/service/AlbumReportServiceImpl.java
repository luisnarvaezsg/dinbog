package com.dinbog.api.service;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.dto.AlbumReportCreateDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.AlbumReport;
import com.dinbog.api.repository.AlbumReportRepository;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlbumReportServiceImpl implements IAlbumReportService {
    
	@Autowired
    private AlbumReportRepository albumReportRepository;

    @Autowired
    private IProfileService profileService;

    @Autowired
    private IAlbumService albumService;

    @Autowired
    private IReportTypeService reportTypeService;

    @Override
	@Transactional(propagation = Propagation.REQUIRED)
    public AlbumReport create(Long albumId, AlbumReportCreateDTO request){
        try {
            AlbumReport albumReport = new AlbumReport();
            albumReport
                    .setOwner(profileService.findById(request.getProfileId())
                            .orElseThrow(() -> new InvalidRequestException(
                                    Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
                                            ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
            albumReport
                    .setAlbum(albumService.findById(albumId)
                            .orElseThrow(() -> new InvalidRequestException(
                                    Lists.mutable.of(new ApiError(ApiErrorEnum.ALBUM_NOT_FOUND.getCode(),
                                            ApiErrorEnum.ALBUM_NOT_FOUND.getMessage())))));
            albumReport
                    .setReportType(reportTypeService.findById(request.getReportTypeId())
                            .orElseThrow(() -> new InvalidRequestException(
                                    Lists.mutable.of(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
                                            ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage())))));
            albumReport.setDetail(request.getDetail());
            return albumReportRepository.save(albumReport);
        } catch (Exception e) {
            // VALIDA SI ES ERROR CONTROLADO
            if (Util.isCustomException(e))
                throw e;
            // EN CASO DE ERROR NO CONTROLADO
            log.error("*** Exception EN [AlbumReportService.create]");
            Util.printLogError(e);
            throw new InternalErrorException(e);
        }
    }
}
