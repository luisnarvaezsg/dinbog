package com.dinbog.api.service;

import com.dinbog.api.models.vo.Email;
import com.dinbog.api.util.email.EmailConstants;
import com.dinbog.api.util.email.LogEmailService;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class EmailQueueService {

    private RabbitTemplate rabbitTemplate;

    @Autowired
    public EmailQueueService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    RabbitTemplate.ConfirmCallback confirmCallback = ((correlationData, ack, cause) -> {
            Gson gson =  new Gson();
            LogEmailService.infoLog(gson.toJson( correlationData));
            if(!ack){
                LogEmailService.warnLog(cause);
            }
    });

    RabbitTemplate.ReturnCallback returnCallback = ((msg, code, text, exchange, key) -> {
            Gson gson =  new Gson();
            LogEmailService.infoLog( gson.toJson( msg ));
    });

    public void sendToQueue(Email email){
        long startingAt = System.currentTimeMillis();
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnCallback(returnCallback);
        CorrelationData correlationData =  new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend(EmailConstants.EXCHANGER.getValue(),EmailConstants.ROUTE_KEY.getValue(), email , correlationData);
        long endingAt = System.currentTimeMillis();
        LogEmailService.sendingLog(startingAt,endingAt,email.getTo());
    }
}
