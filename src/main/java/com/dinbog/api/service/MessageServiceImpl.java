package com.dinbog.api.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dinbog.api.dto.MensajeDTO;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.Message;
import com.dinbog.api.models.entity.Profile;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.repository.MessageRepository;
import com.dinbog.api.security.jwt.JwtProvider;

@Service
public class MessageServiceImpl implements IMessageService {

	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	IProfileService iProfileService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Autowired
	private JwtProvider jwtProvider; 

	@Override
	public Message createMessage(HttpServletRequest request, Long idDest, MensajeDTO mensajeDTO, String lang) {
		
		Message message = new Message();
		User user = jwtProvider.getUser(request);
		Profile profileSrc = iProfileService.findByUser(user).orElseThrow(() -> new InvalidRequestException(
						Lists.mutable.of(errorService.getApiError(ApiErrorEnum.PROFILE_NOT_FOUND, lang))));				
		message.setProfileSrc(profileSrc);
		message.setProfileDest(iProfileService.readById(idDest, lang));
		String content = mensajeDTO.getMensaje(); 
		if ((content == null) || (content.isEmpty()))
			throw new InvalidRequestException(
					Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EMPTY_MESSAGE, lang)));
		message.setContent(content);
		message.setMessageDate(new Date());
		return messageRepository.save(message);
	}
	


	@Override
	public Page<Message> findAllByProfileSrc(Profile profileSrc, Pageable pageable) {
		
		return messageRepository.findAllByProfileSrc(profileSrc, pageable);
	}

	@Override
	public Page<Message> findAllByProfileDest(Profile profileDest, Pageable pageable) {
		
		return messageRepository.findAllByProfileDest(profileDest, pageable);
	}

	@Override
	public void deleteMessage(Message message) {
		
		messageRepository.delete(message);
	}
	
	
}
