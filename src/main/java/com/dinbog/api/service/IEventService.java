package com.dinbog.api.service;

import java.util.List;

import com.dinbog.api.dto.EventDTO;
import com.dinbog.api.dto.EventRequestDTO;
import com.dinbog.api.models.entity.Event;
import com.dinbog.api.models.entity.User;
import com.dinbog.api.util.PaginateResponse;

public interface IEventService {

	/**
	 * @param request
	 * @return
	 */
	Event create(EventRequestDTO request, User user, String lang);

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param search
	 * @return
	 */
	PaginateResponse<EventDTO> readAll(Integer pageNumber, Integer pageSize, String search, String lang);

	/**
	 * @param id
	 * @return
	 */
	Event readById(Long id, String lang);

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	Event edit(Long id, EventRequestDTO request);

	/**
	 * @param id
	 */
	void deleteById(Long id);

	List<Event> findByOwnerId(Long id);

}