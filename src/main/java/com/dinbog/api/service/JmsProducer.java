package com.dinbog.api.service;

import com.dinbog.api.models.vo.Email;

public interface JmsProducer {

    void sendToQueue(String topic, Email email, String lang);
}
