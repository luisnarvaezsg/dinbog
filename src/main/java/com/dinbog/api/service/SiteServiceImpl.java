package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;

import com.dinbog.api.dto.SiteCreateDTO;
import com.dinbog.api.dto.SiteDTO;
import com.dinbog.api.repository.SiteRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.Site;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.SiteMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("SiteService")
public class SiteServiceImpl implements ISiteService {

	@Autowired
	private SiteRepository siteRepository;

	/**
	 * @return
	 */
	@Override
	public MutableList<SiteDTO> readAll() {

		try {
			List<Site> sites = siteRepository.findAll();
			MutableList<SiteDTO> siteDTOS = Lists.mutable.empty();
			if (sites.isEmpty()) {
				throw new ResourceNotFoundException(Lists.mutable.of(new ApiError(
						ApiErrorEnum.SITE_NO_RESULTS.getCode(), ApiErrorEnum.SITE_NO_RESULTS.getMessage())));
			} else {
				sites.forEach(i -> siteDTOS.add(SiteMapper.mapSite(i)));
			}
			return siteDTOS;
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("Error en [SiteService.readAll] ");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public Site create(SiteCreateDTO request) {
		try {

			Site site = new Site();
			site.setActive(request.getActive());
			site.setDomain(request.getDomain());
			site.setName(request.getName());
			return siteRepository.save(site);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void patchById(Long id, SiteCreateDTO request) {
		try {
			Optional<Site> optional = siteRepository.findById(id);

			if (!optional.isPresent()) {
				throw new ResourceNotFoundException(Lists.mutable.of(
						new ApiError(ApiErrorEnum.SITE_NOT_FOUND.getCode(), ApiErrorEnum.SITE_NOT_FOUND.getMessage())));
			} else {
				Site site = optional.get();
				site.setActive(request.getActive());
				site.setDomain(request.getDomain());
				site.setName(request.getName());
				siteRepository.save(site);
			}
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteService.patchById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		try {

			siteRepository.findById(id).orElseThrow(() -> new InvalidRequestException(Lists.mutable.of(
					new ApiError(ApiErrorEnum.SITE_NOT_FOUND.getCode(), ApiErrorEnum.SITE_NOT_FOUND.getMessage()))));

			siteRepository.deleteById(id);

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [SiteService.deleteById]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
