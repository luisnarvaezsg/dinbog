package com.dinbog.api.service;

import org.eclipse.collections.api.list.MutableList;

import com.dinbog.api.dto.LanguageDTO;
import com.dinbog.api.dto.LanguageRequestDTO;
import com.dinbog.api.models.entity.Language;

public interface ILanguageService {

	/**
	 * Metodo para crear un language
	 * 
	 * @param request
	 * @return
	 */
	Language create(LanguageRequestDTO request);

	/**
	 * @return
	 */
	MutableList<LanguageDTO> read();

	/**
	 * Método para actualizar un language
	 * 
	 * @param id
	 * @param request
	 * @return
	 */
	Language update(Long id, LanguageRequestDTO request);

	/**
	 * Borra un language por su id
	 *
	 * @param id
	 * @return
	 */
	void delete(Long id);
	
	String validLanguage(String isoCode);

}