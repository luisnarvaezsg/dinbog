package com.dinbog.api.service;

import java.util.Date;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.commons.CommonsConstant;
import com.dinbog.api.dto.PostReportRequestDTO;
import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.models.entity.PostReport;
import com.dinbog.api.repository.PostReportRepository;
import com.dinbog.api.util.Util;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PostReportServiceImpl implements IPostReportService {
	
	@Autowired
	private IPostService postService;
	
	@Autowired
	private IProfileService profileService;
	
	@Autowired
	private IReportTypeService reportTypeService;
	
	@Autowired
	private PostReportRepository postReportRepository;
	
	/**
	 * @param postId
	 * @param request
	 * @return
	 */
	@Override
	public PostReport create(Long postId, PostReportRequestDTO request) {
		try {
			PostReport postReport = new PostReport();
			postReport.setPost(postService.findById(postId, CommonsConstant.LANGUAGE_BY_DEFAULT).orElseThrow(
					() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.POST_NOT_FOUND.getCode(),
									ApiErrorEnum.POST_NOT_FOUND.getMessage())))
			));
			postReport.setReportType(reportTypeService.findById(request.getReportTypeId()).orElseThrow(
					()->new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getCode(),
									ApiErrorEnum.REPORT_TYPE_NOT_FOUND.getMessage())))
			));
			postReport.setOwner(profileService.findById(request.getOwnerId()).orElseThrow(
					()->new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))
			));
			postReport.setDetail(request.getDetail());
			postReport.setCreated(new Date());
			return postReportRepository.save(postReport);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [PostReportService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
