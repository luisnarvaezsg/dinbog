package com.dinbog.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.models.entity.Traslation;

@Service
public class ApiErrorServiceImpl implements IApiErrorService {
	
	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private ILanguageService languageService;
	
	@Override
	public ApiError getApiError(ApiErrorEnum errorCode, String language) {
		
		ApiError apiError = new ApiError();
		apiError.setErrorCode(errorCode.getCode());
		
		language = languageService.validLanguage(language);
		
		Traslation traslation = traslationService.findByKeyAndIsoCode(errorCode.getKey(), language);
		
		if (traslation == null) {
			apiError.setMessage(errorCode.getKey());
		}else {
			apiError.setMessage(traslation.getValue());
		}
		return apiError;
	}
	

}
