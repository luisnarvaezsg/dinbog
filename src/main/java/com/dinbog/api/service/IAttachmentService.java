package com.dinbog.api.service;

import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.dinbog.api.dto.AttachmentDTO;
import com.dinbog.api.dto.AttachmentLikeDTO;
import com.dinbog.api.dto.AttachmentReportRequestDTO;
import com.dinbog.api.models.entity.Attachment;
import com.dinbog.api.models.entity.AttachmentReport;
import com.dinbog.api.models.entity.AttachmentType;
import com.dinbog.api.util.PaginateResponse;

public interface IAttachmentService {



	/**
	 * @param profileId
	 * @param attachmentId
	 */
	void createLike(AttachmentLikeDTO attachment, String language);

	/**
	 * @param profileId
	 * @param attachmentId
	 */
	void deleteLike(AttachmentLikeDTO attachment, String language);

	/**
	 * @param pageNumber
	 * @param pageSize
	 * @param extra
	 * @return
	 */
	PaginateResponse<AttachmentDTO> readAll(Integer pageNumber, Integer pageSize, String extra);

	/**
	 * @param id
	 * @return
	 */
	Attachment readById(Long id);

	/**
	 * @param id
	 */
	void deleteById(Long id);

	/**
	 * @param id
	 * @param request
	 * @return
	 */
	AttachmentReport report(Long id, AttachmentReportRequestDTO request);

	AttachmentType findType(MultipartFile file, String lang);

	/**
	 * @param file
	 * @return
	 */
	Attachment create(MultipartFile file, MultipartFile cover, String metadata, String lang, String bucketName);

	Attachment create(String url, String lang);

	String findURL(String attachmentPath);

	void delete(Attachment attachment);

	Optional<Attachment> findById(Long attachmentId);


}
