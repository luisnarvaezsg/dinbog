package com.dinbog.api.service;

import com.dinbog.api.dto.ReportTypeDTO;
import com.dinbog.api.dto.ReportTypeRequestDTO;
import com.dinbog.api.dto.ReportTypeResponseDTO;
import com.dinbog.api.models.entity.ReportType;

import java.util.Optional;

import org.eclipse.collections.api.list.MutableList;

public interface IReportTypeService {
    ReportTypeResponseDTO create(ReportTypeRequestDTO request);

    MutableList<ReportTypeDTO> read(String langCode);

    ReportTypeResponseDTO update(Long id, ReportTypeRequestDTO request);

    void delete(Long id);

	Optional<ReportType> findById(Long reportTypeId);
}
