package com.dinbog.api.service;

import java.util.List;
import java.util.Optional;
import com.dinbog.api.repository.ProfileDetailRepository;
import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.models.entity.ProfileDetail;
import com.dinbog.api.util.Util;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Luis
 *
 */
@Slf4j
@Transactional
@Service
public class ProfileDetailServiceImpl implements IProfileDetailService {

	@Autowired
	private ProfileDetailRepository profileDetailRepository;

	/**
	 * @param profileId
	 * @return
	 */
	@Override
	public List<ProfileDetail> getBasicDetails(Long profileId) {

		try {
			Optional<List<ProfileDetail>> details = profileDetailRepository.findByProfile(profileId);
			
			if (details.isPresent()) {
				return details.get();
			} else {
				return Lists.mutable.empty();
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileDetailService.getBasicDetails]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}
}
