package com.dinbog.api.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.dinbog.api.dto.ProfileRecommendationCreateDTO;
import com.dinbog.api.dto.ProfileRecommendationDTO;
import com.dinbog.api.dto.ProfileRecommendationEditDTO;
import com.dinbog.api.repository.ProfileRecommendationRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiError;
import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.InternalErrorException;
import com.dinbog.api.exception.InvalidRequestException;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.ProfileRecommendation;
import com.dinbog.api.util.Util;
import com.dinbog.api.util.mapper.ProfileRecommendationMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileRecommendationServiceImpl implements IProfileRecommendationService {

	@Autowired
	private IProfileService profileService;

	@Autowired
	private ProfileRecommendationRepository profileRecommendationRepository;

	@Autowired
	private EntityManager entityManager;

	@Override
	public ProfileRecommendation create(Long profileId, ProfileRecommendationCreateDTO request) {
		try {
			ProfileRecommendation profileRecommendation = new ProfileRecommendation();
			profileRecommendation.setOwnerProfile(profileService.findById(request.getOwnerProfileId())
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_OWNER_NOT_FOUND.getMessage())))));

			profileRecommendation
					.setProfile(profileService.findById(profileId)
							.orElseThrow(() -> new InvalidRequestException(
									Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_NOT_FOUND.getCode(),
											ApiErrorEnum.PROFILE_NOT_FOUND.getMessage())))));
			profileRecommendation.setValue(request.getValue());
			profileRecommendation.setIsPublic(request.getIsPublic());

			return profileRecommendationRepository.save(profileRecommendation);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRecommendationService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public ProfileRecommendation patch(Long id, Long profileId, ProfileRecommendationEditDTO request) {
		try {

			ProfileRecommendation profileRecommendation = profileRecommendationRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_RECOMMENDATION_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_RECOMMENDATION_NOT_FOUND.getMessage()))));
			profileRecommendation.setValue(request.getValue());
			profileRecommendation.setIsPublic(request.getIsPublic());

			return profileRecommendationRepository.save(profileRecommendation);
		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRecommendationService.create]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public MutableList<ProfileRecommendationDTO> getListProfileRecommendationByProfileId(Long profileId) {

		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ProfileRecommendation> criteriaQuery = criteriaBuilder
					.createQuery(ProfileRecommendation.class);

			Root<ProfileRecommendation> from = criteriaQuery.from(ProfileRecommendation.class);
												criteriaQuery.where(
												criteriaBuilder.and(criteriaBuilder.equal(from.get("profile"), profileId)));

			List<ProfileRecommendation> query = entityManager.createQuery(criteriaQuery).getResultList();
			if (query.isEmpty()) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_RECOMMENDATION_NO_RESULT.getCode(),
								ApiErrorEnum.PROFILE_RECOMMENDATION_NO_RESULT.getMessage())));
			}
			MutableList<ProfileRecommendationDTO> profileRecommendationDTOs = Lists.mutable.empty();
			query.forEach(profileRecommendation -> profileRecommendationDTOs
					.add(ProfileRecommendationMapper.mapEntityToDTO(profileRecommendation)));
			return profileRecommendationDTOs;

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileLikeService.getListProfileRecommendationByProfileId]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

	@Override
	public ProfileRecommendation getProfileRecommendationById(Long id) {

		return  profileRecommendationRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_RECOMMENDATION_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_RECOMMENDATION_NOT_FOUND.getMessage()))));
	}

	@Override
	public void delete(Long id, Long profileId) {
		try {
			profileRecommendationRepository.findById(id)
					.orElseThrow(() -> new InvalidRequestException(
							Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_RECOMMENDATION_NOT_FOUND.getCode(),
									ApiErrorEnum.PROFILE_RECOMMENDATION_NOT_FOUND.getMessage()))));

			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ProfileRecommendation> criteriaQuery = criteriaBuilder
					.createQuery(ProfileRecommendation.class);
			Root<ProfileRecommendation> from = criteriaQuery.from(ProfileRecommendation.class);
			// Join<ProfileRecommendation, Profile> profiles = from.join("profile")

			// CriteriaQuery<ProfileRecommendation> where =
			criteriaQuery.where(
					// criteriaBuilder.and( criteriaBuilder.equal(profiles.get("deleted"), false) ),

					criteriaBuilder.and(criteriaBuilder.equal(from.get("id"), id),
							criteriaBuilder.equal(from.get("profile"), profileId)));

			ProfileRecommendation query = entityManager.createQuery(criteriaQuery).getSingleResult();
			if (query == null) {
				throw new ResourceNotFoundException(
						Lists.mutable.of(new ApiError(ApiErrorEnum.PROFILE_RECOMMENDATION_NO_RESULT.getCode(),
								ApiErrorEnum.PROFILE_RECOMMENDATION_NO_RESULT.getMessage())));
			} else {
				profileRecommendationRepository.deleteRecommendationByIdAndProfileId(id, profileId);
			}

		} catch (Exception e) {
			// VALIDA SI ES ERROR CONTROLADO
			if (Util.isCustomException(e))
				throw e;
			// EN CASO DE ERROR NO CONTROLADO
			log.error("*** Exception EN [ProfileRecommendationService.delete]");
			Util.printLogError(e);
			throw new InternalErrorException(e);
		}
	}

}
