package com.dinbog.api.service;

import java.util.Optional;

import org.eclipse.collections.api.factory.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dinbog.api.exception.ApiErrorEnum;
import com.dinbog.api.exception.ResourceNotFoundException;
import com.dinbog.api.models.entity.EventStatus;
import com.dinbog.api.models.entity.Traslation;
import com.dinbog.api.repository.EventStatusRepository;

@Service("EventStatusService")
public class EventStatusServiceImpl implements IEventStatusService {

	@Autowired
	private EventStatusRepository eventStatusRepository;
	
	@Autowired
	private ILanguageService languageService;

	@Autowired
	private ITraslationService traslationService;
	
	@Autowired
	private IApiErrorService errorService;
	
	@Override
	public Optional<EventStatus> findById(Long id, String lang) {
		
		Optional<EventStatus> eventStatus = Optional.empty();
		
		lang = languageService.validLanguage(lang);
		
		try {
			
			eventStatus = eventStatusRepository.findById(id);
			
			if(eventStatus.isPresent()) {
				
				Traslation traslation =  traslationService.findByKeyAndIsoCode(eventStatus.get().getKey(), lang);
				if(traslation == null) {
					eventStatus.get().setValue(eventStatus.get().getKey());	
				}else {
					eventStatus.get().setValue(traslation.getValue());
				}
			}
			
		} catch (Exception e) {
			throw new ResourceNotFoundException(Lists.mutable.of(errorService.getApiError(ApiErrorEnum.EVENT_STATUS_NOT_FOUND, lang)));
		}
		
		return eventStatus;
	}

}
