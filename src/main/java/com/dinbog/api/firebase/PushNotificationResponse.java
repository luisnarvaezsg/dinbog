package com.dinbog.api.firebase;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@ToString
public class PushNotificationResponse {

	private int status;
	private String message;
}
