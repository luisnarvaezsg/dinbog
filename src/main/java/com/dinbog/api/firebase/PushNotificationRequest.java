package com.dinbog.api.firebase;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class PushNotificationRequest {

	private String title;
	private String message;
	private String topic;
	private String token;
}
