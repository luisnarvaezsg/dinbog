package com.dinbog.api.commons;

public class CommonsConstant {
	
	public static final String LANGUAGE_BY_DEFAULT = "EN";
	public static final String PATH_IMAGE="https://s3.us-east-2.amazonaws.com/dinbog-post/";
	public static final String BUCKET_NAME = "dinbog-post";
	public static final Integer ACTIVE_STATUS = 1;
	public static final Integer INACTIVE_STATUS = 0;

}
