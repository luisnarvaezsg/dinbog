package com.dinbog.api.commons;

public enum AlbumTypeEnum {
	
	BOOK(1L),
	AUDIO(2L),
	VIDEO(3L);
	
	private Long albumTypeId;
	
	public Long getAlbumTypeId() { 
		
		return albumTypeId; 
	} 
	
	AlbumTypeEnum(Long albumTypeId) {
		this.albumTypeId = albumTypeId;
	}
}
