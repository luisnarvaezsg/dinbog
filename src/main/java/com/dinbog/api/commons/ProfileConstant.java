package com.dinbog.api.commons;

public class ProfileConstant {
	
	public static final Long CATEGORIES_SEARCH_LEVEL_BY_DEFAULT = 1L;
	
	public static final Long TALENT_PROFILE_TYPE = 1L;
	
	public static final Long COMPANY_PROFILE_TYPE = 2L;
	
	public static final Long COUNTRY_ORIGIN_TYPE = 1L;
	
	public static final Long COUNTRY_BRANCH_TYPE = 2L;
	
	public static final Long COUNTRY_RESIDENCE_TYPE = 2L;
    
    private ProfileConstant() {
    }

}
