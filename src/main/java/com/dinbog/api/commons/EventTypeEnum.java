package com.dinbog.api.commons;

public enum EventTypeEnum {
	
	EVENT(1L),
	CASTING(2L),
	CASTING_ONLINE(3L),
	CONTEST(4L);
	
	private Long eventTypeId;
	
	public Long getEventTypeId() { 
		
		return eventTypeId; 
	} 
	
	EventTypeEnum(Long eventTypeId) {
		this.eventTypeId = eventTypeId;
	}
}
