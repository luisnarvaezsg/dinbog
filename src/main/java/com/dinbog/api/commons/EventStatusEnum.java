package com.dinbog.api.commons;

public enum EventStatusEnum {
	
	DRAFT(1L),
	PUBLISHED(2L),
	APROVED(3L),
	DENIED(4L);
	
	private Long eventStatusId;
	
	public Long getEventStatusId() { 
		
		return eventStatusId; 
	} 
	
	EventStatusEnum(Long eventStatusId) {
		this.eventStatusId = eventStatusId;
	}
}
