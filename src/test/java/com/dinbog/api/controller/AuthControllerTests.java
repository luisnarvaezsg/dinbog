/**
 * 
 */
package com.dinbog.api.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dinbog.api.ApiApplication;
import com.dinbog.api.dto.LoginDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Tests para la clase {@link com.dinbog.api.controller.AuthController AuthController}
 * 
 * @author Luis
 *
 */
@SpringBootTest(classes = ApiApplication.class)
@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
class AuthControllerTests {

	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	/**
	 * @param webApplicationContext
	 * @param restDocumentation
	 */
	@BeforeEach
	public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(restDocumentation)).build();
	}

	/**
	 * Test del método
	 * {@link com.dinbog.api.controller.AuthController#login(com.dinbog.api.dto.LoginDTO)
	 * login(LoginDTO)}
	 * 
	 * @throws Exception
	 * @author Luis
	 */
	@Test
	void login() throws Exception {

		LoginDTO request = new LoginDTO();
		request.setEmail("rupacheco843@hotmail.com");
		request.setPassword("12345");

		ResultActions result = this.mockMvc.perform(RestDocumentationRequestBuilders.post("/v1/auth/login")
						.contentType(MediaType.APPLICATION_JSON)
						.content(this.objectMapper.writeValueAsString(request)))
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						.andDo(document("auth/login", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
						// REQUEST FIELDS
						requestFields(
								fieldWithPath("email").description("El email del usuario"),
								fieldWithPath("password").description("El password del usuario")),
						// RESPONSE FIELDS
						responseFields(
								fieldWithPath("token").description("El token de autenticacion para usar en todos los request"),
								fieldWithPath("userId").description("El id del user autenticado"),
								fieldWithPath("profileUrl").description("El URL del profile del user"),
								fieldWithPath("profileId").description("El Id del profile del user"),
								fieldWithPath("avatarPath").description("La ruta del avatar del profile del user"),
								fieldWithPath("profileTypeId").description("El tipo del profile del user"))

				));

		Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
	}
}
