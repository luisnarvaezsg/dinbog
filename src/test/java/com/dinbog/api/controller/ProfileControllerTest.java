/**
 * 
 */
package com.dinbog.api.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dinbog.api.ApiApplication;
import com.dinbog.api.dto.ProfileRequestDTO;
import com.dinbog.api.dto.ProfileWorkDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Tests para la clase {@link com.dinbog.api.controller.ProfilController
 * ProfilController}. Se delegan casi todos los tests de esta clase a
 * {@link com.dinbog.api.controller.UserProfileControllerTests
 * UserProfileControllerTests}. Acá solo van test que no tengan relacion directa
 * con otros y que no necesitan crear data previa
 * 
 */
@SpringBootTest(classes = ApiApplication.class)
@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
class ProfileControllerTest {

	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	@BeforeEach
	public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(restDocumentation)).build();

	}

	/**
	 * @throws Exception
	 */
	@Test
	void readAllCategoriesTest() throws Exception {

		Long pType = 1L;
		String lang = "EN";
		Long level = 1L;
		Boolean childs = true;

		FieldDescriptor[] categories = new FieldDescriptor[] { 
				subsectionWithPath("profileType").description("Type of Profile (Talent=1, Company=2)"),
				subsectionWithPath("level").description("Search Level (1-Category, 2-SubCategory, etc.)"),
				subsectionWithPath("id").description("Category Id"),
				subsectionWithPath("parent").description("Category Parent"),
				subsectionWithPath("child").description("Category Childs"),
				subsectionWithPath("value").description("Category Description"),
				subsectionWithPath("key").description("Key for Multilanguage")};

		ResultActions result = this.mockMvc
				.perform(RestDocumentationRequestBuilders.get("/v1/profiles/categories/?pType={pType}&level={level}&childs={childs}&lang={lang}"
						,pType, level, childs, lang)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andDo(document("profile/read-all-categories", preprocessResponse(prettyPrint()),
						requestParameters(
								parameterWithName("lang").description("Language").optional(),
								parameterWithName("pType").description("Profile Type"),
								parameterWithName("level").description("Search Level").optional(),
								parameterWithName("childs").description("Add Childs?").optional()),
						responseFields(fieldWithPath("[]").description("Una lista de categorias de Profiles"))
								.andWithPrefix("[].", categories)));

		Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
		
		}

	@Test
	void createProfile() throws Exception {

		ProfileRequestDTO request = new ProfileRequestDTO();
		request.setUserId(114L);
		request.setBirthDate(new Date());
		request.setGenderId(1L);
		request.setPhoneNumber("999-9999-999");
		request.setIsSearchable(true);
		request.setRegistrationStep(1L);
		request.setResidenceCountryId(144L);
		request.setResidenceCityId(12L);
		request.setBornCountryId(145L);
		request.setBornCityId(2L);
		request.setMembershipId(1L); 
		request.setProfileTypeId(1L); 
		request.setAvatarPath("http://52.207.106.153/api-guide/ver3/#resources-profile-create");
		String[] categories = {"601", "602"};
		request.setCategories(categories);
		
		
		ResultActions result = this.mockMvc.perform(RestDocumentationRequestBuilders.post("/v1/profiles/save?lang=EN")
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(objectMapper.writeValueAsString(request)))
	                .andExpect(status().isCreated())
	                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
	                .andDo(document("profile/create", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
	                		responseFields(
	                				fieldWithPath("id").description("Profile id"),
	                				fieldWithPath("user").description("User data:"),
									fieldWithPath("user.id").description("User id associates to Profile"),
									fieldWithPath("user.lastLogin").description("User date of last login"),
									fieldWithPath("user.created").description("User created date"),
									fieldWithPath("user.emails").description("Emails associates to user"),
									fieldWithPath("user.groups").description("Users groups to user"),
									fieldWithPath("user.userStatus.id").description("User status"),
									fieldWithPath("user.userStatus.name").description("Status name"),
									fieldWithPath("urlName").description("Random name to Profile"),
									fieldWithPath("firstName").description("Profile first name"),
									fieldWithPath("lastName").description("Profile last name"),
									fieldWithPath("fullName").description("Profile full name"),
									fieldWithPath("birthDate").description("Profile birth date"),
									fieldWithPath("gender").description("Genders data"),
									fieldWithPath("gender.id").description("Profile gender id"),
									fieldWithPath("gender.value").description("Gender values"),
									fieldWithPath("gender.key").description("Gender key for internationalization"),
									fieldWithPath("profileType").description("Profile tyoe data"),
									fieldWithPath("profileType.id").description("Profile type id"),
									fieldWithPath("profileType.value").description("Profile type description"),
									fieldWithPath("isSearchable").description("Is profile searchable?"),
									fieldWithPath("searchableSince").description("Searchable date"),
									fieldWithPath("membership").description("Membership data"),
									fieldWithPath("membership.id").description("Membership id"),
									fieldWithPath("membership.name").description("Membership name"),
									fieldWithPath("membership.created").description("Memebership reated date"),
									fieldWithPath("countLike").description("Likes cantity"),
									fieldWithPath("countFollow").description("Follows cantity"),
									fieldWithPath("countFollowers").description("Followers cantity"),
									fieldWithPath("countConections").description("Connections cantity"),
									fieldWithPath("countPost").description("Post cantity"),
									fieldWithPath("countViews").description("Views cantity"),
									fieldWithPath("registrationStep").description("Profile step of registration"),
									fieldWithPath("avatarPath").description("Profile avatar path on AWS"),
									fieldWithPath("coverId").description("Profile cover id"),
									fieldWithPath("created").description("Profile created date"),
									fieldWithPath("bornCountry").description("Born country data"),
									fieldWithPath("bornCountry.id").description("Born country id"),
									fieldWithPath("bornCountry.name").description("Born country name"),
									fieldWithPath("bornCountry.isoCode").description("Born country isoCode"),
									fieldWithPath("bornCountry.unitMeasurement").description("Born country unit measurement"),
									fieldWithPath("bornCountry.wearMeasurement").description("Born country wear measurement"),
									fieldWithPath("bornCountry.isOnu").description("Country is ONU?"),
									fieldWithPath("bornCountry.created").description("Country created date"),
									fieldWithPath("bornCountry.language").description("Country language"),
									fieldWithPath("bornCity").description("Born City data"),
									fieldWithPath("bornCity.id").description("Born City id"),
									fieldWithPath("bornCity.code").description("Born City iso code"),
									fieldWithPath("bornCity.latitude").description("Born City latitude"),
									fieldWithPath("bornCity.longitude").description("Born City longitude"),
									fieldWithPath("bornCity.stateName").description("Born City name"),
									fieldWithPath("bornCity.countryId").description("Born City country id"),
									fieldWithPath("bornCity.key").description("Born City key for internationalization"),
									fieldWithPath("residenceCountry").description("Residence Country data"),
									fieldWithPath("residenceCountry.id").description("Residence Country id"),
									fieldWithPath("residenceCountry.name").description("Residence Country name"),
									fieldWithPath("residenceCountry.isoCode").description("Residence Country isoCode"),
									fieldWithPath("residenceCountry.unitMeasurement").description("Residence Country unit measurement"),
									fieldWithPath("residenceCountry.wearMeasurement").description("Residence Country wear measurement"),
									fieldWithPath("residenceCountry.isOnu").description("Country is ONU?"),
									fieldWithPath("residenceCountry.created").description("Residence country created date"),
									fieldWithPath("residenceCountry.language").description("Residence country language"),
									fieldWithPath("residenceCity").description("Residence city data"),
									fieldWithPath("residenceCity.id").description("Residence city id"),
									fieldWithPath("residenceCity.code").description("Residence city iso code"),
									fieldWithPath("residenceCity.latitude").description("Residence city latitude"),
									fieldWithPath("residenceCity.longitude").description("Residence city longitude"),
									fieldWithPath("residenceCity.stateName").description("Residence city name"),
									fieldWithPath("residenceCity.countryId").description("Residence city country id"),
									fieldWithPath("residenceCity.key").description("Residence city key for internationalization"),
									fieldWithPath("phoneNumber").description("Profile phone niumber"),
									fieldWithPath("categories[]").description("Profile categories"),
									fieldWithPath("categories[].id").description("Category id"),
									fieldWithPath("categories[].parent").description("Category parent"),
									fieldWithPath("categories[].child").description("Residence city childs"),
									fieldWithPath("categories[].profileType").description("Category profile type"),
									fieldWithPath("categories[].value").description("Category description"),
									fieldWithPath("categories[].key").description("Category key for internationalization"),
									fieldWithPath("categories[].level").description("Category level")
									)
						));
		 
		 Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
		
		}

	/**
	 * Test del método
	 * {@link com.dinbog.api.controller.ProfilController#uploadWork(ProfileWorkDTO, String)
	 * #uploadWork(ProfileWorkDTO, String)}
	 * 
	 * @throws Exception
	 */
	@Test
	void updloadWork() throws Exception {

		ProfileWorkDTO request = new ProfileWorkDTO();
		request.setId(0L);
		request.setProfileId(108L);
		request.setClient("Dinbog LLC");
		request.setDescription("Trabajo que hice");
		request.setYearJob(2020L);

		ResultActions result = this.mockMvc.perform(RestDocumentationRequestBuilders.post("/v1/profiles/upload-work")
						.contentType(MediaType.APPLICATION_JSON)
						.content(this.objectMapper.writeValueAsString(request)))
						.andExpect(status().isCreated())
						.andExpect(content().contentType(MediaType.APPLICATION_JSON))
						.andDo(document("profiles/upload-work", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
						// REQUEST FIELDS
						requestFields(
								fieldWithPath("id").description("ProfileWork id (0 if is new, any valor if is update"),
								fieldWithPath("profileId").description("Profile id to associate the work"),
								fieldWithPath("client").description("Name Client"),
								fieldWithPath("description").description("Work description"),
								fieldWithPath("yearJob").description("Work year"),
								fieldWithPath("profile").description("Profile work").ignored(),
								fieldWithPath("created").description("Work created at").ignored()),
						// RESPONSE FIELDS
						responseFields(
								fieldWithPath("id").description("ProfileWork id"),
								fieldWithPath("profileId").description("Profile id to associate the work"),
								fieldWithPath("client").description("Name Client"),
								fieldWithPath("description").description("Work description"),
								fieldWithPath("yearJob").description("Work year"),
								fieldWithPath("profile").description("Profile work"),
								fieldWithPath("created").description("Work created at"))

				));

		Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
	}
}
