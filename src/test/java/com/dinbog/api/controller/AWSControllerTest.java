package com.dinbog.api.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amazonaws.util.IOUtils;
import com.dinbog.api.ApiApplication;

/**
 * Tests para la clase {@link com.dinbog.api.controller.AuthController AuthController}
 * 
 * @author rpacheco
 *
 */
@SpringBootTest(classes = ApiApplication.class)
@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
class AWSControllerTest {
	
	
	private MockMvc mockMvc;

	/**
	 * @param webApplicationContext
	 * @param restDocumentation
	 */
	@BeforeEach
	public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(restDocumentation)).build();
	}
	
	/**
	 * Test del método
	 * {@link com.dinbog.api.controller.AuthController#login(com.dinbog.api.dto.LoginDTO)
	 * login(LoginDTO)}
	 * 
	 * @throws Exception
	 * @author Luis
	 */
	 @Test
	 void recognition() throws Exception {
		 
		 InputStream inputStreamSecondImage = new BufferedInputStream(new FileInputStream("src/main/resources/static/images/rostro.jpg"));
		 byte[] image = IOUtils.toByteArray(inputStreamSecondImage);

	     MockMultipartFile file = new MockMultipartFile("file", "rostro.jpg", MediaType.IMAGE_JPEG_VALUE, image);
	     ResultActions result = mockMvc.perform(multipart("/v1/aws/recognition").file(file))
	        	.andExpect(status().isOk())
	        	.andDo(document("aws/face-recognition", preprocessResponse(prettyPrint()),
						//requestParameters(
						//	parameterWithName("file").description("Photo to evaluated")),
						responseFields(
							fieldWithPath("validAvatar").description("Is a valid Avatar?"),
							fieldWithPath("urlFile").description("Url file on Amazon S3"),
							fieldWithPath("face").description("Data of Face")
					)));
	     
	     Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
	 }

}
