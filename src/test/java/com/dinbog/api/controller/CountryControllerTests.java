package com.dinbog.api.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dinbog.api.ApiApplication;

/**
 * Tests para la clase {@link com.dinbog.api.controller.CountryController
 * CountryController}
 * 
 *
 */
@SpringBootTest(classes = ApiApplication.class)
@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
class CountryControllerTests {

	private MockMvc mockMvc;

	@BeforeEach
	public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(restDocumentation)).build();
	}

	@Test
	void readAllByLanguageIdTest() throws Exception {

		String lang = "EN";
		
		ResultActions result = this.mockMvc
				.perform(RestDocumentationRequestBuilders.get("/v1/countries/?lang={lang}",lang).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andDo(document("countries/read-All", preprocessResponse(prettyPrint()),
						requestParameters(
							parameterWithName("lang").description("Language traslation").optional()),
						responseFields(
							fieldWithPath("[].id").description("Country id"),
							fieldWithPath("[].name").description("Country Name"),
							fieldWithPath("[].isoCode").description("Country IsoCode"),
							fieldWithPath("[].unitMeasurement").description("Country unit measurement"),
							fieldWithPath("[].wearMeasurement").description("Country wear measurement"),
							fieldWithPath("[].isOnu").description("Country is ONU?"),
							fieldWithPath("[].created").description("Created Date"),
							fieldWithPath("[].language").description("Id language")
					)));
		  
		Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
	}

	@Test
	void readCityByCountryIdTest() throws Exception {
		
		String lang = "EN";

		FieldDescriptor[] cities = new FieldDescriptor[] { 
				fieldWithPath("id").description("City Id"),
				fieldWithPath("code").description("City isoCode"),
				fieldWithPath("latitude").description("City Latitud"),
				fieldWithPath("longitude").description("City longitude"),
				fieldWithPath("stateName").description("City Name"),
				fieldWithPath("countryId").description("Country Id"),
				subsectionWithPath("key").description("City key for internationalization") };

		ResultActions result;
		result = this.mockMvc
				.perform(RestDocumentationRequestBuilders.get("/v1/countries/{id}/cities?lang={lang}", 183, lang)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andDo(document("city/read-cities-by-countryId", preprocessRequest(prettyPrint()),
						preprocessResponse(prettyPrint()),
						pathParameters(parameterWithName("id").description("Country Id")),
						requestParameters(parameterWithName("lang").description("Language traslation").optional()),
						responseFields(fieldWithPath("[]").description("Cities list by Country")).andWithPrefix("[].",
								cities))

				);

		Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
	}
}
