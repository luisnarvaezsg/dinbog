/**
 * 
 */
package com.dinbog.api.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.dinbog.api.dto.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dinbog.api.ApiApplication;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Tests para las clases {@link UserController
 * UserController} y {@link com.dinbog.api.controller.ProfilController
 * ProfilController}. Se manejan los test desde una sola clase para manejar data
 * generada dinamicamente
 * 
 * @author Luis
 *
 */
@SpringBootTest(classes = ApiApplication.class)
@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
class UserProfileControllerTests {

	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	/**
	 * @param webApplicationContext
	 * @param restDocumentation
	 */
	@BeforeEach
	public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(restDocumentation)).build();
	}

	
	@Test
	void createUser() throws Exception {

		UserCreateDTO user = new UserCreateDTO();
		int length = 10;
	    boolean useLetters = true;
	    boolean useNumbers = false;
	    String emailTest = RandomStringUtils.random(length, useLetters, useNumbers);
	 
	    user.setEmail(emailTest+"@gmail.com");
		user.setPassword("andrea21");
		user.setFirstName("Ruben");
		user.setProfileTypeId(1L);
		user.setLastName("Pacheco");
		
		ResultActions result = this.mockMvc.perform(RestDocumentationRequestBuilders.post("/v1/users")
				.contentType(MediaType.APPLICATION_JSON)
				.content(this.objectMapper.writeValueAsString(user)))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andDo(document("user/create", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
						// REQUEST FIELDS
						requestFields(
								fieldWithPath("email").description("User e.mail"),
								fieldWithPath("password").description("User password without decode"),
								fieldWithPath("firstName").description("User first name"),
								fieldWithPath("lastName").description("User last name"),
								fieldWithPath("profileTypeId").description("User profile type"),
								fieldWithPath("groups").description("User Groups data"),
								fieldWithPath("userStatusId").description("Status id")
                				
								),
						// RESPONSE FIELDS
						responseFields(
								fieldWithPath("id").description("User id created in database"),
                				fieldWithPath("firstName").description("User first name"),
                				fieldWithPath("lastName").description("User last name"),
                				fieldWithPath("email").description("User email"),
                				fieldWithPath("emailToken").description("Token email generated"),
                				fieldWithPath("token").description("Token generated"),
                				fieldWithPath("urlAvatar").description("User avatar path"),
                				fieldWithPath("userStatus").description("User staus data"),
                				fieldWithPath("userStatus.id").description("Status id"),
                				fieldWithPath("userStatus.name").description("Status name"),
                				fieldWithPath("groups[].id").description("Group id"),
								fieldWithPath("groups[].name").description("Group name"),
								fieldWithPath("groups[].description").description("Group description"),
								fieldWithPath("groups[].isActive").description("Group is active?"),
								fieldWithPath("groups[].created").description("Group create date")
								)

				));

		
		 Assertions.assertNotNull(result.andReturn().getResponse().getContentAsString());
		
		}



}
