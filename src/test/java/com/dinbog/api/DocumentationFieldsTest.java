package com.dinbog.api;

import lombok.Getter;

@Getter
public enum DocumentationFieldsTest {

    //Identificadores
    ID("id","Identificador"),
    PROFILE_ID("profileId","Id del Profile"),
    ALBUM_ID("albumId","Id del Album"),
    PARENT_ID("albumId","Parent ID Herencia Recursiva"),
    ALBUM_TYPE_ID("albumId","Id del Tipo de Album"),
    ATTACHMENT_ID("albumId","Id del Attachament"),
    REPORT_TYPE_ID("albumId","Id del Tipo de Reporte"),
    USER_STATUS_ID("albumId","Id del status del usuario"),
    OWNER_PROFILE_ID("albumId","Id del Profile Perfil Propietario Quien Realiza La Accion"),
    GENDER_ID("albumId","Id del Genero"),
    MEMBERSHIP_ID("albumId","Id de la Membresia"),
    AVATAR_ID("albumId","Id del avatar(foto)"),

    //Elementos y campos comunes
    LEVEL("level","Indica el nivel de prioridad del email (1=email principal)"),
    REPORT_DETAIL("albumId","Detalle o informacion del reporte"),
    URL_NAME("urlName","URL del Perfil"),
    VALUE("value","Valor o Nombre en Ingles"),
    VALUE_IN_LANG("value","Valor o Nombre en el idioma Selecionado del usuario"),
    VALUE_CHI("valueChi","Valor o Nombre en Chino"),
    VALUE_FRA("valueFra","Valor o Nombre en Frances"),
    VALUE_ITA("valueIta","Valor o Nombre en Italiano"),
    VALUE_SPA("valueSpa","Valor o Nombre en Español"),
    VALUE_POR("valuePor","Valor o Nombre en Portugues"),
    VALUE_RUS("valueRus","Valor o Nombre en Ruso"),



    DOMAIN("domain","Nombre de Dominio"),
    ACTIVE("active","Boleano si el elemento esta o no activo"),
    CLIENT_ID("clientId","Identificador String de Cliente"),
    KEY("key","Key o password de cliente"),
    NAME("name","Nombre de la entidad"),
    SECRET("secret","Password Secreto o alter Key del site"),


    DESCRIPTION("description",""),
    IS_PRIVATE("isPrivate","Boleano si el elemento es provado"),
    CONTENT("description","Contenido"),
    POST_CONTENT("content","Contenido del Post"),
    PROGRAMED("description","Boleano 1 marcado como programado 0 como no programado Post"),
    PROGRAMED_GMT("description","Fecha programada para publicacion del post en GMT"),

    //Contadores
    COUNT_COMMENT("description","Conteo de Likes"),
    COUNT_LIKE("description","Conteo de Comentarios"),

    //Objetos o Sub Sections
    PARENT("description","Padre Herencia o Recursividad"),
    CHILD("child","Hijos o realaciones de herencias"),
    SUB_SECTION_PROFILE("description","Perfil Objeto"),
    SUB_SECTION_PROFILE_TYPE("profileType","Tipo de Perfil Objeto"),
    SUB_SECTION_ALBUM("album","Album Objeto"),
    SUB_SECTION_ALBUM_TYPE("albumType","Tipo de Album Objeto"),

    OBJECT("{}","Objeto"),

    //List Elements
    LIST("[]","Lista "),
    LIST_ELEMENTS("listElements","Lista o Arreglo con todos los elementos solicitados de la busqueda"),

    //Paginations
    PAGE_NUMBER("pageNumber","Número de Página"),
    PAGE_SIZE("pageSize","Cantidad de Elementos de la pagina"),
    TOTAL_ELEMENTS("totalElements","Cantidad Total de Elementos del la busqueda"),
    TOTAL_PAGES("totalPages","Cantidad Total de Paginas"),
    SEARCH("search","Elemento de Busqueda"),

    //Standarts Date
    CREATED("created","Fecha de creacion"),
    UPDATED("updated","Fecha de Actualizacion"),
    ;


    private final String field;
    private final String description;

    DocumentationFieldsTest(String field, String description){
        this.field = field;
        this.description = description;
    }

    public String getField(){
        return this.field;
    }

    public String getDescription(){
        return this.description;
    }
}
